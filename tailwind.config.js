module.exports = {
  mode: 'jit',
  content: [
    './frontend/src/components/**/*.{jsx,ts,tsx}',
    './frontend/src/pages/**/*.{jsx,ts,tsx}',
    './frontend/src/layouts/**/*.{js,jsx,ts,tsx,vue}',
    './frontend/src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  media: false,
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
