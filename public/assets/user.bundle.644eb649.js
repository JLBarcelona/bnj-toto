(()=>{"use strict";var e,t,a,r={595:(e,t,a)=>{a.d(t,{Z:()=>m});var r=a(7537),n=a.n(r),l=a(3645),o=a.n(l)()(n());o.push([e.id,'/*\n! tailwindcss v3.2.4 | MIT License | https://tailwindcss.com\n*//*\n1. Prevent padding and border from affecting element width. (https://github.com/mozdevs/cssremedy/issues/4)\n2. Allow adding a border to an element by just adding a border-width. (https://github.com/tailwindcss/tailwindcss/pull/116)\n*/\n\n*,\n::before,\n::after {\n  box-sizing: border-box; /* 1 */\n  border-width: 0; /* 2 */\n  border-style: solid; /* 2 */\n  border-color: #e5e7eb; /* 2 */\n}\n\n::before,\n::after {\n  --tw-content: \'\';\n}\n\n/*\n1. Use a consistent sensible line-height in all browsers.\n2. Prevent adjustments of font size after orientation changes in iOS.\n3. Use a more readable tab size.\n4. Use the user\'s configured `sans` font-family by default.\n5. Use the user\'s configured `sans` font-feature-settings by default.\n*/\n\nhtml {\n  line-height: 1.5; /* 1 */\n  -webkit-text-size-adjust: 100%; /* 2 */\n  -moz-tab-size: 4; /* 3 */\n  -o-tab-size: 4;\n     tab-size: 4; /* 3 */\n  font-family: ui-sans-serif, system-ui, -apple-system, Segoe UI, Roboto, Ubuntu, Cantarell, Noto Sans, sans-serif, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; /* 4 */\n  font-feature-settings: normal; /* 5 */\n}\n\n/*\n1. Remove the margin in all browsers.\n2. Inherit line-height from `html` so users can set them as a class directly on the `html` element.\n*/\n\nbody {\n  margin: 0; /* 1 */\n  line-height: inherit; /* 2 */\n}\n\n/*\n1. Add the correct height in Firefox.\n2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)\n3. Ensure horizontal rules are visible by default.\n*/\n\nhr {\n  height: 0; /* 1 */\n  color: inherit; /* 2 */\n  border-top-width: 1px; /* 3 */\n}\n\n/*\nAdd the correct text decoration in Chrome, Edge, and Safari.\n*/\n\nabbr:where([title]) {\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n}\n\n/*\nRemove the default font size and weight for headings.\n*/\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-size: inherit;\n  font-weight: inherit;\n}\n\n/*\nReset links to optimize for opt-in styling instead of opt-out.\n*/\n\na {\n  color: inherit;\n  text-decoration: inherit;\n}\n\n/*\nAdd the correct font weight in Edge and Safari.\n*/\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\n/*\n1. Use the user\'s configured `mono` font family by default.\n2. Correct the odd `em` font sizing in all browsers.\n*/\n\ncode,\nkbd,\nsamp,\npre {\n  font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace; /* 1 */\n  font-size: 1em; /* 2 */\n}\n\n/*\nAdd the correct font size in all browsers.\n*/\n\nsmall {\n  font-size: 80%;\n}\n\n/*\nPrevent `sub` and `sup` elements from affecting the line height in all browsers.\n*/\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n/*\n1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)\n2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)\n3. Remove gaps between table borders by default.\n*/\n\ntable {\n  text-indent: 0; /* 1 */\n  border-color: inherit; /* 2 */\n  border-collapse: collapse; /* 3 */\n}\n\n/*\n1. Change the font styles in all browsers.\n2. Remove the margin in Firefox and Safari.\n3. Remove default padding in all browsers.\n*/\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit; /* 1 */\n  font-size: 100%; /* 1 */\n  font-weight: inherit; /* 1 */\n  line-height: inherit; /* 1 */\n  color: inherit; /* 1 */\n  margin: 0; /* 2 */\n  padding: 0; /* 3 */\n}\n\n/*\nRemove the inheritance of text transform in Edge and Firefox.\n*/\n\nbutton,\nselect {\n  text-transform: none;\n}\n\n/*\n1. Correct the inability to style clickable types in iOS and Safari.\n2. Remove default button styles.\n*/\n\nbutton,\n[type=\'button\'],\n[type=\'reset\'],\n[type=\'submit\'] {\n  -webkit-appearance: button; /* 1 */\n  background-color: transparent; /* 2 */\n  background-image: none; /* 2 */\n}\n\n/*\nUse the modern Firefox focus style for all focusable elements.\n*/\n\n:-moz-focusring {\n  outline: auto;\n}\n\n/*\nRemove the additional `:invalid` styles in Firefox. (https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737)\n*/\n\n:-moz-ui-invalid {\n  box-shadow: none;\n}\n\n/*\nAdd the correct vertical alignment in Chrome and Firefox.\n*/\n\nprogress {\n  vertical-align: baseline;\n}\n\n/*\nCorrect the cursor style of increment and decrement buttons in Safari.\n*/\n\n::-webkit-inner-spin-button,\n::-webkit-outer-spin-button {\n  height: auto;\n}\n\n/*\n1. Correct the odd appearance in Chrome and Safari.\n2. Correct the outline style in Safari.\n*/\n\n[type=\'search\'] {\n  -webkit-appearance: textfield; /* 1 */\n  outline-offset: -2px; /* 2 */\n}\n\n/*\nRemove the inner padding in Chrome and Safari on macOS.\n*/\n\n::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n/*\n1. Correct the inability to style clickable types in iOS and Safari.\n2. Change font properties to `inherit` in Safari.\n*/\n\n::-webkit-file-upload-button {\n  -webkit-appearance: button; /* 1 */\n  font: inherit; /* 2 */\n}\n\n/*\nAdd the correct display in Chrome and Safari.\n*/\n\nsummary {\n  display: list-item;\n}\n\n/*\nRemoves the default spacing and border for appropriate elements.\n*/\n\nblockquote,\ndl,\ndd,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\nhr,\nfigure,\np,\npre {\n  margin: 0;\n}\n\nfieldset {\n  margin: 0;\n  padding: 0;\n}\n\nlegend {\n  padding: 0;\n}\n\nol,\nul,\nmenu {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n}\n\n/*\nPrevent resizing textareas horizontally by default.\n*/\n\ntextarea {\n  resize: vertical;\n}\n\n/*\n1. Reset the default placeholder opacity in Firefox. (https://github.com/tailwindlabs/tailwindcss/issues/3300)\n2. Set the default placeholder color to the user\'s configured gray 400 color.\n*/\n\ninput::-moz-placeholder, textarea::-moz-placeholder {\n  opacity: 1; /* 1 */\n  color: #9ca3af; /* 2 */\n}\n\ninput:-ms-input-placeholder, textarea:-ms-input-placeholder {\n  opacity: 1; /* 1 */\n  color: #9ca3af; /* 2 */\n}\n\ninput::placeholder,\ntextarea::placeholder {\n  opacity: 1; /* 1 */\n  color: #9ca3af; /* 2 */\n}\n\n/*\nSet the default cursor for buttons.\n*/\n\nbutton,\n[role="button"] {\n  cursor: pointer;\n}\n\n/*\nMake sure disabled buttons don\'t get the pointer cursor.\n*/\n:disabled {\n  cursor: default;\n}\n\n/*\n1. Make replaced elements `display: block` by default. (https://github.com/mozdevs/cssremedy/issues/14)\n2. Add `vertical-align: middle` to align replaced elements more sensibly by default. (https://github.com/jensimmons/cssremedy/issues/14#issuecomment-634934210)\n   This can trigger a poorly considered lint error in some tools but is included by design.\n*/\n\nimg,\nsvg,\nvideo,\ncanvas,\naudio,\niframe,\nembed,\nobject {\n  display: block; /* 1 */\n  vertical-align: middle; /* 2 */\n}\n\n/*\nConstrain images and videos to the parent width and preserve their intrinsic aspect ratio. (https://github.com/mozdevs/cssremedy/issues/14)\n*/\n\nimg,\nvideo {\n  max-width: 100%;\n  height: auto;\n}\n\n/* Make elements with the HTML hidden attribute stay hidden by default */\n[hidden] {\n  display: none;\n}\n\n*, ::before, ::after {\n  --tw-border-spacing-x: 0;\n  --tw-border-spacing-y: 0;\n  --tw-translate-x: 0;\n  --tw-translate-y: 0;\n  --tw-rotate: 0;\n  --tw-skew-x: 0;\n  --tw-skew-y: 0;\n  --tw-scale-x: 1;\n  --tw-scale-y: 1;\n  --tw-pan-x:  ;\n  --tw-pan-y:  ;\n  --tw-pinch-zoom:  ;\n  --tw-scroll-snap-strictness: proximity;\n  --tw-ordinal:  ;\n  --tw-slashed-zero:  ;\n  --tw-numeric-figure:  ;\n  --tw-numeric-spacing:  ;\n  --tw-numeric-fraction:  ;\n  --tw-ring-inset:  ;\n  --tw-ring-offset-width: 0px;\n  --tw-ring-offset-color: #fff;\n  --tw-ring-color: rgb(59 130 246 / 0.5);\n  --tw-ring-offset-shadow: 0 0 rgba(0,0,0,0);\n  --tw-ring-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow-colored: 0 0 rgba(0,0,0,0);\n  --tw-blur:  ;\n  --tw-brightness:  ;\n  --tw-contrast:  ;\n  --tw-grayscale:  ;\n  --tw-hue-rotate:  ;\n  --tw-invert:  ;\n  --tw-saturate:  ;\n  --tw-sepia:  ;\n  --tw-drop-shadow:  ;\n  --tw-backdrop-blur:  ;\n  --tw-backdrop-brightness:  ;\n  --tw-backdrop-contrast:  ;\n  --tw-backdrop-grayscale:  ;\n  --tw-backdrop-hue-rotate:  ;\n  --tw-backdrop-invert:  ;\n  --tw-backdrop-opacity:  ;\n  --tw-backdrop-saturate:  ;\n  --tw-backdrop-sepia:  ;\n}\n\n::backdrop {\n  --tw-border-spacing-x: 0;\n  --tw-border-spacing-y: 0;\n  --tw-translate-x: 0;\n  --tw-translate-y: 0;\n  --tw-rotate: 0;\n  --tw-skew-x: 0;\n  --tw-skew-y: 0;\n  --tw-scale-x: 1;\n  --tw-scale-y: 1;\n  --tw-pan-x:  ;\n  --tw-pan-y:  ;\n  --tw-pinch-zoom:  ;\n  --tw-scroll-snap-strictness: proximity;\n  --tw-ordinal:  ;\n  --tw-slashed-zero:  ;\n  --tw-numeric-figure:  ;\n  --tw-numeric-spacing:  ;\n  --tw-numeric-fraction:  ;\n  --tw-ring-inset:  ;\n  --tw-ring-offset-width: 0px;\n  --tw-ring-offset-color: #fff;\n  --tw-ring-color: rgb(59 130 246 / 0.5);\n  --tw-ring-offset-shadow: 0 0 rgba(0,0,0,0);\n  --tw-ring-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow-colored: 0 0 rgba(0,0,0,0);\n  --tw-blur:  ;\n  --tw-brightness:  ;\n  --tw-contrast:  ;\n  --tw-grayscale:  ;\n  --tw-hue-rotate:  ;\n  --tw-invert:  ;\n  --tw-saturate:  ;\n  --tw-sepia:  ;\n  --tw-drop-shadow:  ;\n  --tw-backdrop-blur:  ;\n  --tw-backdrop-brightness:  ;\n  --tw-backdrop-contrast:  ;\n  --tw-backdrop-grayscale:  ;\n  --tw-backdrop-hue-rotate:  ;\n  --tw-backdrop-invert:  ;\n  --tw-backdrop-opacity:  ;\n  --tw-backdrop-saturate:  ;\n  --tw-backdrop-sepia:  ;\n}\n.sr-only {\n  position: absolute;\n  width: 1px;\n  height: 1px;\n  padding: 0;\n  margin: -1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0);\n  white-space: nowrap;\n  border-width: 0;\n}\n.pointer-events-none {\n  pointer-events: none;\n}\n.fixed {\n  position: fixed;\n}\n.absolute {\n  position: absolute;\n}\n.relative {\n  position: relative;\n}\n.sticky {\n  position: sticky;\n}\n.inset-x-0 {\n  left: 0px;\n  right: 0px;\n}\n.inset-y-0 {\n  top: 0px;\n  bottom: 0px;\n}\n.top-0 {\n  top: 0px;\n}\n.right-0 {\n  right: 0px;\n}\n.left-0 {\n  left: 0px;\n}\n.top-full {\n  top: 100%;\n}\n.bottom-0 {\n  bottom: 0px;\n}\n.top-60 {\n  top: 15rem;\n}\n.right-10 {\n  right: 2.5rem;\n}\n.z-10 {\n  z-index: 10;\n}\n.z-0 {\n  z-index: 0;\n}\n.col-span-6 {\n  grid-column: span 6 / span 6;\n}\n.col-span-2 {\n  grid-column: span 2 / span 2;\n}\n.col-span-12 {\n  grid-column: span 12 / span 12;\n}\n.col-span-8 {\n  grid-column: span 8 / span 8;\n}\n.col-span-4 {\n  grid-column: span 4 / span 4;\n}\n.col-span-3 {\n  grid-column: span 3 / span 3;\n}\n.col-span-1 {\n  grid-column: span 1 / span 1;\n}\n.col-span-full {\n  grid-column: 1 / -1;\n}\n.m-auto {\n  margin: auto;\n}\n.-my-2 {\n  margin-top: -0.5rem;\n  margin-bottom: -0.5rem;\n}\n.mx-auto {\n  margin-left: auto;\n  margin-right: auto;\n}\n.my-4 {\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n.my-2 {\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n.mx-1 {\n  margin-left: 0.25rem;\n  margin-right: 0.25rem;\n}\n.mx-2 {\n  margin-left: 0.5rem;\n  margin-right: 0.5rem;\n}\n.my-auto {\n  margin-top: auto;\n  margin-bottom: auto;\n}\n.mx-4 {\n  margin-left: 1rem;\n  margin-right: 1rem;\n}\n.my-6 {\n  margin-top: 1.5rem;\n  margin-bottom: 1.5rem;\n}\n.ml-4 {\n  margin-left: 1rem;\n}\n.mb-4 {\n  margin-bottom: 1rem;\n}\n.mt-6 {\n  margin-top: 1.5rem;\n}\n.mt-1 {\n  margin-top: 0.25rem;\n}\n.mt-4 {\n  margin-top: 1rem;\n}\n.mt-8 {\n  margin-top: 2rem;\n}\n.ml-3 {\n  margin-left: 0.75rem;\n}\n.mt-2 {\n  margin-top: 0.5rem;\n}\n.-mb-px {\n  margin-bottom: -1px;\n}\n.ml-10 {\n  margin-left: 2.5rem;\n}\n.mt-10 {\n  margin-top: 2.5rem;\n}\n.ml-1 {\n  margin-left: 0.25rem;\n}\n.mr-4 {\n  margin-right: 1rem;\n}\n.ml-auto {\n  margin-left: auto;\n}\n.ml-2 {\n  margin-left: 0.5rem;\n}\n.mr-2 {\n  margin-right: 0.5rem;\n}\n.mr-3 {\n  margin-right: 0.75rem;\n}\n.-ml-1 {\n  margin-left: -0.25rem;\n}\n.mb-2 {\n  margin-bottom: 0.5rem;\n}\n.mt-5 {\n  margin-top: 1.25rem;\n}\n.block {\n  display: block;\n}\n.inline-block {\n  display: inline-block;\n}\n.flex {\n  display: flex;\n}\n.inline-flex {\n  display: inline-flex;\n}\n.table {\n  display: table;\n}\n.grid {\n  display: grid;\n}\n.hidden {\n  display: none;\n}\n.h-5 {\n  height: 1.25rem;\n}\n.h-full {\n  height: 100%;\n}\n.h-12 {\n  height: 3rem;\n}\n.h-4 {\n  height: 1rem;\n}\n.h-6 {\n  height: 1.5rem;\n}\n.h-8 {\n  height: 2rem;\n}\n.h-10 {\n  height: 2.5rem;\n}\n.h-72 {\n  height: 18rem;\n}\n.h-64 {\n  height: 16rem;\n}\n.h-0\\.5 {\n  height: 0.125rem;\n}\n.h-0 {\n  height: 0px;\n}\n.h-44 {\n  height: 11rem;\n}\n.h-40 {\n  height: 10rem;\n}\n.h-2 {\n  height: 0.5rem;\n}\n.h-3 {\n  height: 0.75rem;\n}\n.h-screen {\n  height: 100vh;\n}\n.max-h-\\[560px\\] {\n  max-height: 560px;\n}\n.min-h-screen {\n  min-height: 100vh;\n}\n.w-5 {\n  width: 1.25rem;\n}\n.w-full {\n  width: 100%;\n}\n.w-64 {\n  width: 16rem;\n}\n.w-44 {\n  width: 11rem;\n}\n.w-40 {\n  width: 10rem;\n}\n.w-12 {\n  width: 3rem;\n}\n.w-28 {\n  width: 7rem;\n}\n.w-20 {\n  width: 5rem;\n}\n.w-auto {\n  width: auto;\n}\n.w-72 {\n  width: 18rem;\n}\n.w-10 {\n  width: 2.5rem;\n}\n.w-32 {\n  width: 8rem;\n}\n.w-6 {\n  width: 1.5rem;\n}\n.w-80 {\n  width: 20rem;\n}\n.w-24 {\n  width: 6rem;\n}\n.w-screen {\n  width: 100vw;\n}\n.w-4 {\n  width: 1rem;\n}\n.w-60 {\n  width: 15rem;\n}\n.w-8 {\n  width: 2rem;\n}\n.w-48 {\n  width: 12rem;\n}\n.w-96 {\n  width: 24rem;\n}\n.w-\\[1100px\\] {\n  width: 1100px;\n}\n.w-36 {\n  width: 9rem;\n}\n.w-16 {\n  width: 4rem;\n}\n.w-3 {\n  width: 0.75rem;\n}\n.w-\\[200px\\] {\n  width: 200px;\n}\n.min-w-full {\n  min-width: 100%;\n}\n.max-w-xl {\n  max-width: 36rem;\n}\n.flex-1 {\n  flex: 1 1 0%;\n}\n.flex-shrink-0 {\n  flex-shrink: 0;\n}\n.origin-top-right {\n  transform-origin: top right;\n}\n.transform {\n  transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));\n}\n@keyframes spin {\n\n  to {\n    transform: rotate(360deg);\n  }\n}\n.animate-spin {\n  animation: spin 1s linear infinite;\n}\n.cursor-pointer {\n  cursor: pointer;\n}\n.list-disc {\n  list-style-type: disc;\n}\n.appearance-none {\n  -webkit-appearance: none;\n     -moz-appearance: none;\n          appearance: none;\n}\n.grid-cols-1 {\n  grid-template-columns: repeat(1, minmax(0, 1fr));\n}\n.grid-cols-5 {\n  grid-template-columns: repeat(5, minmax(0, 1fr));\n}\n.flex-col {\n  flex-direction: column;\n}\n.flex-wrap {\n  flex-wrap: wrap;\n}\n.items-start {\n  align-items: flex-start;\n}\n.items-center {\n  align-items: center;\n}\n.justify-end {\n  justify-content: flex-end;\n}\n.justify-center {\n  justify-content: center;\n}\n.justify-between {\n  justify-content: space-between;\n}\n.gap-2 {\n  gap: 0.5rem;\n}\n.gap-4 {\n  gap: 1rem;\n}\n.gap-y-6 {\n  row-gap: 1.5rem;\n}\n.gap-x-4 {\n  -moz-column-gap: 1rem;\n       column-gap: 1rem;\n}\n.gap-y-2 {\n  row-gap: 0.5rem;\n}\n.gap-x-2 {\n  -moz-column-gap: 0.5rem;\n       column-gap: 0.5rem;\n}\n.space-x-4 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(1rem * var(--tw-space-x-reverse));\n  margin-left: calc(1rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-2 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(0.5rem * var(--tw-space-x-reverse));\n  margin-left: calc(0.5rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-y-1 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-y-reverse: 0;\n  margin-top: calc(0.25rem * calc(1 - var(--tw-space-y-reverse)));\n  margin-bottom: calc(0.25rem * var(--tw-space-y-reverse));\n}\n.space-y-6 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-y-reverse: 0;\n  margin-top: calc(1.5rem * calc(1 - var(--tw-space-y-reverse)));\n  margin-bottom: calc(1.5rem * var(--tw-space-y-reverse));\n}\n.space-x-8 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(2rem * var(--tw-space-x-reverse));\n  margin-left: calc(2rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-y-4 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-y-reverse: 0;\n  margin-top: calc(1rem * calc(1 - var(--tw-space-y-reverse)));\n  margin-bottom: calc(1rem * var(--tw-space-y-reverse));\n}\n.space-x-1 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(0.25rem * var(--tw-space-x-reverse));\n  margin-left: calc(0.25rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-7 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(1.75rem * var(--tw-space-x-reverse));\n  margin-left: calc(1.75rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.-space-x-px > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(-1px * var(--tw-space-x-reverse));\n  margin-left: calc(-1px * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-6 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(1.5rem * var(--tw-space-x-reverse));\n  margin-left: calc(1.5rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.divide-y > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-y-reverse: 0;\n  border-top-width: calc(1px * calc(1 - var(--tw-divide-y-reverse)));\n  border-bottom-width: calc(1px * var(--tw-divide-y-reverse));\n}\n.divide-gray-200 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(229 231 235 / var(--tw-divide-opacity));\n}\n.divide-gray-400 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(156 163 175 / var(--tw-divide-opacity));\n}\n.divide-gray-100 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(243 244 246 / var(--tw-divide-opacity));\n}\n.divide-gray-300 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(209 213 219 / var(--tw-divide-opacity));\n}\n.overflow-hidden {\n  overflow: hidden;\n}\n.overflow-x-auto {\n  overflow-x: auto;\n}\n.overflow-y-auto {\n  overflow-y: auto;\n}\n.overflow-y-scroll {\n  overflow-y: scroll;\n}\n.whitespace-nowrap {\n  white-space: nowrap;\n}\n.whitespace-pre {\n  white-space: pre;\n}\n.whitespace-pre-wrap {\n  white-space: pre-wrap;\n}\n.rounded-md {\n  border-radius: 0.375rem;\n}\n.rounded-sm {\n  border-radius: 0.125rem;\n}\n.rounded {\n  border-radius: 0.25rem;\n}\n.rounded-full {\n  border-radius: 9999px;\n}\n.rounded-lg {\n  border-radius: 0.5rem;\n}\n.rounded-l-md {\n  border-top-left-radius: 0.375rem;\n  border-bottom-left-radius: 0.375rem;\n}\n.rounded-r-md {\n  border-top-right-radius: 0.375rem;\n  border-bottom-right-radius: 0.375rem;\n}\n.border {\n  border-width: 1px;\n}\n.border-b {\n  border-bottom-width: 1px;\n}\n.border-b-2 {\n  border-bottom-width: 2px;\n}\n.border-t {\n  border-top-width: 1px;\n}\n.border-gray-300 {\n  --tw-border-opacity: 1;\n  border-color: rgb(209 213 219 / var(--tw-border-opacity));\n}\n.border-transparent {\n  border-color: transparent;\n}\n.border-gray-200 {\n  --tw-border-opacity: 1;\n  border-color: rgb(229 231 235 / var(--tw-border-opacity));\n}\n.border-indigo-500 {\n  --tw-border-opacity: 1;\n  border-color: rgb(99 102 241 / var(--tw-border-opacity));\n}\n.border-gray-400 {\n  --tw-border-opacity: 1;\n  border-color: rgb(156 163 175 / var(--tw-border-opacity));\n}\n.border-red-400 {\n  --tw-border-opacity: 1;\n  border-color: rgb(248 113 113 / var(--tw-border-opacity));\n}\n.border-gray-500 {\n  --tw-border-opacity: 1;\n  border-color: rgb(107 114 128 / var(--tw-border-opacity));\n}\n.border-red-500 {\n  --tw-border-opacity: 1;\n  border-color: rgb(239 68 68 / var(--tw-border-opacity));\n}\n.border-gray-900 {\n  --tw-border-opacity: 1;\n  border-color: rgb(17 24 39 / var(--tw-border-opacity));\n}\n.border-rose-600 {\n  --tw-border-opacity: 1;\n  border-color: rgb(225 29 72 / var(--tw-border-opacity));\n}\n.bg-white {\n  --tw-bg-opacity: 1;\n  background-color: rgb(255 255 255 / var(--tw-bg-opacity));\n}\n.bg-indigo-600 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(79 70 229 / var(--tw-bg-opacity));\n}\n.bg-gray-50 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(249 250 251 / var(--tw-bg-opacity));\n}\n.bg-red-50 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 242 242 / var(--tw-bg-opacity));\n}\n.bg-gray-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(243 244 246 / var(--tw-bg-opacity));\n}\n.bg-blue-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(219 234 254 / var(--tw-bg-opacity));\n}\n.bg-yellow-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 249 195 / var(--tw-bg-opacity));\n}\n.bg-red-200 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 202 202 / var(--tw-bg-opacity));\n}\n.bg-indigo-50 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(238 242 255 / var(--tw-bg-opacity));\n}\n.bg-gray-200 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(229 231 235 / var(--tw-bg-opacity));\n}\n.bg-green-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(220 252 231 / var(--tw-bg-opacity));\n}\n.bg-transparent {\n  background-color: transparent;\n}\n.bg-indigo-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(224 231 255 / var(--tw-bg-opacity));\n}\n.bg-gray-600 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(75 85 99 / var(--tw-bg-opacity));\n}\n.bg-red-500 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(239 68 68 / var(--tw-bg-opacity));\n}\n.bg-gray-300 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(209 213 219 / var(--tw-bg-opacity));\n}\n.bg-blue-300 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(147 197 253 / var(--tw-bg-opacity));\n}\n.fill-blue-600 {\n  fill: #2563eb;\n}\n.object-contain {\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.p-4 {\n  padding: 1rem;\n}\n.p-6 {\n  padding: 1.5rem;\n}\n.p-2 {\n  padding: 0.5rem;\n}\n.p-0 {\n  padding: 0px;\n}\n.px-2 {\n  padding-left: 0.5rem;\n  padding-right: 0.5rem;\n}\n.py-2 {\n  padding-top: 0.5rem;\n  padding-bottom: 0.5rem;\n}\n.py-4 {\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n}\n.px-10 {\n  padding-left: 2.5rem;\n  padding-right: 2.5rem;\n}\n.px-4 {\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n.py-1 {\n  padding-top: 0.25rem;\n  padding-bottom: 0.25rem;\n}\n.py-3 {\n  padding-top: 0.75rem;\n  padding-bottom: 0.75rem;\n}\n.py-8 {\n  padding-top: 2rem;\n  padding-bottom: 2rem;\n}\n.px-3 {\n  padding-left: 0.75rem;\n  padding-right: 0.75rem;\n}\n.px-1 {\n  padding-left: 0.25rem;\n  padding-right: 0.25rem;\n}\n.px-6 {\n  padding-left: 1.5rem;\n  padding-right: 1.5rem;\n}\n.py-6 {\n  padding-top: 1.5rem;\n  padding-bottom: 1.5rem;\n}\n.px-2\\.5 {\n  padding-left: 0.625rem;\n  padding-right: 0.625rem;\n}\n.py-1\\.5 {\n  padding-top: 0.375rem;\n  padding-bottom: 0.375rem;\n}\n.py-0\\.5 {\n  padding-top: 0.125rem;\n  padding-bottom: 0.125rem;\n}\n.py-0 {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n.py-3\\.5 {\n  padding-top: 0.875rem;\n  padding-bottom: 0.875rem;\n}\n.py-12 {\n  padding-top: 3rem;\n  padding-bottom: 3rem;\n}\n.pl-5 {\n  padding-left: 1.25rem;\n}\n.pl-3 {\n  padding-left: 0.75rem;\n}\n.pr-10 {\n  padding-right: 2.5rem;\n}\n.pt-4 {\n  padding-top: 1rem;\n}\n.pb-10 {\n  padding-bottom: 2.5rem;\n}\n.pl-4 {\n  padding-left: 1rem;\n}\n.pr-3 {\n  padding-right: 0.75rem;\n}\n.pl-10 {\n  padding-left: 2.5rem;\n}\n.pl-1 {\n  padding-left: 0.25rem;\n}\n.pb-20 {\n  padding-bottom: 5rem;\n}\n.pt-5 {\n  padding-top: 1.25rem;\n}\n.pb-4 {\n  padding-bottom: 1rem;\n}\n.text-left {\n  text-align: left;\n}\n.text-center {\n  text-align: center;\n}\n.text-right {\n  text-align: right;\n}\n.align-top {\n  vertical-align: top;\n}\n.align-middle {\n  vertical-align: middle;\n}\n.text-sm {\n  font-size: 0.875rem;\n  line-height: 1.25rem;\n}\n.text-2xl {\n  font-size: 1.5rem;\n  line-height: 2rem;\n}\n.text-xs {\n  font-size: 0.75rem;\n  line-height: 1rem;\n}\n.text-3xl {\n  font-size: 1.875rem;\n  line-height: 2.25rem;\n}\n.text-base {\n  font-size: 1rem;\n  line-height: 1.5rem;\n}\n.text-\\[10px\\] {\n  font-size: 10px;\n}\n.text-xl {\n  font-size: 1.25rem;\n  line-height: 1.75rem;\n}\n.text-lg {\n  font-size: 1.125rem;\n  line-height: 1.75rem;\n}\n.font-medium {\n  font-weight: 500;\n}\n.font-bold {\n  font-weight: 700;\n}\n.font-extrabold {\n  font-weight: 800;\n}\n.font-semibold {\n  font-weight: 600;\n}\n.uppercase {\n  text-transform: uppercase;\n}\n.leading-9 {\n  line-height: 2.25rem;\n}\n.leading-8 {\n  line-height: 2rem;\n}\n.leading-6 {\n  line-height: 1.5rem;\n}\n.leading-7 {\n  line-height: 1.75rem;\n}\n.tracking-wider {\n  letter-spacing: 0.05em;\n}\n.text-gray-500 {\n  --tw-text-opacity: 1;\n  color: rgb(107 114 128 / var(--tw-text-opacity));\n}\n.text-gray-400 {\n  --tw-text-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-text-opacity));\n}\n.text-gray-700 {\n  --tw-text-opacity: 1;\n  color: rgb(55 65 81 / var(--tw-text-opacity));\n}\n.text-white {\n  --tw-text-opacity: 1;\n  color: rgb(255 255 255 / var(--tw-text-opacity));\n}\n.text-gray-900 {\n  --tw-text-opacity: 1;\n  color: rgb(17 24 39 / var(--tw-text-opacity));\n}\n.text-red-800 {\n  --tw-text-opacity: 1;\n  color: rgb(153 27 27 / var(--tw-text-opacity));\n}\n.text-red-700 {\n  --tw-text-opacity: 1;\n  color: rgb(185 28 28 / var(--tw-text-opacity));\n}\n.text-indigo-600 {\n  --tw-text-opacity: 1;\n  color: rgb(79 70 229 / var(--tw-text-opacity));\n}\n.text-red-400 {\n  --tw-text-opacity: 1;\n  color: rgb(248 113 113 / var(--tw-text-opacity));\n}\n.text-blue-500 {\n  --tw-text-opacity: 1;\n  color: rgb(59 130 246 / var(--tw-text-opacity));\n}\n.text-red-500 {\n  --tw-text-opacity: 1;\n  color: rgb(239 68 68 / var(--tw-text-opacity));\n}\n.text-red-600 {\n  --tw-text-opacity: 1;\n  color: rgb(220 38 38 / var(--tw-text-opacity));\n}\n.text-gray-300 {\n  --tw-text-opacity: 1;\n  color: rgb(209 213 219 / var(--tw-text-opacity));\n}\n.text-gray-200 {\n  --tw-text-opacity: 1;\n  color: rgb(229 231 235 / var(--tw-text-opacity));\n}\n.text-yellow-800 {\n  --tw-text-opacity: 1;\n  color: rgb(133 77 14 / var(--tw-text-opacity));\n}\n.text-black {\n  --tw-text-opacity: 1;\n  color: rgb(0 0 0 / var(--tw-text-opacity));\n}\n.text-green-800 {\n  --tw-text-opacity: 1;\n  color: rgb(22 101 52 / var(--tw-text-opacity));\n}\n.text-gray-800 {\n  --tw-text-opacity: 1;\n  color: rgb(31 41 55 / var(--tw-text-opacity));\n}\n.text-gray-600 {\n  --tw-text-opacity: 1;\n  color: rgb(75 85 99 / var(--tw-text-opacity));\n}\n.text-rose-600 {\n  --tw-text-opacity: 1;\n  color: rgb(225 29 72 / var(--tw-text-opacity));\n}\n.text-green-700 {\n  --tw-text-opacity: 1;\n  color: rgb(21 128 61 / var(--tw-text-opacity));\n}\n.text-blue-700 {\n  --tw-text-opacity: 1;\n  color: rgb(29 78 216 / var(--tw-text-opacity));\n}\n.placeholder-gray-400::-moz-placeholder {\n  --tw-placeholder-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-placeholder-opacity));\n}\n.placeholder-gray-400:-ms-input-placeholder {\n  --tw-placeholder-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-placeholder-opacity));\n}\n.placeholder-gray-400::placeholder {\n  --tw-placeholder-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-placeholder-opacity));\n}\n.opacity-25 {\n  opacity: 0.25;\n}\n.opacity-75 {\n  opacity: 0.75;\n}\n.shadow-sm {\n  --tw-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);\n  --tw-shadow-colored: 0 1px 2px 0 var(--tw-shadow-color);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: var(--tw-ring-offset-shadow, 0 0 rgba(0,0,0,0)), var(--tw-ring-shadow, 0 0 rgba(0,0,0,0)), var(--tw-shadow);\n}\n.shadow {\n  --tw-shadow: 0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1);\n  --tw-shadow-colored: 0 1px 3px 0 var(--tw-shadow-color), 0 1px 2px -1px var(--tw-shadow-color);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: var(--tw-ring-offset-shadow, 0 0 rgba(0,0,0,0)), var(--tw-ring-shadow, 0 0 rgba(0,0,0,0)), var(--tw-shadow);\n}\n.shadow-lg {\n  --tw-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);\n  --tw-shadow-colored: 0 10px 15px -3px var(--tw-shadow-color), 0 4px 6px -4px var(--tw-shadow-color);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: var(--tw-ring-offset-shadow, 0 0 rgba(0,0,0,0)), var(--tw-ring-shadow, 0 0 rgba(0,0,0,0)), var(--tw-shadow);\n}\n.ring-1 {\n  --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);\n  --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 rgba(0,0,0,0));\n}\n.ring-black {\n  --tw-ring-opacity: 1;\n  --tw-ring-color: rgb(0 0 0 / var(--tw-ring-opacity));\n}\n.ring-opacity-5 {\n  --tw-ring-opacity: 0.05;\n}\n.filter {\n  filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);\n}\n\nhtml,\nbody {\n  padding: 0;\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans,\n    Helvetica Neue, sans-serif;\n}\n\na {\n  color: inherit;\n  text-decoration: none;\n}\n\n* {\n  box-sizing: border-box;\n}\n\n.hover\\:border-gray-300:hover {\n  --tw-border-opacity: 1;\n  border-color: rgb(209 213 219 / var(--tw-border-opacity));\n}\n\n.hover\\:bg-gray-50:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(249 250 251 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-indigo-300:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(165 180 252 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-indigo-700:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(67 56 202 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-200:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(229 231 235 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-red-200:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 202 202 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-red-300:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(252 165 165 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-300:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(209 213 219 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-100:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(243 244 246 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-rose-50:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(255 241 242 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-red-400:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(248 113 113 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-blue-100:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(219 234 254 / var(--tw-bg-opacity));\n}\n\n.hover\\:text-gray-700:hover {\n  --tw-text-opacity: 1;\n  color: rgb(55 65 81 / var(--tw-text-opacity));\n}\n\n.hover\\:text-blue-300:hover {\n  --tw-text-opacity: 1;\n  color: rgb(147 197 253 / var(--tw-text-opacity));\n}\n\n.hover\\:text-gray-800:hover {\n  --tw-text-opacity: 1;\n  color: rgb(31 41 55 / var(--tw-text-opacity));\n}\n\n.hover\\:text-gray-900:hover {\n  --tw-text-opacity: 1;\n  color: rgb(17 24 39 / var(--tw-text-opacity));\n}\n\n.hover\\:underline:hover {\n  -webkit-text-decoration-line: underline;\n          text-decoration-line: underline;\n}\n\n.focus\\:border-indigo-500:focus {\n  --tw-border-opacity: 1;\n  border-color: rgb(99 102 241 / var(--tw-border-opacity));\n}\n\n.focus\\:border-red-500:focus {\n  --tw-border-opacity: 1;\n  border-color: rgb(239 68 68 / var(--tw-border-opacity));\n}\n\n.focus\\:outline-none:focus {\n  outline: 2px solid transparent;\n  outline-offset: 2px;\n}\n\n.focus\\:ring-2:focus {\n  --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);\n  --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 rgba(0,0,0,0));\n}\n\n.focus\\:ring-indigo-500:focus {\n  --tw-ring-opacity: 1;\n  --tw-ring-color: rgb(99 102 241 / var(--tw-ring-opacity));\n}\n\n.focus\\:ring-red-500:focus {\n  --tw-ring-opacity: 1;\n  --tw-ring-color: rgb(239 68 68 / var(--tw-ring-opacity));\n}\n\n.focus\\:ring-offset-2:focus {\n  --tw-ring-offset-width: 2px;\n}\n\n.focus\\:ring-offset-gray-100:focus {\n  --tw-ring-offset-color: #f3f4f6;\n}\n\n.disabled\\:opacity-30:disabled {\n  opacity: 0.3;\n}\n\n.disabled\\:opacity-40:disabled {\n  opacity: 0.4;\n}\n\n.disabled\\:opacity-50:disabled {\n  opacity: 0.5;\n}\n\n@media (prefers-color-scheme: dark) {\n\n  .dark\\:text-gray-600 {\n    --tw-text-opacity: 1;\n    color: rgb(75 85 99 / var(--tw-text-opacity));\n  }\n}\n\n@media (min-width: 640px) {\n\n  .sm\\:col-span-4 {\n    grid-column: span 4 / span 4;\n  }\n\n  .sm\\:col-span-6 {\n    grid-column: span 6 / span 6;\n  }\n\n  .sm\\:col-start-1 {\n    grid-column-start: 1;\n  }\n\n  .sm\\:-mx-6 {\n    margin-left: -1.5rem;\n    margin-right: -1.5rem;\n  }\n\n  .sm\\:mx-auto {\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .sm\\:mt-0 {\n    margin-top: 0px;\n  }\n\n  .sm\\:block {\n    display: block;\n  }\n\n  .sm\\:w-full {\n    width: 100%;\n  }\n\n  .sm\\:max-w-md {\n    max-width: 28rem;\n  }\n\n  .sm\\:max-w-lg {\n    max-width: 32rem;\n  }\n\n  .sm\\:grid-cols-6 {\n    grid-template-columns: repeat(6, minmax(0, 1fr));\n  }\n\n  .sm\\:grid-cols-12 {\n    grid-template-columns: repeat(12, minmax(0, 1fr));\n  }\n\n  .sm\\:rounded-lg {\n    border-radius: 0.5rem;\n  }\n\n  .sm\\:px-6 {\n    padding-left: 1.5rem;\n    padding-right: 1.5rem;\n  }\n\n  .sm\\:px-10 {\n    padding-left: 2.5rem;\n    padding-right: 2.5rem;\n  }\n\n  .sm\\:pl-6 {\n    padding-left: 1.5rem;\n  }\n\n  .sm\\:text-sm {\n    font-size: 0.875rem;\n    line-height: 1.25rem;\n  }\n\n  .sm\\:text-xs {\n    font-size: 0.75rem;\n    line-height: 1rem;\n  }\n}\n\n@media (min-width: 768px) {\n\n  .md\\:col-span-2 {\n    grid-column: span 2 / span 2;\n  }\n}\n\n@media (min-width: 1024px) {\n\n  .lg\\:-mx-8 {\n    margin-left: -2rem;\n    margin-right: -2rem;\n  }\n\n  .lg\\:px-8 {\n    padding-left: 2rem;\n    padding-right: 2rem;\n  }\n}\n',"",{version:3,sources:["webpack://./frontend/src/globals.css","<no source>"],names:[],mappings:"AAAA;;CAAc,CAAd;;;CAAc;;AAAd;;;EAAA,sBAAc,EAAd,MAAc;EAAd,eAAc,EAAd,MAAc;EAAd,mBAAc,EAAd,MAAc;EAAd,qBAAc,EAAd,MAAc;AAAA;;AAAd;;EAAA,gBAAc;AAAA;;AAAd;;;;;;CAAc;;AAAd;EAAA,gBAAc,EAAd,MAAc;EAAd,8BAAc,EAAd,MAAc;EAAd,gBAAc,EAAd,MAAc;EAAd,cAAc;KAAd,WAAc,EAAd,MAAc;EAAd,wRAAc,EAAd,MAAc;EAAd,6BAAc,EAAd,MAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,SAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;AAAA;;AAAd;;;;CAAc;;AAAd;EAAA,SAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;EAAd,qBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,yCAAc;UAAd,iCAAc;AAAA;;AAAd;;CAAc;;AAAd;;;;;;EAAA,kBAAc;EAAd,oBAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,cAAc;EAAd,wBAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,mBAAc;AAAA;;AAAd;;;CAAc;;AAAd;;;;EAAA,+GAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,cAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,cAAc;EAAd,cAAc;EAAd,kBAAc;EAAd,wBAAc;AAAA;;AAAd;EAAA,eAAc;AAAA;;AAAd;EAAA,WAAc;AAAA;;AAAd;;;;CAAc;;AAAd;EAAA,cAAc,EAAd,MAAc;EAAd,qBAAc,EAAd,MAAc;EAAd,yBAAc,EAAd,MAAc;AAAA;;AAAd;;;;CAAc;;AAAd;;;;;EAAA,oBAAc,EAAd,MAAc;EAAd,eAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;EAAd,SAAc,EAAd,MAAc;EAAd,UAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,oBAAc;AAAA;;AAAd;;;CAAc;;AAAd;;;;EAAA,0BAAc,EAAd,MAAc;EAAd,6BAAc,EAAd,MAAc;EAAd,sBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,aAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,gBAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,wBAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,YAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,6BAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,wBAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,0BAAc,EAAd,MAAc;EAAd,aAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,kBAAc;AAAA;;AAAd;;CAAc;;AAAd;;;;;;;;;;;;;EAAA,SAAc;AAAA;;AAAd;EAAA,SAAc;EAAd,UAAc;AAAA;;AAAd;EAAA,UAAc;AAAA;;AAAd;;;EAAA,gBAAc;EAAd,SAAc;EAAd,UAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,gBAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,UAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;EAAA,UAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;;EAAA,UAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,eAAc;AAAA;;AAAd;;CAAc;AAAd;EAAA,eAAc;AAAA;;AAAd;;;;CAAc;;AAAd;;;;;;;;EAAA,cAAc,EAAd,MAAc;EAAd,sBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,eAAc;EAAd,YAAc;AAAA;;AAAd,wEAAc;AAAd;EAAA,aAAc;AAAA;;AAAd;EAAA,wBAAc;EAAd,wBAAc;EAAd,mBAAc;EAAd,mBAAc;EAAd,cAAc;EAAd,cAAc;EAAd,cAAc;EAAd,eAAc;EAAd,eAAc;EAAd,aAAc;EAAd,aAAc;EAAd,kBAAc;EAAd,sCAAc;EAAd,eAAc;EAAd,oBAAc;EAAd,sBAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,kBAAc;EAAd,2BAAc;EAAd,4BAAc;EAAd,sCAAc;EAAd,0CAAc;EAAd,mCAAc;EAAd,8BAAc;EAAd,sCAAc;EAAd,YAAc;EAAd,kBAAc;EAAd,gBAAc;EAAd,iBAAc;EAAd,kBAAc;EAAd,cAAc;EAAd,gBAAc;EAAd,aAAc;EAAd,mBAAc;EAAd,qBAAc;EAAd,2BAAc;EAAd,yBAAc;EAAd,0BAAc;EAAd,2BAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,yBAAc;EAAd;AAAc;;AAAd;EAAA,wBAAc;EAAd,wBAAc;EAAd,mBAAc;EAAd,mBAAc;EAAd,cAAc;EAAd,cAAc;EAAd,cAAc;EAAd,eAAc;EAAd,eAAc;EAAd,aAAc;EAAd,aAAc;EAAd,kBAAc;EAAd,sCAAc;EAAd,eAAc;EAAd,oBAAc;EAAd,sBAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,kBAAc;EAAd,2BAAc;EAAd,4BAAc;EAAd,sCAAc;EAAd,0CAAc;EAAd,mCAAc;EAAd,8BAAc;EAAd,sCAAc;EAAd,YAAc;EAAd,kBAAc;EAAd,gBAAc;EAAd,iBAAc;EAAd,kBAAc;EAAd,cAAc;EAAd,gBAAc;EAAd,aAAc;EAAd,mBAAc;EAAd,qBAAc;EAAd,2BAAc;EAAd,yBAAc;EAAd,0BAAc;EAAd,2BAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,yBAAc;EAAd;AAAc;AAEd;EAAA,kBAAmB;EAAnB,UAAmB;EAAnB,WAAmB;EAAnB,UAAmB;EAAnB,YAAmB;EAAnB,gBAAmB;EAAnB,sBAAmB;EAAnB,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,SAAmB;EAAnB;AAAmB;AAAnB;EAAA,QAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,gBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,gBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;;EAAA;IAAA;EAAmB;AAAA;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,wBAAmB;KAAnB,qBAAmB;UAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,qBAAmB;OAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,uBAAmB;OAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,oDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,sDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,+DAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,8DAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,oDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,4DAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,uDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,uDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,oDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,sDAAmB;EAAnB;AAAmB;AAAnB;EAAA,wBAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,gCAAmB;EAAnB;AAAmB;AAAnB;EAAA,iCAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,sBAAmB;KAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,gBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,eAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,2BAAmB;EAAnB;AAAmB;AAAnB;EAAA,2BAAmB;EAAnB;AAAmB;AAAnB;EAAA,2BAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,0CAAmB;EAAnB,uDAAmB;EAAnB,kEAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,0EAAmB;EAAnB,8FAAmB;EAAnB,kEAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,+EAAmB;EAAnB,mGAAmB;EAAnB,kEAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,2GAAmB;EAAnB,yGAAmB;EAAnB,kFAAmB;EAAnB,kFAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;;AAEnB;;EAEE,UAAU;EACV,SAAS;EACT;8BAC4B;AAC9B;;AAEA;EACE,cAAc;EACd,qBAAqB;AACvB;;AAEA;EACE,sBAAsB;AACxB;;AAnBA;EAAA,uBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,wCCAA;UDAA;CCAA;;ADAA;EAAA,uBCAA;EDAA;CCAA;;ADAA;EAAA,uBCAA;EDAA;CCAA;;ADAA;EAAA,+BCAA;EDAA;CCAA;;ADAA;EAAA,4GCAA;EDAA,0GCAA;EDAA,mFCAA;EDAA,mFCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;;EAAA;IAAA,qBCAA;IDAA;GCAA;CAAA;;ADAA;;EAAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA,qBCAA;IDAA;GCAA;;EDAA;IAAA,kBCAA;IDAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA,qBCAA;IDAA;GCAA;;EDAA;IAAA,qBCAA;IDAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA,oBCAA;IDAA;GCAA;;EDAA;IAAA,mBCAA;IDAA;GCAA;CAAA;;ADAA;;EAAA;IAAA;GCAA;CAAA;;ADAA;;EAAA;IAAA,mBCAA;IDAA;GCAA;;EDAA;IAAA,mBCAA;IDAA;GCAA;CAAA",sourcesContent:["@tailwind base;\n@tailwind components;\n@tailwind utilities;\n\nhtml,\nbody {\n  padding: 0;\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans,\n    Helvetica Neue, sans-serif;\n}\n\na {\n  color: inherit;\n  text-decoration: none;\n}\n\n* {\n  box-sizing: border-box;\n}\n",null],sourceRoot:""}]);const m=o},5514:(e,t,a)=>{var r=a(7294),n=a(3935),l=a(5614),o=a(3581),m=a(3768),s=a(6988),i=a(9776),c=a.n(i),d=a(3200),u=a(3727),A=a(6550),p=a(3253),g=a.n(p),b=a(5755),y=a(1642),x=a(319),f=a(1641),E=Object.defineProperty,h=Object.getOwnPropertySymbols,v=Object.prototype.hasOwnProperty,w=Object.prototype.propertyIsEnumerable,N=(e,t,a)=>t in e?E(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,B=(e,t)=>{for(var a in t||(t={}))v.call(t,a)&&N(e,a,t[a]);if(h)for(var a of h(t))w.call(t,a)&&N(e,a,t[a]);return e};const k={},C=y.Ps`
    fragment buildingFragment on Building {
  id
  name
  address
  rank
  rankText
  mcTypeId
  buildingTypeId
}
    `,I=y.Ps`
    fragment buildingNumberFragment on BuildingNumber {
  id
  buildingId
  name
  position
}
    `,D=y.Ps`
    fragment buildingTypeFragment on BuildingType {
  id
  name
}
    `,j=y.Ps`
    fragment commentFragment on Comment {
  id
  comment
  commentTypeId
  remark
}
    `,$=y.Ps`
    fragment commentItemCodeFragment on CommentItemCode {
  id
  commentId
  itemCodeId
}
    `,F=y.Ps`
    fragment commentTypeFragment on CommentType {
  id
  name
  position
}
    `,S=y.Ps`
    fragment companyFragment on Company {
  id
  isOrderEach
  isOrderMonthly
  name
  zipcode
  address
  tel
  fax
  companyTypeIds
}
    `,P=y.Ps`
    fragment companyTypeFragment on CompanyType {
  id
  name
  position
}
    `,T=y.Ps`
    fragment imageCommentFragment on ImageComment {
  id
  projectImageId
  code
  text
}
    `,O=y.Ps`
    fragment itemFragment on Item {
  id
  name
  shortName
  price
  orderPrice
  companyTypeIds
  companyId
  order
  unitId
}
    `,M=y.Ps`
    fragment itemCodeFragment on ItemCode {
  id
  code
  name
}
    `,U=y.Ps`
    fragment jkkUserFragment on JkkUser {
  id
  email
  firstName
  lastName
  active
}
    `,R=y.Ps`
    fragment mcTypeFragment on McType {
  id
  name
}
    `,V=y.Ps`
    fragment orderFragment on Order {
  id
  projectId
  companyId
  orderType
  isInvoice
  orderDate
  totalPrice
  exportCsvDate
}
    `,Y=y.Ps`
    fragment paginationFragment on Pagination {
  resultsCount
  totalCount
  totalPages
  currentPage
  nextPage
  previousPage
  hasNextPage
  hasPreviousPage
}
    `,q=y.Ps`
    fragment projectFragment on Project {
  id
  yearId
  buildingId
  buildingNumberId
  roomId
  userIds
  jkkUserId
  assesmentUserId
  constructionUserId
  totalItemPrice
  totalItemPriceRound
  tax
  totalPrice
  totalPriceInput
  name
  status1
  status2
  status3
  status4
  status5
  status6
  jkkStatus1
  jkkStatus2
  jkkStatus3
  jkkStatus4
  orderNumberType
  orderNumber
  assessmentDate
  assessmentUrl
  orderDate1
  orderMemo1
  orderDate2
  orderMemo2
  orderDate3
  orderMemo3
  electricStartDate1
  electricEndDate1
  waterStartDate1
  waterEndDate1
  electricStartDate2
  electricEndDate2
  waterStartDate2
  waterEndDate2
  electricStartDate3
  electricEndDate3
  waterStartDate3
  waterEndDate3
  closeTypes
  closeReason
  isVermiculite
  isFinishedVermiculite
  vermiculiteDate
  processCreatedDate
  beforeConstructionImageDate
  afterConstructionImageDate
  checklistFirstDate
  checklistLastDate
  address
  constructionStartDate
  constructionEndDate
  imageReportUrl
  isFaq
  isLackImage
  isCompleteImage
  submitDocumentDate
  availableDate
  livingYear
  livingMonth
  leaveDate
  isManagerAsbestosDocument1
  isManagerAsbestosDocument2
  isManagerAsbestosDocument3
  isOfficeworkAsbestosDocument1
  isOfficeworkAsbestosDocument2
  isOfficeworkAsbestosDocument3
  remark
  updatedItemsAt
  editUserId
}
    `,L=y.Ps`
    fragment projectImageFragment on ProjectImage {
  id
  projectId
  roomTypeId
  imageType
  imageBefore
  imageAfter
  image
  comment
  position
}
    `,_=y.Ps`
    fragment projectItemFragment on ProjectItem {
  id
  projectId
  itemId
  companyId
  code
  name
  count
  unitPrice
  amount
  roomTypeIds
  sequenceNumber
  updatedItemsAt
}
    `,z=y.Ps`
    fragment projectReportFragment on ProjectReport {
  id
  projectId
  worksheetBase
  worksheetItems
  worksheetImages
  worksheetChecklist
  spreadsheetId1
  spreadsheetId2
  spreadsheetId3
  spreadsheetId4
  spreadsheetId5
  spreadsheetId6
  spreadsheetId7
  spreadsheetDatetime1
  spreadsheetDatetime2
  spreadsheetDatetime3
  spreadsheetDatetime4
  spreadsheetDatetime5
  spreadsheetDatetime6
  spreadsheetDatetime7
  spreadsheetCheckReport1
  spreadsheetCheckReport2
}
    `,W=y.Ps`
    fragment purchaseOrderFragment on PurchaseOrder {
  id
  projectId
  fileUrl
}
    `,Z=y.Ps`
    fragment roomFragment on Room {
  id
  buildingNumberId
  name
  position
  roomModelTypeId
  roomLayoutId
}
    `,H=y.Ps`
    fragment roomLayoutFragment on RoomLayout {
  id
  name
  price1
  price2
  position
}
    `,G=y.Ps`
    fragment roomModelTypeFragment on RoomModelType {
  id
  name
  position
}
    `,K=y.Ps`
    fragment roomTypeFragment on RoomType {
  id
  name
  position
}
    `,J=y.Ps`
    fragment unitFragment on Unit {
  id
  name
}
    `,Q=y.Ps`
    fragment userFragment on User {
  id
  loginId
  email
  role
  firstName
  lastName
  tel
  active
  isAssesment
}
    `,X=y.Ps`
    fragment yearFragment on Year {
  id
  year
}
    `,ee=y.Ps`
    mutation signin($loginId: String!, $password: String!) {
  signin(input: {loginId: $loginId, password: $password}) {
    user {
      ...userFragment
    }
    error
  }
}
    ${Q}`,te=y.Ps`
    mutation signout {
  signout(input: {}) {
    result
  }
}
    `,ae=y.Ps`
    mutation createBuilding($attributes: BuildingAttributes!) {
  createBuilding(input: {attributes: $attributes}) {
    building {
      ...buildingFragment
    }
    error
  }
}
    ${C}`,re=y.Ps`
    mutation updateBuilding($id: ID!, $attributes: BuildingAttributes!) {
  updateBuilding(input: {id: $id, attributes: $attributes}) {
    building {
      ...buildingFragment
    }
    error
  }
}
    ${C}`,ne=y.Ps`
    mutation createBuildingNumber($id: ID!, $attributes: BuildingNumberAttributes!) {
  createBuildingNumber(input: {id: $id, attributes: $attributes}) {
    building {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
    error
  }
}
    ${C}
${I}`,le=y.Ps`
    mutation removeBuildingNumber($id: ID!) {
  removeBuildingNumber(input: {id: $id}) {
    building {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
    error
  }
}
    ${C}
${I}`,oe=y.Ps`
    mutation updateBuildingNumber($id: ID!, $attributes: BuildingNumberAttributes!) {
  updateBuildingNumber(input: {id: $id, attributes: $attributes}) {
    building {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
    error
  }
}
    ${C}
${I}`,me=y.Ps`
    mutation createComment($attributes: CommentAttributes!) {
  createComment(input: {attributes: $attributes}) {
    comment {
      ...commentFragment
    }
    error
  }
}
    ${j}`,se=y.Ps`
    mutation removeComment($id: ID!) {
  removeComment(input: {id: $id}) {
    comment {
      ...commentFragment
    }
    error
  }
}
    ${j}`,ie=y.Ps`
    mutation updateComment($id: ID!, $attributes: CommentAttributes!) {
  updateComment(input: {id: $id, attributes: $attributes}) {
    comment {
      ...commentFragment
    }
    error
  }
}
    ${j}`,ce=y.Ps`
    mutation createCommentTypeModel($attributes: CommentTypeAttributes!) {
  createCommentTypeModel(input: {attributes: $attributes}) {
    commentType {
      ...commentTypeFragment
    }
    error
  }
}
    ${F}`,de=y.Ps`
    mutation removeCommentTypeModel($id: ID!) {
  removeCommentTypeModel(input: {id: $id}) {
    commentType {
      ...commentTypeFragment
    }
    error
  }
}
    ${F}`,ue=y.Ps`
    mutation updateCommentTypeModel($id: ID!, $attributes: CommentTypeAttributes!) {
  updateCommentTypeModel(input: {id: $id, attributes: $attributes}) {
    commentType {
      ...commentTypeFragment
    }
    error
  }
}
    ${F}`,Ae=y.Ps`
    mutation createCompany($attributes: CompanyAttributes!) {
  createCompany(input: {attributes: $attributes}) {
    company {
      ...companyFragment
    }
    error
  }
}
    ${S}`,pe=y.Ps`
    mutation removeCompany($id: ID!) {
  removeCompany(input: {id: $id}) {
    result
    error
  }
}
    `,ge=y.Ps`
    mutation updateCompany($id: ID!, $attributes: CompanyAttributes!) {
  updateCompany(input: {id: $id, attributes: $attributes}) {
    company {
      ...companyFragment
      companyTypes {
        ...companyTypeFragment
      }
    }
    error
  }
}
    ${S}
${P}`,be=y.Ps`
    mutation createCompanyTypeModel($attributes: CompanyTypeAttributes!) {
  createCompanyTypeModel(input: {attributes: $attributes}) {
    companyType {
      ...companyTypeFragment
    }
    error
  }
}
    ${P}`,ye=y.Ps`
    mutation removeCompanyTypeModel($id: ID!) {
  removeCompanyTypeModel(input: {id: $id}) {
    companyType {
      ...companyTypeFragment
    }
    error
  }
}
    ${P}`,xe=y.Ps`
    mutation updateCompanyTypeModel($id: ID!, $attributes: CompanyTypeAttributes!) {
  updateCompanyTypeModel(input: {id: $id, attributes: $attributes}) {
    companyType {
      ...companyTypeFragment
    }
    error
  }
}
    ${P}`,fe=y.Ps`
    mutation updateCompanyTypeOrder($id: ID!, $attributes: CompanyTypeAttributes!) {
  updateCompanyTypeOrder(input: {id: $id, attributes: $attributes}) {
    companyTypes {
      ...companyTypeFragment
    }
    error
  }
}
    ${P}`,Ee=y.Ps`
    mutation copyItems {
  copyItems(input: {}) {
    error
  }
}
    `,he=y.Ps`
    mutation createItem($code: String!, $year: String!, $attributes: ItemAttributes!) {
  createItem(input: {code: $code, year: $year, attributes: $attributes}) {
    item {
      ...itemFragment
    }
    error
  }
}
    ${O}`,ve=y.Ps`
    mutation updateItem($id: ID!, $attributes: ItemAttributes!) {
  updateItem(input: {id: $id, attributes: $attributes}) {
    item {
      ...itemFragment
      company {
        ...companyFragment
      }
    }
    error
  }
}
    ${O}
${S}`,we=y.Ps`
    mutation createJkkUser($attributes: JkkUserAttributes!) {
  createJkkUser(input: {attributes: $attributes}) {
    jkkUser {
      ...jkkUserFragment
    }
    error
  }
}
    ${U}`,Ne=y.Ps`
    mutation updateJkkUser($id: ID!, $attributes: JkkUserAttributes!) {
  updateJkkUser(input: {id: $id, attributes: $attributes}) {
    jkkUser {
      ...jkkUserFragment
    }
    error
  }
}
    ${U}`,Be=y.Ps`
    mutation outputOrder($spreadsheetId: String!, $search: OrderSearch!) {
  outputOrder(input: {spreadsheetId: $spreadsheetId, search: $search}) {
    result
  }
}
    `,ke=y.Ps`
    mutation clearProjectUser($id: ID!) {
  clearProjectUser(input: {id: $id}) {
    project {
      ...projectFragment
      editUser {
        ...userFragment
      }
    }
  }
}
    ${q}
${Q}`,Ce=y.Ps`
    mutation createProject($attributes: ProjectAttributes!) {
  createProject(input: {attributes: $attributes}) {
    project {
      ...projectFragment
    }
    error
  }
}
    ${q}`,Ie=y.Ps`
    mutation createProjectImageReport($id: ID!) {
  createProjectImageReport(input: {id: $id}) {
    project {
      ...projectFragment
    }
    error
  }
}
    ${q}`,De=y.Ps`
    mutation createProjectItem($id: ID!, $attributes: ProjectItemAttributes!) {
  createProjectItem(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      updatedItemsUser {
        ...userFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
        roomTypes {
          ...roomTypeFragment
        }
        updatedItemsUser {
          ...userFragment
        }
      }
    }
    error
  }
}
    ${q}
${Q}
${_}
${O}
${K}`,je=y.Ps`
    mutation createSpreadsheet($id: ID!, $reportType: String!, $attributes: ProjectReportAttributes!) {
  createSpreadsheet(
    input: {id: $id, reportType: $reportType, attributes: $attributes}
  ) {
    projectReport {
      ...projectReportFragment
    }
    error
  }
}
    ${z}`,$e=y.Ps`
    mutation editProjectUser($id: ID!) {
  editProjectUser(input: {id: $id}) {
    project {
      ...projectFragment
      editUser {
        ...userFragment
      }
    }
  }
}
    ${q}
${Q}`,Fe=y.Ps`
    mutation importItem($id: ID!, $file: File!) {
  importItem(input: {id: $id, file: $file}) {
    project {
      ...projectFragment
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
      }
    }
    error
  }
}
    ${q}
${_}
${O}`,Se=y.Ps`
    mutation removeProject($id: ID!) {
  removeProject(input: {id: $id}) {
    result
    error
  }
}
    `,Pe=y.Ps`
    mutation removeProjectItem($id: ID!) {
  removeProjectItem(input: {id: $id}) {
    project {
      ...projectFragment
    }
  }
}
    ${q}`,Te=y.Ps`
    mutation updateProject($id: ID!, $attributes: ProjectAttributes!) {
  updateProject(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
    }
    error
  }
}
    ${q}`;function Oe(e){const t=B(B({},k),e);return x.D(Te,t)}const Me=y.Ps`
    mutation updateProjectItem($id: ID!, $attributes: ProjectItemAttributes!) {
  updateProjectItem(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      updatedItemsUser {
        ...userFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
        updatedItemsUser {
          ...userFragment
        }
      }
      purchaseOrders {
        ...purchaseOrderFragment
      }
    }
    error
  }
}
    ${q}
${Q}
${_}
${O}
${W}`,Ue=y.Ps`
    mutation updateProjectItemsCompany($id: ID!, $companyTypeId: ID!, $companyId: ID!) {
  updateProjectItemsCompany(
    input: {id: $id, companyTypeId: $companyTypeId, companyId: $companyId}
  ) {
    project {
      ...projectFragment
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
      }
    }
    error
  }
}
    ${q}
${_}
${O}`,Re=y.Ps`
    mutation updateProjectOrder($id: ID!, $attributes: OrderAttributes!) {
  updateProjectOrder(input: {id: $id, attributes: $attributes}) {
    order {
      ...orderFragment
    }
  }
}
    ${V}`;y.Ps`
    mutation updateProjectOrderCsv($id: ID!) {
  updateProjectOrderCsv(input: {id: $id}) {
    order {
      ...orderFragment
    }
  }
}
    ${V}`;const Ve=y.Ps`
    mutation updateProjectReport($id: ID!, $attributes: ProjectReportAttributes!) {
  updateProjectReport(input: {id: $id, attributes: $attributes}) {
    projectReport {
      ...projectReportFragment
    }
    error
  }
}
    ${z}`,Ye=y.Ps`
    mutation updateProjectYear($id: ID!, $yearId: ID!) {
  updateProjectYear(input: {id: $id, yearId: $yearId}) {
    project {
      ...projectFragment
      year {
        ...yearFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
      }
      purchaseOrders {
        ...purchaseOrderFragment
      }
    }
    error
  }
}
    ${q}
${X}
${_}
${O}
${W}`,qe=y.Ps`
    mutation addComment($id: ID!, $commentId: ID!) {
  addComment(input: {id: $id, commentId: $commentId}) {
    projectImage {
      ...projectImageFragment
      comments {
        ...commentFragment
      }
    }
  }
}
    ${L}
${j}`,Le=y.Ps`
    mutation addImageComment($id: ID!, $itemId: ID!) {
  addImageComment(input: {id: $id, itemId: $itemId}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${L}
${T}`;y.Ps`
    mutation addImageCommentAll($id: ID!, $commentId: ID!) {
  addImageCommentAll(input: {id: $id, commentId: $commentId}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${L}
${T}`;const _e=y.Ps`
    mutation createProjectImage($count: Int!, $attributes: ProjectImageAttributes!) {
  createProjectImage(input: {count: $count, attributes: $attributes}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${q}
${L}`;function ze(e){const t=B(B({},k),e);return x.D(_e,t)}const We=y.Ps`
    mutation removeProjectImageComment($id: ID!, $commentId: ID!) {
  removeProjectImageComment(input: {id: $id, commentId: $commentId}) {
    projectImage {
      ...projectImageFragment
      comments {
        ...commentFragment
      }
    }
  }
}
    ${L}
${j}`,Ze=y.Ps`
    mutation removeImageComment($id: ID!) {
  removeImageComment(input: {id: $id}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${L}
${T}`;y.Ps`
    mutation removeImageCommentAll($id: ID!) {
  removeImageCommentAll(input: {id: $id}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${L}
${T}`;const He=y.Ps`
    mutation removeProjectImage($id: ID!) {
  removeProjectImage(input: {id: $id}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${q}
${L}`;function Ge(e){const t=B(B({},k),e);return x.D(He,t)}const Ke=y.Ps`
    mutation updateImageComment($id: ID!, $attributes: ImageCommentAttributes!) {
  updateImageComment(input: {id: $id, attributes: $attributes}) {
    imageComment {
      ...imageCommentFragment
    }
  }
}
    ${T}`,Je=y.Ps`
    mutation updateProjectImage($id: ID!, $attributes: ProjectImageAttributes!) {
  updateProjectImage(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${q}
${L}`;function Qe(e){const t=B(B({},k),e);return x.D(Je,t)}const Xe=y.Ps`
    mutation updateProjectImageOrder($id: ID!, $attributes: ProjectImageAttributes!) {
  updateProjectImageOrder(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${q}
${L}`,et=y.Ps`
    mutation copyRoom($id: ID!, $attributes: RoomAttributes!) {
  copyRoom(input: {id: $id, attributes: $attributes}) {
    buildingNumber {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
      }
    }
    error
  }
}
    ${I}
${Z}`;const tt=y.Ps`
    mutation createRoom($buildingNumberId: ID!, $attributes: RoomAttributes!) {
  createRoom(
    input: {buildingNumberId: $buildingNumberId, attributes: $attributes}
  ) {
    buildingNumber {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
      }
    }
    error
  }
}
    ${I}
${Z}`,at=y.Ps`
    mutation removeRoom($id: ID!) {
  removeRoom(input: {id: $id}) {
    buildingNumber {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
        roomTypes {
          ...roomTypeFragment
        }
      }
    }
    error
  }
}
    ${I}
${Z}
${K}`,rt=y.Ps`
    mutation updateRoom($id: ID!, $attributes: RoomAttributes!, $roomTypeIds: [ID!]!) {
  updateRoom(input: {id: $id, attributes: $attributes, roomTypeIds: $roomTypeIds}) {
    room {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
    error
  }
}
    ${Z}
${K}`,nt=y.Ps`
    mutation createRoomLayout($attributes: RoomLayoutAttributes!) {
  createRoomLayout(input: {attributes: $attributes}) {
    roomLayout {
      ...roomLayoutFragment
    }
    error
  }
}
    ${H}`,lt=y.Ps`
    mutation updateRoomLayout($id: ID!, $attributes: RoomLayoutAttributes!) {
  updateRoomLayout(input: {id: $id, attributes: $attributes}) {
    roomLayout {
      ...roomLayoutFragment
    }
    error
  }
}
    ${H}`,ot=y.Ps`
    mutation updateRoomLayoutOrder($id: ID!, $attributes: RoomLayoutAttributes!) {
  updateRoomLayoutOrder(input: {id: $id, attributes: $attributes}) {
    roomLayouts {
      ...roomLayoutFragment
    }
    error
  }
}
    ${H}`,mt=y.Ps`
    mutation createRoomTypeModel($attributes: RoomTypeAttributes!) {
  createRoomTypeModel(input: {attributes: $attributes}) {
    roomType {
      ...roomTypeFragment
    }
    error
  }
}
    ${K}`,st=y.Ps`
    mutation updateRoomTypeModel($id: ID!, $attributes: RoomTypeAttributes!) {
  updateRoomTypeModel(input: {id: $id, attributes: $attributes}) {
    roomType {
      ...roomTypeFragment
    }
    error
  }
}
    ${K}`;function it(e){const t=B(B({},k),e);return x.D(st,t)}const ct=y.Ps`
    mutation updateRoomTypeOrder($id: ID!, $attributes: RoomTypeAttributes!) {
  updateRoomTypeOrder(input: {id: $id, attributes: $attributes}) {
    roomTypes {
      ...roomTypeFragment
    }
    error
  }
}
    ${K}`,dt=y.Ps`
    mutation createUser($attributes: UserAttributes!) {
  createUser(input: {attributes: $attributes}) {
    user {
      ...userFragment
    }
    error
  }
}
    ${Q}`,ut=y.Ps`
    mutation updateUser($id: ID!, $attributes: UserAttributes!) {
  updateUser(input: {id: $id, attributes: $attributes}) {
    user {
      ...userFragment
    }
    error
  }
}
    ${Q}`;function At(e){const t=B(B({},k),e);return x.D(ut,t)}const pt=y.Ps`
    query allBuildings {
  allBuildings {
    ...buildingFragment
    buildingNumbers {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
      }
    }
  }
}
    ${C}
${I}
${Z}`,gt=y.Ps`
    query building($id: ID!) {
  building(id: $id) {
    ...buildingFragment
    buildingNumbers {
      ...buildingNumberFragment
    }
  }
}
    ${C}
${I}`;function bt(e){const t=B(B({},k),e);return f.a(gt,t)}const yt=y.Ps`
    query buildingNumber($id: ID!) {
  buildingNumber(id: $id) {
    ...buildingNumberFragment
    rooms {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
  }
}
    ${I}
${Z}
${K}`,xt=y.Ps`
    query buildings($page: Int, $perPage: Int, $search: BuildingSearch) {
  buildings(page: $page, perPage: $perPage, search: $search) {
    buildings {
      ...buildingFragment
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${C}
${Y}`,ft=y.Ps`
    query room($id: ID!) {
  room(id: $id) {
    ...roomFragment
    roomTypes {
      ...roomTypeFragment
    }
  }
}
    ${Z}
${K}`,Et=y.Ps`
    query buildingTypes {
  buildingTypes {
    ...buildingTypeFragment
  }
}
    ${D}`,ht=y.Ps`
    query comment($id: ID!) {
  comment(id: $id) {
    ...commentFragment
    commentItemCodes {
      ...commentItemCodeFragment
      itemCode {
        ...itemCodeFragment
      }
    }
    commentType {
      ...commentTypeFragment
    }
  }
}
    ${j}
${$}
${M}
${F}`,vt=y.Ps`
    query comments($search: CommentSearch) {
  comments(search: $search) {
    ...commentFragment
    commentType {
      ...commentTypeFragment
    }
  }
}
    ${j}
${F}`;function wt(e){const t=B(B({},k),e);return f.a(vt,t)}const Nt=y.Ps`
    query commentType($id: ID!) {
  commentType(id: $id) {
    ...commentTypeFragment
  }
}
    ${F}`,Bt=y.Ps`
    query commentTypes {
  commentTypes {
    ...commentTypeFragment
  }
}
    ${F}`;function kt(e){const t=B(B({},k),e);return f.a(Bt,t)}const Ct=y.Ps`
    query companies {
  companies {
    ...companyFragment
    companyTypes {
      ...companyTypeFragment
    }
  }
}
    ${S}
${P}`;function It(e){const t=B(B({},k),e);return f.a(Ct,t)}const Dt=y.Ps`
    query company($id: ID!) {
  company(id: $id) {
    ...companyFragment
  }
}
    ${S}`,jt=y.Ps`
    query companyType($id: ID!) {
  companyType(id: $id) {
    ...companyTypeFragment
  }
}
    ${P}`,$t=y.Ps`
    query companyTypes {
  companyTypes {
    ...companyTypeFragment
    companies {
      ...companyFragment
    }
  }
}
    ${P}
${S}`;function Ft(e){const t=B(B({},k),e);return f.a($t,t)}const St=y.Ps`
    query allItems($search: ItemSearch) {
  allItems(search: $search) {
    ...itemFragment
    itemCode {
      ...itemCodeFragment
    }
  }
}
    ${O}
${M}`;function Pt(e){const t=B(B({},k),e);return f.a(St,t)}const Tt=y.Ps`
    query item($id: ID!) {
  item(id: $id) {
    ...itemFragment
    itemCode {
      ...itemCodeFragment
    }
    year {
      ...yearFragment
    }
    unit {
      ...unitFragment
    }
  }
}
    ${O}
${M}
${X}
${J}`,Ot=y.Ps`
    query items($page: Int, $perPage: Int, $search: ItemSearch) {
  items(page: $page, perPage: $perPage, search: $search) {
    items {
      ...itemFragment
      itemCode {
        ...itemCodeFragment
      }
      year {
        ...yearFragment
      }
      company {
        ...companyFragment
      }
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${O}
${M}
${X}
${S}
${Y}`,Mt=y.Ps`
    query jkkUser($id: ID!) {
  jkkUser(id: $id) {
    ...jkkUserFragment
  }
}
    ${U}`,Ut=y.Ps`
    query jkkUsers($search: JkkUserSearch) {
  jkkUsers(search: $search) {
    ...jkkUserFragment
  }
}
    ${U}`;function Rt(e){const t=B(B({},k),e);return f.a(Ut,t)}const Vt=y.Ps`
    query mcTypes {
  mcTypes {
    ...mcTypeFragment
  }
}
    ${R}`,Yt=y.Ps`
    query orders($page: Int, $perPage: Int, $search: OrderSearch) {
  orders(page: $page, perPage: $perPage, search: $search) {
    orders {
      ...orderFragment
      company {
        ...companyFragment
      }
      project {
        ...projectFragment
      }
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${V}
${S}
${q}
${Y}`,qt=y.Ps`
    query project($id: ID!) {
  project(id: $id) {
    ...projectFragment
    year {
      ...yearFragment
    }
    projectItems {
      ...projectItemFragment
      item {
        ...itemFragment
      }
      roomTypes {
        ...roomTypeFragment
      }
    }
    purchaseOrders {
      ...purchaseOrderFragment
    }
    building {
      ...buildingFragment
      mcType {
        ...mcTypeFragment
      }
      buildingType {
        ...buildingTypeFragment
      }
    }
    buildingNumber {
      ...buildingNumberFragment
    }
    room {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
    projectImages {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
      comments {
        ...commentFragment
      }
    }
    projectReport {
      ...projectReportFragment
    }
    updatedItemsUser {
      ...userFragment
    }
    editUser {
      ...userFragment
    }
  }
}
    ${q}
${X}
${_}
${O}
${K}
${W}
${C}
${R}
${D}
${I}
${Z}
${L}
${T}
${j}
${z}
${Q}`;function Lt(e){const t=B(B({},k),e);return f.a(qt,t)}const _t=y.Ps`
    query projectItems($id: ID!, $orderItem: String, $order: String) {
  projectItems(id: $id, orderItem: $orderItem, order: $order) {
    ...projectItemFragment
    item {
      ...itemFragment
    }
    roomTypes {
      ...roomTypeFragment
    }
    company {
      ...companyFragment
      companyTypes {
        ...companyTypeFragment
      }
    }
    selectableCompanies {
      ...companyFragment
    }
    updatedItemsUser {
      ...userFragment
    }
  }
}
    ${_}
${O}
${K}
${S}
${P}
${Q}`,zt=y.Ps`
    query projectOrders($id: ID!) {
  project(id: $id) {
    ...projectFragment
    ordersEach {
      ...orderFragment
      company {
        ...companyFragment
      }
    }
    ordersMonthly {
      ...orderFragment
      company {
        ...companyFragment
      }
    }
  }
}
    ${q}
${V}
${S}`;function Wt(e){const t=B(B({},k),e);return f.a(zt,t)}const Zt=y.Ps`
    query projects($page: Int, $perPage: Int, $search: ProjectSearch) {
  projects(page: $page, perPage: $perPage, search: $search) {
    projects {
      ...projectFragment
      building {
        ...buildingFragment
      }
      buildingNumber {
        ...buildingNumberFragment
      }
      room {
        ...roomFragment
      }
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${q}
${C}
${I}
${Z}
${Y}`;function Ht(e){const t=B(B({},k),e);return f.a(Zt,t)}const Gt=y.Ps`
    query roomLayout($id: ID!) {
  roomLayout(id: $id) {
    ...roomLayoutFragment
  }
}
    ${H}`,Kt=y.Ps`
    query roomLayouts {
  roomLayouts {
    ...roomLayoutFragment
  }
}
    ${H}`;function Jt(e){const t=B(B({},k),e);return f.a(Kt,t)}const Qt=y.Ps`
    query roomModelTypes {
  roomModelTypes {
    ...roomModelTypeFragment
  }
}
    ${G}`,Xt=y.Ps`
    query roomType($id: ID!) {
  roomType(id: $id) {
    ...roomTypeFragment
  }
}
    ${K}`;function ea(e){const t=B(B({},k),e);return f.a(Xt,t)}const ta=y.Ps`
    query roomTypes {
  roomTypes {
    ...roomTypeFragment
  }
}
    ${K}`;function aa(e){const t=B(B({},k),e);return f.a(ta,t)}const ra=y.Ps`
    query units {
  units {
    ...unitFragment
  }
}
    ${J}`;function na(e){const t=B(B({},k),e);return f.a(ra,t)}const la=y.Ps`
    query currentUser {
  currentUser {
    ...userFragment
  }
}
    ${Q}`;function oa(e){const t=B(B({},k),e);return f.a(la,t)}const ma=y.Ps`
    query user($id: ID!) {
  user(id: $id) {
    ...userFragment
  }
}
    ${Q}`;function sa(e){const t=B(B({},k),e);return f.a(ma,t)}const ia=y.Ps`
    query users($search: UserSearch) {
  users(search: $search) {
    ...userFragment
  }
}
    ${Q}`;function ca(e){const t=B(B({},k),e);return f.a(ia,t)}const da=y.Ps`
    query years {
  years {
    ...yearFragment
  }
}
    ${X}`;function ua(e){const t=B(B({},k),e);return f.a(da,t)}var Aa=a(6252);const pa=({children:e})=>{console.log("layout");const t=(0,Aa.x)(),a=(0,A.k6)(),{data:{currentUser:n}={}}=oa(),[l]=function(e){const t=B(B({},k),void 0);return x.D(te,t)}();return(0,r.useEffect)((()=>{null===n&&a.push("/login")}),[n]),void 0===n?null:r.createElement("div",{className:"flex h-screen overflow-hidden bg-gray-100"},r.createElement("div",{className:"flex flex-col pt-5 pb-4 bg-white w-[200px]"},r.createElement("div",{className:"flex items-center flex-shrink-0 px-4"},"案件管理システム"),r.createElement("div",{className:"flex-1 h-0 mt-5 overflow-y-auto"},r.createElement("nav",{className:"px-4"},r.createElement(u.rU,{to:"/projects"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/projects")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"案件管理")),r.createElement(u.rU,{to:"/orders"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/orders")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"発注管理")),r.createElement("h3",{className:"block px-3 mt-10 text-xs font-semibold tracking-wider text-gray-500 uppercase",id:"projects-headline"},"マスター管理"),r.createElement(u.rU,{to:"/buildings"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/buildings")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"物件")),r.createElement(u.rU,{to:"/items"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/items")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"項目")),r.createElement(u.rU,{to:"/comments"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/comments")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"コメント")),r.createElement(u.rU,{to:"/commenttypes"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/commenttypes")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"コメント種別")),r.createElement(u.rU,{to:"/companies"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/companies")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"業者")),r.createElement(u.rU,{to:"/companytypes"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/companytypes")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"業者種別")),r.createElement(u.rU,{to:"/roomtypes"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/roomtypes")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"部屋種別")),r.createElement(u.rU,{to:"/roomlayouts"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/roomlayouts")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"部屋レイアウト")),r.createElement(u.rU,{to:"/users"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/users")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"従業員")),r.createElement(u.rU,{to:"/jkk_users"},r.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/jkk_users")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"公社担当者")),r.createElement("a",{href:"#",className:"text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md",onClick:()=>l().then((()=>{t.resetStore(),a.push("/login")}))},"ログアウト")))),r.createElement("div",{className:"flex-1 px-4 pb-4 overflow-y-auto col-span-full"},e))},ga=({className:e,type:t,disabled:a,loading:n,children:l,onClick:o})=>r.createElement("button",{type:t,className:`${e} disabled:opacity-40`,disabled:a,onClick:o},n&&r.createElement("svg",{className:"w-5 h-5 mr-3 -ml-1 text-white animate-spin",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24"},r.createElement("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"}),r.createElement("path",{className:"opacity-75",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"})),!n&&l),ba=({className:e})=>r.createElement("div",{className:"flex justify-center"},r.createElement("svg",{role:"status",className:`w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600 ${e}`,viewBox:"0 0 100 101",fill:"none",xmlns:"http://www.w3.org/2000/svg"},r.createElement("path",{d:"M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z",fill:"currentColor"}),r.createElement("path",{d:"M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z",fill:"currentFill"}))),ya="...",xa=(e,t)=>{let a=t-e+1;return Array.from({length:a},((t,a)=>a+e))},fa=({totalCount:e,siblingCount:t=1,currentPage:a,pageSize:n,totalPages:l})=>{const[o,m]=(0,b.Wd)("page",b.yz),s=(({totalCount:e,pageSize:t,siblingCount:a=1,currentPage:n})=>(0,r.useMemo)((()=>{const r=Math.ceil(e/t);if(a+5>=r)return xa(1,r);const l=Math.max(n-a,1),o=Math.min(n+a,r),m=l>2,s=o<r-2,i=r;if(!m&&s)return[...xa(1,3+2*a),ya,r];if(m&&!s){let e=xa(r-(3+2*a)+1,r);return[1,ya,...e]}if(m&&s){let e=xa(l,o);return[1,ya,...e,ya,i]}}),[e,t,a,n]))({currentPage:a,totalCount:e,siblingCount:t,pageSize:n});return 0===a||!s||(null==s?void 0:s.length)<2?null:r.createElement(r.Fragment,null,r.createElement("div",null,r.createElement("nav",{className:"relative z-0 inline-flex -space-x-px rounded-md shadow-sm","aria-label":"Pagination"},a>1&&r.createElement("a",{className:"relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-pointer rounded-l-md hover:bg-gray-50",onClick:()=>{console.log("🚀 ~ file: index.tsx ~ line 54 ~ onPrevious ~ onPrevious",a),m(a-1)}},r.createElement("span",{className:"sr-only"},"Previous"),r.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},r.createElement("path",{fillRule:"evenodd",d:"M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z",clipRule:"evenodd"}))),1===a&&r.createElement("span",{className:"relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 bg-gray-100 border border-gray-300 rounded-l-md hover:bg-gray-50"},r.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},r.createElement("path",{fillRule:"evenodd",d:"M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z",clipRule:"evenodd"}))),s.map((e=>e===ya?r.createElement("span",{className:"relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300",key:`pageNumber-${e}`},"..."):r.createElement("a",{key:e,className:`bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium cursor-pointer\n\t\t\t\t\t\t\t\t${e===a?"border-indigo-500 text-indigo-600 z-10 bg-indigo-50":""}\n\t\t\t\t\t\t\t\t`,onClick:()=>m(e)},e))),a<l&&r.createElement("a",{className:"relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 cursor-pointer\n\t\t\t\t\t\t",onClick:()=>{console.log("🚀 ~ file: index.tsx ~ line 49 ~ onNext ~ onNext",a),m(a+1)}},r.createElement("span",{className:"sr-only"},"Next"),r.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},r.createElement("path",{fillRule:"evenodd",d:"M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z",clipRule:"evenodd"}))),a===l&&r.createElement("span",{className:"relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 bg-gray-100 border border-gray-300 rounded-l-md hover:bg-gray-50"},r.createElement("span",{className:"sr-only"},"Next"),r.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},r.createElement("path",{fillRule:"evenodd",d:"M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z",clipRule:"evenodd"}))))))};var Ea=a(4649),ha=a(7536),va=Object.defineProperty,wa=Object.getOwnPropertySymbols,Na=Object.prototype.hasOwnProperty,Ba=Object.prototype.propertyIsEnumerable,ka=(e,t,a)=>t in e?va(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Ca={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Ia=({isOpen:e,onClose:t})=>{var a;const n=(0,A.k6)(),[l,{loading:o,data:m}]=function(e){const t=B(B({},k),e);return x.D(ae,t)}({onCompleted:e=>{e.createBuilding.error||(b(),n.push(`/buildings/${e.createBuilding.building.id}/base`))}}),{register:s,handleSubmit:i,reset:c,formState:{isDirty:d,isValid:u,errors:p}}=(0,ha.cI)({defaultValues:{name:""}}),b=()=>{c(),t()};return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:e,onRequestClose:b,style:Ca},r.createElement("h1",{className:"text-xl"},"物件新規作成"),(null==m?void 0:m.createBuilding.error)&&r.createElement("p",{className:"text-red-500"},m.createBuilding.error),r.createElement("form",{className:"p-6 space-y-6",onSubmit:i((e=>{l({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))Na.call(t,a)&&ka(e,a,t[a]);if(wa)for(var a of wa(t))Ba.call(t,a)&&ka(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("name",{required:!0}))),"required"===(null==(a=p.name)?void 0:a.type)&&r.createElement("p",{className:"text-red-400"},"名称は必須項目です")),r.createElement("div",{className:"py-3 text-right flex items-center space-x-4"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm",onClick:()=>b()},"キャンセル"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存")))))},Da=()=>{const[e,t]=(0,b.Kx)({page:b.yz,keyword:b.Zp,active:b.dJ}),[a,n]=(0,r.useState)(!1),{data:{buildings:{buildings:l=[],pagination:o={}}={}}={}}=function(e){const t=B(B({},k),e);return f.a(xt,t)}({variables:{page:e.page,search:{keyword:e.keyword}},fetchPolicy:"cache-and-network"}),m=(0,Ea.TA)({initialValues:{keyword:e.keyword,active:!1},onSubmit:e=>{t({keyword:e.keyword,active:e.active,page:1})}});return r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"物件管理"),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>n(!0)},"新規作成"),r.createElement(Ia,{isOpen:a,onClose:()=>n(!1)}),r.createElement("form",{className:"px-10 py-2 mb-4 bg-white",onSubmit:m.handleSubmit},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"sm:col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"キーワード"),r.createElement("div",{className:"mt-1"},r.createElement("input",{name:"keyword",type:"text",className:"block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:m.values.keyword,onChange:m.handleChange,onBlur:()=>m.handleSubmit()}))))),r.createElement(fa,{totalCount:o.totalCount,currentPage:o.currentPage,totalPages:o.totalPages,pageSize:20}),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"w-12 px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"ID"),r.createElement("th",{scope:"col",className:"w-72 px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"住所"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"ランク"),r.createElement("th",null))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},l.map((e=>r.createElement("tr",{className:"bg-white",key:e.id},r.createElement("td",{className:"ml-10"},r.createElement("div",{className:"px-2 cursor-pointer"},e.id)),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/buildings/${e.id}/base${location.search}`},e.name)),r.createElement("td",{className:"ml-10"},r.createElement("div",{className:"px-2"},e.address)),r.createElement("td",{className:"ml-10"},r.createElement("div",{className:"px-2"},e.rankText))))))))))};var ja;function $a(){return $a=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},$a.apply(this,arguments)}const Fa=function(e){return r.createElement("svg",$a({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Check_svg__w-6 Check_svg__h-6"},e),ja||(ja=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0z"})))};var Sa;function Pa(){return Pa=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Pa.apply(this,arguments)}const Ta=function(e){return r.createElement("svg",Pa({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"ChevronRightIcon_svg__w-6 ChevronRightIcon_svg__h-6"},e),Sa||(Sa=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"m8.25 4.5 7.5 7.5-7.5 7.5"})))};var Oa;function Ma(){return Ma=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Ma.apply(this,arguments)}const Ua=function(e){return r.createElement("svg",Ma({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"DocumentArrowDownload_svg__w-6 DocumentArrowDownload_svg__h-6"},e),Oa||(Oa=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m.75 12 3 3m0 0 3-3m-3 3v-6m-1.5-9H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9z"})))};var Ra;function Va(){return Va=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Va.apply(this,arguments)}const Ya=function(e){return r.createElement("svg",Va({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"EllipseVertical_svg__w-6 EllipseVertical_svg__h-6"},e),Ra||(Ra=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M12 6.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5zm0 6a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5zm0 6a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5z"})))};var qa;function La(){return La=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},La.apply(this,arguments)}const _a=function(e){return r.createElement("svg",La({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Minus_svg__w-6 Minus_svg__h-6"},e),qa||(qa=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M19.5 12h-15"})))};var za;function Wa(){return Wa=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Wa.apply(this,arguments)}const Za=function(e){return r.createElement("svg",Wa({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Plus_svg__w-6 Plus_svg__h-6"},e),za||(za=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M12 4.5v15m7.5-7.5h-15"})))};var Ha;function Ga(){return Ga=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Ga.apply(this,arguments)}const Ka=function(e){return r.createElement("svg",Ga({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Trash_svg__w-6 Trash_svg__h-6"},e),Ha||(Ha=r.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"})))},Ja=({building:e})=>{const{id:t}=(0,A.UO)();return r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/buildings",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"物件管理"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},e.name)))))},Qa=()=>{const{id:e}=(0,A.UO)(),t=e=>location.pathname.indexOf(e)>-1;return r.createElement("div",{className:"hidden sm:block"},r.createElement("div",{className:"border-b border-gray-200"},r.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},r.createElement(u.rU,{to:`/buildings/${e}/base${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("base")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報")),r.createElement(u.rU,{to:`/buildings/${e}/building_numbers${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("building_numbers")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"号棟")))))};var Xa=Object.defineProperty,er=Object.defineProperties,tr=Object.getOwnPropertyDescriptors,ar=Object.getOwnPropertySymbols,rr=Object.prototype.hasOwnProperty,nr=Object.prototype.propertyIsEnumerable,lr=(e,t,a)=>t in e?Xa(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,or=(e,t)=>{for(var a in t||(t={}))rr.call(t,a)&&lr(e,a,t[a]);if(ar)for(var a of ar(t))nr.call(t,a)&&lr(e,a,t[a]);return e},mr=(e,t)=>er(e,tr(t));const sr=e=>{const{id:t}=(0,A.UO)(),[a,{loading:n,data:l}]=function(e){const t=B(B({},k),void 0);return x.D(re,t)}(),{register:o,handleSubmit:m,formState:{isDirty:s,isValid:i,errors:c}}=(0,ha.cI)({defaultValues:{name:e.building.name,buildingTypeId:e.building.buildingTypeId,mcTypeId:e.building.mcTypeId,address:e.building.address,rank:e.building.rank}});return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},e.building&&r.createElement(Ja,{building:e.building}),r.createElement(Qa,null),(null==l?void 0:l.updateBuilding.error)&&r.createElement("p",{className:"text-red-500"},l.updateBuilding.error),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:m((e=>{a({variables:{id:t,attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",or({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("name",{required:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"住所"),r.createElement("input",or({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("address")))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"建物区分"),r.createElement("select",mr(or({},o("buildingTypeId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),r.createElement("option",{value:""}),e.buildingTypes.map((e=>r.createElement("option",{value:e.id,key:`buildingType-${e.id}`},e.name))))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"管轄MC"),r.createElement("select",mr(or({},o("mcTypeId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),r.createElement("option",{value:""}),e.mcTypes.map((e=>r.createElement("option",{value:e.id,key:`mcType-${e.id}`},e.name))))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"ランク"),r.createElement("select",mr(or({},o("rank")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),r.createElement("option",{value:""}),r.createElement("option",{value:"a"},"A"),r.createElement("option",{value:"b"},"B"),r.createElement("option",{value:"c"},"C"),r.createElement("option",{value:"d"},"D"))),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!s||!i,loading:n},"保存")))))))},ir=()=>{const{id:e}=(0,A.UO)(),{data:{building:t=null}={}}=bt({variables:{id:e}}),{data:{buildingTypes:a=[]}={}}=function(e){const t=B(B({},k),void 0);return f.a(Et,t)}(),{data:{mcTypes:n=[]}={}}=function(e){const t=B(B({},k),void 0);return f.a(Vt,t)}();return t&&a&&n?r.createElement(sr,{building:t,buildingTypes:a,mcTypes:n}):null},cr={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},dr=({buildingNumber:e,isOpen:t,handleClose:a})=>{const{id:n}=(0,A.UO)(),[l,{loading:o}]=function(e){const t=B(B({},k),void 0);return x.D(ne,t)}(),[m]=function(e){const t=B(B({},k),void 0);return x.D(oe,t)}(),s=(0,Ea.TA)({enableReinitialize:!0,initialValues:{name:(null==e?void 0:e.name)||""},onSubmit:t=>{e?m({variables:{id:e.id,attributes:t}}):l({variables:{id:n,attributes:t}}),a(),s.resetForm()}});return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:t,onRequestClose:a,style:cr},r.createElement("h1",{className:"text-xl"},"号棟"),r.createElement("form",{className:"p-6 space-y-6",onSubmit:s.handleSubmit},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:s.values.name,onChange:s.handleChange,onBlur:s.handleBlur})),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:o},"保存")))))};var ur=a(5622),Ar=Object.defineProperty,pr=Object.defineProperties,gr=Object.getOwnPropertyDescriptors,br=Object.getOwnPropertySymbols,yr=Object.prototype.hasOwnProperty,xr=Object.prototype.propertyIsEnumerable,fr=(e,t,a)=>t in e?Ar(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Er=(e,t)=>{for(var a in t||(t={}))yr.call(t,a)&&fr(e,a,t[a]);if(br)for(var a of br(t))xr.call(t,a)&&fr(e,a,t[a]);return e},hr=(e,t)=>pr(e,gr(t));const vr=e=>{var t;const a=e.roomTypes.map((e=>({value:e.id,label:e.name}))),[n,{loading:l}]=function(e){const t=B(B({},k),void 0);return x.D(rt,t)}(),{register:o,handleSubmit:m,setValue:s,watch:i,formState:{isDirty:c,isValid:d}}=(0,ha.cI)({defaultValues:{name:e.room.name,roomModelTypeId:e.room.roomModelTypeId,roomLayoutId:e.room.roomLayoutId,roomTypeIds:null==(t=e.room.roomTypes)?void 0:t.map((e=>e.id))}}),u=i("roomTypeIds"),A=(0,r.useCallback)((()=>a.filter((e=>null==u?void 0:u.includes(e.value)))),[u]);return(0,r.useEffect)((()=>{var t;s("name",e.room.name),s("roomModelTypeId",e.room.roomModelTypeId),s("roomLayoutId",e.room.roomLayoutId),s("roomTypeIds",null==(t=e.room.roomTypes)?void 0:t.map((e=>e.id)))}),[e.room.id]),r.createElement("form",{className:"flex-1 px-4 py-2 ml-4 border border-gray-300",onSubmit:m((t=>{const a=t,r=a.roomTypeIds;delete a.roomTypeIds,n({variables:{id:e.room.id,attributes:a,roomTypeIds:r}})}))},r.createElement("div",{className:"divide-y divide-gray-200"},r.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},e.room.name),r.createElement("div",{className:"pt-4"},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"sm:col-span-6"},r.createElement("label",{htmlFor:"roomType",className:"block text-sm font-medium text-gray-700"},"部屋種別"),r.createElement(ur.ZP,{options:a,isMulti:!0,isClearable:!0,closeMenuOnSelect:!1,value:A(),onChange:e=>(e=>{const t=e.map((e=>e.value));s("roomTypeIds",t,{shouldDirty:!0})})(e)}))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"建物形式"),r.createElement("select",hr(Er({},o("roomModelTypeId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),r.createElement("option",{value:""}),e.roomModelTypes.map((e=>r.createElement("option",{value:e.id,key:`roomModelTypeId-${e.id}`},e.name))))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"間取図レイアウトタイプ"),r.createElement("select",hr(Er({},o("roomLayoutId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),r.createElement("option",{value:""}),e.roomLayouts.map((e=>r.createElement("option",{value:e.id,key:`roomLayoutId-${e.id}`},e.name))))))),r.createElement("div",{className:"pt-5"},r.createElement("div",{className:"flex justify-end"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 ml-3 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm disabled:opacity-50",disabled:!c||l||!d,loading:l},"保存"))))},wr=()=>{const{roomId:e}=(0,A.UO)(),{data:{room:t=null}={}}=function(e){const t=B(B({},k),e);return f.a(ft,t)}({variables:{id:e}}),{data:{roomTypes:a=[]}={}}=aa(),{data:{roomLayouts:n=[]}={}}=Jt(),{data:{roomModelTypes:l=[]}={}}=function(e){const t=B(B({},k),void 0);return f.a(Qt,t)}();return t?r.createElement(vr,{room:t,roomTypes:a,roomLayouts:n,roomModelTypes:l}):null},Nr={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Br=({buildingNumber:e,isOpen:t,handleClose:a})=>{const[n,{loading:l}]=function(e){const t=B(B({},k),void 0);return x.D(tt,t)}(),o=(0,Ea.TA)({initialValues:{name:""},onSubmit:t=>{n({variables:{buildingNumberId:e.id,attributes:t}}),a(),o.resetForm()}});return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:t,onRequestClose:a,style:Nr},r.createElement("h1",{className:"text-xl"},"号室"),r.createElement("form",{className:"p-6 space-y-6",onSubmit:o.handleSubmit},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:o.values.name,onChange:o.handleChange,onBlur:o.handleBlur})),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:l},"保存")))))};var kr=Object.defineProperty,Cr=Object.getOwnPropertySymbols,Ir=Object.prototype.hasOwnProperty,Dr=Object.prototype.propertyIsEnumerable,jr=(e,t,a)=>t in e?kr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const $r={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Fr=({id:e,isOpen:t,handleClose:a})=>{var n;const[l,{loading:o,data:m}]=function(e){const t=B(B({},k),e);return x.D(et,t)}({onCompleted:e=>{e.copyRoom.error||a()}}),{register:s,handleSubmit:i,formState:{errors:c,isDirty:d,isValid:u}}=(0,ha.cI)({mode:"all",defaultValues:{name:""}});return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:t,onRequestClose:a,style:$r},r.createElement("h1",{className:"text-xl"},"部屋コピー"),(null==m?void 0:m.copyRoom.error)&&r.createElement("p",{className:"text-red-500"},null==m?void 0:m.copyRoom.error),r.createElement("form",{className:"p-6 space-y-6",onSubmit:i((t=>{l({variables:{id:e,attributes:t}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))Ir.call(t,a)&&jr(e,a,t[a]);if(Cr)for(var a of Cr(t))Dr.call(t,a)&&jr(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("name",{required:!0}))),"required"===(null==(n=c.name)?void 0:n.type)&&r.createElement("p",{className:"text-red-400"},"名称は必須項目です")),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存")))))},Sr=e=>{const{id:t,roomId:a}=(0,A.UO)(),[n]=function(e){const t=B(B({},k),e);return x.D(at,t)}({onCompleted:e=>{e.removeRoom.error&&alert(e.removeRoom.error)}}),[l,o]=(0,r.useState)(null),m=(0,r.useMemo)((()=>a===e.room.id),[a,e.room.id]);return r.createElement(r.Fragment,null,r.createElement("tr",{key:e.room.id,className:m?"bg-blue-100":""},r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/buildings/${t}/building_numbers/${e.room.buildingNumberId}/rooms/${e.room.id}`,className:"hover:underline focus:outline-none"},e.room.name)),r.createElement("td",{className:"px-6 py-4 text-xs font-medium text-gray-900 flex items-center justify-center space-x-2"},r.createElement(ga,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>o(e.room.id)},"コピー"),r.createElement(ga,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>{confirm("本当に削除しますか？")&&n({variables:{id:e.room.id}})}},"削除"))),l&&r.createElement(Fr,{id:l,isOpen:!!l,handleClose:()=>o(null)}))},Pr=()=>{var e;const{buidingNumberId:t}=(0,A.UO)(),{data:{buildingNumber:a=null}={}}=function(e){const t=B(B({},k),e);return f.a(yt,t)}({variables:{id:t}}),[n,l]=(0,r.useState)(!1);return a?r.createElement(r.Fragment,null,r.createElement("div",{className:"w-64 ml-4 border border-gray-300 rounded-lg shadow"},r.createElement("table",{className:"w-full p-0 divide-y divide-gray-200"},r.createElement("thead",{className:"h-5 bg-gray-100"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"flex px-6 py-1 text-xs font-medium leading-7 tracking-wider text-gray-500"},"号室"),r.createElement("th",null,r.createElement("span",null,r.createElement(ga,{className:"inline-flex justify-center px-2 py-1 ml-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm",type:"button",onClick:()=>{l(!0)}},"追加"))))),r.createElement("tbody",null,null==(e=null==a?void 0:a.rooms)?void 0:e.map((e=>r.createElement(Sr,{key:e.id,room:e}))))),r.createElement(Br,{buildingNumber:a,isOpen:n,handleClose:()=>l(!1)})),r.createElement(A.AW,{path:"/buildings/:id/building_numbers/:buidingNumberId/rooms/:roomId"},r.createElement(wr,null))):null},Tr=()=>{var e;const{id:t,buidingNumberId:a}=(0,A.UO)(),[n,l]=(0,r.useState)(!1),[o,m]=(0,r.useState)(null),{data:{building:s=null}={}}=bt({variables:{id:t}}),[i]=function(e){const t=B(B({},k),e);return x.D(le,t)}({onCompleted:e=>{e.removeBuildingNumber.error&&alert(e.removeBuildingNumber.error)}}),c=e=>e.id===a;return r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},s&&r.createElement(Ja,{building:s}),r.createElement(Qa,null),r.createElement("div",{className:"mt-6"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>l(!0)},"号棟追加")),r.createElement(dr,{isOpen:n,handleClose:()=>{l(!1),m(null)},buildingNumber:o}),r.createElement("div",{className:"flex mt-6"},r.createElement("div",{className:"overflow-hidden border border-gray-300 shadow sm:rounded-lg"},r.createElement("table",{className:"divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-100"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"px-6 py-1 text-xs font-medium leading-8 tracking-wider text-gray-500"},"号棟"),r.createElement("th",{scope:"col",className:"relative px-6 py-3"},r.createElement("span",{className:"sr-only"},"Edit")))),r.createElement("tbody",null,null==(e=null==s?void 0:s.buildingNumbers)?void 0:e.map((e=>r.createElement("tr",{key:e.id,className:c(e)?"bg-blue-100":""},r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/buildings/${t}/building_numbers/${e.id}`,className:"hover:underline focus:outline-none"},e.name)),r.createElement("td",{className:"px-2 py-2 text-xs font-medium flex items-center"},r.createElement(ga,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>(e=>{m(e),l(!0)})(e)},"編集"),r.createElement(ga,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>(e=>{confirm("削除してよろしいですか？")&&i({variables:{id:e.id}})})(e)},"削除")))))))),r.createElement(A.rs,null,r.createElement(A.AW,{path:"/buildings/:id/building_numbers/:buidingNumberId/rooms/:roomId"},r.createElement(Pr,null)),r.createElement(A.AW,{path:"/buildings/:id/building_numbers/:buidingNumberId"},r.createElement(Pr,null))))))},Or=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/buildings/:id/building_numbers/:buidingNumberId"},r.createElement(Tr,null)),r.createElement(A.AW,{path:"/buildings/:id/building_numbers"},r.createElement(Tr,null))),Mr=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/buildings/:id/building_numbers"},r.createElement(Or,null)),r.createElement(A.AW,{path:"/buildings/:id/base"},r.createElement(ir,null)),r.createElement(A.AW,{path:"/buildings"},r.createElement(Da,null)))),Ur=({comment:e})=>r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/comments",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"コメント一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==e?void 0:e.id)))));var Rr=Object.defineProperty,Vr=Object.defineProperties,Yr=Object.getOwnPropertyDescriptors,qr=Object.getOwnPropertySymbols,Lr=Object.prototype.hasOwnProperty,_r=Object.prototype.propertyIsEnumerable,zr=(e,t,a)=>t in e?Rr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Wr=(e,t)=>{for(var a in t||(t={}))Lr.call(t,a)&&zr(e,a,t[a]);if(qr)for(var a of qr(t))_r.call(t,a)&&zr(e,a,t[a]);return e};const Zr=e=>{var t;const a=(0,A.k6)(),[n,{loading:l}]=function(e){const t=B(B({},k),e);return x.D(ie,t)}({onCompleted:e=>{e.updateComment.error?alert(e.updateComment.error):a.push(`/comments${location.search}`)}}),[o]=function(e){const t=B(B({},k),e);return x.D(se,t)}({onCompleted:e=>{e.removeComment.error?alert(e.removeComment.error):a.push(`/comments${location.search}`)}}),{register:m,handleSubmit:s,formState:{isDirty:i,isValid:c,errors:d}}=(0,ha.cI)({defaultValues:{comment:e.comment.comment,commentTypeId:e.comment.commentTypeId}});return r.createElement("div",{className:"mt-4"},r.createElement(Ur,{comment:e.comment}),r.createElement("form",{className:"p-6 space-y-6",onSubmit:s((t=>{n({variables:{id:e.comment.id,attributes:t}})}))},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"管轄MC"),r.createElement("select",(u=Wr({},m("commentTypeId")),Vr(u,Yr({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}))),r.createElement("option",{value:""}),e.commentTypes.map((e=>r.createElement("option",{value:e.id,key:`commentType-${e.id}`},e.name))))),r.createElement("div",{className:"col-span-6"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",Wr({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("comment",{required:!0}))),"required"===(null==(t=d.comment)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です"))),r.createElement("div",{className:"py-3 text-right flex items-center"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm hover:bg-gray-200",onClick:()=>a.push(`/comments${location.search}`)},"戻る"),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-red-400 rounded-sm shadow-sm hover:bg-red-200",onClick:()=>{confirm("削除してよろしいですか？")&&o({variables:{id:e.comment.id}})}},"削除"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:l||!c||!i,loading:l},"保存"))));var u},Hr=()=>{const{id:e}=(0,A.UO)(),{data:{comment:t=null}={}}=function(e){const t=B(B({},k),e);return f.a(ht,t)}({variables:{id:e}}),{data:{commentTypes:a=[]}={}}=kt();return t&&a?r.createElement(Zr,{comment:t,commentTypes:a}):null},Gr=()=>{const{data:{comments:e=[]}={}}=wt({fetchPolicy:"cache-and-network",variables:{search:{}}});return r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"コメント"),r.createElement(u.rU,{to:"/comments/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"ID"),r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"分類"),r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),r.createElement("th",null))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},e.map((e=>r.createElement("tr",{className:"bg-white",key:null==e?void 0:e.id},r.createElement("td",{className:"w-10 ml-10 text-xs"},r.createElement("div",{className:"px-2 cursor-pointer"},null==e?void 0:e.id)),r.createElement("td",{className:"w-32 ml-10 text-xs"},r.createElement("div",{className:"px-2 cursor-pointer"},null==e?void 0:e.commentType.name)),r.createElement("td",{className:"px-2 py-4 text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300"},r.createElement(u.rU,{to:`/comments/${null==e?void 0:e.id}`},null==e?void 0:e.comment))))))))))};var Kr=Object.defineProperty,Jr=Object.defineProperties,Qr=Object.getOwnPropertyDescriptors,Xr=Object.getOwnPropertySymbols,en=Object.prototype.hasOwnProperty,tn=Object.prototype.propertyIsEnumerable,an=(e,t,a)=>t in e?Kr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,rn=(e,t)=>{for(var a in t||(t={}))en.call(t,a)&&an(e,a,t[a]);if(Xr)for(var a of Xr(t))tn.call(t,a)&&an(e,a,t[a]);return e};const nn=()=>{var e,t;const a=(0,A.k6)(),[n,{loading:l}]=function(e){const t=B(B({},k),e);return x.D(me,t)}({onCompleted:e=>{e.createComment.error||a.push(`/comments${location.search}`)}}),{data:{commentTypes:o=[]}={}}=kt(),{register:m,handleSubmit:s,formState:{isDirty:i,isValid:c,errors:d}}=(0,ha.cI)({mode:"all"});return r.createElement("div",{className:"w-full"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/comments",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"コメント一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{n({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"管轄MC"),r.createElement("select",(p=rn({},m("commentTypeId",{required:!0})),Jr(p,Qr({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}))),r.createElement("option",{value:""}),o.map((e=>r.createElement("option",{value:e.id,key:`commentType-${e.id}`},e.name)))),"required"===(null==(e=d.commentTypeId)?void 0:e.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"comment",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",rn({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("comment",{required:!0}))),"required"===(null==(t=d.comment)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:l||!c||!i,loading:l},"保存")))))));var p},ln=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/comments/new"},r.createElement(nn,null)),r.createElement(A.AW,{path:"/comments/:id"},r.createElement(Hr,null)),r.createElement(A.AW,{path:"/comments"},r.createElement(Gr,null)))),on=({commentType:e})=>r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/commenttypes",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"コメント種別一覧"))),e&&r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==e?void 0:e.id)))));var mn=Object.defineProperty,sn=Object.getOwnPropertySymbols,cn=Object.prototype.hasOwnProperty,dn=Object.prototype.propertyIsEnumerable,un=(e,t,a)=>t in e?mn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const An=e=>{var t;const a=(0,A.k6)(),[n,{loading:l}]=function(e){const t=B(B({},k),e);return x.D(ue,t)}({onCompleted:e=>{e.updateCommentTypeModel.error?alert(e.updateCommentTypeModel.error):a.push(`/commenttypes${location.search}`)}}),[o]=function(e){const t=B(B({},k),e);return x.D(de,t)}({onCompleted:e=>{e.removeCommentTypeModel.error?alert(e.removeCommentTypeModel.error):a.push(`/commenttypes${location.search}`)}}),{register:m,handleSubmit:s,formState:{isDirty:i,isValid:c,errors:d}}=(0,ha.cI)({defaultValues:{name:e.commentType.name}});return r.createElement("div",{className:"mt-4"},r.createElement(on,{commentType:e.commentType}),r.createElement("form",{className:"p-6 space-y-6",onSubmit:s((t=>{n({variables:{id:e.commentType.id,attributes:t}})}))},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"col-span-6"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))cn.call(t,a)&&un(e,a,t[a]);if(sn)for(var a of sn(t))dn.call(t,a)&&un(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(t=d.name)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です"))),r.createElement("div",{className:"py-3 text-right flex items-center"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm hover:bg-gray-200",onClick:()=>a.push(`/commenttypes${location.search}`)},"戻る"),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-red-400 rounded-sm shadow-sm hover:bg-red-200",onClick:()=>{confirm("削除してよろしいですか？")&&o({variables:{id:e.commentType.id}})}},"削除"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:l||!c||!i,loading:l},"保存"))))},pn=()=>{const{id:e}=(0,A.UO)(),{data:{commentType:t=null}={}}=function(e){const t=B(B({},k),e);return f.a(Nt,t)}({variables:{id:e}});return t?r.createElement(An,{commentType:t}):null},gn=()=>{const{data:{commentTypes:e=[]}={}}=kt({fetchPolicy:"cache-and-network"});return r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"コメント種別"),r.createElement(u.rU,{to:"/commenttypes/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"ID"),r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),r.createElement("th",null))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},e.map((e=>r.createElement("tr",{className:"bg-white",key:null==e?void 0:e.id},r.createElement("td",{className:"w-10 ml-10 text-xs"},r.createElement("div",{className:"px-2 cursor-pointer"},null==e?void 0:e.id)),r.createElement("td",{className:"px-2 py-4 text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300"},r.createElement(u.rU,{to:`/commenttypes/${e.id}`},e.name))))))))))};var bn=Object.defineProperty,yn=Object.getOwnPropertySymbols,xn=Object.prototype.hasOwnProperty,fn=Object.prototype.propertyIsEnumerable,En=(e,t,a)=>t in e?bn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const hn=()=>{var e;const t=(0,A.k6)(),[a,{loading:n}]=function(e){const t=B(B({},k),e);return x.D(ce,t)}({onCompleted:e=>{e.createCommentTypeModel.error||t.push(`/commenttypes${location.search}`)}}),{register:l,handleSubmit:o,formState:{isDirty:m,isValid:s,errors:i}}=(0,ha.cI)({mode:"all"});return r.createElement("div",{className:"w-full"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement(on,null)),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:o((e=>{a({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))xn.call(t,a)&&En(e,a,t[a]);if(yn)for(var a of yn(t))fn.call(t,a)&&En(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("name",{required:!0}))),"required"===(null==(e=i.name)?void 0:e.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!s||!m,loading:n},"保存")))))))},vn=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/commenttypes/new"},r.createElement(hn,null)),r.createElement(A.AW,{path:"/commenttypes/:id"},r.createElement(pn,null)),r.createElement(A.AW,{path:"/commenttypes"},r.createElement(gn,null))));var wn=Object.defineProperty,Nn=Object.getOwnPropertySymbols,Bn=Object.prototype.hasOwnProperty,kn=Object.prototype.propertyIsEnumerable,Cn=(e,t,a)=>t in e?wn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const In={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Dn=({isOpen:e,onClose:t})=>{var a;const n=(0,A.k6)(),[l,{loading:o,data:m}]=function(e){const t=B(B({},k),e);return x.D(Ae,t)}({onCompleted:e=>{e.createCompany.error||(b(),n.push(`/companies/${e.createCompany.company.id}`))}}),{register:s,handleSubmit:i,reset:c,formState:{isDirty:d,isValid:u,errors:p}}=(0,ha.cI)({defaultValues:{name:""}}),b=()=>{c(),t()};return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:e,onRequestClose:b,style:In},r.createElement("h1",{className:"text-xl"},"業者新規作成"),(null==m?void 0:m.createCompany.error)&&r.createElement("p",{className:"text-red-500"},m.createCompany.error),r.createElement("form",{className:"p-6 space-y-6",onSubmit:i((e=>{l({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))Bn.call(t,a)&&Cn(e,a,t[a]);if(Nn)for(var a of Nn(t))kn.call(t,a)&&Cn(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("name",{required:!0}))),"required"===(null==(a=p.name)?void 0:a.type)&&r.createElement("p",{className:"text-red-400"},"名称は必須項目です")),r.createElement("div",{className:"py-3 text-right flex items-center space-x-4"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm",onClick:()=>b()},"キャンセル"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存")))))},jn=()=>{const{data:{companies:e=[]}={}}=It({fetchPolicy:"cache-and-network"}),[t,a]=(0,r.useState)(!1);return r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"業者マスター"),r.createElement("div",{className:"my-4"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>a(!0)},"新規作成"),t&&r.createElement(Dn,{isOpen:t,onClose:()=>a(!1)})),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"w-6 px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"名称"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"業務種別"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"都度契約"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"月次契約"))),r.createElement("tbody",{className:"divide-y divide-gray-200 bg-white"},e.map((e=>{var t;return r.createElement("tr",{className:"bg-white",key:e.id},r.createElement("td",{className:""},r.createElement("div",{className:"px-2"},e.id)),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},r.createElement(u.rU,{to:`/companies/${e.id}${location.search}`},e.name)),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},null==(t=e.companyTypes)?void 0:t.map((e=>e.name)).join("/")),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},e.isOrderEach&&"都度契約"),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},e.isOrderMonthly&&"月次契約"))})))))))};var $n=Object.defineProperty,Fn=Object.getOwnPropertySymbols,Sn=Object.prototype.hasOwnProperty,Pn=Object.prototype.propertyIsEnumerable,Tn=(e,t,a)=>t in e?$n(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,On=(e,t)=>{for(var a in t||(t={}))Sn.call(t,a)&&Tn(e,a,t[a]);if(Fn)for(var a of Fn(t))Pn.call(t,a)&&Tn(e,a,t[a]);return e};const Mn=e=>{var t;const{id:a}=(0,A.UO)(),n=(0,A.k6)(),[l,{loading:o}]=function(e){const t=B(B({},k),e);return x.D(ge,t)}({onCompleted:e=>{e.updateCompany.error?alert(e.updateCompany.error):n.push(`/companies${location.search}`)}}),[m]=function(e){const t=B(B({},k),e);return x.D(pe,t)}({onCompleted:e=>{e.removeCompany.error?alert(e.removeCompany.error):n.push(`/companies${location.search}`)}}),{register:s,handleSubmit:i,formState:{isDirty:c,isValid:d,errors:p}}=(0,ha.cI)({defaultValues:{name:e.company.name,zipcode:e.company.name,address:e.company.address,tel:e.company.tel,fax:e.company.fax,companyTypeIds:e.company.companyTypeIds,isOrderEach:e.company.isOrderEach,isOrderMonthly:e.company.isOrderMonthly}});return r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:`/companies${location.search}`,className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"業者マスター一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},e.company.name))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:i((e=>{l({variables:{id:a,attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",On({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("name",{required:!0}))),"required"===(null==(t=p.name)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"zipcode",className:"block text-sm font-medium text-gray-700"},"郵便番号"),r.createElement("input",On({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("zipcode")))),r.createElement("div",null,r.createElement("label",{htmlFor:"address",className:"block text-sm font-medium text-gray-700"},"住所"),r.createElement("input",On({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("address")))),r.createElement("div",null,r.createElement("label",{htmlFor:"tel",className:"block text-sm font-medium text-gray-700"},"TEL"),r.createElement("input",On({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("tel")))),r.createElement("div",null,r.createElement("label",{htmlFor:"fax",className:"block text-sm font-medium text-gray-700"},"FAX"),r.createElement("input",On({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("fax")))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"業務種別"),r.createElement("div",{className:"flex mb-2 space-x-7"},e.companyTypes.map((e=>r.createElement("div",{className:"flex items-center space-x-2",key:e.id},r.createElement("input",On({id:e.id,value:e.id,type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},s("companyTypeIds"))),r.createElement("label",{className:"block text-sm font-medium text-gray-700"},e.name)))))),r.createElement("div",{className:"mt-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"契約種別"),r.createElement("div",{className:"flex items-center space-x-4"},r.createElement("div",{className:"flex items-center space-x-2"},r.createElement("input",On({id:"isOrderEach",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},s("isOrderEach"))),r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"都度契約")),r.createElement("div",{className:"flex items-center space-x-2"},r.createElement("input",On({id:"isOrderMonthly",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},s("isOrderMonthly"))),r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"月次契約")))),r.createElement("div",{className:"py-3 text-right"},r.createElement("div",{className:"inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},r.createElement(u.rU,{to:`/companies${location.search}`},"戻る")),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-gray-300 rounded-sm shadow-sm hover:bg-red-200",onClick:()=>{confirm("削除してよろしいですか？")&&m({variables:{id:a}})}},"削除"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!c||!d,loading:o},"保存"))))))},Un=()=>{const{id:e}=(0,A.UO)(),{data:{company:t=null}={}}=function(e){const t=B(B({},k),e);return f.a(Dt,t)}({variables:{id:e}}),{data:{companyTypes:a=[]}={}}=Ft();return t&&a?r.createElement(Mn,{company:t,companyTypes:a}):null},Rn=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/companies/:id"},r.createElement(Un,null)),r.createElement(A.AW,{path:"/companies"},r.createElement(jn,null))));var Vn=Object.defineProperty,Yn=Object.getOwnPropertySymbols,qn=Object.prototype.hasOwnProperty,Ln=Object.prototype.propertyIsEnumerable,_n=(e,t,a)=>t in e?Vn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const zn=e=>{var t;const{id:a}=(0,A.UO)(),n=(0,A.k6)(),[l,{loading:o,data:m}]=function(e){const t=B(B({},k),e);return x.D(xe,t)}({onCompleted:e=>{e.updateCompanyTypeModel.error||n.push(`/companytypes${location.search}`)}}),{register:s,handleSubmit:i,formState:{errors:c,isDirty:d,isValid:u}}=(0,ha.cI)({mode:"all",defaultValues:{name:e.companyType.name}});return r.createElement("div",{className:"mt-10 md:col-span-2 w-screenmax-w-xl"},(null==m?void 0:m.updateCompanyTypeModel.error)&&r.createElement("p",{className:"text-red-500"},null==m?void 0:m.updateCompanyTypeModel.error),r.createElement("form",{className:"max-w-xl p-6 space-y-6",onSubmit:i((e=>{l({variables:{id:a,attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))qn.call(t,a)&&_n(e,a,t[a]);if(Yn)for(var a of Yn(t))Ln.call(t,a)&&_n(e,a,t[a]);return e})({type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},s("name",{required:!0}))),"required"===(null==(t=c.name)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"名称は必須項目です")),r.createElement("div",{className:"flex space-x-4 py-3 text-right"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-500 border-red-400 border  rounded-sm shadow-sm hover:bg-red-300",onClick:e.onRemove,loading:o},"削除"),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border-gray-400 border rounded-sm shadow-sm hover:bg-gray-300",onClick:()=>n.push("/companytypes")},"戻る"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存"))))},Wn=e=>{var t;const{id:a}=(0,A.UO)();return r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/companytypes",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"業者種別"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==(t=e.companyType)?void 0:t.name))))),e.children))},Zn=()=>{const{id:e}=(0,A.UO)(),t=(0,A.k6)(),{data:{companyType:a=null}={}}=function(e){const t=B(B({},k),e);return f.a(jt,t)}({variables:{id:e}}),[n]=function(e){const t=B(B({},k),e);return x.D(ye,t)}({onCompleted:e=>{e.removeCompanyTypeModel.error||t.push(`/companytypes${location.search}`)}});return a?r.createElement(Wn,{companyType:a},r.createElement(zn,{companyType:a,onRemove:()=>{confirm("削除しますか？")&&n({variables:{id:e}})}})):null};var Hn=Object.defineProperty,Gn=Object.getOwnPropertySymbols,Kn=Object.prototype.hasOwnProperty,Jn=Object.prototype.propertyIsEnumerable,Qn=(e,t,a)=>t in e?Hn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Xn=()=>{var e;const{id:t}=(0,A.UO)(),a=(0,A.k6)(),[n,{loading:l,data:o}]=function(e){const t=B(B({},k),e);return x.D(be,t)}({onCompleted:e=>{e.createCompanyTypeModel.error||a.push("/companytypes")}}),{register:m,handleSubmit:s,formState:{errors:i,isDirty:c,isValid:d}}=(0,ha.cI)({mode:"all"});return r.createElement(Wn,null,r.createElement("div",{className:"mt-10 md:col-span-2 w-screenmax-w-xl"},(null==o?void 0:o.createCompanyTypeModel.error)&&r.createElement("p",{className:"text-red-500"},null==o?void 0:o.createCompanyTypeModel.error),r.createElement("form",{className:"max-w-xl p-6 space-y-6",onSubmit:s((e=>{n({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))Kn.call(t,a)&&Qn(e,a,t[a]);if(Gn)for(var a of Gn(t))Jn.call(t,a)&&Qn(e,a,t[a]);return e})({type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(e=i.name)?void 0:e.type)&&r.createElement("p",{className:"text-red-400"},"名称は必須項目です")),r.createElement("div",{className:"flex space-x-4 py-3 text-right"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border-gray-400 border rounded-sm shadow-sm hover:bg-gray-300",onClick:()=>a.push("/companytypes")},"戻る"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm disabled:opacity-30",disabled:!c||!d,loading:l},"保存")))))};var el=a(7145),tl=a.n(el),al=a(8217),rl=a(2445),nl=a(2452),ll=a(9555);const ol=({id:e,index:t,companyType:a,moveItem:n,order:l})=>{const o=(0,r.useRef)(null),[{handlerId:m},s]=(0,nl.L)({accept:"CompanyType",collect:e=>({handlerId:e.getHandlerId()}),hover(e,a){var r;if(!o.current)return;const l=e.index,m=t;if(l===m)return;const s=null==(r=o.current)?void 0:r.getBoundingClientRect(),i=(s.bottom-s.top)/2,c=a.getClientOffset().y-s.top;l<m&&c<i||l>m&&c>i||(n(e.id,l,m),e.index=m)}}),[{isDragging:i},c]=(0,ll.c)({type:"CompanyType",item:()=>({id:e,index:t}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: CompanyType.tsx:87 ~ CompanyType ~ item",e),l(e.id,e.index)}}),d=i?0:1;return c(s(o)),r.createElement("div",{ref:o,className:"bg-white flex w-full",style:{opacity:d}},r.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/companytypes/${a.id}`},a.name)))},ml=()=>{const[e,t]=(0,r.useState)([]),{data:{companyTypes:a=[]}={}}=Ft({fetchPolicy:"cache-and-network"}),[n]=function(e){const t=B(B({},k),void 0);return x.D(fe,t)}(),l=(e,a,r)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${a}`),console.log(`update: hoverIndex: ${r}`),t((e=>tl()(e,{$splice:[[a,1],[r,0,e[a]]]})))},o=(e,t)=>{console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ id",e),console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ index",t),n({variables:{id:e,attributes:{position:t}}})};return(0,r.useEffect)((()=>{t(a)}),[a]),r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"業者種別"),r.createElement(u.rU,{to:"/companytypes/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("div",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("div",{className:"bg-gray-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"名称"))),r.createElement(al.W,{backend:rl.PD},r.createElement("div",{className:"bg-white divide-y divide-gray-200"},e.map(((e,t)=>r.createElement(ol,{id:e.id,index:t,companyType:e,key:e.id,moveItem:l,order:o})))))))))},sl=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/companytypes/new"},r.createElement(Xn,null)),r.createElement(A.AW,{path:"/companytypes/:id"},r.createElement(Zn,null)),r.createElement(A.AW,{path:"/companytypes"},r.createElement(ml,null)))),il=()=>r.createElement(pa,null,r.createElement("div",null,"Dashboard"));var cl=Object.defineProperty,dl=Object.getOwnPropertySymbols,ul=Object.prototype.hasOwnProperty,Al=Object.prototype.propertyIsEnumerable,pl=(e,t,a)=>t in e?cl(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,gl=(e,t)=>{for(var a in t||(t={}))ul.call(t,a)&&pl(e,a,t[a]);if(dl)for(var a of dl(t))Al.call(t,a)&&pl(e,a,t[a]);return e};const bl=e=>{var t;const a=(0,A.k6)(),[n,{loading:l}]=function(e){const t=B(B({},k),void 0);return x.D(ve,t)}(),{register:o,handleSubmit:m,formState:{errors:s,isDirty:i,isValid:c}}=(0,ha.cI)({mode:"all",defaultValues:{name:e.item.name,shortName:e.item.shortName,price:e.item.price,orderPrice:e.item.orderPrice,companyId:e.item.companyId,companyTypeIds:e.item.companyTypeIds,order:e.item.order,unitId:e.item.unitId}});return r.createElement("div",{className:"flex flex-col py-1 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:m((t=>{n({variables:{id:e.item.id,attributes:t}}).then((()=>a.push(`/items${location.search}`)))}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",gl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("name",{required:!0}))),"required"===(null==(t=s.name)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"shortName",className:"block text-sm font-medium text-gray-700"},"略称"),r.createElement("input",gl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("shortName")))),r.createElement("div",null,r.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"単価"),r.createElement("input",gl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("price",{valueAsNumber:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"orderPrice",className:"block text-sm font-medium text-gray-700"},"発注単価"),r.createElement("input",gl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("orderPrice",{valueAsNumber:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"業者種別"),r.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},e.companyTypes.map((e=>r.createElement("div",{className:"flex items-center h-5",key:`companyType-${e.id}`},r.createElement("input",gl({id:e.id,key:e.id,type:"checkbox",value:e.id,className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},o("companyTypeIds"))),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.name))))))),r.createElement("div",null,r.createElement("label",{htmlFor:"unitId",className:"block text-sm font-medium text-gray-700"},"単位"),r.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},r.createElement("select",gl({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},o("unitId")),r.createElement("option",{value:""}),e.units.map((e=>r.createElement("option",{key:`unit-${e.id}`,value:e.id},e.name)))))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"発注対象"),r.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},r.createElement("input",gl({type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},o("order"))))),r.createElement("div",{className:"py-3 text-right"},r.createElement("div",{className:"inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},r.createElement(u.rU,{to:`/items${location.search}`},"閉じる")),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!i||!c,loading:l},"保存")))))},yl={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"800px",height:"680px"}},xl=()=>{const{id:e}=(0,A.UO)(),t=(0,A.k6)(),{data:{item:a=null}={}}=function(e){const t=B(B({},k),e);return f.a(Tt,t)}({variables:{id:e}}),{data:{companyTypes:n=[]}={}}=Ft(),{data:{units:l=[]}={}}=na();return a&&0!==n.length&&0!==l.length?r.createElement(g(),{isOpen:!0,onRequestClose:()=>{t.push(`/items${location.search}`)},style:yl},r.createElement(bl,{item:a,companyTypes:n,units:l})):null};var fl=Object.defineProperty,El=Object.getOwnPropertySymbols,hl=Object.prototype.hasOwnProperty,vl=Object.prototype.propertyIsEnumerable,wl=(e,t,a)=>t in e?fl(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Nl=(e,t)=>{for(var a in t||(t={}))hl.call(t,a)&&wl(e,a,t[a]);if(El)for(var a of El(t))vl.call(t,a)&&wl(e,a,t[a]);return e};const Bl={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"800px",height:"880px"}},kl=e=>{var t,a;(0,A.k6)();const{data:{companyTypes:n=[]}={}}=Ft(),l=()=>{e.onClose()},[o,{loading:m,data:s}]=function(e){const t=B(B({},k),e);return x.D(he,t)}({onCompleted:e=>{e.createItem.error||l()}}),{data:{units:i=[]}={}}=na(),{register:c,handleSubmit:d,formState:{errors:p,isDirty:b,isValid:y}}=(0,ha.cI)({mode:"all"});return r.createElement(g(),{isOpen:e.isOpen,onRequestClose:l,style:Bl},r.createElement("div",{className:"flex flex-col py-1 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:d((t=>{const a=t.code;delete t.code,o({variables:{code:a,year:e.year,attributes:t}})}))},(null==s?void 0:s.createItem.error)&&r.createElement("p",{className:"text-red-400"},null==s?void 0:s.createItem.error),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"コード"),r.createElement("input",Nl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},c("code",{required:!0}))),"required"===(null==(t=p.code)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",Nl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},c("name",{required:!0}))),"required"===(null==(a=p.name)?void 0:a.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"shortName",className:"block text-sm font-medium text-gray-700"},"略称"),r.createElement("input",Nl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},c("shortName")))),r.createElement("div",null,r.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"単価"),r.createElement("input",Nl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},c("price",{valueAsNumber:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"orderPrice",className:"block text-sm font-medium text-gray-700"},"発注単価"),r.createElement("input",Nl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},c("orderPrice",{valueAsNumber:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"業者種別"),r.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},n.map((e=>r.createElement("div",{className:"flex items-center h-5",key:`companyType-${e.id}`},r.createElement("input",Nl({id:e.id,key:e.id,type:"checkbox",value:e.id,className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},c("companyTypeIds"))),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.name))))))),r.createElement("div",null,r.createElement("label",{htmlFor:"unitId",className:"block text-sm font-medium text-gray-700"},"単位"),r.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},r.createElement("select",Nl({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},c("unitId")),r.createElement("option",{value:""}),i.map((e=>r.createElement("option",{key:`unit-${e.id}`,value:e.id},e.name)))))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"発注対象"),r.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},r.createElement("input",Nl({id:"order",type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},c("order"))))),r.createElement("div",{className:"py-3 text-right"},r.createElement("div",{className:"inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},r.createElement(u.rU,{to:`/items${location.search}`},"閉じる")),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!b||!y,loading:m},"保存"))))))};var Cl=Object.defineProperty,Il=Object.getOwnPropertySymbols,Dl=Object.prototype.hasOwnProperty,jl=Object.prototype.propertyIsEnumerable,$l=(e,t,a)=>t in e?Cl(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Fl=(e,t)=>{for(var a in t||(t={}))Dl.call(t,a)&&$l(e,a,t[a]);if(Il)for(var a of Il(t))jl.call(t,a)&&$l(e,a,t[a]);return e};const Sl=e=>{const[t,a]=(0,b.Kx)({page:b.yz,year:b.Zp,keyword:b.Zp}),[n,l]=(0,r.useState)(!1),{register:o,handleSubmit:m,watch:s,formState:{errors:i,isDirty:c,isValid:d}}=(0,ha.cI)({defaultValues:{year:t.year,keyword:t.keyword}}),A=s("year");let p=e.years.length>0?e.years[0].year+1:null;const[g,{loading:y}]=function(e){const t=B(B({},k),e);return x.D(Ee,t)}({onCompleted:()=>{e.refetch(),e.refetchYear()}});return(0,r.useEffect)((()=>{a({year:A,page:1,keyword:t.keyword})}),[A]),r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"項目"),r.createElement("form",{onSubmit:m((e=>{a({year:e.year,page:1,keyword:e.keyword})}))},r.createElement("div",{className:"my-4"},r.createElement("select",Fl({className:"px-4 py-2 border border-gray-300"},o("year")),r.createElement("option",{value:""}),e.years.map((e=>r.createElement("option",{key:`year-${e.id}`,value:String(e.year)},e.year)))),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 ml-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>l(!0),disabled:!A},"新規作成"),r.createElement(kl,{year:A,isOpen:n,onClose:()=>{l(!1),e.refetch()}}),r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 ml-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{confirm(`${p}年度の項目をコピーで作成しますか？`)&&g()},loading:y},"新年度コピー")),r.createElement("div",{className:"grid grid-cols-1 my-2 gap-y-6 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"sm:col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"キーワード"),r.createElement("div",{className:"mt-1"},r.createElement("input",Fl({type:"text",className:"block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("keyword"))))))),r.createElement(fa,{totalCount:e.pagination.totalCount||0,currentPage:e.pagination.currentPage||0,totalPages:e.pagination.totalPages||0,pageSize:20}),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"w-6 px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),r.createElement("th",{scope:"col",className:"w-24 px-6 py-3 text-xs font-medium text-left text-gray-500"},"コード"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"名称"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"単価"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"発注対象"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"発注単価"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"業者区分"))),r.createElement("tbody",{className:`divide-y divide-gray-200 ${!e.loadingItems&&"bg-white"}`},e.loadingItems&&r.createElement(ba,null),!e.loadingItems&&e.items.map((t=>r.createElement("tr",{className:"bg-white",key:`item-${t.id}`},r.createElement("td",{className:""},r.createElement("div",{className:"px-2 text-sm"},t.id)),r.createElement("td",{className:""},r.createElement("div",{className:"px-2 cursor-pointer"},r.createElement(u.rU,{to:`/items/${t.id}${location.search}`},t.itemCode.code))),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},r.createElement(u.rU,{to:`/items/${t.id}${location.search}`},t.name)),r.createElement("td",{className:"w-20"},r.createElement("div",{className:"px-2 text-sm text-right"},t.price)),r.createElement("td",{className:"w-20"},r.createElement("div",{className:"px-2 text-sm text-right"},!t.order&&"発注なし")),r.createElement("td",{className:"w-20"},r.createElement("div",{className:"px-2 text-sm text-right"},t.orderPrice)),r.createElement("td",{className:""},r.createElement("div",{className:"px-2 text-sm"},(t=>{if(!t)return"";let a=[];for(const r of t){const t=e.companyTypes.find((e=>e.id===`${r}`));t&&a.push(t.name)}return a.join(",")})(t.companyTypeIds)))))))))))},Pl=()=>{const[e]=(0,b.Kx)({page:b.yz,year:b.Zp,keyword:b.Zp}),{data:{items:{items:t=[],pagination:a={}}={}}={},refetch:n,loading:l}=function(e){const t=B(B({},k),e);return f.a(Ot,t)}({variables:{page:e.page,search:{year:e.year,keyword:e.keyword}},skip:!e.year}),{data:{years:o=[]}={},refetch:m}=ua(),{data:{companyTypes:s=[]}={}}=Ft();return o&&0!==o.length?r.createElement(Sl,{items:t,pagination:a,loadingItems:l,companyTypes:s,refetch:n,years:o,refetchYear:m}):null},Tl=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/items/:id"},r.createElement(Pl,null),r.createElement(xl,null)),r.createElement(A.AW,{path:"/items"},r.createElement(Pl,null)))),Ol=()=>{const[e,t]=(0,b.Kx)({active:b.dJ});return r.createElement("div",{className:"hidden sm:block"},r.createElement("div",{className:"border-b border-gray-200"},r.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},r.createElement("div",{onClick:()=>t({active:!0})},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(e.active?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"有効")),r.createElement("div",{onClick:()=>t({active:!1})},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(e.active?"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300":"border-indigo-500 text-indigo-600")},"無効")))))},Ml=()=>{const[e,t]=(0,b.Kx)({active:b.dJ}),{data:{jkkUsers:a=[]}={}}=Rt({variables:{search:{active:e.active}},fetchPolicy:"cache-and-network"});return(0,r.useEffect)((()=>{null!==e.active&&void 0!==e.active||t({active:!0})}),[]),null===e.active||void 0===e.active?null:r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"公社担当者"),r.createElement(Ol,null),r.createElement(u.rU,{to:"/jkk_users/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 mt-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),r.createElement("th",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-60"},"名称"))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},a.map((e=>r.createElement("tr",{className:"bg-white",key:e.id},r.createElement("td",{className:"w-6"},r.createElement("div",{className:"px-2 cursor-pointer text-xs"},e.id)),r.createElement("td",{className:"px-4 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/jkk_users/${e.id}`},e.lastName," ",e.firstName))))))))))};var Ul=Object.defineProperty,Rl=Object.getOwnPropertySymbols,Vl=Object.prototype.hasOwnProperty,Yl=Object.prototype.propertyIsEnumerable,ql=(e,t,a)=>t in e?Ul(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Ll=(e,t)=>{for(var a in t||(t={}))Vl.call(t,a)&&ql(e,a,t[a]);if(Rl)for(var a of Rl(t))Yl.call(t,a)&&ql(e,a,t[a]);return e};const _l=()=>{var e,t;const a=(0,A.k6)(),[n,{loading:l}]=function(e){const t=B(B({},k),e);return x.D(we,t)}({onCompleted:()=>{a.push(`/jkk_users${location.search}`)}}),{register:o,handleSubmit:m,formState:{errors:s,isDirty:i,isValid:c}}=(0,ha.cI)({mode:"all"});return r.createElement("div",{className:"w-full"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/jkk_users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:m((e=>{n({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),r.createElement("input",Ll({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("lastName",{required:!0}))),"required"===(null==(e=s.lastName)?void 0:e.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),r.createElement("input",Ll({type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("firstName",{required:!0}))),"required"===(null==(t=s.firstName)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:l||!c||!i,loading:l},"保存")))))))};var zl=a(9501),Wl=Object.defineProperty,Zl=Object.getOwnPropertySymbols,Hl=Object.prototype.hasOwnProperty,Gl=Object.prototype.propertyIsEnumerable,Kl=(e,t,a)=>t in e?Wl(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Jl=(e,t)=>{for(var a in t||(t={}))Hl.call(t,a)&&Kl(e,a,t[a]);if(Zl)for(var a of Zl(t))Gl.call(t,a)&&Kl(e,a,t[a]);return e};zl.Ry().shape({lastName:zl.Z_().trim().required("必須項目です"),firstName:zl.Z_().trim().required("必須項目です")});const Ql=e=>{const t=(0,A.k6)(),[a,{loading:n}]=function(e){const t=B(B({},k),e);return x.D(Ne,t)}({onCompleted:()=>t.push("/jkk_users")}),l={lastName:e.jkkUser.lastName,firstName:e.jkkUser.firstName,active:e.jkkUser.active},{register:o,handleSubmit:m,setValue:s,watch:i,formState:{errors:c,isDirty:d,isValid:p,touchedFields:g}}=(0,ha.cI)({mode:"all",defaultValues:l}),b=i("active");return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/jkk_users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},e.jkkUser.lastName," ",e.jkkUser.firstName))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:m((t=>{a({variables:{id:e.jkkUser.id,attributes:t}})}))},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),r.createElement("input",Jl({type:"text",name:"lastName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("lastName",{required:!0})))),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),r.createElement("input",Jl({type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("firstName",{required:!0})))),r.createElement("div",{className:"mt-4 space-y-4"},r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",defaultChecked:!0===l.active,checked:!0===b,onChange:e=>{e.target.checked&&s("active",!0,{shouldDirty:!0})}}),r.createElement("label",{htmlFor:"active",className:"block ml-3 text-sm font-medium text-gray-700"},"有効")),r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"non-active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",defaultChecked:!1===l.active,checked:!1===b,onChange:e=>{e.target.checked&&s("active",!1,{shouldDirty:!0})}}),r.createElement("label",{htmlFor:"non-active",className:"block ml-3 text-sm font-medium text-gray-700"},"無効"))),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!p||!d,loading:n},"保存")))))))},Xl=()=>{const{id:e}=(0,A.UO)(),{data:{jkkUser:t=null}={}}=function(e){const t=B(B({},k),e);return f.a(Mt,t)}({variables:{id:e}});return t?r.createElement(Ql,{jkkUser:t}):null},eo=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/jkk_users/new"},r.createElement(_l,null)),r.createElement(A.AW,{path:"/jkk_users/:id"},r.createElement(Xl,null)),r.createElement(A.AW,{path:"/jkk_users"},r.createElement(Ml,null)))),to=({children:e})=>{const t=(0,A.k6)(),{data:{currentUser:a=null}={}}=oa();return console.log("🚀 ~ file: Login.tsx ~ line 17 ~ currentUser",a),(0,r.useEffect)((()=>{a&&t.push("/")}),[a]),r.createElement("div",{className:"flex flex-col justify-center min-h-screen py-12 bg-gray-50 sm:px-6 lg:px-8"},e)},ao=(y.ZP`
  fragment buildingFragment on Building {
    id
    name
    address
    rank
    rankText
    mcTypeId
    buildingTypeId
  }
`,y.ZP`
  fragment buildingNumberFragment on BuildingNumber {
    id
    buildingId
    name
    position
  }
`,y.ZP`
  fragment buildingTypeFragment on BuildingType {
    id
    name
  }
`,y.ZP`
  fragment commentFragment on Comment {
    id
    comment
    commentTypeId
    remark
  }
`,y.ZP`
  fragment commentItemCodeFragment on CommentItemCode {
    id
    commentId
    itemCodeId
  }
`,y.ZP`
  fragment commentTypeFragment on CommentType {
    id
    name
    position
  }
`,y.ZP`
  fragment companyFragment on Company {
    id
    isOrderEach
    isOrderMonthly
    name
    zipcode
    address
    tel
    fax
    companyTypeIds
  }
`,y.ZP`
  fragment companyTypeFragment on CompanyType {
    id
    name
    position
  }
`,y.ZP`
  fragment imageCommentFragment on ImageComment {
    id
    projectImageId
    code
    text
  }
`,y.ZP`
  fragment itemFragment on Item {
    id
    name
    shortName
    price
    orderPrice
    companyTypeIds
    companyId
    order
    unitId
  }
`,y.ZP`
  fragment itemCodeFragment on ItemCode {
    id
    code
    name
  }
`,y.ZP`
  fragment jkkUserFragment on JkkUser {
    id
    email
    firstName
    lastName
    active
  }
`,y.ZP`
  fragment mcTypeFragment on McType {
    id
    name
  }
`,y.ZP`
  fragment orderFragment on Order {
    id
    projectId
    companyId
    orderType
    isInvoice
    orderDate
    totalPrice
    exportCsvDate
  }
`,y.ZP`
  fragment paginationFragment on Pagination {
    resultsCount
    totalCount
    totalPages
    currentPage
    nextPage
    previousPage
    hasNextPage
    hasPreviousPage
  }
`,y.ZP`
  fragment projectFragment on Project {
    id
    yearId
    buildingId
    buildingNumberId
    roomId
    userIds
    jkkUserId
    assesmentUserId
    constructionUserId
    totalItemPrice
    totalItemPriceRound
    tax
    totalPrice
    totalPriceInput
    name
    status1
    status2
    status3
    status4
    status5
    status6
    jkkStatus1
    jkkStatus2
    jkkStatus3
    jkkStatus4
    orderNumberType
    orderNumber
    assessmentDate
    assessmentUrl
    orderDate1
    orderMemo1
    orderDate2
    orderMemo2
    orderDate3
    orderMemo3
    electricStartDate1
    electricEndDate1
    waterStartDate1
    waterEndDate1
    electricStartDate2
    electricEndDate2
    waterStartDate2
    waterEndDate2
    electricStartDate3
    electricEndDate3
    waterStartDate3
    waterEndDate3
    closeTypes
    closeReason
    isVermiculite
    isFinishedVermiculite
    vermiculiteDate
    processCreatedDate
    beforeConstructionImageDate
    afterConstructionImageDate
    checklistFirstDate
    checklistLastDate
    address
    constructionStartDate
    constructionEndDate
    imageReportUrl
    isFaq
    isLackImage
    isCompleteImage
    submitDocumentDate
    availableDate
    livingYear
    livingMonth
    leaveDate
    isManagerAsbestosDocument1
    isManagerAsbestosDocument2
    isManagerAsbestosDocument3
    isOfficeworkAsbestosDocument1
    isOfficeworkAsbestosDocument2
    isOfficeworkAsbestosDocument3
    remark
    updatedItemsAt
    editUserId
  }
`,y.ZP`
  fragment projectImageFragment on ProjectImage {
    id
    projectId
    roomTypeId
    imageType
    imageBefore
    imageAfter
    image
    comment
    position
  }
`,y.ZP`
  fragment projectItemFragment on ProjectItem {
    id
    projectId
    itemId
    companyId
    code
    name
    count
    unitPrice
    amount
    roomTypeIds
    sequenceNumber
    updatedItemsAt
  }
`,y.ZP`
  fragment projectReportFragment on ProjectReport {
    id
    projectId
    worksheetBase
    worksheetItems
    worksheetImages
    worksheetChecklist
    spreadsheetId1
    spreadsheetId2
    spreadsheetId3
    spreadsheetId4
    spreadsheetId5
    spreadsheetId6
    spreadsheetId7
    spreadsheetDatetime1
    spreadsheetDatetime2
    spreadsheetDatetime3
    spreadsheetDatetime4
    spreadsheetDatetime5
    spreadsheetDatetime6
    spreadsheetDatetime7
    spreadsheetCheckReport1
    spreadsheetCheckReport2
  }
`,y.ZP`
  fragment purchaseOrderFragment on PurchaseOrder {
    id
    projectId
    fileUrl
  }
`,y.ZP`
  fragment roomFragment on Room {
    id
    buildingNumberId
    name
    position
    roomModelTypeId
    roomLayoutId
  }
`,y.ZP`
  fragment roomLayoutFragment on RoomLayout {
    id
    name
    price1
    price2
    position
  }
`,y.ZP`
  fragment roomModelTypeFragment on RoomModelType {
    id
    name
    position
  }
`,y.ZP`
  fragment roomTypeFragment on RoomType {
    id
    name
    position
  }
`,y.ZP`
  fragment unitFragment on Unit {
    id
    name
  }
`,y.ZP`
  fragment userFragment on User {
    id
    loginId
    email
    role
    firstName
    lastName
    tel
    active
    isAssesment
  }
`),ro=(y.ZP`
  fragment yearFragment on Year {
    id
    year
  }
`,y.ZP`
  query currentUser {
    currentUser {
      ...userFragment
    }
  }
  ${ao}
`),no=()=>{const e=(0,A.k6)(),[t,{data:a,loading:n}]=function(e){const t=B(B({},k),e);return x.D(ee,t)}({update:(e,{data:t})=>{(null==t?void 0:t.signin.error)||e.writeQuery({query:ro,data:{currentUser:null==t?void 0:t.signin.user}})},onCompleted:t=>{(null==t?void 0:t.signin.error)||e.push("/")}}),l=(0,Ea.TA)({initialValues:{loginId:"",password:""},onSubmit:e=>{t({variables:{loginId:e.loginId,password:e.password}})}});return r.createElement(to,null,r.createElement("div",{className:"sm:mx-auto sm:w-full sm:max-w-md"},r.createElement("img",{className:"w-auto h-12 mx-auto",src:"https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg",alt:"Workflow"}),r.createElement("h2",{className:"mt-6 text-3xl font-extrabold text-center text-gray-900"},"TOTO Login")),r.createElement("div",{className:"mt-8 sm:mx-auto sm:w-full sm:max-w-lg"},r.createElement("div",{className:"px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10"},(null==a?void 0:a.signin.error)&&r.createElement("div",{className:"p-4 rounded-md bg-red-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"ml-3"},r.createElement("h3",{className:"text-sm font-medium text-red-800"},"エラーをご確認ください"),r.createElement("div",{className:"mt-2 text-sm text-red-700"},r.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},r.createElement("li",null,null==a?void 0:a.signin.error)))))),r.createElement("form",{className:"space-y-6",onSubmit:l.handleSubmit},r.createElement("div",null,r.createElement("label",{htmlFor:"loginId",className:"block text-sm font-medium text-gray-700"},"ログインID"),r.createElement("div",{className:"mt-1"},r.createElement("input",{name:"loginId",type:"text",required:!0,className:"block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:l.values.loginId,onChange:l.handleChange,onBlur:l.handleBlur}))),r.createElement("div",null,r.createElement("label",{htmlFor:"password",className:"block text-sm font-medium text-gray-700"},"Password"),r.createElement("div",{className:"mt-1"},r.createElement("input",{id:"password",name:"password",type:"password",autoComplete:"current-password",required:!0,className:"block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:l.values.password,onChange:l.handleChange,onBlur:l.handleBlur}))),r.createElement("div",null,r.createElement(ga,{type:"submit",className:"flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:n},"ログイン"))))))},lo=()=>r.createElement(no,null);var oo=a(381),mo=a.n(oo),so=a(9198),io=a.n(so);const co=({order:e})=>{var t;return r.createElement("tr",{className:"bg-white"},r.createElement("td",{className:"text-sm px-2"},null==e?void 0:e.id),r.createElement("td",{className:"py-2 text-sm"},e.orderDate),r.createElement("td",{className:"px-2 py-4 text-sm"},e.company.name),r.createElement("td",{className:"px-2 py-4 text-sm"},e.project.orderNumberType,"-",e.project.orderNumber),r.createElement("td",{className:"px-2 py-4 text-sm"},e.project.name),r.createElement("td",{className:"px-2 py-4 text-sm"},e.project.address),r.createElement("td",{className:"px-2 py-4 text-sm"},null==(t=e.totalPrice)?void 0:t.toLocaleString()),r.createElement("td",{className:"px-2 py-4 text-sm"}))},uo=()=>{const[e,t]=(0,b.Kx)({page:b.yz,from:b.Zp,to:b.Zp,companyId:b.Zp,spreadsheetId:b.Zp}),{data:{orders:{orders:a=[],pagination:n={}}={}}={}}=function(e){const t=B(B({},k),e);return f.a(Yt,t)}({variables:{page:e.page||1,search:{from:e.from,to:e.to,companyId:e.companyId}},fetchPolicy:"cache-and-network"}),{data:{companies:l=[]}={}}=It(),o=(0,Ea.TA)({initialValues:{from:e.from,to:e.to,companyId:e.companyId,spreadsheetId:e.spreadsheetId},onSubmit:e=>{t({from:e.from,to:e.to,companyId:e.companyId,page:1,spreadsheetId:e.spreadsheetId})}}),[m]=function(e){const t=B(B({},k),void 0);return x.D(Be,t)}();return r.createElement("div",{className:"flex flex-col h-full"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 h-full"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"発注一覧"),r.createElement("form",{className:"px-10 py-2 mb-4 bg-white",onSubmit:o.handleSubmit},r.createElement("div",{className:"mt-6"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"発注日"),r.createElement("div",{className:"flex items-center space-x-4"},r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",selected:o.values.from&&new Date(o.values.from),className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",onChange:e=>o.setFieldValue("from",mo()(e).format("YYYY-MM-DD"))})),r.createElement("span",null," - "),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",selected:o.values.to&&new Date(o.values.to),className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",onChange:e=>o.setFieldValue("to",mo()(e).format("YYYY-MM-DD"))}))),r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"業者"),r.createElement("div",{className:"relative w-64"},r.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",value:o.values.companyId,onChange:e=>o.setFieldValue("companyId",e.target.value)},r.createElement("option",{value:""},"業者を選択してください"),l.map((e=>r.createElement("option",{value:e.id},e.name))))),r.createElement("div",{className:"sm:col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"スプレッドシートID"),r.createElement("div",{className:"mt-1"},r.createElement("input",{name:"spreadsheetId",type:"text",className:"block w-44 px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:o.values.spreadsheetId,onChange:o.handleChange,onBlur:o.handleBlur})))),r.createElement("div",{className:"mt-6 flex space-x-2"},r.createElement(ga,{type:"button",className:"text-white w-40 px-4 py-2 text-sm font-medium bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-300",onClick:()=>o.handleSubmit()},"検索"),r.createElement(ga,{type:"button",disabled:!o.values.spreadsheetId,className:"text-white w-44 px-4 py-2 text-sm font-medium bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-300",onClick:()=>{m({variables:{spreadsheetId:o.values.spreadsheetId,search:{from:o.values.from,to:o.values.to,companyId:o.values.companyId}}})}},"スプレッドシート出力"))),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-12"},"ID"),r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-28"},"発注日"),r.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"業者"),r.createElement("th",{scope:"col",className:"w-28 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"指示番号"),r.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"案件名"),r.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"案件住所"),r.createElement("th",{scope:"col",className:"w-20 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"金額"),r.createElement("th",null))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},a.map((e=>r.createElement(co,{order:e,key:null==e?void 0:e.id}))))),r.createElement(fa,{totalCount:n.totalCount||0,currentPage:n.currentPage||0,totalPages:n.totalPages||0,pageSize:20}))))},Ao=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/orders"},r.createElement(uo,null)))),po=({project:e})=>{var t,a,n;return r.createElement("tr",{className:"bg-white"},r.createElement("td",{className:"text-sm px-2"},null==e?void 0:e.id),r.createElement("td",{className:"py-2"},r.createElement("div",{className:"text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300 mt-2"},r.createElement(u.rU,{to:`/projects/${null==e?void 0:e.id}/form/base${location.search}`},null==e?void 0:e.name)),r.createElement("div",{className:"flex space-x-8"},r.createElement("div",{className:"text-sm text-gray-500 w-32"},r.createElement("span",{className:"mr-2"},null==(t=null==e?void 0:e.buildingNumber)?void 0:t.name," 号棟"),r.createElement("span",null,null==(a=null==e?void 0:e.room)?void 0:a.name," 号室")),r.createElement("div",null,r.createElement("div",null,e.status1&&r.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"1.査定表読込済"),e.status2&&r.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"2.施工前済・質疑回答待ち"),e.status3&&r.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"3.施工前済・最終CL済"),e.status4&&r.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"4.施工後不足写真待ち"),e.status5&&r.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"5.施工後済"),e.status6&&r.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"6.請求書済")),r.createElement("div",null,e.jkkStatus1&&r.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"1.CL済"),e.jkkStatus2&&r.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"2.指示書済済"),e.jkkStatus3&&r.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"3.指示書再発行待ち"),e.jkkStatus4&&r.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"4.指示書再発行済ち"))))),r.createElement("td",{className:"px-2 py-4 text-sm font-medium whitespace-nowrap"},r.createElement("div",{className:"text-gray-700 text-right"},null==(n=e.totalPrice)?void 0:n.toLocaleString())),r.createElement("td",{className:"px-2 py-4 text-sm font-medium whitespace-nowrap"},r.createElement("div",{className:"text-gray-700"},r.createElement("span",null,e.constructionStartDate),r.createElement("span",{className:"mx-2"},"-"),r.createElement("span",null,e.constructionEndDate))))},go=()=>{const[e,t]=(0,b.Kx)({page:b.yz,keyword:b.Zp,active:b.dJ,orderItem:b.Zp,orderType:b.Zp}),{data:{projects:{projects:a=[],pagination:n={}}={}}={}}=Ht({variables:{page:e.page,search:{keyword:e.keyword,orderItem:e.orderItem,orderType:e.orderType}},fetchPolicy:"cache-and-network",skip:!e.orderItem}),l=(0,Ea.TA)({initialValues:{keyword:e.keyword||"",active:!1},onSubmit:e=>{t({keyword:e.keyword,active:e.active,page:1})}});return(0,r.useEffect)((()=>{e.orderItem||t({orderItem:"id",orderType:"desc"})}),[]),r.createElement("div",{className:"flex flex-col h-full"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 h-full"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"案件一覧"),r.createElement("form",{className:"px-10 py-2 mb-4 bg-white",onSubmit:l.handleSubmit},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"sm:col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"キーワード"),r.createElement("div",{className:"mt-1"},r.createElement("input",{name:"keyword",type:"text",className:"block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:l.values.keyword,onChange:l.handleChange,onBlur:()=>l.handleSubmit()}))))),r.createElement(u.rU,{to:"/projects/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("div",{className:"flex mt-4"},r.createElement("div",{className:"relative w-32"},r.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",value:e.orderItem,onChange:e=>t({orderItem:e.target.value})},r.createElement("option",{value:"id"},"登録順"),r.createElement("option",{value:"status"},"ステータス"),r.createElement("option",{value:"jkk_status"},"JKKステータス"))),r.createElement("div",{className:"relative w-32"},r.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",value:e.orderType,onChange:e=>t({orderType:e.target.value})},r.createElement("option",{value:"asc"},"昇順"),r.createElement("option",{value:"desc"},"降順")))),r.createElement(fa,{totalCount:n.totalCount||0,currentPage:n.currentPage||0,totalPages:n.totalPages||0,pageSize:20}),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-12"},"ID"),r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),r.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"工事金額（指示書/税込）"),r.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"工期（開始日 / 終了日）"),r.createElement("th",null))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},a.map((e=>r.createElement(po,{project:e,key:null==e?void 0:e.id}))))))))},bo=[{value:"close_type1",name:"閉鎖住宅"},{value:"close_type2",name:"私負担清算"},{value:"close_type3",name:"本工事"},{value:"close_type4",name:"工事保留"},{value:"close_type5",name:"工事中止"},{value:"close_type6",name:"工事再開"},{value:"close_type7",name:"被災者住宅"},{value:"close_type8",name:"検査対象物件"},{value:"close_type9",name:"その他"}],yo=zl.Ry().shape({yearId:zl.Z_().trim().required("必須項目です"),orderNumberType:zl.Z_().trim().required("必須項目です"),orderNumber:zl.Z_().trim().required("必須項目です"),buildingId:zl.Z_().trim().required("必須項目です"),buildingNumberId:zl.Z_().trim().required("必須項目です"),roomId:zl.Z_().trim().required("必須項目です")}),xo=()=>{var e;const t=(0,A.k6)(),[a,n]=(0,r.useState)(null),[l,o]=(0,r.useState)(null),[m,{loading:s}]=function(e){const t=B(B({},k),void 0);return x.D(Ce,t)}(),[i,c]=(0,r.useState)(null),[d,p]=(0,r.useState)(null),{data:{years:g=[]}={}}=ua(),{data:{allBuildings:b=[]}={}}=function(e){const t=B(B({},k),void 0);return f.a(pt,t)}(),y=b.map((e=>({value:e.id,label:e.name}))),E=null==(e=null==i?void 0:i.buildingNumbers)?void 0:e.map((e=>({value:e.id,label:e.name}))),h=null==d?void 0:d.rooms.map((e=>({value:e.id,label:e.name}))),{data:{projects:{projects:v=[]}={}}={}}=Ht({variables:{search:{orderNumber:a,orderNumberType:l}},skip:!a}),w=(0,Ea.TA)({enableReinitialize:!0,validationSchema:yo,initialValues:{yearId:"",orderNumberType:"",orderNumber:"",buildingId:null,buildingNumberId:null,roomId:null,closeTypes:[],closeReason:""},onSubmit:e=>{m({variables:{attributes:e}}).then((()=>t.push(`/projects${location.search}`)))}});return r.createElement("div",{className:"w-full"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/projects",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"案件一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:w.handleSubmit},r.createElement("div",{className:"my-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"年度"),r.createElement("select",{value:w.values.yearId,className:"px-4 py-2 border border-gray-300",onChange:e=>{w.setFieldValue("yearId",e.target.value)}},r.createElement("option",{value:""}),g.map((e=>r.createElement("option",{key:`year-${e.id}`,value:e.id},e.year))))),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"指示番号"),r.createElement("div",{className:"flex"},r.createElement("input",{type:"text",name:"orderNumberType",className:"block w-16 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:w.values.orderNumberType,onChange:w.handleChange,onBlur:()=>{o(w.values.orderNumberType)}}),r.createElement("span",{className:"inline-flex items-center mx-4"},"-"),r.createElement("input",{type:"text",name:"orderNumber",className:"block w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:w.values.orderNumber,onChange:w.handleChange,onBlur:()=>{n(w.values.orderNumber)}})),v.length>0&&r.createElement("span",{className:"text-red-500 text-sm"},"すでに登録済みの指示番号です")),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"建物"),r.createElement(ur.ZP,{options:y,isClearable:!0,closeMenuOnSelect:!0,onChange:e=>{w.setFieldValue("buildingId",e.value),w.setFieldValue("name",e.label);const t=b.find((t=>t.id===e.value));c(t)}})),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"号棟"),r.createElement(ur.ZP,{options:E,isClearable:!0,closeMenuOnSelect:!0,onChange:e=>{w.setFieldValue("buildingNumberId",e.value);const t=i.buildingNumbers.find((t=>t.id===e.value));p(t)}})),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"号室"),r.createElement(ur.ZP,{options:h,isClearable:!0,closeMenuOnSelect:!0,onChange:e=>{w.setFieldValue("roomId",e.value)}})),r.createElement("div",{className:"col-span-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"閉鎖区分"),r.createElement("div",{className:"relative flex items-start space-x-4"},bo.map((e=>r.createElement(r.Fragment,null,r.createElement("div",{className:"flex items-center h-5"},r.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,onChange:e=>{const t=`${e.target.value}`;w.values.closeTypes.includes(t)?w.setFieldValue("closeTypes",w.values.closeTypes.filter((e=>e!==t))):w.setFieldValue("closeTypes",[...w.values.closeTypes,t])},onBlur:w.handleBlur}),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.name)))))))),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!w.dirty||!w.isValid||v.length>0,loading:s},"保存")))))))},fo=({project:e})=>r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/projects",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700","aria-current":void 0},"案件一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==e?void 0:e.name))))),Eo=()=>{const{id:e}=(0,A.UO)(),t=e=>location.pathname.indexOf(e)>-1;return r.createElement("div",{className:"hidden sm:block"},r.createElement("div",{className:"border-b border-gray-200"},r.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},r.createElement(u.rU,{to:`/projects/${e}/form/base${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("form")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報")),r.createElement(u.rU,{to:`/projects/${e}/items${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("items")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"明細")),r.createElement(u.rU,{to:`/projects/${e}/company${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("company")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"発注")),r.createElement(u.rU,{to:`/projects/${e}/images${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("images")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"画像")),r.createElement(u.rU,{to:`/projects/${e}/report${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("report")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"レポート")),r.createElement(u.rU,{to:`/projects/${e}/remark${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("remark")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"備考")))))};var ho=a(4770);const vo=e=>r.createElement("div",{className:"flex items-center space-x-1"},r.createElement("input",{id:e.name,name:e.name,type:"checkbox",className:"w-5 h-6 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:e.checked,onChange:t=>{e.onChange(t)}}),r.createElement("label",{className:"block text-xs font-medium text-gray-700"},e.label)),wo=({project:e})=>{var t,a,n,l,o,m,s,i,c,d,u;const p=(0,A.k6)(),{data:{currentUser:g}={}}=oa(),b=!(!e.editUserId||e.editUserId===g.id),[y]=Oe(),f=(0,Ea.TA)({enableReinitialize:!0,initialValues:{status1:e.status1,status2:e.status2,status3:e.status3,status4:e.status4,status5:e.status5,status6:e.status6,jkkStatus1:e.jkkStatus1,jkkStatus2:e.jkkStatus2,jkkStatus3:e.jkkStatus3,jkkStatus4:e.jkkStatus4},onSubmit:t=>{y({variables:{id:e.id,attributes:t}})}}),[E]=function(e){const t=B(B({},k),e);return x.D(Se,t)}({onCompleted:()=>p.push("/projects")});return r.createElement("form",{onSubmit:f.handleSubmit},r.createElement("div",{className:"flex items-center justify-between"},r.createElement("div",{className:"my-4 font-bold text-2xl"},"指示番号：",e.orderNumberType,"-",e.orderNumber),b&&r.createElement("div",{className:"ml-auto"},r.createElement("div",{className:"text-right"},r.createElement("div",{className:"inline-flex items-center border border-gray-500 px-4 py-2"},r.createElement("span",{className:"text-xs text-gray-500"},"編集中："),r.createElement("div",{className:"text-sm text-gray-500"},null==(t=e.editUser)?void 0:t.lastName," ",null==(a=e.editUser)?void 0:a.firstName))),r.createElement("p",{className:"text-red-600"},"他ユーザーが編集中のため更新できません"))),r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"flex items-center space-x-2 text-xs"},r.createElement("span",{className:"text-gray-500"},"件名"),r.createElement("span",{className:"text-gray-900 text-base font-medium"},null==(n=e.building)?void 0:n.name),r.createElement("span",{className:"text-gray-900 text-base font-medium"},null==(l=e.buildingNumber)?void 0:l.name,r.createElement("span",{className:"ml-2 text-gray-500"},"号棟")),r.createElement("span",{className:"text-gray-900 text-base font-medium"},null==(o=e.room)?void 0:o.name,r.createElement("span",{className:"ml-2 text-gray-500"},"号室"))),r.createElement("div",{className:"flex items-center space-x-2"},r.createElement("span",{className:"text-xs font-medium text-gray-500"},"住所"),r.createElement("span",{className:"font-medium text-gray-900 text-md"},null==(m=e.building)?void 0:m.address)),r.createElement(ho.v,{as:"div",className:"relative flex text-left ml-4"},r.createElement(ho.v.Button,null,r.createElement(Ya,{className:"h-6 text-gray-500 cursor-pointer"})),r.createElement(ho.v.Items,{className:"absolute right-0 mt-2 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none py-2 px-4 whitespace-pre"},(()=>r.createElement("button",{className:"hover:bg-gray-300 text-gray-500 flex items-center rounded-md px-2 py-2 text-sm",onClick:()=>{confirm("削除してよろしいですか？")&&E({variables:{id:e.id}})}},r.createElement(Ka,{className:"h-4 mr-2"}),"削除"))))),r.createElement("div",{className:"flex my-2 space-x-7 text-gray-500 text-sm"},r.createElement("div",null,e.year.year),r.createElement("div",null,null==(i=null==(s=e.building)?void 0:s.mcType)?void 0:i.name),r.createElement("div",null,null==(d=null==(c=e.building)?void 0:c.buildingType)?void 0:d.name),r.createElement("div",{className:"flex items-center space-x-2"},r.createElement("label",{className:"block text-sm font-medium"},"工事金額"),r.createElement("span",null,null==(u=e.totalPriceInput)?void 0:u.toLocaleString(),r.createElement("span",{className:"ml-1 text-sm text-gray-700"},"円")))),r.createElement("div",{className:"flex mt-2 items-center"},r.createElement("div",{className:"text-xs font-medium text-gray-500 mr-4"},"当社状況"),r.createElement("div",{className:"flex space-x-4"},r.createElement(vo,{label:"①査定表読込済",name:"status1",checked:f.values.status1,onChange:e=>{f.setFieldValue("status1",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"②施工前済・質疑回答待ち",name:"status2",checked:f.values.status2,onChange:e=>{f.setFieldValue("status2",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"③施工前済・最終CL済",name:"status3",checked:f.values.status3,onChange:e=>{f.setFieldValue("status3",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"④施工後不足写真待ち",name:"status4",checked:f.values.status4,onChange:e=>{f.setFieldValue("status4",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"⑤施工後済",name:"status5",checked:f.values.status5,onChange:e=>{f.setFieldValue("status5",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"⑥請求書済",name:"status6",checked:f.values.status6,onChange:e=>{f.setFieldValue("status6",e.target.checked),f.handleSubmit()}}))),r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"text-xs font-medium text-gray-500 mr-4"},"JKK状況"),r.createElement("div",{className:"flex space-x-4"},r.createElement(vo,{label:"①CL済",name:"jkkStatus1",checked:f.values.jkkStatus1,onChange:e=>{f.setFieldValue("jkkStatus1",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"②指示書済",name:"jkkStatus2",checked:f.values.jkkStatus2,onChange:e=>{f.setFieldValue("jkkStatus2",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"③指示書再発行待ち",name:"jkkStatus3",checked:f.values.jkkStatus3,onChange:e=>{f.setFieldValue("jkkStatus3",e.target.checked),f.handleSubmit()}}),r.createElement(vo,{label:"④指示書再発行済み",name:"jkkStatus4",checked:f.values.jkkStatus4,onChange:e=>{f.setFieldValue("jkkStatus4",e.target.checked),f.handleSubmit()}}))))},No=({children:e})=>{const{id:t}=(0,A.UO)(),{data:{project:a=null}={}}=Lt({variables:{id:t}});return a?r.createElement("div",{className:"px-6"},r.createElement("div",{className:"top-0 bg-gray-100 z-10 sticky pt-4"},r.createElement(fo,{project:a}),r.createElement(wo,{project:a}),r.createElement(Eo,null)),r.createElement("div",{className:"pb-10"},e)):null};var Bo=a(2660),ko=(e,t,a)=>new Promise(((r,n)=>{var l=e=>{try{m(a.next(e))}catch(e){n(e)}},o=e=>{try{m(a.throw(e))}catch(e){n(e)}},m=e=>e.done?r(e.value):Promise.resolve(e.value).then(l,o);m((a=a.apply(e,t)).next())}));(0,so.registerLocale)("ja",Bo.Z);const Co=({order:e})=>{const[t]=function(e){const t=B(B({},k),void 0);return x.D(Re,t)}(),a=(0,Ea.TA)({initialValues:{companyId:e.companyId,isInvoice:e.isInvoice,orderDate:e.orderDate},onSubmit:a=>{t({variables:{id:e.id,attributes:a}})}});return r.createElement("form",{className:"flex",onSubmit:a.handleSubmit},r.createElement("div",{className:"w-64 px-4 ml-4"},r.createElement("div",{className:"flex items-center"},e.company.name)),r.createElement("div",{className:"w-48 px-4"},r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"flex rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:a.values.orderDate&&new Date(a.values.orderDate),onChange:e=>ko(void 0,null,(function*(){yield a.setFieldValue("orderDate",mo()(e).format("YYYY-MM-DD")),a.handleSubmit()}))})))),r.createElement("div",{className:"w-32 px-4 my-auto"},r.createElement("div",{className:"flex justify-center rounded-md shadow-sm"},r.createElement("input",{type:"checkbox",name:"isInvoice",className:"w-4 h-4 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500",checked:a.values.isInvoice,onChange:e=>ko(void 0,null,(function*(){yield a.setFieldValue("isInvoice",e.target.checked),a.handleSubmit()}))}))),"monthly"===e.orderType&&r.createElement("div",{className:"w-20 text-sm"},e.exportCsvDate))},Io=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Wt({variables:{id:e},fetchPolicy:"cache-and-network"});return t?r.createElement("div",{className:"flex flex-col mt-10"},r.createElement("div",{className:"text-lg font-bold"},"都度発注"),r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},r.createElement("div",{className:"min-w-full divide-y divide-gray-200"},r.createElement("div",{className:"flex flex-1 bg-gray-50"},r.createElement("div",{className:"w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"会社"),r.createElement("div",{className:"w-48 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"発注日"),r.createElement("div",{className:"w-32 px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"請求書確認")),r.createElement("div",{className:"bg-white divide-y divide-gray-200"},t.ordersEach.map((e=>r.createElement(Co,{key:e.id,order:e}))))))))):null},Do=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Wt({variables:{id:e},fetchPolicy:"cache-and-network"});return t?r.createElement("div",{className:"flex flex-col mt-10"},r.createElement("div",{className:"text-lg font-bold"},"月次発注"),r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},r.createElement("div",{className:"min-w-full divide-y divide-gray-200"},r.createElement("div",{className:"flex flex-1 bg-gray-50"},r.createElement("div",{className:"w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"会社"),r.createElement("div",{className:"w-48 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"発注日"),r.createElement("div",{className:"w-32 px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"発注書類作成可"),r.createElement("div",{className:"px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"レポート出力日")),r.createElement("div",{className:"bg-white divide-y divide-gray-200"},t.ordersMonthly.map((e=>r.createElement(Co,{key:e.id,order:e}))))))))):null},jo=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Lt({variables:{id:e}});return t?r.createElement(No,null,r.createElement(Io,null),r.createElement(Do,null)):null};var $o=a(6486),Fo=a.n($o);const So=()=>{const{id:e}=(0,A.UO)(),t=e=>location.pathname.indexOf(e)>-1;return r.createElement("nav",{className:"flex mt-6 space-x-4","aria-label":"Tabs"},r.createElement(u.rU,{to:`/projects/${e}/form/base`,className:`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${t("form/base")&&"bg-gray-200"}`},"現場詳細"),r.createElement(u.rU,{to:`/projects/${e}/form/docs`,className:`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${t("form/docs")&&"bg-gray-200"}`},"書類管理"),r.createElement(u.rU,{to:`/projects/${e}/form/management${location.search}`,className:`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${t("form/management")&&"bg-gray-200"}`},"現場管理"))},Po=({user:e})=>r.createElement("div",null,r.createElement("span",{className:"relative inline-flex items-center rounded-full border border-gray-400 px-3 py-0.5 text-sm"},null==e?void 0:e.lastName," ",null==e?void 0:e.firstName));(0,so.registerLocale)("ja",Bo.Z);const To=()=>{const{id:e}=(0,A.UO)(),[t,a]=(0,r.useState)(null),[n,l]=(0,r.useState)(!1),{data:{project:o={}}={}}=Lt({variables:{id:e}}),{data:{currentUser:m}={}}=oa(),s=o.editUserId&&o.editUserId!==m.id,[i]=Oe(),{data:{users:c=[]}={}}=ca({variables:{search:{active:!0,isAssesment:!0}}}),{data:{users:d=[]}={}}=ca({variables:{search:{active:!0}}}),{data:{jkkUsers:u=[]}={}}=Rt({variables:{search:{active:!0}}}),p=(0,Ea.TA)({enableReinitialize:!0,initialValues:{orderNumberType:(null==o?void 0:o.orderNumberType)||"",orderNumber:(null==o?void 0:o.orderNumber)||"",name:(null==o?void 0:o.name)||"",jkkUserId:(null==o?void 0:o.jkkUserId)||"",assesmentUserId:(null==o?void 0:o.assesmentUserId)||"",constructionUserId:(null==o?void 0:o.constructionUserId)||"",address:(null==o?void 0:o.address)||"",assessmentDate:null==o?void 0:o.assessmentDate,availableDate:(null==o?void 0:o.availableDate)||"",livingYear:null==o?void 0:o.livingYear,livingMonth:null==o?void 0:o.livingMonth,leaveDate:(null==o?void 0:o.leaveDate)||"",totalPriceInput:null==o?void 0:o.totalPriceInput,orderDate1:(null==o?void 0:o.orderDate1)||"",orderMemo1:(null==o?void 0:o.orderMemo1)||"",orderDate2:(null==o?void 0:o.orderDate2)||"",orderMemo2:(null==o?void 0:o.orderMemo2)||"",orderDate3:(null==o?void 0:o.orderDate3)||"",orderMemo3:(null==o?void 0:o.orderMemo3)||"",closeTypes:(null==o?void 0:o.closeTypes)||[],closeReason:null==o?void 0:o.closeReason,electricStartDate1:(null==o?void 0:o.electricStartDate1)||"",electricEndDate1:(null==o?void 0:o.electricEndDate1)||"",waterStartDate1:(null==o?void 0:o.waterStartDate1)||"",waterEndDate1:(null==o?void 0:o.waterEndDate1)||"",electricStartDate2:(null==o?void 0:o.electricStartDate2)||"",electricEndDate2:(null==o?void 0:o.electricEndDate2)||"",waterStartDate2:(null==o?void 0:o.waterStartDate2)||"",waterEndDate2:(null==o?void 0:o.waterEndDate2)||"",electricStartDate3:(null==o?void 0:o.electricStartDate3)||"",electricEndDate3:(null==o?void 0:o.electricEndDate3)||"",waterStartDate3:(null==o?void 0:o.waterStartDate3)||"",waterEndDate3:(null==o?void 0:o.waterEndDate3)||"",isVermiculite:null==o?void 0:o.isVermiculite,isFinishedVermiculite:null==o?void 0:o.isFinishedVermiculite,vermiculiteDate:(null==o?void 0:o.vermiculiteDate)||"",beforeConstructionImageDate:(null==o?void 0:o.beforeConstructionImageDate)||"",checklistFirstDate:(null==o?void 0:o.checklistFirstDate)||"",checklistLastDate:(null==o?void 0:o.checklistLastDate)||"",constructionStartDate:(null==o?void 0:o.constructionStartDate)||"",constructionEndDate:(null==o?void 0:o.constructionEndDate)||"",userIds:(null==o?void 0:o.userIds)||[]},onSubmit:t=>{i({variables:{id:e,attributes:t}})}}),g=e=>c.find((t=>t.id===e)),b=u.map((e=>({value:e.id,label:`${e.lastName} ${e.firstName}`})));return(0,r.useEffect)((()=>{var e;p.values.totalPriceInput&&a(null==(e=p.values.totalPriceInput)?void 0:e.toLocaleString())}),[p.values.totalPriceInput]),o?r.createElement(No,null,r.createElement(So,null),r.createElement("div",{className:"mt-4"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:p.handleSubmit},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-12"},r.createElement("div",{className:"col-span-12"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示番号"),r.createElement("div",{className:"flex"},r.createElement("input",{type:"text",disabled:s,name:"orderNumberType",className:"block w-16 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.orderNumberType,onChange:p.handleChange,onBlur:()=>p.handleSubmit()}),r.createElement("span",{className:"inline-flex items-center mx-4"},"-"),r.createElement("input",{type:"text",name:"orderNumber",disabled:s,className:"block w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.orderNumber,onChange:p.handleChange,onBlur:()=>p.handleSubmit()}))),r.createElement("div",{className:"col-span-12"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"閉鎖区分"),r.createElement("div",{className:"flex items-start space-x-4"},bo.map((e=>r.createElement("div",{className:"flex items-center h-5",key:`closeTypeValues-${e.value}`},r.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,checked:p.values.closeTypes.includes(e.value),onChange:e=>{const t=`${e.target.value}`;p.values.closeTypes.includes(t)?p.setFieldValue("closeTypes",p.values.closeTypes.filter((e=>e!==t))):p.setFieldValue("closeTypes",[...p.values.closeTypes,t]),p.handleSubmit()},onBlur:p.handleBlur}),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.name))))))),p.values.closeTypes.includes("close_type9")&&r.createElement(r.Fragment,null,r.createElement("div",{className:"col-span-8"},r.createElement("label",null,"閉鎖区分その他内容"),r.createElement("input",{type:"text",name:"closeReason",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.closeReason,onChange:p.handleChange,onBlur:()=>p.handleSubmit()})),r.createElement("div",{className:"col-span-4"})),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"査定日"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.assessmentDate&&new Date(p.values.assessmentDate),onChange:e=>{p.setFieldValue("assessmentDate",mo()(e).format("YYYY-MM-DD")),p.handleSubmit()}}))),r.createElement("div",{className:"col-span-8"}),r.createElement("div",{className:"col-span-12 my-2 border-t border-gray-300"}),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"公社担当者"),r.createElement(ur.ZP,{options:b,className:"px-4 w-72",isClearable:!0,closeMenuOnSelect:!0,value:b.find((e=>e.value===p.values.jkkUserId)),onChange:e=>{p.setFieldValue("jkkUserId",e.value)&&p.handleSubmit()}})),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"当社担当者（査定）"),r.createElement("select",{value:p.values.assesmentUserId,className:"px-4 py-2 border border-gray-300",onChange:e=>p.setFieldValue("assesmentUserId",e.target.value)&&p.handleSubmit()},r.createElement("option",{value:""}),d.map((e=>r.createElement("option",{key:`assesmentUserId-${e.id}`,value:e.id},`${e.lastName} ${e.firstName}`))))),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"当社担当者（工事）"),r.createElement("select",{value:p.values.constructionUserId,className:"px-4 py-2 border border-gray-300",onChange:e=>p.setFieldValue("constructionUserId",e.target.value)&&p.handleSubmit()},r.createElement("option",{value:""}),d.map((e=>r.createElement("option",{key:`constructionUser-${e.id}`,value:e.id},`${e.lastName} ${e.firstName}`))))),r.createElement("div",{className:"col-span-12"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"査定同行者"," ",r.createElement("span",{className:"text-gray-600 bg-gray-200 ml-auto inline-block py-0.5 px-3 text-xs rounded-full"},p.values.userIds.length)),r.createElement("div",{className:"flex"},p.values.userIds.map((e=>r.createElement(Po,{user:g(e),key:`user-${e}`})))),r.createElement("div",{className:"h-40 px-4 py-2 mt-1 overflow-y-scroll border border-gray-300 rounded-md flex flex-wrap gap-4"},c.map((e=>r.createElement("div",{className:"flex items-center h-5",key:`user-${e.id}`},r.createElement("input",{id:e.id,name:`${e.lastName} ${e.firstName}`,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.id,checked:p.values.userIds.includes(e.id),onChange:e=>{const t=`${e.target.value}`;p.values.userIds.includes(t)?p.setFieldValue("userIds",p.values.userIds.filter((e=>e!==t))):p.setFieldValue("userIds",[...p.values.userIds,t]),p.handleSubmit()},onBlur:p.handleBlur}),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},`${e.lastName} ${e.firstName}`))))))),r.createElement("div",{className:"col-span-12 my-6 border-t border-gray-300"}),r.createElement("div",{className:"col-span-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"退去日"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.leaveDate&&new Date(p.values.leaveDate),onChange:e=>p.setFieldValue("leaveDate",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"募集可能日"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.availableDate&&new Date(p.values.availableDate),onChange:e=>p.setFieldValue("availableDate",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"入居年数-年"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement("input",{type:"number",name:"livingYear",min:1,className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.livingYear,onChange:p.handleChange,onBlur:()=>p.handleSubmit()}))),r.createElement("div",{className:"col-span-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"入居年数-ヶ月"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement("select",{value:p.values.livingMonth,className:"px-4 py-1 mt-1 border border-gray-300",onChange:e=>p.setFieldValue("livingMonth",Number(e.target.value))&&p.handleSubmit()},r.createElement("option",{value:""}),Fo().times(11).map((e=>r.createElement("option",{key:`livingMonth-${e}`,value:Number(e+1)},e+1)))))),r.createElement("div",{className:"col-span-8"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"住所"),r.createElement("input",{type:"text",name:"address",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.address,onChange:p.handleChange,onBlur:()=>p.handleSubmit()})),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"工事金額（税込み）"),r.createElement("div",{className:"relative"},r.createElement("div",{className:"absolute inset-y-0 left-0 items-center px-3 pointer-events-none bg-gray-200 text-sm inline-flex"},"¥"),r.createElement("input",{type:n?"number":"text",name:"totalPriceInput",className:"pl-10 block w-full py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:n?p.values.totalPriceInput:t,onChange:p.handleChange,onBlur:()=>{l(!1),p.handleSubmit()},onFocus:()=>{l(!0)}}))),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"工期開始日"),r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.constructionStartDate&&new Date(p.values.constructionStartDate),onChange:e=>p.setFieldValue("constructionStartDate",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()})),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"工期終了日"),r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.constructionEndDate&&new Date(p.values.constructionEndDate),onChange:e=>p.setFieldValue("constructionEndDate",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()})),r.createElement("div",{className:"col-span-4"}),r.createElement("div",{className:"col-span-12 border-t border-gray-300"}),r.createElement("div",{className:"col-span-12 border-t border-gray-300"}),r.createElement("div",{className:"col-span-12"},r.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"指示書発行")),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示書発行日1"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.orderDate1&&new Date(p.values.orderDate1),onChange:e=>p.setFieldValue("orderDate1",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-8"},r.createElement("label",null,"指示書発行メモ1"),r.createElement("input",{type:"text",name:"orderMemo1",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.orderMemo1,onChange:p.handleChange,onBlur:()=>p.handleSubmit()})),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示書発行日2"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.orderDate2&&new Date(p.values.orderDate2),onChange:e=>p.setFieldValue("orderDate2",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-8"},r.createElement("label",null,"指示書発行メモ2"),r.createElement("input",{type:"text",name:"orderMemo2",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.orderMemo2,onChange:p.handleChange,onBlur:()=>p.handleSubmit()})),r.createElement("div",{className:"col-span-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示書発行日3"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:p.values.orderDate3&&new Date(p.values.orderDate3),onChange:e=>p.setFieldValue("orderDate3",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-8"},r.createElement("label",null,"指示書発行メモ3"),r.createElement("input",{type:"text",name:"orderMemo2",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:p.values.orderMemo3,onChange:p.handleChange,onBlur:()=>p.handleSubmit()})),r.createElement("div",{className:"col-span-12 border-t border-gray-300"}),r.createElement("div",{className:"col-span-12"},r.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"電気水道")),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気開始日1"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.electricStartDate1&&new Date(p.values.electricStartDate1),onChange:e=>p.setFieldValue("electricStartDate1",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気停止日1"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.electricEndDate1&&new Date(p.values.electricEndDate1),onChange:e=>p.setFieldValue("electricEndDate1",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道開始日1"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.waterStartDate1&&new Date(p.values.waterStartDate1),onChange:e=>p.setFieldValue("waterStartDate1",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道停止日1"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.waterEndDate1&&new Date(p.values.waterEndDate1),onChange:e=>p.setFieldValue("waterEndDate1",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気開始日2"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.electricStartDate2&&new Date(p.values.electricStartDate2),onChange:e=>p.setFieldValue("electricStartDate2",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気停止日2"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.electricEndDate2&&new Date(p.values.electricEndDate2),onChange:e=>p.setFieldValue("electricEndDate2",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道開始日2"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.waterStartDate2&&new Date(p.values.waterStartDate2),onChange:e=>p.setFieldValue("waterStartDate2",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道停止日2"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.waterEndDate2&&new Date(p.values.waterEndDate2),onChange:e=>p.setFieldValue("waterEndDate2",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気開始日3"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.electricStartDate3&&new Date(p.values.electricStartDate3),onChange:e=>p.setFieldValue("electricStartDate3",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気停止日3"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.electricEndDate3&&new Date(p.values.electricEndDate3),onChange:e=>p.setFieldValue("electricEndDate3",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道開始日3"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.waterStartDate3&&new Date(p.values.waterStartDate3),onChange:e=>p.setFieldValue("waterStartDate3",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-3"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道停止日3"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${p.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:p.values.waterEndDate3&&new Date(p.values.waterEndDate3),onChange:e=>p.setFieldValue("waterEndDate3",mo()(e).format("YYYY-MM-DD"))&&p.handleSubmit()}))),r.createElement("div",{className:"col-span-4"}))))):null};(0,so.registerLocale)("ja",Bo.Z);const Oo=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Lt({variables:{id:e},fetchPolicy:"cache-and-network"}),[a,{loading:n}]=Oe(),l=(0,Ea.TA)({enableReinitialize:!0,initialValues:{beforeConstructionImageDate:(null==t?void 0:t.beforeConstructionImageDate)||"",afterConstructionImageDate:(null==t?void 0:t.afterConstructionImageDate)||"",checklistFirstDate:(null==t?void 0:t.checklistFirstDate)||"",checklistLastDate:(null==t?void 0:t.checklistLastDate)||"",isFaq:null==t?void 0:t.isFaq,isLackImage:null==t?void 0:t.isLackImage,isCompleteImage:null==t?void 0:t.isCompleteImage,submitDocumentDate:null==t?void 0:t.submitDocumentDate},onSubmit:t=>{a({variables:{id:e,attributes:t}})}});return t?r.createElement(No,null,r.createElement(So,null),r.createElement("div",{className:"mt-4"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:l.handleSubmit},r.createElement("div",{className:"grid mt-6 gap-y-6 gap-x-2 grid-cols-5"},r.createElement("div",{className:"col-span-1"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"施工前写真資料・写真確認"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.beforeConstructionImageDate&&new Date(l.values.beforeConstructionImageDate),onChange:e=>l.setFieldValue("beforeConstructionImageDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))),r.createElement("div",{className:"col-span-1"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"初回チェックリスト"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.checklistFirstDate&&new Date(l.values.checklistFirstDate),onChange:e=>l.setFieldValue("checklistFirstDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))),r.createElement("div",{className:"col-span-1"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"最終チェックリスト"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.checklistLastDate&&new Date(l.values.checklistLastDate),onChange:e=>l.setFieldValue("checklistLastDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))),r.createElement("div",{className:"col-span-1 h-5"},r.createElement("label",{htmlFor:"isFaq",className:"block text-sm font-medium text-gray-700"},"質疑回答"),r.createElement("input",{id:"isFaq",name:"isFaq",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:l.values.isFaq,onChange:e=>{l.setFieldValue("isFaq",e.target.checked)},onBlur:()=>l.handleSubmit()}))),r.createElement("div",{className:"grid mt-6 gap-y-6 gap-x-2 grid-cols-5"},r.createElement("div",{className:"col-span-1"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"施工後写真資料"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.afterConstructionImageDate&&new Date(l.values.afterConstructionImageDate),onChange:e=>l.setFieldValue("afterConstructionImageDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))),r.createElement("div",{className:"h-5"},r.createElement("label",{htmlFor:"isLackImage",className:"block text-sm font-medium text-gray-700"},"不足写真あり"),r.createElement("input",{id:"isLackImage",name:"isLackImage",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:l.values.isLackImage,disabled:l.values.isCompleteImage,onChange:e=>{l.setFieldValue("isLackImage",e.target.checked)&&l.handleSubmit()},onBlur:l.handleBlur})),r.createElement("div",{className:"h-5"},r.createElement("label",{htmlFor:"isCompleteImage",className:"block text-sm font-medium text-gray-700"},"写真完了"),r.createElement("input",{id:"isCompleteImage",name:"isCompleteImage",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:l.values.isCompleteImage,disabled:l.values.isLackImage,onChange:e=>{l.setFieldValue("isCompleteImage",e.target.checked)&&l.handleSubmit()},onBlur:l.handleBlur}))),r.createElement("div",{className:"grid mt-6 gap-y-6 gap-x-2 grid-cols-5"},r.createElement("div",{className:"col-span-1"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"請求書類提出日"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.submitDocumentDate&&new Date(l.values.submitDocumentDate),onChange:e=>l.setFieldValue("submitDocumentDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))))))):null};(0,so.registerLocale)("ja",Bo.Z);const Mo=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Lt({variables:{id:e}}),[a,{loading:n}]=Oe(),l=(0,Ea.TA)({enableReinitialize:!0,initialValues:{isVermiculite:null==t?void 0:t.isVermiculite,vermiculiteDate:(null==t?void 0:t.vermiculiteDate)||"",isManagerAsbestosDocument1:null==t?void 0:t.isManagerAsbestosDocument1,isManagerAsbestosDocument2:null==t?void 0:t.isManagerAsbestosDocument2,isManagerAsbestosDocument3:null==t?void 0:t.isManagerAsbestosDocument3,isOfficeworkAsbestosDocument1:null==t?void 0:t.isOfficeworkAsbestosDocument1,isOfficeworkAsbestosDocument2:null==t?void 0:t.isOfficeworkAsbestosDocument2,isOfficeworkAsbestosDocument3:null==t?void 0:t.isOfficeworkAsbestosDocument3,processCreatedDate:(null==t?void 0:t.processCreatedDate)||""},onSubmit:t=>{a({variables:{id:e,attributes:t}})}}),o=[{label:"石綿事前調査報告書（公社提出用）",value:"isManagerAsbestosDocument1",check:l.values.isManagerAsbestosDocument1},{label:"石綿事前調査報告書（電子申請用）",value:"isManagerAsbestosDocument2",check:l.values.isManagerAsbestosDocument2},{label:"墨出し・ガス漏報告書",value:"isManagerAsbestosDocument3",check:l.values.isManagerAsbestosDocument3}],m=[{label:"石綿事前調査報告書（公社提出用）",value:"isOfficeworkAsbestosDocument1",check:l.values.isOfficeworkAsbestosDocument1},{label:"石綿事前調査報告書（電子申請用）",value:"isOfficeworkAsbestosDocument2",check:l.values.isOfficeworkAsbestosDocument2},{label:"墨出し・ガス漏報告書",value:"isOfficeworkAsbestosDocument3",check:l.values.isOfficeworkAsbestosDocument3}];return t?r.createElement(No,null,r.createElement(So,null),r.createElement("div",{className:"mt-4"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:l.handleSubmit},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-2 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"col-span-6"},r.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"蛭石調査")),r.createElement("div",{className:"col-span-1"},r.createElement("div",{className:"flex space-x-6"},r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"isVermiculiteTrue",name:"isVermiculite",type:"radio",checked:l.values.isVermiculite,className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",onChange:()=>l.setFieldValue("isVermiculite",!0)&&l.handleSubmit()}),r.createElement("label",{htmlFor:"isVermiculiteTrue",className:"block ml-3 text-sm font-medium text-gray-700 cursor-pointer"},"有")),r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"isVermiculiteFalse",name:"isVermiculite",type:"radio",checked:!l.values.isVermiculite,className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",onChange:()=>l.setFieldValue("isVermiculite",!1)&&l.handleSubmit()}),r.createElement("label",{htmlFor:"isVermiculiteFalse",className:"block ml-3 text-sm font-medium text-gray-700 cursor-pointer"},"無")))),r.createElement("div",{className:"col-span-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"蛭石調査完了日"),r.createElement("div",{className:"rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.vermiculiteDate&&new Date(l.values.vermiculiteDate),onChange:e=>l.setFieldValue("vermiculiteDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))),r.createElement("div",{className:"col-span-6 mt-10"},r.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"石綿調査報告管理")),r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"監督"),r.createElement("div",{className:"flex col-span-6 space-x-8"},o.map((e=>r.createElement("div",{className:"flex items-center h-5"},r.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,checked:e.check,onChange:t=>{l.setFieldValue(e.value,t.target.checked)&&l.handleSubmit()},onBlur:l.handleBlur}),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.label)))))),r.createElement("label",{className:"block mt-6 text-sm font-medium text-gray-700"},"事務"),r.createElement("div",{className:"flex col-span-6 space-x-8"},m.map((e=>r.createElement("div",{className:"flex items-center h-5"},r.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,checked:e.check,onChange:t=>{l.setFieldValue(e.value,t.target.checked)&&l.handleSubmit()},onBlur:l.handleBlur}),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.label)))))),r.createElement("div",{className:"col-span-2 mt-10"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"工程表作成日"),r.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},r.createElement(io(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:l.values.processCreatedDate&&new Date(l.values.processCreatedDate),onChange:e=>l.setFieldValue("processCreatedDate",mo()(e).format("YYYY-MM-DD"))&&l.handleSubmit()}))))))):null},Uo=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/projects/:id/form/base"},r.createElement(To,null)),r.createElement(A.AW,{path:"/projects/:id/form/docs"},r.createElement(Oo,null)),r.createElement(A.AW,{path:"/projects/:id/form/management"},r.createElement(Mo,null))),Ro=({projectImage:e,item:t})=>{const[a,{loading:n}]=function(e){const t=B(B({},k),void 0);return x.D(Le,t)}();return r.createElement("tr",{className:"bg-white",key:null==t?void 0:t.id},r.createElement("td",{className:"w-6 m-auto"},!n&&r.createElement(Za,{className:"text-blue-700 cursor-pointer",onClick:()=>{a({variables:{id:e.id,itemId:t.id}})}}),n&&r.createElement(ba,{className:"w-3 h-3"})),r.createElement("td",{className:"w-20 px-4 text-xs"},t.itemCode.code),r.createElement("td",{className:"flex-1 px-4 text-xs"},t.itemCode.name))},Vo=({imageComment:e})=>{const[t]=function(e){const t=B(B({},k),void 0);return x.D(Ke,t)}(),a=(0,Ea.TA)({initialValues:{text:e.text},onSubmit:a=>{var r;r=a,t({variables:{id:e.id,attributes:r}})}});return r.createElement("form",{onSubmit:a.handleSubmit},r.createElement("input",{type:"text",name:"text",value:a.values.text,onChange:a.handleChange,onBlur:()=>a.handleSubmit(),className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"}))},Yo=({project:e,projectImage:t})=>{var a,n;const{data:{roomType:l=null}={}}=ea({variables:{id:t.roomTypeId}}),{data:{allItems:o=[]}={}}=Pt({variables:{search:{year:String(e.year.year)}}}),[m]=function(e){const t=B(B({},k),void 0);return x.D(Ze,t)}();return r.createElement("div",null,r.createElement("div",{style:{height:"240px"}},r.createElement("table",{className:"w-full divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",{className:"flex text-center"},r.createElement("th",{className:"w-4"}),r.createElement("th",{className:"w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コード"),r.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"名称"))),r.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200",style:{height:"200px"}},null==(a=(()=>{const a=e.projectItems.filter((e=>e.roomTypeIds.includes(t.roomTypeId))).map((e=>e.code)),r=o.filter((e=>a.includes(e.itemCode.code)));return console.log("🚀 ~ file: Comment.tsx:56 ~  filtertems",r),r})())?void 0:a.map(((e,a)=>r.createElement(Ro,{projectImage:t,item:e,key:`item-${a}-${e.id}`})))))),r.createElement("table",{className:"w-full divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",{className:"flex text-center"},r.createElement("th",{className:"w-4"}),r.createElement("th",{className:"w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コード"),r.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"名称"))),r.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200"},null==(n=null==t?void 0:t.imageComments)?void 0:n.map((e=>r.createElement("tr",{className:"flex bg-white",key:null==e?void 0:e.id},r.createElement("td",{className:"w-4 m-auto"},r.createElement(_a,{className:"text-blue-700 cursor-pointer",onClick:()=>(e=>{m({variables:{id:e.id}})})(e)})),r.createElement("td",{className:"w-20 px-4 my-auto text-xs"},e.code),r.createElement("td",{className:"flex-1 px-4 text-xs"},r.createElement(Vo,{imageComment:e}))))))))},qo={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"600px"},overlay:{zIndex:"10"}},Lo=({project:e,projectImage:t,onClose:a})=>{var n;const[l]=Qe(),[o,m]=(0,r.useState)(!1),s=(0,Ea.TA)({enableReinitialize:!0,initialValues:{comment:null==t?void 0:t.comment},onSubmit:e=>{t&&l({variables:{id:t.id,attributes:e}}),a()}});return r.createElement("form",{onSubmit:s.handleSubmit,className:"h-44"},r.createElement("button",{type:"button",onClick:()=>m(!0),className:"block px-2 py-1 text-xs text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm ont-medium hover:bg-gray-50"},"編集"),r.createElement(g(),{isOpen:o,onRequestClose:()=>{m(!1)},style:qo},r.createElement(Yo,{projectImage:t,project:e})),r.createElement("table",{className:"w-full border border-gray-200 divide-y divide-gray-200"},r.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-400",style:{height:"180px"}},null==(n=null==t?void 0:t.imageComments)?void 0:n.map((e=>r.createElement("tr",{className:"flex bg-white",key:null==e?void 0:e.id},r.createElement("td",{className:"flex-1 px-2 py-1 text-xs"},e.text)))))))},_o={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"800px",height:"680px"},overlay:{zIndex:"20"}},zo=e=>r.createElement(g(),{isOpen:e.isOpen,onRequestClose:e.onClose,style:_o},r.createElement("div",null,r.createElement("img",{className:"max-h-[560px]",src:e.image}),r.createElement("div",{className:"flex justify-end items-center mt-10"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm",onClick:()=>e.onClose()},"閉じる")))),Wo=({project:e,id:t,projectImage:a,roomTypeId:n,imageType:l,index:o,moveImage:m,order:s})=>{const[i,c]=(0,r.useState)(null),d=(0,r.useRef)(null),u=(0,r.useRef)(null),[A,p]=(0,r.useState)(!1),[g]=Qe(),[b]=Ge(),y=(e,t)=>{let r=null;r="before"===t?{imageBefore:e}:{imageAfter:e},g({variables:{id:a.id,attributes:r}})},x=(0,r.useRef)(null),[{handlerId:f},E]=(0,nl.L)({accept:"Image",collect:e=>({handlerId:e.getHandlerId()}),hover(e,t){var a;if(!x.current)return;const r=e.index,n=o;if(r===n)return;const l=null==(a=x.current)?void 0:a.getBoundingClientRect(),s=(l.bottom-l.top)/2,i=t.getClientOffset().y-l.top;r<n&&i<s||r>n&&i>s||(m(e.id,r,n),e.index=n)}}),[{isDragging:h},v]=(0,ll.c)({type:"Image",item:()=>({id:t,index:o}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: Image.tsx ~ line 147 ~ Image ~ item",e),s(e.id,e.index)}}),w=h?0:1;return v(E(x)),r.createElement("div",{className:"flex mt-4 h-72",ref:x,style:{opacity:w},"data-handler-id":f},r.createElement("div",{className:"w-8 my-auto"},r.createElement(Ya,{className:"cursor-pointer w-8"})),r.createElement("div",{className:"flex p-4 border border-gray-300 w-96"},r.createElement("div",{className:"w-64 h-64"},(null==a?void 0:a.imageBefore)&&r.createElement("img",{className:"h-full w-full object-contain cursor-pointer",src:null==a?void 0:a.imageBefore,onClick:()=>c(a.imageBefore)})),r.createElement("div",{className:"mt-2 ml-4"},r.createElement("button",{type:"button",className:"block px-4 py-2 ml-auto text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>d.current.click()},"登録"),r.createElement("button",{type:"button",className:"block px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{y(null,"before")}},"削除"),r.createElement("input",{id:"fileBefore",name:"fileBefore",type:"file",accept:".jpg,.gif,.png,image/gif,image/jpeg,image/png",ref:d,style:{display:"none"},onChange:e=>{(e=>{const t=e.target.files[0];t&&y(t,"before")})(e)}}))),r.createElement("div",{className:"flex p-4 border border-gray-300 w-96"},r.createElement("div",{className:"w-64 h-64"},(null==a?void 0:a.imageAfter)&&r.createElement("img",{className:"h-full w-full object-contain cursor-pointer",src:null==a?void 0:a.imageAfter,onClick:()=>c(a.imageAfter)})),r.createElement("div",{className:"mt-2 ml-4"},r.createElement("button",{type:"button",className:"block px-4 py-2 ml-auto text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>u.current.click()},"登録"),r.createElement("button",{type:"button",className:"block px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{y(null,"after")}},"削除"),r.createElement("input",{id:"fileAfter",name:"fileAfter",type:"file",accept:".jpg,.gif,.png,image/gif,image/jpeg,image/png",ref:u,style:{display:"none"},onChange:e=>{e.preventDefault(),(e=>{const t=e.target.files[0];t&&y(t,"after")})(e)}}))),r.createElement("div",{className:"p-4 align-top border border-gray-300 w-72",onClick:()=>p(!0)},!A&&r.createElement("div",{className:"whitespace-pre"},null==a?void 0:a.comment),r.createElement(Lo,{project:e,projectImage:a,onClose:()=>p(!1)})),r.createElement(Ka,{className:"w-4 ml-2 text-red-500 cursor-pointer",onClick:()=>{b({variables:{id:t}})}}),r.createElement(zo,{isOpen:!!i,image:i,onClose:()=>c(null)}))},Zo=({project:e,projectImages:t,roomTypeId:a})=>{const[n,l]=(0,r.useState)([]),[o]=function(e){const t=B(B({},k),void 0);return x.D(Xe,t)}(),m=(e,t,a)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${t}`),console.log(`update: hoverIndex: ${a}`),l((e=>tl()(e,{$splice:[[t,1],[a,0,e[t]]]})))},s=(e,t)=>{o({variables:{id:e,attributes:{position:t}}})};return(0,r.useEffect)((()=>{l(t)}),[t]),r.createElement("div",null,r.createElement(al.W,{backend:rl.PD},n.map(((t,n)=>r.createElement(Wo,{index:n,id:t.id,key:t.id,project:e,projectImage:t,roomTypeId:a,imageType:t.imageType,moveImage:m,order:s})))))},Ho=({project:e,roomTypeId:t})=>{const[a,n]=(0,r.useState)(1),[l,{loading:o}]=ze(),m=a=>{const r=e.projectImages.filter((e=>e.imageType===a&&e.roomTypeId===t));return Fo().sortBy(r,["position"])},s=[1,2,3,4,5,10,20,30];return r.createElement("div",null,[{name:"壁1",value:"wall1"},{name:"壁2",value:"wall2"},{name:"壁3",value:"wall3"},{name:"壁4",value:"wall4"},{name:"床",value:"floor"},{name:"天井",value:"roof"}].map((i=>r.createElement("div",{className:"mt-10 w-[1100px]",key:i.value},r.createElement("h3",{className:"text-lg font-bold"},i.name),r.createElement(Zo,{project:e,roomTypeId:t,projectImages:m(i.value)}),r.createElement("div",{className:"inline-flex mt-4"},r.createElement("select",{name:"location",className:"block py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",defaultValue:a,onChange:e=>n(Number(e.target.value))},s.map((e=>r.createElement("option",{key:e},e)))),r.createElement(ga,{type:"button",className:"block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>(r=>{l({variables:{count:a,attributes:{projectId:e.id,roomTypeId:t,imageType:r}}})})(i.value),loading:o},"追加"))))))},Go=()=>{var e,t;const{id:a,roomTypeId:n}=(0,A.UO)(),{data:{project:l=null}={}}=Lt({variables:{id:a},fetchPolicy:"cache-and-network"}),[o]=function(e){const t=B(B({},k),e);return x.D(Ie,t)}({onCompleted:({createProjectImageReport:{project:e}})=>window.location.href=e.imageReportUrl});return l?(console.log("🚀 ~ file: ImageBase.tsx ~ line 16 ~ Image ~ project",l),r.createElement(No,null,l.imageReportUrl&&r.createElement("a",{className:"block mt-6",onClick:()=>{o({variables:{id:a}})}},r.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},r.createElement(Ua,{className:"flex-shrink-0 w-5 h-5"}),r.createElement("span",null,"Excelレポート"))),r.createElement("nav",{className:"flex space-x-4","aria-label":"Tabs"},r.createElement(u.rU,{to:`/projects/${a}/images`},r.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-800 bg-gray-200 rounded-md hover:text-gray-800 "},"塗装前")),r.createElement(u.rU,{to:`/projects/${a}/images/other`},r.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-800"},"塗装以降"))),r.createElement("div",{className:"mt-6"},r.createElement("nav",{className:"flex space-x-1"},null==(t=null==(e=l.room)?void 0:e.roomTypes)?void 0:t.map((e=>r.createElement(u.rU,{key:e.id,to:`/projects/${a}/images/${e.id}`,className:"text-gray-500 hover:text-gray-700 w-24 border border-gray-300  bg-white py-2 text-sm font-medium text-center hover:bg-gray-200 rounded-sm "+(e.id===n?"bg-gray-200 border border-gray-900":"")},r.createElement("span",null,e.name),r.createElement("span",{"aria-hidden":"true",className:"bg-transparent absolute inset-x-0 bottom-0 h-0.5"})))))),n&&r.createElement(Ho,{project:l,roomTypeId:n}))):null},Ko=({projectImage:e,comment:t})=>{const[a,{loading:n}]=function(e){const t=B(B({},k),void 0);return x.D(qe,t)}(),[l]=function(e){const t=B(B({},k),void 0);return x.D(We,t)}(),o=t=>e.comments.findIndex((e=>e.id===t.id))>-1;return r.createElement("tr",{className:`bg-white ${o(t)&&"bg-gray-200"}`,key:null==t?void 0:t.id},r.createElement("td",{className:"w-6 m-auto"},!n&&!o(t)&&r.createElement(Za,{className:"text-blue-700 cursor-pointer",onClick:()=>{return r=t.id,void a({variables:{id:e.id,commentId:r}});var r}}),!n&&o(t)&&r.createElement(_a,{className:"text-xs text-blue-700 cursor-pointer",onClick:()=>{return a=t.id,void l({variables:{id:e.id,commentId:a}});var a}}),n&&r.createElement(ba,{className:"w-3 h-2"})),r.createElement("td",{className:"flex-1 px-4 text-xs"},t.comment))},Jo=({projectImage:e})=>{const[t,a]=(0,r.useState)(null),{data:{comments:n=[]}={}}=wt({variables:{search:{commentTypeId:t}},skip:!t}),{data:{commentTypes:l=[]}={}}=kt();return r.createElement("div",{style:{height:"240px"}},r.createElement("div",{className:"flex flex-wrap space-x-2 space-y-1"},l.map((e=>r.createElement("div",{key:e.id,className:`inline-flex items-center rounded-md bg-gray-100 px-2.5 py-0.5 text-sm font-medium cursor-pointer hover:bg-blue-100 ${t===e.id&&"bg-blue-300"}`,onClick:()=>a(e.id)},e.name)))),r.createElement("table",{className:"w-full divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",{className:"flex text-center"},r.createElement("th",{className:"w-4"}),r.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コメント"))),r.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200"},null==n?void 0:n.map((t=>r.createElement(Ko,{projectImage:e,comment:t,key:`comment-${t.id}`}))))))},Qo={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"600px"},overlay:{zIndex:"10"}},Xo=({project:e,projectImage:t,onClose:a})=>{var n;const[l]=Qe(),[o,m]=(0,r.useState)(!1),s=(0,Ea.TA)({enableReinitialize:!0,initialValues:{comment:null==t?void 0:t.comment},onSubmit:e=>{t&&l({variables:{id:t.id,attributes:e}}),a()}});return r.createElement("form",{onSubmit:s.handleSubmit},r.createElement("button",{type:"button",onClick:()=>m(!0),className:"block h-8 px-2 py-1 text-xs text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm ont-medium hover:bg-gray-50"},"編集"),r.createElement(g(),{isOpen:o,onRequestClose:()=>{m(!1)},style:Qo},r.createElement(Jo,{projectImage:t})),r.createElement("table",{className:"w-full border border-gray-200 divide-y divide-gray-200"},r.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-400",style:{height:"180px"}},null==(n=null==t?void 0:t.comments)?void 0:n.map((e=>r.createElement("tr",{className:"flex bg-white",key:null==e?void 0:e.id},r.createElement("td",{className:"flex-1 px-1 py-1 text-xs"},e.comment)))))))},em=({project:e,projectImage:t})=>{const[a,n]=(0,r.useState)(null),l=(0,r.useRef)(null),[o]=Qe(),[m]=Ge();return r.createElement("div",{className:"mt-4"},r.createElement("div",{className:"text-gray-500 text-sm"},"#",t.position),r.createElement("div",{className:"flex h-72"},r.createElement("div",{className:"flex p-4 border border-gray-300 w-96"},r.createElement("div",{className:"w-64 h-64"},(null==t?void 0:t.image)&&r.createElement("img",{className:"h-full w-full object-contain cursor-pointer",src:null==t?void 0:t.image,onClick:()=>n(t.image)})),r.createElement("div",{className:"mt-2 ml-2"},r.createElement("button",{type:"button",className:"block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>l.current.click()},"登録"),r.createElement("button",{type:"button",className:"block px-2 py-2 text-xs font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{m({variables:{id:t.id}})}},"削除"),r.createElement("input",{id:"file",name:"file",type:"file",accept:".jpg,.gif,.png,image/gif,image/jpeg,image/png",ref:l,style:{display:"none"},onChange:e=>{(e=>{const a=e.target.files[0];a&&(e=>{let a=null;a={image:e},o({variables:{id:t.id,attributes:a}})})(a)})(e)}}))),r.createElement("div",{className:"px-2 align-top border border-gray-300 w-44"},r.createElement(Xo,{project:e,projectImage:t,onClose:()=>{}}))),r.createElement(zo,{isOpen:!!a,image:a,onClose:()=>n(null)}))},tm=({project:e,projectImages:t})=>{const[a,n]=(0,r.useState)([]);return(0,r.useEffect)((()=>{n(t)}),[t]),r.createElement("div",{className:"flex flex-wrap w-full gap-2"},a.map((t=>r.createElement(em,{key:t.id,project:e,projectImage:t}))))},am=({project:e})=>{const[t,a]=(0,r.useState)(1),[n,{loading:l}]=ze();return r.createElement("div",null,r.createElement("div",{className:"mt-10"},r.createElement(tm,{project:e,projectImages:(()=>{const t=e.projectImages.filter((e=>"other"===e.imageType));return Fo().sortBy(t,["position"])})()}),r.createElement("div",{className:"inline-flex mt-4"},r.createElement("select",{name:"location",className:"block py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",defaultValue:t,onChange:e=>a(Number(e.target.value))},r.createElement("option",null,"1"),r.createElement("option",null,"5"),r.createElement("option",null,"10"),r.createElement("option",null,"20"),r.createElement("option",null,"30")),r.createElement(ga,{type:"button",className:"block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{n({variables:{count:t,attributes:{projectId:e.id,imageType:"other"}}})},loading:l},"追加"))))},rm=()=>{const{id:e,roomTypeId:t}=(0,A.UO)(),{data:{project:a=null}={}}=Lt({variables:{id:e},fetchPolicy:"cache-and-network"});return a?r.createElement(No,null,a.imageReportUrl&&r.createElement("a",{href:a.imageReportUrl,className:"block mt-6"},r.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},r.createElement(Ua,{className:"flex-shrink-0 w-5 h-5"}),r.createElement("span",null,"Excelレポート"))),r.createElement("nav",{className:"flex space-x-4","aria-label":"Tabs"},r.createElement(u.rU,{to:`/projects/${e}/images`},r.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-800"},"塗装前")),r.createElement(u.rU,{to:`/projects/${e}/images/other`},r.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-800 bg-gray-200 rounded-md hover:text-gray-800 "},"塗装以降"))),r.createElement(am,{project:a})):null},nm=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/projects/:id/images/other"},r.createElement(rm,null)),r.createElement(A.AW,{path:"/projects/:id/images/:roomTypeId"},r.createElement(Go,null)),r.createElement(A.AW,{path:"/projects/:id/images"},r.createElement(Go,null))),lm=({companyType:e,setCompany:t})=>{const[a,n]=(0,r.useState)(null);return r.createElement("tr",null,r.createElement("td",{className:"whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6"},e.name),r.createElement("td",{className:"whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6"},r.createElement("select",{value:a,onChange:a=>{var r;n(a.target.value),r=a.target.value,t(e.id,r)},className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},r.createElement("option",{value:""}),e.companies.map((e=>r.createElement("option",{value:e.id},e.name))))))},om={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"800px"},overlay:{zIndex:"10"}},mm=({isOpen:e,project:t,handleClose:a})=>{const[n,l]=(0,r.useState)([]),{data:{companyTypes:o=[]}={}}=Ft(),[m,{loading:s}]=function(e){const t=B(B({},k),void 0);return x.D(Ue,t)}(),i=(e,t)=>{const a=n.filter((t=>t.companyTypeId!==e));(t||0!==t.length)&&a.push({companyTypeId:e,companyId:t}),l(a)};return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:e,onRequestClose:a,style:om},r.createElement("div",{className:""},r.createElement("table",{className:"min-w-full divide-y divide-gray-300"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6"},"業者種別"),r.createElement("th",{scope:"col",className:"px-3 py-3.5 text-left text-sm font-semibold text-gray-900"},"業者"))),r.createElement("tbody",{className:"divide-y divide-gray-200 bg-white"},o.map((e=>r.createElement(lm,{companyType:e,key:e.id,setCompany:i}))))),r.createElement("div",{className:"mt-10 text-right"},r.createElement(ga,{type:"button",className:"px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{for(const e of n)m({variables:{id:t.id,companyId:e.companyId,companyTypeId:e.companyTypeId}});a()},disabled:0===n.length,loading:s},"更新")))))},sm=({setKeyword:e})=>{const t=(0,Ea.TA)({initialValues:{keyword:""},onSubmit:t=>{e(t.keyword)}});return r.createElement("form",{onSubmit:t.handleSubmit},r.createElement("div",{className:"relative mt-1 rounded-md shadow-sm"},r.createElement("div",{className:"absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none"},r.createElement("svg",{xmlns:"http://www.w3.org/2000/svg",className:"w-4 h-4 text-gray-400",fill:"none",viewBox:"0 0 24 24",stroke:"currentColor","stroke-width":"2"},r.createElement("path",{"stroke-linecap":"round","stroke-linejoin":"round",d:"M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"}))),r.createElement("input",{type:"text",name:"keyword",className:"block w-full px-4 py-1 pl-10 mt-1 border border-gray-400 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:t.values.keyword,onChange:t.handleChange,onBlur:()=>t.handleSubmit()})))},im={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"800px"},overlay:{zIndex:"10"}},cm=({projectItem:e,project:t,isOpen:a,handleClose:n,refetch:l})=>{const[o,m]=(0,r.useState)(null),{data:{allItems:s=[]}={},loading:i}=Pt({variables:{search:{year:`${t.year.year}`,keyword:o}},skip:!o}),[c,{loading:d}]=function(e){const t=B(B({},k),e);return x.D(De,t)}({onCompleted:()=>l()}),u=(0,Ea.TA)({initialValues:{itemId:(null==e?void 0:e.itemId)||"",code:(null==e?void 0:e.code)||"",name:(null==e?void 0:e.name)||"",unitPrice:null==e?void 0:e.unitPrice,count:null==e?void 0:e.count},onSubmit:e=>{c({variables:{id:t.id,attributes:e}}),u.resetForm(),n()}}),A=e=>{u.setFieldValue("itemId",e.id),u.setFieldValue("code",`${e.itemCode.code}`),u.setFieldValue("name",e.name),u.setFieldValue("unitPrice",e.price)};return r.createElement("div",{className:"w-screen max-w-xl"},r.createElement(g(),{isOpen:a,onRequestClose:n,style:im},r.createElement("div",{className:""},r.createElement(sm,{setKeyword:m}),r.createElement("table",{className:"w-full mt-4 border border-gray-300 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",{className:"flex text-center"},r.createElement("th",{className:"w-4"}),r.createElement("th",{className:"w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コード"),r.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"名称"),r.createElement("th",null))),r.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200",style:{height:"300px"}},i&&r.createElement("div",{className:"mx-auto"},r.createElement(ba,null)),!i&&s.map((e=>r.createElement("tr",{className:`flex w-full text-left bg-white px-2 py-2 cursor-pointer hover:bg-gray-100 ${u.values.itemId===e.id&&"bg-indigo-100"}`,key:null==e?void 0:e.id,onClick:()=>A(e)},r.createElement("td",{className:"w-4 m-auto"},r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"itemId",name:"itemId",type:"radio",value:e.id,checked:u.values.itemId===e.id,className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",onChange:t=>{A(e)}}))),r.createElement("td",{className:"w-28 px-4"},e.itemCode.code),r.createElement("td",{className:"flex-1 px-4 my-auto ml-2 text-sm"},e.name))))))),r.createElement("form",{onSubmit:u.handleSubmit},r.createElement("div",{className:"mt-4"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"コード"),r.createElement("input",{type:"text",name:"code",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.code,onChange:u.handleChange,onBlur:u.handleBlur,disabled:!0})),r.createElement("div",{className:"mt-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.name,onChange:u.handleChange,onBlur:u.handleBlur})),r.createElement("div",{className:"mt-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"単価"),r.createElement("input",{type:"number",name:"unitPrice",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.unitPrice,onChange:u.handleChange,onBlur:u.handleBlur})),r.createElement("div",{className:"mt-2"},r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"数量"),r.createElement("input",{type:"number",name:"count",step:"0.1",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.count,onChange:u.handleChange,onBlur:u.handleBlur})),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:d||!u.isValid||!u.dirty,loading:d},"登録")))))},dm=({project:e,projectItem:t,onCopy:a,refetch:n})=>{var l,o,m,s,i,c,d,u,A,p;const{data:{years:g}={}}=ua(),[b]=function(e){const t=B(B({},k),void 0);return x.D(Me,t)}(),[y]=function(e){const t=B(B({},k),e);return x.D(Pe,t)}({onCompleted:()=>n()});null==(o=null==(l=e.room)?void 0:l.roomTypes)||o.map((e=>({value:e.id,label:e.name})));let f=[];f=t.selectableCompanies.map((e=>({value:e.id,label:e.name}))),t.company&&t.selectableCompanies.includes(t.company)&&f.push({value:t.company.id,label:t.company.name});const E=(0,Ea.TA)({enableReinitialize:!0,initialValues:{name:t.name,count:t.count||0,unitPrice:t.unitPrice||0,amount:t.amount||0,roomTypeIds:t.roomTypeIds||[],companyId:t.companyId},onSubmit:e=>{b({variables:{id:t.id,attributes:e}})}}),h=()=>{let e=!1;return["OM","SM"].forEach((a=>{t.code.indexOf(a)>-1&&(e=!0)})),e};return g?r.createElement("form",{onSubmit:E.handleSubmit},r.createElement("div",{className:"flex"},r.createElement("div",{className:"w-10 text-gray-600 text-sm flex items-center px-4"},"#",t.sequenceNumber),r.createElement("div",{className:"px-1 m-auto w-20"},r.createElement("button",{type:"button",className:"block w-full px-2 py-1 text-xs font-medium text-white bg-indigo-600 border border-transparent rounded shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 whitespace-nowrap",onClick:()=>{a(t)}},"コピー"),r.createElement("button",{type:"button",className:"block w-full px-2 py-1 mx-auto mt-1 text-xs font-medium border rounded shadow-sm border-rose-600 text-rose-600 focus:outline-none focus:ring-2 focus:ring-offset-2 hover:bg-rose-50",onClick:()=>{y({variables:{id:t.id}})}},"削除")),r.createElement("div",{className:"w-64 px-2 py-4"},r.createElement("div",{className:"text-xs font-medium text-gray-900"},t.code),r.createElement("div",{className:"text-xs text-gray-500"},r.createElement("input",{type:"text",name:"name",className:"block w-full px-2 py-1 mt-1 border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs",value:E.values.name,disabled:!h(),onChange:E.handleChange,onBlur:()=>E.handleSubmit()}))),r.createElement("div",{className:"w-36 px-2 py-4 flex"},r.createElement("div",null,r.createElement("input",{name:"count",type:"number",step:"0.1",value:E.values.count,className:"w-16 px-1 py-1 mt-1 text-xs text-right border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500",onChange:E.handleChange,onBlur:()=>E.handleSubmit()})),r.createElement("div",null,r.createElement("input",{name:"unitPrice",type:"number",value:E.values.unitPrice,className:"w-16 px-1 py-1 mt-1 text-xs text-right border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500",onChange:E.handleChange,disabled:!h(),onBlur:()=>E.handleSubmit()}))),r.createElement("div",{className:"w-16 px-2 py-4"},r.createElement("div",{className:"text-xs leading-8 text-right text-gray-500"},null==(m=t.amount)?void 0:m.toLocaleString(),"円")),r.createElement("div",{className:"w-32 px-4 flex flex-col justify-center"},r.createElement("div",{className:"text-sm"},null==(i=null==(s=t.company)?void 0:s.companyTypes)?void 0:i.map((e=>e.name)).join("/")),r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"text-xs  border border-gray-400 px-2 py-1 rounded-sm "+(t.item.order?"bg-gray-600 text-white":"text-gray-500")},(null==(c=t.item)?void 0:c.order)?"発注対象":"発注なし"),E.values.companyId&&r.createElement(Fa,{className:"w-6 text-green-700"}))),r.createElement("div",{className:"flex-1 px-2 py-4 text-xs leading-8 text-gray-500"},r.createElement("select",{value:E.values.companyId,onChange:e=>{return t=e.target.value,E.setFieldValue("companyId",t),void E.submitForm();var t},className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},r.createElement("option",{value:""}),f.map((e=>r.createElement("option",{value:e.value},e.label)))),r.createElement("div",{className:"flex items-center mt-2 flex-wrap gap-2"},null==(u=null==(d=e.room)?void 0:d.roomTypes)?void 0:u.map((e=>r.createElement("div",{className:"flex items-center h-5",key:`${t.id}-roomType-${e.id}`},r.createElement("input",{id:`${t.id}-roomType-${e.id}`,type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:E.values.roomTypeIds.includes(e.id),onChange:t=>{var a;(null==(a=E.values.roomTypeIds)?void 0:a.includes(e.id))?E.setFieldValue("roomTypeIds",E.values.roomTypeIds.filter((t=>t!==e.id))):E.setFieldValue("roomTypeIds",[...E.values.roomTypeIds,e.id]),E.handleSubmit()},onBlur:E.handleBlur}),r.createElement("div",{className:"ml-1 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},e.name)))))),r.createElement("div",{className:"flex text-sm text-gray-500 mt-4 space-x-4"},r.createElement("div",null,"最終更新日時：",t.updatedItemsAt&&mo()(t.updatedItemsAt).format("YYYY/MM/DD HH:mm")),r.createElement("div",null,"最終更新ユーザー：",null==(A=t.updatedItemsUser)?void 0:A.lastName,null==(p=t.updatedItemsUser)?void 0:p.firstName))))):null},um=({project:e})=>{const{data:{years:t}={}}=ua(),[a]=function(e){const t=B(B({},k),void 0);return x.D(Ye,t)}(),n=(0,Ea.TA)({initialValues:{yearId:e.yearId},onSubmit:t=>{a({variables:{id:e.id,yearId:t.yearId}})}});return t?r.createElement("form",{onSubmit:n.handleSubmit},r.createElement("div",{className:"w-20 py-1 pl-1"},r.createElement("select",{value:n.values.yearId,onChange:t=>{return r=t.target.value,n.setFieldValue("yearId",r),void a({variables:{id:e.id,yearId:r}});var r},className:"block w-full py-2 pl-1 mt-1 text-xs border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500"},t.map((e=>r.createElement("option",{value:e.id},e.year)))))):null},Am=()=>{var e,t,a,n,l;const[{orderItem:o,order:m},s]=(0,b.Kx)({orderItem:b.Zp,order:b.Zp}),[i,c]=(0,r.useState)(!1),[d,u]=(0,r.useState)(!1),[p,g]=(0,r.useState)(null),y=e=>{return void 0,null,t=function*(){yield g(e),c(!0)},new Promise(((e,a)=>{var r=e=>{try{l(t.next(e))}catch(e){a(e)}},n=e=>{try{l(t.throw(e))}catch(e){a(e)}},l=t=>t.done?e(t.value):Promise.resolve(t.value).then(r,n);l((t=t.apply(undefined,null)).next())}));var t},{id:E}=(0,A.UO)(),{data:{project:h=null}={}}=Lt({variables:{id:E}}),{data:{projectItems:v=[]}={},refetch:w}=function(e){const t=B(B({},k),e);return f.a(_t,t)}({variables:{id:E,orderItem:o,order:m},fetchPolicy:"cache-and-network",skip:!o||!m}),[N]=function(e){const t=B(B({},k),e);return x.D(Fe,t)}({onCompleted:()=>w()});return(0,r.useEffect)((()=>{null!==o&&void 0!==m||s({orderItem:"id",order:"asc"})}),[]),h?r.createElement(No,null,r.createElement("div",{className:"flex"},r.createElement(um,{project:h}),r.createElement("div",{className:"mt-2 ml-4"},r.createElement("input",{id:"file-upload",name:"file-upload",type:"file",accept:".xlsx",onChange:e=>{e.preventDefault(),(e=>{e.preventDefault();const t=e.target.files[0];if(!t)return;const a=new FileReader;a.onload=e=>{N({variables:{id:h.id,file:t}}),e.preventDefault()},a.readAsDataURL(t)})(e)}}),h.assessmentUrl&&r.createElement("a",{href:h.assessmentUrl},r.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},r.createElement(Ua,{className:"flex-shrink-0 w-5 h-5"}),r.createElement("span",null,"査定表"))))),r.createElement("div",{className:"flex mt-4"},r.createElement("button",{type:"button",className:"mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>c(!0)},"追加"),r.createElement("button",{type:"button",className:"mr-4 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>u(!0)},"業者指定"),r.createElement("div",{className:"w-32"},r.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",onChange:e=>s({orderItem:e.target.value})},r.createElement("option",{value:"id"},"登録順"),r.createElement("option",{value:"code"},"コード順"))),r.createElement("div",{className:"w-32"},r.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",onChange:e=>s({order:e.target.value})},r.createElement("option",{value:"asc"},"昇順"),r.createElement("option",{value:"desc"},"降順"))),r.createElement("div",{className:"flex space-x-6 text-gray-700 text-sm ml-4 items-center"},r.createElement("div",null,"金額小計：",null==(e=h.totalItemPrice)?void 0:e.toLocaleString(),"円"),r.createElement("div",null,"金額：",null==(t=h.totalItemPriceRound)?void 0:t.toLocaleString(),"円"),r.createElement("div",null,"金額（税込）：",null==(a=h.totalPrice)?void 0:a.toLocaleString(),"円"))),r.createElement("div",{className:"flex text-sm text-gray-500 mt-4 space-x-4"},r.createElement("div",null,"最終更新日時：",h.updatedItemsAt&&mo()(h.updatedItemsAt).format("YYYY/MM/DD HH:mm")),r.createElement("div",null,"最終更新ユーザー：",null==(n=h.updatedItemsUser)?void 0:n.lastName,null==(l=h.updatedItemsUser)?void 0:l.firstName)),r.createElement("div",{className:"mt-4 space-y-6 pb-20"},r.createElement("div",{className:"overflow-hidden border-b border-gray-400 sm:rounded-lg"},r.createElement("div",{className:"min-w-full border border-gray-300 divide-y divide-gray-300"},r.createElement("div",{className:"flex bg-gray-50"},r.createElement("div",{className:"w-10"}),r.createElement("div",{className:"w-20"}),r.createElement("div",{className:"w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"コード/項目"),r.createElement("div",{className:"w-36 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"単価/数量"),r.createElement("div",{className:"w-16 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"金額"),r.createElement("div",{className:"w-32 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"標準業者"),r.createElement("div",{className:"flex-1 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"確定業者/部屋種別")),r.createElement("div",{className:"bg-white divide-y divide-gray-300"},v.map((e=>r.createElement(dm,{project:h,projectItem:e,key:e.id,onCopy:y,refetch:w}))))))),i&&r.createElement(cm,{project:h,projectItem:p,isOpen:i,handleClose:()=>c(!1),refetch:w}),r.createElement(mm,{isOpen:d,project:h,handleClose:()=>u(!1)})):null},pm=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Lt({variables:{id:e},fetchPolicy:"cache-and-network"});return t?r.createElement(No,null,r.createElement("div",{className:"mt-10"},r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},r.createElement("table",{className:"min-w-full divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"},"業者"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"},"発注書"))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},t.purchaseOrders.map((e=>r.createElement("tr",{key:e.id},r.createElement("td",{className:"px-6 py-4"},r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"ml-4"}))),r.createElement("td",{className:"px-6 py-4 whitespace-nowrap"},r.createElement("div",{className:"text-sm text-gray-900"},e.fileUrl&&r.createElement("a",{href:e.fileUrl},r.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},r.createElement(Ua,{className:"flex-shrink-0 w-5 h-5"}),r.createElement("span",null,"発注書")))))))))))))))):null},gm=e=>r.createElement("div",{className:"bg-white divide-y divide-gray-200"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"w-64 px-4 py-4 ml-4"},r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"text-sm font-medium text-gray-900"},e.label))),r.createElement("div",{className:"px-2 py-2 w-64"},r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"flex rounded-md shadow-sm w-full"},r.createElement("input",{type:"text",name:e.name,value:e.value,className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",onChange:e.handleChange,onBlur:e.handleBlue})))),r.createElement("div",{className:"px-2 py-2 w-24"},r.createElement("button",{type:"button",className:" px-2 py-2 text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 disabled:opacity-30",disabled:!e.value,onClick:()=>e.handleSubmit(e.reportType)},"出力")),r.createElement("div",{className:"text-xs text-gray-500 my-auto w-32"},r.createElement("p",null,e.datetime)),["report1","report2"].includes(e.reportType)&&r.createElement("div",{className:"text-xs text-gray-500 my-auto"},r.createElement("input",{name:e.checkName,type:"checkbox",checked:e.checkValue,className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",onChange:t=>{e.handleFieldValue(t.target.checked)},onBlur:e.handleBlue})))),bm=({projectReport:e})=>{const[t,{loading:a}]=function(e){const t=B(B({},k),void 0);return x.D(je,t)}(),[n]=function(e){const t=B(B({},k),void 0);return x.D(Ve,t)}(),l=(0,Ea.TA)({initialValues:{spreadsheetId1:e.spreadsheetId1,spreadsheetId2:e.spreadsheetId2,spreadsheetId3:e.spreadsheetId3,spreadsheetId4:e.spreadsheetId4,spreadsheetId5:e.spreadsheetId5,spreadsheetId6:e.spreadsheetId6,spreadsheetId7:e.spreadsheetId7,spreadsheetCheckReport1:e.spreadsheetCheckReport1,spreadsheetCheckReport2:e.spreadsheetCheckReport2},onSubmit:e=>{}}),o=a=>{t({variables:{id:e.id,reportType:a,attributes:l.values}})};return r.createElement("form",{onSubmit:l.handleSubmit},r.createElement(gm,{name:"spreadsheetId1",label:"査定表",value:l.values.spreadsheetId1,datetime:e.spreadsheetDatetime1&&mo()(e.spreadsheetDatetime1).format("YYYY/MM/DD HH:mm"),reportType:"assesment",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o}),r.createElement(gm,{name:"spreadsheetId2",label:"材料発注",value:l.values.spreadsheetId2,datetime:e.spreadsheetDatetime2&&mo()(e.spreadsheetDatetime2).format("YYYY/MM/DD HH:mm"),reportType:"orderMaterial",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o}),r.createElement(gm,{name:"spreadsheetId3",label:"クリーニング業者",value:l.values.spreadsheetId3,datetime:e.spreadsheetDatetime3&&mo()(e.spreadsheetDatetime3).format("YYYY/MM/DD HH:mm"),reportType:"cleaning",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o}),r.createElement(gm,{name:"spreadsheetId4",label:"チェック表",value:l.values.spreadsheetId4,datetime:e.spreadsheetDatetime4&&mo()(e.spreadsheetDatetime4).format("YYYY/MM/DD HH:mm"),reportType:"checklist",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o}),r.createElement(gm,{name:"spreadsheetId5",label:"発注書（塗装/木工/雑工）",value:l.values.spreadsheetId5,datetime:e.spreadsheetDatetime5&&mo()(e.spreadsheetDatetime5).format("YYYY/MM/DD HH:mm"),reportType:"order",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o}),r.createElement(gm,{name:"spreadsheetId6",label:"写真報告書①",value:l.values.spreadsheetId6,datetime:e.spreadsheetDatetime6&&mo()(e.spreadsheetDatetime6).format("YYYY/MM/DD HH:mm"),reportType:"report1",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o,checkName:"spreadsheetCheckReport1",checkValue:l.values.spreadsheetCheckReport1,handleFieldValue:t=>{l.setFieldValue("spreadsheetCheckReport1",t),n({variables:{id:e.id,attributes:{spreadsheetCheckReport1:t}}})}}),r.createElement(gm,{name:"spreadsheetId7",label:"写真報告書②",value:l.values.spreadsheetId7,datetime:e.spreadsheetDatetime7&&mo()(e.spreadsheetDatetime7).format("YYYY/MM/DD HH:mm"),reportType:"report2",handleChange:l.handleChange,handleBlue:l.handleBlur,handleSubmit:o,checkName:"spreadsheetCheckReport2",checkValue:l.values.spreadsheetCheckReport2,handleFieldValue:t=>{l.setFieldValue("spreadsheetCheckReport2",t),n({variables:{id:e.id,attributes:{spreadsheetCheckReport2:t}}})}}))},ym=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Lt({variables:{id:e}});return(0,Ea.TA)({initialValues:{},onSubmit:e=>{console.log("🚀 ~ file: index.tsx:20 ~ ProjectReport ~ values",e)}}),t?r.createElement(No,null,r.createElement("div",{className:"mt-2"},r.createElement("div",{className:"text-lg font-bold"},"レポート出力"),r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},r.createElement("div",{className:"min-w-full divide-y divide-gray-200"},r.createElement("div",{className:"flex flex-1 bg-gray-50"},r.createElement("div",{className:"w-64 px-2 py-3 ml-4 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"レポート"),r.createElement("div",{className:"px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase w-64"},"スプレッドシートID"),r.createElement("div",{className:"px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase w-24"}),r.createElement("div",{className:"px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"最終出力日時")),r.createElement(bm,{projectReport:t.projectReport})))))))):null};var xm=Object.defineProperty,fm=Object.getOwnPropertySymbols,Em=Object.prototype.hasOwnProperty,hm=Object.prototype.propertyIsEnumerable,vm=(e,t,a)=>t in e?xm(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const wm=e=>{const[t]=Oe(),{register:a,handleSubmit:n}=(0,ha.cI)({defaultValues:{remark:e.project.remark},mode:"onBlur"}),l=a=>{t({variables:{id:e.project.id,attributes:a}})};return r.createElement(No,null,r.createElement("form",{onSubmit:n(l)},r.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-2 gap-x-4 sm:grid-cols-6"},r.createElement("div",{className:"col-span-6"},r.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"備考")),r.createElement("div",{className:"col-span-6"},r.createElement("textarea",((e,t)=>{for(var a in t||(t={}))Em.call(t,a)&&vm(e,a,t[a]);if(fm)for(var a of fm(t))hm.call(t,a)&&vm(e,a,t[a]);return e})({rows:5,className:"block w-full p-2 text-sm border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"},a("remark",{onBlur:n(l)})))))))},Nm=()=>{const{id:e}=(0,A.UO)(),{data:{project:t=null}={}}=Lt({variables:{id:e}});return t?r.createElement(wm,{project:t}):null},Bm=()=>{const{id:e}=(0,A.UO)(),{data:{project:t={}}={}}=Lt({variables:{id:e}}),[a]=function(e){const t=B(B({},k),void 0);return x.D($e,t)}(),[n]=function(e){const t=B(B({},k),void 0);return x.D(ke,t)}();return(0,r.useEffect)((()=>(console.log("mounting"),a({variables:{id:t.id}}),()=>{console.log("unmounting"),n({variables:{id:t.id}})})),[]),r.createElement(A.rs,null,r.createElement(A.AW,{path:"/projects/:id/form"},r.createElement(Uo,null)),r.createElement(A.AW,{path:"/projects/:id/orders"},r.createElement(pm,null)),r.createElement(A.AW,{path:"/projects/:id/images"},r.createElement(nm,null)),r.createElement(A.AW,{path:"/projects/:id/company"},r.createElement(jo,null)),r.createElement(A.AW,{path:"/projects/:id/items"},r.createElement(Am,null)),r.createElement(A.AW,{path:"/projects/:id/report"},r.createElement(ym,null)),r.createElement(A.AW,{path:"/projects/:id/remark"},r.createElement(Nm,null)))},km=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/projects/new"},r.createElement(xo,null)),r.createElement(A.AW,{path:"/projects/:id"},r.createElement(Bm,null)),r.createElement(A.AW,{path:"/projects"},r.createElement(go,null)))),Cm=e=>{var t;return r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/roomlayouts",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"部屋レイアウト"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==(t=e.roomLayout)?void 0:t.name))))),e.children))};var Im=Object.defineProperty,Dm=Object.getOwnPropertySymbols,jm=Object.prototype.hasOwnProperty,$m=Object.prototype.propertyIsEnumerable,Fm=(e,t,a)=>t in e?Im(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Sm=(e,t)=>{for(var a in t||(t={}))jm.call(t,a)&&Fm(e,a,t[a]);if(Dm)for(var a of Dm(t))$m.call(t,a)&&Fm(e,a,t[a]);return e};const Pm=()=>{var e;const{id:t}=(0,A.UO)(),a=(0,A.k6)(),[n,{loading:l,data:o}]=function(e){const t=B(B({},k),e);return x.D(nt,t)}({onCompleted:e=>{e.createRoomLayout.error||a.push("/roomlayouts")}}),{register:m,handleSubmit:s,formState:{errors:i,isDirty:c,isValid:d}}=(0,ha.cI)({mode:"all"});return r.createElement(Cm,null,r.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},(null==o?void 0:o.createRoomLayout.error)&&r.createElement("p",{className:"text-red-500"},null==o?void 0:o.createRoomLayout.error),r.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{n({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",Sm({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(e=i.name)?void 0:e.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"金額1"),r.createElement("input",Sm({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("price1",{valueAsNumber:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"金額2"),r.createElement("input",Sm({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("price2",{valueAsNumber:!0})))),r.createElement("div",{className:"py-3 text-right flex items-center"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm",onClick:()=>a.push("/roomlayouts")},"戻る"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!c||!d,loading:l},"保存")))))};var Tm=Object.defineProperty,Om=Object.getOwnPropertySymbols,Mm=Object.prototype.hasOwnProperty,Um=Object.prototype.propertyIsEnumerable,Rm=(e,t,a)=>t in e?Tm(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Vm=(e,t)=>{for(var a in t||(t={}))Mm.call(t,a)&&Rm(e,a,t[a]);if(Om)for(var a of Om(t))Um.call(t,a)&&Rm(e,a,t[a]);return e};const Ym=e=>{var t;const{id:a}=(0,A.UO)(),n=(0,A.k6)(),[l,{loading:o}]=function(e){const t=B(B({},k),e);return x.D(lt,t)}({onCompleted:e=>{e.updateRoomLayout.error||n.push(`/roomlayouts${location.search}`)}}),{register:m,handleSubmit:s,formState:{isDirty:i,isValid:c,errors:d}}=(0,ha.cI)({defaultValues:{name:e.roomLayout.name,price1:e.roomLayout.price1,price2:e.roomLayout.price2}});return r.createElement(Cm,{roomLayout:e.roomLayout},r.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{l({variables:{id:a,attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",Vm({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(t=d.name)?void 0:t.type)&&r.createElement("p",{className:"text-red-400"},"必須項目です")),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"金額1"),r.createElement("input",Vm({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("price1",{valueAsNumber:!0})))),r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"金額2"),r.createElement("input",Vm({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("price2",{valueAsNumber:!0})))),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!i||!c||o,loading:o},"保存")))))},qm=(zl.Ry().shape({name:zl.Z_().trim().required("必須項目です")}),()=>{const{id:e}=(0,A.UO)(),[t,{loading:a}]=((0,A.k6)(),it()),{data:{roomLayout:n=null}={}}=function(e){const t=B(B({},k),e);return f.a(Gt,t)}({variables:{id:e}});return n?r.createElement(Ym,{roomLayout:n}):null}),Lm=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/roomlayouts/new"},r.createElement(Pm,null)),r.createElement(A.AW,{path:"/roomlayouts/:id"},r.createElement(qm,null))),_m=({id:e,index:t,roomLayout:a,moveItem:n,order:l})=>{var o,m;const s=(0,r.useRef)(null),[{handlerId:i},c]=(0,nl.L)({accept:"RoomLayout",collect:e=>({handlerId:e.getHandlerId()}),hover(e,a){var r;if(!s.current)return;const l=e.index,o=t;if(l===o)return;const m=null==(r=s.current)?void 0:r.getBoundingClientRect(),i=(m.bottom-m.top)/2,c=a.getClientOffset().y-m.top;l<o&&c<i||l>o&&c>i||(n(e.id,l,o),e.index=o)}}),[{isDragging:d},A]=(0,ll.c)({type:"RoomLayout",item:()=>({id:e,index:t}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: RoomLayout.tsx:87 ~ RoomLayout ~ item",e),l(e.id,e.index)}}),p=d?0:1;return A(c(s)),r.createElement("div",{ref:s,className:"bg-white flex w-full",style:{opacity:p}},r.createElement("div",{className:"w-44 px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/roomlayouts/${a.id}`},a.name)),r.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},null==(o=a.price1)?void 0:o.toLocaleString()),r.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},null==(m=a.price2)?void 0:m.toLocaleString()))},zm=e=>{const[t,a]=(0,r.useState)([]),[n]=function(e){const t=B(B({},k),void 0);return x.D(ot,t)}(),l=(e,t,r)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${t}`),console.log(`update: hoverIndex: ${r}`),a((e=>tl()(e,{$splice:[[t,1],[r,0,e[t]]]})))},o=(e,t)=>{console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ id",e),console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ index",t),n({variables:{id:e,attributes:{position:t}}})};return(0,r.useEffect)((()=>{a(e.roomLayouts)}),[e.roomLayouts]),r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"部屋レイアウト"),r.createElement(u.rU,{to:"/roomlayouts/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("div",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("div",{className:"bg-gray-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"w-44 px-6 py-3 text-xs font-medium text-left text-gray-500"},"名称"),r.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"金額1"),r.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"金額2"))),r.createElement(al.W,{backend:rl.PD},r.createElement("div",{className:"bg-white divide-y divide-gray-200"},t.map(((e,t)=>r.createElement(_m,{id:e.id,index:t,roomLayout:e,key:e.id,moveItem:l,order:o})))))))))},Wm=()=>{const{data:{roomLayouts:e=null}={}}=Jt({fetchPolicy:"cache-and-network"});return e?r.createElement(zm,{roomLayouts:e}):null},Zm=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/roomlayouts/:id"},r.createElement(Lm,null)),r.createElement(A.AW,{path:"/roomlayouts"},r.createElement(Wm,null))),Hm=()=>r.createElement(pa,null,r.createElement(Zm,null)),Gm=({id:e,index:t,roomType:a,moveItem:n,order:l})=>{const o=(0,r.useRef)(null),[{handlerId:m},s]=(0,nl.L)({accept:"RoomType",collect:e=>({handlerId:e.getHandlerId()}),hover(e,a){var r;if(!o.current)return;const l=e.index,m=t;if(l===m)return;const s=null==(r=o.current)?void 0:r.getBoundingClientRect(),i=(s.bottom-s.top)/2,c=a.getClientOffset().y-s.top;l<m&&c<i||l>m&&c>i||(n(e.id,l,m),e.index=m)}}),[{isDragging:i},c]=(0,ll.c)({type:"RoomType",item:()=>({id:e,index:t}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: RoomType.tsx:87 ~ RoomType ~ item",e),l(e.id,e.index)}}),d=i?0:1;return c(s(o)),r.createElement("div",{ref:o,className:"bg-white flex w-full",style:{opacity:d}},r.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/roomtypes/${a.id}/base`},a.name)))},Km=()=>{const[e,t]=(0,r.useState)([]),{data:{roomTypes:a=[]}={}}=aa({fetchPolicy:"cache-and-network"}),[n]=function(e){const t=B(B({},k),void 0);return x.D(ct,t)}(),l=(e,a,r)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${a}`),console.log(`update: hoverIndex: ${r}`),t((e=>tl()(e,{$splice:[[a,1],[r,0,e[a]]]})))},o=(e,t)=>{console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ id",e),console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ index",t),n({variables:{id:e,attributes:{position:t}}})};return(0,r.useEffect)((()=>{t(a)}),[a]),r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"部屋種別"),r.createElement(u.rU,{to:"/roomtypes/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("div",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("div",{className:"bg-gray-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"名称"))),r.createElement(al.W,{backend:rl.PD},r.createElement("div",{className:"bg-white divide-y divide-gray-200"},e.map(((e,t)=>r.createElement(Gm,{id:e.id,index:t,roomType:e,key:e.id,moveItem:l,order:o})))))))))},Jm=()=>r.createElement(pa,null,r.createElement(Km,null)),Qm=e=>{var t;const{id:a}=(0,A.UO)();return r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/roomTypes",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700","aria-current":void 0},"部屋種別"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==(t=e.roomType)?void 0:t.name))))),r.createElement("div",{className:"hidden sm:block"},r.createElement("div",{className:"border-b border-gray-200"},r.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},r.createElement(u.rU,{to:`/roomtypes/${a}/base${location.search}`},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+("base",location.pathname.indexOf("base")>-1?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報"))))),e.children))},Xm=zl.Ry().shape({name:zl.Z_().trim().required("必須項目です")}),es=()=>{const{id:e}=(0,A.UO)(),t=(0,A.k6)(),[a,{loading:n}]=it(),{data:{roomType:l=null}={}}=ea({variables:{id:e}}),o=(0,Ea.TA)({enableReinitialize:!0,validationSchema:Xm,initialValues:{name:(null==l?void 0:l.name)||""},onSubmit:r=>{a({variables:{id:e,attributes:r}}).then((()=>t.push("/roomtypes")))}});return l?r.createElement(Qm,{roomType:l},r.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},r.createElement("form",{className:"p-6 space-y-6",onSubmit:o.handleSubmit},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:o.values.name,onChange:o.handleChange,onBlur:o.handleBlur})),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:n},"保存"))))):null};var ts=Object.defineProperty,as=Object.getOwnPropertySymbols,rs=Object.prototype.hasOwnProperty,ns=Object.prototype.propertyIsEnumerable,ls=(e,t,a)=>t in e?ts(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const os=()=>{var e;const{id:t}=(0,A.UO)(),a=(0,A.k6)(),[n,{loading:l,data:o}]=function(e){const t=B(B({},k),e);return x.D(mt,t)}({onCompleted:e=>{e.createRoomTypeModel.error||a.push("/roomtypes")}}),{register:m,handleSubmit:s,formState:{errors:i,isDirty:c,isValid:d}}=(0,ha.cI)({mode:"all"});return r.createElement(Qm,null,r.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},(null==o?void 0:o.createRoomTypeModel.error)&&r.createElement("p",{className:"text-red-500"},null==o?void 0:o.createRoomTypeModel.error),r.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{n({variables:{attributes:e}})}))},r.createElement("div",null,r.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),r.createElement("input",((e,t)=>{for(var a in t||(t={}))rs.call(t,a)&&ls(e,a,t[a]);if(as)for(var a of as(t))ns.call(t,a)&&ls(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(e=i.name)?void 0:e.type)&&r.createElement("p",{className:"text-red-400"},"名称は必須項目です")),r.createElement("div",{className:"py-3 text-right flex items-center"},r.createElement(ga,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm",onClick:()=>a.push("/roomtypes")},"戻る"),r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!c||!d,loading:l},"保存")))))},ms=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/roomtypes/new"},r.createElement(os,null)),r.createElement(A.AW,{path:"/roomtypes/:id/base"},r.createElement(es,null))),ss=()=>r.createElement(pa,null,r.createElement(ms,null)),is=()=>{const[e,t]=(0,b.Kx)({active:b.dJ});return r.createElement("div",{className:"hidden sm:block"},r.createElement("div",{className:"border-b border-gray-200"},r.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},r.createElement("div",{onClick:()=>t({active:!0})},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(e.active?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"有効")),r.createElement("div",{onClick:()=>t({active:!1})},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(e.active?"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300":"border-indigo-500 text-indigo-600")},"無効")))))},cs=()=>{const[e,t]=(0,b.Kx)({active:b.dJ}),{data:{users:a=[]}={}}=ca({variables:{search:{active:e.active}},fetchPolicy:"cache-and-network"});return(0,r.useEffect)((()=>{null!==e.active&&void 0!==e.active||t({active:!0})}),[]),null===e.active||void 0===e.active?null:r.createElement("div",{className:"flex flex-col"},r.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},r.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},r.createElement("h1",{className:"mb-4 text-2xl font-bold"},"従業員"),r.createElement(is,null),r.createElement(u.rU,{to:"/users/new"},r.createElement("div",{className:"inline-flex justify-center px-4 py-2 mt-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),r.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},r.createElement("thead",{className:"bg-gray-50"},r.createElement("tr",null,r.createElement("th",{scope:"col",className:"w-6 px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"ログインID"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-60"},"名称"),r.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"査定同行者"),r.createElement("th",null))),r.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},a.map((e=>r.createElement("tr",{className:"bg-white",key:e.id},r.createElement("td",{className:"ml-10"},r.createElement("div",{className:"px-2 cursor-pointer"},e.id)),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/users/${e.id}/base`},e.loginId)),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},r.createElement(u.rU,{to:`/users/${e.id}/base`},e.lastName," ",e.firstName)),r.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},e.isAssesment&&r.createElement("div",null,r.createElement("span",{className:"inline-flex items-center rounded-md bg-yellow-100 px-2.5 py-0.5 text-sm font-medium text-yellow-800"},"査定同行者")))))))))))},ds=zl.Ry().shape({loginId:zl.Z_().required("必須項目です"),email:zl.Z_().email("メールアドレスの形式が不正です"),lastName:zl.Z_().trim().required("必須項目です"),firstName:zl.Z_().trim().required("必須項目です"),password:zl.Z_().trim().required("必須項目です").min(6,"6文字以上で設定してください"),passwordConfirmation:zl.Z_().trim().required("必須項目です").oneOf([zl.iH("password"),null],"パスワードが一致していません")}),us=()=>{const e=(0,A.k6)(),[t,{data:a,loading:n}]=function(e){const t=B(B({},k),e);return x.D(dt,t)}({onCompleted:t=>{t.createUser.error||e.push(`/users${location.search}`)}}),l=(0,Ea.TA)({enableReinitialize:!0,validationSchema:ds,initialValues:{loginId:"",email:"",lastName:"",firstName:"",password:"",passwordConfirmation:""},onSubmit:e=>{t({variables:{attributes:e}})}});return r.createElement("div",{className:"w-full"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"従業員"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),r.createElement("div",{className:"mt-10 md:col-span-2"},(null==a?void 0:a.createUser.error)&&r.createElement("div",{className:"p-4 rounded-md bg-red-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"ml-3"},r.createElement("div",{className:"mt-2 text-sm text-red-700"},r.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},r.createElement("li",null,null==a?void 0:a.createUser.error)))))),r.createElement("form",{className:"p-6 space-y-6",onSubmit:l.handleSubmit},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"ログインID"),r.createElement("input",{type:"text",name:"loginId",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.loginId&&l.touched.loginId?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.loginId,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.loginId&&l.touched.loginId&&r.createElement("span",{className:"text-red-500"},l.errors.loginId)),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),r.createElement("input",{type:"text",name:"lastName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.lastName&&l.touched.lastName?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.lastName,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.lastName&&l.touched.lastName&&r.createElement("span",{className:"text-red-500"},l.errors.lastName)),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),r.createElement("input",{type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.firstName&&l.touched.firstName?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.firstName,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.firstName&&l.touched.firstName&&r.createElement("span",{className:"text-red-500"},l.errors.firstName)),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード"),r.createElement("input",{type:"password",name:"password",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.password&&l.touched.password?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.password,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.password&&l.touched.password&&r.createElement("span",{className:"text-red-500"},l.errors.password)),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード(確認)"),r.createElement("input",{type:"password",name:"passwordConfirmation",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.passwordConfirmation&&l.touched.passwordConfirmation?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.passwordConfirmation,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.passwordConfirmation&&l.touched.passwordConfirmation&&r.createElement("span",{className:"text-red-500"},l.errors.passwordConfirmation)),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"メールアドレス"),r.createElement("input",{type:"email",name:"email",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.email&&l.touched.email?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.email,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.email&&l.touched.email&&r.createElement("span",{className:"text-red-500"},l.errors.email)),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!l.isValid||!l.dirty,loading:n},"保存")))))))},As=()=>{const{id:e}=(0,A.UO)(),t=e=>location.pathname.endsWith(e);return r.createElement("div",{className:"hidden sm:block"},r.createElement("div",{className:"border-b border-gray-200"},r.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(t("base")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},r.createElement(u.rU,{to:`/users/${e}/base`},"基本情報")),r.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(t("password")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},r.createElement(u.rU,{to:`/users/${e}/password`},"パスワード")))))},ps=zl.Ry().shape({lastName:zl.Z_().trim().required("必須項目です"),firstName:zl.Z_().trim().required("必須項目です")}),gs=()=>{const{id:e}=(0,A.UO)(),t=(0,A.k6)(),[a,{data:n,loading:l}]=At({onCompleted:e=>{e.updateUser.error||t.push(`/users${location.search}`)}}),{data:{user:o=null}={}}=sa({variables:{id:e}}),m=(0,Ea.TA)({enableReinitialize:!0,validationSchema:ps,initialValues:{loginId:(null==o?void 0:o.loginId)||"",lastName:(null==o?void 0:o.lastName)||"",firstName:(null==o?void 0:o.firstName)||"",isAssesment:null==o?void 0:o.isAssesment,tel:null==o?void 0:o.tel,active:null==o?void 0:o.active},onSubmit:t=>{a({variables:{id:e,attributes:t}})}});return o?r.createElement("div",{className:"w-screen max-w-xl"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"従業員一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},o.lastName," ",o.firstName))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement(As,null),(null==n?void 0:n.updateUser.error)&&r.createElement("div",{className:"p-4 rounded-md bg-red-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"ml-3"},r.createElement("div",{className:"mt-2 text-sm text-red-700"},r.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},r.createElement("li",null,null==n?void 0:n.updateUser.error)))))),r.createElement("form",{className:"p-6 space-y-6",onSubmit:m.handleSubmit},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"ログインID"),r.createElement("input",{type:"text",name:"loginId",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(m.errors.loginId&&m.touched.loginId?"border-red-500 bg-red-200 focus:border-red-500":""),value:m.values.loginId,onChange:m.handleChange,onBlur:m.handleBlur})),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),r.createElement("input",{type:"text",name:"lastName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(m.errors.lastName&&m.touched.lastName?"border-red-500 bg-red-200 focus:border-red-500":""),value:m.values.lastName,onChange:m.handleChange,onBlur:m.handleBlur})),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),r.createElement("input",{type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(m.errors.firstName&&m.touched.firstName?"border-red-500 bg-red-200 focus:border-red-500":""),value:m.values.firstName,onChange:m.handleChange,onBlur:m.handleBlur})),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"TEL"),r.createElement("input",{type:"text",name:"tel",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(m.errors.tel&&m.touched.tel?"border-red-500 bg-red-200 focus:border-red-500":""),value:m.values.tel,onChange:m.handleChange,onBlur:m.handleBlur})),r.createElement("div",{className:"mt-4 space-y-4"},r.createElement("div",{className:"flex items-center"},r.createElement("div",{className:"relative flex items-start"},r.createElement("div",{className:"flex items-center h-5"},r.createElement("input",{id:"comments","aria-describedby":"comments-description",name:"isAddesment",type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",onChange:e=>m.setFieldValue("isAssesment",e.target.checked),checked:m.values.isAssesment})),r.createElement("div",{className:"ml-3 text-sm"},r.createElement("label",{className:"font-medium text-gray-700"},"査定同行者"))))),r.createElement("div",{className:"mt-4 space-y-4"},r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"active",name:"active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",checked:m.values.active,onChange:e=>m.setFieldValue("active",!0)}),r.createElement("label",{htmlFor:"active",className:"block ml-3 text-sm font-medium text-gray-700"},"有効")),r.createElement("div",{className:"flex items-center"},r.createElement("input",{id:"non-active",name:"non-active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",checked:!m.values.active,onChange:e=>{m.setFieldValue("active",!1)}}),r.createElement("label",{htmlFor:"non-active",className:"block ml-3 text-sm font-medium text-gray-700"},"無効"))),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:l||!m.isValid||!m.dirty,loading:l},"保存"))))))):null},bs=zl.Ry().shape({password:zl.Z_().trim().required("必須項目です").min(6,"6文字以上で設定してください"),passwordConfirmation:zl.Z_().trim().required("必須項目です").oneOf([zl.iH("password"),null],"パスワードが一致していません")}),ys=()=>{const{id:e}=(0,A.UO)(),t=(0,A.k6)(),[a,{data:n,loading:l}]=At({onCompleted:e=>{e.updateUser.error||t.push(`/users${location.search}`)}}),{data:{user:o=null}={}}=sa({variables:{id:e}}),m=(0,Ea.TA)({enableReinitialize:!0,validationSchema:bs,initialValues:{password:"",passwordConfirmation:""},onSubmit:t=>{a({variables:{id:e,attributes:t}})}});return o?r.createElement("div",{className:"w-screen max-w-xl"},r.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},r.createElement("div",{className:"px-4 sm:px-6"},r.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},r.createElement("ol",{role:"list",className:"flex items-center space-x-4"},r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(u.rU,{to:"/users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"従業員一覧"))),r.createElement("li",null,r.createElement("div",{className:"flex items-center"},r.createElement(Ta,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),r.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},o.lastName," ",o.firstName))))),r.createElement("div",{className:"mt-10 md:col-span-2"},r.createElement(As,null),(null==n?void 0:n.updateUser.error)&&r.createElement("div",{className:"p-4 rounded-md bg-red-50"},r.createElement("div",{className:"flex"},r.createElement("div",{className:"ml-3"},r.createElement("div",{className:"mt-2 text-sm text-red-700"},r.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},r.createElement("li",null,null==n?void 0:n.updateUser.error)))))),r.createElement("form",{className:"p-6 space-y-6",onSubmit:m.handleSubmit},r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード"),r.createElement("input",{type:"password",name:"password",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(m.errors.password&&m.touched.password?"border-red-500 bg-red-200 focus:border-red-500":""),value:m.values.password,onChange:m.handleChange,onBlur:m.handleBlur}),m.errors.password&&m.touched.password&&r.createElement("span",{className:"text-red-500"},m.errors.password)),r.createElement("div",null,r.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード(確認)"),r.createElement("input",{type:"password",name:"passwordConfirmation",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(m.errors.passwordConfirmation&&m.touched.passwordConfirmation?"border-red-500 bg-red-200 focus:border-red-500":""),value:m.values.passwordConfirmation,onChange:m.handleChange,onBlur:m.handleBlur}),m.errors.passwordConfirmation&&m.touched.passwordConfirmation&&r.createElement("span",{className:"text-red-500"},m.errors.passwordConfirmation)),r.createElement("div",{className:"py-3 text-right"},r.createElement(ga,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:l||!m.isValid||!m.dirty,loading:l},"保存"))))))):null},xs=()=>r.createElement(pa,null,r.createElement(A.rs,null,r.createElement(A.AW,{path:"/users/new"},r.createElement(us,null)),r.createElement(A.AW,{path:"/users/:id/base"},r.createElement(gs,null)),r.createElement(A.AW,{path:"/users/:id/password"},r.createElement(ys,null)),r.createElement(A.AW,{path:"/users"},r.createElement(cs,null)))),fs=()=>r.createElement(A.rs,null,r.createElement(A.AW,{path:"/login"},r.createElement(lo,null)),r.createElement(A.AW,{path:"/comments"},r.createElement(ln,null)),r.createElement(A.AW,{path:"/commenttypes"},r.createElement(vn,null)),r.createElement(A.AW,{path:"/buildings"},r.createElement(Mr,null)),r.createElement(A.AW,{path:"/roomtypes/:id"},r.createElement(ss,null)),r.createElement(A.AW,{path:"/roomtypes"},r.createElement(Jm,null)),r.createElement(A.AW,{path:"/roomlayouts"},r.createElement(Hm,null)),r.createElement(A.AW,{path:"/projects"},r.createElement(km,null)),r.createElement(A.AW,{path:"/orders"},r.createElement(Ao,null)),r.createElement(A.AW,{path:"/items"},r.createElement(Tl,null)),r.createElement(A.AW,{path:"/companies"},r.createElement(Rn,null)),r.createElement(A.AW,{path:"/companytypes"},r.createElement(sl,null)),r.createElement(A.AW,{path:"/users"},r.createElement(xs,null)),r.createElement(A.AW,{path:"/jkk_users"},r.createElement(eo,null)),r.createElement(A.AW,{path:"/"},r.createElement(il,null)));var Es=a(3379),hs=a.n(Es),vs=a(7795),ws=a.n(vs),Ns=a(569),Bs=a.n(Ns),ks=a(3565),Cs=a.n(ks),Is=a(9216),Ds=a.n(Is),js=a(4589),$s=a.n(js),Fs=a(595),Ss={};Ss.styleTagTransform=$s(),Ss.setAttributes=Cs(),Ss.insert=Bs().bind(null,"head"),Ss.domAPI=ws(),Ss.insertStyleElement=Ds(),hs()(Fs.Z,Ss),Fs.Z&&Fs.Z.locals&&Fs.Z.locals,a(9657);const Ps=c()({uri:"/graphql",credentials:"include"}),Ts=new l.f({link:o.i.from([(0,d.q)((({graphQLErrors:e,networkError:t})=>{e&&e.map((({message:e,locations:t,path:a})=>{})),t&&console.error(`[Network error]: ${t}`)})),Ps]),cache:new s.h});g().setAppElement("#app");const Os=()=>r.createElement(m.e,{client:Ts},r.createElement(u.VK,null,r.createElement(b.Fz,{ReactRouterRoute:A.AW},r.createElement(fs,null))));n.render(r.createElement(Os,null),document.getElementById("app"))}},n={};function l(e){var t=n[e];if(void 0!==t)return t.exports;var a=n[e]={id:e,loaded:!1,exports:{}};return r[e].call(a.exports,a,a.exports,l),a.loaded=!0,a.exports}l.m=r,e=[],l.O=(t,a,r,n)=>{if(!a){var o=1/0;for(c=0;c<e.length;c++){for(var[a,r,n]=e[c],m=!0,s=0;s<a.length;s++)(!1&n||o>=n)&&Object.keys(l.O).every((e=>l.O[e](a[s])))?a.splice(s--,1):(m=!1,n<o&&(o=n));if(m){e.splice(c--,1);var i=r();void 0!==i&&(t=i)}}return t}n=n||0;for(var c=e.length;c>0&&e[c-1][2]>n;c--)e[c]=e[c-1];e[c]=[a,r,n]},l.n=e=>{var t=e&&e.__esModule?()=>e.default:()=>e;return l.d(t,{a:t}),t},a=Object.getPrototypeOf?e=>Object.getPrototypeOf(e):e=>e.__proto__,l.t=function(e,r){if(1&r&&(e=this(e)),8&r)return e;if("object"==typeof e&&e){if(4&r&&e.__esModule)return e;if(16&r&&"function"==typeof e.then)return e}var n=Object.create(null);l.r(n);var o={};t=t||[null,a({}),a([]),a(a)];for(var m=2&r&&e;"object"==typeof m&&!~t.indexOf(m);m=a(m))Object.getOwnPropertyNames(m).forEach((t=>o[t]=()=>e[t]));return o.default=()=>e,l.d(n,o),n},l.d=(e,t)=>{for(var a in t)l.o(t,a)&&!l.o(e,a)&&Object.defineProperty(e,a,{enumerable:!0,get:t[a]})},l.g=function(){if("object"==typeof globalThis)return globalThis;try{return this||new Function("return this")()}catch(e){if("object"==typeof window)return window}}(),l.o=(e,t)=>Object.prototype.hasOwnProperty.call(e,t),l.r=e=>{"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},l.nmd=e=>(e.paths=[],e.children||(e.children=[]),e),(()=>{var e={378:0};l.O.j=t=>0===e[t];var t=(t,a)=>{var r,n,[o,m,s]=a,i=0;if(o.some((t=>0!==e[t]))){for(r in m)l.o(m,r)&&(l.m[r]=m[r]);if(s)var c=s(l)}for(t&&t(a);i<o.length;i++)n=o[i],l.o(e,n)&&e[n]&&e[n][0](),e[o[i]]=0;return l.O(c)},a=self.webpackChunknode=self.webpackChunknode||[];a.forEach(t.bind(null,0)),a.push=t.bind(null,a.push.bind(a))})();var o=l.O(void 0,[736],(()=>l(5514)));o=l.O(o)})();
//# sourceMappingURL=user.bundle.644eb649.js.map