(()=>{"use strict";var e,t,a,r={595:(e,t,a)=>{a.d(t,{Z:()=>m});var r=a(7537),n=a.n(r),o=a(3645),l=a.n(o)()(n());l.push([e.id,'/*\n! tailwindcss v3.3.2 | MIT License | https://tailwindcss.com\n*//*\n1. Prevent padding and border from affecting element width. (https://github.com/mozdevs/cssremedy/issues/4)\n2. Allow adding a border to an element by just adding a border-width. (https://github.com/tailwindcss/tailwindcss/pull/116)\n*/\n\n*,\n::before,\n::after {\n  box-sizing: border-box; /* 1 */\n  border-width: 0; /* 2 */\n  border-style: solid; /* 2 */\n  border-color: #e5e7eb; /* 2 */\n}\n\n::before,\n::after {\n  --tw-content: \'\';\n}\n\n/*\n1. Use a consistent sensible line-height in all browsers.\n2. Prevent adjustments of font size after orientation changes in iOS.\n3. Use a more readable tab size.\n4. Use the user\'s configured `sans` font-family by default.\n5. Use the user\'s configured `sans` font-feature-settings by default.\n6. Use the user\'s configured `sans` font-variation-settings by default.\n*/\n\nhtml {\n  line-height: 1.5; /* 1 */\n  -webkit-text-size-adjust: 100%; /* 2 */\n  -moz-tab-size: 4; /* 3 */\n  -o-tab-size: 4;\n     tab-size: 4; /* 3 */\n  font-family: ui-sans-serif, system-ui, -apple-system, Segoe UI, Roboto, Ubuntu, Cantarell, Noto Sans, sans-serif, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; /* 4 */\n  font-feature-settings: normal; /* 5 */\n  font-variation-settings: normal; /* 6 */\n}\n\n/*\n1. Remove the margin in all browsers.\n2. Inherit line-height from `html` so users can set them as a class directly on the `html` element.\n*/\n\nbody {\n  margin: 0; /* 1 */\n  line-height: inherit; /* 2 */\n}\n\n/*\n1. Add the correct height in Firefox.\n2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)\n3. Ensure horizontal rules are visible by default.\n*/\n\nhr {\n  height: 0; /* 1 */\n  color: inherit; /* 2 */\n  border-top-width: 1px; /* 3 */\n}\n\n/*\nAdd the correct text decoration in Chrome, Edge, and Safari.\n*/\n\nabbr:where([title]) {\n  -webkit-text-decoration: underline dotted;\n          text-decoration: underline dotted;\n}\n\n/*\nRemove the default font size and weight for headings.\n*/\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-size: inherit;\n  font-weight: inherit;\n}\n\n/*\nReset links to optimize for opt-in styling instead of opt-out.\n*/\n\na {\n  color: inherit;\n  text-decoration: inherit;\n}\n\n/*\nAdd the correct font weight in Edge and Safari.\n*/\n\nb,\nstrong {\n  font-weight: bolder;\n}\n\n/*\n1. Use the user\'s configured `mono` font family by default.\n2. Correct the odd `em` font sizing in all browsers.\n*/\n\ncode,\nkbd,\nsamp,\npre {\n  font-family: ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace; /* 1 */\n  font-size: 1em; /* 2 */\n}\n\n/*\nAdd the correct font size in all browsers.\n*/\n\nsmall {\n  font-size: 80%;\n}\n\n/*\nPrevent `sub` and `sup` elements from affecting the line height in all browsers.\n*/\n\nsub,\nsup {\n  font-size: 75%;\n  line-height: 0;\n  position: relative;\n  vertical-align: baseline;\n}\n\nsub {\n  bottom: -0.25em;\n}\n\nsup {\n  top: -0.5em;\n}\n\n/*\n1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)\n2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)\n3. Remove gaps between table borders by default.\n*/\n\ntable {\n  text-indent: 0; /* 1 */\n  border-color: inherit; /* 2 */\n  border-collapse: collapse; /* 3 */\n}\n\n/*\n1. Change the font styles in all browsers.\n2. Remove the margin in Firefox and Safari.\n3. Remove default padding in all browsers.\n*/\n\nbutton,\ninput,\noptgroup,\nselect,\ntextarea {\n  font-family: inherit; /* 1 */\n  font-size: 100%; /* 1 */\n  font-weight: inherit; /* 1 */\n  line-height: inherit; /* 1 */\n  color: inherit; /* 1 */\n  margin: 0; /* 2 */\n  padding: 0; /* 3 */\n}\n\n/*\nRemove the inheritance of text transform in Edge and Firefox.\n*/\n\nbutton,\nselect {\n  text-transform: none;\n}\n\n/*\n1. Correct the inability to style clickable types in iOS and Safari.\n2. Remove default button styles.\n*/\n\nbutton,\n[type=\'button\'],\n[type=\'reset\'],\n[type=\'submit\'] {\n  -webkit-appearance: button; /* 1 */\n  background-color: transparent; /* 2 */\n  background-image: none; /* 2 */\n}\n\n/*\nUse the modern Firefox focus style for all focusable elements.\n*/\n\n:-moz-focusring {\n  outline: auto;\n}\n\n/*\nRemove the additional `:invalid` styles in Firefox. (https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737)\n*/\n\n:-moz-ui-invalid {\n  box-shadow: none;\n}\n\n/*\nAdd the correct vertical alignment in Chrome and Firefox.\n*/\n\nprogress {\n  vertical-align: baseline;\n}\n\n/*\nCorrect the cursor style of increment and decrement buttons in Safari.\n*/\n\n::-webkit-inner-spin-button,\n::-webkit-outer-spin-button {\n  height: auto;\n}\n\n/*\n1. Correct the odd appearance in Chrome and Safari.\n2. Correct the outline style in Safari.\n*/\n\n[type=\'search\'] {\n  -webkit-appearance: textfield; /* 1 */\n  outline-offset: -2px; /* 2 */\n}\n\n/*\nRemove the inner padding in Chrome and Safari on macOS.\n*/\n\n::-webkit-search-decoration {\n  -webkit-appearance: none;\n}\n\n/*\n1. Correct the inability to style clickable types in iOS and Safari.\n2. Change font properties to `inherit` in Safari.\n*/\n\n::-webkit-file-upload-button {\n  -webkit-appearance: button; /* 1 */\n  font: inherit; /* 2 */\n}\n\n/*\nAdd the correct display in Chrome and Safari.\n*/\n\nsummary {\n  display: list-item;\n}\n\n/*\nRemoves the default spacing and border for appropriate elements.\n*/\n\nblockquote,\ndl,\ndd,\nh1,\nh2,\nh3,\nh4,\nh5,\nh6,\nhr,\nfigure,\np,\npre {\n  margin: 0;\n}\n\nfieldset {\n  margin: 0;\n  padding: 0;\n}\n\nlegend {\n  padding: 0;\n}\n\nol,\nul,\nmenu {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n}\n\n/*\nPrevent resizing textareas horizontally by default.\n*/\n\ntextarea {\n  resize: vertical;\n}\n\n/*\n1. Reset the default placeholder opacity in Firefox. (https://github.com/tailwindlabs/tailwindcss/issues/3300)\n2. Set the default placeholder color to the user\'s configured gray 400 color.\n*/\n\ninput::-moz-placeholder, textarea::-moz-placeholder {\n  opacity: 1; /* 1 */\n  color: #9ca3af; /* 2 */\n}\n\ninput:-ms-input-placeholder, textarea:-ms-input-placeholder {\n  opacity: 1; /* 1 */\n  color: #9ca3af; /* 2 */\n}\n\ninput::placeholder,\ntextarea::placeholder {\n  opacity: 1; /* 1 */\n  color: #9ca3af; /* 2 */\n}\n\n/*\nSet the default cursor for buttons.\n*/\n\nbutton,\n[role="button"] {\n  cursor: pointer;\n}\n\n/*\nMake sure disabled buttons don\'t get the pointer cursor.\n*/\n:disabled {\n  cursor: default;\n}\n\n/*\n1. Make replaced elements `display: block` by default. (https://github.com/mozdevs/cssremedy/issues/14)\n2. Add `vertical-align: middle` to align replaced elements more sensibly by default. (https://github.com/jensimmons/cssremedy/issues/14#issuecomment-634934210)\n   This can trigger a poorly considered lint error in some tools but is included by design.\n*/\n\nimg,\nsvg,\nvideo,\ncanvas,\naudio,\niframe,\nembed,\nobject {\n  display: block; /* 1 */\n  vertical-align: middle; /* 2 */\n}\n\n/*\nConstrain images and videos to the parent width and preserve their intrinsic aspect ratio. (https://github.com/mozdevs/cssremedy/issues/14)\n*/\n\nimg,\nvideo {\n  max-width: 100%;\n  height: auto;\n}\n\n/* Make elements with the HTML hidden attribute stay hidden by default */\n[hidden] {\n  display: none;\n}\n\n*, ::before, ::after {\n  --tw-border-spacing-x: 0;\n  --tw-border-spacing-y: 0;\n  --tw-translate-x: 0;\n  --tw-translate-y: 0;\n  --tw-rotate: 0;\n  --tw-skew-x: 0;\n  --tw-skew-y: 0;\n  --tw-scale-x: 1;\n  --tw-scale-y: 1;\n  --tw-pan-x:  ;\n  --tw-pan-y:  ;\n  --tw-pinch-zoom:  ;\n  --tw-scroll-snap-strictness: proximity;\n  --tw-gradient-from-position:  ;\n  --tw-gradient-via-position:  ;\n  --tw-gradient-to-position:  ;\n  --tw-ordinal:  ;\n  --tw-slashed-zero:  ;\n  --tw-numeric-figure:  ;\n  --tw-numeric-spacing:  ;\n  --tw-numeric-fraction:  ;\n  --tw-ring-inset:  ;\n  --tw-ring-offset-width: 0px;\n  --tw-ring-offset-color: #fff;\n  --tw-ring-color: rgb(59 130 246 / 0.5);\n  --tw-ring-offset-shadow: 0 0 rgba(0,0,0,0);\n  --tw-ring-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow-colored: 0 0 rgba(0,0,0,0);\n  --tw-blur:  ;\n  --tw-brightness:  ;\n  --tw-contrast:  ;\n  --tw-grayscale:  ;\n  --tw-hue-rotate:  ;\n  --tw-invert:  ;\n  --tw-saturate:  ;\n  --tw-sepia:  ;\n  --tw-drop-shadow:  ;\n  --tw-backdrop-blur:  ;\n  --tw-backdrop-brightness:  ;\n  --tw-backdrop-contrast:  ;\n  --tw-backdrop-grayscale:  ;\n  --tw-backdrop-hue-rotate:  ;\n  --tw-backdrop-invert:  ;\n  --tw-backdrop-opacity:  ;\n  --tw-backdrop-saturate:  ;\n  --tw-backdrop-sepia:  ;\n}\n\n::backdrop {\n  --tw-border-spacing-x: 0;\n  --tw-border-spacing-y: 0;\n  --tw-translate-x: 0;\n  --tw-translate-y: 0;\n  --tw-rotate: 0;\n  --tw-skew-x: 0;\n  --tw-skew-y: 0;\n  --tw-scale-x: 1;\n  --tw-scale-y: 1;\n  --tw-pan-x:  ;\n  --tw-pan-y:  ;\n  --tw-pinch-zoom:  ;\n  --tw-scroll-snap-strictness: proximity;\n  --tw-gradient-from-position:  ;\n  --tw-gradient-via-position:  ;\n  --tw-gradient-to-position:  ;\n  --tw-ordinal:  ;\n  --tw-slashed-zero:  ;\n  --tw-numeric-figure:  ;\n  --tw-numeric-spacing:  ;\n  --tw-numeric-fraction:  ;\n  --tw-ring-inset:  ;\n  --tw-ring-offset-width: 0px;\n  --tw-ring-offset-color: #fff;\n  --tw-ring-color: rgb(59 130 246 / 0.5);\n  --tw-ring-offset-shadow: 0 0 rgba(0,0,0,0);\n  --tw-ring-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow: 0 0 rgba(0,0,0,0);\n  --tw-shadow-colored: 0 0 rgba(0,0,0,0);\n  --tw-blur:  ;\n  --tw-brightness:  ;\n  --tw-contrast:  ;\n  --tw-grayscale:  ;\n  --tw-hue-rotate:  ;\n  --tw-invert:  ;\n  --tw-saturate:  ;\n  --tw-sepia:  ;\n  --tw-drop-shadow:  ;\n  --tw-backdrop-blur:  ;\n  --tw-backdrop-brightness:  ;\n  --tw-backdrop-contrast:  ;\n  --tw-backdrop-grayscale:  ;\n  --tw-backdrop-hue-rotate:  ;\n  --tw-backdrop-invert:  ;\n  --tw-backdrop-opacity:  ;\n  --tw-backdrop-saturate:  ;\n  --tw-backdrop-sepia:  ;\n}\n.sr-only {\n  position: absolute;\n  width: 1px;\n  height: 1px;\n  padding: 0;\n  margin: -1px;\n  overflow: hidden;\n  clip: rect(0, 0, 0, 0);\n  white-space: nowrap;\n  border-width: 0;\n}\n.pointer-events-none {\n  pointer-events: none;\n}\n.fixed {\n  position: fixed;\n}\n.absolute {\n  position: absolute;\n}\n.relative {\n  position: relative;\n}\n.sticky {\n  position: sticky;\n}\n.inset-x-0 {\n  left: 0px;\n  right: 0px;\n}\n.inset-y-0 {\n  top: 0px;\n  bottom: 0px;\n}\n.bottom-0 {\n  bottom: 0px;\n}\n.left-0 {\n  left: 0px;\n}\n.right-0 {\n  right: 0px;\n}\n.right-10 {\n  right: 2.5rem;\n}\n.top-0 {\n  top: 0px;\n}\n.top-60 {\n  top: 15rem;\n}\n.top-full {\n  top: 100%;\n}\n.z-0 {\n  z-index: 0;\n}\n.z-10 {\n  z-index: 10;\n}\n.col-span-1 {\n  grid-column: span 1 / span 1;\n}\n.col-span-12 {\n  grid-column: span 12 / span 12;\n}\n.col-span-2 {\n  grid-column: span 2 / span 2;\n}\n.col-span-3 {\n  grid-column: span 3 / span 3;\n}\n.col-span-4 {\n  grid-column: span 4 / span 4;\n}\n.col-span-6 {\n  grid-column: span 6 / span 6;\n}\n.col-span-8 {\n  grid-column: span 8 / span 8;\n}\n.col-span-full {\n  grid-column: 1 / -1;\n}\n.m-auto {\n  margin: auto;\n}\n.-my-2 {\n  margin-top: -0.5rem;\n  margin-bottom: -0.5rem;\n}\n.mx-1 {\n  margin-left: 0.25rem;\n  margin-right: 0.25rem;\n}\n.mx-2 {\n  margin-left: 0.5rem;\n  margin-right: 0.5rem;\n}\n.mx-4 {\n  margin-left: 1rem;\n  margin-right: 1rem;\n}\n.mx-auto {\n  margin-left: auto;\n  margin-right: auto;\n}\n.my-2 {\n  margin-top: 0.5rem;\n  margin-bottom: 0.5rem;\n}\n.my-4 {\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n.my-6 {\n  margin-top: 1.5rem;\n  margin-bottom: 1.5rem;\n}\n.my-auto {\n  margin-top: auto;\n  margin-bottom: auto;\n}\n.-mb-px {\n  margin-bottom: -1px;\n}\n.-ml-1 {\n  margin-left: -0.25rem;\n}\n.mb-2 {\n  margin-bottom: 0.5rem;\n}\n.mb-4 {\n  margin-bottom: 1rem;\n}\n.ml-1 {\n  margin-left: 0.25rem;\n}\n.ml-10 {\n  margin-left: 2.5rem;\n}\n.ml-2 {\n  margin-left: 0.5rem;\n}\n.ml-3 {\n  margin-left: 0.75rem;\n}\n.ml-4 {\n  margin-left: 1rem;\n}\n.ml-auto {\n  margin-left: auto;\n}\n.mr-2 {\n  margin-right: 0.5rem;\n}\n.mr-3 {\n  margin-right: 0.75rem;\n}\n.mr-4 {\n  margin-right: 1rem;\n}\n.mt-1 {\n  margin-top: 0.25rem;\n}\n.mt-10 {\n  margin-top: 2.5rem;\n}\n.mt-2 {\n  margin-top: 0.5rem;\n}\n.mt-4 {\n  margin-top: 1rem;\n}\n.mt-5 {\n  margin-top: 1.25rem;\n}\n.mt-6 {\n  margin-top: 1.5rem;\n}\n.mt-8 {\n  margin-top: 2rem;\n}\n.block {\n  display: block;\n}\n.inline-block {\n  display: inline-block;\n}\n.flex {\n  display: flex;\n}\n.inline-flex {\n  display: inline-flex;\n}\n.table {\n  display: table;\n}\n.grid {\n  display: grid;\n}\n.hidden {\n  display: none;\n}\n.h-0 {\n  height: 0px;\n}\n.h-0\\.5 {\n  height: 0.125rem;\n}\n.h-10 {\n  height: 2.5rem;\n}\n.h-12 {\n  height: 3rem;\n}\n.h-2 {\n  height: 0.5rem;\n}\n.h-3 {\n  height: 0.75rem;\n}\n.h-4 {\n  height: 1rem;\n}\n.h-40 {\n  height: 10rem;\n}\n.h-44 {\n  height: 11rem;\n}\n.h-5 {\n  height: 1.25rem;\n}\n.h-6 {\n  height: 1.5rem;\n}\n.h-64 {\n  height: 16rem;\n}\n.h-72 {\n  height: 18rem;\n}\n.h-8 {\n  height: 2rem;\n}\n.h-\\[60px\\] {\n  height: 60px;\n}\n.h-full {\n  height: 100%;\n}\n.h-screen {\n  height: 100vh;\n}\n.max-h-\\[560px\\] {\n  max-height: 560px;\n}\n.min-h-screen {\n  min-height: 100vh;\n}\n.w-10 {\n  width: 2.5rem;\n}\n.w-12 {\n  width: 3rem;\n}\n.w-16 {\n  width: 4rem;\n}\n.w-20 {\n  width: 5rem;\n}\n.w-24 {\n  width: 6rem;\n}\n.w-28 {\n  width: 7rem;\n}\n.w-3 {\n  width: 0.75rem;\n}\n.w-32 {\n  width: 8rem;\n}\n.w-36 {\n  width: 9rem;\n}\n.w-4 {\n  width: 1rem;\n}\n.w-40 {\n  width: 10rem;\n}\n.w-44 {\n  width: 11rem;\n}\n.w-48 {\n  width: 12rem;\n}\n.w-5 {\n  width: 1.25rem;\n}\n.w-6 {\n  width: 1.5rem;\n}\n.w-60 {\n  width: 15rem;\n}\n.w-64 {\n  width: 16rem;\n}\n.w-72 {\n  width: 18rem;\n}\n.w-8 {\n  width: 2rem;\n}\n.w-80 {\n  width: 20rem;\n}\n.w-96 {\n  width: 24rem;\n}\n.w-\\[1100px\\] {\n  width: 1100px;\n}\n.w-\\[200px\\] {\n  width: 200px;\n}\n.w-auto {\n  width: auto;\n}\n.w-full {\n  width: 100%;\n}\n.w-screen {\n  width: 100vw;\n}\n.min-w-full {\n  min-width: 100%;\n}\n.max-w-xl {\n  max-width: 36rem;\n}\n.flex-1 {\n  flex: 1 1 0%;\n}\n.flex-shrink-0 {\n  flex-shrink: 0;\n}\n.origin-top-right {\n  transform-origin: top right;\n}\n.transform {\n  transform: translate(var(--tw-translate-x), var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));\n}\n@keyframes spin {\n\n  to {\n    transform: rotate(360deg);\n  }\n}\n.animate-spin {\n  animation: spin 1s linear infinite;\n}\n.cursor-pointer {\n  cursor: pointer;\n}\n.list-disc {\n  list-style-type: disc;\n}\n.appearance-none {\n  -webkit-appearance: none;\n     -moz-appearance: none;\n          appearance: none;\n}\n.grid-cols-1 {\n  grid-template-columns: repeat(1, minmax(0, 1fr));\n}\n.grid-cols-5 {\n  grid-template-columns: repeat(5, minmax(0, 1fr));\n}\n.flex-col {\n  flex-direction: column;\n}\n.flex-wrap {\n  flex-wrap: wrap;\n}\n.items-start {\n  align-items: flex-start;\n}\n.items-center {\n  align-items: center;\n}\n.justify-end {\n  justify-content: flex-end;\n}\n.justify-center {\n  justify-content: center;\n}\n.justify-between {\n  justify-content: space-between;\n}\n.gap-2 {\n  gap: 0.5rem;\n}\n.gap-4 {\n  gap: 1rem;\n}\n.gap-8 {\n  gap: 2rem;\n}\n.gap-x-2 {\n  -moz-column-gap: 0.5rem;\n       column-gap: 0.5rem;\n}\n.gap-x-4 {\n  -moz-column-gap: 1rem;\n       column-gap: 1rem;\n}\n.gap-y-2 {\n  row-gap: 0.5rem;\n}\n.gap-y-6 {\n  row-gap: 1.5rem;\n}\n.-space-x-px > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(-1px * var(--tw-space-x-reverse));\n  margin-left: calc(-1px * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-1 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(0.25rem * var(--tw-space-x-reverse));\n  margin-left: calc(0.25rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-2 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(0.5rem * var(--tw-space-x-reverse));\n  margin-left: calc(0.5rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-4 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(1rem * var(--tw-space-x-reverse));\n  margin-left: calc(1rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-6 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(1.5rem * var(--tw-space-x-reverse));\n  margin-left: calc(1.5rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-7 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(1.75rem * var(--tw-space-x-reverse));\n  margin-left: calc(1.75rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-x-8 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-x-reverse: 0;\n  margin-right: calc(2rem * var(--tw-space-x-reverse));\n  margin-left: calc(2rem * calc(1 - var(--tw-space-x-reverse)));\n}\n.space-y-1 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-y-reverse: 0;\n  margin-top: calc(0.25rem * calc(1 - var(--tw-space-y-reverse)));\n  margin-bottom: calc(0.25rem * var(--tw-space-y-reverse));\n}\n.space-y-4 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-y-reverse: 0;\n  margin-top: calc(1rem * calc(1 - var(--tw-space-y-reverse)));\n  margin-bottom: calc(1rem * var(--tw-space-y-reverse));\n}\n.space-y-6 > :not([hidden]) ~ :not([hidden]) {\n  --tw-space-y-reverse: 0;\n  margin-top: calc(1.5rem * calc(1 - var(--tw-space-y-reverse)));\n  margin-bottom: calc(1.5rem * var(--tw-space-y-reverse));\n}\n.divide-y > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-y-reverse: 0;\n  border-top-width: calc(1px * calc(1 - var(--tw-divide-y-reverse)));\n  border-bottom-width: calc(1px * var(--tw-divide-y-reverse));\n}\n.divide-gray-100 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(243 244 246 / var(--tw-divide-opacity));\n}\n.divide-gray-200 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(229 231 235 / var(--tw-divide-opacity));\n}\n.divide-gray-300 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(209 213 219 / var(--tw-divide-opacity));\n}\n.divide-gray-400 > :not([hidden]) ~ :not([hidden]) {\n  --tw-divide-opacity: 1;\n  border-color: rgb(156 163 175 / var(--tw-divide-opacity));\n}\n.overflow-hidden {\n  overflow: hidden;\n}\n.overflow-x-auto {\n  overflow-x: auto;\n}\n.overflow-y-auto {\n  overflow-y: auto;\n}\n.overflow-y-scroll {\n  overflow-y: scroll;\n}\n.whitespace-nowrap {\n  white-space: nowrap;\n}\n.whitespace-pre {\n  white-space: pre;\n}\n.whitespace-pre-wrap {\n  white-space: pre-wrap;\n}\n.rounded {\n  border-radius: 0.25rem;\n}\n.rounded-full {\n  border-radius: 9999px;\n}\n.rounded-lg {\n  border-radius: 0.5rem;\n}\n.rounded-md {\n  border-radius: 0.375rem;\n}\n.rounded-sm {\n  border-radius: 0.125rem;\n}\n.rounded-l-md {\n  border-top-left-radius: 0.375rem;\n  border-bottom-left-radius: 0.375rem;\n}\n.rounded-r-md {\n  border-top-right-radius: 0.375rem;\n  border-bottom-right-radius: 0.375rem;\n}\n.border {\n  border-width: 1px;\n}\n.border-b {\n  border-bottom-width: 1px;\n}\n.border-b-2 {\n  border-bottom-width: 2px;\n}\n.border-t {\n  border-top-width: 1px;\n}\n.border-gray-200 {\n  --tw-border-opacity: 1;\n  border-color: rgb(229 231 235 / var(--tw-border-opacity));\n}\n.border-gray-300 {\n  --tw-border-opacity: 1;\n  border-color: rgb(209 213 219 / var(--tw-border-opacity));\n}\n.border-gray-400 {\n  --tw-border-opacity: 1;\n  border-color: rgb(156 163 175 / var(--tw-border-opacity));\n}\n.border-gray-500 {\n  --tw-border-opacity: 1;\n  border-color: rgb(107 114 128 / var(--tw-border-opacity));\n}\n.border-gray-900 {\n  --tw-border-opacity: 1;\n  border-color: rgb(17 24 39 / var(--tw-border-opacity));\n}\n.border-indigo-500 {\n  --tw-border-opacity: 1;\n  border-color: rgb(99 102 241 / var(--tw-border-opacity));\n}\n.border-red-400 {\n  --tw-border-opacity: 1;\n  border-color: rgb(248 113 113 / var(--tw-border-opacity));\n}\n.border-red-500 {\n  --tw-border-opacity: 1;\n  border-color: rgb(239 68 68 / var(--tw-border-opacity));\n}\n.border-rose-600 {\n  --tw-border-opacity: 1;\n  border-color: rgb(225 29 72 / var(--tw-border-opacity));\n}\n.border-transparent {\n  border-color: transparent;\n}\n.bg-blue-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(219 234 254 / var(--tw-bg-opacity));\n}\n.bg-blue-300 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(147 197 253 / var(--tw-bg-opacity));\n}\n.bg-gray-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(243 244 246 / var(--tw-bg-opacity));\n}\n.bg-gray-200 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(229 231 235 / var(--tw-bg-opacity));\n}\n.bg-gray-300 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(209 213 219 / var(--tw-bg-opacity));\n}\n.bg-gray-50 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(249 250 251 / var(--tw-bg-opacity));\n}\n.bg-gray-600 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(75 85 99 / var(--tw-bg-opacity));\n}\n.bg-green-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(220 252 231 / var(--tw-bg-opacity));\n}\n.bg-indigo-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(224 231 255 / var(--tw-bg-opacity));\n}\n.bg-indigo-50 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(238 242 255 / var(--tw-bg-opacity));\n}\n.bg-indigo-600 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(79 70 229 / var(--tw-bg-opacity));\n}\n.bg-red-200 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 202 202 / var(--tw-bg-opacity));\n}\n.bg-red-50 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 242 242 / var(--tw-bg-opacity));\n}\n.bg-red-500 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(239 68 68 / var(--tw-bg-opacity));\n}\n.bg-transparent {\n  background-color: transparent;\n}\n.bg-white {\n  --tw-bg-opacity: 1;\n  background-color: rgb(255 255 255 / var(--tw-bg-opacity));\n}\n.bg-yellow-100 {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 249 195 / var(--tw-bg-opacity));\n}\n.fill-blue-600 {\n  fill: #2563eb;\n}\n.object-contain {\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.p-0 {\n  padding: 0px;\n}\n.p-2 {\n  padding: 0.5rem;\n}\n.p-4 {\n  padding: 1rem;\n}\n.p-6 {\n  padding: 1.5rem;\n}\n.px-1 {\n  padding-left: 0.25rem;\n  padding-right: 0.25rem;\n}\n.px-10 {\n  padding-left: 2.5rem;\n  padding-right: 2.5rem;\n}\n.px-2 {\n  padding-left: 0.5rem;\n  padding-right: 0.5rem;\n}\n.px-2\\.5 {\n  padding-left: 0.625rem;\n  padding-right: 0.625rem;\n}\n.px-3 {\n  padding-left: 0.75rem;\n  padding-right: 0.75rem;\n}\n.px-4 {\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n.px-6 {\n  padding-left: 1.5rem;\n  padding-right: 1.5rem;\n}\n.py-0 {\n  padding-top: 0px;\n  padding-bottom: 0px;\n}\n.py-0\\.5 {\n  padding-top: 0.125rem;\n  padding-bottom: 0.125rem;\n}\n.py-1 {\n  padding-top: 0.25rem;\n  padding-bottom: 0.25rem;\n}\n.py-1\\.5 {\n  padding-top: 0.375rem;\n  padding-bottom: 0.375rem;\n}\n.py-12 {\n  padding-top: 3rem;\n  padding-bottom: 3rem;\n}\n.py-2 {\n  padding-top: 0.5rem;\n  padding-bottom: 0.5rem;\n}\n.py-3 {\n  padding-top: 0.75rem;\n  padding-bottom: 0.75rem;\n}\n.py-3\\.5 {\n  padding-top: 0.875rem;\n  padding-bottom: 0.875rem;\n}\n.py-4 {\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n}\n.py-6 {\n  padding-top: 1.5rem;\n  padding-bottom: 1.5rem;\n}\n.py-8 {\n  padding-top: 2rem;\n  padding-bottom: 2rem;\n}\n.pb-10 {\n  padding-bottom: 2.5rem;\n}\n.pb-20 {\n  padding-bottom: 5rem;\n}\n.pb-4 {\n  padding-bottom: 1rem;\n}\n.pl-1 {\n  padding-left: 0.25rem;\n}\n.pl-10 {\n  padding-left: 2.5rem;\n}\n.pl-3 {\n  padding-left: 0.75rem;\n}\n.pl-4 {\n  padding-left: 1rem;\n}\n.pl-5 {\n  padding-left: 1.25rem;\n}\n.pr-10 {\n  padding-right: 2.5rem;\n}\n.pr-3 {\n  padding-right: 0.75rem;\n}\n.pt-4 {\n  padding-top: 1rem;\n}\n.pt-5 {\n  padding-top: 1.25rem;\n}\n.text-left {\n  text-align: left;\n}\n.text-center {\n  text-align: center;\n}\n.text-right {\n  text-align: right;\n}\n.align-top {\n  vertical-align: top;\n}\n.align-middle {\n  vertical-align: middle;\n}\n.text-2xl {\n  font-size: 1.5rem;\n  line-height: 2rem;\n}\n.text-3xl {\n  font-size: 1.875rem;\n  line-height: 2.25rem;\n}\n.text-\\[10px\\] {\n  font-size: 10px;\n}\n.text-base {\n  font-size: 1rem;\n  line-height: 1.5rem;\n}\n.text-lg {\n  font-size: 1.125rem;\n  line-height: 1.75rem;\n}\n.text-sm {\n  font-size: 0.875rem;\n  line-height: 1.25rem;\n}\n.text-xl {\n  font-size: 1.25rem;\n  line-height: 1.75rem;\n}\n.text-xs {\n  font-size: 0.75rem;\n  line-height: 1rem;\n}\n.font-bold {\n  font-weight: 700;\n}\n.font-extrabold {\n  font-weight: 800;\n}\n.font-medium {\n  font-weight: 500;\n}\n.font-semibold {\n  font-weight: 600;\n}\n.uppercase {\n  text-transform: uppercase;\n}\n.leading-6 {\n  line-height: 1.5rem;\n}\n.leading-7 {\n  line-height: 1.75rem;\n}\n.leading-8 {\n  line-height: 2rem;\n}\n.leading-9 {\n  line-height: 2.25rem;\n}\n.tracking-wider {\n  letter-spacing: 0.05em;\n}\n.text-black {\n  --tw-text-opacity: 1;\n  color: rgb(0 0 0 / var(--tw-text-opacity));\n}\n.text-blue-500 {\n  --tw-text-opacity: 1;\n  color: rgb(59 130 246 / var(--tw-text-opacity));\n}\n.text-blue-700 {\n  --tw-text-opacity: 1;\n  color: rgb(29 78 216 / var(--tw-text-opacity));\n}\n.text-gray-200 {\n  --tw-text-opacity: 1;\n  color: rgb(229 231 235 / var(--tw-text-opacity));\n}\n.text-gray-300 {\n  --tw-text-opacity: 1;\n  color: rgb(209 213 219 / var(--tw-text-opacity));\n}\n.text-gray-400 {\n  --tw-text-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-text-opacity));\n}\n.text-gray-500 {\n  --tw-text-opacity: 1;\n  color: rgb(107 114 128 / var(--tw-text-opacity));\n}\n.text-gray-600 {\n  --tw-text-opacity: 1;\n  color: rgb(75 85 99 / var(--tw-text-opacity));\n}\n.text-gray-700 {\n  --tw-text-opacity: 1;\n  color: rgb(55 65 81 / var(--tw-text-opacity));\n}\n.text-gray-800 {\n  --tw-text-opacity: 1;\n  color: rgb(31 41 55 / var(--tw-text-opacity));\n}\n.text-gray-900 {\n  --tw-text-opacity: 1;\n  color: rgb(17 24 39 / var(--tw-text-opacity));\n}\n.text-green-700 {\n  --tw-text-opacity: 1;\n  color: rgb(21 128 61 / var(--tw-text-opacity));\n}\n.text-green-800 {\n  --tw-text-opacity: 1;\n  color: rgb(22 101 52 / var(--tw-text-opacity));\n}\n.text-indigo-600 {\n  --tw-text-opacity: 1;\n  color: rgb(79 70 229 / var(--tw-text-opacity));\n}\n.text-indigo-700 {\n  --tw-text-opacity: 1;\n  color: rgb(67 56 202 / var(--tw-text-opacity));\n}\n.text-red-400 {\n  --tw-text-opacity: 1;\n  color: rgb(248 113 113 / var(--tw-text-opacity));\n}\n.text-red-500 {\n  --tw-text-opacity: 1;\n  color: rgb(239 68 68 / var(--tw-text-opacity));\n}\n.text-red-600 {\n  --tw-text-opacity: 1;\n  color: rgb(220 38 38 / var(--tw-text-opacity));\n}\n.text-red-700 {\n  --tw-text-opacity: 1;\n  color: rgb(185 28 28 / var(--tw-text-opacity));\n}\n.text-red-800 {\n  --tw-text-opacity: 1;\n  color: rgb(153 27 27 / var(--tw-text-opacity));\n}\n.text-rose-600 {\n  --tw-text-opacity: 1;\n  color: rgb(225 29 72 / var(--tw-text-opacity));\n}\n.text-white {\n  --tw-text-opacity: 1;\n  color: rgb(255 255 255 / var(--tw-text-opacity));\n}\n.text-yellow-800 {\n  --tw-text-opacity: 1;\n  color: rgb(133 77 14 / var(--tw-text-opacity));\n}\n.placeholder-gray-400::-moz-placeholder {\n  --tw-placeholder-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-placeholder-opacity));\n}\n.placeholder-gray-400:-ms-input-placeholder {\n  --tw-placeholder-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-placeholder-opacity));\n}\n.placeholder-gray-400::placeholder {\n  --tw-placeholder-opacity: 1;\n  color: rgb(156 163 175 / var(--tw-placeholder-opacity));\n}\n.opacity-25 {\n  opacity: 0.25;\n}\n.opacity-75 {\n  opacity: 0.75;\n}\n.shadow {\n  --tw-shadow: 0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1);\n  --tw-shadow-colored: 0 1px 3px 0 var(--tw-shadow-color), 0 1px 2px -1px var(--tw-shadow-color);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: var(--tw-ring-offset-shadow, 0 0 rgba(0,0,0,0)), var(--tw-ring-shadow, 0 0 rgba(0,0,0,0)), var(--tw-shadow);\n}\n.shadow-lg {\n  --tw-shadow: 0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1);\n  --tw-shadow-colored: 0 10px 15px -3px var(--tw-shadow-color), 0 4px 6px -4px var(--tw-shadow-color);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: var(--tw-ring-offset-shadow, 0 0 rgba(0,0,0,0)), var(--tw-ring-shadow, 0 0 rgba(0,0,0,0)), var(--tw-shadow);\n}\n.shadow-sm {\n  --tw-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);\n  --tw-shadow-colored: 0 1px 2px 0 var(--tw-shadow-color);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: 0 0 rgba(0,0,0,0), 0 0 rgba(0,0,0,0), var(--tw-shadow);\n  box-shadow: var(--tw-ring-offset-shadow, 0 0 rgba(0,0,0,0)), var(--tw-ring-shadow, 0 0 rgba(0,0,0,0)), var(--tw-shadow);\n}\n.ring-1 {\n  --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);\n  --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 rgba(0,0,0,0));\n}\n.ring-black {\n  --tw-ring-opacity: 1;\n  --tw-ring-color: rgb(0 0 0 / var(--tw-ring-opacity));\n}\n.ring-opacity-5 {\n  --tw-ring-opacity: 0.05;\n}\n.filter {\n  filter: var(--tw-blur) var(--tw-brightness) var(--tw-contrast) var(--tw-grayscale) var(--tw-hue-rotate) var(--tw-invert) var(--tw-saturate) var(--tw-sepia) var(--tw-drop-shadow);\n}\n.transition {\n  transition-property: color, background-color, border-color, fill, stroke, opacity, box-shadow, transform, filter, -webkit-text-decoration-color, -webkit-backdrop-filter;\n  transition-property: color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter;\n  transition-property: color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter, -webkit-text-decoration-color, -webkit-backdrop-filter;\n  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n  transition-duration: 150ms;\n}\n\nhtml,\nbody {\n  padding: 0;\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans,\n    Helvetica Neue, sans-serif;\n}\n\na {\n  color: inherit;\n  text-decoration: none;\n}\n\n* {\n  box-sizing: border-box;\n}\n\n.hover\\:border-gray-300:hover {\n  --tw-border-opacity: 1;\n  border-color: rgb(209 213 219 / var(--tw-border-opacity));\n}\n\n.hover\\:bg-blue-100:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(219 234 254 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-100:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(243 244 246 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-200:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(229 231 235 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-300:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(209 213 219 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-gray-50:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(249 250 251 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-indigo-300:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(165 180 252 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-indigo-700:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(67 56 202 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-red-200:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(254 202 202 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-red-300:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(252 165 165 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-red-400:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(248 113 113 / var(--tw-bg-opacity));\n}\n\n.hover\\:bg-rose-50:hover {\n  --tw-bg-opacity: 1;\n  background-color: rgb(255 241 242 / var(--tw-bg-opacity));\n}\n\n.hover\\:text-blue-300:hover {\n  --tw-text-opacity: 1;\n  color: rgb(147 197 253 / var(--tw-text-opacity));\n}\n\n.hover\\:text-gray-700:hover {\n  --tw-text-opacity: 1;\n  color: rgb(55 65 81 / var(--tw-text-opacity));\n}\n\n.hover\\:text-gray-800:hover {\n  --tw-text-opacity: 1;\n  color: rgb(31 41 55 / var(--tw-text-opacity));\n}\n\n.hover\\:text-gray-900:hover {\n  --tw-text-opacity: 1;\n  color: rgb(17 24 39 / var(--tw-text-opacity));\n}\n\n.hover\\:underline:hover {\n  -webkit-text-decoration-line: underline;\n          text-decoration-line: underline;\n}\n\n.focus\\:border-indigo-500:focus {\n  --tw-border-opacity: 1;\n  border-color: rgb(99 102 241 / var(--tw-border-opacity));\n}\n\n.focus\\:border-red-500:focus {\n  --tw-border-opacity: 1;\n  border-color: rgb(239 68 68 / var(--tw-border-opacity));\n}\n\n.focus\\:outline-none:focus {\n  outline: 2px solid transparent;\n  outline-offset: 2px;\n}\n\n.focus\\:ring-2:focus {\n  --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);\n  --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), 0 0 rgba(0,0,0,0);\n  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 rgba(0,0,0,0));\n}\n\n.focus\\:ring-indigo-500:focus {\n  --tw-ring-opacity: 1;\n  --tw-ring-color: rgb(99 102 241 / var(--tw-ring-opacity));\n}\n\n.focus\\:ring-red-500:focus {\n  --tw-ring-opacity: 1;\n  --tw-ring-color: rgb(239 68 68 / var(--tw-ring-opacity));\n}\n\n.focus\\:ring-offset-2:focus {\n  --tw-ring-offset-width: 2px;\n}\n\n.focus\\:ring-offset-gray-100:focus {\n  --tw-ring-offset-color: #f3f4f6;\n}\n\n.disabled\\:opacity-30:disabled {\n  opacity: 0.3;\n}\n\n.disabled\\:opacity-40:disabled {\n  opacity: 0.4;\n}\n\n.disabled\\:opacity-50:disabled {\n  opacity: 0.5;\n}\n\n@media (prefers-color-scheme: dark) {\n\n  .dark\\:text-gray-600 {\n    --tw-text-opacity: 1;\n    color: rgb(75 85 99 / var(--tw-text-opacity));\n  }\n}\n\n@media (min-width: 640px) {\n\n  .sm\\:col-span-4 {\n    grid-column: span 4 / span 4;\n  }\n\n  .sm\\:col-span-6 {\n    grid-column: span 6 / span 6;\n  }\n\n  .sm\\:col-start-1 {\n    grid-column-start: 1;\n  }\n\n  .sm\\:-mx-6 {\n    margin-left: -1.5rem;\n    margin-right: -1.5rem;\n  }\n\n  .sm\\:mx-auto {\n    margin-left: auto;\n    margin-right: auto;\n  }\n\n  .sm\\:mt-0 {\n    margin-top: 0px;\n  }\n\n  .sm\\:block {\n    display: block;\n  }\n\n  .sm\\:w-full {\n    width: 100%;\n  }\n\n  .sm\\:max-w-lg {\n    max-width: 32rem;\n  }\n\n  .sm\\:max-w-md {\n    max-width: 28rem;\n  }\n\n  .sm\\:grid-cols-12 {\n    grid-template-columns: repeat(12, minmax(0, 1fr));\n  }\n\n  .sm\\:grid-cols-6 {\n    grid-template-columns: repeat(6, minmax(0, 1fr));\n  }\n\n  .sm\\:rounded-lg {\n    border-radius: 0.5rem;\n  }\n\n  .sm\\:px-10 {\n    padding-left: 2.5rem;\n    padding-right: 2.5rem;\n  }\n\n  .sm\\:px-6 {\n    padding-left: 1.5rem;\n    padding-right: 1.5rem;\n  }\n\n  .sm\\:pl-6 {\n    padding-left: 1.5rem;\n  }\n\n  .sm\\:text-sm {\n    font-size: 0.875rem;\n    line-height: 1.25rem;\n  }\n\n  .sm\\:text-xs {\n    font-size: 0.75rem;\n    line-height: 1rem;\n  }\n}\n\n@media (min-width: 768px) {\n\n  .md\\:col-span-2 {\n    grid-column: span 2 / span 2;\n  }\n}\n\n@media (min-width: 1024px) {\n\n  .lg\\:-mx-8 {\n    margin-left: -2rem;\n    margin-right: -2rem;\n  }\n\n  .lg\\:px-8 {\n    padding-left: 2rem;\n    padding-right: 2rem;\n  }\n}\n',"",{version:3,sources:["webpack://./frontend/src/globals.css","<no source>"],names:[],mappings:"AAAA;;CAAc,CAAd;;;CAAc;;AAAd;;;EAAA,sBAAc,EAAd,MAAc;EAAd,eAAc,EAAd,MAAc;EAAd,mBAAc,EAAd,MAAc;EAAd,qBAAc,EAAd,MAAc;AAAA;;AAAd;;EAAA,gBAAc;AAAA;;AAAd;;;;;;;CAAc;;AAAd;EAAA,gBAAc,EAAd,MAAc;EAAd,8BAAc,EAAd,MAAc;EAAd,gBAAc,EAAd,MAAc;EAAd,cAAc;KAAd,WAAc,EAAd,MAAc;EAAd,wRAAc,EAAd,MAAc;EAAd,6BAAc,EAAd,MAAc;EAAd,+BAAc,EAAd,MAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,SAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;AAAA;;AAAd;;;;CAAc;;AAAd;EAAA,SAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;EAAd,qBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,yCAAc;UAAd,iCAAc;AAAA;;AAAd;;CAAc;;AAAd;;;;;;EAAA,kBAAc;EAAd,oBAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,cAAc;EAAd,wBAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,mBAAc;AAAA;;AAAd;;;CAAc;;AAAd;;;;EAAA,+GAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,cAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,cAAc;EAAd,cAAc;EAAd,kBAAc;EAAd,wBAAc;AAAA;;AAAd;EAAA,eAAc;AAAA;;AAAd;EAAA,WAAc;AAAA;;AAAd;;;;CAAc;;AAAd;EAAA,cAAc,EAAd,MAAc;EAAd,qBAAc,EAAd,MAAc;EAAd,yBAAc,EAAd,MAAc;AAAA;;AAAd;;;;CAAc;;AAAd;;;;;EAAA,oBAAc,EAAd,MAAc;EAAd,eAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;EAAd,SAAc,EAAd,MAAc;EAAd,UAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,oBAAc;AAAA;;AAAd;;;CAAc;;AAAd;;;;EAAA,0BAAc,EAAd,MAAc;EAAd,6BAAc,EAAd,MAAc;EAAd,sBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,aAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,gBAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,wBAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,YAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,6BAAc,EAAd,MAAc;EAAd,oBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,wBAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,0BAAc,EAAd,MAAc;EAAd,aAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,kBAAc;AAAA;;AAAd;;CAAc;;AAAd;;;;;;;;;;;;;EAAA,SAAc;AAAA;;AAAd;EAAA,SAAc;EAAd,UAAc;AAAA;;AAAd;EAAA,UAAc;AAAA;;AAAd;;;EAAA,gBAAc;EAAd,SAAc;EAAd,UAAc;AAAA;;AAAd;;CAAc;;AAAd;EAAA,gBAAc;AAAA;;AAAd;;;CAAc;;AAAd;EAAA,UAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;EAAA,UAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;;EAAA,UAAc,EAAd,MAAc;EAAd,cAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,eAAc;AAAA;;AAAd;;CAAc;AAAd;EAAA,eAAc;AAAA;;AAAd;;;;CAAc;;AAAd;;;;;;;;EAAA,cAAc,EAAd,MAAc;EAAd,sBAAc,EAAd,MAAc;AAAA;;AAAd;;CAAc;;AAAd;;EAAA,eAAc;EAAd,YAAc;AAAA;;AAAd,wEAAc;AAAd;EAAA,aAAc;AAAA;;AAAd;EAAA,wBAAc;EAAd,wBAAc;EAAd,mBAAc;EAAd,mBAAc;EAAd,cAAc;EAAd,cAAc;EAAd,cAAc;EAAd,eAAc;EAAd,eAAc;EAAd,aAAc;EAAd,aAAc;EAAd,kBAAc;EAAd,sCAAc;EAAd,8BAAc;EAAd,6BAAc;EAAd,4BAAc;EAAd,eAAc;EAAd,oBAAc;EAAd,sBAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,kBAAc;EAAd,2BAAc;EAAd,4BAAc;EAAd,sCAAc;EAAd,0CAAc;EAAd,mCAAc;EAAd,8BAAc;EAAd,sCAAc;EAAd,YAAc;EAAd,kBAAc;EAAd,gBAAc;EAAd,iBAAc;EAAd,kBAAc;EAAd,cAAc;EAAd,gBAAc;EAAd,aAAc;EAAd,mBAAc;EAAd,qBAAc;EAAd,2BAAc;EAAd,yBAAc;EAAd,0BAAc;EAAd,2BAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,yBAAc;EAAd;AAAc;;AAAd;EAAA,wBAAc;EAAd,wBAAc;EAAd,mBAAc;EAAd,mBAAc;EAAd,cAAc;EAAd,cAAc;EAAd,cAAc;EAAd,eAAc;EAAd,eAAc;EAAd,aAAc;EAAd,aAAc;EAAd,kBAAc;EAAd,sCAAc;EAAd,8BAAc;EAAd,6BAAc;EAAd,4BAAc;EAAd,eAAc;EAAd,oBAAc;EAAd,sBAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,kBAAc;EAAd,2BAAc;EAAd,4BAAc;EAAd,sCAAc;EAAd,0CAAc;EAAd,mCAAc;EAAd,8BAAc;EAAd,sCAAc;EAAd,YAAc;EAAd,kBAAc;EAAd,gBAAc;EAAd,iBAAc;EAAd,kBAAc;EAAd,cAAc;EAAd,gBAAc;EAAd,aAAc;EAAd,mBAAc;EAAd,qBAAc;EAAd,2BAAc;EAAd,yBAAc;EAAd,0BAAc;EAAd,2BAAc;EAAd,uBAAc;EAAd,wBAAc;EAAd,yBAAc;EAAd;AAAc;AAEd;EAAA,kBAAmB;EAAnB,UAAmB;EAAnB,WAAmB;EAAnB,UAAmB;EAAnB,YAAmB;EAAnB,gBAAmB;EAAnB,sBAAmB;EAAnB,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,SAAmB;EAAnB;AAAmB;AAAnB;EAAA,QAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,gBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,gBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;;EAAA;IAAA;EAAmB;AAAA;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,wBAAmB;KAAnB,qBAAmB;UAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,uBAAmB;OAAnB;AAAmB;AAAnB;EAAA,qBAAmB;OAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,oDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,uDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,sDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,oDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,sDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,uDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,oDAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,+DAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,4DAAmB;EAAnB;AAAmB;AAAnB;EAAA,uBAAmB;EAAnB,8DAAmB;EAAnB;AAAmB;AAAnB;EAAA,wBAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,gCAAmB;EAAnB;AAAmB;AAAnB;EAAA,iCAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,sBAAmB;KAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,sBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,gBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,qBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,iBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,eAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,mBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA,kBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA,2BAAmB;EAAnB;AAAmB;AAAnB;EAAA,2BAAmB;EAAnB;AAAmB;AAAnB;EAAA,2BAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,0EAAmB;EAAnB,8FAAmB;EAAnB,kEAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,+EAAmB;EAAnB,mGAAmB;EAAnB,kEAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,0CAAmB;EAAnB,uDAAmB;EAAnB,kEAAmB;EAAnB,kEAAmB;EAAnB;AAAmB;AAAnB;EAAA,2GAAmB;EAAnB,yGAAmB;EAAnB,kFAAmB;EAAnB,kFAAmB;EAAnB;AAAmB;AAAnB;EAAA,oBAAmB;EAAnB;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA;AAAmB;AAAnB;EAAA,wKAAmB;EAAnB,wJAAmB;EAAnB,gNAAmB;EAAnB,wDAAmB;EAAnB;AAAmB;;AAEnB;;EAEE,UAAU;EACV,SAAS;EACT;8BAC4B;AAC9B;;AAEA;EACE,cAAc;EACd,qBAAqB;AACvB;;AAEA;EACE,sBAAsB;AACxB;;AAnBA;EAAA,uBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,mBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,wCCAA;UDAA;CCAA;;ADAA;EAAA,uBCAA;EDAA;CCAA;;ADAA;EAAA,uBCAA;EDAA;CCAA;;ADAA;EAAA,+BCAA;EDAA;CCAA;;ADAA;EAAA,4GCAA;EDAA,0GCAA;EDAA,mFCAA;EDAA,mFCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA,qBCAA;EDAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;EAAA;CCAA;;ADAA;;EAAA;IAAA,qBCAA;IDAA;GCAA;CAAA;;ADAA;;EAAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA,qBCAA;IDAA;GCAA;;EDAA;IAAA,kBCAA;IDAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA,qBCAA;IDAA;GCAA;;EDAA;IAAA,qBCAA;IDAA;GCAA;;EDAA;IAAA;GCAA;;EDAA;IAAA,oBCAA;IDAA;GCAA;;EDAA;IAAA,mBCAA;IDAA;GCAA;CAAA;;ADAA;;EAAA;IAAA;GCAA;CAAA;;ADAA;;EAAA;IAAA,mBCAA;IDAA;GCAA;;EDAA;IAAA,mBCAA;IDAA;GCAA;CAAA",sourcesContent:["@tailwind base;\n@tailwind components;\n@tailwind utilities;\n\nhtml,\nbody {\n  padding: 0;\n  margin: 0;\n  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans,\n    Helvetica Neue, sans-serif;\n}\n\na {\n  color: inherit;\n  text-decoration: none;\n}\n\n* {\n  box-sizing: border-box;\n}\n",null],sourceRoot:""}]);const m=l},2309:(e,t,a)=>{var r=a(6400),n=a(3581),o=a(3768),l=a(521),m=a(3200),s=a(9776),i=a.n(s),c=a(7294),d=(a(9657),a(3935)),u=a(3253),A=a.n(u),p=a(9655),g=a(3379),b=a.n(g),y=a(7795),x=a.n(y),f=a(569),E=a.n(f),h=a(3565),v=a.n(h),w=a(9216),N=a.n(w),B=a(4589),k=a.n(B),C=a(595),I={};I.styleTagTransform=k(),I.setAttributes=v(),I.insert=E().bind(null,"head"),I.domAPI=x(),I.insertStyleElement=N(),b()(C.Z,I),C.Z&&C.Z.locals&&C.Z.locals;var D=a(9250),j=a(9098),$=a(319),P=a(4316),T=Object.defineProperty,S=Object.getOwnPropertySymbols,F=Object.prototype.hasOwnProperty,O=Object.prototype.propertyIsEnumerable,M=(e,t,a)=>t in e?T(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,U=(e,t)=>{for(var a in t||(t={}))F.call(t,a)&&M(e,a,t[a]);if(S)for(var a of S(t))O.call(t,a)&&M(e,a,t[a]);return e};const R={},Y=j.Ps`
    fragment buildingFragment on Building {
  id
  name
  address
  rank
  rankText
  mcTypeId
  buildingTypeId
}
    `,V=j.Ps`
    fragment buildingNumberFragment on BuildingNumber {
  id
  buildingId
  name
  position
}
    `,q=j.Ps`
    fragment buildingTypeFragment on BuildingType {
  id
  name
}
    `,L=j.Ps`
    fragment commentFragment on Comment {
  id
  comment
  commentTypeId
  remark
}
    `,_=(j.Ps`
    fragment commentItemCodeFragment on CommentItemCode {
  id
  commentId
  itemCodeId
  position
}
    `,j.Ps`
    fragment commentTypeFragment on CommentType {
  id
  name
  position
}
    `),Z=j.Ps`
    fragment companyFragment on Company {
  id
  isOrderEach
  isOrderMonthly
  name
  zipcode
  address
  tel
  fax
  companyTypeIds
}
    `,z=j.Ps`
    fragment companyTypeFragment on CompanyType {
  id
  name
  position
}
    `,W=j.Ps`
    fragment imageCommentFragment on ImageComment {
  id
  projectImageId
  code
  text
}
    `,H=j.Ps`
    fragment itemFragment on Item {
  id
  name
  shortName
  price
  orderPrice
  companyTypeIds
  companyId
  order
  unitId
}
    `,G=j.Ps`
    fragment itemCodeFragment on ItemCode {
  id
  code
  name
}
    `,J=j.Ps`
    fragment jkkUserFragment on JkkUser {
  id
  email
  firstName
  lastName
  active
}
    `,K=j.Ps`
    fragment mcTypeFragment on McType {
  id
  name
}
    `,Q=j.Ps`
    fragment orderFragment on Order {
  id
  projectId
  companyId
  orderType
  isInvoice
  orderDate
  totalPrice
  exportCsvDate
}
    `,X=j.Ps`
    fragment paginationFragment on Pagination {
  resultsCount
  totalCount
  totalPages
  currentPage
  nextPage
  previousPage
  hasNextPage
  hasPreviousPage
}
    `,ee=j.Ps`
    fragment projectFragment on Project {
  id
  yearId
  buildingId
  buildingNumberId
  roomId
  userIds
  jkkUserId
  assesmentUserId
  constructionUserId
  totalItemPrice
  totalItemPriceRound
  tax
  totalPrice
  totalPriceInput
  name
  jkkStatus1
  jkkStatus2
  jkkStatus3
  jkkStatus4
  orderNumberType
  orderNumber
  assessmentDate
  assessmentUrl
  orderDate1
  orderMemo1
  orderDate2
  orderMemo2
  orderDate3
  orderMemo3
  electricStartDate1
  electricEndDate1
  waterStartDate1
  waterEndDate1
  electricStartDate2
  electricEndDate2
  waterStartDate2
  waterEndDate2
  electricStartDate3
  electricEndDate3
  waterStartDate3
  waterEndDate3
  closeTypes
  closeReason
  isVermiculite
  isFinishedVermiculite
  vermiculiteDate
  processCreatedDate
  beforeConstructionImageDate
  afterConstructionImageDate
  checklistFirstDate
  checklistLastDate
  address
  constructionStartDate
  constructionEndDate
  imageReportUrl
  isFaq
  isLackImage
  isCompleteImage
  submitDocumentDate
  availableDate
  livingYear
  livingMonth
  leaveDate
  isManagerAsbestosDocument1
  isManagerAsbestosDocument2
  isManagerAsbestosDocument3
  isOfficeworkAsbestosDocument1
  isOfficeworkAsbestosDocument2
  isOfficeworkAsbestosDocument3
  remark
  updatedItemsAt
  editUserId
  itemUploadDateText
  reportDateText
  checklistDateText
  lackImageDateText
  completeImageDateText
  invoiceDateText
  imageStatus
}
    `,te=j.Ps`
    fragment projectImageFragment on ProjectImage {
  id
  projectId
  roomTypeId
  imageType
  imageBefore
  imageAfter
  image
  comment
  position
}
    `,ae=j.Ps`
    fragment projectItemFragment on ProjectItem {
  id
  projectId
  itemId
  companyId
  code
  name
  count
  unitPrice
  amount
  roomTypeIds
  sequenceNumber
  updatedItemsAt
}
    `,re=j.Ps`
    fragment projectReportFragment on ProjectReport {
  id
  projectId
  worksheetBase
  worksheetItems
  worksheetImages
  worksheetChecklist
  spreadsheetId1
  spreadsheetId2
  spreadsheetId3
  spreadsheetId4
  spreadsheetId5
  spreadsheetId6
  spreadsheetId7
  spreadsheetDatetime1
  spreadsheetDatetime2
  spreadsheetDatetime3
  spreadsheetDatetime4
  spreadsheetDatetime5
  spreadsheetDatetime6
  spreadsheetDatetime7
  spreadsheetCheckReport1
  spreadsheetCheckReport2
}
    `,ne=j.Ps`
    fragment purchaseOrderFragment on PurchaseOrder {
  id
  projectId
  fileUrl
}
    `,oe=j.Ps`
    fragment roomFragment on Room {
  id
  buildingNumberId
  name
  position
  roomModelTypeId
  roomLayoutId
}
    `,le=j.Ps`
    fragment roomLayoutFragment on RoomLayout {
  id
  name
  price1
  price2
  position
}
    `,me=j.Ps`
    fragment roomModelTypeFragment on RoomModelType {
  id
  name
  position
}
    `,se=j.Ps`
    fragment roomTypeFragment on RoomType {
  id
  name
  position
}
    `,ie=j.Ps`
    fragment unitFragment on Unit {
  id
  name
}
    `,ce=j.Ps`
    fragment userFragment on User {
  id
  loginId
  email
  role
  firstName
  lastName
  tel
  active
  isAssesment
}
    `,de=j.Ps`
    fragment yearFragment on Year {
  id
  year
}
    `,ue=j.Ps`
    mutation signin($loginId: String!, $password: String!) {
  signin(input: {loginId: $loginId, password: $password}) {
    user {
      ...userFragment
    }
    error
  }
}
    ${ce}`,Ae=j.Ps`
    mutation signout {
  signout(input: {}) {
    result
  }
}
    `,pe=j.Ps`
    mutation createBuilding($attributes: BuildingAttributes!) {
  createBuilding(input: {attributes: $attributes}) {
    building {
      ...buildingFragment
    }
    error
  }
}
    ${Y}`,ge=j.Ps`
    mutation updateBuilding($id: ID!, $attributes: BuildingAttributes!) {
  updateBuilding(input: {id: $id, attributes: $attributes}) {
    building {
      ...buildingFragment
    }
    error
  }
}
    ${Y}`,be=j.Ps`
    mutation createBuildingNumber($id: ID!, $attributes: BuildingNumberAttributes!) {
  createBuildingNumber(input: {id: $id, attributes: $attributes}) {
    building {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
    error
  }
}
    ${Y}
${V}`,ye=j.Ps`
    mutation removeBuildingNumber($id: ID!) {
  removeBuildingNumber(input: {id: $id}) {
    building {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
    error
  }
}
    ${Y}
${V}`,xe=j.Ps`
    mutation updateBuildingNumber($id: ID!, $attributes: BuildingNumberAttributes!) {
  updateBuildingNumber(input: {id: $id, attributes: $attributes}) {
    building {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
    error
  }
}
    ${Y}
${V}`,fe=j.Ps`
    mutation addCommentCode($id: ID!, $itemCodeId: ID!) {
  addCommentCode(input: {id: $id, itemCodeId: $itemCodeId}) {
    comment {
      ...commentFragment
      itemCodes {
        ...itemCodeFragment
      }
    }
    error
  }
}
    ${L}
${G}`,Ee=j.Ps`
    mutation createComment($attributes: CommentAttributes!) {
  createComment(input: {attributes: $attributes}) {
    comment {
      ...commentFragment
    }
    error
  }
}
    ${L}`,he=j.Ps`
    mutation removeComment($id: ID!) {
  removeComment(input: {id: $id}) {
    comment {
      ...commentFragment
    }
    error
  }
}
    ${L}`,ve=j.Ps`
    mutation removeCommentCode($id: ID!, $itemCodeId: ID!) {
  removeCommentCode(input: {id: $id, itemCodeId: $itemCodeId}) {
    comment {
      ...commentFragment
      itemCodes {
        ...itemCodeFragment
      }
    }
    error
  }
}
    ${L}
${G}`,we=j.Ps`
    mutation updateComment($id: ID!, $attributes: CommentAttributes!) {
  updateComment(input: {id: $id, attributes: $attributes}) {
    comment {
      ...commentFragment
    }
    error
  }
}
    ${L}`,Ne=j.Ps`
    mutation createCommentTypeModel($attributes: CommentTypeAttributes!) {
  createCommentTypeModel(input: {attributes: $attributes}) {
    commentType {
      ...commentTypeFragment
    }
    error
  }
}
    ${_}`,Be=j.Ps`
    mutation removeCommentTypeModel($id: ID!) {
  removeCommentTypeModel(input: {id: $id}) {
    commentType {
      ...commentTypeFragment
    }
    error
  }
}
    ${_}`,ke=j.Ps`
    mutation updateCommentOrder($id: ID!, $position: Int!) {
  updateCommentOrder(input: {id: $id, position: $position}) {
    commentType {
      ...commentTypeFragment
      comments {
        ...commentFragment
      }
    }
    error
  }
}
    ${_}
${L}`,Ce=j.Ps`
    mutation updateCommentTypeModel($id: ID!, $attributes: CommentTypeAttributes!) {
  updateCommentTypeModel(input: {id: $id, attributes: $attributes}) {
    commentType {
      ...commentTypeFragment
    }
    error
  }
}
    ${_}`,Ie=j.Ps`
    mutation createCompany($attributes: CompanyAttributes!) {
  createCompany(input: {attributes: $attributes}) {
    company {
      ...companyFragment
    }
    error
  }
}
    ${Z}`,De=j.Ps`
    mutation removeCompany($id: ID!) {
  removeCompany(input: {id: $id}) {
    result
    error
  }
}
    `,je=j.Ps`
    mutation updateCompany($id: ID!, $attributes: CompanyAttributes!) {
  updateCompany(input: {id: $id, attributes: $attributes}) {
    company {
      ...companyFragment
      companyTypes {
        ...companyTypeFragment
      }
    }
    error
  }
}
    ${Z}
${z}`,$e=j.Ps`
    mutation createCompanyTypeModel($attributes: CompanyTypeAttributes!) {
  createCompanyTypeModel(input: {attributes: $attributes}) {
    companyType {
      ...companyTypeFragment
    }
    error
  }
}
    ${z}`,Pe=j.Ps`
    mutation removeCompanyTypeModel($id: ID!) {
  removeCompanyTypeModel(input: {id: $id}) {
    companyType {
      ...companyTypeFragment
    }
    error
  }
}
    ${z}`,Te=j.Ps`
    mutation updateCompanyTypeModel($id: ID!, $attributes: CompanyTypeAttributes!) {
  updateCompanyTypeModel(input: {id: $id, attributes: $attributes}) {
    companyType {
      ...companyTypeFragment
    }
    error
  }
}
    ${z}`,Se=j.Ps`
    mutation updateCompanyTypeOrder($id: ID!, $attributes: CompanyTypeAttributes!) {
  updateCompanyTypeOrder(input: {id: $id, attributes: $attributes}) {
    companyTypes {
      ...companyTypeFragment
    }
    error
  }
}
    ${z}`,Fe=j.Ps`
    mutation copyItems {
  copyItems(input: {}) {
    error
  }
}
    `,Oe=j.Ps`
    mutation createItem($code: String!, $year: String!, $attributes: ItemAttributes!) {
  createItem(input: {code: $code, year: $year, attributes: $attributes}) {
    item {
      ...itemFragment
    }
    error
  }
}
    ${H}`,Me=j.Ps`
    mutation updateItem($id: ID!, $attributes: ItemAttributes!) {
  updateItem(input: {id: $id, attributes: $attributes}) {
    item {
      ...itemFragment
      company {
        ...companyFragment
      }
    }
    error
  }
}
    ${H}
${Z}`,Ue=j.Ps`
    mutation createJkkUser($attributes: JkkUserAttributes!) {
  createJkkUser(input: {attributes: $attributes}) {
    jkkUser {
      ...jkkUserFragment
    }
    error
  }
}
    ${J}`,Re=j.Ps`
    mutation updateJkkUser($id: ID!, $attributes: JkkUserAttributes!) {
  updateJkkUser(input: {id: $id, attributes: $attributes}) {
    jkkUser {
      ...jkkUserFragment
    }
    error
  }
}
    ${J}`,Ye=j.Ps`
    mutation outputOrder($spreadsheetId: String!, $search: OrderSearch!) {
  outputOrder(input: {spreadsheetId: $spreadsheetId, search: $search}) {
    result
  }
}
    `,Ve=j.Ps`
    mutation clearProjectUser($id: ID!) {
  clearProjectUser(input: {id: $id}) {
    project {
      ...projectFragment
      editUser {
        ...userFragment
      }
    }
  }
}
    ${ee}
${ce}`,qe=j.Ps`
    mutation createProject($attributes: ProjectAttributes!) {
  createProject(input: {attributes: $attributes}) {
    project {
      ...projectFragment
    }
    error
  }
}
    ${ee}`,Le=j.Ps`
    mutation createProjectImageReport($id: ID!) {
  createProjectImageReport(input: {id: $id}) {
    project {
      ...projectFragment
    }
    error
  }
}
    ${ee}`,_e=j.Ps`
    mutation createProjectItem($id: ID!, $attributes: ProjectItemAttributes!) {
  createProjectItem(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      updatedItemsUser {
        ...userFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
        roomTypes {
          ...roomTypeFragment
        }
        updatedItemsUser {
          ...userFragment
        }
      }
    }
    error
  }
}
    ${ee}
${ce}
${ae}
${H}
${se}`,Ze=j.Ps`
    mutation createSpreadsheet($id: ID!, $reportType: String!, $attributes: ProjectReportAttributes!) {
  createSpreadsheet(
    input: {id: $id, reportType: $reportType, attributes: $attributes}
  ) {
    projectReport {
      ...projectReportFragment
    }
    error
  }
}
    ${re}`,ze=j.Ps`
    mutation editProjectUser($id: ID!) {
  editProjectUser(input: {id: $id}) {
    project {
      ...projectFragment
      editUser {
        ...userFragment
      }
    }
  }
}
    ${ee}
${ce}`,We=j.Ps`
    mutation importItem($id: ID!, $file: File!) {
  importItem(input: {id: $id, file: $file}) {
    project {
      ...projectFragment
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
      }
    }
    error
  }
}
    ${ee}
${ae}
${H}`,He=j.Ps`
    mutation removeProject($id: ID!) {
  removeProject(input: {id: $id}) {
    result
    error
  }
}
    `,Ge=j.Ps`
    mutation removeProjectItem($id: ID!) {
  removeProjectItem(input: {id: $id}) {
    project {
      ...projectFragment
    }
  }
}
    ${ee}`,Je=j.Ps`
    mutation updateProject($id: ID!, $attributes: ProjectAttributes!) {
  updateProject(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
    }
    error
  }
}
    ${ee}`;function Ke(e){const t=U(U({},R),e);return $.D(Je,t)}const Qe=j.Ps`
    mutation updateProjectItem($id: ID!, $attributes: ProjectItemAttributes!) {
  updateProjectItem(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      updatedItemsUser {
        ...userFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
        updatedItemsUser {
          ...userFragment
        }
      }
      purchaseOrders {
        ...purchaseOrderFragment
      }
    }
    error
  }
}
    ${ee}
${ce}
${ae}
${H}
${ne}`,Xe=j.Ps`
    mutation updateProjectItemsCompany($id: ID!, $companyTypeId: ID!, $companyId: ID!) {
  updateProjectItemsCompany(
    input: {id: $id, companyTypeId: $companyTypeId, companyId: $companyId}
  ) {
    project {
      ...projectFragment
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
      }
    }
    error
  }
}
    ${ee}
${ae}
${H}`,et=j.Ps`
    mutation updateProjectOrder($id: ID!, $attributes: OrderAttributes!) {
  updateProjectOrder(input: {id: $id, attributes: $attributes}) {
    order {
      ...orderFragment
    }
  }
}
    ${Q}`;j.Ps`
    mutation updateProjectOrderCsv($id: ID!) {
  updateProjectOrderCsv(input: {id: $id}) {
    order {
      ...orderFragment
    }
  }
}
    ${Q}`;const tt=j.Ps`
    mutation updateProjectReport($id: ID!, $attributes: ProjectReportAttributes!) {
  updateProjectReport(input: {id: $id, attributes: $attributes}) {
    projectReport {
      ...projectReportFragment
    }
    error
  }
}
    ${re}`,at=j.Ps`
    mutation updateProjectYear($id: ID!, $yearId: ID!) {
  updateProjectYear(input: {id: $id, yearId: $yearId}) {
    project {
      ...projectFragment
      year {
        ...yearFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
      }
      purchaseOrders {
        ...purchaseOrderFragment
      }
    }
    error
  }
}
    ${ee}
${de}
${ae}
${H}
${ne}`,rt=j.Ps`
    mutation addComment($id: ID!, $commentId: ID!) {
  addComment(input: {id: $id, commentId: $commentId}) {
    projectImage {
      ...projectImageFragment
      comments {
        ...commentFragment
      }
    }
  }
}
    ${te}
${L}`,nt=j.Ps`
    mutation addImageComment($id: ID!, $itemId: ID!) {
  addImageComment(input: {id: $id, itemId: $itemId}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${te}
${W}`;j.Ps`
    mutation addImageCommentAll($id: ID!, $commentId: ID!) {
  addImageCommentAll(input: {id: $id, commentId: $commentId}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${te}
${W}`;const ot=j.Ps`
    mutation createProjectImage($count: Int!, $attributes: ProjectImageAttributes!) {
  createProjectImage(input: {count: $count, attributes: $attributes}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${ee}
${te}`;function lt(e){const t=U(U({},R),e);return $.D(ot,t)}const mt=j.Ps`
    mutation removeProjectImageComment($id: ID!, $commentId: ID!) {
  removeProjectImageComment(input: {id: $id, commentId: $commentId}) {
    projectImage {
      ...projectImageFragment
      comments {
        ...commentFragment
      }
    }
  }
}
    ${te}
${L}`,st=j.Ps`
    mutation removeImageComment($id: ID!) {
  removeImageComment(input: {id: $id}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${te}
${W}`;j.Ps`
    mutation removeImageCommentAll($id: ID!) {
  removeImageCommentAll(input: {id: $id}) {
    projectImage {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
    }
  }
}
    ${te}
${W}`;const it=j.Ps`
    mutation removeProjectImage($id: ID!) {
  removeProjectImage(input: {id: $id}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${ee}
${te}`;function ct(e){const t=U(U({},R),e);return $.D(it,t)}const dt=j.Ps`
    mutation updateImageComment($id: ID!, $attributes: ImageCommentAttributes!) {
  updateImageComment(input: {id: $id, attributes: $attributes}) {
    imageComment {
      ...imageCommentFragment
    }
  }
}
    ${W}`,ut=j.Ps`
    mutation updateProjectImage($id: ID!, $attributes: ProjectImageAttributes!) {
  updateProjectImage(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${ee}
${te}`;function At(e){const t=U(U({},R),e);return $.D(ut,t)}const pt=j.Ps`
    mutation updateProjectImageOrder($id: ID!, $attributes: ProjectImageAttributes!) {
  updateProjectImageOrder(input: {id: $id, attributes: $attributes}) {
    project {
      ...projectFragment
      projectImages {
        ...projectImageFragment
      }
    }
    error
  }
}
    ${ee}
${te}`,gt=j.Ps`
    mutation copyRoom($id: ID!, $attributes: RoomAttributes!) {
  copyRoom(input: {id: $id, attributes: $attributes}) {
    buildingNumber {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
      }
    }
    error
  }
}
    ${V}
${oe}`;const bt=j.Ps`
    mutation createRoom($buildingNumberId: ID!, $attributes: RoomAttributes!) {
  createRoom(
    input: {buildingNumberId: $buildingNumberId, attributes: $attributes}
  ) {
    buildingNumber {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
      }
    }
    error
  }
}
    ${V}
${oe}`,yt=j.Ps`
    mutation removeRoom($id: ID!) {
  removeRoom(input: {id: $id}) {
    buildingNumber {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
        roomTypes {
          ...roomTypeFragment
        }
      }
    }
    error
  }
}
    ${V}
${oe}
${se}`,xt=j.Ps`
    mutation updateRoom($id: ID!, $attributes: RoomAttributes!, $roomTypeIds: [ID!]!) {
  updateRoom(input: {id: $id, attributes: $attributes, roomTypeIds: $roomTypeIds}) {
    room {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
    error
  }
}
    ${oe}
${se}`,ft=j.Ps`
    mutation createRoomLayout($attributes: RoomLayoutAttributes!) {
  createRoomLayout(input: {attributes: $attributes}) {
    roomLayout {
      ...roomLayoutFragment
    }
    error
  }
}
    ${le}`,Et=j.Ps`
    mutation updateRoomLayout($id: ID!, $attributes: RoomLayoutAttributes!) {
  updateRoomLayout(input: {id: $id, attributes: $attributes}) {
    roomLayout {
      ...roomLayoutFragment
    }
    error
  }
}
    ${le}`,ht=j.Ps`
    mutation updateRoomLayoutOrder($id: ID!, $attributes: RoomLayoutAttributes!) {
  updateRoomLayoutOrder(input: {id: $id, attributes: $attributes}) {
    roomLayouts {
      ...roomLayoutFragment
    }
    error
  }
}
    ${le}`,vt=j.Ps`
    mutation createRoomTypeModel($attributes: RoomTypeAttributes!) {
  createRoomTypeModel(input: {attributes: $attributes}) {
    roomType {
      ...roomTypeFragment
    }
    error
  }
}
    ${se}`,wt=j.Ps`
    mutation updateRoomTypeModel($id: ID!, $attributes: RoomTypeAttributes!) {
  updateRoomTypeModel(input: {id: $id, attributes: $attributes}) {
    roomType {
      ...roomTypeFragment
    }
    error
  }
}
    ${se}`,Nt=j.Ps`
    mutation updateRoomTypeOrder($id: ID!, $attributes: RoomTypeAttributes!) {
  updateRoomTypeOrder(input: {id: $id, attributes: $attributes}) {
    roomTypes {
      ...roomTypeFragment
    }
    error
  }
}
    ${se}`,Bt=j.Ps`
    mutation createUser($attributes: UserAttributes!) {
  createUser(input: {attributes: $attributes}) {
    user {
      ...userFragment
    }
    error
  }
}
    ${ce}`,kt=j.Ps`
    mutation updateUser($id: ID!, $attributes: UserAttributes!) {
  updateUser(input: {id: $id, attributes: $attributes}) {
    user {
      ...userFragment
    }
    error
  }
}
    ${ce}`;function Ct(e){const t=U(U({},R),e);return $.D(kt,t)}const It=j.Ps`
    query allBuildings {
  allBuildings {
    ...buildingFragment
    buildingNumbers {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
      }
    }
  }
}
    ${Y}
${V}
${oe}`,Dt=j.Ps`
    query building($id: ID!) {
  building(id: $id) {
    ...buildingFragment
    buildingNumbers {
      ...buildingNumberFragment
    }
  }
}
    ${Y}
${V}`;function jt(e){const t=U(U({},R),e);return P.a(Dt,t)}const $t=j.Ps`
    query buildingNumber($id: ID!) {
  buildingNumber(id: $id) {
    ...buildingNumberFragment
    rooms {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
  }
}
    ${V}
${oe}
${se}`,Pt=j.Ps`
    query buildings($page: Int, $perPage: Int, $search: BuildingSearch) {
  buildings(page: $page, perPage: $perPage, search: $search) {
    buildings {
      ...buildingFragment
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${Y}
${X}`,Tt=j.Ps`
    query room($id: ID!) {
  room(id: $id) {
    ...roomFragment
    roomTypes {
      ...roomTypeFragment
    }
  }
}
    ${oe}
${se}`,St=j.Ps`
    query buildingTypes {
  buildingTypes {
    ...buildingTypeFragment
  }
}
    ${q}`,Ft=j.Ps`
    query comment($id: ID!) {
  comment(id: $id) {
    ...commentFragment
    itemCodes {
      ...itemCodeFragment
    }
    commentType {
      ...commentTypeFragment
    }
  }
}
    ${L}
${G}
${_}`;function Ot(e){const t=U(U({},R),e);return P.a(Ft,t)}const Mt=j.Ps`
    query comments($search: CommentSearch) {
  comments(search: $search) {
    ...commentFragment
    commentType {
      ...commentTypeFragment
    }
    itemCodes {
      ...itemCodeFragment
    }
  }
}
    ${L}
${_}
${G}`;function Ut(e){const t=U(U({},R),e);return P.a(Mt,t)}const Rt=j.Ps`
    query commentType($id: ID!) {
  commentType(id: $id) {
    ...commentTypeFragment
    comments {
      ...commentFragment
    }
  }
}
    ${_}
${L}`;function Yt(e){const t=U(U({},R),e);return P.a(Rt,t)}const Vt=j.Ps`
    query commentTypes {
  commentTypes {
    ...commentTypeFragment
  }
}
    ${_}`;function qt(e){const t=U(U({},R),e);return P.a(Vt,t)}const Lt=j.Ps`
    query companies {
  companies {
    ...companyFragment
    companyTypes {
      ...companyTypeFragment
    }
  }
}
    ${Z}
${z}`;function _t(e){const t=U(U({},R),e);return P.a(Lt,t)}const Zt=j.Ps`
    query company($id: ID!) {
  company(id: $id) {
    ...companyFragment
  }
}
    ${Z}`,zt=j.Ps`
    query companyType($id: ID!) {
  companyType(id: $id) {
    ...companyTypeFragment
  }
}
    ${z}`,Wt=j.Ps`
    query companyTypes {
  companyTypes {
    ...companyTypeFragment
    companies {
      ...companyFragment
    }
  }
}
    ${z}
${Z}`;function Ht(e){const t=U(U({},R),e);return P.a(Wt,t)}const Gt=j.Ps`
    query allItems($search: ItemSearch) {
  allItems(search: $search) {
    ...itemFragment
    itemCode {
      ...itemCodeFragment
    }
  }
}
    ${H}
${G}`;function Jt(e){const t=U(U({},R),e);return P.a(Gt,t)}const Kt=j.Ps`
    query item($id: ID!) {
  item(id: $id) {
    ...itemFragment
    itemCode {
      ...itemCodeFragment
    }
    year {
      ...yearFragment
    }
    unit {
      ...unitFragment
    }
  }
}
    ${H}
${G}
${de}
${ie}`,Qt=j.Ps`
    query items($page: Int, $perPage: Int, $search: ItemSearch) {
  items(page: $page, perPage: $perPage, search: $search) {
    items {
      ...itemFragment
      itemCode {
        ...itemCodeFragment
      }
      year {
        ...yearFragment
      }
      company {
        ...companyFragment
      }
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${H}
${G}
${de}
${Z}
${X}`,Xt=j.Ps`
    query jkkUser($id: ID!) {
  jkkUser(id: $id) {
    ...jkkUserFragment
  }
}
    ${J}`,ea=j.Ps`
    query jkkUsers($search: JkkUserSearch) {
  jkkUsers(search: $search) {
    ...jkkUserFragment
  }
}
    ${J}`;function ta(e){const t=U(U({},R),e);return P.a(ea,t)}const aa=j.Ps`
    query mcTypes {
  mcTypes {
    ...mcTypeFragment
  }
}
    ${K}`,ra=j.Ps`
    query orders($page: Int, $perPage: Int, $search: OrderSearch) {
  orders(page: $page, perPage: $perPage, search: $search) {
    orders {
      ...orderFragment
      company {
        ...companyFragment
      }
      project {
        ...projectFragment
      }
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${Q}
${Z}
${ee}
${X}`,na=j.Ps`
    query project($id: ID!) {
  project(id: $id) {
    ...projectFragment
    year {
      ...yearFragment
    }
    projectItems {
      ...projectItemFragment
      item {
        ...itemFragment
      }
      roomTypes {
        ...roomTypeFragment
      }
      company {
        ...companyFragment
      }
    }
    purchaseOrders {
      ...purchaseOrderFragment
    }
    building {
      ...buildingFragment
      mcType {
        ...mcTypeFragment
      }
      buildingType {
        ...buildingTypeFragment
      }
    }
    buildingNumber {
      ...buildingNumberFragment
    }
    room {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
    projectImages {
      ...projectImageFragment
      imageComments {
        ...imageCommentFragment
      }
      comments {
        ...commentFragment
      }
    }
    projectReport {
      ...projectReportFragment
    }
    updatedItemsUser {
      ...userFragment
    }
    editUser {
      ...userFragment
    }
  }
}
    ${ee}
${de}
${ae}
${H}
${se}
${Z}
${ne}
${Y}
${K}
${q}
${V}
${oe}
${te}
${W}
${L}
${re}
${ce}`;function oa(e){const t=U(U({},R),e);return P.a(na,t)}const la=j.Ps`
    query projectItems($id: ID!, $orderItem: String, $order: String) {
  projectItems(id: $id, orderItem: $orderItem, order: $order) {
    ...projectItemFragment
    item {
      ...itemFragment
      companyTypes {
        ...companyTypeFragment
      }
    }
    roomTypes {
      ...roomTypeFragment
    }
    company {
      ...companyFragment
      companyTypes {
        ...companyTypeFragment
      }
    }
    selectableCompanies {
      ...companyFragment
    }
    updatedItemsUser {
      ...userFragment
    }
  }
}
    ${ae}
${H}
${z}
${se}
${Z}
${ce}`,ma=j.Ps`
    query projectOrders($id: ID!) {
  project(id: $id) {
    ...projectFragment
    ordersEach {
      ...orderFragment
      company {
        ...companyFragment
      }
    }
    ordersMonthly {
      ...orderFragment
      company {
        ...companyFragment
      }
    }
  }
}
    ${ee}
${Q}
${Z}`;function sa(e){const t=U(U({},R),e);return P.a(ma,t)}const ia=j.Ps`
    query projects($page: Int, $perPage: Int, $search: ProjectSearch) {
  projects(page: $page, perPage: $perPage, search: $search) {
    projects {
      ...projectFragment
      building {
        ...buildingFragment
      }
      buildingNumber {
        ...buildingNumberFragment
      }
      room {
        ...roomFragment
      }
    }
    pagination {
      ...paginationFragment
    }
  }
}
    ${ee}
${Y}
${V}
${oe}
${X}`;function ca(e){const t=U(U({},R),e);return P.a(ia,t)}const da=j.Ps`
    query roomLayout($id: ID!) {
  roomLayout(id: $id) {
    ...roomLayoutFragment
  }
}
    ${le}`,ua=j.Ps`
    query roomLayouts {
  roomLayouts {
    ...roomLayoutFragment
  }
}
    ${le}`;function Aa(e){const t=U(U({},R),e);return P.a(ua,t)}const pa=j.Ps`
    query roomModelTypes {
  roomModelTypes {
    ...roomModelTypeFragment
  }
}
    ${me}`,ga=j.Ps`
    query roomType($id: ID!) {
  roomType(id: $id) {
    ...roomTypeFragment
  }
}
    ${se}`;function ba(e){const t=U(U({},R),e);return P.a(ga,t)}const ya=j.Ps`
    query roomTypes {
  roomTypes {
    ...roomTypeFragment
  }
}
    ${se}`;function xa(e){const t=U(U({},R),e);return P.a(ya,t)}const fa=j.Ps`
    query units {
  units {
    ...unitFragment
  }
}
    ${ie}`;function Ea(e){const t=U(U({},R),e);return P.a(fa,t)}const ha=j.Ps`
    query currentUser {
  currentUser {
    ...userFragment
  }
}
    ${ce}`;function va(e){const t=U(U({},R),e);return P.a(ha,t)}const wa=j.Ps`
    query user($id: ID!) {
  user(id: $id) {
    ...userFragment
  }
}
    ${ce}`;function Na(e){const t=U(U({},R),e);return P.a(wa,t)}const Ba=j.Ps`
    query users($search: UserSearch) {
  users(search: $search) {
    ...userFragment
  }
}
    ${ce}`;function ka(e){const t=U(U({},R),e);return P.a(Ba,t)}const Ca=j.Ps`
    query years {
  years {
    ...yearFragment
  }
}
    ${de}`;function Ia(e){const t=U(U({},R),e);return P.a(Ca,t)}const Da=({className:e,type:t,disabled:a,loading:r,children:n,onClick:o})=>c.createElement("button",{type:t,className:`${e} disabled:opacity-40`,disabled:a,onClick:o},r&&c.createElement("svg",{className:"w-5 h-5 mr-3 -ml-1 text-white animate-spin",xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24"},c.createElement("circle",{className:"opacity-25",cx:"12",cy:"12",r:"10",stroke:"currentColor",strokeWidth:"4"}),c.createElement("path",{className:"opacity-75",fill:"currentColor",d:"M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"})),!r&&n),ja=({className:e})=>c.createElement("div",{className:"flex justify-center"},c.createElement("svg",{role:"status",className:`w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600 ${e}`,viewBox:"0 0 100 101",fill:"none",xmlns:"http://www.w3.org/2000/svg"},c.createElement("path",{d:"M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z",fill:"currentColor"}),c.createElement("path",{d:"M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z",fill:"currentFill"})));var $a,Pa=a(7536),Ta=a(582);function Sa(){return Sa=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Sa.apply(this,arguments)}const Fa=function(e){return c.createElement("svg",Sa({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Check_svg__w-6 Check_svg__h-6"},e),$a||($a=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0z"})))};var Oa;function Ma(){return Ma=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Ma.apply(this,arguments)}const Ua=function(e){return c.createElement("svg",Ma({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"ChevronRightIcon_svg__w-6 ChevronRightIcon_svg__h-6"},e),Oa||(Oa=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"m8.25 4.5 7.5 7.5-7.5 7.5"})))};var Ra;function Ya(){return Ya=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Ya.apply(this,arguments)}const Va=function(e){return c.createElement("svg",Ya({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"DocumentArrowDownload_svg__w-6 DocumentArrowDownload_svg__h-6"},e),Ra||(Ra=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m.75 12 3 3m0 0 3-3m-3 3v-6m-1.5-9H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9z"})))};var qa;function La(){return La=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},La.apply(this,arguments)}const _a=function(e){return c.createElement("svg",La({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"EllipseVertical_svg__w-6 EllipseVertical_svg__h-6"},e),qa||(qa=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M12 6.75a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5zm0 6a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5zm0 6a.75.75 0 1 1 0-1.5.75.75 0 0 1 0 1.5z"})))};var Za;function za(){return za=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},za.apply(this,arguments)}const Wa=function(e){return c.createElement("svg",za({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Minus_svg__w-6 Minus_svg__h-6"},e),Za||(Za=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M19.5 12h-15"})))};var Ha;function Ga(){return Ga=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Ga.apply(this,arguments)}const Ja=function(e){return c.createElement("svg",Ga({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Plus_svg__w-6 Plus_svg__h-6"},e),Ha||(Ha=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"M12 4.5v15m7.5-7.5h-15"})))};var Ka;function Qa(){return Qa=Object.assign?Object.assign.bind():function(e){for(var t=1;t<arguments.length;t++){var a=arguments[t];for(var r in a)Object.prototype.hasOwnProperty.call(a,r)&&(e[r]=a[r])}return e},Qa.apply(this,arguments)}const Xa=function(e){return c.createElement("svg",Qa({xmlns:"http://www.w3.org/2000/svg",fill:"none",viewBox:"0 0 24 24",strokeWidth:1.5,stroke:"currentColor",className:"Trash_svg__w-6 Trash_svg__h-6"},e),Ka||(Ka=c.createElement("path",{strokeLinecap:"round",strokeLinejoin:"round",d:"m14.74 9-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 0 1-2.244 2.077H8.084a2.25 2.25 0 0 1-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 0 0-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 0 1 3.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 0 0-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 0 0-7.5 0"})))},er=({building:e})=>{const{id:t}=(0,D.UO)();return c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/buildings",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"物件管理"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},e.name)))))},tr=()=>{const{id:e}=(0,D.UO)(),t=e=>location.pathname.indexOf(e)>-1;return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement(p.rU,{to:`/buildings/${e}/base${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("base")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報")),c.createElement(p.rU,{to:`/buildings/${e}/building_numbers${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("building_numbers")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"号棟")))))};var ar=Object.defineProperty,rr=Object.defineProperties,nr=Object.getOwnPropertyDescriptors,or=Object.getOwnPropertySymbols,lr=Object.prototype.hasOwnProperty,mr=Object.prototype.propertyIsEnumerable,sr=(e,t,a)=>t in e?ar(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,ir=(e,t)=>{for(var a in t||(t={}))lr.call(t,a)&&sr(e,a,t[a]);if(or)for(var a of or(t))mr.call(t,a)&&sr(e,a,t[a]);return e},cr=(e,t)=>rr(e,nr(t));const dr=e=>{const{id:t}=(0,D.UO)(),[a,{loading:r,data:n}]=function(e){const t=U(U({},R),e);return $.D(ge,t)}({onCompleted:()=>{Ta.ZP.success("保存しました")}}),{register:o,handleSubmit:l,formState:{isDirty:m,isValid:s,errors:i}}=(0,Pa.cI)({defaultValues:{name:e.building.name,buildingTypeId:e.building.buildingTypeId,mcTypeId:e.building.mcTypeId,address:e.building.address,rank:e.building.rank}});return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},e.building&&c.createElement(er,{building:e.building}),c.createElement(tr,null),(null==n?void 0:n.updateBuilding.error)&&c.createElement("p",{className:"text-red-500"},n.updateBuilding.error),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:l((e=>{a({variables:{id:t,attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",ir({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("name",{required:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"住所"),c.createElement("input",ir({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("address")))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"建物区分"),c.createElement("select",cr(ir({},o("buildingTypeId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),c.createElement("option",{value:""}),e.buildingTypes.map((e=>c.createElement("option",{value:e.id,key:`buildingType-${e.id}`},e.name))))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"管轄MC"),c.createElement("select",cr(ir({},o("mcTypeId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),c.createElement("option",{value:""}),e.mcTypes.map((e=>c.createElement("option",{value:e.id,key:`mcType-${e.id}`},e.name))))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"ランク"),c.createElement("select",cr(ir({},o("rank")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),c.createElement("option",{value:""}),c.createElement("option",{value:"a"},"A"),c.createElement("option",{value:"b"},"B"),c.createElement("option",{value:"c"},"C"),c.createElement("option",{value:"d"},"D"))),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!m||!s,loading:r},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))))},ur=()=>{const{id:e}=(0,D.UO)(),{data:{building:t=null}={}}=jt({variables:{id:e}}),{data:{buildingTypes:a=[]}={}}=function(e){const t=U(U({},R),void 0);return P.a(St,t)}(),{data:{mcTypes:r=[]}={}}=function(e){const t=U(U({},R),void 0);return P.a(aa,t)}();return t&&a&&r?c.createElement(dr,{building:t,buildingTypes:a,mcTypes:r}):null};var Ar=a(4649);const pr={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},gr=({buildingNumber:e,isOpen:t,handleClose:a})=>{const{id:r}=(0,D.UO)(),[n,{loading:o}]=function(e){const t=U(U({},R),e);return $.D(be,t)}({onCompleted:()=>{Ta.ZP.success("保存しました")}}),[l]=function(e){const t=U(U({},R),e);return $.D(xe,t)}({onCompleted:()=>{Ta.ZP.success("保存しました")}}),m=(0,Ar.TA)({enableReinitialize:!0,initialValues:{name:(null==e?void 0:e.name)||""},onSubmit:t=>{e?l({variables:{id:e.id,attributes:t}}):n({variables:{id:r,attributes:t}}),a(),m.resetForm()}});return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:t,onRequestClose:a,style:pr},c.createElement("h1",{className:"text-xl"},"号棟"),c.createElement("form",{className:"p-6 space-y-6",onSubmit:m.handleSubmit},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:m.values.name,onChange:m.handleChange,onBlur:m.handleBlur})),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:o},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))};var br=a(1330),yr=Object.defineProperty,xr=Object.defineProperties,fr=Object.getOwnPropertyDescriptors,Er=Object.getOwnPropertySymbols,hr=Object.prototype.hasOwnProperty,vr=Object.prototype.propertyIsEnumerable,wr=(e,t,a)=>t in e?yr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Nr=(e,t)=>{for(var a in t||(t={}))hr.call(t,a)&&wr(e,a,t[a]);if(Er)for(var a of Er(t))vr.call(t,a)&&wr(e,a,t[a]);return e},Br=(e,t)=>xr(e,fr(t));const kr=e=>{var t;const a=e.roomTypes.map((e=>({value:e.id,label:e.name}))),[r,{loading:n}]=function(e){const t=U(U({},R),e);return $.D(xt,t)}({onCompleted:()=>{Ta.ZP.success("保存しました")}}),{register:o,handleSubmit:l,setValue:m,watch:s,formState:{isDirty:i,isValid:d}}=(0,Pa.cI)({defaultValues:{name:e.room.name,roomModelTypeId:e.room.roomModelTypeId,roomLayoutId:e.room.roomLayoutId,roomTypeIds:null==(t=e.room.roomTypes)?void 0:t.map((e=>e.id))}}),u=s("roomTypeIds"),A=(0,c.useCallback)((()=>a.filter((e=>null==u?void 0:u.includes(e.value)))),[u]);return(0,c.useEffect)((()=>{var t;m("name",e.room.name),m("roomModelTypeId",e.room.roomModelTypeId),m("roomLayoutId",e.room.roomLayoutId),m("roomTypeIds",null==(t=e.room.roomTypes)?void 0:t.map((e=>e.id)))}),[e.room.id]),c.createElement("form",{className:"flex-1 px-4 py-2 ml-4 border border-gray-300",onSubmit:l((t=>{const a=t,n=a.roomTypeIds;delete a.roomTypeIds,r({variables:{id:e.room.id,attributes:a,roomTypeIds:n}})}))},c.createElement("div",{className:"divide-y divide-gray-200"},c.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},e.room.name),c.createElement("div",{className:"pt-4"},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"sm:col-span-6"},c.createElement("label",{htmlFor:"roomType",className:"block text-sm font-medium text-gray-700"},"部屋種別"),c.createElement(br.ZP,{options:a,isMulti:!0,isClearable:!0,closeMenuOnSelect:!1,value:A(),onChange:e=>(e=>{const t=e.map((e=>e.value));m("roomTypeIds",t,{shouldDirty:!0})})(e)}))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"建物形式"),c.createElement("select",Br(Nr({},o("roomModelTypeId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),c.createElement("option",{value:""}),e.roomModelTypes.map((e=>c.createElement("option",{value:e.id,key:`roomModelTypeId-${e.id}`},e.name))))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"間取図レイアウトタイプ"),c.createElement("select",Br(Nr({},o("roomLayoutId")),{className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}),c.createElement("option",{value:""}),e.roomLayouts.map((e=>c.createElement("option",{value:e.id,key:`roomLayoutId-${e.id}`},e.name))))))),c.createElement("div",{className:"pt-5"},c.createElement("div",{className:"flex justify-end"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 ml-3 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm disabled:opacity-50",disabled:!i||n||!d,loading:n},"保存"),c.createElement(Ta.x7,{position:"top-right"}))))},Cr=()=>{const{roomId:e}=(0,D.UO)(),{data:{room:t=null}={}}=function(e){const t=U(U({},R),e);return P.a(Tt,t)}({variables:{id:e}}),{data:{roomTypes:a=null}={}}=xa(),{data:{roomLayouts:r=null}={}}=Aa(),{data:{roomModelTypes:n=null}={}}=function(e){const t=U(U({},R),void 0);return P.a(pa,t)}();return t&&n&&a&&r?c.createElement(kr,{room:t,roomTypes:a,roomLayouts:r,roomModelTypes:n}):null},Ir={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Dr=({buildingNumber:e,isOpen:t,handleClose:a})=>{const[r,{loading:n}]=function(e){const t=U(U({},R),e);return $.D(bt,t)}({onCompleted:()=>{Ta.ZP.success("保存しました")}}),o=(0,Ar.TA)({initialValues:{name:""},onSubmit:t=>{r({variables:{buildingNumberId:e.id,attributes:t}}),a(),o.resetForm()}});return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:t,onRequestClose:a,style:Ir},c.createElement("h1",{className:"text-xl"},"号室"),c.createElement("form",{className:"p-6 space-y-6",onSubmit:o.handleSubmit},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:o.values.name,onChange:o.handleChange,onBlur:o.handleBlur})),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:n},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))};var jr=Object.defineProperty,$r=Object.getOwnPropertySymbols,Pr=Object.prototype.hasOwnProperty,Tr=Object.prototype.propertyIsEnumerable,Sr=(e,t,a)=>t in e?jr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Fr={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Or=({id:e,isOpen:t,handleClose:a})=>{var r;const[n,{loading:o,data:l}]=function(e){const t=U(U({},R),e);return $.D(gt,t)}({onCompleted:e=>{e.copyRoom.error||a()}}),{register:m,handleSubmit:s,formState:{errors:i,isDirty:d,isValid:u}}=(0,Pa.cI)({mode:"all",defaultValues:{name:""}});return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:t,onRequestClose:a,style:Fr},c.createElement("h1",{className:"text-xl"},"部屋コピー"),(null==l?void 0:l.copyRoom.error)&&c.createElement("p",{className:"text-red-500"},null==l?void 0:l.copyRoom.error),c.createElement("form",{className:"p-6 space-y-6",onSubmit:s((t=>{n({variables:{id:e,attributes:t}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))Pr.call(t,a)&&Sr(e,a,t[a]);if($r)for(var a of $r(t))Tr.call(t,a)&&Sr(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(r=i.name)?void 0:r.type)&&c.createElement("p",{className:"text-red-400"},"名称は必須項目です")),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存")))))},Mr=e=>{const{id:t,roomId:a}=(0,D.UO)(),[r]=function(e){const t=U(U({},R),e);return $.D(yt,t)}({onCompleted:e=>{e.removeRoom.error&&alert(e.removeRoom.error)}}),[n,o]=(0,c.useState)(null),l=(0,c.useMemo)((()=>a===e.room.id),[a,e.room.id]);return c.createElement(c.Fragment,null,c.createElement("tr",{key:e.room.id,className:l?"bg-blue-100":""},c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/buildings/${t}/building_numbers/${e.room.buildingNumberId}/rooms/${e.room.id}`,className:"hover:underline focus:outline-none"},e.room.name)),c.createElement("td",{className:"px-6 py-4 text-xs font-medium text-gray-900 flex items-center justify-center space-x-2"},c.createElement(Da,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>o(e.room.id)},"コピー"),c.createElement(Da,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>{confirm("本当に削除しますか？")&&r({variables:{id:e.room.id}})}},"削除"))),n&&c.createElement(Or,{id:n,isOpen:!!n,handleClose:()=>o(null)}))},Ur=()=>{var e;const{buidingNumberId:t}=(0,D.UO)(),{data:{buildingNumber:a=null}={}}=function(e){const t=U(U({},R),e);return P.a($t,t)}({variables:{id:t}}),[r,n]=(0,c.useState)(!1);return a?c.createElement(c.Fragment,null,c.createElement("div",{className:"w-64 ml-4 border border-gray-300 rounded-lg shadow"},c.createElement("table",{className:"w-full p-0 divide-y divide-gray-200"},c.createElement("thead",{className:"h-5 bg-gray-100"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"flex px-6 py-1 text-xs font-medium leading-7 tracking-wider text-gray-500"},"号室"),c.createElement("th",null,c.createElement("span",null,c.createElement(Da,{className:"inline-flex justify-center px-2 py-1 ml-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm",type:"button",onClick:()=>{n(!0)}},"追加"))))),c.createElement("tbody",null,null==(e=null==a?void 0:a.rooms)?void 0:e.map((e=>c.createElement(Mr,{key:e.id,room:e}))))),c.createElement(Dr,{buildingNumber:a,isOpen:r,handleClose:()=>n(!1)})),c.createElement(Cr,null)):null},Rr=()=>{var e;console.log("🚀 ~ file: BuildingNumber.tsx:114 ~ BuildingNumber ~ BuildingNumber:##");const{id:t,buidingNumberId:a}=(0,D.UO)(),[r,n]=(0,c.useState)(!1),[o,l]=(0,c.useState)(null),{data:{building:m=null}={}}=jt({variables:{id:t}}),[s]=function(e){const t=U(U({},R),e);return $.D(ye,t)}({onCompleted:e=>{e.removeBuildingNumber.error&&alert(e.removeBuildingNumber.error)}}),i=e=>e.id===a;return c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},m&&c.createElement(er,{building:m}),c.createElement(tr,null),c.createElement("div",{className:"mt-6"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>n(!0)},"号棟追加")),c.createElement(gr,{isOpen:r,handleClose:()=>{n(!1),l(null)},buildingNumber:o}),c.createElement("div",{className:"flex mt-6"},c.createElement("div",{className:"overflow-hidden border border-gray-300 shadow sm:rounded-lg"},c.createElement("table",{className:"divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-100"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"px-6 py-1 text-xs font-medium leading-8 tracking-wider text-gray-500"},"号棟"),c.createElement("th",{scope:"col",className:"relative px-6 py-3"},c.createElement("span",{className:"sr-only"},"Edit")))),c.createElement("tbody",null,null==(e=null==m?void 0:m.buildingNumbers)?void 0:e.map((e=>c.createElement("tr",{key:e.id,className:i(e)?"bg-blue-100":""},c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/buildings/${t}/building_numbers/${e.id}`,className:"hover:underline focus:outline-none"},e.name)),c.createElement("td",{className:"px-2 py-2 text-xs font-medium flex items-center"},c.createElement(Da,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>(e=>{l(e),n(!0)})(e)},"編集"),c.createElement(Da,{type:"button",className:"block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100",onClick:()=>(e=>{confirm("削除してよろしいですか？")&&s({variables:{id:e.id}})})(e)},"削除")))))))),c.createElement(D.Z5,null,c.createElement(D.AW,{path:"rooms/:roomId/*",element:c.createElement(Ur,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Ur,null)})))))},Yr=()=>c.createElement(D.Z5,null,c.createElement(D.AW,{path:":buidingNumberId/*",element:c.createElement(Rr,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Rr,null)})),Vr="...",qr=(e,t)=>{let a=t-e+1;return Array.from({length:a},((t,a)=>a+e))};var Lr=Object.defineProperty,_r=Object.defineProperties,Zr=Object.getOwnPropertyDescriptors,zr=Object.getOwnPropertySymbols,Wr=Object.prototype.hasOwnProperty,Hr=Object.prototype.propertyIsEnumerable,Gr=(e,t,a)=>t in e?Lr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Jr=({totalCount:e,siblingCount:t=1,currentPage:a,pageSize:r,totalPages:n})=>{const[o,l]=(0,p.lr)(),m=(0,c.useMemo)((()=>Array.from(new Set(o.entries()))),[o]),s=(e,t)=>{let a={};var r;m.forEach((e=>a[e[0]]=e[1])),l((r=((e,t)=>{for(var a in t||(t={}))Wr.call(t,a)&&Gr(e,a,t[a]);if(zr)for(var a of zr(t))Hr.call(t,a)&&Gr(e,a,t[a]);return e})({},a),_r(r,Zr({[e]:t}))))},i=(({totalCount:e,pageSize:t,siblingCount:a=1,currentPage:r})=>(0,c.useMemo)((()=>{const n=Math.ceil(e/t);if(a+5>=n)return qr(1,n);const o=Math.max(r-a,1),l=Math.min(r+a,n),m=o>2,s=l<n-2,i=n;if(!m&&s)return[...qr(1,3+2*a),Vr,n];if(m&&!s){let e=qr(n-(3+2*a)+1,n);return[1,Vr,...e]}if(m&&s){let e=qr(o,l);return[1,Vr,...e,Vr,i]}}),[e,t,a,r]))({currentPage:a,totalCount:e,siblingCount:t,pageSize:r});return 0===a||!i||(null==i?void 0:i.length)<2?null:c.createElement(c.Fragment,null,c.createElement("div",null,c.createElement("nav",{className:"relative z-0 inline-flex -space-x-px rounded-md shadow-sm","aria-label":"Pagination"},a>1&&c.createElement("a",{className:"relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-pointer rounded-l-md hover:bg-gray-50",onClick:()=>{s("page",String(a-1))}},c.createElement("span",{className:"sr-only"},"Previous"),c.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},c.createElement("path",{fillRule:"evenodd",d:"M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z",clipRule:"evenodd"}))),1===a&&c.createElement("span",{className:"relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 bg-gray-100 border border-gray-300 rounded-l-md hover:bg-gray-50"},c.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},c.createElement("path",{fillRule:"evenodd",d:"M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z",clipRule:"evenodd"}))),i.map((e=>e===Vr?c.createElement("span",{className:"relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300",key:`pageNumber-${e}`},"..."):c.createElement("a",{key:e,className:`bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium cursor-pointer\n\t\t\t\t\t\t\t\t${e===a?"border-indigo-500 text-indigo-600 z-10 bg-indigo-50":""}\n\t\t\t\t\t\t\t\t`,onClick:()=>s("page",e)},e))),a<n&&c.createElement("a",{className:"relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 cursor-pointer\n\t\t\t\t\t\t",onClick:()=>{s("page",String(a+1))}},c.createElement("span",{className:"sr-only"},"Next"),c.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},c.createElement("path",{fillRule:"evenodd",d:"M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z",clipRule:"evenodd"}))),a===n&&c.createElement("span",{className:"relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 bg-gray-100 border border-gray-300 rounded-l-md hover:bg-gray-50"},c.createElement("span",{className:"sr-only"},"Next"),c.createElement("svg",{className:"w-5 h-5",xmlns:"http://www.w3.org/2000/svg",viewBox:"0 0 20 20",fill:"currentColor","aria-hidden":"true"},c.createElement("path",{fillRule:"evenodd",d:"M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z",clipRule:"evenodd"}))))))};var Kr=Object.defineProperty,Qr=Object.getOwnPropertySymbols,Xr=Object.prototype.hasOwnProperty,en=Object.prototype.propertyIsEnumerable,tn=(e,t,a)=>t in e?Kr(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const an={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},rn=({isOpen:e,onClose:t})=>{var a;const r=(0,D.s0)(),[n,{loading:o,data:l}]=function(e){const t=U(U({},R),e);return $.D(pe,t)}({onCompleted:e=>{e.createBuilding.error||(Ta.ZP.success("保存しました"),g(),r(`/buildings/${e.createBuilding.building.id}/base`))}}),{register:m,handleSubmit:s,reset:i,formState:{isDirty:d,isValid:u,errors:p}}=(0,Pa.cI)({defaultValues:{name:""}}),g=()=>{i(),t()};return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:e,onRequestClose:g,style:an},c.createElement("h1",{className:"text-xl"},"物件新規作成"),(null==l?void 0:l.createBuilding.error)&&c.createElement("p",{className:"text-red-500"},l.createBuilding.error),c.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{n({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))Xr.call(t,a)&&tn(e,a,t[a]);if(Qr)for(var a of Qr(t))en.call(t,a)&&tn(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(a=p.name)?void 0:a.type)&&c.createElement("p",{className:"text-red-400"},"名称は必須項目です")),c.createElement("div",{className:"py-3 text-right flex items-center space-x-4"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm",onClick:()=>g()},"キャンセル"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))},nn=()=>{const[e,t]=(0,p.lr)(),[a,r]=(0,c.useState)(!1),n=e.get("page")?Number(e.get("page")):1,o=e.get("keyword"),{data:{buildings:{buildings:l=[],pagination:m={}}={}}={}}=function(e){const t=U(U({},R),e);return P.a(Pt,t)}({variables:{page:n,search:{keyword:o}},fetchPolicy:"cache-and-network"}),s=(0,Ar.TA)({initialValues:{keyword:o},onSubmit:e=>{t({keyword:e.keyword,page:"1"})}});return c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"物件管理"),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>r(!0)},"新規作成"),c.createElement(rn,{isOpen:a,onClose:()=>r(!1)}),c.createElement("form",{className:"px-10 py-2 mb-4 bg-white",onSubmit:s.handleSubmit},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"sm:col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"キーワード"),c.createElement("div",{className:"mt-1"},c.createElement("input",{name:"keyword",type:"text",className:"block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:s.values.keyword,onChange:s.handleChange,onBlur:()=>s.handleSubmit()}))))),c.createElement(Jr,{totalCount:m.totalCount,currentPage:m.currentPage,totalPages:m.totalPages,pageSize:20}),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"w-12 px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"ID"),c.createElement("th",{scope:"col",className:"w-72 px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"住所"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"ランク"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},l.map((e=>c.createElement("tr",{className:"bg-white",key:e.id},c.createElement("td",{className:"ml-10"},c.createElement("div",{className:"px-2 cursor-pointer"},e.id)),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/buildings/${e.id}/base${location.search}`},e.name)),c.createElement("td",{className:"ml-10"},c.createElement("div",{className:"px-2"},e.address)),c.createElement("td",{className:"ml-10"},c.createElement("div",{className:"px-2"},e.rankText))))))))))};var on=a(2795);const ln=({children:e})=>{console.log("layout");const t=(0,on.x)(),a=(0,D.s0)(),{data:{currentUser:r}={}}=va(),[n]=function(e){const t=U(U({},R),void 0);return $.D(Ae,t)}();return(0,c.useEffect)((()=>{null===r&&a("/login")}),[r]),void 0===r?null:c.createElement("div",{className:"flex h-screen overflow-hidden bg-gray-100"},c.createElement("div",{className:"flex flex-col pt-5 pb-4 bg-white w-[200px]"},c.createElement("div",{className:"flex items-center flex-shrink-0 px-4"},"案件管理システム"),c.createElement("div",{className:"flex-1 h-0 mt-5 overflow-y-auto"},c.createElement("nav",{className:"px-4"},c.createElement(p.rU,{to:"/projects"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/projects")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"案件管理")),c.createElement(p.rU,{to:"/orders"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/orders")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"発注管理")),c.createElement("h3",{className:"block px-3 mt-10 text-xs font-semibold tracking-wider text-gray-500 uppercase",id:"projects-headline"},"マスター管理"),c.createElement(p.rU,{to:"/buildings"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/buildings")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"物件")),c.createElement(p.rU,{to:"/items"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/items")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"項目")),c.createElement(p.rU,{to:"/comments"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/comments")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"コメント")),c.createElement(p.rU,{to:"/commenttypes"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/commenttypes")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"コメント種別")),c.createElement(p.rU,{to:"/companies"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/companies")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"業者")),c.createElement(p.rU,{to:"/companytypes"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/companytypes")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"業者種別")),c.createElement(p.rU,{to:"/roomtypes"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/roomtypes")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"部屋種別")),c.createElement(p.rU,{to:"/roomlayouts"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/roomlayouts")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"部屋レイアウト")),c.createElement(p.rU,{to:"/users"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/users")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"従業員")),c.createElement(p.rU,{to:"/jkk_users"},c.createElement("a",{className:`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md\n\t\t\t\t\t\t\t\t${location.pathname.startsWith("/jkk_users")?"bg-gray-100 text-gray-900":""}\n\t\t\t\t\t\t\t`},"公社担当者")),c.createElement("a",{href:"#",className:"text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md",onClick:()=>n().then((()=>{t.resetStore(),a("/login")}))},"ログアウト")))),c.createElement("div",{className:"flex-1 px-4 pb-4 overflow-y-auto col-span-full"},e))},mn=()=>(console.log("🚀 ~ file: index.tsx:19 ~ BuildingPage ~ BuildingPage:!!!!"),c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:":id/building_numbers/*",element:c.createElement(Yr,null)}),c.createElement(D.AW,{path:":id/base",element:c.createElement(ur,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(nn,null)})))),sn=({comment:e})=>c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/comments",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"コメント一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==e?void 0:e.id))))),cn=()=>{const{id:e}=(0,D.UO)(),t=e=>location.pathname.indexOf(e)>-1;return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement(p.rU,{to:`/comments/${e}/base${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("base")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報")),c.createElement(p.rU,{to:`/comments/${e}/codes${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("codes")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"コード")))))};var dn=Object.defineProperty,un=Object.defineProperties,An=Object.getOwnPropertyDescriptors,pn=Object.getOwnPropertySymbols,gn=Object.prototype.hasOwnProperty,bn=Object.prototype.propertyIsEnumerable,yn=(e,t,a)=>t in e?dn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,xn=(e,t)=>{for(var a in t||(t={}))gn.call(t,a)&&yn(e,a,t[a]);if(pn)for(var a of pn(t))bn.call(t,a)&&yn(e,a,t[a]);return e};const fn=e=>{var t;const a=(0,D.s0)(),[r,{loading:n}]=function(e){const t=U(U({},R),e);return $.D(we,t)}({onCompleted:e=>{e.updateComment.error?alert(e.updateComment.error):(Ta.ZP.success("保存しました"),setTimeout((()=>{a(`/comments${location.search}`)}),500))}}),[o]=function(e){const t=U(U({},R),e);return $.D(he,t)}({onCompleted:e=>{e.removeComment.error?alert(e.removeComment.error):a(`/comments${location.search}`)}}),{register:l,handleSubmit:m,formState:{isDirty:s,isValid:i,errors:d}}=(0,Pa.cI)({defaultValues:{comment:e.comment.comment,commentTypeId:e.comment.commentTypeId}});return c.createElement("div",{className:"mt-4"},c.createElement(sn,{comment:e.comment}),c.createElement(cn,null),c.createElement("form",{className:"p-6 space-y-6",onSubmit:m((t=>{r({variables:{id:e.comment.id,attributes:t}})}))},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"コメント種別"),c.createElement("select",(u=xn({},l("commentTypeId")),un(u,An({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}))),c.createElement("option",{value:""}),e.commentTypes.map((e=>c.createElement("option",{value:e.id,key:`commentType-${e.id}`},e.name))))),c.createElement("div",{className:"col-span-6"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",xn({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("comment",{required:!0}))),"required"===(null==(t=d.comment)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です"))),c.createElement("div",{className:"py-3 text-right flex items-center"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm hover:bg-gray-200",onClick:()=>a(`/comments${location.search}`)},"戻る"),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-red-400 rounded-sm shadow-sm hover:bg-red-200",onClick:()=>{confirm("削除してよろしいですか？")&&o({variables:{id:e.comment.id}})}},"削除"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!i||!s,loading:n},"保存"),c.createElement(Ta.x7,{position:"top-right"}))));var u},En=()=>{const{id:e}=(0,D.UO)(),{data:{comment:t=null}={}}=Ot({variables:{id:e}}),{data:{commentTypes:a=[]}={}}=qt();return t&&a?c.createElement(fn,{comment:t,commentTypes:a}):null},hn=e=>{const[t]=function(e){const t=U(U({},R),void 0);return $.D(ve,t)}();return c.createElement("tr",{className:"bg-white"},c.createElement("td",null,c.createElement(Da,{type:"button",className:"mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{t({variables:{id:e.comment.id,itemCodeId:e.itemCode.id}})}},"削除")),c.createElement("td",{className:"text-sm px-2"},e.itemCode.code),c.createElement("td",{className:"text-sm px-2"},e.itemCode.name))};var vn=Object.defineProperty,wn=Object.defineProperties,Nn=Object.getOwnPropertyDescriptors,Bn=Object.getOwnPropertySymbols,kn=Object.prototype.hasOwnProperty,Cn=Object.prototype.propertyIsEnumerable,In=(e,t,a)=>t in e?vn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Dn=e=>{const[t,a]=(0,c.useState)(null),{register:r,handleSubmit:n}=(0,Pa.cI)({defaultValues:{keyword:""}}),{data:{allItems:o=[]}={}}=Jt({variables:{search:{keyword:t}},skip:!t}),l=e=>{a(e.keyword)},[m]=function(e){const t=U(U({},R),e);return $.D(fe,t)}({onCompleted:()=>{e.onClose()}});return c.createElement("form",{onSubmit:n(l)},c.createElement("input",(s=((e,t)=>{for(var a in t||(t={}))kn.call(t,a)&&In(e,a,t[a]);if(Bn)for(var a of Bn(t))Cn.call(t,a)&&In(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-400 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},r("keyword")),i={onBlur:n(l)},wn(s,Nn(i)))),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{className:"w-20"}),c.createElement("th",{className:"px-2 py-3 text-xs font-medium text-left text-gray-500 w-28"},"コード"),c.createElement("th",{className:"px-2 py-3 text-xs font-medium text-left text-gray-500"},"名称"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},o.map((t=>c.createElement("tr",{key:`item-${t.id}`},c.createElement("td",null,c.createElement(Da,{type:"button",className:"mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{var a;a=t.itemCode.id,m({variables:{id:e.comment.id,itemCodeId:a}})}},"追加")),c.createElement("td",{className:"px-2 py-3 text-sm text-gray-500"},t.itemCode.code),c.createElement("td",{className:"px-2 py-3 text-sm text-gray-500"},t.name)))))));var s,i},jn={content:{top:"40%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"900px",height:"600px"}},$n=e=>{const[t,a]=(0,c.useState)(!1),r=()=>{a(!1)};return c.createElement("div",{className:"mt-4"},c.createElement(sn,{comment:e.comment}),c.createElement(cn,null),c.createElement("button",{type:"button",className:"mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>a(!0)},"追加"),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{className:"w-20"}),c.createElement("th",{className:"px-2 py-3 text-xs font-medium text-left text-gray-500 w-28"},"コード"),c.createElement("th",{className:"px-2 py-3 text-xs font-medium text-left text-gray-500"},"名称"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},e.comment.itemCodes.map((t=>c.createElement(hn,{comment:e.comment,itemCode:t,key:t.id}))))),c.createElement(A(),{isOpen:t,onRequestClose:r,style:jn},c.createElement(Dn,{comment:e.comment,onClose:r})))},Pn=()=>{const{id:e}=(0,D.UO)(),{data:{comment:t=null}={}}=Ot({variables:{id:e}});return t?c.createElement($n,{comment:t}):null},Tn=()=>{const{data:{comments:e=[]}={}}=Ut({fetchPolicy:"cache-and-network",variables:{search:{}}});return c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"コメント"),c.createElement(p.rU,{to:"/comments/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"分類"),c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},e.map((e=>c.createElement("tr",{className:"bg-white",key:null==e?void 0:e.id},c.createElement("td",{className:"w-32 ml-10 text-xs"},c.createElement("div",{className:"px-2 cursor-pointer"},null==e?void 0:e.commentType.name)),c.createElement("td",{className:"px-2 py-4 text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300"},c.createElement(p.rU,{to:`/comments/${null==e?void 0:e.id}/base`},null==e?void 0:e.comment))))))))))};var Sn=Object.defineProperty,Fn=Object.defineProperties,On=Object.getOwnPropertyDescriptors,Mn=Object.getOwnPropertySymbols,Un=Object.prototype.hasOwnProperty,Rn=Object.prototype.propertyIsEnumerable,Yn=(e,t,a)=>t in e?Sn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Vn=(e,t)=>{for(var a in t||(t={}))Un.call(t,a)&&Yn(e,a,t[a]);if(Mn)for(var a of Mn(t))Rn.call(t,a)&&Yn(e,a,t[a]);return e};const qn=()=>{var e,t;const a=(0,D.s0)(),[r,{loading:n}]=function(e){const t=U(U({},R),e);return $.D(Ee,t)}({onCompleted:e=>{e.createComment.error||(Ta.ZP.success("保存しました"),setTimeout((()=>{a(`/comments${location.search}`)}),500))}}),{data:{commentTypes:o=[]}={}}=qt(),{register:l,handleSubmit:m,formState:{isDirty:s,isValid:i,errors:d}}=(0,Pa.cI)({mode:"all"});return c.createElement("div",{className:"w-full"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/comments",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"コメント一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:m((e=>{r({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"コメント種別"),c.createElement("select",(u=Vn({},l("commentTypeId",{required:!0})),Fn(u,On({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"}))),c.createElement("option",{value:""}),o.map((e=>c.createElement("option",{value:e.id,key:`commentType-${e.id}`},e.name)))),"required"===(null==(e=d.commentTypeId)?void 0:e.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{htmlFor:"comment",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",Vn({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("comment",{required:!0}))),"required"===(null==(t=d.comment)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!i||!s,loading:n},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))));var u},Ln=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(qn,null)}),c.createElement(D.AW,{path:":id/base",element:c.createElement(En,null)}),c.createElement(D.AW,{path:":id/codes",element:c.createElement(Pn,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Tn,null)}))),_n=({commentType:e})=>c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/commenttypes",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"コメント種別一覧"))),e&&c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==e?void 0:e.id))))),Zn=()=>{const{id:e}=(0,D.UO)(),t=e=>location.pathname.indexOf(e)>-1;return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement(p.rU,{to:`/commenttypes/${e}/base${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("base")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報")),c.createElement(p.rU,{to:`/commenttypes/${e}/comments${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("comments")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"コメント")))))};var zn=Object.defineProperty,Wn=Object.getOwnPropertySymbols,Hn=Object.prototype.hasOwnProperty,Gn=Object.prototype.propertyIsEnumerable,Jn=(e,t,a)=>t in e?zn(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Kn=e=>{var t;const a=(0,D.s0)(),[r,{loading:n}]=function(e){const t=U(U({},R),e);return $.D(Ce,t)}({onCompleted:e=>{e.updateCommentTypeModel.error?alert(e.updateCommentTypeModel.error):(Ta.ZP.success("保存しました"),setTimeout((()=>{a(`/commenttypes${location.search}`)}),500))}}),[o]=function(e){const t=U(U({},R),e);return $.D(Be,t)}({onCompleted:e=>{e.removeCommentTypeModel.error?alert(e.removeCommentTypeModel.error):a(`/commenttypes${location.search}`)}}),{register:l,handleSubmit:m,formState:{isDirty:s,isValid:i,errors:d}}=(0,Pa.cI)({defaultValues:{name:e.commentType.name}});return c.createElement("div",{className:"mt-4"},c.createElement(_n,{commentType:e.commentType}),c.createElement(Zn,null),c.createElement("form",{className:"p-6 space-y-6",onSubmit:m((t=>{r({variables:{id:e.commentType.id,attributes:t}})}))},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"col-span-6"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))Hn.call(t,a)&&Jn(e,a,t[a]);if(Wn)for(var a of Wn(t))Gn.call(t,a)&&Jn(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("name",{required:!0}))),"required"===(null==(t=d.name)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です"))),c.createElement("div",{className:"py-3 text-right flex items-center"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm hover:bg-gray-200",onClick:()=>a(`/commenttypes${location.search}`)},"戻る"),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-red-400 rounded-sm shadow-sm hover:bg-red-200",onClick:()=>{confirm("削除してよろしいですか？")&&o({variables:{id:e.commentType.id}})}},"削除"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!i||!s,loading:n},"保存"),c.createElement(Ta.x7,{position:"top-right"}))))},Qn=()=>{const{id:e}=(0,D.UO)(),{data:{commentType:t=null}={}}=Yt({variables:{id:e}});return t?c.createElement(Kn,{commentType:t}):null};var Xn=a(9752),eo=a(5587),to=a(4285),ao=Object.defineProperty,ro=Object.defineProperties,no=Object.getOwnPropertyDescriptors,oo=Object.getOwnPropertySymbols,lo=Object.prototype.hasOwnProperty,mo=Object.prototype.propertyIsEnumerable,so=(e,t,a)=>t in e?ao(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,io=(e,t)=>{for(var a in t||(t={}))lo.call(t,a)&&so(e,a,t[a]);if(oo)for(var a of oo(t))mo.call(t,a)&&so(e,a,t[a]);return e};const co=e=>{const{setNodeRef:t,attributes:a,listeners:r,transform:n,transition:o}=(0,eo.nB)({id:e.comment.id}),l={transform:to.ux.Transform.toString(n),transition:o};return c.createElement("tr",(m=io(io({ref:t,className:"bg-white"},a),r),ro(m,no({style:l}))),c.createElement("td",null,c.createElement(_a,{className:"h-6 text-gray-500 cursor-pointer"})),c.createElement("td",{className:"text-sm px-2 py-2"},e.comment.comment));var m},uo=e=>{const[t,a]=(0,c.useState)(e.commentType.comments.map((e=>e.id))),[r,n]=(0,c.useState)(null),o=e=>t.indexOf(String(e)),l=r?o(r):-1,[m]=function(e){const t=U(U({},R),e);return $.D(ke,t)}({onCompleted:()=>{n(null)}}),s=(0,Xn.Dy)((0,Xn.VT)(Xn.we),(0,Xn.VT)(Xn.Lg,{coordinateGetter:eo.is}));return e.commentType.comments?c.createElement("div",{className:"mt-4"},c.createElement(_n,{commentType:e.commentType}),c.createElement(Zn,null),c.createElement(Xn.LB,{sensors:s,collisionDetection:Xn.pE,onDragStart:({active:e})=>{e&&n(String(e.id))},onDragEnd:({active:e,over:t})=>{if(t){const r=o(t.id);l!==r&&(a((e=>(0,eo.Rp)(e,l,r))),m({variables:{id:String(e.id),position:r}}))}}},c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{className:"w-20"}),c.createElement("th",{className:"px-2 py-3 text-xs font-medium text-left text-gray-500"},"名称"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},c.createElement(eo.Fo,{items:e.commentType.comments,strategy:eo.qw},t.map((t=>c.createElement(co,{comment:e.commentType.comments.find((e=>e.id===t)),key:t})))))))):null},Ao=()=>{const{id:e}=(0,D.UO)(),{data:{commentType:t=null}={}}=Yt({variables:{id:e},fetchPolicy:"cache-and-network"});return t?c.createElement(uo,{commentType:t}):null},po=()=>{const{data:{commentTypes:e=[]}={}}=qt({fetchPolicy:"cache-and-network"});return c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"コメント種別"),c.createElement(p.rU,{to:"/commenttypes/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},e.map((e=>c.createElement("tr",{className:"bg-white",key:null==e?void 0:e.id},c.createElement("td",{className:"px-2 py-4 text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300"},c.createElement(p.rU,{to:`/commenttypes/${e.id}/base`},e.name))))))))))};var go=Object.defineProperty,bo=Object.getOwnPropertySymbols,yo=Object.prototype.hasOwnProperty,xo=Object.prototype.propertyIsEnumerable,fo=(e,t,a)=>t in e?go(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Eo=()=>{var e;const t=(0,D.s0)(),[a,{loading:r}]=function(e){const t=U(U({},R),e);return $.D(Ne,t)}({onCompleted:e=>{e.createCommentTypeModel.error||(Ta.ZP.success("保存しました"),setTimeout((()=>{t(`/commenttypes${location.search}`)}),500))}}),{register:n,handleSubmit:o,formState:{isDirty:l,isValid:m,errors:s}}=(0,Pa.cI)({mode:"all"});return c.createElement("div",{className:"w-full"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement(_n,null)),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:o((e=>{a({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))yo.call(t,a)&&fo(e,a,t[a]);if(bo)for(var a of bo(t))xo.call(t,a)&&fo(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("name",{required:!0}))),"required"===(null==(e=s.name)?void 0:e.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:r||!m||!l,loading:r},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))))},ho=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(Eo,null)}),c.createElement(D.AW,{path:":id/base",element:c.createElement(Qn,null)}),c.createElement(D.AW,{path:":id/comments",element:c.createElement(Ao,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(po,null)})));var vo=Object.defineProperty,wo=Object.getOwnPropertySymbols,No=Object.prototype.hasOwnProperty,Bo=Object.prototype.propertyIsEnumerable,ko=(e,t,a)=>t in e?vo(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Co=(e,t)=>{for(var a in t||(t={}))No.call(t,a)&&ko(e,a,t[a]);if(wo)for(var a of wo(t))Bo.call(t,a)&&ko(e,a,t[a]);return e};const Io=e=>{var t;const{id:a}=(0,D.UO)(),r=(0,D.s0)(),[n,{loading:o}]=function(e){const t=U(U({},R),e);return $.D(je,t)}({onCompleted:e=>{e.updateCompany.error?alert(e.updateCompany.error):(Ta.ZP.success("保存しました"),setTimeout((()=>{r(`/companies${location.search}`)}),500))}}),[l]=function(e){const t=U(U({},R),e);return $.D(De,t)}({onCompleted:e=>{e.removeCompany.error?alert(e.removeCompany.error):r(`/companies${location.search}`)}}),{register:m,handleSubmit:s,formState:{isDirty:i,isValid:d,errors:u}}=(0,Pa.cI)({defaultValues:{name:e.company.name,companyTypeIds:e.company.companyTypeIds,isOrderEach:e.company.isOrderEach,isOrderMonthly:e.company.isOrderMonthly}});return c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:`/companies${location.search}`,className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"業者マスター一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},e.company.name))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{n({variables:{id:a,attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",Co({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(t=u.name)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"業務種別"),c.createElement("div",{className:"flex mb-2 space-x-7"},e.companyTypes.map((e=>c.createElement("div",{className:"flex items-center space-x-2",key:e.id},c.createElement("input",Co({id:e.id,value:e.id,type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},m("companyTypeIds"))),c.createElement("label",{className:"block text-sm font-medium text-gray-700"},e.name)))))),c.createElement("div",{className:"mt-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"契約種別"),c.createElement("div",{className:"flex items-center space-x-4"},c.createElement("div",{className:"flex items-center space-x-2"},c.createElement("input",Co({id:"isOrderEach",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},m("isOrderEach"))),c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"都度契約")),c.createElement("div",{className:"flex items-center space-x-2"},c.createElement("input",Co({id:"isOrderMonthly",type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},m("isOrderMonthly"))),c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"月次契約")))),c.createElement("div",{className:"py-3 text-right"},c.createElement("div",{className:"inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},c.createElement(p.rU,{to:`/companies${location.search}`},"戻る")),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-gray-300 rounded-sm shadow-sm hover:bg-red-200",onClick:()=>{confirm("削除してよろしいですか？")&&(l({variables:{id:a}}),Ta.ZP.success("削除しました"))}},"削除"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!i||!d,loading:o},"保存"),c.createElement(Ta.x7,{position:"top-right"}))))))},Do=()=>{const{id:e}=(0,D.UO)(),{data:{company:t=null}={}}=function(e){const t=U(U({},R),e);return P.a(Zt,t)}({variables:{id:e}}),{data:{companyTypes:a=[]}={}}=Ht();return t&&a?c.createElement(Io,{company:t,companyTypes:a}):null};var jo=Object.defineProperty,$o=Object.getOwnPropertySymbols,Po=Object.prototype.hasOwnProperty,To=Object.prototype.propertyIsEnumerable,So=(e,t,a)=>t in e?jo(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Fo={content:{top:"30%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"300px"}},Oo=({isOpen:e,onClose:t})=>{var a;const r=(0,D.s0)(),[n,{loading:o,data:l}]=function(e){const t=U(U({},R),e);return $.D(Ie,t)}({onCompleted:e=>{e.createCompany.error||(Ta.ZP.success("保存しました"),setTimeout((()=>{g(),r(`/companies/${e.createCompany.company.id}`)}),500))}}),{register:m,handleSubmit:s,reset:i,formState:{isDirty:d,isValid:u,errors:p}}=(0,Pa.cI)({defaultValues:{name:""}}),g=()=>{i(),t()};return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:e,onRequestClose:g,style:Fo},c.createElement("h1",{className:"text-xl"},"業者新規作成"),(null==l?void 0:l.createCompany.error)&&c.createElement("p",{className:"text-red-500"},l.createCompany.error),c.createElement("form",{className:"p-6 space-y-6",onSubmit:s((e=>{n({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))Po.call(t,a)&&So(e,a,t[a]);if($o)for(var a of $o(t))To.call(t,a)&&So(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(a=p.name)?void 0:a.type)&&c.createElement("p",{className:"text-red-400"},"名称は必須項目です")),c.createElement("div",{className:"py-3 text-right flex items-center space-x-4"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm",onClick:()=>g()},"キャンセル"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))},Mo=()=>{const{data:{companies:e=[]}={}}=_t({fetchPolicy:"cache-and-network"}),[t,a]=(0,c.useState)(!1);return c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"業者マスター"),c.createElement("div",{className:"my-4"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>a(!0)},"新規作成"),t&&c.createElement(Oo,{isOpen:t,onClose:()=>a(!1)})),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"w-6 px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"名称"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"業務種別"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"都度契約"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"月次契約"))),c.createElement("tbody",{className:"divide-y divide-gray-200 bg-white"},e.map((e=>{var t;return c.createElement("tr",{className:"bg-white",key:e.id},c.createElement("td",{className:""},c.createElement("div",{className:"px-2"},e.id)),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},c.createElement(p.rU,{to:`/companies/${e.id}${location.search}`},e.name)),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},null==(t=e.companyTypes)?void 0:t.map((e=>e.name)).join("/")),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},e.isOrderEach&&"都度契約"),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},e.isOrderMonthly&&"月次契約"))})))))))},Uo=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:":id",element:c.createElement(Do,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Mo,null)})));var Ro=Object.defineProperty,Yo=Object.getOwnPropertySymbols,Vo=Object.prototype.hasOwnProperty,qo=Object.prototype.propertyIsEnumerable,Lo=(e,t,a)=>t in e?Ro(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const _o=e=>{var t;const{id:a}=(0,D.UO)(),r=(0,D.s0)(),[n,{loading:o,data:l}]=function(e){const t=U(U({},R),e);return $.D(Te,t)}({onCompleted:e=>{e.updateCompanyTypeModel.error||(Ta.ZP.success("保存しました"),setTimeout((()=>{r(`/companytypes${location.search}`)}),500))}}),{register:m,handleSubmit:s,formState:{errors:i,isDirty:d,isValid:u}}=(0,Pa.cI)({mode:"all",defaultValues:{name:e.companyType.name}});return c.createElement("div",{className:"mt-10 md:col-span-2 w-screenmax-w-xl"},(null==l?void 0:l.updateCompanyTypeModel.error)&&c.createElement("p",{className:"text-red-500"},null==l?void 0:l.updateCompanyTypeModel.error),c.createElement("form",{className:"max-w-xl p-6 space-y-6",onSubmit:s((e=>{n({variables:{id:a,attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))Vo.call(t,a)&&Lo(e,a,t[a]);if(Yo)for(var a of Yo(t))qo.call(t,a)&&Lo(e,a,t[a]);return e})({type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("name",{required:!0}))),"required"===(null==(t=i.name)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"名称は必須項目です")),c.createElement("div",{className:"flex space-x-4 py-3 text-right"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-red-500 border-red-400 border  rounded-sm shadow-sm hover:bg-red-300",onClick:e.onRemove,loading:o},"削除"),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border-gray-400 border rounded-sm shadow-sm hover:bg-gray-300",onClick:()=>r("/companytypes")},"戻る"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!d||!u,loading:o},"保存"),c.createElement(Ta.x7,{position:"top-right"}))))},Zo=e=>{var t;const{id:a}=(0,D.UO)();return c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/companytypes",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"業者種別"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==(t=e.companyType)?void 0:t.name))))),e.children))},zo=()=>{const{id:e}=(0,D.UO)(),t=(0,D.s0)(),{data:{companyType:a=null}={}}=function(e){const t=U(U({},R),e);return P.a(zt,t)}({variables:{id:e}}),[r]=function(e){const t=U(U({},R),e);return $.D(Pe,t)}({onCompleted:e=>{e.removeCompanyTypeModel.error||t(`/companytypes${location.search}`)}});return a?c.createElement(Zo,{companyType:a},c.createElement(_o,{companyType:a,onRemove:()=>{confirm("削除しますか？")&&r({variables:{id:e}})}})):null};var Wo=Object.defineProperty,Ho=Object.getOwnPropertySymbols,Go=Object.prototype.hasOwnProperty,Jo=Object.prototype.propertyIsEnumerable,Ko=(e,t,a)=>t in e?Wo(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Qo=()=>{var e;const t=(0,D.s0)(),[a,{loading:r,data:n}]=function(e){const t=U(U({},R),e);return $.D($e,t)}({onCompleted:e=>{e.createCompanyTypeModel.error||(Ta.ZP.success("保存しました"),setTimeout((()=>{t("/companytypes")}),500))}}),{register:o,handleSubmit:l,formState:{errors:m,isDirty:s,isValid:i}}=(0,Pa.cI)({mode:"all"});return c.createElement(Zo,null,c.createElement("div",{className:"mt-10 md:col-span-2 w-screenmax-w-xl"},(null==n?void 0:n.createCompanyTypeModel.error)&&c.createElement("p",{className:"text-red-500"},null==n?void 0:n.createCompanyTypeModel.error),c.createElement("form",{className:"max-w-xl p-6 space-y-6",onSubmit:l((e=>{a({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))Go.call(t,a)&&Ko(e,a,t[a]);if(Ho)for(var a of Ho(t))Jo.call(t,a)&&Ko(e,a,t[a]);return e})({type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("name",{required:!0}))),"required"===(null==(e=m.name)?void 0:e.type)&&c.createElement("p",{className:"text-red-400"},"名称は必須項目です")),c.createElement("div",{className:"flex space-x-4 py-3 text-right"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border-gray-400 border rounded-sm shadow-sm hover:bg-gray-300",onClick:()=>t("/companytypes")},"戻る"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm disabled:opacity-30",disabled:!s||!i,loading:r},"保存"),c.createElement(Ta.x7,{position:"top-right"})))))};var Xo=a(7145),el=a.n(Xo),tl=a(1261),al=a(7384),rl=a(994),nl=a(6487);const ol=({id:e,index:t,companyType:a,moveItem:r,order:n})=>{const o=(0,c.useRef)(null),[{handlerId:l},m]=(0,rl.L)({accept:"CompanyType",collect:e=>({handlerId:e.getHandlerId()}),hover(e,a){var n;if(!o.current)return;const l=e.index,m=t;if(l===m)return;const s=null==(n=o.current)?void 0:n.getBoundingClientRect(),i=(s.bottom-s.top)/2,c=a.getClientOffset().y-s.top;l<m&&c<i||l>m&&c>i||(r(e.id,l,m),e.index=m)}}),[{isDragging:s},i]=(0,nl.c)({type:"CompanyType",item:()=>({id:e,index:t}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: CompanyType.tsx:87 ~ CompanyType ~ item",e),n(e.id,e.index)}}),d=s?0:1;return i(m(o)),c.createElement("div",{ref:o,className:"bg-white flex w-full",style:{opacity:d}},c.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/companytypes/${a.id}`},a.name)))},ll=()=>{const[e,t]=(0,c.useState)([]),{data:{companyTypes:a=[]}={}}=Ht({fetchPolicy:"cache-and-network"}),[r]=function(e){const t=U(U({},R),void 0);return $.D(Se,t)}(),n=(e,a,r)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${a}`),console.log(`update: hoverIndex: ${r}`),t((e=>el()(e,{$splice:[[a,1],[r,0,e[a]]]})))},o=(e,t)=>{console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ id",e),console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ index",t),r({variables:{id:e,attributes:{position:t}}})};return(0,c.useEffect)((()=>{t(a)}),[a]),c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"業者種別"),c.createElement(p.rU,{to:"/companytypes/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("div",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("div",{className:"bg-gray-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"名称"))),c.createElement(tl.W,{backend:al.PD},c.createElement("div",{className:"bg-white divide-y divide-gray-200"},e.map(((e,t)=>c.createElement(ol,{id:e.id,index:t,companyType:e,key:e.id,moveItem:n,order:o})))))))))},ml=()=>(console.log("🚀 ~ file: index.tsx:19 ~ CompanyTypePage ~ CompanyTypePage :",ml),c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(Qo,null)}),c.createElement(D.AW,{path:":id",element:c.createElement(zo,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(ll,null)})))),sl=()=>c.createElement(ln,null,c.createElement("div",null,"Dashboard"));var il=Object.defineProperty,cl=Object.getOwnPropertySymbols,dl=Object.prototype.hasOwnProperty,ul=Object.prototype.propertyIsEnumerable,Al=(e,t,a)=>t in e?il(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,pl=(e,t)=>{for(var a in t||(t={}))dl.call(t,a)&&Al(e,a,t[a]);if(cl)for(var a of cl(t))ul.call(t,a)&&Al(e,a,t[a]);return e};const gl=e=>{var t;const[a,{loading:r}]=function(e){const t=U(U({},R),e);return $.D(Me,t)}({onCompleted:t=>{t.updateItem.error||e.onSubmit()}}),{register:n,handleSubmit:o,formState:{errors:l,isDirty:m,isValid:s}}=(0,Pa.cI)({mode:"all",defaultValues:{name:e.item.name,shortName:e.item.shortName,price:e.item.price,orderPrice:e.item.orderPrice,companyId:e.item.companyId,companyTypeIds:e.item.companyTypeIds,order:e.item.order,unitId:e.item.unitId}});return c.createElement("div",{className:"flex flex-col py-1 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:o((t=>{a({variables:{id:e.item.id,attributes:t}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",pl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("name",{required:!0}))),"required"===(null==(t=l.name)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{htmlFor:"shortName",className:"block text-sm font-medium text-gray-700"},"略称"),c.createElement("input",pl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("shortName")))),c.createElement("div",null,c.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"単価"),c.createElement("input",pl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("price",{valueAsNumber:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"orderPrice",className:"block text-sm font-medium text-gray-700"},"発注単価"),c.createElement("input",pl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("orderPrice",{valueAsNumber:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"業者種別"),c.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},e.companyTypes.map((e=>c.createElement("div",{className:"flex items-center h-5",key:`companyType-${e.id}`},c.createElement("input",pl({id:e.id,key:e.id,type:"checkbox",value:e.id,className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},n("companyTypeIds"))),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.name))))))),c.createElement("div",null,c.createElement("label",{htmlFor:"unitId",className:"block text-sm font-medium text-gray-700"},"単位"),c.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},c.createElement("select",pl({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},n("unitId")),c.createElement("option",{value:""}),e.units.map((e=>c.createElement("option",{key:`unit-${e.id}`,value:e.id},e.name)))))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"発注対象"),c.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},c.createElement("input",pl({type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},n("order"))))),c.createElement("div",{className:"py-3 text-right"},c.createElement("div",{className:"inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},c.createElement(p.rU,{to:`/items${location.search}`},"閉じる")),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!m||!s,loading:r},"保存")))))},bl={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"800px",height:"680px"}},yl=()=>{const{id:e}=(0,D.UO)(),t=(0,D.s0)(),{data:{item:a=null}={}}=function(e){const t=U(U({},R),e);return P.a(Kt,t)}({variables:{id:e}}),{data:{companyTypes:r=[]}={}}=Ht(),{data:{units:n=[]}={}}=Ea();return a&&0!==r.length&&0!==n.length?c.createElement(c.Fragment,null,c.createElement(A(),{isOpen:!0,onRequestClose:()=>{t(`/items${location.search}`)},style:bl},c.createElement(gl,{item:a,companyTypes:r,units:n,onSubmit:()=>{Ta.ZP.success("保存しました"),setTimeout((()=>{t(`/items${location.search}`)}),500)}})),c.createElement(Ta.x7,{position:"top-right"})):null};var xl=Object.defineProperty,fl=Object.getOwnPropertySymbols,El=Object.prototype.hasOwnProperty,hl=Object.prototype.propertyIsEnumerable,vl=(e,t,a)=>t in e?xl(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,wl=(e,t)=>{for(var a in t||(t={}))El.call(t,a)&&vl(e,a,t[a]);if(fl)for(var a of fl(t))hl.call(t,a)&&vl(e,a,t[a]);return e};const Nl={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"800px",height:"880px"}},Bl=e=>{var t,a;const{data:{companyTypes:r=[]}={}}=Ht(),n=()=>{e.onClose()},[o,{loading:l,data:m}]=function(e){const t=U(U({},R),e);return $.D(Oe,t)}({onCompleted:e=>{e.createItem.error||(Ta.ZP.success("保存しました"),setTimeout((()=>{n()}),500))}}),{data:{units:s=[]}={}}=Ea(),{register:i,handleSubmit:d,formState:{errors:u,isDirty:p,isValid:g}}=(0,Pa.cI)({mode:"all"});return c.createElement(A(),{isOpen:e.isOpen,onRequestClose:n,style:Nl},c.createElement("div",{className:"flex flex-col py-1 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:d((t=>{const a=t.code;delete t.code,o({variables:{code:a,year:e.year,attributes:t}})}))},(null==m?void 0:m.createItem.error)&&c.createElement("p",{className:"text-red-400"},null==m?void 0:m.createItem.error),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"コード"),c.createElement("input",wl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},i("code",{required:!0}))),"required"===(null==(t=u.code)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",wl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},i("name",{required:!0}))),"required"===(null==(a=u.name)?void 0:a.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{htmlFor:"shortName",className:"block text-sm font-medium text-gray-700"},"略称"),c.createElement("input",wl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},i("shortName")))),c.createElement("div",null,c.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"単価"),c.createElement("input",wl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},i("price",{valueAsNumber:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"orderPrice",className:"block text-sm font-medium text-gray-700"},"発注単価"),c.createElement("input",wl({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},i("orderPrice",{valueAsNumber:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"price",className:"block text-sm font-medium text-gray-700"},"業者種別"),c.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},r.map((e=>c.createElement("div",{className:"flex items-center h-5",key:`companyType-${e.id}`},c.createElement("input",wl({id:e.id,key:e.id,type:"checkbox",value:e.id,className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},i("companyTypeIds"))),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.name))))))),c.createElement("div",null,c.createElement("label",{htmlFor:"unitId",className:"block text-sm font-medium text-gray-700"},"単位"),c.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},c.createElement("select",wl({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},i("unitId")),c.createElement("option",{value:""}),s.map((e=>c.createElement("option",{key:`unit-${e.id}`,value:e.id},e.name)))))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"発注対象"),c.createElement("div",{className:"flex flex-col justify-center rounded-md shadow-sm"},c.createElement("input",wl({id:"order",type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},i("order"))))),c.createElement("div",{className:"py-3 text-right"},c.createElement("div",{className:"inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},c.createElement(Da,{type:"button",onClick:()=>e.onClose()},"閉じる")),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!p||!g,loading:l},"保存"),c.createElement(Ta.x7,{position:"top-right"}))))))};var kl=Object.defineProperty,Cl=Object.getOwnPropertySymbols,Il=Object.prototype.hasOwnProperty,Dl=Object.prototype.propertyIsEnumerable,jl=(e,t,a)=>t in e?kl(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,$l=(e,t)=>{for(var a in t||(t={}))Il.call(t,a)&&jl(e,a,t[a]);if(Cl)for(var a of Cl(t))Dl.call(t,a)&&jl(e,a,t[a]);return e};const Pl=e=>{const[t,a]=(0,p.lr)(),r=t.get("year"),n=t.get("keyword"),[o,l]=(0,c.useState)(!1),{register:m,handleSubmit:s,watch:i}=(0,Pa.cI)({defaultValues:{year:r,keyword:n}}),d=i("year");let u=e.years.length>0?e.years[0].year+1:null;const[A,{loading:g}]=function(e){const t=U(U({},R),e);return $.D(Fe,t)}({onCompleted:()=>{e.refetch(),e.refetchYear()}});return(0,c.useEffect)((()=>{a({year:d||"",page:"1",keyword:n||""})}),[d]),c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"項目"),c.createElement("form",{onSubmit:s((e=>{a({year:e.year,page:"1",keyword:e.keyword})}))},c.createElement("div",{className:"my-4"},c.createElement("select",$l({className:"px-4 py-2 border border-gray-300"},m("year")),c.createElement("option",{value:""}),e.years.map((e=>c.createElement("option",{key:`year-${e.id}`,value:String(e.year)},e.year)))),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 ml-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>l(!0),disabled:!d},"新規作成"),c.createElement(Bl,{year:d,isOpen:o,onClose:()=>{l(!1),e.refetch()}}),c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 ml-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{confirm(`${u}年度の項目をコピーで作成しますか？`)&&A()},loading:g},"新年度コピー")),c.createElement("div",{className:"grid grid-cols-1 my-2 gap-y-6 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"sm:col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"キーワード"),c.createElement("div",{className:"mt-1"},c.createElement("input",$l({type:"text",className:"block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},m("keyword"))))))),c.createElement(Jr,{totalCount:e.pagination.totalCount||0,currentPage:e.pagination.currentPage||0,totalPages:e.pagination.totalPages||0,pageSize:20}),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"w-6 px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),c.createElement("th",{scope:"col",className:"w-24 px-6 py-3 text-xs font-medium text-left text-gray-500"},"コード"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"名称"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"単価"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"発注対象"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"発注単価"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"業者区分"))),c.createElement("tbody",{className:`divide-y divide-gray-200 ${!e.loadingItems&&"bg-white"}`},e.loadingItems&&c.createElement(ja,null),!e.loadingItems&&e.items.map((t=>c.createElement("tr",{className:"bg-white",key:`item-${t.id}`},c.createElement("td",{className:""},c.createElement("div",{className:"px-2 text-sm"},t.id)),c.createElement("td",{className:""},c.createElement("div",{className:"px-2 cursor-pointer"},c.createElement(p.rU,{to:`/items/${t.id}${location.search}`},t.itemCode.code))),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900"},c.createElement(p.rU,{to:`/items/${t.id}${location.search}`},t.name)),c.createElement("td",{className:"w-20"},c.createElement("div",{className:"px-2 text-sm text-right"},t.price)),c.createElement("td",{className:"w-20"},c.createElement("div",{className:"px-2 text-sm text-right"},!t.order&&"発注なし")),c.createElement("td",{className:"w-20"},c.createElement("div",{className:"px-2 text-sm text-right"},t.orderPrice)),c.createElement("td",{className:""},c.createElement("div",{className:"px-2 text-sm"},(t=>{if(!t)return"";let a=[];for(const r of t){const t=e.companyTypes.find((e=>e.id===`${r}`));t&&a.push(t.name)}return a.join(",")})(t.companyTypeIds)))))))))))},Tl=()=>{const[e]=(0,p.lr)(),t=e.get("page")?Number(e.get("page")):1,a=e.get("year"),r=e.get("keyword"),{data:{items:{items:n=[],pagination:o={}}={}}={},refetch:l,loading:m}=function(e){const t=U(U({},R),e);return P.a(Qt,t)}({variables:{page:t,search:{year:a,keyword:r}},skip:!a}),{data:{years:s=[]}={},refetch:i}=Ia(),{data:{companyTypes:d=[]}={}}=Ht();return s&&0!==s.length?c.createElement(Pl,{items:n,pagination:o,loadingItems:m,companyTypes:d,refetch:l,years:s,refetchYear:i}):null},Sl=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:":id",element:c.createElement(c.Fragment,null,c.createElement(Tl,null),c.createElement(yl,null))}),c.createElement(D.AW,{index:!0,element:c.createElement(Tl,null)})));var Fl=a(9501),Ol=Object.defineProperty,Ml=Object.getOwnPropertySymbols,Ul=Object.prototype.hasOwnProperty,Rl=Object.prototype.propertyIsEnumerable,Yl=(e,t,a)=>t in e?Ol(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Vl=(e,t)=>{for(var a in t||(t={}))Ul.call(t,a)&&Yl(e,a,t[a]);if(Ml)for(var a of Ml(t))Rl.call(t,a)&&Yl(e,a,t[a]);return e};Fl.Ry().shape({lastName:Fl.Z_().trim().required("必須項目です"),firstName:Fl.Z_().trim().required("必須項目です")});const ql=e=>{const t=(0,D.s0)(),[a,{loading:r}]=function(e){const t=U(U({},R),e);return $.D(Re,t)}({onCompleted:()=>t("/jkk_users")}),n={lastName:e.jkkUser.lastName,firstName:e.jkkUser.firstName,active:e.jkkUser.active},{register:o,handleSubmit:l,setValue:m,watch:s,formState:{errors:i,isDirty:d,isValid:u,touchedFields:A}}=(0,Pa.cI)({mode:"all",defaultValues:n}),g=s("active");return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/jkk_users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},e.jkkUser.lastName," ",e.jkkUser.firstName))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:l((t=>{a({variables:{id:e.jkkUser.id,attributes:t}})}))},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),c.createElement("input",Vl({type:"text",name:"lastName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("lastName",{required:!0})))),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),c.createElement("input",Vl({type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("firstName",{required:!0})))),c.createElement("div",{className:"mt-4 space-y-4"},c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",defaultChecked:!0===n.active,checked:!0===g,onChange:e=>{e.target.checked&&m("active",!0,{shouldDirty:!0})}}),c.createElement("label",{htmlFor:"active",className:"block ml-3 text-sm font-medium text-gray-700"},"有効")),c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"non-active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",defaultChecked:!1===n.active,checked:!1===g,onChange:e=>{e.target.checked&&m("active",!1,{shouldDirty:!0})}}),c.createElement("label",{htmlFor:"non-active",className:"block ml-3 text-sm font-medium text-gray-700"},"無効"))),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:r||!u||!d,loading:r},"保存")))))))},Ll=()=>{const{id:e}=(0,D.UO)(),{data:{jkkUser:t=null}={}}=function(e){const t=U(U({},R),e);return P.a(Xt,t)}({variables:{id:e}});return t?c.createElement(ql,{jkkUser:t}):null};var _l=Object.defineProperty,Zl=Object.getOwnPropertySymbols,zl=Object.prototype.hasOwnProperty,Wl=Object.prototype.propertyIsEnumerable,Hl=(e,t,a)=>t in e?_l(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Gl=(e,t)=>{for(var a in t||(t={}))zl.call(t,a)&&Hl(e,a,t[a]);if(Zl)for(var a of Zl(t))Wl.call(t,a)&&Hl(e,a,t[a]);return e};const Jl=()=>{var e,t;const a=(0,D.s0)(),[r,{loading:n}]=function(e){const t=U(U({},R),e);return $.D(Ue,t)}({onCompleted:()=>{a(`/jkk_users${location.search}`)}}),{register:o,handleSubmit:l,formState:{errors:m,isDirty:s,isValid:i}}=(0,Pa.cI)({mode:"all"});return c.createElement("div",{className:"w-full"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/jkk_users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:l((e=>{r({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),c.createElement("input",Gl({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("lastName",{required:!0}))),"required"===(null==(e=m.lastName)?void 0:e.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),c.createElement("input",Gl({type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},o("firstName",{required:!0}))),"required"===(null==(t=m.firstName)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!i||!s,loading:n},"保存")))))))},Kl=()=>{const[e,t]=(0,p.lr)(),a=e.get("active");return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement("div",{onClick:()=>t({active:"true"})},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+("true"===a?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"有効")),c.createElement("div",{onClick:()=>t({active:"false"})},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+("true"!==a?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"無効")))))},Ql=()=>{const[e,t]=(0,p.lr)(),a=e.get("active"),{data:{jkkUsers:r=[]}={}}=ta({variables:{search:{active:"true"===a}},fetchPolicy:"cache-and-network"});return(0,c.useEffect)((()=>{null==a&&t({active:"true"})}),[]),null==a?null:c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"公社担当者"),c.createElement(Kl,null),c.createElement(p.rU,{to:"/jkk_users/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 mt-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),c.createElement("th",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-60"},"名称"))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},r.map((e=>c.createElement("tr",{className:"bg-white",key:e.id},c.createElement("td",{className:"w-6"},c.createElement("div",{className:"px-2 cursor-pointer text-xs"},e.id)),c.createElement("td",{className:"px-4 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/jkk_users/${e.id}`},e.lastName," ",e.firstName))))))))))},Xl=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(Jl,null)}),c.createElement(D.AW,{path:":id",element:c.createElement(Ll,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Ql,null)}))),em=(j.ZP`
  fragment buildingFragment on Building {
    id
    name
    address
    rank
    rankText
    mcTypeId
    buildingTypeId
  }
`,j.ZP`
  fragment buildingNumberFragment on BuildingNumber {
    id
    buildingId
    name
    position
  }
`,j.ZP`
  fragment buildingTypeFragment on BuildingType {
    id
    name
  }
`,j.ZP`
  fragment commentFragment on Comment {
    id
    comment
    commentTypeId
    remark
  }
`,j.ZP`
  fragment commentItemCodeFragment on CommentItemCode {
    id
    commentId
    itemCodeId
    position
  }
`,j.ZP`
  fragment commentTypeFragment on CommentType {
    id
    name
    position
  }
`,j.ZP`
  fragment companyFragment on Company {
    id
    isOrderEach
    isOrderMonthly
    name
    zipcode
    address
    tel
    fax
    companyTypeIds
  }
`,j.ZP`
  fragment companyTypeFragment on CompanyType {
    id
    name
    position
  }
`,j.ZP`
  fragment imageCommentFragment on ImageComment {
    id
    projectImageId
    code
    text
  }
`,j.ZP`
  fragment itemFragment on Item {
    id
    name
    shortName
    price
    orderPrice
    companyTypeIds
    companyId
    order
    unitId
  }
`,j.ZP`
  fragment itemCodeFragment on ItemCode {
    id
    code
    name
  }
`,j.ZP`
  fragment jkkUserFragment on JkkUser {
    id
    email
    firstName
    lastName
    active
  }
`,j.ZP`
  fragment mcTypeFragment on McType {
    id
    name
  }
`,j.ZP`
  fragment orderFragment on Order {
    id
    projectId
    companyId
    orderType
    isInvoice
    orderDate
    totalPrice
    exportCsvDate
  }
`,j.ZP`
  fragment paginationFragment on Pagination {
    resultsCount
    totalCount
    totalPages
    currentPage
    nextPage
    previousPage
    hasNextPage
    hasPreviousPage
  }
`,j.ZP`
  fragment projectFragment on Project {
    id
    yearId
    buildingId
    buildingNumberId
    roomId
    userIds
    jkkUserId
    assesmentUserId
    constructionUserId
    totalItemPrice
    totalItemPriceRound
    tax
    totalPrice
    totalPriceInput
    name
    jkkStatus1
    jkkStatus2
    jkkStatus3
    jkkStatus4
    orderNumberType
    orderNumber
    assessmentDate
    assessmentUrl
    orderDate1
    orderMemo1
    orderDate2
    orderMemo2
    orderDate3
    orderMemo3
    electricStartDate1
    electricEndDate1
    waterStartDate1
    waterEndDate1
    electricStartDate2
    electricEndDate2
    waterStartDate2
    waterEndDate2
    electricStartDate3
    electricEndDate3
    waterStartDate3
    waterEndDate3
    closeTypes
    closeReason
    isVermiculite
    isFinishedVermiculite
    vermiculiteDate
    processCreatedDate
    beforeConstructionImageDate
    afterConstructionImageDate
    checklistFirstDate
    checklistLastDate
    address
    constructionStartDate
    constructionEndDate
    imageReportUrl
    isFaq
    isLackImage
    isCompleteImage
    submitDocumentDate
    availableDate
    livingYear
    livingMonth
    leaveDate
    isManagerAsbestosDocument1
    isManagerAsbestosDocument2
    isManagerAsbestosDocument3
    isOfficeworkAsbestosDocument1
    isOfficeworkAsbestosDocument2
    isOfficeworkAsbestosDocument3
    remark
    updatedItemsAt
    editUserId
    itemUploadDateText
    reportDateText
    checklistDateText
    lackImageDateText
    completeImageDateText
    invoiceDateText
    imageStatus
  }
`,j.ZP`
  fragment projectImageFragment on ProjectImage {
    id
    projectId
    roomTypeId
    imageType
    imageBefore
    imageAfter
    image
    comment
    position
  }
`,j.ZP`
  fragment projectItemFragment on ProjectItem {
    id
    projectId
    itemId
    companyId
    code
    name
    count
    unitPrice
    amount
    roomTypeIds
    sequenceNumber
    updatedItemsAt
  }
`,j.ZP`
  fragment projectReportFragment on ProjectReport {
    id
    projectId
    worksheetBase
    worksheetItems
    worksheetImages
    worksheetChecklist
    spreadsheetId1
    spreadsheetId2
    spreadsheetId3
    spreadsheetId4
    spreadsheetId5
    spreadsheetId6
    spreadsheetId7
    spreadsheetDatetime1
    spreadsheetDatetime2
    spreadsheetDatetime3
    spreadsheetDatetime4
    spreadsheetDatetime5
    spreadsheetDatetime6
    spreadsheetDatetime7
    spreadsheetCheckReport1
    spreadsheetCheckReport2
  }
`,j.ZP`
  fragment purchaseOrderFragment on PurchaseOrder {
    id
    projectId
    fileUrl
  }
`,j.ZP`
  fragment roomFragment on Room {
    id
    buildingNumberId
    name
    position
    roomModelTypeId
    roomLayoutId
  }
`,j.ZP`
  fragment roomLayoutFragment on RoomLayout {
    id
    name
    price1
    price2
    position
  }
`,j.ZP`
  fragment roomModelTypeFragment on RoomModelType {
    id
    name
    position
  }
`,j.ZP`
  fragment roomTypeFragment on RoomType {
    id
    name
    position
  }
`,j.ZP`
  fragment unitFragment on Unit {
    id
    name
  }
`,j.ZP`
  fragment userFragment on User {
    id
    loginId
    email
    role
    firstName
    lastName
    tel
    active
    isAssesment
  }
`),tm=(j.ZP`
  fragment yearFragment on Year {
    id
    year
  }
`,j.ZP`
  query currentUser {
    currentUser {
      ...userFragment
    }
  }
  ${em}
`),am=({children:e})=>{const t=(0,D.s0)(),{data:{currentUser:a=null}={}}=va();return console.log("🚀 ~ file: Login.tsx ~ line 17 ~ currentUser",a),(0,c.useEffect)((()=>{a&&t("/")}),[a]),c.createElement("div",{className:"flex flex-col justify-center min-h-screen py-12 bg-gray-50 sm:px-6 lg:px-8"},e)},rm=()=>{const e=(0,D.s0)(),[t,{data:a,loading:r}]=function(e){const t=U(U({},R),e);return $.D(ue,t)}({update:(e,{data:t})=>{(null==t?void 0:t.signin.error)||e.writeQuery({query:tm,data:{currentUser:null==t?void 0:t.signin.user}})},onCompleted:t=>{(null==t?void 0:t.signin.error)||e("/")}}),n=(0,Ar.TA)({initialValues:{loginId:"",password:""},onSubmit:e=>{t({variables:{loginId:e.loginId,password:e.password}})}});return c.createElement(am,null,c.createElement("div",{className:"sm:mx-auto sm:w-full sm:max-w-md"},c.createElement("img",{className:"w-auto h-12 mx-auto",src:"https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg",alt:"Workflow"}),c.createElement("h2",{className:"mt-6 text-3xl font-extrabold text-center text-gray-900"},"TOTO Login")),c.createElement("div",{className:"mt-8 sm:mx-auto sm:w-full sm:max-w-lg"},c.createElement("div",{className:"px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10"},(null==a?void 0:a.signin.error)&&c.createElement("div",{className:"p-4 rounded-md bg-red-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"ml-3"},c.createElement("h3",{className:"text-sm font-medium text-red-800"},"エラーをご確認ください"),c.createElement("div",{className:"mt-2 text-sm text-red-700"},c.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},c.createElement("li",null,null==a?void 0:a.signin.error)))))),c.createElement("form",{className:"space-y-6",onSubmit:n.handleSubmit},c.createElement("div",null,c.createElement("label",{htmlFor:"loginId",className:"block text-sm font-medium text-gray-700"},"ログインID"),c.createElement("div",{className:"mt-1"},c.createElement("input",{name:"loginId",type:"text",required:!0,className:"block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:n.values.loginId,onChange:n.handleChange,onBlur:n.handleBlur}))),c.createElement("div",null,c.createElement("label",{htmlFor:"password",className:"block text-sm font-medium text-gray-700"},"Password"),c.createElement("div",{className:"mt-1"},c.createElement("input",{id:"password",name:"password",type:"password",autoComplete:"current-password",required:!0,className:"block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:n.values.password,onChange:n.handleChange,onBlur:n.handleBlur}))),c.createElement("div",null,c.createElement(Da,{type:"submit",className:"flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:r},"ログイン"))))))},nm=()=>c.createElement(rm,null);var om=a(381),lm=a.n(om),mm=a(9198),sm=a.n(mm);const im=({order:e})=>{var t;return c.createElement("tr",{className:"bg-white"},c.createElement("td",{className:"text-sm px-2"},null==e?void 0:e.id),c.createElement("td",{className:"py-2 text-sm"},e.orderDate),c.createElement("td",{className:"px-2 py-4 text-sm"},e.company.name),c.createElement("td",{className:"px-2 py-4 text-sm"},e.project.orderNumberType,"-",e.project.orderNumber),c.createElement("td",{className:"px-2 py-4 text-sm"},e.project.name),c.createElement("td",{className:"px-2 py-4 text-sm"},e.project.address),c.createElement("td",{className:"px-2 py-4 text-sm"},null==(t=e.totalPrice)?void 0:t.toLocaleString()),c.createElement("td",{className:"px-2 py-4 text-sm"}))},cm=()=>{const[e,t]=(0,p.lr)(),a=e.get("page"),r=e.get("from"),n=e.get("to"),o=e.get("companyId"),l=e.get("spreadsheetId"),{data:{orders:{orders:m=[],pagination:s={}}={}}={}}=function(e){const t=U(U({},R),e);return P.a(ra,t)}({variables:{page:a?Number(a):1,search:{from:r,to:n,companyId:o}},fetchPolicy:"cache-and-network"}),{data:{companies:i=[]}={}}=_t(),d=(0,Ar.TA)({initialValues:{from:r,to:n,companyId:o,spreadsheetId:l},onSubmit:e=>{t({from:e.from||"",to:e.to||"",companyId:e.companyId||"",page:"1",spreadsheetId:e.spreadsheetId||""})}}),[u]=function(e){const t=U(U({},R),void 0);return $.D(Ye,t)}();return c.createElement("div",{className:"flex flex-col h-full"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 h-full"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"発注一覧"),c.createElement("form",{className:"px-10 py-2 mb-4 bg-white",onSubmit:d.handleSubmit},c.createElement("div",{className:"mt-6"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"発注日"),c.createElement("div",{className:"flex items-center space-x-4"},c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",selected:d.values.from&&new Date(d.values.from),className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",onChange:e=>d.setFieldValue("from",lm()(e).format("YYYY-MM-DD"))})),c.createElement("span",null," - "),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",selected:d.values.to&&new Date(d.values.to),className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",onChange:e=>d.setFieldValue("to",lm()(e).format("YYYY-MM-DD"))}))),c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"業者"),c.createElement("div",{className:"relative w-64"},c.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",value:d.values.companyId,onChange:e=>d.setFieldValue("companyId",e.target.value)},c.createElement("option",{value:""},"業者を選択してください"),i.map((e=>c.createElement("option",{value:e.id},e.name))))),c.createElement("div",{className:"sm:col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"スプレッドシートID"),c.createElement("div",{className:"mt-1"},c.createElement("input",{name:"spreadsheetId",type:"text",className:"block w-44 px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:d.values.spreadsheetId,onChange:d.handleChange,onBlur:d.handleBlur})))),c.createElement("div",{className:"mt-6 flex space-x-2"},c.createElement(Da,{type:"button",className:"text-white w-40 px-4 py-2 text-sm font-medium bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-300",onClick:()=>d.handleSubmit()},"検索"),c.createElement(Da,{type:"button",disabled:!d.values.spreadsheetId,className:"text-white w-44 px-4 py-2 text-sm font-medium bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-300",onClick:()=>{u({variables:{spreadsheetId:d.values.spreadsheetId,search:{from:d.values.from,to:d.values.to,companyId:d.values.companyId}}})}},"スプレッドシート出力"))),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-12"},"ID"),c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-28"},"発注日"),c.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"業者"),c.createElement("th",{scope:"col",className:"w-28 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"指示番号"),c.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"案件名"),c.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"案件住所"),c.createElement("th",{scope:"col",className:"w-20 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"金額"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},m.map((e=>c.createElement(im,{order:e,key:null==e?void 0:e.id}))))),c.createElement(Jr,{totalCount:s.totalCount||0,currentPage:s.currentPage||0,totalPages:s.totalPages||0,pageSize:20}))))},dm=()=>c.createElement(ln,null,c.createElement(cm,null)),um=({project:e})=>c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/projects",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700","aria-current":void 0},"案件一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==e?void 0:e.name))))),Am=()=>{const{id:e}=(0,D.UO)(),t=e=>location.pathname.indexOf(e)>-1;return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement(p.rU,{to:`/projects/${e}/form/base${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("form")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報")),c.createElement(p.rU,{to:`/projects/${e}/items${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("items")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"明細")),c.createElement(p.rU,{to:`/projects/${e}/company${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("company")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"発注")),c.createElement(p.rU,{to:`/projects/${e}/images${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("images")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"画像")),c.createElement(p.rU,{to:`/projects/${e}/report${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("report")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"レポート")),c.createElement(p.rU,{to:`/projects/${e}/remark${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+(t("remark")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"備考")))))};var pm=a(4770);const gm=e=>c.createElement("div",{className:"flex items-center space-x-1"},c.createElement("input",{id:e.name,name:e.name,type:"checkbox",className:"w-5 h-6 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:e.checked,onChange:t=>{e.onChange(t)}}),c.createElement("label",{className:"block text-xs font-medium text-gray-700"},e.label)),bm=e=>c.createElement("div",{className:"mt-1 text-indigo-700 font-bold"},c.createElement("p",null,e.date&&e.date.split(" ")[0]),c.createElement("p",null,e.date&&e.date.split(" ")[1])),ym=({project:e})=>{var t,a,r,n,o,l,m,s,i,d,u;const A=(0,D.s0)(),{data:{currentUser:p}={}}=va(),g=!(!e.editUserId||e.editUserId===p.id),[b]=Ke(),y=(0,Ar.TA)({enableReinitialize:!0,initialValues:{jkkStatus1:e.jkkStatus1,jkkStatus2:e.jkkStatus2,jkkStatus3:e.jkkStatus3,jkkStatus4:e.jkkStatus4},onSubmit:t=>{b({variables:{id:e.id,attributes:t}})}}),[x]=function(e){const t=U(U({},R),e);return $.D(He,t)}({onCompleted:()=>A("/projects")});return c.createElement("form",{onSubmit:y.handleSubmit},c.createElement("div",{className:"flex items-center justify-between"},c.createElement("div",{className:"my-4 font-bold text-2xl"},"指示番号：",e.orderNumberType,"-",e.orderNumber),g&&c.createElement("div",{className:"ml-auto"},c.createElement("div",{className:"text-right"},c.createElement("div",{className:"inline-flex items-center border border-gray-500 px-4 py-2"},c.createElement("span",{className:"text-xs text-gray-500"},"編集中："),c.createElement("div",{className:"text-sm text-gray-500"},null==(t=e.editUser)?void 0:t.lastName," ",null==(a=e.editUser)?void 0:a.firstName))),c.createElement("p",{className:"text-red-600"},"他ユーザーが編集中のため更新できません"))),c.createElement("div",{className:"flex items-center gap-8"},c.createElement("div",{className:"flex items-center space-x-2 text-xs"},c.createElement("span",{className:"text-gray-500"},"件名："),c.createElement("span",{className:"text-gray-900 text-base font-medium"},null==(r=e.building)?void 0:r.name),c.createElement("span",{className:"text-gray-900 text-base font-medium"},null==(n=e.buildingNumber)?void 0:n.name,c.createElement("span",{className:"ml-2 text-gray-500"},"号棟")),c.createElement("span",{className:"text-gray-900 text-base font-medium"},null==(o=e.room)?void 0:o.name,c.createElement("span",{className:"ml-2 text-gray-500"},"号室"))),c.createElement("div",{className:"flex items-center space-x-2"},c.createElement("span",{className:"text-xs font-medium text-gray-500"},"住所："),c.createElement("span",{className:"font-medium text-gray-900 text-md"},null==(l=e.building)?void 0:l.address)),c.createElement(pm.v,{as:"div",className:"relative flex text-left ml-4"},c.createElement(pm.v.Button,null,c.createElement(_a,{className:"h-6 text-gray-500 cursor-pointer"})),c.createElement(pm.v.Items,{className:"absolute right-0 mt-2 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none py-2 px-4 whitespace-pre"},(()=>c.createElement("button",{className:"hover:bg-gray-300 text-gray-500 flex items-center rounded-md px-2 py-2 text-sm",onClick:()=>{confirm("削除してよろしいですか？")&&x({variables:{id:e.id}})}},c.createElement(Xa,{className:"h-4 mr-2"}),"削除"))))),c.createElement("div",{className:"flex my-2 space-x-7 text-gray-500 text-sm"},c.createElement("div",null,"公社年度：",e.year.year),c.createElement("div",null,"管轄窓口：",null==(s=null==(m=e.building)?void 0:m.mcType)?void 0:s.name),c.createElement("div",null,"住宅区分：",null==(d=null==(i=e.building)?void 0:i.buildingType)?void 0:d.name),c.createElement("div",{className:"flex items-center space-x-2"},c.createElement("label",{className:"block text-sm font-medium"},"工事金額："),c.createElement("span",null,null==(u=e.totalPriceInput)?void 0:u.toLocaleString(),c.createElement("span",{className:"ml-1 text-sm text-gray-700"},"円")))),c.createElement("div",{className:"flex mt-2 items-center"},c.createElement("div",{className:"text-xs font-medium text-gray-500 mr-4"},"当社状況"),c.createElement("div",{className:"flex items-start space-x-1 h-[60px]"},c.createElement("div",{className:"text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center"},c.createElement("div",null,"①査定表読込済"),c.createElement(bm,{date:e.itemUploadDateText})),c.createElement("div",{className:"text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center"},c.createElement("div",null,"②施工前済・質疑回答待ち"),c.createElement(bm,{date:e.reportDateText})),c.createElement("div",{className:"text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center"},c.createElement("div",null,"③施工前済・最終CL済"),c.createElement(bm,{date:e.checklistDateText})),c.createElement("div",{className:"text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center"},c.createElement("div",null,"④施工後不足写真待ち"),c.createElement(bm,{date:e.lackImageDateText})),c.createElement("div",{className:"text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center"},c.createElement("div",null,"⑤施工後済"),c.createElement(bm,{date:e.completeImageDateText})),c.createElement("div",{className:"text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center"},c.createElement("div",null,"⑥請求書済"),c.createElement(bm,{date:e.invoiceDateText})))),c.createElement("div",{className:"flex items-center mt-2"},c.createElement("div",{className:"text-xs font-medium text-gray-500 mr-4"},"JKK状況"),c.createElement("div",{className:"flex space-x-4"},c.createElement(gm,{label:"①CL済",name:"jkkStatus1",checked:y.values.jkkStatus1,onChange:e=>{y.setFieldValue("jkkStatus1",e.target.checked),y.handleSubmit()}}),c.createElement(gm,{label:"②指示書済",name:"jkkStatus2",checked:y.values.jkkStatus2,onChange:e=>{y.setFieldValue("jkkStatus2",e.target.checked),y.handleSubmit()}}),c.createElement(gm,{label:"③指示書再発行待ち",name:"jkkStatus3",checked:y.values.jkkStatus3,onChange:e=>{y.setFieldValue("jkkStatus3",e.target.checked),y.handleSubmit()}}),c.createElement(gm,{label:"④指示書再発行済み",name:"jkkStatus4",checked:y.values.jkkStatus4,onChange:e=>{y.setFieldValue("jkkStatus4",e.target.checked),y.handleSubmit()}}))))},xm=({children:e})=>{const{id:t}=(0,D.UO)(),{data:{project:a=null}={}}=oa({variables:{id:t}});return a?c.createElement("div",{className:"px-6"},c.createElement("div",{className:"top-0 bg-gray-100 z-10 sticky pt-4"},c.createElement(um,{project:a}),c.createElement(ym,{project:a}),c.createElement(Am,null)),c.createElement("div",{className:"pb-10"},e)):null};var fm=a(2660),Em=(e,t,a)=>new Promise(((r,n)=>{var o=e=>{try{m(a.next(e))}catch(e){n(e)}},l=e=>{try{m(a.throw(e))}catch(e){n(e)}},m=e=>e.done?r(e.value):Promise.resolve(e.value).then(o,l);m((a=a.apply(e,t)).next())}));(0,mm.registerLocale)("ja",fm.Z);const hm=({order:e})=>{const[t]=function(e){const t=U(U({},R),void 0);return $.D(et,t)}(),a=(0,Ar.TA)({initialValues:{companyId:e.companyId,isInvoice:e.isInvoice,orderDate:e.orderDate},onSubmit:a=>{t({variables:{id:e.id,attributes:a}})}});return c.createElement("form",{className:"flex",onSubmit:a.handleSubmit},c.createElement("div",{className:"w-64 px-4 ml-4"},c.createElement("div",{className:"flex items-center"},e.company.name)),c.createElement("div",{className:"w-48 px-4"},c.createElement("div",{className:"flex items-center"},c.createElement("div",{className:"flex rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:a.values.orderDate&&new Date(a.values.orderDate),onChange:e=>Em(void 0,null,(function*(){yield a.setFieldValue("orderDate",lm()(e).format("YYYY-MM-DD")),a.handleSubmit()}))})))),c.createElement("div",{className:"w-32 px-4 my-auto"},c.createElement("div",{className:"flex justify-center rounded-md shadow-sm"},c.createElement("input",{type:"checkbox",name:"isInvoice",className:"w-4 h-4 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500",checked:a.values.isInvoice,onChange:e=>Em(void 0,null,(function*(){yield a.setFieldValue("isInvoice",e.target.checked),a.handleSubmit()}))}))),"monthly"===e.orderType&&c.createElement("div",{className:"w-20 text-sm"},e.exportCsvDate))},vm=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=sa({variables:{id:e},fetchPolicy:"cache-and-network"});return t?c.createElement("div",{className:"flex flex-col mt-10"},c.createElement("div",{className:"text-lg font-bold"},"都度発注"),c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},c.createElement("div",{className:"min-w-full divide-y divide-gray-200"},c.createElement("div",{className:"flex flex-1 bg-gray-50"},c.createElement("div",{className:"w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"会社"),c.createElement("div",{className:"w-48 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"発注日"),c.createElement("div",{className:"w-32 px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"請求書確認")),c.createElement("div",{className:"bg-white divide-y divide-gray-200"},t.ordersEach.map((e=>c.createElement(hm,{key:e.id,order:e}))))))))):null},wm=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=sa({variables:{id:e},fetchPolicy:"cache-and-network"});return t?c.createElement("div",{className:"flex flex-col mt-10"},c.createElement("div",{className:"text-lg font-bold"},"月次発注"),c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},c.createElement("div",{className:"min-w-full divide-y divide-gray-200"},c.createElement("div",{className:"flex flex-1 bg-gray-50"},c.createElement("div",{className:"w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"会社"),c.createElement("div",{className:"w-48 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"発注日"),c.createElement("div",{className:"w-32 px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"発注書類作成可"),c.createElement("div",{className:"px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"レポート出力日")),c.createElement("div",{className:"bg-white divide-y divide-gray-200"},t.ordersMonthly.map((e=>c.createElement(hm,{key:e.id,order:e}))))))))):null},Nm=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=oa({variables:{id:e},fetchPolicy:"cache-and-network"});return t?c.createElement(xm,null,c.createElement(vm,null),c.createElement(wm,null)):null};var Bm=a(6486),km=a.n(Bm);const Cm=[{value:"close_type1",name:"閉鎖住宅"},{value:"close_type2",name:"私負担清算"},{value:"close_type3",name:"本工事"},{value:"close_type4",name:"工事保留"},{value:"close_type5",name:"工事中止"},{value:"close_type6",name:"工事再開"},{value:"close_type7",name:"被災者住宅"},{value:"close_type8",name:"検査対象物件"},{value:"close_type9",name:"その他"}],Im=()=>{const{id:e}=(0,D.UO)(),t=e=>location.pathname.indexOf(e)>-1;return c.createElement("nav",{className:"flex mt-6 space-x-4","aria-label":"Tabs"},c.createElement(p.rU,{to:`/projects/${e}/form/base`,className:`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${t("form/base")&&"bg-gray-200"}`},"現場詳細"),c.createElement(p.rU,{to:`/projects/${e}/form/docs`,className:`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${t("form/docs")&&"bg-gray-200"}`},"書類管理"),c.createElement(p.rU,{to:`/projects/${e}/form/management${location.search}`,className:`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${t("form/management")&&"bg-gray-200"}`},"現場管理"))},Dm=({user:e})=>c.createElement("div",null,c.createElement("span",{className:"relative inline-flex items-center rounded-full border border-gray-400 px-3 py-0.5 text-sm"},null==e?void 0:e.lastName," ",null==e?void 0:e.firstName));(0,mm.registerLocale)("ja",fm.Z);const jm=()=>{const{id:e}=(0,D.UO)(),[t,a]=(0,c.useState)(null),[r,n]=(0,c.useState)(!1),{data:{project:o={}}={}}=oa({variables:{id:e}}),{data:{currentUser:l}={}}=va(),m=o.editUserId&&o.editUserId!==l.id,[s]=Ke(),{data:{users:i=[]}={}}=ka({variables:{search:{active:!0,isAssesment:!0}}}),{data:{users:d=[]}={}}=ka({variables:{search:{active:!0}}}),{data:{jkkUsers:u=[]}={}}=ta({variables:{search:{active:!0}}}),A=(0,Ar.TA)({enableReinitialize:!0,initialValues:{orderNumberType:(null==o?void 0:o.orderNumberType)||"",orderNumber:(null==o?void 0:o.orderNumber)||"",name:(null==o?void 0:o.name)||"",jkkUserId:(null==o?void 0:o.jkkUserId)||"",assesmentUserId:(null==o?void 0:o.assesmentUserId)||"",constructionUserId:(null==o?void 0:o.constructionUserId)||"",address:(null==o?void 0:o.address)||"",assessmentDate:null==o?void 0:o.assessmentDate,availableDate:(null==o?void 0:o.availableDate)||"",livingYear:null==o?void 0:o.livingYear,livingMonth:null==o?void 0:o.livingMonth,leaveDate:(null==o?void 0:o.leaveDate)||"",totalPriceInput:null==o?void 0:o.totalPriceInput,orderDate1:(null==o?void 0:o.orderDate1)||"",orderMemo1:(null==o?void 0:o.orderMemo1)||"",orderDate2:(null==o?void 0:o.orderDate2)||"",orderMemo2:(null==o?void 0:o.orderMemo2)||"",orderDate3:(null==o?void 0:o.orderDate3)||"",orderMemo3:(null==o?void 0:o.orderMemo3)||"",closeTypes:(null==o?void 0:o.closeTypes)||[],closeReason:null==o?void 0:o.closeReason,electricStartDate1:(null==o?void 0:o.electricStartDate1)||"",electricEndDate1:(null==o?void 0:o.electricEndDate1)||"",waterStartDate1:(null==o?void 0:o.waterStartDate1)||"",waterEndDate1:(null==o?void 0:o.waterEndDate1)||"",electricStartDate2:(null==o?void 0:o.electricStartDate2)||"",electricEndDate2:(null==o?void 0:o.electricEndDate2)||"",waterStartDate2:(null==o?void 0:o.waterStartDate2)||"",waterEndDate2:(null==o?void 0:o.waterEndDate2)||"",electricStartDate3:(null==o?void 0:o.electricStartDate3)||"",electricEndDate3:(null==o?void 0:o.electricEndDate3)||"",waterStartDate3:(null==o?void 0:o.waterStartDate3)||"",waterEndDate3:(null==o?void 0:o.waterEndDate3)||"",isVermiculite:null==o?void 0:o.isVermiculite,isFinishedVermiculite:null==o?void 0:o.isFinishedVermiculite,vermiculiteDate:(null==o?void 0:o.vermiculiteDate)||"",beforeConstructionImageDate:(null==o?void 0:o.beforeConstructionImageDate)||"",checklistFirstDate:(null==o?void 0:o.checklistFirstDate)||"",checklistLastDate:(null==o?void 0:o.checklistLastDate)||"",constructionStartDate:(null==o?void 0:o.constructionStartDate)||"",constructionEndDate:(null==o?void 0:o.constructionEndDate)||"",userIds:(null==o?void 0:o.userIds)||[]},onSubmit:t=>{s({variables:{id:e,attributes:t}})}}),p=e=>i.find((t=>t.id===e)),g=u.map((e=>({value:e.id,label:`${e.lastName} ${e.firstName}`})));return(0,c.useEffect)((()=>{var e;A.values.totalPriceInput&&a(null==(e=A.values.totalPriceInput)?void 0:e.toLocaleString())}),[A.values.totalPriceInput]),o?c.createElement(xm,null,c.createElement(Im,null),c.createElement("div",{className:"mt-4"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:A.handleSubmit},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-12"},c.createElement("div",{className:"col-span-12"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示番号"),c.createElement("div",{className:"flex"},c.createElement("input",{type:"text",disabled:m,name:"orderNumberType",className:"block w-16 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.orderNumberType,onChange:A.handleChange,onBlur:()=>A.handleSubmit()}),c.createElement("span",{className:"inline-flex items-center mx-4"},"-"),c.createElement("input",{type:"text",name:"orderNumber",disabled:m,className:"block w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.orderNumber,onChange:A.handleChange,onBlur:()=>A.handleSubmit()}))),c.createElement("div",{className:"col-span-12"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"閉鎖区分"),c.createElement("div",{className:"flex items-start space-x-4"},Cm.map((e=>c.createElement("div",{className:"flex items-center h-5",key:`closeTypeValues-${e.value}`},c.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,checked:A.values.closeTypes.includes(e.value),onChange:e=>{const t=`${e.target.value}`;A.values.closeTypes.includes(t)?A.setFieldValue("closeTypes",A.values.closeTypes.filter((e=>e!==t))):A.setFieldValue("closeTypes",[...A.values.closeTypes,t]),A.handleSubmit()},onBlur:A.handleBlur}),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.name))))))),A.values.closeTypes.includes("close_type9")&&c.createElement(c.Fragment,null,c.createElement("div",{className:"col-span-8"},c.createElement("label",null,"閉鎖区分その他内容"),c.createElement("input",{type:"text",name:"closeReason",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.closeReason,onChange:A.handleChange,onBlur:()=>A.handleSubmit()})),c.createElement("div",{className:"col-span-4"})),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"査定日"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.assessmentDate&&new Date(A.values.assessmentDate),onChange:e=>{A.setFieldValue("assessmentDate",lm()(e).format("YYYY-MM-DD")),A.handleSubmit()}}))),c.createElement("div",{className:"col-span-8"}),c.createElement("div",{className:"col-span-12 my-2 border-t border-gray-300"}),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"公社担当者"),c.createElement(br.ZP,{options:g,className:"px-4 w-72",isClearable:!0,closeMenuOnSelect:!0,value:g.find((e=>e.value===A.values.jkkUserId)),onChange:e=>{A.setFieldValue("jkkUserId",e.value)&&A.handleSubmit()}})),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"当社担当者（査定）"),c.createElement("select",{value:A.values.assesmentUserId,className:"px-4 py-2 border border-gray-300",onChange:e=>A.setFieldValue("assesmentUserId",e.target.value)&&A.handleSubmit()},c.createElement("option",{value:""}),d.map((e=>c.createElement("option",{key:`assesmentUserId-${e.id}`,value:e.id},`${e.lastName} ${e.firstName}`))))),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"当社担当者（工事）"),c.createElement("select",{value:A.values.constructionUserId,className:"px-4 py-2 border border-gray-300",onChange:e=>A.setFieldValue("constructionUserId",e.target.value)&&A.handleSubmit()},c.createElement("option",{value:""}),d.map((e=>c.createElement("option",{key:`constructionUser-${e.id}`,value:e.id},`${e.lastName} ${e.firstName}`))))),c.createElement("div",{className:"col-span-12"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"査定同行者"," ",c.createElement("span",{className:"text-gray-600 bg-gray-200 ml-auto inline-block py-0.5 px-3 text-xs rounded-full"},A.values.userIds.length)),c.createElement("div",{className:"flex"},A.values.userIds.map((e=>c.createElement(Dm,{user:p(e),key:`user-${e}`})))),c.createElement("div",{className:"h-40 px-4 py-2 mt-1 overflow-y-scroll border border-gray-300 rounded-md flex flex-wrap gap-4"},i.map((e=>c.createElement("div",{className:"flex items-center h-5",key:`user-${e.id}`},c.createElement("input",{id:e.id,name:`${e.lastName} ${e.firstName}`,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.id,checked:A.values.userIds.includes(e.id),onChange:e=>{const t=`${e.target.value}`;A.values.userIds.includes(t)?A.setFieldValue("userIds",A.values.userIds.filter((e=>e!==t))):A.setFieldValue("userIds",[...A.values.userIds,t]),A.handleSubmit()},onBlur:A.handleBlur}),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},`${e.lastName} ${e.firstName}`))))))),c.createElement("div",{className:"col-span-12 my-6 border-t border-gray-300"}),c.createElement("div",{className:"col-span-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"退去日"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.leaveDate&&new Date(A.values.leaveDate),onChange:e=>A.setFieldValue("leaveDate",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"募集可能日"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.availableDate&&new Date(A.values.availableDate),onChange:e=>A.setFieldValue("availableDate",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"入居年数-年"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement("input",{type:"number",name:"livingYear",min:1,className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.livingYear,onChange:A.handleChange,onBlur:()=>A.handleSubmit()}))),c.createElement("div",{className:"col-span-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"入居年数-ヶ月"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement("select",{value:A.values.livingMonth,className:"px-4 py-1 mt-1 border border-gray-300",onChange:e=>A.setFieldValue("livingMonth",Number(e.target.value))&&A.handleSubmit()},c.createElement("option",{value:""}),km().times(11).map((e=>c.createElement("option",{key:`livingMonth-${e}`,value:Number(e+1)},e+1)))))),c.createElement("div",{className:"col-span-8"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"住所"),c.createElement("input",{type:"text",name:"address",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.address,onChange:A.handleChange,onBlur:()=>A.handleSubmit()})),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"工事金額（税込み）"),c.createElement("div",{className:"relative"},c.createElement("div",{className:"absolute inset-y-0 left-0 items-center px-3 pointer-events-none bg-gray-200 text-sm inline-flex"},"¥"),c.createElement("input",{type:r?"number":"text",name:"totalPriceInput",className:"pl-10 block w-full py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:r?A.values.totalPriceInput:t,onChange:A.handleChange,onBlur:()=>{n(!1),A.handleSubmit()},onFocus:()=>{n(!0)}}))),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示日"),c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.constructionStartDate&&new Date(A.values.constructionStartDate),onChange:e=>A.setFieldValue("constructionStartDate",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()})),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"完了予定日"),c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.constructionEndDate&&new Date(A.values.constructionEndDate),onChange:e=>A.setFieldValue("constructionEndDate",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()})),c.createElement("div",{className:"col-span-4"}),c.createElement("div",{className:"col-span-12 border-t border-gray-300"}),c.createElement("div",{className:"col-span-12 border-t border-gray-300"}),c.createElement("div",{className:"col-span-12"},c.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"指示書発行")),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示書発行日1"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.orderDate1&&new Date(A.values.orderDate1),onChange:e=>A.setFieldValue("orderDate1",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-8"},c.createElement("label",null,"指示書発行メモ1"),c.createElement("input",{type:"text",name:"orderMemo1",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.orderMemo1,onChange:A.handleChange,onBlur:()=>A.handleSubmit()})),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示書発行日2"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.orderDate2&&new Date(A.values.orderDate2),onChange:e=>A.setFieldValue("orderDate2",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-8"},c.createElement("label",null,"指示書発行メモ2"),c.createElement("input",{type:"text",name:"orderMemo2",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.orderMemo2,onChange:A.handleChange,onBlur:()=>A.handleSubmit()})),c.createElement("div",{className:"col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"指示書発行日3"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:A.values.orderDate3&&new Date(A.values.orderDate3),onChange:e=>A.setFieldValue("orderDate3",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-8"},c.createElement("label",null,"指示書発行メモ3"),c.createElement("input",{type:"text",name:"orderMemo2",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:A.values.orderMemo3,onChange:A.handleChange,onBlur:()=>A.handleSubmit()})),c.createElement("div",{className:"col-span-12 border-t border-gray-300"}),c.createElement("div",{className:"col-span-12"},c.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"電気水道")),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気開始日1"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.electricStartDate1&&new Date(A.values.electricStartDate1),onChange:e=>A.setFieldValue("electricStartDate1",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気停止日1"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.electricEndDate1&&new Date(A.values.electricEndDate1),onChange:e=>A.setFieldValue("electricEndDate1",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道開始日1"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.waterStartDate1&&new Date(A.values.waterStartDate1),onChange:e=>A.setFieldValue("waterStartDate1",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道停止日1"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.waterEndDate1&&new Date(A.values.waterEndDate1),onChange:e=>A.setFieldValue("waterEndDate1",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気開始日2"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.electricStartDate2&&new Date(A.values.electricStartDate2),onChange:e=>A.setFieldValue("electricStartDate2",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気停止日2"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.electricEndDate2&&new Date(A.values.electricEndDate2),onChange:e=>A.setFieldValue("electricEndDate2",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道開始日2"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.waterStartDate2&&new Date(A.values.waterStartDate2),onChange:e=>A.setFieldValue("waterStartDate2",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道停止日2"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.waterEndDate2&&new Date(A.values.waterEndDate2),onChange:e=>A.setFieldValue("waterEndDate2",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気開始日3"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.electricStartDate3&&new Date(A.values.electricStartDate3),onChange:e=>A.setFieldValue("electricStartDate3",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"電気停止日3"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.electricEndDate3&&new Date(A.values.electricEndDate3),onChange:e=>A.setFieldValue("electricEndDate3",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道開始日3"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.waterStartDate3&&new Date(A.values.waterStartDate3),onChange:e=>A.setFieldValue("waterStartDate3",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-3"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"水道停止日3"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${A.values.closeTypes.includes("close_type1")&&"bg-gray-300"}`,selected:A.values.waterEndDate3&&new Date(A.values.waterEndDate3),onChange:e=>A.setFieldValue("waterEndDate3",lm()(e).format("YYYY-MM-DD"))&&A.handleSubmit()}))),c.createElement("div",{className:"col-span-4"}))))):null};var $m=Object.defineProperty,Pm=Object.defineProperties,Tm=Object.getOwnPropertyDescriptors,Sm=Object.getOwnPropertySymbols,Fm=Object.prototype.hasOwnProperty,Om=Object.prototype.propertyIsEnumerable,Mm=(e,t,a)=>t in e?$m(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,Um=(e,t)=>{for(var a in t||(t={}))Fm.call(t,a)&&Mm(e,a,t[a]);if(Sm)for(var a of Sm(t))Om.call(t,a)&&Mm(e,a,t[a]);return e},Rm=(e,t)=>Pm(e,Tm(t)),Ym=(e,t,a)=>new Promise(((r,n)=>{var o=e=>{try{m(a.next(e))}catch(e){n(e)}},l=e=>{try{m(a.throw(e))}catch(e){n(e)}},m=e=>e.done?r(e.value):Promise.resolve(e.value).then(o,l);m((a=a.apply(e,t)).next())}));(0,mm.registerLocale)("ja",fm.Z);const Vm=e=>{const{id:t}=(0,D.UO)(),[a,{loading:r}]=Ke(),{register:n,handleSubmit:o,getValues:l,setValue:m,formState:{isDirty:s,isValid:i,errors:d}}=(0,Pa.cI)({mode:"onBlur",defaultValues:{beforeConstructionImageDate:e.project.beforeConstructionImageDate,afterConstructionImageDate:e.project.afterConstructionImageDate,checklistFirstDate:e.project.checklistFirstDate,checklistLastDate:e.project.checklistLastDate,isFaq:e.project.isFaq,imageStatus:e.project.imageStatus,submitDocumentDate:e.project.submitDocumentDate}}),u=e=>{a({variables:{id:t,attributes:e}})};return c.createElement(xm,null,c.createElement(Im,null),c.createElement("div",{className:"mt-4"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:o(u)},c.createElement("div",{className:"grid mt-6 gap-y-6 gap-x-2 grid-cols-5"},c.createElement("div",{className:"col-span-1"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"施工前写真資料・写真確認"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),Rm(Um({locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("beforeConstructionImageDate")),{selected:l("beforeConstructionImageDate")&&new Date(l("beforeConstructionImageDate")),onChange:e=>Ym(void 0,null,(function*(){yield m("beforeConstructionImageDate",lm()(e).format("YYYY-MM-DD")),o(u)()}))})))),c.createElement("div",{className:"col-span-1"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"初回チェックリスト"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),Rm(Um({locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("checklistFirstDate")),{selected:l("checklistFirstDate")&&new Date(l("checklistFirstDate")),onChange:e=>Ym(void 0,null,(function*(){yield m("checklistFirstDate",lm()(e).format("YYYY-MM-DD")),o(u)()}))})))),c.createElement("div",{className:"col-span-1"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"最終チェックリスト"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),Rm(Um({locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("checklistLastDate")),{selected:l("checklistLastDate")&&new Date(l("checklistLastDate")),onChange:e=>Ym(void 0,null,(function*(){yield m("checklistLastDate",lm()(e).format("YYYY-MM-DD")),o(u)()}))})))),c.createElement("div",{className:"col-span-1 h-5"},c.createElement("label",{htmlFor:"isFaq",className:"block text-sm font-medium text-gray-700"},"質疑回答"),c.createElement("input",Um({type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"},n("isFaq",{onBlur:o(u)}))))),c.createElement("div",{className:"grid mt-6 gap-y-6 gap-x-2 grid-cols-5"},c.createElement("div",{className:"col-span-1"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"施工後写真資料"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),Rm(Um({locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("afterConstructionImageDate")),{selected:l("afterConstructionImageDate")&&new Date(l("afterConstructionImageDate")),onChange:e=>Ym(void 0,null,(function*(){yield m("afterConstructionImageDate",lm()(e).format("YYYY-MM-DD")),o(u)()}))})))),c.createElement("div",{className:"h-5"},c.createElement("label",{htmlFor:"isLackImage",className:"block text-sm font-medium text-gray-700"},"写真ステータス"),c.createElement("select",Um({className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},n("imageStatus",{onChange:o(u)})),c.createElement("option",{value:""}),c.createElement("option",{value:"lack"},"不足写真あり"),c.createElement("option",{value:"complete"},"写真完了")))),c.createElement("div",{className:"grid mt-6 gap-y-6 gap-x-2 grid-cols-5"},c.createElement("div",{className:"col-span-1"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"請求書類提出日"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),Rm(Um({locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},n("submitDocumentDate")),{selected:l("submitDocumentDate")&&new Date(l("submitDocumentDate")),onChange:e=>Ym(void 0,null,(function*(){yield m("submitDocumentDate",lm()(e).format("YYYY-MM-DD")),o(u)()}))}))))))))};(0,mm.registerLocale)("ja",fm.Z);const qm=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=oa({variables:{id:e},fetchPolicy:"cache-and-network"});return t?c.createElement(Vm,{project:t}):null};(0,mm.registerLocale)("ja",fm.Z);const Lm=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=oa({variables:{id:e}}),[a,{loading:r}]=Ke(),n=(0,Ar.TA)({enableReinitialize:!0,initialValues:{isVermiculite:null==t?void 0:t.isVermiculite,vermiculiteDate:(null==t?void 0:t.vermiculiteDate)||"",isManagerAsbestosDocument1:null==t?void 0:t.isManagerAsbestosDocument1,isManagerAsbestosDocument2:null==t?void 0:t.isManagerAsbestosDocument2,isManagerAsbestosDocument3:null==t?void 0:t.isManagerAsbestosDocument3,isOfficeworkAsbestosDocument1:null==t?void 0:t.isOfficeworkAsbestosDocument1,isOfficeworkAsbestosDocument2:null==t?void 0:t.isOfficeworkAsbestosDocument2,isOfficeworkAsbestosDocument3:null==t?void 0:t.isOfficeworkAsbestosDocument3,processCreatedDate:(null==t?void 0:t.processCreatedDate)||""},onSubmit:t=>{a({variables:{id:e,attributes:t}})}}),o=[{label:"石綿事前調査報告書（公社提出用）",value:"isManagerAsbestosDocument1",check:n.values.isManagerAsbestosDocument1},{label:"石綿事前調査報告書（電子申請用）",value:"isManagerAsbestosDocument2",check:n.values.isManagerAsbestosDocument2},{label:"墨出し・ガス漏報告書",value:"isManagerAsbestosDocument3",check:n.values.isManagerAsbestosDocument3}],l=[{label:"石綿事前調査報告書（公社提出用）",value:"isOfficeworkAsbestosDocument1",check:n.values.isOfficeworkAsbestosDocument1},{label:"石綿事前調査報告書（電子申請用）",value:"isOfficeworkAsbestosDocument2",check:n.values.isOfficeworkAsbestosDocument2},{label:"墨出し・ガス漏報告書",value:"isOfficeworkAsbestosDocument3",check:n.values.isOfficeworkAsbestosDocument3}];return t?c.createElement(xm,null,c.createElement(Im,null),c.createElement("div",{className:"mt-4"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:n.handleSubmit},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-2 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"col-span-6"},c.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"蛭石調査")),c.createElement("div",{className:"col-span-1"},c.createElement("div",{className:"flex space-x-6"},c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"isVermiculiteTrue",name:"isVermiculite",type:"radio",checked:n.values.isVermiculite,className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",onChange:()=>n.setFieldValue("isVermiculite",!0)&&n.handleSubmit()}),c.createElement("label",{htmlFor:"isVermiculiteTrue",className:"block ml-3 text-sm font-medium text-gray-700 cursor-pointer"},"有")),c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"isVermiculiteFalse",name:"isVermiculite",type:"radio",checked:!n.values.isVermiculite,className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",onChange:()=>n.setFieldValue("isVermiculite",!1)&&n.handleSubmit()}),c.createElement("label",{htmlFor:"isVermiculiteFalse",className:"block ml-3 text-sm font-medium text-gray-700 cursor-pointer"},"無")))),c.createElement("div",{className:"col-span-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"蛭石調査完了日"),c.createElement("div",{className:"rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:n.values.vermiculiteDate&&new Date(n.values.vermiculiteDate),onChange:e=>n.setFieldValue("vermiculiteDate",lm()(e).format("YYYY-MM-DD"))&&n.handleSubmit()}))),c.createElement("div",{className:"col-span-6 mt-10"},c.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"石綿調査報告管理")),c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"監督"),c.createElement("div",{className:"flex col-span-6 space-x-8"},o.map((e=>c.createElement("div",{className:"flex items-center h-5"},c.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,checked:e.check,onChange:t=>{n.setFieldValue(e.value,t.target.checked)&&n.handleSubmit()},onBlur:n.handleBlur}),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.label)))))),c.createElement("label",{className:"block mt-6 text-sm font-medium text-gray-700"},"事務"),c.createElement("div",{className:"flex col-span-6 space-x-8"},l.map((e=>c.createElement("div",{className:"flex items-center h-5"},c.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,checked:e.check,onChange:t=>{n.setFieldValue(e.value,t.target.checked)&&n.handleSubmit()},onBlur:n.handleBlur}),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.label)))))),c.createElement("div",{className:"col-span-2 mt-10"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"工程表作成日"),c.createElement("div",{className:"flex mt-1 rounded-md shadow-sm"},c.createElement(sm(),{locale:"ja",dateFormat:"yyyy/MM/dd",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",selected:n.values.processCreatedDate&&new Date(n.values.processCreatedDate),onChange:e=>n.setFieldValue("processCreatedDate",lm()(e).format("YYYY-MM-DD"))&&n.handleSubmit()}))))))):null},_m=()=>c.createElement(D.Z5,null,c.createElement(D.AW,{path:"base",element:c.createElement(jm,null)}),c.createElement(D.AW,{path:"docs",element:c.createElement(qm,null)}),c.createElement(D.AW,{path:"management",element:c.createElement(Lm,null)})),Zm=({imageComment:e})=>{const[t]=function(e){const t=U(U({},R),void 0);return $.D(dt,t)}(),a=(0,Ar.TA)({initialValues:{text:e.text},onSubmit:a=>{var r;r=a,t({variables:{id:e.id,attributes:r}})}});return c.createElement("form",{onSubmit:a.handleSubmit},c.createElement("input",{type:"text",name:"text",value:a.values.text,onChange:a.handleChange,onBlur:()=>a.handleSubmit(),className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"}))},zm=({projectImage:e,item:t})=>{const[a,{loading:r}]=function(e){const t=U(U({},R),void 0);return $.D(nt,t)}();return c.createElement("tr",{className:"bg-white",key:null==t?void 0:t.id},c.createElement("td",{className:"w-6 m-auto"},!r&&c.createElement(Ja,{className:"text-blue-700 cursor-pointer",onClick:()=>{a({variables:{id:e.id,itemId:t.id}})}}),r&&c.createElement(ja,{className:"w-3 h-3"})),c.createElement("td",{className:"w-20 px-4 text-xs"},t.itemCode.code),c.createElement("td",{className:"flex-1 px-4 text-xs"},t.itemCode.name))},Wm=({project:e,projectImage:t})=>{var a,r;const{data:{roomType:n=null}={}}=ba({variables:{id:t.roomTypeId}}),{data:{allItems:o=[]}={}}=Jt({variables:{search:{year:String(e.year.year)}}}),[l]=function(e){const t=U(U({},R),void 0);return $.D(st,t)}();return c.createElement("div",null,c.createElement("div",{style:{height:"240px"}},c.createElement("table",{className:"w-full divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",{className:"flex text-center"},c.createElement("th",{className:"w-4"}),c.createElement("th",{className:"w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コード"),c.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"名称"))),c.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200",style:{height:"200px"}},null==(a=(()=>{const a=e.projectItems.filter((e=>{var a;return null==(a=e.roomTypeIds)?void 0:a.includes(t.roomTypeId)})).map((e=>e.code)),r=o.filter((e=>a.includes(e.itemCode.code)));return console.log("🚀 ~ file: Comment.tsx:56 ~  filtertems",r),r})())?void 0:a.map(((e,a)=>c.createElement(zm,{projectImage:t,item:e,key:`item-${a}-${e.id}`})))))),c.createElement("table",{className:"w-full divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",{className:"flex text-center"},c.createElement("th",{className:"w-4"}),c.createElement("th",{className:"w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コード"),c.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"名称"))),c.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200"},null==(r=null==t?void 0:t.imageComments)?void 0:r.map((e=>c.createElement("tr",{className:"flex bg-white",key:null==e?void 0:e.id},c.createElement("td",{className:"w-4 m-auto"},c.createElement(Wa,{className:"text-blue-700 cursor-pointer",onClick:()=>(e=>{l({variables:{id:e.id}})})(e)})),c.createElement("td",{className:"w-20 px-4 my-auto text-xs"},e.code),c.createElement("td",{className:"flex-1 px-4 text-xs"},c.createElement(Zm,{imageComment:e}))))))))},Hm={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"600px"},overlay:{zIndex:"10"}},Gm=({project:e,projectImage:t,onClose:a})=>{var r;const[n]=At(),[o,l]=(0,c.useState)(!1),m=(0,Ar.TA)({enableReinitialize:!0,initialValues:{comment:null==t?void 0:t.comment},onSubmit:e=>{t&&n({variables:{id:t.id,attributes:e}}),a()}});return c.createElement("form",{onSubmit:m.handleSubmit,className:"h-44"},c.createElement("button",{type:"button",onClick:()=>l(!0),className:"block px-2 py-1 text-xs text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm ont-medium hover:bg-gray-50"},"編集"),c.createElement(A(),{isOpen:o,onRequestClose:()=>{l(!1)},style:Hm},c.createElement(Wm,{projectImage:t,project:e})),c.createElement("table",{className:"w-full border border-gray-200 divide-y divide-gray-200"},c.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-400",style:{height:"180px"}},null==(r=null==t?void 0:t.imageComments)?void 0:r.map((e=>c.createElement("tr",{className:"flex bg-white",key:null==e?void 0:e.id},c.createElement("td",{className:"flex-1 px-2 py-1 text-xs"},e.text)))))))},Jm={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"800px",height:"680px"},overlay:{zIndex:"20"}},Km=e=>c.createElement(A(),{isOpen:e.isOpen,onRequestClose:e.onClose,style:Jm},c.createElement("div",null,c.createElement("img",{className:"max-h-[560px]",src:e.image}),c.createElement("div",{className:"flex justify-end items-center mt-10"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm",onClick:()=>e.onClose()},"閉じる")))),Qm=({project:e,id:t,projectImage:a,roomTypeId:r,imageType:n,index:o,moveImage:l,order:m})=>{const[s,i]=(0,c.useState)(null),d=(0,c.useRef)(null),u=(0,c.useRef)(null),[A,p]=(0,c.useState)(!1),[g]=At(),[b]=ct(),y=(e,t)=>{let r=null;r="before"===t?{imageBefore:e}:{imageAfter:e},g({variables:{id:a.id,attributes:r}})},x=(0,c.useRef)(null),[{handlerId:f},E]=(0,rl.L)({accept:"Image",collect:e=>({handlerId:e.getHandlerId()}),hover(e,t){var a;if(!x.current)return;const r=e.index,n=o;if(r===n)return;const m=null==(a=x.current)?void 0:a.getBoundingClientRect(),s=(m.bottom-m.top)/2,i=t.getClientOffset().y-m.top;r<n&&i<s||r>n&&i>s||(l(e.id,r,n),e.index=n)}}),[{isDragging:h},v]=(0,nl.c)({type:"Image",item:()=>({id:t,index:o}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: Image.tsx ~ line 147 ~ Image ~ item",e),m(e.id,e.index)}}),w=h?0:1;return v(E(x)),c.createElement("div",{className:"flex mt-4 h-72",ref:x,style:{opacity:w},"data-handler-id":f},c.createElement("div",{className:"w-8 my-auto"},c.createElement(_a,{className:"cursor-pointer w-8"})),c.createElement("div",{className:"flex p-4 border border-gray-300 w-96"},c.createElement("div",{className:"w-64 h-64"},(null==a?void 0:a.imageBefore)&&c.createElement("img",{className:"h-full w-full object-contain cursor-pointer",src:null==a?void 0:a.imageBefore,onClick:()=>i(a.imageBefore)})),c.createElement("div",{className:"mt-2 ml-4"},c.createElement("button",{type:"button",className:"block px-4 py-2 ml-auto text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>d.current.click()},"登録"),c.createElement("button",{type:"button",className:"block px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{y(null,"before")}},"削除"),c.createElement("input",{id:"fileBefore",name:"fileBefore",type:"file",accept:".jpg,.gif,.png,image/gif,image/jpeg,image/png",ref:d,style:{display:"none"},onChange:e=>{(e=>{const t=e.target.files[0];t&&y(t,"before")})(e)}}))),c.createElement("div",{className:"flex p-4 border border-gray-300 w-96"},c.createElement("div",{className:"w-64 h-64"},(null==a?void 0:a.imageAfter)&&c.createElement("img",{className:"h-full w-full object-contain cursor-pointer",src:null==a?void 0:a.imageAfter,onClick:()=>i(a.imageAfter)})),c.createElement("div",{className:"mt-2 ml-4"},c.createElement("button",{type:"button",className:"block px-4 py-2 ml-auto text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>u.current.click()},"登録"),c.createElement("button",{type:"button",className:"block px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{y(null,"after")}},"削除"),c.createElement("input",{id:"fileAfter",name:"fileAfter",type:"file",accept:".jpg,.gif,.png,image/gif,image/jpeg,image/png",ref:u,style:{display:"none"},onChange:e=>{e.preventDefault(),(e=>{const t=e.target.files[0];t&&y(t,"after")})(e)}}))),c.createElement("div",{className:"p-4 align-top border border-gray-300 w-72",onClick:()=>p(!0)},!A&&c.createElement("div",{className:"whitespace-pre"},null==a?void 0:a.comment),c.createElement(Gm,{project:e,projectImage:a,onClose:()=>p(!1)})),c.createElement(Xa,{className:"w-4 ml-2 text-red-500 cursor-pointer",onClick:()=>{b({variables:{id:t}})}}),c.createElement(Km,{isOpen:!!s,image:s,onClose:()=>i(null)}))},Xm=({project:e,projectImages:t,roomTypeId:a})=>{const[r,n]=(0,c.useState)([]),[o]=function(e){const t=U(U({},R),void 0);return $.D(pt,t)}(),l=(e,t,a)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${t}`),console.log(`update: hoverIndex: ${a}`),n((e=>el()(e,{$splice:[[t,1],[a,0,e[t]]]})))},m=(e,t)=>{o({variables:{id:e,attributes:{position:t}}})};return(0,c.useEffect)((()=>{n(t)}),[t]),c.createElement("div",null,c.createElement(tl.W,{backend:al.PD},r.map(((t,r)=>c.createElement(Qm,{index:r,id:t.id,key:t.id,project:e,projectImage:t,roomTypeId:a,imageType:t.imageType,moveImage:l,order:m})))))},es=({project:e,roomTypeId:t})=>{const[a,r]=(0,c.useState)(1),[n,{loading:o}]=lt(),l=a=>{const r=e.projectImages.filter((e=>e.imageType===a&&e.roomTypeId===t));return km().sortBy(r,["position"])},m=[1,2,3,4,5,10,20,30];return c.createElement("div",null,[{name:"壁1",value:"wall1"},{name:"壁2",value:"wall2"},{name:"壁3",value:"wall3"},{name:"壁4",value:"wall4"},{name:"床",value:"floor"},{name:"天井",value:"roof"}].map((s=>c.createElement("div",{className:"mt-10 w-[1100px]",key:s.value},c.createElement("h3",{className:"text-lg font-bold"},s.name),c.createElement(Xm,{project:e,roomTypeId:t,projectImages:l(s.value)}),c.createElement("div",{className:"inline-flex mt-4"},c.createElement("select",{name:"location",className:"block py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",defaultValue:a,onChange:e=>r(Number(e.target.value))},m.map((e=>c.createElement("option",{key:e},e)))),c.createElement(Da,{type:"button",className:"block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>(r=>{n({variables:{count:a,attributes:{projectId:e.id,roomTypeId:t,imageType:r}}})})(s.value),loading:o},"追加"))))))},ts=()=>{var e,t;const{id:a,roomTypeId:r}=(0,D.UO)(),{data:{project:n=null}={}}=oa({variables:{id:a},fetchPolicy:"cache-and-network"}),[o]=function(e){const t=U(U({},R),e);return $.D(Le,t)}({onCompleted:({createProjectImageReport:{project:e}})=>window.location.href=e.imageReportUrl});return n?(console.log("🚀 ~ file: ImageBase.tsx ~ line 16 ~ Image ~ project",n),c.createElement(xm,null,n.imageReportUrl&&c.createElement("a",{className:"block mt-6",onClick:()=>{o({variables:{id:a}})}},c.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},c.createElement(Va,{className:"flex-shrink-0 w-5 h-5"}),c.createElement("span",null,"Excelレポート"))),c.createElement("nav",{className:"flex space-x-4","aria-label":"Tabs"},c.createElement(p.rU,{to:`/projects/${a}/images`},c.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-800 bg-gray-200 rounded-md hover:text-gray-800 "},"塗装前")),c.createElement(p.rU,{to:`/projects/${a}/images/other`},c.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-800"},"塗装以降"))),c.createElement("div",{className:"mt-6"},c.createElement("nav",{className:"flex space-x-1"},null==(t=null==(e=n.room)?void 0:e.roomTypes)?void 0:t.map((e=>c.createElement(p.rU,{key:e.id,to:`/projects/${a}/images/${e.id}`,className:"text-gray-500 hover:text-gray-700 w-24 border border-gray-300  bg-white py-2 text-sm font-medium text-center hover:bg-gray-200 rounded-sm "+(e.id===r?"bg-gray-200 border border-gray-900":"")},c.createElement("span",null,e.name),c.createElement("span",{"aria-hidden":"true",className:"bg-transparent absolute inset-x-0 bottom-0 h-0.5"})))))),r&&c.createElement(es,{project:n,roomTypeId:r}))):null},as=({projectImage:e,comment:t})=>{const[a,{loading:r}]=function(e){const t=U(U({},R),void 0);return $.D(rt,t)}(),[n]=function(e){const t=U(U({},R),void 0);return $.D(mt,t)}(),o=t=>e.comments.findIndex((e=>e.id===t.id))>-1;return c.createElement("tr",{className:`bg-white ${o(t)&&"bg-gray-200"}`,key:null==t?void 0:t.id},c.createElement("td",{className:"w-6 m-auto"},!r&&!o(t)&&c.createElement(Ja,{className:"text-blue-700 cursor-pointer",onClick:()=>{return r=t.id,void a({variables:{id:e.id,commentId:r}});var r}}),!r&&o(t)&&c.createElement(Wa,{className:"text-xs text-blue-700 cursor-pointer",onClick:()=>{return a=t.id,void n({variables:{id:e.id,commentId:a}});var a}}),r&&c.createElement(ja,{className:"w-3 h-2"})),c.createElement("td",{className:"flex-1 px-4 text-xs"},t.comment))},rs=e=>{var t;const[a,r]=(0,c.useState)(null),{data:{comments:n=[]}={}}=Ut({variables:{search:{commentTypeId:a}},skip:!a}),{data:{commentTypes:o=[]}={}}=qt(),l=e.project.projectItems.map((e=>e.code));return c.createElement("div",{style:{height:"240px"}},c.createElement("div",{className:"flex flex-wrap space-x-2 space-y-1"},o.map((e=>c.createElement("div",{key:e.id,className:`inline-flex items-center rounded-md bg-gray-100 px-2.5 py-0.5 text-sm font-medium cursor-pointer hover:bg-blue-100 ${a===e.id&&"bg-blue-300"}`,onClick:()=>r(e.id)},e.name)))),c.createElement("table",{className:"w-full divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",{className:"flex text-center"},c.createElement("th",{className:"w-4"}),c.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コメント"))),c.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200"},null==(t=n.filter((e=>e.itemCodes.findIndex((e=>l.includes(e.code)))>-1)))?void 0:t.map((t=>c.createElement(as,{projectImage:e.projectImage,comment:t,key:`comment-${t.id}`}))))))},ns={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"600px"},overlay:{zIndex:"10"}},os=({project:e,projectImage:t,onClose:a})=>{var r;const[n]=At(),[o,l]=(0,c.useState)(!1),m=(0,Ar.TA)({enableReinitialize:!0,initialValues:{comment:null==t?void 0:t.comment},onSubmit:e=>{t&&n({variables:{id:t.id,attributes:e}}),a()}});return c.createElement("form",{onSubmit:m.handleSubmit},c.createElement("button",{type:"button",onClick:()=>l(!0),className:"block h-8 px-2 py-1 text-xs text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm ont-medium hover:bg-gray-50"},"編集"),c.createElement(A(),{isOpen:o,onRequestClose:()=>{l(!1)},style:ns},c.createElement(rs,{project:e,projectImage:t})),c.createElement("table",{className:"w-full border border-gray-200 divide-y divide-gray-200"},c.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-400",style:{height:"180px"}},null==(r=null==t?void 0:t.comments)?void 0:r.map((e=>c.createElement("tr",{className:"flex bg-white",key:null==e?void 0:e.id},c.createElement("td",{className:"flex-1 px-1 py-1 text-xs"},e.comment)))))))},ls=({project:e,projectImage:t})=>{const[a,r]=(0,c.useState)(null),n=(0,c.useRef)(null),[o]=At(),[l]=ct();return c.createElement("div",{className:"mt-4"},c.createElement("div",{className:"text-gray-500 text-sm"},"#",t.position),c.createElement("div",{className:"flex h-72"},c.createElement("div",{className:"flex p-4 border border-gray-300 w-96"},c.createElement("div",{className:"w-64 h-64"},(null==t?void 0:t.image)&&c.createElement("img",{className:"h-full w-full object-contain cursor-pointer",src:null==t?void 0:t.image,onClick:()=>r(t.image)})),c.createElement("div",{className:"mt-2 ml-2"},c.createElement("button",{type:"button",className:"block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>n.current.click()},"登録"),c.createElement("button",{type:"button",className:"block px-2 py-2 text-xs font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{l({variables:{id:t.id}})}},"削除"),c.createElement("input",{id:"file",name:"file",type:"file",accept:".jpg,.gif,.png,image/gif,image/jpeg,image/png",ref:n,style:{display:"none"},onChange:e=>{(e=>{const a=e.target.files[0];a&&(e=>{let a=null;a={image:e},o({variables:{id:t.id,attributes:a}})})(a)})(e)}}))),c.createElement("div",{className:"px-2 align-top border border-gray-300 w-44"},c.createElement(os,{project:e,projectImage:t,onClose:()=>{}}))),c.createElement(Km,{isOpen:!!a,image:a,onClose:()=>r(null)}))},ms=({project:e,projectImages:t})=>{const[a,r]=(0,c.useState)([]);return(0,c.useEffect)((()=>{r(t)}),[t]),c.createElement("div",{className:"flex flex-wrap w-full gap-2"},a.map((t=>c.createElement(ls,{key:t.id,project:e,projectImage:t}))))},ss=({project:e})=>{const[t,a]=(0,c.useState)(1),[r,{loading:n}]=lt();return c.createElement("div",null,c.createElement("div",{className:"mt-10"},c.createElement(ms,{project:e,projectImages:(()=>{const t=e.projectImages.filter((e=>"other"===e.imageType));return km().sortBy(t,["position"])})()}),c.createElement("div",{className:"inline-flex mt-4"},c.createElement("select",{name:"location",className:"block py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",defaultValue:t,onChange:e=>a(Number(e.target.value))},c.createElement("option",null,"1"),c.createElement("option",null,"5"),c.createElement("option",null,"10"),c.createElement("option",null,"20"),c.createElement("option",null,"30")),c.createElement(Da,{type:"button",className:"block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{r({variables:{count:t,attributes:{projectId:e.id,imageType:"other"}}})},loading:n},"追加"))))},is=()=>{const{id:e,roomTypeId:t}=(0,D.UO)(),{data:{project:a=null}={}}=oa({variables:{id:e},fetchPolicy:"cache-and-network"});return a?c.createElement(xm,null,a.imageReportUrl&&c.createElement("a",{href:a.imageReportUrl,className:"block mt-6"},c.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},c.createElement(Va,{className:"flex-shrink-0 w-5 h-5"}),c.createElement("span",null,"Excelレポート"))),c.createElement("nav",{className:"flex space-x-4","aria-label":"Tabs"},c.createElement(p.rU,{to:`/projects/${e}/images`},c.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-800"},"塗装前")),c.createElement(p.rU,{to:`/projects/${e}/images/other`},c.createElement("div",{className:"px-3 py-2 text-sm font-medium text-gray-800 bg-gray-200 rounded-md hover:text-gray-800 "},"塗装以降"))),c.createElement(ss,{project:a})):null},cs=()=>c.createElement(D.Z5,null,c.createElement(D.AW,{path:"other",element:c.createElement(is,null)}),c.createElement(D.AW,{path:":roomTypeId",element:c.createElement(ts,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(ts,null)})),ds=({companyType:e,setCompany:t})=>{const[a,r]=(0,c.useState)(null);return c.createElement("tr",null,c.createElement("td",{className:"whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6"},e.name),c.createElement("td",{className:"whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6"},c.createElement("select",{value:a,onChange:a=>{var n;r(a.target.value),n=a.target.value,t(e.id,n)},className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},c.createElement("option",{value:""}),e.companies.map((e=>c.createElement("option",{value:e.id},e.name))))))},us={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"800px"},overlay:{zIndex:"10"}},As=({isOpen:e,project:t,handleClose:a})=>{const[r,n]=(0,c.useState)([]),{data:{companyTypes:o=[]}={}}=Ht(),[l,{loading:m}]=function(e){const t=U(U({},R),void 0);return $.D(Xe,t)}(),s=(e,t)=>{const a=r.filter((t=>t.companyTypeId!==e));(t||0!==t.length)&&a.push({companyTypeId:e,companyId:t}),n(a)};return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:e,onRequestClose:a,style:us},c.createElement("div",{className:""},c.createElement("table",{className:"min-w-full divide-y divide-gray-300"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6"},"業者種別"),c.createElement("th",{scope:"col",className:"px-3 py-3.5 text-left text-sm font-semibold text-gray-900"},"業者"))),c.createElement("tbody",{className:"divide-y divide-gray-200 bg-white"},o.map((e=>c.createElement(ds,{companyType:e,key:e.id,setCompany:s}))))),c.createElement("div",{className:"mt-10 text-right"},c.createElement(Da,{type:"button",className:"px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>{for(const e of r)l({variables:{id:t.id,companyId:e.companyId,companyTypeId:e.companyTypeId}});a()},disabled:0===r.length,loading:m},"更新")))))},ps=({setKeyword:e})=>{const t=(0,Ar.TA)({initialValues:{keyword:""},onSubmit:t=>{e(t.keyword)}});return c.createElement("form",{onSubmit:t.handleSubmit},c.createElement("div",{className:"relative mt-1 rounded-md shadow-sm"},c.createElement("div",{className:"absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none"},c.createElement("svg",{xmlns:"http://www.w3.org/2000/svg",className:"w-4 h-4 text-gray-400",fill:"none",viewBox:"0 0 24 24",stroke:"currentColor","stroke-width":"2"},c.createElement("path",{"stroke-linecap":"round","stroke-linejoin":"round",d:"M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"}))),c.createElement("input",{type:"text",name:"keyword",className:"block w-full px-4 py-1 pl-10 mt-1 border border-gray-400 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:t.values.keyword,onChange:t.handleChange,onBlur:()=>t.handleSubmit()})))},gs={content:{top:"50%",left:"50%",right:"auto",bottom:"auto",marginRight:"-50%",transform:"translate(-50%, -50%)",width:"600px",height:"800px"},overlay:{zIndex:"10"}},bs=({projectItem:e,project:t,isOpen:a,handleClose:r,refetch:n})=>{const[o,l]=(0,c.useState)(null),{data:{allItems:m=[]}={},loading:s}=Jt({variables:{search:{year:`${t.year.year}`,keyword:o}},skip:!o}),[i,{loading:d}]=function(e){const t=U(U({},R),e);return $.D(_e,t)}({onCompleted:()=>n()}),u=(0,Ar.TA)({initialValues:{itemId:(null==e?void 0:e.itemId)||"",code:(null==e?void 0:e.code)||"",name:(null==e?void 0:e.name)||"",unitPrice:null==e?void 0:e.unitPrice,count:null==e?void 0:e.count},onSubmit:e=>{i({variables:{id:t.id,attributes:e}}),u.resetForm(),r()}}),p=e=>{u.setFieldValue("itemId",e.id),u.setFieldValue("code",`${e.itemCode.code}`),u.setFieldValue("name",e.name),u.setFieldValue("unitPrice",e.price)};return c.createElement("div",{className:"w-screen max-w-xl"},c.createElement(A(),{isOpen:a,onRequestClose:r,style:gs},c.createElement("div",{className:""},c.createElement(ps,{setKeyword:l}),c.createElement("table",{className:"w-full mt-4 border border-gray-300 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",{className:"flex text-center"},c.createElement("th",{className:"w-4"}),c.createElement("th",{className:"w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"コード"),c.createElement("th",{className:"flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500"},"名称"),c.createElement("th",null))),c.createElement("tbody",{className:"flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200",style:{height:"300px"}},s&&c.createElement("div",{className:"mx-auto"},c.createElement(ja,null)),!s&&m.map((e=>c.createElement("tr",{className:`flex w-full text-left bg-white px-2 py-2 cursor-pointer hover:bg-gray-100 ${u.values.itemId===e.id&&"bg-indigo-100"}`,key:null==e?void 0:e.id,onClick:()=>p(e)},c.createElement("td",{className:"w-4 m-auto"},c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"itemId",name:"itemId",type:"radio",value:e.id,checked:u.values.itemId===e.id,className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",onChange:t=>{p(e)}}))),c.createElement("td",{className:"w-28 px-4"},e.itemCode.code),c.createElement("td",{className:"flex-1 px-4 my-auto ml-2 text-sm"},e.name))))))),c.createElement("form",{onSubmit:u.handleSubmit},c.createElement("div",{className:"mt-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"コード"),c.createElement("input",{type:"text",name:"code",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.code,onChange:u.handleChange,onBlur:u.handleBlur,disabled:!0})),c.createElement("div",{className:"mt-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.name,onChange:u.handleChange,onBlur:u.handleBlur})),c.createElement("div",{className:"mt-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"単価"),c.createElement("input",{type:"number",name:"unitPrice",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.unitPrice,onChange:u.handleChange,onBlur:u.handleBlur})),c.createElement("div",{className:"mt-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"数量"),c.createElement("input",{type:"number",name:"count",step:"0.1",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:u.values.count,onChange:u.handleChange,onBlur:u.handleBlur})),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:d||!u.isValid||!u.dirty,loading:d},"登録")))))},ys=({project:e,projectItem:t,onCopy:a,refetch:r})=>{var n,o,l,m,s,i,d,u,A;const{data:{years:p}={}}=Ia(),[g]=function(e){const t=U(U({},R),void 0);return $.D(Qe,t)}(),[b]=function(e){const t=U(U({},R),e);return $.D(Ge,t)}({onCompleted:()=>r()});null==(o=null==(n=e.room)?void 0:n.roomTypes)||o.map((e=>({value:e.id,label:e.name})));let y=[];y=t.selectableCompanies.map((e=>({value:e.id,label:e.name}))),t.company&&t.selectableCompanies.includes(t.company)&&y.push({value:t.company.id,label:t.company.name});const x=(0,Ar.TA)({enableReinitialize:!0,initialValues:{name:t.name,count:t.count||0,unitPrice:t.unitPrice||0,amount:t.amount||0,roomTypeIds:t.roomTypeIds||[],companyId:t.companyId},onSubmit:e=>{g({variables:{id:t.id,attributes:e}})}}),f=()=>{let e=!1;return["OM","SM"].forEach((a=>{t.code.indexOf(a)>-1&&(e=!0)})),e};return p?c.createElement("form",{onSubmit:x.handleSubmit},c.createElement("div",{className:"flex"},c.createElement("div",{className:"w-10 text-gray-600 text-sm flex items-center px-4"},"#",t.sequenceNumber),c.createElement("div",{className:"px-1 m-auto w-20"},c.createElement("button",{type:"button",className:"block w-full px-2 py-1 text-xs font-medium text-white bg-indigo-600 border border-transparent rounded shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 whitespace-nowrap",onClick:()=>{a(t)}},"コピー"),c.createElement("button",{type:"button",className:"block w-full px-2 py-1 mx-auto mt-1 text-xs font-medium border rounded shadow-sm border-rose-600 text-rose-600 focus:outline-none focus:ring-2 focus:ring-offset-2 hover:bg-rose-50",onClick:()=>{b({variables:{id:t.id}})}},"削除")),c.createElement("div",{className:"w-64 px-2 py-4"},c.createElement("div",{className:"text-xs font-medium text-gray-900"},t.code),c.createElement("div",{className:"text-xs text-gray-500"},c.createElement("input",{type:"text",name:"name",className:"block w-full px-2 py-1 mt-1 border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs",value:x.values.name,disabled:!f(),onChange:x.handleChange,onBlur:()=>x.handleSubmit()}))),c.createElement("div",{className:"w-36 px-2 py-4 flex"},c.createElement("div",null,c.createElement("input",{name:"count",type:"number",step:"0.1",value:x.values.count,className:"w-16 px-1 py-1 mt-1 text-xs text-right border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500",onChange:x.handleChange,onBlur:()=>x.handleSubmit()})),c.createElement("div",null,c.createElement("input",{name:"unitPrice",type:"number",value:x.values.unitPrice,className:"w-16 px-1 py-1 mt-1 text-xs text-right border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500",onChange:x.handleChange,disabled:!f(),onBlur:()=>x.handleSubmit()}))),c.createElement("div",{className:"w-16 px-2 py-4"},c.createElement("div",{className:"text-xs leading-8 text-right text-gray-500"},null==(l=t.amount)?void 0:l.toLocaleString(),"円")),c.createElement("div",{className:"w-32 px-4 flex flex-col mt-4"},c.createElement("div",{className:"text-xs"},null==(m=t.item)?void 0:m.companyTypes.map((e=>e.name)).join("/")),c.createElement("div",{className:"flex items-center"},c.createElement("div",{className:"text-xs  border border-gray-400 px-2 py-1 rounded-sm "+(t.item.order?"bg-gray-600 text-white":"text-gray-500")},(null==(s=t.item)?void 0:s.order)?"発注対象":"発注なし"),x.values.companyId&&c.createElement(Fa,{className:"w-6 text-green-700"}))),c.createElement("div",{className:"flex-1 px-2 py-4 text-xs leading-8 text-gray-500"},c.createElement("select",{value:x.values.companyId,onChange:e=>{return t=e.target.value,x.setFieldValue("companyId",t),void x.submitForm();var t},className:"block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"},c.createElement("option",{value:""}),y.map((e=>c.createElement("option",{value:e.value},e.label)))),c.createElement("div",{className:"flex items-center mt-2 flex-wrap gap-2"},null==(d=null==(i=e.room)?void 0:i.roomTypes)?void 0:d.map((e=>c.createElement("div",{className:"flex items-center h-5",key:`${t.id}-roomType-${e.id}`},c.createElement("input",{id:`${t.id}-roomType-${e.id}`,type:"checkbox",className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",checked:x.values.roomTypeIds.includes(e.id),onChange:t=>{var a;(null==(a=x.values.roomTypeIds)?void 0:a.includes(e.id))?x.setFieldValue("roomTypeIds",x.values.roomTypeIds.filter((t=>t!==e.id))):x.setFieldValue("roomTypeIds",[...x.values.roomTypeIds,e.id]),x.handleSubmit()},onBlur:x.handleBlur}),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.name)))))),c.createElement("div",{className:"flex text-sm text-gray-500 mt-4 space-x-4"},c.createElement("div",null,"最終更新日時：",t.updatedItemsAt&&lm()(t.updatedItemsAt).format("YYYY/MM/DD HH:mm")),c.createElement("div",null,"最終更新ユーザー：",null==(u=t.updatedItemsUser)?void 0:u.lastName,null==(A=t.updatedItemsUser)?void 0:A.firstName))))):null},xs=({project:e})=>{const{data:{years:t}={}}=Ia(),[a]=function(e){const t=U(U({},R),void 0);return $.D(at,t)}(),r=(0,Ar.TA)({initialValues:{yearId:e.yearId},onSubmit:t=>{a({variables:{id:e.id,yearId:t.yearId}})}});return t?c.createElement("form",{onSubmit:r.handleSubmit},c.createElement("div",{className:"w-20 py-1 pl-1"},c.createElement("select",{value:r.values.yearId,onChange:t=>{return n=t.target.value,r.setFieldValue("yearId",n),void a({variables:{id:e.id,yearId:n}});var n},className:"block w-full py-2 pl-1 mt-1 text-xs border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500"},t.map((e=>c.createElement("option",{value:e.id},e.year)))))):null};var fs=Object.defineProperty,Es=Object.defineProperties,hs=Object.getOwnPropertyDescriptors,vs=Object.getOwnPropertySymbols,ws=Object.prototype.hasOwnProperty,Ns=Object.prototype.propertyIsEnumerable,Bs=(e,t,a)=>t in e?fs(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const ks=()=>{var e,t,a,r,n;const[o,l]=(0,p.lr)(),m=o.get("orderItem"),s=o.get("order"),[i,d]=(0,c.useState)(!1),[u,A]=(0,c.useState)(!1),[g,b]=(0,c.useState)(null),y=e=>{return void 0,null,t=function*(){yield b(e),d(!0)},new Promise(((e,a)=>{var r=e=>{try{o(t.next(e))}catch(e){a(e)}},n=e=>{try{o(t.throw(e))}catch(e){a(e)}},o=t=>t.done?e(t.value):Promise.resolve(t.value).then(r,n);o((t=t.apply(undefined,null)).next())}));var t},{id:x}=(0,D.UO)(),{data:{project:f=null}={}}=oa({variables:{id:x}}),{data:{projectItems:E=[]}={},refetch:h}=function(e){const t=U(U({},R),e);return P.a(la,t)}({variables:{id:x,orderItem:m,order:s},fetchPolicy:"cache-and-network",skip:!m||!s}),[v]=function(e){const t=U(U({},R),e);return $.D(We,t)}({onCompleted:()=>h()}),w=(0,c.useMemo)((()=>Array.from(new Set(o.entries()))),[o]),N=(e,t)=>{let a={};var r;w.forEach((e=>a[e[0]]=e[1])),l((r=((e,t)=>{for(var a in t||(t={}))ws.call(t,a)&&Bs(e,a,t[a]);if(vs)for(var a of vs(t))Ns.call(t,a)&&Bs(e,a,t[a]);return e})({},a),Es(r,hs({[e]:t}))))};return(0,c.useEffect)((()=>{null!==m&&void 0!==s||l({orderItem:"id",order:"asc"})}),[]),f?c.createElement(xm,null,c.createElement("div",{className:"flex"},c.createElement(xs,{project:f}),c.createElement("div",{className:"mt-2 ml-4"},c.createElement("input",{id:"file-upload",name:"file-upload",type:"file",accept:".xlsx",onChange:e=>{e.preventDefault(),(e=>{e.preventDefault();const t=e.target.files[0];if(!t)return;const a=new FileReader;a.onload=e=>{v({variables:{id:f.id,file:t}}),e.preventDefault()},a.readAsDataURL(t)})(e)}}),f.assessmentUrl&&c.createElement("a",{href:f.assessmentUrl},c.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},c.createElement(Va,{className:"flex-shrink-0 w-5 h-5"}),c.createElement("span",null,"査定表"))))),c.createElement("div",{className:"flex mt-4"},c.createElement("button",{type:"button",className:"mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>d(!0)},"追加"),c.createElement("button",{type:"button",className:"mr-4 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",onClick:()=>A(!0)},"業者指定"),c.createElement("div",{className:"w-32"},c.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",onChange:e=>N("orderItem",e.target.value)},c.createElement("option",{value:"id"},"登録順"),c.createElement("option",{value:"code"},"コード順"))),c.createElement("div",{className:"w-32"},c.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",onChange:e=>N("order",e.target.value)},c.createElement("option",{value:"asc"},"昇順"),c.createElement("option",{value:"desc"},"降順"))),c.createElement("div",{className:"flex space-x-6 text-gray-700 text-sm ml-4 items-center"},c.createElement("div",null,"金額小計：",null==(e=f.totalItemPrice)?void 0:e.toLocaleString(),"円"),c.createElement("div",null,"金額：",null==(t=f.totalItemPriceRound)?void 0:t.toLocaleString(),"円"),c.createElement("div",null,"金額（税込）：",null==(a=f.totalPrice)?void 0:a.toLocaleString(),"円"))),c.createElement("div",{className:"flex text-sm text-gray-500 mt-4 space-x-4"},c.createElement("div",null,"最終更新日時：",f.updatedItemsAt&&lm()(f.updatedItemsAt).format("YYYY/MM/DD HH:mm")),c.createElement("div",null,"最終更新ユーザー：",null==(r=f.updatedItemsUser)?void 0:r.lastName,null==(n=f.updatedItemsUser)?void 0:n.firstName)),c.createElement("div",{className:"mt-4 space-y-6 pb-20"},c.createElement("div",{className:"overflow-hidden border-b border-gray-400 sm:rounded-lg"},c.createElement("div",{className:"min-w-full border border-gray-300 divide-y divide-gray-300"},c.createElement("div",{className:"flex bg-gray-50"},c.createElement("div",{className:"w-10"}),c.createElement("div",{className:"w-20"}),c.createElement("div",{className:"w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"コード/項目"),c.createElement("div",{className:"w-36 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"単価/数量"),c.createElement("div",{className:"w-16 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"金額"),c.createElement("div",{className:"w-32 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"標準業者"),c.createElement("div",{className:"flex-1 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500"},"確定業者/部屋種別")),c.createElement("div",{className:"bg-white divide-y divide-gray-300"},E.map((e=>c.createElement(ys,{project:f,projectItem:e,key:e.id,onCopy:y,refetch:h}))))))),i&&c.createElement(bs,{project:f,projectItem:g,isOpen:i,handleClose:()=>d(!1),refetch:h}),c.createElement(As,{isOpen:u,project:f,handleClose:()=>A(!1)})):null},Cs=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=oa({variables:{id:e},fetchPolicy:"cache-and-network"});return t?c.createElement(xm,null,c.createElement("div",{className:"mt-10"},c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},c.createElement("table",{className:"min-w-full divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"},"業者"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase"},"発注書"))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},t.purchaseOrders.map((e=>c.createElement("tr",{key:e.id},c.createElement("td",{className:"px-6 py-4"},c.createElement("div",{className:"flex items-center"},c.createElement("div",{className:"ml-4"}))),c.createElement("td",{className:"px-6 py-4 whitespace-nowrap"},c.createElement("div",{className:"text-sm text-gray-900"},e.fileUrl&&c.createElement("a",{href:e.fileUrl},c.createElement("span",{className:"inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"},c.createElement(Va,{className:"flex-shrink-0 w-5 h-5"}),c.createElement("span",null,"発注書")))))))))))))))):null},Is=e=>c.createElement("div",{className:"bg-white divide-y divide-gray-200"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"w-64 px-4 py-4 ml-4"},c.createElement("div",{className:"flex items-center"},c.createElement("div",{className:"text-sm font-medium text-gray-900"},e.label))),c.createElement("div",{className:"px-2 py-2 w-64"},c.createElement("div",{className:"flex items-center"},c.createElement("div",{className:"flex rounded-md shadow-sm w-full"},c.createElement("input",{type:"text",name:e.name,value:e.value,className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",onChange:e.handleChange,onBlur:e.handleBlue})))),c.createElement("div",{className:"px-2 py-2 w-24"},c.createElement("button",{type:"button",className:" px-2 py-2 text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 disabled:opacity-30",disabled:!e.value,onClick:()=>e.handleSubmit(e.reportType)},"出力")),c.createElement("div",{className:"text-xs text-gray-500 my-auto w-32"},c.createElement("p",null,e.datetime)),["report1","report2"].includes(e.reportType)&&c.createElement("div",{className:"text-xs text-gray-500 my-auto"},c.createElement("input",{name:e.checkName,type:"checkbox",checked:e.checkValue,className:"w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",onChange:t=>{e.handleFieldValue(t.target.checked)},onBlur:e.handleBlue})))),Ds=({projectReport:e})=>{const[t,{loading:a}]=function(e){const t=U(U({},R),void 0);return $.D(Ze,t)}(),[r]=function(e){const t=U(U({},R),void 0);return $.D(tt,t)}(),n=(0,Ar.TA)({initialValues:{spreadsheetId1:e.spreadsheetId1,spreadsheetId2:e.spreadsheetId2,spreadsheetId3:e.spreadsheetId3,spreadsheetId4:e.spreadsheetId4,spreadsheetId5:e.spreadsheetId5,spreadsheetId6:e.spreadsheetId6,spreadsheetId7:e.spreadsheetId7,spreadsheetCheckReport1:e.spreadsheetCheckReport1,spreadsheetCheckReport2:e.spreadsheetCheckReport2},onSubmit:e=>{}}),o=a=>{t({variables:{id:e.id,reportType:a,attributes:n.values}})};return c.createElement("form",{onSubmit:n.handleSubmit},c.createElement(Is,{name:"spreadsheetId1",label:"査定表",value:n.values.spreadsheetId1,datetime:e.spreadsheetDatetime1&&lm()(e.spreadsheetDatetime1).format("YYYY/MM/DD HH:mm"),reportType:"assesment",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o}),c.createElement(Is,{name:"spreadsheetId2",label:"材料発注",value:n.values.spreadsheetId2,datetime:e.spreadsheetDatetime2&&lm()(e.spreadsheetDatetime2).format("YYYY/MM/DD HH:mm"),reportType:"orderMaterial",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o}),c.createElement(Is,{name:"spreadsheetId3",label:"クリーニング業者",value:n.values.spreadsheetId3,datetime:e.spreadsheetDatetime3&&lm()(e.spreadsheetDatetime3).format("YYYY/MM/DD HH:mm"),reportType:"cleaning",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o}),c.createElement(Is,{name:"spreadsheetId4",label:"チェック表",value:n.values.spreadsheetId4,datetime:e.spreadsheetDatetime4&&lm()(e.spreadsheetDatetime4).format("YYYY/MM/DD HH:mm"),reportType:"checklist",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o}),c.createElement(Is,{name:"spreadsheetId5",label:"発注書（塗装/木工/雑工）",value:n.values.spreadsheetId5,datetime:e.spreadsheetDatetime5&&lm()(e.spreadsheetDatetime5).format("YYYY/MM/DD HH:mm"),reportType:"order",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o}),c.createElement(Is,{name:"spreadsheetId6",label:"写真報告書①",value:n.values.spreadsheetId6,datetime:e.spreadsheetDatetime6&&lm()(e.spreadsheetDatetime6).format("YYYY/MM/DD HH:mm"),reportType:"report1",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o,checkName:"spreadsheetCheckReport1",checkValue:n.values.spreadsheetCheckReport1,handleFieldValue:t=>{n.setFieldValue("spreadsheetCheckReport1",t),r({variables:{id:e.id,attributes:{spreadsheetCheckReport1:t}}})}}),c.createElement(Is,{name:"spreadsheetId7",label:"写真報告書②",value:n.values.spreadsheetId7,datetime:e.spreadsheetDatetime7&&lm()(e.spreadsheetDatetime7).format("YYYY/MM/DD HH:mm"),reportType:"report2",handleChange:n.handleChange,handleBlue:n.handleBlur,handleSubmit:o,checkName:"spreadsheetCheckReport2",checkValue:n.values.spreadsheetCheckReport2,handleFieldValue:t=>{n.setFieldValue("spreadsheetCheckReport2",t),r({variables:{id:e.id,attributes:{spreadsheetCheckReport2:t}}})}}))},js=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=oa({variables:{id:e}});return(0,Ar.TA)({initialValues:{},onSubmit:e=>{console.log("🚀 ~ file: index.tsx:20 ~ ProjectReport ~ values",e)}}),t?c.createElement(xm,null,c.createElement("div",{className:"mt-2"},c.createElement("div",{className:"text-lg font-bold"},"レポート出力"),c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("div",{className:"overflow-hidden border-b border-gray-200 shadow sm:rounded-lg"},c.createElement("div",{className:"min-w-full divide-y divide-gray-200"},c.createElement("div",{className:"flex flex-1 bg-gray-50"},c.createElement("div",{className:"w-64 px-2 py-3 ml-4 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"レポート"),c.createElement("div",{className:"px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase w-64"},"スプレッドシートID"),c.createElement("div",{className:"px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase w-24"}),c.createElement("div",{className:"px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase"},"最終出力日時")),c.createElement(Ds,{projectReport:t.projectReport})))))))):null};var $s=Object.defineProperty,Ps=Object.getOwnPropertySymbols,Ts=Object.prototype.hasOwnProperty,Ss=Object.prototype.propertyIsEnumerable,Fs=(e,t,a)=>t in e?$s(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Os=e=>{const[t]=Ke(),{register:a,handleSubmit:r}=(0,Pa.cI)({defaultValues:{remark:e.project.remark},mode:"onBlur"}),n=a=>{t({variables:{id:e.project.id,attributes:a}})};return c.createElement(xm,null,c.createElement("form",{onSubmit:r(n)},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-2 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"col-span-6"},c.createElement("h3",{className:"text-lg font-medium leading-6 text-gray-900"},"備考")),c.createElement("div",{className:"col-span-6"},c.createElement("textarea",((e,t)=>{for(var a in t||(t={}))Ts.call(t,a)&&Fs(e,a,t[a]);if(Ps)for(var a of Ps(t))Ss.call(t,a)&&Fs(e,a,t[a]);return e})({rows:5,className:"block w-full p-2 text-sm border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"},a("remark",{onBlur:r(n)})))))))},Ms=()=>{const{id:e}=(0,D.UO)(),{data:{project:t=null}={}}=oa({variables:{id:e}});return t?c.createElement(Os,{project:t}):null},Us=()=>{const{id:e}=(0,D.UO)();console.log("🚀 ~ file: index.tsx:15 ~ Project ~ id:",e);const{data:{project:t=null}={}}=oa({variables:{id:e}}),[a]=function(e){const t=U(U({},R),void 0);return $.D(ze,t)}(),[r]=function(e){const t=U(U({},R),void 0);return $.D(Ve,t)}();return(0,c.useEffect)((()=>(t&&(console.log("mounting"),a({variables:{id:t.id}})),()=>{console.log("unmounting"),t&&r({variables:{id:t.id}})})),[]),c.createElement(D.Z5,null,c.createElement(D.AW,{path:"form/*",element:c.createElement(_m,null)}),c.createElement(D.AW,{path:"orders",element:c.createElement(Cs,null)}),c.createElement(D.AW,{path:"images/*",element:c.createElement(cs,null)}),c.createElement(D.AW,{path:"company",element:c.createElement(Nm,null)}),c.createElement(D.AW,{path:"items",element:c.createElement(ks,null)}),c.createElement(D.AW,{path:"report",element:c.createElement(js,null)}),c.createElement(D.AW,{path:"remark",element:c.createElement(Ms,null)}))},Rs=Fl.Ry().shape({yearId:Fl.Z_().trim().required("必須項目です"),orderNumberType:Fl.Z_().trim().required("必須項目です"),orderNumber:Fl.Z_().trim().required("必須項目です"),buildingId:Fl.Z_().trim().required("必須項目です"),buildingNumberId:Fl.Z_().trim().required("必須項目です"),roomId:Fl.Z_().trim().required("必須項目です")}),Ys=()=>{var e;const t=(0,D.s0)(),[a,r]=(0,c.useState)(null),[n,o]=(0,c.useState)(null),[l,{loading:m}]=function(e){const t=U(U({},R),void 0);return $.D(qe,t)}(),[s,i]=(0,c.useState)(null),[d,u]=(0,c.useState)(null),{data:{years:A=[]}={}}=Ia(),{data:{allBuildings:g=[]}={}}=function(e){const t=U(U({},R),void 0);return P.a(It,t)}(),b=g.map((e=>({value:e.id,label:e.name}))),y=null==(e=null==s?void 0:s.buildingNumbers)?void 0:e.map((e=>({value:e.id,label:e.name}))),x=null==d?void 0:d.rooms.map((e=>({value:e.id,label:e.name}))),{data:{projects:{projects:f=[]}={}}={}}=ca({variables:{search:{orderNumber:a,orderNumberType:n}},skip:!a}),E=(0,Ar.TA)({enableReinitialize:!0,validationSchema:Rs,initialValues:{yearId:"",orderNumberType:"",orderNumber:"",buildingId:null,buildingNumberId:null,roomId:null,closeTypes:[],closeReason:""},onSubmit:e=>{l({variables:{attributes:e}}).then((()=>t(`/projects${location.search}`)))}});return c.createElement("div",{className:"w-full"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/projects",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"案件一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:E.handleSubmit},c.createElement("div",{className:"my-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"年度"),c.createElement("select",{value:E.values.yearId,className:"px-4 py-2 border border-gray-300",onChange:e=>{E.setFieldValue("yearId",e.target.value)}},c.createElement("option",{value:""}),A.map((e=>c.createElement("option",{key:`year-${e.id}`,value:e.id},e.year))))),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"指示番号"),c.createElement("div",{className:"flex"},c.createElement("input",{type:"text",name:"orderNumberType",className:"block w-16 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:E.values.orderNumberType,onChange:E.handleChange,onBlur:()=>{o(E.values.orderNumberType)}}),c.createElement("span",{className:"inline-flex items-center mx-4"},"-"),c.createElement("input",{type:"text",name:"orderNumber",className:"block w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:E.values.orderNumber,onChange:E.handleChange,onBlur:()=>{r(E.values.orderNumber)}})),f.length>0&&c.createElement("span",{className:"text-red-500 text-sm"},"すでに登録済みの指示番号です")),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"建物"),c.createElement(br.ZP,{options:b,isClearable:!0,closeMenuOnSelect:!0,onChange:e=>{E.setFieldValue("buildingId",e.value),E.setFieldValue("name",e.label);const t=g.find((t=>t.id===e.value));i(t)}})),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"号棟"),c.createElement(br.ZP,{options:y,isClearable:!0,closeMenuOnSelect:!0,onChange:e=>{E.setFieldValue("buildingNumberId",e.value);const t=s.buildingNumbers.find((t=>t.id===e.value));u(t)}})),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"号室"),c.createElement(br.ZP,{options:x,isClearable:!0,closeMenuOnSelect:!0,onChange:e=>{E.setFieldValue("roomId",e.value)}})),c.createElement("div",{className:"col-span-2"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"閉鎖区分"),c.createElement("div",{className:"relative flex items-start space-x-4"},Cm.map((e=>c.createElement(c.Fragment,null,c.createElement("div",{className:"flex items-center h-5"},c.createElement("input",{id:e.value,name:e.value,type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",value:e.value,onChange:e=>{const t=`${e.target.value}`;E.values.closeTypes.includes(t)?E.setFieldValue("closeTypes",E.values.closeTypes.filter((e=>e!==t))):E.setFieldValue("closeTypes",[...E.values.closeTypes,t])},onBlur:E.handleBlur}),c.createElement("div",{className:"ml-1 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},e.name)))))))),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!E.dirty||!E.isValid||f.length>0,loading:m},"保存")))))))},Vs=({project:e})=>{var t,a,r;return c.createElement("tr",{className:"bg-white"},c.createElement("td",{className:"text-sm px-2"},null==e?void 0:e.id),c.createElement("td",{className:"py-2"},c.createElement("div",{className:"text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300 mt-2"},c.createElement(p.rU,{to:`/projects/${null==e?void 0:e.id}/form/base${location.search}`},null==e?void 0:e.name)),c.createElement("div",{className:"flex space-x-8"},c.createElement("div",{className:"text-sm text-gray-500 w-32"},c.createElement("span",{className:"mr-2"},null==(t=null==e?void 0:e.buildingNumber)?void 0:t.name," 号棟"),c.createElement("span",null,null==(a=null==e?void 0:e.room)?void 0:a.name," 号室")),c.createElement("div",null,c.createElement("div",null,e.itemUploadDateText&&c.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"1.査定表読込済"),e.reportDateText&&c.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"2.施工前済・質疑回答待ち"),e.checklistDateText&&c.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"3.施工前済・最終CL済"),e.lackImageDateText&&c.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"4.施工後不足写真待ち"),e.completeImageDateText&&c.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"5.施工後済"),e.invoiceDateText&&c.createElement("span",{className:"rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1"},"6.請求書済")),c.createElement("div",null,e.jkkStatus1&&c.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"1.CL済"),e.jkkStatus2&&c.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"2.指示書済済"),e.jkkStatus3&&c.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"3.指示書再発行待ち"),e.jkkStatus4&&c.createElement("span",{className:"rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1"},"4.指示書再発行済ち"))))),c.createElement("td",{className:"px-2 py-4 text-sm font-medium whitespace-nowrap"},c.createElement("div",{className:"text-gray-700 text-right"},null==(r=e.totalPrice)?void 0:r.toLocaleString())),c.createElement("td",{className:"px-2 py-4 text-sm font-medium whitespace-nowrap"},c.createElement("div",{className:"text-gray-700"},c.createElement("span",null,e.constructionStartDate),c.createElement("span",{className:"mx-2"},"-"),c.createElement("span",null,e.constructionEndDate))))};var qs=Object.defineProperty,Ls=Object.defineProperties,_s=Object.getOwnPropertyDescriptors,Zs=Object.getOwnPropertySymbols,zs=Object.prototype.hasOwnProperty,Ws=Object.prototype.propertyIsEnumerable,Hs=(e,t,a)=>t in e?qs(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const Gs=()=>{const[e,t]=(0,p.lr)(),a=e.get("page"),r=e.get("keyword"),n=e.get("orderType"),o=(0,c.useMemo)((()=>Array.from(new Set(e.entries()))),[e]),{data:{projects:{projects:l=[],pagination:m={}}={}}={}}=ca({variables:{page:a?Number(a):1,search:{keyword:r,orderType:n}},fetchPolicy:"cache-and-network",skip:!n}),s=(0,Ar.TA)({initialValues:{keyword:r||"",active:!1},onSubmit:e=>{t({keyword:e.keyword,page:"1",orderType:n})}});return(0,c.useEffect)((()=>{n||t({orderType:"desc",keyword:r||"",page:a||"1"})}),[n]),c.createElement("div",{className:"flex flex-col h-full"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 h-full"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"案件一覧"),c.createElement("form",{className:"px-10 py-2 mb-4 bg-white",onSubmit:s.handleSubmit},c.createElement("div",{className:"grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6"},c.createElement("div",{className:"sm:col-span-4"},c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"キーワード"),c.createElement("div",{className:"mt-1"},c.createElement("input",{name:"keyword",type:"text",className:"block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:s.values.keyword,onChange:s.handleChange,onBlur:()=>s.handleSubmit()}))))),c.createElement(p.rU,{to:"/projects/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("div",{className:"flex mt-4"},c.createElement("div",{className:"relative w-32"},c.createElement("select",{className:"inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500",value:n,onChange:e=>((e,a)=>{let r={};var n;o.forEach((e=>r[e[0]]=e[1])),t((n=((e,t)=>{for(var a in t||(t={}))zs.call(t,a)&&Hs(e,a,t[a]);if(Zs)for(var a of Zs(t))Ws.call(t,a)&&Hs(e,a,t[a]);return e})({},r),Ls(n,_s({[e]:a}))))})("orderType",e.target.value)},c.createElement("option",{value:"asc"},"昇順"),c.createElement("option",{value:"desc"},"降順")))),c.createElement(Jr,{totalCount:m.totalCount||0,currentPage:m.currentPage||0,totalPages:m.totalPages||0,pageSize:20}),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-12"},"ID"),c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"名称"),c.createElement("th",{scope:"col",className:"w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"工事金額（指示書/税込）"),c.createElement("th",{scope:"col",className:"px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500"},"工期（開始日 / 終了日）"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-400"},l.map((e=>c.createElement(Vs,{project:e,key:null==e?void 0:e.id}))))))))},Js=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(Ys,null)}),c.createElement(D.AW,{path:":id/*",element:c.createElement(Us,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Gs,null)}))),Ks=e=>{var t;return c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/roomlayouts",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"部屋レイアウト"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==(t=e.roomLayout)?void 0:t.name))))),e.children))};var Qs=Object.defineProperty,Xs=Object.getOwnPropertySymbols,ei=Object.prototype.hasOwnProperty,ti=Object.prototype.propertyIsEnumerable,ai=(e,t,a)=>t in e?Qs(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,ri=(e,t)=>{for(var a in t||(t={}))ei.call(t,a)&&ai(e,a,t[a]);if(Xs)for(var a of Xs(t))ti.call(t,a)&&ai(e,a,t[a]);return e};const ni=()=>{var e;const{id:t}=(0,D.UO)(),a=(0,D.s0)(),[r,{loading:n,data:o}]=function(e){const t=U(U({},R),e);return $.D(ft,t)}({onCompleted:e=>{e.createRoomLayout.error||a("/roomlayouts")}}),{register:l,handleSubmit:m,formState:{errors:s,isDirty:i,isValid:d}}=(0,Pa.cI)({mode:"all"});return c.createElement(Ks,null,c.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},(null==o?void 0:o.createRoomLayout.error)&&c.createElement("p",{className:"text-red-500"},null==o?void 0:o.createRoomLayout.error),c.createElement("form",{className:"p-6 space-y-6",onSubmit:m((e=>{r({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",ri({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("name",{required:!0}))),"required"===(null==(e=s.name)?void 0:e.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"工事金額：25万円（税抜）未満"),c.createElement("input",ri({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("price1",{valueAsNumber:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"工事金額：25万円（税抜）以上"),c.createElement("input",ri({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("price2",{valueAsNumber:!0})))),c.createElement("div",{className:"py-3 text-right flex items-center"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm",onClick:()=>a("/roomlayouts")},"戻る"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!i||!d,loading:n},"保存")))))};var oi=Object.defineProperty,li=Object.getOwnPropertySymbols,mi=Object.prototype.hasOwnProperty,si=Object.prototype.propertyIsEnumerable,ii=(e,t,a)=>t in e?oi(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a,ci=(e,t)=>{for(var a in t||(t={}))mi.call(t,a)&&ii(e,a,t[a]);if(li)for(var a of li(t))si.call(t,a)&&ii(e,a,t[a]);return e};const di=e=>{var t;const{id:a}=(0,D.UO)(),r=(0,D.s0)(),[n,{loading:o}]=function(e){const t=U(U({},R),e);return $.D(Et,t)}({onCompleted:e=>{e.updateRoomLayout.error||r(`/roomlayouts${location.search}`)}}),{register:l,handleSubmit:m,formState:{isDirty:s,isValid:i,errors:d}}=(0,Pa.cI)({defaultValues:{name:e.roomLayout.name,price1:e.roomLayout.price1,price2:e.roomLayout.price2}});return c.createElement(Ks,{roomLayout:e.roomLayout},c.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:m((e=>{n({variables:{id:a,attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",ci({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("name",{required:!0}))),"required"===(null==(t=d.name)?void 0:t.type)&&c.createElement("p",{className:"text-red-400"},"必須項目です")),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"工事金額：25万円（税抜）未満"),c.createElement("input",ci({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("price1",{valueAsNumber:!0})))),c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"工事金額：25万円（税抜）以上"),c.createElement("input",ci({type:"number",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("price2",{valueAsNumber:!0})))),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!s||!i||o,loading:o},"保存")))))},ui=()=>{const{id:e}=(0,D.UO)(),{data:{roomLayout:t=null}={}}=function(e){const t=U(U({},R),e);return P.a(da,t)}({variables:{id:e}});return t?c.createElement(di,{roomLayout:t}):null},Ai=({id:e,index:t,roomLayout:a,moveItem:r,order:n})=>{var o,l;const m=(0,c.useRef)(null),[{handlerId:s},i]=(0,rl.L)({accept:"RoomLayout",collect:e=>({handlerId:e.getHandlerId()}),hover(e,a){var n;if(!m.current)return;const o=e.index,l=t;if(o===l)return;const s=null==(n=m.current)?void 0:n.getBoundingClientRect(),i=(s.bottom-s.top)/2,c=a.getClientOffset().y-s.top;o<l&&c<i||o>l&&c>i||(r(e.id,o,l),e.index=l)}}),[{isDragging:d},u]=(0,nl.c)({type:"RoomLayout",item:()=>({id:e,index:t}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: RoomLayout.tsx:87 ~ RoomLayout ~ item",e),n(e.id,e.index)}}),A=d?0:1;return u(i(m)),c.createElement("div",{ref:m,className:"bg-white flex w-full",style:{opacity:A}},c.createElement("div",{className:"w-44 px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/roomlayouts/${a.id}`},a.name)),c.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},null==(o=a.price1)?void 0:o.toLocaleString()),c.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},null==(l=a.price2)?void 0:l.toLocaleString()))},pi=e=>{const[t,a]=(0,c.useState)([]),[r]=function(e){const t=U(U({},R),void 0);return $.D(ht,t)}(),n=(e,t,r)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${t}`),console.log(`update: hoverIndex: ${r}`),a((e=>el()(e,{$splice:[[t,1],[r,0,e[t]]]})))},o=(e,t)=>{console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ id",e),console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ index",t),r({variables:{id:e,attributes:{position:t}}})};return(0,c.useEffect)((()=>{a(e.roomLayouts)}),[e.roomLayouts]),c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"部屋レイアウト"),c.createElement(p.rU,{to:"/roomlayouts/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("div",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("div",{className:"bg-gray-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"w-44 px-6 py-3 text-xs font-medium text-left text-gray-500"},"名称"),c.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"工事金額：25万円（税抜）未満"),c.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"工事金額：25万円（税抜）以上"))),c.createElement(tl.W,{backend:al.PD},c.createElement("div",{className:"bg-white divide-y divide-gray-200"},t.map(((e,t)=>c.createElement(Ai,{id:e.id,index:t,roomLayout:e,key:e.id,moveItem:n,order:o})))))))))},gi=()=>{const{data:{roomLayouts:e=null}={}}=Aa({fetchPolicy:"cache-and-network"});return e?c.createElement(pi,{roomLayouts:e}):null},bi=()=>c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(ni,null)}),c.createElement(D.AW,{path:":id",element:c.createElement(ui,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(gi,null)})),yi=()=>c.createElement(ln,null,c.createElement(bi,null)),xi=e=>{var t;const{id:a}=(0,D.UO)();return c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/roomtypes",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700","aria-current":void 0},"部屋種別"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},null==(t=e.roomType)?void 0:t.name))))),c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement(p.rU,{to:`/roomtypes/${a}/base${location.search}`},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm "+("base",location.pathname.indexOf("base")>-1?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"基本情報"))))),e.children))},fi=Fl.Ry().shape({name:Fl.Z_().trim().required("必須項目です")}),Ei=()=>{const{id:e}=(0,D.UO)(),t=(0,D.s0)(),[a,{loading:r}]=function(e){const t=U(U({},R),void 0);return $.D(wt,t)}(),{data:{roomType:n=null}={}}=ba({variables:{id:e}}),o=(0,Ar.TA)({enableReinitialize:!0,validationSchema:fi,initialValues:{name:(null==n?void 0:n.name)||""},onSubmit:r=>{a({variables:{id:e,attributes:r}}).then((()=>t("/roomtypes")))}});return n?c.createElement(xi,{roomType:n},c.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},c.createElement("form",{className:"p-6 space-y-6",onSubmit:o.handleSubmit},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",{type:"text",name:"name",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm",value:o.values.name,onChange:o.handleChange,onBlur:o.handleBlur})),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",loading:r},"保存"))))):null};var hi=Object.defineProperty,vi=Object.getOwnPropertySymbols,wi=Object.prototype.hasOwnProperty,Ni=Object.prototype.propertyIsEnumerable,Bi=(e,t,a)=>t in e?hi(e,t,{enumerable:!0,configurable:!0,writable:!0,value:a}):e[t]=a;const ki=()=>{var e;const{id:t}=(0,D.UO)(),a=(0,D.s0)(),[r,{loading:n,data:o}]=function(e){const t=U(U({},R),e);return $.D(vt,t)}({onCompleted:e=>{e.createRoomTypeModel.error||a("/roomtypes")}}),{register:l,handleSubmit:m,formState:{errors:s,isDirty:i,isValid:d}}=(0,Pa.cI)({mode:"all"});return c.createElement(xi,null,c.createElement("div",{className:"mt-10 md:col-span-2 w-screen max-w-xl"},(null==o?void 0:o.createRoomTypeModel.error)&&c.createElement("p",{className:"text-red-500"},null==o?void 0:o.createRoomTypeModel.error),c.createElement("form",{className:"p-6 space-y-6",onSubmit:m((e=>{r({variables:{attributes:e}})}))},c.createElement("div",null,c.createElement("label",{htmlFor:"name",className:"block text-sm font-medium text-gray-700"},"名称"),c.createElement("input",((e,t)=>{for(var a in t||(t={}))wi.call(t,a)&&Bi(e,a,t[a]);if(vi)for(var a of vi(t))Ni.call(t,a)&&Bi(e,a,t[a]);return e})({type:"text",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"},l("name",{required:!0}))),"required"===(null==(e=s.name)?void 0:e.type)&&c.createElement("p",{className:"text-red-400"},"名称は必須項目です")),c.createElement("div",{className:"py-3 text-right flex items-center"},c.createElement(Da,{type:"button",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm",onClick:()=>a("/roomtypes")},"戻る"),c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:!i||!d,loading:n},"保存")))))},Ci=()=>c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(ki,null)}),c.createElement(D.AW,{path:"base",element:c.createElement(Ei,null)})),Ii=({id:e,index:t,roomType:a,moveItem:r,order:n})=>{const o=(0,c.useRef)(null),[{handlerId:l},m]=(0,rl.L)({accept:"RoomType",collect:e=>({handlerId:e.getHandlerId()}),hover(e,a){var n;if(!o.current)return;const l=e.index,m=t;if(l===m)return;const s=null==(n=o.current)?void 0:n.getBoundingClientRect(),i=(s.bottom-s.top)/2,c=a.getClientOffset().y-s.top;l<m&&c<i||l>m&&c>i||(r(e.id,l,m),e.index=m)}}),[{isDragging:s},i]=(0,nl.c)({type:"RoomType",item:()=>({id:e,index:t}),collect:e=>({isDragging:e.isDragging()}),end:e=>{console.log("🚀 ~ file: RoomType.tsx:87 ~ RoomType ~ item",e),n(e.id,e.index)}}),d=s?0:1;return i(m(o)),c.createElement("div",{ref:o,className:"bg-white flex w-full",style:{opacity:d}},c.createElement("div",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/roomtypes/${a.id}/base`},a.name)))},Di=()=>{const[e,t]=(0,c.useState)([]),{data:{roomTypes:a=[]}={}}=xa({fetchPolicy:"cache-and-network"}),[r]=function(e){const t=U(U({},R),void 0);return $.D(Nt,t)}(),n=(e,a,r)=>{console.log(`update: id: ${e}`),console.log(`update: dragIndex: ${a}`),console.log(`update: hoverIndex: ${r}`),t((e=>el()(e,{$splice:[[a,1],[r,0,e[a]]]})))},o=(e,t)=>{console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ id",e),console.log("🚀 ~ file: index.tsx:28 ~ handleOrder ~ index",t),r({variables:{id:e,attributes:{position:t}}})};return(0,c.useEffect)((()=>{t(a)}),[a]),c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"部屋種別"),c.createElement(p.rU,{to:"/roomtypes/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("div",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("div",{className:"bg-gray-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"名称"))),c.createElement(tl.W,{backend:al.PD},c.createElement("div",{className:"bg-white divide-y divide-gray-200"},e.map(((e,t)=>c.createElement(Ii,{id:e.id,index:t,roomType:e,key:e.id,moveItem:n,order:o})))))))))},ji=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(ki,null)}),c.createElement(D.AW,{path:":id/*",element:c.createElement(Ci,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Di,null)}))),$i=()=>{const{id:e}=(0,D.UO)(),t=e=>location.pathname.endsWith(e);return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(t("base")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},c.createElement(p.rU,{to:`/users/${e}/base`},"基本情報")),c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+(t("password")?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},c.createElement(p.rU,{to:`/users/${e}/password`},"パスワード")))))},Pi=Fl.Ry().shape({lastName:Fl.Z_().trim().required("必須項目です"),firstName:Fl.Z_().trim().required("必須項目です")}),Ti=()=>{const{id:e}=(0,D.UO)(),t=(0,D.s0)(),[a,{data:r,loading:n}]=Ct({onCompleted:e=>{e.updateUser.error||t(`/users${location.search}`)}}),{data:{user:o=null}={}}=Na({variables:{id:e}}),l=(0,Ar.TA)({enableReinitialize:!0,validationSchema:Pi,initialValues:{loginId:(null==o?void 0:o.loginId)||"",lastName:(null==o?void 0:o.lastName)||"",firstName:(null==o?void 0:o.firstName)||"",isAssesment:null==o?void 0:o.isAssesment,tel:null==o?void 0:o.tel,active:null==o?void 0:o.active},onSubmit:t=>{a({variables:{id:e,attributes:t}})}});return o?c.createElement("div",{className:"w-screen max-w-xl"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"従業員一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},o.lastName," ",o.firstName))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement($i,null),(null==r?void 0:r.updateUser.error)&&c.createElement("div",{className:"p-4 rounded-md bg-red-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"ml-3"},c.createElement("div",{className:"mt-2 text-sm text-red-700"},c.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},c.createElement("li",null,null==r?void 0:r.updateUser.error)))))),c.createElement("form",{className:"p-6 space-y-6",onSubmit:l.handleSubmit},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"ログインID"),c.createElement("input",{type:"text",name:"loginId",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.loginId&&l.touched.loginId?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.loginId,onChange:l.handleChange,onBlur:l.handleBlur})),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),c.createElement("input",{type:"text",name:"lastName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.lastName&&l.touched.lastName?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.lastName,onChange:l.handleChange,onBlur:l.handleBlur})),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),c.createElement("input",{type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.firstName&&l.touched.firstName?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.firstName,onChange:l.handleChange,onBlur:l.handleBlur})),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"TEL"),c.createElement("input",{type:"text",name:"tel",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.tel&&l.touched.tel?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.tel,onChange:l.handleChange,onBlur:l.handleBlur})),c.createElement("div",{className:"mt-4 space-y-4"},c.createElement("div",{className:"flex items-center"},c.createElement("div",{className:"relative flex items-start"},c.createElement("div",{className:"flex items-center h-5"},c.createElement("input",{id:"comments","aria-describedby":"comments-description",name:"isAddesment",type:"checkbox",className:"w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer",onChange:e=>l.setFieldValue("isAssesment",e.target.checked),checked:l.values.isAssesment})),c.createElement("div",{className:"ml-3 text-sm"},c.createElement("label",{className:"font-medium text-gray-700"},"査定同行者"))))),c.createElement("div",{className:"mt-4 space-y-4"},c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"active",name:"active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",checked:l.values.active,onChange:e=>l.setFieldValue("active",!0)}),c.createElement("label",{htmlFor:"active",className:"block ml-3 text-sm font-medium text-gray-700"},"有効")),c.createElement("div",{className:"flex items-center"},c.createElement("input",{id:"non-active",name:"non-active",type:"radio",className:"w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500",checked:!l.values.active,onChange:e=>{l.setFieldValue("active",!1)}}),c.createElement("label",{htmlFor:"non-active",className:"block ml-3 text-sm font-medium text-gray-700"},"無効"))),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!l.isValid||!l.dirty,loading:n},"保存"))))))):null},Si=Fl.Ry().shape({password:Fl.Z_().trim().required("必須項目です").min(6,"6文字以上で設定してください"),passwordConfirmation:Fl.Z_().trim().required("必須項目です").oneOf([Fl.iH("password"),null],"パスワードが一致していません")}),Fi=()=>{const{id:e}=(0,D.UO)(),t=(0,D.s0)(),[a,{data:r,loading:n}]=Ct({onCompleted:e=>{e.updateUser.error||t(`/users${location.search}`)}}),{data:{user:o=null}={}}=Na({variables:{id:e}}),l=(0,Ar.TA)({enableReinitialize:!0,validationSchema:Si,initialValues:{password:"",passwordConfirmation:""},onSubmit:t=>{a({variables:{id:e,attributes:t}})}});return o?c.createElement("div",{className:"w-screen max-w-xl"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"従業員一覧"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},o.lastName," ",o.firstName))))),c.createElement("div",{className:"mt-10 md:col-span-2"},c.createElement($i,null),(null==r?void 0:r.updateUser.error)&&c.createElement("div",{className:"p-4 rounded-md bg-red-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"ml-3"},c.createElement("div",{className:"mt-2 text-sm text-red-700"},c.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},c.createElement("li",null,null==r?void 0:r.updateUser.error)))))),c.createElement("form",{className:"p-6 space-y-6",onSubmit:l.handleSubmit},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード"),c.createElement("input",{type:"password",name:"password",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.password&&l.touched.password?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.password,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.password&&l.touched.password&&c.createElement("span",{className:"text-red-500"},l.errors.password)),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード(確認)"),c.createElement("input",{type:"password",name:"passwordConfirmation",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(l.errors.passwordConfirmation&&l.touched.passwordConfirmation?"border-red-500 bg-red-200 focus:border-red-500":""),value:l.values.passwordConfirmation,onChange:l.handleChange,onBlur:l.handleBlur}),l.errors.passwordConfirmation&&l.touched.passwordConfirmation&&c.createElement("span",{className:"text-red-500"},l.errors.passwordConfirmation)),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:n||!l.isValid||!l.dirty,loading:n},"保存"))))))):null},Oi=Fl.Ry().shape({loginId:Fl.Z_().required("必須項目です"),email:Fl.Z_().email("メールアドレスの形式が不正です"),lastName:Fl.Z_().trim().required("必須項目です"),firstName:Fl.Z_().trim().required("必須項目です"),password:Fl.Z_().trim().required("必須項目です").min(6,"6文字以上で設定してください"),passwordConfirmation:Fl.Z_().trim().required("必須項目です").oneOf([Fl.iH("password"),null],"パスワードが一致していません")}),Mi=()=>{const e=(0,D.s0)(),[t,{data:a,loading:r}]=function(e){const t=U(U({},R),e);return $.D(Bt,t)}({onCompleted:t=>{t.createUser.error||e(`/users${location.search}`)}}),n=(0,Ar.TA)({enableReinitialize:!0,validationSchema:Oi,initialValues:{loginId:"",email:"",lastName:"",firstName:"",password:"",passwordConfirmation:""},onSubmit:e=>{t({variables:{attributes:e}})}});return c.createElement("div",{className:"w-full"},c.createElement("div",{className:"flex flex-col h-full py-6 overflow-y-scroll bg-white"},c.createElement("div",{className:"px-4 sm:px-6"},c.createElement("nav",{className:"flex","aria-label":"Breadcrumb"},c.createElement("ol",{role:"list",className:"flex items-center space-x-4"},c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(p.rU,{to:"/users",className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"従業員"))),c.createElement("li",null,c.createElement("div",{className:"flex items-center"},c.createElement(Ua,{className:"flex-shrink-0 w-5 h-5 text-gray-400","aria-hidden":"true"}),c.createElement("p",{className:"ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"},"新規作成"))))),c.createElement("div",{className:"mt-10 md:col-span-2"},(null==a?void 0:a.createUser.error)&&c.createElement("div",{className:"p-4 rounded-md bg-red-50"},c.createElement("div",{className:"flex"},c.createElement("div",{className:"ml-3"},c.createElement("div",{className:"mt-2 text-sm text-red-700"},c.createElement("ul",{role:"list",className:"pl-5 space-y-1 list-disc"},c.createElement("li",null,null==a?void 0:a.createUser.error)))))),c.createElement("form",{className:"p-6 space-y-6",onSubmit:n.handleSubmit},c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"ログインID"),c.createElement("input",{type:"text",name:"loginId",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(n.errors.loginId&&n.touched.loginId?"border-red-500 bg-red-200 focus:border-red-500":""),value:n.values.loginId,onChange:n.handleChange,onBlur:n.handleBlur}),n.errors.loginId&&n.touched.loginId&&c.createElement("span",{className:"text-red-500"},n.errors.loginId)),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"姓"),c.createElement("input",{type:"text",name:"lastName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(n.errors.lastName&&n.touched.lastName?"border-red-500 bg-red-200 focus:border-red-500":""),value:n.values.lastName,onChange:n.handleChange,onBlur:n.handleBlur}),n.errors.lastName&&n.touched.lastName&&c.createElement("span",{className:"text-red-500"},n.errors.lastName)),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"名"),c.createElement("input",{type:"text",name:"firstName",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(n.errors.firstName&&n.touched.firstName?"border-red-500 bg-red-200 focus:border-red-500":""),value:n.values.firstName,onChange:n.handleChange,onBlur:n.handleBlur}),n.errors.firstName&&n.touched.firstName&&c.createElement("span",{className:"text-red-500"},n.errors.firstName)),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード"),c.createElement("input",{type:"password",name:"password",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(n.errors.password&&n.touched.password?"border-red-500 bg-red-200 focus:border-red-500":""),value:n.values.password,onChange:n.handleChange,onBlur:n.handleBlur}),n.errors.password&&n.touched.password&&c.createElement("span",{className:"text-red-500"},n.errors.password)),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"パスワード(確認)"),c.createElement("input",{type:"password",name:"passwordConfirmation",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(n.errors.passwordConfirmation&&n.touched.passwordConfirmation?"border-red-500 bg-red-200 focus:border-red-500":""),value:n.values.passwordConfirmation,onChange:n.handleChange,onBlur:n.handleBlur}),n.errors.passwordConfirmation&&n.touched.passwordConfirmation&&c.createElement("span",{className:"text-red-500"},n.errors.passwordConfirmation)),c.createElement("div",null,c.createElement("label",{className:"block text-sm font-medium text-gray-700"},"メールアドレス"),c.createElement("input",{type:"email",name:"email",className:"block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm "+(n.errors.email&&n.touched.email?"border-red-500 bg-red-200 focus:border-red-500":""),value:n.values.email,onChange:n.handleChange,onBlur:n.handleBlur}),n.errors.email&&n.touched.email&&c.createElement("span",{className:"text-red-500"},n.errors.email)),c.createElement("div",{className:"py-3 text-right"},c.createElement(Da,{type:"submit",className:"inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",disabled:r||!n.isValid||!n.dirty,loading:r},"保存")))))))},Ui=()=>{const[e,t]=(0,p.lr)(),a=e.get("active");return c.createElement("div",{className:"hidden sm:block"},c.createElement("div",{className:"border-b border-gray-200"},c.createElement("nav",{className:"flex -mb-px space-x-8","aria-label":"Tabs"},c.createElement("div",{onClick:()=>t({active:"true"})},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+("true"===a?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"有効")),c.createElement("div",{onClick:()=>t({active:"false"})},c.createElement("div",{className:"whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer "+("true"!==a?"border-indigo-500 text-indigo-600":"border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300")},"無効")))))},Ri=()=>{const[e,t]=(0,p.lr)(),a=e.get("active"),{data:{users:r=[]}={}}=ka({variables:{search:{active:"true"===a}},fetchPolicy:"cache-and-network"});return(0,c.useEffect)((()=>{null==a&&t({active:"true"})}),[]),null==a?null:c.createElement("div",{className:"flex flex-col"},c.createElement("div",{className:"-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8"},c.createElement("div",{className:"inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8"},c.createElement("h1",{className:"mb-4 text-2xl font-bold"},"従業員"),c.createElement(Ui,null),c.createElement(p.rU,{to:"/users/new"},c.createElement("div",{className:"inline-flex justify-center px-4 py-2 mt-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"},"新規作成")),c.createElement("table",{className:"min-w-full mt-4 divide-y divide-gray-200"},c.createElement("thead",{className:"bg-gray-50"},c.createElement("tr",null,c.createElement("th",{scope:"col",className:"w-6 px-6 py-3 text-xs font-medium text-left text-gray-500"},"ID"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-80"},"ログインID"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500 w-60"},"名称"),c.createElement("th",{scope:"col",className:"px-6 py-3 text-xs font-medium text-left text-gray-500"},"査定同行者"),c.createElement("th",null))),c.createElement("tbody",{className:"bg-white divide-y divide-gray-200"},r.map((e=>c.createElement("tr",{className:"bg-white",key:e.id},c.createElement("td",{className:"ml-10"},c.createElement("div",{className:"px-2 cursor-pointer"},e.id)),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/users/${e.id}/base`},e.loginId)),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},c.createElement(p.rU,{to:`/users/${e.id}/base`},e.lastName," ",e.firstName)),c.createElement("td",{className:"px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap"},e.isAssesment&&c.createElement("div",null,c.createElement("span",{className:"inline-flex items-center rounded-md bg-yellow-100 px-2.5 py-0.5 text-sm font-medium text-yellow-800"},"査定同行者")))))))))))},Yi=()=>c.createElement(ln,null,c.createElement(D.Z5,null,c.createElement(D.AW,{path:"new",element:c.createElement(Mi,null)}),c.createElement(D.AW,{path:":id/base",element:c.createElement(Ti,null)}),c.createElement(D.AW,{path:":id/password",element:c.createElement(Fi,null)}),c.createElement(D.AW,{index:!0,element:c.createElement(Ri,null)}))),Vi=()=>c.createElement(D.Z5,null,c.createElement(D.AW,{path:"/",element:c.createElement(sl,null)}),c.createElement(D.AW,{path:"/login",element:c.createElement(nm,null)}),c.createElement(D.AW,{path:"/comments/*",element:c.createElement(Ln,null)}),c.createElement(D.AW,{path:"/commenttypes/*",element:c.createElement(ho,null)}),c.createElement(D.AW,{path:"/buildings/*",element:c.createElement(mn,null)}),c.createElement(D.AW,{path:"/roomtypes/*",element:c.createElement(ji,null)}),c.createElement(D.AW,{path:"/roomlayouts/*",element:c.createElement(yi,null)}),c.createElement(D.AW,{path:"/projects/*",element:c.createElement(Js,null)}),c.createElement(D.AW,{path:"/orders/*",element:c.createElement(dm,null)}),c.createElement(D.AW,{path:"/items/*",element:c.createElement(Sl,null)}),c.createElement(D.AW,{path:"/companies/*",element:c.createElement(Uo,null)}),c.createElement(D.AW,{path:"/companytypes/*",element:c.createElement(ml,null)}),c.createElement(D.AW,{path:"/users/*",element:c.createElement(Yi,null)}),c.createElement(D.AW,{path:"/jkk_users/*",element:c.createElement(Xl,null)})),qi=i()({uri:"/graphql",credentials:"include"}),Li=new r.f({link:n.i.from([(0,m.q)((({graphQLErrors:e,networkError:t})=>{e&&e.map((({message:e,locations:t,path:a})=>{})),t&&console.error(`[Network error]: ${t}`)})),qi]),cache:new l.h});A().setAppElement("#app");const _i=()=>c.createElement(o.e,{client:Li},c.createElement(p.VK,null,c.createElement(Vi,null)));d.render(c.createElement(_i,null),document.getElementById("app"))}},n={};function o(e){var t=n[e];if(void 0!==t)return t.exports;var a=n[e]={id:e,loaded:!1,exports:{}};return r[e].call(a.exports,a,a.exports,o),a.loaded=!0,a.exports}o.m=r,e=[],o.O=(t,a,r,n)=>{if(!a){var l=1/0;for(c=0;c<e.length;c++){for(var[a,r,n]=e[c],m=!0,s=0;s<a.length;s++)(!1&n||l>=n)&&Object.keys(o.O).every((e=>o.O[e](a[s])))?a.splice(s--,1):(m=!1,n<l&&(l=n));if(m){e.splice(c--,1);var i=r();void 0!==i&&(t=i)}}return t}n=n||0;for(var c=e.length;c>0&&e[c-1][2]>n;c--)e[c]=e[c-1];e[c]=[a,r,n]},o.n=e=>{var t=e&&e.__esModule?()=>e.default:()=>e;return o.d(t,{a:t}),t},a=Object.getPrototypeOf?e=>Object.getPrototypeOf(e):e=>e.__proto__,o.t=function(e,r){if(1&r&&(e=this(e)),8&r)return e;if("object"==typeof e&&e){if(4&r&&e.__esModule)return e;if(16&r&&"function"==typeof e.then)return e}var n=Object.create(null);o.r(n);var l={};t=t||[null,a({}),a([]),a(a)];for(var m=2&r&&e;"object"==typeof m&&!~t.indexOf(m);m=a(m))Object.getOwnPropertyNames(m).forEach((t=>l[t]=()=>e[t]));return l.default=()=>e,o.d(n,l),n},o.d=(e,t)=>{for(var a in t)o.o(t,a)&&!o.o(e,a)&&Object.defineProperty(e,a,{enumerable:!0,get:t[a]})},o.g=function(){if("object"==typeof globalThis)return globalThis;try{return this||new Function("return this")()}catch(e){if("object"==typeof window)return window}}(),o.o=(e,t)=>Object.prototype.hasOwnProperty.call(e,t),o.r=e=>{"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},o.nmd=e=>(e.paths=[],e.children||(e.children=[]),e),(()=>{var e={378:0};o.O.j=t=>0===e[t];var t=(t,a)=>{var r,n,[l,m,s]=a,i=0;if(l.some((t=>0!==e[t]))){for(r in m)o.o(m,r)&&(o.m[r]=m[r]);if(s)var c=s(o)}for(t&&t(a);i<l.length;i++)n=l[i],o.o(e,n)&&e[n]&&e[n][0](),e[n]=0;return o.O(c)},a=self.webpackChunknode=self.webpackChunknode||[];a.forEach(t.bind(null,0)),a.push=t.bind(null,a.push.bind(a))})(),o.nc=void 0;var l=o.O(void 0,[736],(()=>o(2309)));l=o.O(l)})();
//# sourceMappingURL=user.bundle.7bbba753.js.map