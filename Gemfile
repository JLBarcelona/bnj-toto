source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 6.1.4', '>= 6.1.4.1'
gem 'pg', '~> 1.1'
gem 'puma', '~> 5.0'
gem 'net-smtp'
gem 'net-pop'
gem 'net-imap'
# gem 'rack-cors'

# authentication
gem 'bcrypt'
gem 'jwt'

# view
gem 'kaminari'

# upload
gem 'carrierwave'
gem 'rmagick'
gem 'fog'
gem 'fog-aws'
gem 'mini_magick'

# graphql
# gem 'rack-cors'
gem 'graphql', '1.12.16'
gem 'graphql_apollo_upload_client_params', git: 'https://github.com/github0013/graphql_apollo_upload_client_params.git'
gem 'parser'

# config
gem 'enum_help'
gem 'config'
gem 'dotenv-rails'

# db
gem 'seed-fu'
gem 'acts_as_list'

# aws
gem 'aws-sdk'
gem 'aws-sdk-rails'

# excel
gem 'caxlsx'
gem 'roo'

# google
gem "google-apis-sheets_v4"
gem "google-apis-drive_v3"

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'graphiql-rails'
  gem 'annotate'
  gem 'hirb', require: false
  gem 'hirb-unicode', require: false
  gem 'pry'
  gem 'pry-rails'
  gem 'pry-byebug'
  gem 'binding_of_caller'

  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-rbenv'
  gem 'capistrano-bundler'
  gem 'capistrano-npm'
  gem 'capistrano-sidekiq'
  gem 'capistrano3-puma'
  gem "ed25519", ">= 1.2", "< 2.0", require: false
  gem "bcrypt_pbkdf", ">= 1.0", "< 2.0", require: false
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
