/*eslint "no-console": "off"*/
// @ts-nocheck
import {ApolloClient, ApolloLink, ApolloProvider} from '@apollo/client';
import {InMemoryCache} from '@apollo/client/cache';
import {onError} from '@apollo/client/link/error';
import {createUploadLink} from 'apollo-upload-client';
import React from 'react';
import 'react-datepicker/dist/react-datepicker.css';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import {BrowserRouter} from 'react-router-dom';
import './globals.css';
import {AppContainer} from './pages';

const link = createUploadLink({
  uri: '/graphql',
  credentials: 'include',
});

const client = new ApolloClient({
  link: ApolloLink.from([
    onError(({graphQLErrors, networkError}) => {
      if (graphQLErrors)
        graphQLErrors.map(({message, locations, path}) => {
          const msg = `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`;
        });
      if (networkError) console.error(`[Network error]: ${networkError}`);
    }),
    link,
  ]),
  cache: new InMemoryCache(),
});

Modal.setAppElement('#app');

const App = () => (
  <ApolloProvider client={client}>
    <BrowserRouter>
      <AppContainer />
    </BrowserRouter>
  </ApolloProvider>
);

export default App;

ReactDOM.render(<App />, document.getElementById('app'));
