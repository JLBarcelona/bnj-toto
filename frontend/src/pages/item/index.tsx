import {Item} from '@/components/item/detail';
import {Items} from '@/components/item/list';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const ItemContainer = () => {
  return (
    <Layout>
      <Routes>
        <Route
          path=":id"
          element={
            <>
              <Items />
              <Item />
            </>
          }></Route>
        <Route index element={<Items />} />
      </Routes>
    </Layout>
  );
};

export {ItemContainer};
