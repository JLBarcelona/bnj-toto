import {Form} from '@/components/jkkUser/form';
import {NewForm} from '@/components/jkkUser/new/NewForm';
import {JkkUserList} from '@/components/jkkUser/users';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const JkkUserListPage = () => {
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<NewForm />} />
        <Route path=":id" element={<Form />} />
        <Route index element={<JkkUserList />} />
      </Routes>
    </Layout>
  );
};

export {JkkUserListPage};
