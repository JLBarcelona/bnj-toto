import {RoomLayoutContainer} from '@/components/roomLayout';
import Layout from '@/layouts/Admin';
import React from 'react';

const RoomLayoutPage = () => {
  return (
    <Layout>
      <RoomLayoutContainer />
    </Layout>
  );
};

export {RoomLayoutPage};
