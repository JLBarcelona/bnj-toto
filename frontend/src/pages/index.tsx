import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {BuildingPage} from './buildings';
import {CommentPage} from './comment';
import {CommentTypePage} from './commentType';
import {CompanyContainer} from './company';
import {CompanyTypePage} from './companyType';
import {DashboardPage} from './dashboard';
import {ItemContainer} from './item';
import {JkkUserListPage} from './jkkUser';
import {LoginPage} from './login';
import {OrderPage} from './order';
import {ProjectsPage} from './projects';
import {RoomLayoutPage} from './roomLayouts';
import {RoomTypesPage} from './roomTypes';
import {UserListPage} from './user';

const AppContainer = () => {
  return (
    <Routes>
      <Route path="/" element={<DashboardPage />} />
      <Route path="/login" element={<LoginPage />} />
      <Route path="/comments/*" element={<CommentPage />} />
      <Route path="/commenttypes/*" element={<CommentTypePage />} />
      <Route path="/buildings/*" element={<BuildingPage />} />
      <Route path="/roomtypes/*" element={<RoomTypesPage />} />

      <Route path="/roomlayouts/*" element={<RoomLayoutPage />} />

      <Route path="/projects/*" element={<ProjectsPage />} />

      <Route path="/orders/*" element={<OrderPage />} />

      <Route path="/items/*" element={<ItemContainer />} />

      <Route path="/companies/*" element={<CompanyContainer />} />

      <Route path="/companytypes/*" element={<CompanyTypePage />} />

      <Route path="/users/*" element={<UserListPage />} />

      <Route path="/jkk_users/*" element={<JkkUserListPage />} />
    </Routes>
  );
};

export {AppContainer};
