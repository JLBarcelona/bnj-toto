import {Project} from '@/components/project/project';
import {ProjectNew} from '@/components/project/project/new/New';
import {Projects} from '@/components/project/projects';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const ProjectsPage = () => {
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<ProjectNew />} />
        <Route path=":id/*" element={<Project />} />
        <Route index element={<Projects />} />
      </Routes>
    </Layout>
  );
};

export {ProjectsPage};
