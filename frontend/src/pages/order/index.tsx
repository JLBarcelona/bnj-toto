import {Order} from '@/components/order';
import Layout from '@/layouts/Admin';
import React from 'react';

const OrderPage = () => {
  return (
    <Layout>
      <Order />
    </Layout>
  );
};

export {OrderPage};
