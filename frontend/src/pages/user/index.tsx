import {Form} from '@/components/user/form';
import {PasswordForm} from '@/components/user/form/PasswordForm';
import {NewForm} from '@/components/user/new/NewForm';
import {UserList} from '@/components/user/users';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const UserListPage = () => {
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<NewForm />} />
        <Route path=":id/base" element={<Form />} />
        <Route path=":id/password" element={<PasswordForm />} />
        <Route index element={<UserList />} />
      </Routes>
    </Layout>
  );
};

export {UserListPage};
