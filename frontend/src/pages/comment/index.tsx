import {Comment} from '@/components/comment/base/Form';
import {CodeListContainer} from '@/components/comment/code/CodeListContainer';
import {Comments} from '@/components/comment/list';
import {CommentNew} from '@/components/comment/new/CommentNew';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const CommentPage = () => {
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<CommentNew />} />
        <Route path=":id/base" element={<Comment />} />
        <Route path=":id/codes" element={<CodeListContainer />} />
        <Route index element={<Comments />} />
      </Routes>
    </Layout>
  );
};

export {CommentPage};
