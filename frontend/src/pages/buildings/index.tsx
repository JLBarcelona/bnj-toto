import {Base} from '@/components/building/building/base/Base';
import {BuildingNumberContainer} from '@/components/building/building/buildingNumber';
import {Buildings} from '@/components/building/buildings';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const BuildingPage = () => {
  console.log('🚀 ~ file: index.tsx:19 ~ BuildingPage ~ BuildingPage:!!!!');
  return (
    <Layout>
      <Routes>
        <Route path=":id/building_numbers/*" element={<BuildingNumberContainer />} />
        <Route path=":id/base" element={<Base />} />
        <Route index element={<Buildings />} />
      </Routes>
    </Layout>
  );
};

export {BuildingPage};
