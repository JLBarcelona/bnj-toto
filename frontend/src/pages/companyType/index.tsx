import {CompanyTypeBaseForm} from '@/components/companyType/detail/BaseForm';
import {CompanyTypeNewForm} from '@/components/companyType/detail/new';
import {CompanyTypes} from '@/components/companyType/list';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const CompanyTypePage = () => {
  console.log('🚀 ~ file: index.tsx:19 ~ CompanyTypePage ~ CompanyTypePage :', CompanyTypePage);
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<CompanyTypeNewForm />} />
        <Route path=":id" element={<CompanyTypeBaseForm />} />
        <Route index element={<CompanyTypes />} />
      </Routes>
    </Layout>
  );
};

export {CompanyTypePage};
