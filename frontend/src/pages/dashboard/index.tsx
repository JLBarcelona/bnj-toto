import Layout from '@/layouts/Admin';
import React from 'react';

const DashboardPage = () => {
  return (
    <Layout>
      <div>Dashboard</div>
    </Layout>
  );
};

export {DashboardPage};
