import {RoomTypeContainer} from '@/components/roomType/roomType';
import {RoomTypeNewForm} from '@/components/roomType/roomType/NewForm';
import {RoomTypes} from '@/components/roomType/roomTypes';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router';

const RoomTypesPage = () => {
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<RoomTypeNewForm />} />
        <Route path=":id/*" element={<RoomTypeContainer />} />
        <Route index element={<RoomTypes />} />
      </Routes>
    </Layout>
  );
};

export {RoomTypesPage};
