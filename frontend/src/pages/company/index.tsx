import {Company} from '@/components/company/detail';
import {CompanyList} from '@/components/company/list';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const CompanyContainer = () => {
  return (
    <Layout>
      <Routes>
        <Route path=":id" element={<Company />} />
        <Route index element={<CompanyList />} />
      </Routes>
    </Layout>
  );
};

export {CompanyContainer};
