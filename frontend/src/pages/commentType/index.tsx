import {CommentType} from '@/components/commentType/base';
import {CommentListContainer} from '@/components/commentType/comments/CommentListContainer';
import {CommentTypes} from '@/components/commentType/list';
import {CommentTypeNewForm} from '@/components/commentType/new/CommentTypeNewForm';
import Layout from '@/layouts/Admin';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const CommentTypePage = () => {
  return (
    <Layout>
      <Routes>
        <Route path="new" element={<CommentTypeNewForm />} />
        <Route path=":id/base" element={<CommentType />} />
        <Route path=":id/comments" element={<CommentListContainer />} />
        <Route index element={<CommentTypes />} />
      </Routes>
    </Layout>
  );
};

export {CommentTypePage};
