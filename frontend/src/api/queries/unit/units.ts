import gql from 'graphql-tag';
import {unitFragment} from '../../fragments';

export default gql`
  query units {
    units {
      ...unitFragment
    }
  }
  ${unitFragment}
`;
