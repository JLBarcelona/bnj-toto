import gql from 'graphql-tag';
import {commentTypeFragment, commentFragment} from '../../fragments';

export default gql`
  query commentType($id: ID!) {
    commentType(id: $id) {
      ...commentTypeFragment
      comments {
        ...commentFragment
      }
    }
  }
  ${commentTypeFragment}
  ${commentFragment}
`;
