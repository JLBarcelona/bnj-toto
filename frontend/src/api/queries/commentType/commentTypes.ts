import gql from 'graphql-tag';
import {commentTypeFragment} from '../../fragments';

export default gql`
  query commentTypes {
    commentTypes {
      ...commentTypeFragment
    }
  }
  ${commentTypeFragment}
`;
