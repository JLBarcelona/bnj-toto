import gql from 'graphql-tag';
import {yearFragment} from '../../fragments';

export default gql`
  query years {
    years {
      ...yearFragment
    }
  }
  ${yearFragment}
`;
