import gql from 'graphql-tag';
import {materialCompanyFragment} from '../../fragments';

export default gql`
  query materialCompanies {
    materialCompanies {
      ...materialCompanyFragment
    }
  }
  ${materialCompanyFragment}
`;
