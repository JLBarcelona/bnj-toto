import gql from 'graphql-tag';
import {roomLayoutFragment} from '../../fragments';

export default gql`
  query roomLayouts {
    roomLayouts {
      ...roomLayoutFragment
    }
  }
  ${roomLayoutFragment}
`;
