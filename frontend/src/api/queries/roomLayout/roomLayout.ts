import {roomLayoutFragment} from '@/api/fragments';
import gql from 'graphql-tag';

export default gql`
  query roomLayout($id: ID!) {
    roomLayout(id: $id) {
      ...roomLayoutFragment
    }
  }
  ${roomLayoutFragment}
`;
