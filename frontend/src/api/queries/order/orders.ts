import gql from 'graphql-tag';
import {companyFragment, orderFragment, paginationFragment, projectFragment} from '../../fragments';

export default gql`
  query orders($page: Int, $perPage: Int, $search: OrderSearch) {
    orders(page: $page, perPage: $perPage, search: $search) {
      orders {
        ...orderFragment
        company {
          ...companyFragment
        }
        project {
          ...projectFragment
        }
      }
      pagination {
        ...paginationFragment
      }
    }
  }
  ${orderFragment}
  ${companyFragment}
  ${projectFragment}
  ${paginationFragment}
`;
