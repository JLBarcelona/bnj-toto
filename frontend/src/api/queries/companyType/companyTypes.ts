import gql from 'graphql-tag';
import {companyTypeFragment, companyFragment} from '../../fragments';

export default gql`
  query companyTypes {
    companyTypes {
      ...companyTypeFragment
      companies {
        ...companyFragment
      }
    }
  }
  ${companyTypeFragment}
  ${companyFragment}
`;
