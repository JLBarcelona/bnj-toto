import {companyTypeFragment} from '@/api/fragments';
import gql from 'graphql-tag';

export default gql`
  query companyType($id: ID!) {
    companyType(id: $id) {
      ...companyTypeFragment
    }
  }
  ${companyTypeFragment}
`;
