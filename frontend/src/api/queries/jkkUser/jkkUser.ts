import gql from 'graphql-tag';
import {jkkUserFragment} from '../../fragments';

export default gql`
  query jkkUser($id: ID!) {
    jkkUser(id: $id) {
      ...jkkUserFragment
    }
  }
  ${jkkUserFragment}
`;
