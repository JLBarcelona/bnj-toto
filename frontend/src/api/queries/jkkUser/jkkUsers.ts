import gql from 'graphql-tag';
import {jkkUserFragment} from '../../fragments';

export default gql`
  query jkkUsers($search: JkkUserSearch) {
    jkkUsers(search: $search) {
      ...jkkUserFragment
    }
  }
  ${jkkUserFragment}
`;
