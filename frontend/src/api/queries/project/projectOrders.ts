import gql from 'graphql-tag';
import {companyFragment, orderFragment, projectFragment} from '../../fragments';

export default gql`
  query projectOrders($id: ID!) {
    project(id: $id) {
      ...projectFragment
      ordersEach {
        ...orderFragment
        company {
          ...companyFragment
        }
      }
      ordersMonthly {
        ...orderFragment
        company {
          ...companyFragment
        }
      }
    }
  }
  ${projectFragment}
  ${orderFragment}
  ${companyFragment}
`;
