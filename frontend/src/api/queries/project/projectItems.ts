import gql from 'graphql-tag';
import {
  projectItemFragment,
  itemFragment,
  roomTypeFragment,
  companyFragment,
  userFragment,
  companyTypeFragment,
} from '../../fragments';

export default gql`
  query projectItems($id: ID!, $orderItem: String, $order: String) {
    projectItems(id: $id, orderItem: $orderItem, order: $order) {
      ...projectItemFragment
      item {
        ...itemFragment
        companyTypes {
          ...companyTypeFragment
        }
      }
      roomTypes {
        ...roomTypeFragment
      }
      company {
        ...companyFragment
        companyTypes {
          ...companyTypeFragment
        }
      }
      selectableCompanies {
        ...companyFragment
      }
      updatedItemsUser {
        ...userFragment
      }
    }
  }
  ${projectItemFragment}
  ${itemFragment}
  ${roomTypeFragment}
  ${companyFragment}
  ${userFragment}
  ${companyTypeFragment}
`;
