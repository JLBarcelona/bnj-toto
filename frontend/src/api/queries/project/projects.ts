import gql from 'graphql-tag';
import {
  projectFragment,
  paginationFragment,
  buildingFragment,
  buildingNumberFragment,
  roomFragment,
} from '../../fragments';

export default gql`
  query projects($page: Int, $perPage: Int, $search: ProjectSearch) {
    projects(page: $page, perPage: $perPage, search: $search) {
      projects {
        ...projectFragment
        building {
          ...buildingFragment
        }
        buildingNumber {
          ...buildingNumberFragment
        }
        room {
          ...roomFragment
        }
      }
      pagination {
        ...paginationFragment
      }
    }
  }
  ${projectFragment}
  ${paginationFragment}
  ${buildingFragment}
  ${buildingNumberFragment}
  ${roomFragment}
`;
