import gql from 'graphql-tag';
import {
  buildingFragment,
  buildingNumberFragment,
  buildingTypeFragment,
  commentFragment,
  imageCommentFragment,
  itemFragment,
  mcTypeFragment,
  projectFragment,
  projectImageFragment,
  projectItemFragment,
  projectReportFragment,
  purchaseOrderFragment,
  roomFragment,
  roomTypeFragment,
  userFragment,
  yearFragment,
  companyFragment,
} from '../../fragments';

export default gql`
  query project($id: ID!) {
    project(id: $id) {
      ...projectFragment
      year {
        ...yearFragment
      }
      projectItems {
        ...projectItemFragment
        item {
          ...itemFragment
        }
        roomTypes {
          ...roomTypeFragment
        }
        company {
          ...companyFragment
        }
      }
      purchaseOrders {
        ...purchaseOrderFragment
      }
      building {
        ...buildingFragment
        mcType {
          ...mcTypeFragment
        }
        buildingType {
          ...buildingTypeFragment
        }
      }
      buildingNumber {
        ...buildingNumberFragment
      }
      room {
        ...roomFragment
        roomTypes {
          ...roomTypeFragment
        }
      }
      projectImages {
        ...projectImageFragment
        imageComments {
          ...imageCommentFragment
        }
        comments {
          ...commentFragment
        }
      }
      projectReport {
        ...projectReportFragment
      }
      updatedItemsUser {
        ...userFragment
      }
      editUser {
        ...userFragment
      }
    }
  }
  ${projectFragment}
  ${yearFragment}
  ${mcTypeFragment}
  ${buildingTypeFragment}
  ${projectItemFragment}
  ${itemFragment}
  ${purchaseOrderFragment}
  ${buildingFragment}
  ${buildingNumberFragment}
  ${roomFragment}
  ${roomTypeFragment}
  ${projectImageFragment}
  ${imageCommentFragment}
  ${commentFragment}
  ${projectReportFragment}
  ${userFragment}
  ${companyFragment}
`;
