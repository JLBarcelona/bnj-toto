import gql from 'graphql-tag';
import {roomTypeFragment, itemCodeFragment} from '@/api/fragments';

export default gql`
  query roomType($id: ID!) {
    roomType(id: $id) {
      ...roomTypeFragment
    }
  }
  ${roomTypeFragment}
  ${itemCodeFragment}
`;
