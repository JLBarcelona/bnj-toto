import gql from 'graphql-tag';
import {roomTypeFragment} from '../../fragments';

export default gql`
  query roomTypes {
    roomTypes {
      ...roomTypeFragment
    }
  }
  ${roomTypeFragment}
`;
