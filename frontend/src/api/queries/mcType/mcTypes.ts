import gql from 'graphql-tag';
import {mcTypeFragment} from '../../fragments';

export default gql`
  query mcTypes {
    mcTypes {
      ...mcTypeFragment
    }
  }
  ${mcTypeFragment}
`;
