import gql from 'graphql-tag';
import {roomModelTypeFragment} from '../../fragments';

export default gql`
  query roomModelTypes {
    roomModelTypes {
      ...roomModelTypeFragment
    }
  }
  ${roomModelTypeFragment}
`;
