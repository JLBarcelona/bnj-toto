import {roomFragment, roomTypeFragment} from '@/api/fragments';
import gql from 'graphql-tag';

export default gql`
  query room($id: ID!) {
    room(id: $id) {
      ...roomFragment
      roomTypes {
        ...roomTypeFragment
      }
    }
  }
  ${roomFragment}
  ${roomTypeFragment}
`;
