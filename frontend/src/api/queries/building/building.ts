import gql from 'graphql-tag';
import {buildingFragment, buildingNumberFragment} from '@/api/fragments';

export default gql`
  query building($id: ID!) {
    building(id: $id) {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
      }
    }
  }
  ${buildingFragment}
  ${buildingNumberFragment}
`;
