import {buildingNumberFragment, roomFragment, roomTypeFragment} from '@/api/fragments';
import gql from 'graphql-tag';

export default gql`
  query buildingNumber($id: ID!) {
    buildingNumber(id: $id) {
      ...buildingNumberFragment
      rooms {
        ...roomFragment
        roomTypes {
          ...roomTypeFragment
        }
      }
    }
  }
  ${buildingNumberFragment}
  ${roomFragment}
  ${roomTypeFragment}
`;
