import gql from 'graphql-tag';
import {buildingFragment, paginationFragment} from '../../fragments';

export default gql`
  query buildings($page: Int, $perPage: Int, $search: BuildingSearch) {
    buildings(page: $page, perPage: $perPage, search: $search) {
      buildings {
        ...buildingFragment
      }
      pagination {
        ...paginationFragment
      }
    }
  }
  ${buildingFragment}
  ${paginationFragment}
`;
