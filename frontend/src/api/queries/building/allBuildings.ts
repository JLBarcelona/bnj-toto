import gql from 'graphql-tag';
import {buildingFragment, buildingNumberFragment, roomFragment} from '@/api/fragments';

export default gql`
  query allBuildings {
    allBuildings {
      ...buildingFragment
      buildingNumbers {
        ...buildingNumberFragment
        rooms {
          ...roomFragment
        }
      }
    }
  }
  ${buildingFragment}
  ${buildingNumberFragment}
  ${roomFragment}
`;
