import gql from 'graphql-tag';
import {buildingTypeFragment} from '../../fragments';

export default gql`
  query buildingTypes {
    buildingTypes {
      ...buildingTypeFragment
    }
  }
  ${buildingTypeFragment}
`;
