import gql from 'graphql-tag';
import {userFragment} from '../../fragments';

export default gql`
  query users($search: UserSearch) {
    users(search: $search) {
      ...userFragment
    }
  }
  ${userFragment}
`;
