import gql from 'graphql-tag';
import {userFragment} from '../../fragments';

export default gql`
  query user($id: ID!) {
    user(id: $id) {
      ...userFragment
    }
  }
  ${userFragment}
`;
