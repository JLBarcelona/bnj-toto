import gql from 'graphql-tag';
import {userFragment} from '../../fragments';

export default gql`
  query currentUser {
    currentUser {
      ...userFragment
    }
  }
  ${userFragment}
`;
