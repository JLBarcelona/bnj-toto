import gql from 'graphql-tag';
import {itemCodeFragment, itemFragment, unitFragment, yearFragment} from '../../fragments';

export default gql`
  query item($id: ID!) {
    item(id: $id) {
      ...itemFragment
      itemCode {
        ...itemCodeFragment
      }
      year {
        ...yearFragment
      }
      unit {
        ...unitFragment
      }
    }
  }
  ${itemFragment}
  ${itemCodeFragment}
  ${yearFragment}
  ${unitFragment}
`;
