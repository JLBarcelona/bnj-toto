import gql from 'graphql-tag';
import {itemFragment, itemCodeFragment} from '../../fragments';

export default gql`
  query allItems($search: ItemSearch) {
    allItems(search: $search) {
      ...itemFragment
      itemCode {
        ...itemCodeFragment
      }
    }
  }
  ${itemFragment}
  ${itemCodeFragment}
`;
