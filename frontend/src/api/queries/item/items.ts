import gql from 'graphql-tag';
import {companyFragment, itemCodeFragment, itemFragment, paginationFragment, yearFragment} from '../../fragments';

export default gql`
  query items($page: Int, $perPage: Int, $search: ItemSearch) {
    items(page: $page, perPage: $perPage, search: $search) {
      items {
        ...itemFragment
        itemCode {
          ...itemCodeFragment
        }
        year {
          ...yearFragment
        }
        company {
          ...companyFragment
        }
      }
      pagination {
        ...paginationFragment
      }
    }
  }
  ${itemFragment}
  ${itemCodeFragment}
  ${yearFragment}
  ${companyFragment}
  ${paginationFragment}
`;
