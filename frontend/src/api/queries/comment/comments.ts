import gql from 'graphql-tag';
import {commentFragment, commentTypeFragment, itemCodeFragment} from '../../fragments';

export default gql`
  query comments($search: CommentSearch) {
    comments(search: $search) {
      ...commentFragment
      commentType {
        ...commentTypeFragment
      }
      itemCodes {
        ...itemCodeFragment
      }
    }
  }
  ${commentFragment}
  ${commentTypeFragment}
  ${itemCodeFragment}
`;
