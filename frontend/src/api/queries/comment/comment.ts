import gql from 'graphql-tag';
import {commentFragment, commentTypeFragment, itemCodeFragment} from '../../fragments';

export default gql`
  query comment($id: ID!) {
    comment(id: $id) {
      ...commentFragment
      itemCodes {
        ...itemCodeFragment
      }
      commentType {
        ...commentTypeFragment
      }
    }
  }
  ${commentFragment}
  ${commentTypeFragment}
  ${itemCodeFragment}
`;
