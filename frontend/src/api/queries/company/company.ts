import gql from 'graphql-tag';
import {companyFragment} from '../../fragments';

export default gql`
  query company($id: ID!) {
    company(id: $id) {
      ...companyFragment
    }
  }
  ${companyFragment}
`;
