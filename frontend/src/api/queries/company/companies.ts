import gql from 'graphql-tag';
import {companyFragment, companyTypeFragment} from '../../fragments';

export default gql`
  query companies {
    companies {
      ...companyFragment
      companyTypes {
        ...companyTypeFragment
      }
    }
  }
  ${companyFragment}
  ${companyTypeFragment}
`;
