import gql from 'graphql-tag';

export default gql`
  fragment projectItemFragment on ProjectItem {
    id
    projectId
    itemId
    companyId
    code
    name
    count
    unitPrice
    amount
    roomTypeIds
    sequenceNumber
    updatedItemsAt
  }
`;
