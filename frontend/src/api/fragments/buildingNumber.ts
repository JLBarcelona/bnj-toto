import gql from 'graphql-tag';

export default gql`
  fragment buildingNumberFragment on BuildingNumber {
    id
    buildingId
    name
    position
  }
`;
