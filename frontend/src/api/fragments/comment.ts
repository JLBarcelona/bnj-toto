import gql from 'graphql-tag';

export default gql`
  fragment commentFragment on Comment {
    id
    comment
    commentTypeId
    remark
  }
`;
