import gql from 'graphql-tag';

export default gql`
  fragment userFragment on User {
    id
    loginId
    email
    role
    firstName
    lastName
    tel
    active
    isAssesment
  }
`;
