import buildingFragment from './buiding';
import buildingNumberFragment from './buildingNumber';
import buildingTypeFragment from './buildingType';
import commentFragment from './comment';
import commentItemCodeFragment from './commentItemCode';
import commentTypeFragment from './commentType';
import companyFragment from './company';
import companyTypeFragment from './companyType';
import imageCommentFragment from './imageComment';
import itemFragment from './item';
import itemCodeFragment from './itemCode';
import jkkUserFragment from './jkkUser';
import mcTypeFragment from './mcType';
import orderFragment from './order';
import paginationFragment from './pagination';
import projectFragment from './project';
import projectImageFragment from './projectImage';
import projectItemFragment from './projectItem';
import projectReportFragment from './projectReport';
import purchaseOrderFragment from './purchaseOrder';
import roomFragment from './room';
import roomLayoutFragment from './roomLayout';
import roomModelTypeFragment from './roomModelType';
import roomTypeFragment from './roomType';
import unitFragment from './unit';
import userFragment from './user';
import yearFragment from './year';

export {
  userFragment,
  jkkUserFragment,
  projectFragment,
  projectItemFragment,
  projectImageFragment,
  projectReportFragment,
  purchaseOrderFragment,
  companyFragment,
  companyTypeFragment,
  itemFragment,
  itemCodeFragment,
  yearFragment,
  buildingFragment,
  buildingNumberFragment,
  roomFragment,
  roomTypeFragment,
  paginationFragment,
  commentFragment,
  commentItemCodeFragment,
  commentTypeFragment,
  imageCommentFragment,
  buildingTypeFragment,
  mcTypeFragment,
  orderFragment,
  roomLayoutFragment,
  roomModelTypeFragment,
  unitFragment,
};
