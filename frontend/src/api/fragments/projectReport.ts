import gql from 'graphql-tag';

export default gql`
  fragment projectReportFragment on ProjectReport {
    id
    projectId
    worksheetBase
    worksheetItems
    worksheetImages
    worksheetChecklist
    spreadsheetId1
    spreadsheetId2
    spreadsheetId3
    spreadsheetId4
    spreadsheetId5
    spreadsheetId6
    spreadsheetId7
    spreadsheetDatetime1
    spreadsheetDatetime2
    spreadsheetDatetime3
    spreadsheetDatetime4
    spreadsheetDatetime5
    spreadsheetDatetime6
    spreadsheetDatetime7
    spreadsheetCheckReport1
    spreadsheetCheckReport2
  }
`;
