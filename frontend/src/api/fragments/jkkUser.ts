import gql from 'graphql-tag';

export default gql`
  fragment jkkUserFragment on JkkUser {
    id
    email
    firstName
    lastName
    active
  }
`;
