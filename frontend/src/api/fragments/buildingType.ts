import gql from 'graphql-tag';

export default gql`
  fragment buildingTypeFragment on BuildingType {
    id
    name
  }
`;
