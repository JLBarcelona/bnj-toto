import gql from 'graphql-tag';

export default gql`
  fragment itemFragment on Item {
    id
    name
    shortName
    price
    orderPrice
    companyTypeIds
    companyId
    order
    unitId
  }
`;
