import gql from 'graphql-tag';

export default gql`
  fragment roomLayoutFragment on RoomLayout {
    id
    name
    price1
    price2
    position
  }
`;
