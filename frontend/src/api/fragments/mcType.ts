import gql from 'graphql-tag';

export default gql`
  fragment mcTypeFragment on McType {
    id
    name
  }
`;
