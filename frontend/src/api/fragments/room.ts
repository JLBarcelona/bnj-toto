import gql from 'graphql-tag';

export default gql`
  fragment roomFragment on Room {
    id
    buildingNumberId
    name
    position
    roomModelTypeId
    roomLayoutId
  }
`;
