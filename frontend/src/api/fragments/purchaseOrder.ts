import gql from 'graphql-tag';

export default gql`
  fragment purchaseOrderFragment on PurchaseOrder {
    id
    projectId
    fileUrl
  }
`;
