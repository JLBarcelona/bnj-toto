import gql from 'graphql-tag';

export default gql`
  fragment yearFragment on Year {
    id
    year
  }
`;
