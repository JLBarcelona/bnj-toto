import gql from 'graphql-tag';

export default gql`
  fragment projectImageFragment on ProjectImage {
    id
    projectId
    roomTypeId
    imageType
    imageBefore
    imageAfter
    image
    comment
    position
  }
`;
