import gql from 'graphql-tag';

export default gql`
  fragment itemCodeFragment on ItemCode {
    id
    code
    name
  }
`;
