import gql from 'graphql-tag';

export default gql`
  fragment orderFragment on Order {
    id
    projectId
    companyId
    orderType
    isInvoice
    orderDate
    totalPrice
    exportCsvDate
  }
`;
