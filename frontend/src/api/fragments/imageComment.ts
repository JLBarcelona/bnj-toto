import gql from 'graphql-tag';

export default gql`
  fragment imageCommentFragment on ImageComment {
    id
    projectImageId
    code
    text
  }
`;
