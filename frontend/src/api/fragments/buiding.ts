import gql from 'graphql-tag';

export default gql`
  fragment buildingFragment on Building {
    id
    name
    address
    rank
    rankText
    mcTypeId
    buildingTypeId
  }
`;
