import gql from 'graphql-tag';

export default gql`
  fragment commentTypeFragment on CommentType {
    id
    name
    position
  }
`;
