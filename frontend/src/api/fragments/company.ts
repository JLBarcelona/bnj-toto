import gql from 'graphql-tag';

export default gql`
  fragment companyFragment on Company {
    id
    isOrderEach
    isOrderMonthly
    name
    zipcode
    address
    tel
    fax
    companyTypeIds
  }
`;
