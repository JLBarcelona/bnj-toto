import gql from 'graphql-tag';

export default gql`
  fragment roomTypeFragment on RoomType {
    id
    name
    position
  }
`;
