import gql from 'graphql-tag';

export default gql`
  fragment roomModelTypeFragment on RoomModelType {
    id
    name
    position
  }
`;
