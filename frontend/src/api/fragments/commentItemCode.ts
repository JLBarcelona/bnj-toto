import gql from 'graphql-tag';

export default gql`
  fragment commentItemCodeFragment on CommentItemCode {
    id
    commentId
    itemCodeId
    position
  }
`;
