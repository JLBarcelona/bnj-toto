import gql from 'graphql-tag';

export default gql`
  fragment companyTypeFragment on CompanyType {
    id
    name
    position
  }
`;
