import gql from 'graphql-tag';
import {jkkUserFragment} from '../../fragments';

export default gql`
  mutation createJkkUser($attributes: JkkUserAttributes!) {
    createJkkUser(input: {attributes: $attributes}) {
      jkkUser {
        ...jkkUserFragment
      }
      error
    }
  }
  ${jkkUserFragment}
`;
