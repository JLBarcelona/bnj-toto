import gql from 'graphql-tag';
import {jkkUserFragment} from '../../fragments';

export default gql`
  mutation updateJkkUser($id: ID!, $attributes: JkkUserAttributes!) {
    updateJkkUser(input: {id: $id, attributes: $attributes}) {
      jkkUser {
        ...jkkUserFragment
      }
      error
    }
  }
  ${jkkUserFragment}
`;
