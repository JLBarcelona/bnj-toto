import gql from 'graphql-tag';
import {buildingNumberFragment, roomFragment} from '../../fragments';

export default gql`
  mutation copyRoom($id: ID!, $attributes: RoomAttributes!) {
    copyRoom(input: {id: $id, attributes: $attributes}) {
      buildingNumber {
        ...buildingNumberFragment
        rooms {
          ...roomFragment
        }
      }
      error
    }
  }
  ${roomFragment}
  ${buildingNumberFragment}
`;
