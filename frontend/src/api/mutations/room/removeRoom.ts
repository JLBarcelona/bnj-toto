import gql from 'graphql-tag';
import {buildingNumberFragment, roomFragment, roomTypeFragment} from '../../fragments';

export default gql`
  mutation removeRoom($id: ID!) {
    removeRoom(input: {id: $id}) {
      buildingNumber {
        ...buildingNumberFragment
        rooms {
          ...roomFragment
          roomTypes {
            ...roomTypeFragment
          }
        }
      }
      error
    }
  }
  ${buildingNumberFragment}
  ${roomFragment}
  ${roomTypeFragment}
`;
