import gql from 'graphql-tag';
import {roomFragment, roomTypeFragment} from '../../fragments';

export default gql`
  mutation updateRoom($id: ID!, $attributes: RoomAttributes!, $roomTypeIds: [ID!]!) {
    updateRoom(input: {id: $id, attributes: $attributes, roomTypeIds: $roomTypeIds}) {
      room {
        ...roomFragment
        roomTypes {
          ...roomTypeFragment
        }
      }
      error
    }
  }
  ${roomFragment}
  ${roomTypeFragment}
`;
