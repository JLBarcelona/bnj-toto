import gql from 'graphql-tag';
import {buildingNumberFragment, roomFragment} from '@/api/fragments';

export default gql`
  mutation createRoom($buildingNumberId: ID!, $attributes: RoomAttributes!) {
    createRoom(input: {buildingNumberId: $buildingNumberId, attributes: $attributes}) {
      buildingNumber {
        ...buildingNumberFragment
        rooms {
          ...roomFragment
        }
      }
      error
    }
  }
  ${buildingNumberFragment}
  ${roomFragment}
`;
