import {companyTypeFragment} from '@/api/fragments';
import gql from 'graphql-tag';

export default gql`
  mutation createCompanyTypeModel($attributes: CompanyTypeAttributes!) {
    createCompanyTypeModel(input: {attributes: $attributes}) {
      companyType {
        ...companyTypeFragment
      }
      error
    }
  }
  ${companyTypeFragment}
`;
