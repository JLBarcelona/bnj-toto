import gql from 'graphql-tag';
import {companyTypeFragment} from '../../fragments';

export default gql`
  mutation updateCompanyTypeModel($id: ID!, $attributes: CompanyTypeAttributes!) {
    updateCompanyTypeModel(input: {id: $id, attributes: $attributes}) {
      companyType {
        ...companyTypeFragment
      }
      error
    }
  }
  ${companyTypeFragment}
`;
