import gql from 'graphql-tag';
import {companyTypeFragment} from '../../fragments';

export default gql`
  mutation updateCompanyTypeOrder($id: ID!, $attributes: CompanyTypeAttributes!) {
    updateCompanyTypeOrder(input: {id: $id, attributes: $attributes}) {
      companyTypes {
        ...companyTypeFragment
      }
      error
    }
  }
  ${companyTypeFragment}
`;
