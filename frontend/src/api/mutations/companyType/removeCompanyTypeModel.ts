import {companyTypeFragment} from '@/api/fragments';
import gql from 'graphql-tag';

export default gql`
  mutation removeCompanyTypeModel($id: ID!) {
    removeCompanyTypeModel(input: {id: $id}) {
      companyType {
        ...companyTypeFragment
      }
      error
    }
  }
  ${companyTypeFragment}
`;
