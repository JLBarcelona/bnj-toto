import gql from 'graphql-tag';
import {buildingFragment, buildingNumberFragment} from '../../fragments';

export default gql`
  mutation removeBuildingNumber($id: ID!) {
    removeBuildingNumber(input: {id: $id}) {
      building {
        ...buildingFragment
        buildingNumbers {
          ...buildingNumberFragment
        }
      }
      error
    }
  }
  ${buildingFragment}
  ${buildingNumberFragment}
`;
