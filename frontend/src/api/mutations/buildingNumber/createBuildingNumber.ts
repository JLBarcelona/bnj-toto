import gql from 'graphql-tag';
import {buildingFragment, buildingNumberFragment} from '@/api/fragments';

export default gql`
  mutation createBuildingNumber($id: ID!, $attributes: BuildingNumberAttributes!) {
    createBuildingNumber(input: {id: $id, attributes: $attributes}) {
      building {
        ...buildingFragment
        buildingNumbers {
          ...buildingNumberFragment
        }
      }
      error
    }
  }
  ${buildingFragment}
  ${buildingNumberFragment}
`;
