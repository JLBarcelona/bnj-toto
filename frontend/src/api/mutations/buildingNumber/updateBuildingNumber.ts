import gql from 'graphql-tag';
import {buildingFragment, buildingNumberFragment} from '@/api/fragments';

export default gql`
  mutation updateBuildingNumber($id: ID!, $attributes: BuildingNumberAttributes!) {
    updateBuildingNumber(input: {id: $id, attributes: $attributes}) {
      building {
        ...buildingFragment
        buildingNumbers {
          ...buildingNumberFragment
        }
      }
      error
    }
  }
  ${buildingFragment}
  ${buildingNumberFragment}
`;
