import gql from 'graphql-tag';
import {userFragment} from '../../fragments';

export default gql`
  mutation signin($loginId: String!, $password: String!) {
    signin(input: {loginId: $loginId, password: $password}) {
      user {
        ...userFragment
      }
      error
    }
  }
  ${userFragment}
`;
