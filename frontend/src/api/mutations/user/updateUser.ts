import gql from 'graphql-tag';
import {userFragment} from '../../fragments';

export default gql`
  mutation updateUser($id: ID!, $attributes: UserAttributes!) {
    updateUser(input: {id: $id, attributes: $attributes}) {
      user {
        ...userFragment
      }
      error
    }
  }
  ${userFragment}
`;
