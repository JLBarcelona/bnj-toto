import gql from 'graphql-tag';
import {userFragment} from '../../fragments';

export default gql`
  mutation createUser($attributes: UserAttributes!) {
    createUser(input: {attributes: $attributes}) {
      user {
        ...userFragment
      }
      error
    }
  }
  ${userFragment}
`;
