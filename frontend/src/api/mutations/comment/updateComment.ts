import gql from 'graphql-tag';
import {commentFragment} from '../../fragments';

export default gql`
  mutation updateComment($id: ID!, $attributes: CommentAttributes!) {
    updateComment(input: {id: $id, attributes: $attributes}) {
      comment {
        ...commentFragment
      }
      error
    }
  }
  ${commentFragment}
`;
