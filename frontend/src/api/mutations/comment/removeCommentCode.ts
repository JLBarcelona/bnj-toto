import gql from 'graphql-tag';
import {commentFragment, itemCodeFragment} from '../../fragments';

export default gql`
  mutation removeCommentCode($id: ID!, $itemCodeId: ID!) {
    removeCommentCode(input: {id: $id, itemCodeId: $itemCodeId}) {
      comment {
        ...commentFragment
        itemCodes {
          ...itemCodeFragment
        }
      }
      error
    }
  }
  ${commentFragment}
  ${itemCodeFragment}
`;
