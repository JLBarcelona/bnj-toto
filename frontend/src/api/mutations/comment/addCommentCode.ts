import gql from 'graphql-tag';
import {commentFragment, itemCodeFragment} from '../../fragments';

export default gql`
  mutation addCommentCode($id: ID!, $itemCodeId: ID!) {
    addCommentCode(input: {id: $id, itemCodeId: $itemCodeId}) {
      comment {
        ...commentFragment
        itemCodes {
          ...itemCodeFragment
        }
      }
      error
    }
  }
  ${commentFragment}
  ${itemCodeFragment}
`;
