import gql from 'graphql-tag';
import {commentFragment} from '../../fragments';

export default gql`
  mutation createComment($attributes: CommentAttributes!) {
    createComment(input: {attributes: $attributes}) {
      comment {
        ...commentFragment
      }
      error
    }
  }
  ${commentFragment}
`;
