import gql from 'graphql-tag';
import {commentFragment} from '../../fragments';

export default gql`
  mutation removeComment($id: ID!) {
    removeComment(input: {id: $id}) {
      comment {
        ...commentFragment
      }
      error
    }
  }
  ${commentFragment}
`;
