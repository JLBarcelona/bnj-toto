import gql from 'graphql-tag';
import {commentTypeFragment} from '../../fragments';

export default gql`
  mutation removeCommentTypeModel($id: ID!) {
    removeCommentTypeModel(input: {id: $id}) {
      commentType {
        ...commentTypeFragment
      }
      error
    }
  }
  ${commentTypeFragment}
`;
