import gql from 'graphql-tag';
import {commentTypeFragment, commentFragment} from '../../fragments';

export default gql`
  mutation updateCommentOrder($id: ID!, $position: Int!) {
    updateCommentOrder(input: {id: $id, position: $position}) {
      commentType {
        ...commentTypeFragment
        comments {
          ...commentFragment
        }
      }
      error
    }
  }
  ${commentTypeFragment}
  ${commentFragment}
`;
