import gql from 'graphql-tag';
import {commentTypeFragment} from '../../fragments';

export default gql`
  mutation updateCommentTypeModel($id: ID!, $attributes: CommentTypeAttributes!) {
    updateCommentTypeModel(input: {id: $id, attributes: $attributes}) {
      commentType {
        ...commentTypeFragment
      }
      error
    }
  }
  ${commentTypeFragment}
`;
