import gql from 'graphql-tag';
import {commentTypeFragment} from '../../fragments';

export default gql`
  mutation createCommentTypeModel($attributes: CommentTypeAttributes!) {
    createCommentTypeModel(input: {attributes: $attributes}) {
      commentType {
        ...commentTypeFragment
      }
      error
    }
  }
  ${commentTypeFragment}
`;
