import gql from 'graphql-tag';
import {projectImageFragment, commentFragment} from '../../fragments';

export default gql`
  mutation addComment($id: ID!, $commentId: ID!) {
    addComment(input: {id: $id, commentId: $commentId}) {
      projectImage {
        ...projectImageFragment
        comments {
          ...commentFragment
        }
      }
    }
  }
  ${projectImageFragment}
  ${commentFragment}
`;
