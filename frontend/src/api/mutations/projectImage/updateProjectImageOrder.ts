import gql from 'graphql-tag';
import {projectFragment, projectImageFragment} from '../../fragments';

export default gql`
  mutation updateProjectImageOrder($id: ID!, $attributes: ProjectImageAttributes!) {
    updateProjectImageOrder(input: {id: $id, attributes: $attributes}) {
      project {
        ...projectFragment
        projectImages {
          ...projectImageFragment
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectImageFragment}
`;
