import gql from 'graphql-tag';
import {projectImageFragment, commentFragment} from '../../fragments';

export default gql`
  mutation removeProjectImageComment($id: ID!, $commentId: ID!) {
    removeProjectImageComment(input: {id: $id, commentId: $commentId}) {
      projectImage {
        ...projectImageFragment
        comments {
          ...commentFragment
        }
      }
    }
  }
  ${projectImageFragment}
  ${commentFragment}
`;
