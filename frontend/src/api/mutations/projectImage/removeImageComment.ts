import gql from 'graphql-tag';
import {projectImageFragment, imageCommentFragment} from '../../fragments';

export default gql`
  mutation removeImageComment($id: ID!) {
    removeImageComment(input: {id: $id}) {
      projectImage {
        ...projectImageFragment
        imageComments {
          ...imageCommentFragment
        }
      }
    }
  }
  ${projectImageFragment}
  ${imageCommentFragment}
`;
