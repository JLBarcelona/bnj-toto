import gql from 'graphql-tag';
import {projectFragment, projectImageFragment} from '../../fragments';

export default gql`
  mutation removeProjectImage($id: ID!) {
    removeProjectImage(input: {id: $id}) {
      project {
        ...projectFragment
        projectImages {
          ...projectImageFragment
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectImageFragment}
`;
