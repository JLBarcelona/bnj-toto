import gql from 'graphql-tag';
import {projectImageFragment, imageCommentFragment} from '../../fragments';

export default gql`
  mutation removeImageCommentAll($id: ID!) {
    removeImageCommentAll(input: {id: $id}) {
      projectImage {
        ...projectImageFragment
        imageComments {
          ...imageCommentFragment
        }
      }
    }
  }
  ${projectImageFragment}
  ${imageCommentFragment}
`;
