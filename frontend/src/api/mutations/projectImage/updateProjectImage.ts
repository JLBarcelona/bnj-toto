import gql from 'graphql-tag';
import {projectFragment, projectImageFragment} from '../../fragments';

export default gql`
  mutation updateProjectImage($id: ID!, $attributes: ProjectImageAttributes!) {
    updateProjectImage(input: {id: $id, attributes: $attributes}) {
      project {
        ...projectFragment
        projectImages {
          ...projectImageFragment
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectImageFragment}
`;
