import gql from 'graphql-tag';
import {projectImageFragment, imageCommentFragment} from '../../fragments';

export default gql`
  mutation addImageCommentAll($id: ID!, $commentId: ID!) {
    addImageCommentAll(input: {id: $id, commentId: $commentId}) {
      projectImage {
        ...projectImageFragment
        imageComments {
          ...imageCommentFragment
        }
      }
    }
  }
  ${projectImageFragment}
  ${imageCommentFragment}
`;
