import gql from 'graphql-tag';
import {imageCommentFragment} from '../../fragments';

export default gql`
  mutation updateImageComment($id: ID!, $attributes: ImageCommentAttributes!) {
    updateImageComment(input: {id: $id, attributes: $attributes}) {
      imageComment {
        ...imageCommentFragment
      }
    }
  }
  ${imageCommentFragment}
`;
