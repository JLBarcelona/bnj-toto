import gql from 'graphql-tag';
import {projectFragment, projectImageFragment} from '../../fragments';

export default gql`
  mutation createProjectImage($count: Int!, $attributes: ProjectImageAttributes!) {
    createProjectImage(input: {count: $count, attributes: $attributes}) {
      project {
        ...projectFragment
        projectImages {
          ...projectImageFragment
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectImageFragment}
`;
