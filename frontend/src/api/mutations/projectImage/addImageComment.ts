import gql from 'graphql-tag';
import {projectImageFragment, imageCommentFragment} from '../../fragments';

export default gql`
  mutation addImageComment($id: ID!, $itemId: ID!) {
    addImageComment(input: {id: $id, itemId: $itemId}) {
      projectImage {
        ...projectImageFragment
        imageComments {
          ...imageCommentFragment
        }
      }
    }
  }
  ${projectImageFragment}
  ${imageCommentFragment}
`;
