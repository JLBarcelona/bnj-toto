import gql from 'graphql-tag';
import {roomTypeFragment} from '../../fragments';

export default gql`
  mutation updateRoomTypeModel($id: ID!, $attributes: RoomTypeAttributes!) {
    updateRoomTypeModel(input: {id: $id, attributes: $attributes}) {
      roomType {
        ...roomTypeFragment
      }
      error
    }
  }
  ${roomTypeFragment}
`;
