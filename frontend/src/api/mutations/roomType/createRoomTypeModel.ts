import gql from 'graphql-tag';
import {roomTypeFragment} from '../../fragments';

export default gql`
  mutation createRoomTypeModel($attributes: RoomTypeAttributes!) {
    createRoomTypeModel(input: {attributes: $attributes}) {
      roomType {
        ...roomTypeFragment
      }
      error
    }
  }
  ${roomTypeFragment}
`;
