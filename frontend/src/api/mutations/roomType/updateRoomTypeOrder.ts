import gql from 'graphql-tag';
import {roomTypeFragment} from '../../fragments';

export default gql`
  mutation updateRoomTypeOrder($id: ID!, $attributes: RoomTypeAttributes!) {
    updateRoomTypeOrder(input: {id: $id, attributes: $attributes}) {
      roomTypes {
        ...roomTypeFragment
      }
      error
    }
  }
  ${roomTypeFragment}
`;
