import gql from 'graphql-tag';
import {buildingFragment} from '../../fragments';

export default gql`
  mutation updateBuilding($id: ID!, $attributes: BuildingAttributes!) {
    updateBuilding(input: {id: $id, attributes: $attributes}) {
      building {
        ...buildingFragment
      }
      error
    }
  }
  ${buildingFragment}
`;
