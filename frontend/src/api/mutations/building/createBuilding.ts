import gql from 'graphql-tag';
import {buildingFragment} from '../../fragments';

export default gql`
  mutation createBuilding($attributes: BuildingAttributes!) {
    createBuilding(input: {attributes: $attributes}) {
      building {
        ...buildingFragment
      }
      error
    }
  }
  ${buildingFragment}
`;
