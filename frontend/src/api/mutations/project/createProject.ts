import gql from 'graphql-tag';
import {projectFragment} from '../../fragments';

export default gql`
  mutation createProject($attributes: ProjectAttributes!) {
    createProject(input: {attributes: $attributes}) {
      project {
        ...projectFragment
      }
      error
    }
  }
  ${projectFragment}
`;
