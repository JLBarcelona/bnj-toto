import gql from 'graphql-tag';
import {projectFragment, userFragment} from '../../fragments';

export default gql`
  mutation editProjectUser($id: ID!) {
    editProjectUser(input: {id: $id}) {
      project {
        ...projectFragment
        editUser {
          ...userFragment
        }
      }
    }
  }
  ${projectFragment}
  ${userFragment}
`;
