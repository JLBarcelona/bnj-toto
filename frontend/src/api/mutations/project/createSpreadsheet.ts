import gql from 'graphql-tag';
import {projectReportFragment} from '../../fragments';

export default gql`
  mutation createSpreadsheet($id: ID!, $reportType: String!, $attributes: ProjectReportAttributes!) {
    createSpreadsheet(input: {id: $id, reportType: $reportType, attributes: $attributes}) {
      projectReport {
        ...projectReportFragment
      }
      error
    }
  }
  ${projectReportFragment}
`;
