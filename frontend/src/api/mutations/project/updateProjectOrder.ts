import gql from 'graphql-tag';
import {orderFragment} from '../../fragments';

export default gql`
  mutation updateProjectOrder($id: ID!, $attributes: OrderAttributes!) {
    updateProjectOrder(input: {id: $id, attributes: $attributes}) {
      order {
        ...orderFragment
      }
    }
  }
  ${orderFragment}
`;
