import gql from 'graphql-tag';
import {projectReportFragment} from '../../fragments';

export default gql`
  mutation updateProjectReport($id: ID!, $attributes: ProjectReportAttributes!) {
    updateProjectReport(input: {id: $id, attributes: $attributes}) {
      projectReport {
        ...projectReportFragment
      }
      error
    }
  }
  ${projectReportFragment}
`;
