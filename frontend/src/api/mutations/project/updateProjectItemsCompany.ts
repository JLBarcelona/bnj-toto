import gql from 'graphql-tag';
import {projectFragment, projectItemFragment, itemFragment} from '../../fragments';

export default gql`
  mutation updateProjectItemsCompany($id: ID!, $companyTypeId: ID!, $companyId: ID!) {
    updateProjectItemsCompany(input: {id: $id, companyTypeId: $companyTypeId, companyId: $companyId}) {
      project {
        ...projectFragment
        projectItems {
          ...projectItemFragment
          item {
            ...itemFragment
          }
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectItemFragment}
  ${itemFragment}
`;
