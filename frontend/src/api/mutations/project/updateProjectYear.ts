import gql from 'graphql-tag';
import {projectFragment, yearFragment, projectItemFragment, itemFragment, purchaseOrderFragment} from '../../fragments';

export default gql`
  mutation updateProjectYear($id: ID!, $yearId: ID!) {
    updateProjectYear(input: {id: $id, yearId: $yearId}) {
      project {
        ...projectFragment
        year {
          ...yearFragment
        }
        projectItems {
          ...projectItemFragment
          item {
            ...itemFragment
          }
        }
        purchaseOrders {
          ...purchaseOrderFragment
        }
      }
      error
    }
  }
  ${projectFragment}
  ${yearFragment}
  ${projectItemFragment}
  ${itemFragment}
  ${purchaseOrderFragment}
`;
