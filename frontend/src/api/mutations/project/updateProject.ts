import gql from 'graphql-tag';
import {projectFragment} from '../../fragments';

export default gql`
  mutation updateProject($id: ID!, $attributes: ProjectAttributes!) {
    updateProject(input: {id: $id, attributes: $attributes}) {
      project {
        ...projectFragment
      }
      error
    }
  }
  ${projectFragment}
`;
