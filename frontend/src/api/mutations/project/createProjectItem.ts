import gql from 'graphql-tag';
import {projectFragment, projectItemFragment, itemFragment, roomTypeFragment, userFragment} from '../../fragments';

export default gql`
  mutation createProjectItem($id: ID!, $attributes: ProjectItemAttributes!) {
    createProjectItem(input: {id: $id, attributes: $attributes}) {
      project {
        ...projectFragment
        updatedItemsUser {
          ...userFragment
        }
        projectItems {
          ...projectItemFragment
          item {
            ...itemFragment
          }
          roomTypes {
            ...roomTypeFragment
          }
          updatedItemsUser {
            ...userFragment
          }
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectItemFragment}
  ${itemFragment}
  ${roomTypeFragment}
  ${userFragment}
`;
