import gql from 'graphql-tag';
import {projectFragment} from '../../fragments';

export default gql`
  mutation removeProject($id: ID!) {
    removeProject(input: {id: $id}) {
      result
      error
    }
  }
  ${projectFragment}
`;
