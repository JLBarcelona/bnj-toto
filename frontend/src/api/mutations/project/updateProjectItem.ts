import gql from 'graphql-tag';
import {projectFragment, projectItemFragment, itemFragment, purchaseOrderFragment, userFragment} from '../../fragments';

export default gql`
  mutation updateProjectItem($id: ID!, $attributes: ProjectItemAttributes!) {
    updateProjectItem(input: {id: $id, attributes: $attributes}) {
      project {
        ...projectFragment
        updatedItemsUser {
          ...userFragment
        }
        projectItems {
          ...projectItemFragment
          item {
            ...itemFragment
          }
          updatedItemsUser {
            ...userFragment
          }
        }
        purchaseOrders {
          ...purchaseOrderFragment
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectItemFragment}
  ${itemFragment}
  ${purchaseOrderFragment}
  ${userFragment}
`;
