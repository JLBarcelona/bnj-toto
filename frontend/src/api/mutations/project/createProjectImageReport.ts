import gql from 'graphql-tag';
import {projectFragment} from '../../fragments';

export default gql`
  mutation createProjectImageReport($id: ID!) {
    createProjectImageReport(input: {id: $id}) {
      project {
        ...projectFragment
      }
      error
    }
  }
  ${projectFragment}
`;
