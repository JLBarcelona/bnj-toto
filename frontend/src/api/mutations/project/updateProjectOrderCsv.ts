import gql from 'graphql-tag';
import {orderFragment} from '../../fragments';

export default gql`
  mutation updateProjectOrderCsv($id: ID!) {
    updateProjectOrderCsv(input: {id: $id}) {
      order {
        ...orderFragment
      }
    }
  }
  ${orderFragment}
`;
