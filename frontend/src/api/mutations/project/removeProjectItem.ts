import gql from 'graphql-tag';
import {projectFragment} from '../../fragments';

export default gql`
  mutation removeProjectItem($id: ID!) {
    removeProjectItem(input: {id: $id}) {
      project {
        ...projectFragment
      }
    }
  }
  ${projectFragment}
`;
