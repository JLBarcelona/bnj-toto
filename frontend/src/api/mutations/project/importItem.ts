import gql from 'graphql-tag';
import {projectFragment, projectItemFragment, itemFragment} from '../../fragments';

export default gql`
  mutation importItem($id: ID!, $file: File!) {
    importItem(input: {id: $id, file: $file}) {
      project {
        ...projectFragment
        projectItems {
          ...projectItemFragment
          item {
            ...itemFragment
          }
        }
      }
      error
    }
  }
  ${projectFragment}
  ${projectItemFragment}
  ${itemFragment}
`;
