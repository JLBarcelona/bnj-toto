import gql from 'graphql-tag';
import {projectFragment, userFragment} from '../../fragments';

export default gql`
  mutation clearProjectUser($id: ID!) {
    clearProjectUser(input: {id: $id}) {
      project {
        ...projectFragment
        editUser {
          ...userFragment
        }
      }
    }
  }
  ${projectFragment}
  ${userFragment}
`;
