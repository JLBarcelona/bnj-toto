import gql from 'graphql-tag';
import {roomLayoutFragment} from '../../fragments';

export default gql`
  mutation createRoomLayout($attributes: RoomLayoutAttributes!) {
    createRoomLayout(input: {attributes: $attributes}) {
      roomLayout {
        ...roomLayoutFragment
      }
      error
    }
  }
  ${roomLayoutFragment}
`;
