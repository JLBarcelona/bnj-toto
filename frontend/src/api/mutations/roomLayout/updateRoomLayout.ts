import gql from 'graphql-tag';
import {roomLayoutFragment} from '../../fragments';

export default gql`
  mutation updateRoomLayout($id: ID!, $attributes: RoomLayoutAttributes!) {
    updateRoomLayout(input: {id: $id, attributes: $attributes}) {
      roomLayout {
        ...roomLayoutFragment
      }
      error
    }
  }
  ${roomLayoutFragment}
`;
