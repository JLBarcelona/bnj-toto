import gql from 'graphql-tag';
import {roomLayoutFragment} from '../../fragments';

export default gql`
  mutation updateRoomLayoutOrder($id: ID!, $attributes: RoomLayoutAttributes!) {
    updateRoomLayoutOrder(input: {id: $id, attributes: $attributes}) {
      roomLayouts {
        ...roomLayoutFragment
      }
      error
    }
  }
  ${roomLayoutFragment}
`;
