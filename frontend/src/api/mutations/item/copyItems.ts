import gql from 'graphql-tag';
import {itemFragment} from '../../fragments';

export default gql`
  mutation copyItems {
    copyItems(input: {}) {
      error
    }
  }
  ${itemFragment}
`;
