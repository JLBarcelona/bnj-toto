import gql from 'graphql-tag';
import {itemFragment} from '../../fragments';

export default gql`
  mutation createItem($code: String!, $year: String!, $attributes: ItemAttributes!) {
    createItem(input: {code: $code, year: $year, attributes: $attributes}) {
      item {
        ...itemFragment
      }
      error
    }
  }
  ${itemFragment}
`;
