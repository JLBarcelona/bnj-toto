import gql from 'graphql-tag';
import {companyFragment, itemFragment} from '../../fragments';

export default gql`
  mutation updateItem($id: ID!, $attributes: ItemAttributes!) {
    updateItem(input: {id: $id, attributes: $attributes}) {
      item {
        ...itemFragment
        company {
          ...companyFragment
        }
      }
      error
    }
  }
  ${itemFragment}
  ${companyFragment}
`;
