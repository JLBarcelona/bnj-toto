import gql from 'graphql-tag';
import {companyFragment, companyTypeFragment} from '../../fragments';

export default gql`
  mutation updateCompany($id: ID!, $attributes: CompanyAttributes!) {
    updateCompany(input: {id: $id, attributes: $attributes}) {
      company {
        ...companyFragment
        companyTypes {
          ...companyTypeFragment
        }
      }
      error
    }
  }
  ${companyFragment}
  ${companyTypeFragment}
`;
