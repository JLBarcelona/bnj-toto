import gql from 'graphql-tag';

export default gql`
  mutation removeCompany($id: ID!) {
    removeCompany(input: {id: $id}) {
      result
      error
    }
  }
`;
