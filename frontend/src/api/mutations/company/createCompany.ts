import gql from 'graphql-tag';
import {companyFragment} from '../../fragments';

export default gql`
  mutation createCompany($attributes: CompanyAttributes!) {
    createCompany(input: {attributes: $attributes}) {
      company {
        ...companyFragment
      }
      error
    }
  }
  ${companyFragment}
`;
