import gql from 'graphql-tag';

export default gql`
  mutation outputOrder($spreadsheetId: String!, $search: OrderSearch!) {
    outputOrder(input: {spreadsheetId: $spreadsheetId, search: $search}) {
      result
    }
  }
`;
