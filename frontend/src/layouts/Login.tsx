import {useCurrentUserQuery} from '@/api/index';
import React, {useEffect} from 'react';
import {useNavigate} from 'react-router-dom';

interface Props {
  children: React.ReactNode;
}

const AdminLoginLayout: React.FC<Props> = ({children}) => {
  const navigate = useNavigate();
  const {data: {currentUser = null} = {}} = useCurrentUserQuery();
  console.log('🚀 ~ file: Login.tsx ~ line 17 ~ currentUser', currentUser);

  useEffect(() => {
    if (currentUser) {
      navigate('/');
    }
  }, [currentUser]);

  return <div className="flex flex-col justify-center min-h-screen py-12 bg-gray-50 sm:px-6 lg:px-8">{children}</div>;
};

export default AdminLoginLayout;
