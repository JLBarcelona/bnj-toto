import {useCurrentUserQuery, useSignoutMutation} from '@/api/index';
import {useApolloClient} from '@apollo/react-hooks';
import React, {useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';

interface Props {
  children: React.ReactNode;
}

const AdminLayout: React.FC<Props> = ({children}) => {
  console.log('layout');
  const client = useApolloClient();
  const navigate = useNavigate();

  const {data: {currentUser} = {}} = useCurrentUserQuery();
  const [signout] = useSignoutMutation();
  const handleSignout = () =>
    signout().then(() => {
      client.resetStore();
      navigate('/login');
    });

  useEffect(() => {
    if (currentUser === null) {
      navigate('/login');
    }
  }, [currentUser]);

  if (currentUser === undefined) return null;

  return (
    <div className="flex h-screen overflow-hidden bg-gray-100">
      <div className="flex flex-col pt-5 pb-4 bg-white w-[200px]">
        <div className="flex items-center flex-shrink-0 px-4">案件管理システム</div>
        <div className="flex-1 h-0 mt-5 overflow-y-auto">
          <nav className="px-4">
            <Link to="/projects">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/projects') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                案件管理
              </a>
            </Link>

            <Link to="/orders">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/orders') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                発注管理
              </a>
            </Link>

            <h3
              className="block px-3 mt-10 text-xs font-semibold tracking-wider text-gray-500 uppercase"
              id="projects-headline">
              マスター管理
            </h3>
            <Link to="/buildings">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/buildings') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                物件
              </a>
            </Link>

            <Link to="/items">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/items') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                項目
              </a>
            </Link>

            <Link to="/comments">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/comments') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                コメント
              </a>
            </Link>

            <Link to="/commenttypes">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/commenttypes') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                コメント種別
              </a>
            </Link>

            <Link to="/companies">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/companies') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                業者
              </a>
            </Link>

            <Link to="/companytypes">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/companytypes') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                業者種別
              </a>
            </Link>

            <Link to="/roomtypes">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/roomtypes') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                部屋種別
              </a>
            </Link>

            <Link to="/roomlayouts">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/roomlayouts') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                部屋レイアウト
              </a>
            </Link>

            <Link to="/users">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/users') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                従業員
              </a>
            </Link>

            <Link to="/jkk_users">
              <a
                className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md
								${location.pathname.startsWith('/jkk_users') ? 'bg-gray-100 text-gray-900' : ''}
							`}>
                公社担当者
              </a>
            </Link>

            <a
              href="#"
              className={`text-gray-600 hover:bg-gray-50 hover:text-gray-900 group flex items-center px-2 py-2 text-base font-medium rounded-md`}
              onClick={() => handleSignout()}>
              ログアウト
            </a>
          </nav>
        </div>
      </div>
      <div className="flex-1 px-4 pb-4 overflow-y-auto col-span-full">{children}</div>
    </div>
  );
};

export default AdminLayout;
