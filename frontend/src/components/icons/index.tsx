import CheckIcon from './svg/Check.svg';
import ChevronRightIcon from './svg/ChevronRightIcon.svg';
import DocumentDownloadIcon from './svg/DocumentArrowDownload.svg';
import EllipseVerticalIcon from './svg/EllipseVertical.svg';
import ExclamationIcon from './svg/Exclamation.svg';
import HomeIcon from './svg/Home.svg';
import MinusIcon from './svg/Minus.svg';
import PlusIcon from './svg/Plus.svg';
import TrashIcon from './svg/Trash.svg';

export {
  EllipseVerticalIcon,
  ChevronRightIcon,
  HomeIcon,
  TrashIcon,
  ExclamationIcon,
  DocumentDownloadIcon,
  MinusIcon,
  PlusIcon,
  CheckIcon,
};
