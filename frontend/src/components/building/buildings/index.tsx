import {useBuildingsQuery} from '@/api/index';
import {Button} from '@/components/ui';
import Pagination from '@/components/ui/Pagination';
import {useFormik} from 'formik';
import React, {useState} from 'react';
import {Link, useSearchParams} from 'react-router-dom';
import {BuildingForm} from '../building/new/BuildingForm';

const Buildings = () => {
  const [params, setParams] = useSearchParams();
  const [isOpen, setIsOpen] = useState(false);

  const page = params.get('page') ? Number(params.get('page')) : 1;
  const keyword = params.get('keyword');
  const {data: {buildings: {buildings = [], pagination = {}} = {}} = {}} = useBuildingsQuery({
    variables: {page, search: {keyword}},
    fetchPolicy: 'cache-and-network',
  });

  const formik = useFormik({
    initialValues: {keyword},
    onSubmit: (values) => {
      setParams({keyword: values.keyword, page: '1'});
    },
  });

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">物件管理</h1>

          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={() => setIsOpen(true)}>
            新規作成
          </Button>

          <BuildingForm isOpen={isOpen} onClose={() => setIsOpen(false)} />

          <form className="px-10 py-2 mb-4 bg-white" onSubmit={formik.handleSubmit}>
            <div className="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6">
              <div className="sm:col-span-4">
                <label className="block text-sm font-medium text-gray-700">キーワード</label>
                <div className="mt-1">
                  <input
                    name="keyword"
                    type="text"
                    className="block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.keyword}
                    onChange={formik.handleChange}
                    onBlur={() => formik.handleSubmit()}
                  />
                </div>
              </div>
            </div>
          </form>

          <Pagination
            totalCount={pagination.totalCount}
            currentPage={pagination.currentPage}
            totalPages={pagination.totalPages}
            pageSize={20}
          />
          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="w-12 px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  ID
                </th>
                <th scope="col" className="w-72 px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  名称
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  住所
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  ランク
                </th>
                <th />
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {buildings.map((building) => (
                <tr className="bg-white" key={building.id}>
                  <td className="ml-10">
                    <div className="px-2 cursor-pointer">{building.id}</div>
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
                    <Link to={`/buildings/${building.id}/base${location.search}`}>{building.name}</Link>
                  </td>
                  <td className="ml-10">
                    <div className="px-2">{building.address}</div>
                  </td>
                  <td className="ml-10">
                    <div className="px-2">{building.rankText}</div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {Buildings};
