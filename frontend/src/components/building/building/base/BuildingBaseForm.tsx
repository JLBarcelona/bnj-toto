import {Building, BuildingType, McType, useUpdateBuildingMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useParams} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';
import {BreadCrumb} from '../BreadCrumb';
import {Tab} from '../Tab';

type Props = {
  building: Building;
  buildingTypes: BuildingType[];
  mcTypes: McType[];
};
const BuildingBaseForm = (props: Props) => {
  const {id} = useParams<{id: string}>();
  const [udpateBuilding, {loading, data}] = useUpdateBuildingMutation({
    onCompleted: () => {
      toast.success('保存しました');
    },
  });

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<Building>({
    defaultValues: {
      name: props.building.name,
      buildingTypeId: props.building.buildingTypeId,
      mcTypeId: props.building.mcTypeId,
      address: props.building.address,
      rank: props.building.rank,
    },
  });

  const onSubmit = (data) => {
    udpateBuilding({variables: {id, attributes: data}});
  };

  return (
    <div className="w-screen max-w-xl">
      <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          {props.building && <BreadCrumb building={props.building} />}

          <Tab />

          {data?.updateBuilding.error && <p className="text-red-500">{data.updateBuilding.error}</p>}

          <div className="mt-10 md:col-span-2">
            <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
              <div>
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  名称
                </label>
                <input
                  type="text"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('name', {required: true})}
                />
              </div>

              <div>
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  住所
                </label>
                <input
                  type="text"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('address')}
                />
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">建物区分</label>
                <select
                  {...register('buildingTypeId')}
                  className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
                  <option value=""></option>
                  {props.buildingTypes.map((buildingType) => (
                    <option value={buildingType.id} key={`buildingType-${buildingType.id}`}>
                      {buildingType.name}
                    </option>
                  ))}
                </select>
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">管轄MC</label>
                <select
                  {...register('mcTypeId')}
                  className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
                  <option value=""></option>
                  {props.mcTypes.map((mcType) => (
                    <option value={mcType.id} key={`mcType-${mcType.id}`}>
                      {mcType.name}
                    </option>
                  ))}
                </select>
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">ランク</label>
                <select
                  {...register('rank')}
                  className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
                  <option value=""></option>
                  <option value="a">A</option>
                  <option value="b">B</option>
                  <option value="c">C</option>
                  <option value="d">D</option>
                </select>
              </div>

              <div className="py-3 text-right">
                <Button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  disabled={!isDirty || !isValid}
                  loading={loading}>
                  保存
                </Button>
                <Toaster position="top-right" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export {BuildingBaseForm};
