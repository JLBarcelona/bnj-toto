import {useBuildingQuery, useBuildingTypesQuery, useMcTypesQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {BuildingBaseForm} from './BuildingBaseForm';

const Base = () => {
  const {id} = useParams<{id: string}>();
  const {data: {building = null} = {}} = useBuildingQuery({variables: {id}});
  const {data: {buildingTypes = []} = {}} = useBuildingTypesQuery();
  const {data: {mcTypes = []} = {}} = useMcTypesQuery();

  if (!building || !buildingTypes || !mcTypes) return null;
  return <BuildingBaseForm building={building} buildingTypes={buildingTypes} mcTypes={mcTypes} />;
};

export {Base};
