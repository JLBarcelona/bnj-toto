import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {Base} from './base/Base';

const BuildingContainer = () => {
  return (
    <Routes>
      <Route path="/login">
        <Base />
      </Route>
    </Routes>
  );
};

export {BuildingContainer};
