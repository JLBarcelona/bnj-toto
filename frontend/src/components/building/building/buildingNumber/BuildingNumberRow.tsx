import {BuildingNumber} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {Link, useHistory, useParams} from 'react-router-dom';

type Props = {
  buildingNumber: BuildingNumber;
  setBuildingNumber: (buildingNumber: BuildingNumber | null) => void;
};
const BuildingNumberRow = (props: Props) => {
  const history = useHistory();
  const {id, buidingNumberId} = useParams<{id: string; buidingNumberId: string}>();

  const handleClickBuildingNumber = (buildingNumber: BuildingNumber) => {
    props.setBuildingNumber(buildingNumber);
  };
  const active = (buildingNumber: BuildingNumber) => buildingNumber.id === buidingNumberId;

  return (
    <tr key={props.buildingNumber.id} className={active(props.buildingNumber) ? 'bg-blue-100' : ''}>
      <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
        <Link
          to={`/buildings/${id}/building_numbers/${props.buildingNumber.id}`}
          className="hover:underline focus:outline-none">
          {props.buildingNumber.name}
        </Link>
      </td>
      <td className="px-6 py-4 text-xs font-medium text-right whitespace-nowrap">
        <Button
          type="button"
          className="block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100"
          onClick={() => handleClickBuildingNumber(props.buildingNumber)}>
          編集
        </Button>
      </td>
    </tr>
  );
};

export {BuildingNumberRow};
