import {useRoomLayoutsQuery, useRoomModelTypesQuery, useRoomQuery, useRoomTypesQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {Form} from './Form';

const RoomForm = () => {
  const {roomId} = useParams<{id: string; buidingNumberId: string; roomId: string}>();
  const {data: {room = null} = {}} = useRoomQuery({variables: {id: roomId}});
  const {data: {roomTypes = null} = {}} = useRoomTypesQuery();
  const {data: {roomLayouts = null} = {}} = useRoomLayoutsQuery();
  const {data: {roomModelTypes = null} = {}} = useRoomModelTypesQuery();

  if (!room || !roomModelTypes || !roomTypes || !roomLayouts) return null;

  return <Form room={room} roomTypes={roomTypes} roomLayouts={roomLayouts} roomModelTypes={roomModelTypes} />;
};

export {RoomForm};
