import React from 'react';
import {useFormik} from 'formik';
import ReactModal from 'react-modal';
import toast, {Toaster} from 'react-hot-toast';
import {BuildingNumber, useCreateRoomMutation} from '@/api/index';
import {Button} from '@/components/ui';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
  buildingNumber?: BuildingNumber;
};

const customStyles = {
  content: {
    top: '30%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    height: '300px',
  },
};
const RoomNewForm = ({buildingNumber, isOpen, handleClose}: Props) => {
  const [createRoom, {loading}] = useCreateRoomMutation({
    onCompleted: () => {
      toast.success('保存しました');
    },
  });

  const formik = useFormik({
    initialValues: {
      name: '',
    },
    onSubmit: (values) => {
      createRoom({variables: {buildingNumberId: buildingNumber.id, attributes: values}});
      handleClose();
      formik.resetForm();
    },
  });

  return (
    <div className="w-screen max-w-xl">
      <ReactModal isOpen={isOpen} onRequestClose={handleClose} style={customStyles}>
        <h1 className="text-xl">号室</h1>

        <form className="p-6 space-y-6" onSubmit={formik.handleSubmit}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              name="name"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </div>

          <div className="py-3 text-right">
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              loading={loading}>
              保存
            </Button>
            <Toaster position="top-right" />
          </div>
        </form>
      </ReactModal>
    </div>
  );
};

export {RoomNewForm};
