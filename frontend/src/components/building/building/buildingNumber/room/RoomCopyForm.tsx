import {Room, useCopyRoomMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import ReactModal from 'react-modal';

type Props = {
  id: string;
  isOpen: boolean;
  handleClose: () => void;
};

const customStyles = {
  content: {
    top: '30%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    height: '300px',
  },
};
const RoomCopyForm = ({id, isOpen, handleClose}: Props) => {
  useCopyRoomMutation;
  const [copyRoom, {loading, data}] = useCopyRoomMutation({
    onCompleted: (data) => {
      if (!data.copyRoom.error) {
        handleClose();
      }
    },
  });

  const {
    register,
    handleSubmit,
    formState: {errors, isDirty, isValid},
  } = useForm<{name: string}>({mode: 'all', defaultValues: {name: ''}});

  const onSubmit = (values: Room) => {
    copyRoom({variables: {id, attributes: values}});
  };

  return (
    <div className="w-screen max-w-xl">
      <ReactModal isOpen={isOpen} onRequestClose={handleClose} style={customStyles}>
        <h1 className="text-xl">部屋コピー</h1>
        {data?.copyRoom.error && <p className="text-red-500">{data?.copyRoom.error}</p>}

        <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('name', {required: true})}
            />
            {errors.name?.type === 'required' && <p className="text-red-400">名称は必須項目です</p>}
          </div>

          <div className="py-3 text-right">
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              disabled={!isDirty || !isValid}
              loading={loading}>
              保存
            </Button>
          </div>
        </form>
      </ReactModal>
    </div>
  );
};

export {RoomCopyForm};
