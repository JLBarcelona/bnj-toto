import {Room, useRemoveRoomMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React, {useMemo, useState} from 'react';
import {Link, useParams} from 'react-router-dom';
import {RoomCopyForm} from './RoomCopyForm';

type Props = {
  room: Room;
};
const RoomRow = (props: Props) => {
  const {id, roomId} = useParams<{id: string; buidingNumberId: string; roomId: string}>();
  const [removeRoom] = useRemoveRoomMutation({
    onCompleted: (data) => {
      if (data.removeRoom.error) {
        alert(data.removeRoom.error);
      }
    },
  });

  const [copyRoomId, setCopyRoomId] = useState(null);
  const active = useMemo(() => roomId === props.room.id, [roomId, props.room.id]);
  const handleRemoveRoom = () => {
    if (confirm('本当に削除しますか？')) removeRoom({variables: {id: props.room.id}});
  };

  return (
    <>
      <tr key={props.room.id} className={active ? 'bg-blue-100' : ''}>
        <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
          <Link
            to={`/buildings/${id}/building_numbers/${props.room.buildingNumberId}/rooms/${props.room.id}`}
            className="hover:underline focus:outline-none">
            {props.room.name}
          </Link>
        </td>
        <td className="px-6 py-4 text-xs font-medium text-gray-900 flex items-center justify-center space-x-2">
          <Button
            type="button"
            className="block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100"
            onClick={() => setCopyRoomId(props.room.id)}>
            コピー
          </Button>
          <Button
            type="button"
            className="block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100"
            onClick={() => handleRemoveRoom()}>
            削除
          </Button>
        </td>
      </tr>

      {copyRoomId && <RoomCopyForm id={copyRoomId} isOpen={!!copyRoomId} handleClose={() => setCopyRoomId(null)} />}
    </>
  );
};

export {RoomRow};
