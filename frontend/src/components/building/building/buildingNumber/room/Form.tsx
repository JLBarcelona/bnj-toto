import {Room, RoomLayout, RoomModelType, RoomType, useUpdateRoomMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React, {useCallback, useEffect} from 'react';
import {useForm} from 'react-hook-form';
import Select from 'react-select';
import toast, {Toaster} from 'react-hot-toast';

type Props = {
  room: Room;
  roomTypes: RoomType[];
  roomLayouts: RoomLayout[];
  roomModelTypes: RoomModelType[];
};
type RoomProps = {
  name: string;
  roomModelTypeId: string;
  roomLayoutId: string;
  roomTypeIds: string[];
};
const Form = (props: Props) => {
  const options = props.roomTypes.map((roomType) => ({
    value: roomType.id,
    label: roomType.name,
  }));

  const [updateRoom, {loading}] = useUpdateRoomMutation({
    onCompleted: () => {
      toast.success('保存しました');
    },
  });

  const {
    register,
    handleSubmit,
    setValue,
    watch,
    formState: {isDirty, isValid},
  } = useForm<RoomProps>({
    defaultValues: {
      name: props.room.name,
      roomModelTypeId: props.room.roomModelTypeId,
      roomLayoutId: props.room.roomLayoutId,
      roomTypeIds: props.room.roomTypes?.map((roomType) => roomType.id),
    },
  });
  const onSubmit = (data) => {
    const values = data;
    const roomTypeIds = values.roomTypeIds;
    delete values.roomTypeIds;
    updateRoom({variables: {id: props.room.id, attributes: values, roomTypeIds}});
  };
  const roomTypeIds = watch('roomTypeIds');

  const handleSelect = (selectOptions: any) => {
    const ids = selectOptions.map((op) => op.value);
    setValue('roomTypeIds', ids, {shouldDirty: true});
  };

  const selectOptions = useCallback(
    () => options.filter((option) => roomTypeIds?.includes(option.value)),
    [roomTypeIds],
  );

  useEffect(() => {
    setValue('name', props.room.name);
    setValue('roomModelTypeId', props.room.roomModelTypeId);
    setValue('roomLayoutId', props.room.roomLayoutId);
    setValue(
      'roomTypeIds',
      props.room.roomTypes?.map((roomType) => roomType.id),
    );
  }, [props.room.id]);

  return (
    <form className="flex-1 px-4 py-2 ml-4 border border-gray-300" onSubmit={handleSubmit(onSubmit)}>
      <div className="divide-y divide-gray-200">
        <h3 className="text-lg font-medium leading-6 text-gray-900">{props.room.name}</h3>

        <div className="pt-4">
          <div className="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6">
            <div className="sm:col-span-6">
              <label htmlFor="roomType" className="block text-sm font-medium text-gray-700">
                部屋種別
              </label>
              <Select
                options={options}
                isMulti
                isClearable
                closeMenuOnSelect={false}
                value={selectOptions()}
                onChange={(data) => handleSelect(data)}
              />
            </div>
          </div>

          <div>
            <label className="block text-sm font-medium text-gray-700">建物形式</label>
            <select
              {...register('roomModelTypeId')}
              className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
              <option value=""></option>
              {props.roomModelTypes.map((roomModelType) => (
                <option value={roomModelType.id} key={`roomModelTypeId-${roomModelType.id}`}>
                  {roomModelType.name}
                </option>
              ))}
            </select>
          </div>

          <div>
            <label className="block text-sm font-medium text-gray-700">間取図レイアウトタイプ</label>
            <select
              {...register('roomLayoutId')}
              className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
              <option value=""></option>
              {props.roomLayouts.map((roomLayout) => (
                <option value={roomLayout.id} key={`roomLayoutId-${roomLayout.id}`}>
                  {roomLayout.name}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>

      <div className="pt-5">
        <div className="flex justify-end">
          <Button
            type="submit"
            className="inline-flex justify-center px-4 py-2 ml-3 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm disabled:opacity-50"
            disabled={!isDirty || loading || !isValid}
            loading={loading}>
            保存
          </Button>
          <Toaster position="top-right" />
        </div>
      </div>
    </form>
  );
};

export {Form};
