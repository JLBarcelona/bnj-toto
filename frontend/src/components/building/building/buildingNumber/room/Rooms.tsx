import {useBuildingNumberQuery} from '@/api/index';
import {Button} from '@/components/ui';
import React, {useState} from 'react';
import {useParams} from 'react-router-dom';
import {RoomForm} from './RoomForm';
import {RoomNewForm} from './RoomNewForm';
import {RoomRow} from './RoomRow';

const Rooms = () => {
  const {buidingNumberId} = useParams<{id: string; buidingNumberId: string; roomId: string}>();
  const {data: {buildingNumber = null} = {}} = useBuildingNumberQuery({variables: {id: buidingNumberId}});

  const [open, setOpen] = useState(false);

  if (!buildingNumber) return null;

  return (
    <>
      <div className="w-64 ml-4 border border-gray-300 rounded-lg shadow">
        <table className="w-full p-0 divide-y divide-gray-200">
          <thead className="h-5 bg-gray-100">
            <tr>
              <th scope="col" className="flex px-6 py-1 text-xs font-medium leading-7 tracking-wider text-gray-500">
                号室
              </th>
              <th>
                <span>
                  <Button
                    className="inline-flex justify-center px-2 py-1 ml-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:col-start-1 sm:text-sm"
                    type="button"
                    onClick={() => {
                      setOpen(true);
                    }}>
                    追加
                  </Button>
                </span>
              </th>
            </tr>
          </thead>
          <tbody>
            {buildingNumber?.rooms?.map((room) => (
              <RoomRow key={room.id} room={room} />
            ))}
          </tbody>
        </table>
        <RoomNewForm buildingNumber={buildingNumber} isOpen={open} handleClose={() => setOpen(false)} />
      </div>

      <RoomForm />
    </>
  );
};

export {Rooms};
