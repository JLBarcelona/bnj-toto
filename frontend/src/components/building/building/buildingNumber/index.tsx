import {BuildingNumber} from '@/components/building/building/buildingNumber/BuildingNumber';
import React from 'react';
import {Route, Routes} from 'react-router-dom';

const BuildingNumberContainer = () => {
  return (
    <Routes>
      <Route path=":buidingNumberId/*" element={<BuildingNumber />} />
      <Route index element={<BuildingNumber />} />
    </Routes>
  );
};

export {BuildingNumberContainer};
