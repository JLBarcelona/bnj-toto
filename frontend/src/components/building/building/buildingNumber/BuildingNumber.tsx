import {BuildingNumber, useBuildingQuery, useRemoveBuildingNumberMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React, {useState} from 'react';
import {Link, Route, Routes, useParams} from 'react-router-dom';
import {BreadCrumb} from '../BreadCrumb';
import {Tab} from '../Tab';
import {BuildingNumberForm} from './BuildingNumberForm';
import {Rooms} from './room/Rooms';

const BuildingNumber = () => {
  console.log('🚀 ~ file: BuildingNumber.tsx:114 ~ BuildingNumber ~ BuildingNumber:##');
  const {id, buidingNumberId} = useParams<{id: string; buidingNumberId: string}>();
  const [open, setOpen] = useState(false);
  const [buildingNumber, setBuildingNumber] = useState(null);
  const {data: {building = null} = {}} = useBuildingQuery({variables: {id}});
  const [removeBuildingNumber] = useRemoveBuildingNumberMutation({
    onCompleted: (data) => {
      if (data.removeBuildingNumber.error) {
        alert(data.removeBuildingNumber.error);
      }
    },
  });

  const handleClickBuildingNumber = (buildingNumber: BuildingNumber) => {
    setBuildingNumber(buildingNumber);
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    setBuildingNumber(null);
  };

  const handleRemoveBuildingNumber = (buildingNumber: BuildingNumber) => {
    if (confirm('削除してよろしいですか？')) {
      removeBuildingNumber({variables: {id: buildingNumber.id}});
    }
  };

  const active = (buildingNumber: BuildingNumber) => buildingNumber.id === buidingNumberId;

  return (
    <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
      <div className="px-4 sm:px-6">
        {building && <BreadCrumb building={building} />}
        <Tab />

        <div className="mt-6">
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={() => setOpen(true)}>
            号棟追加
          </Button>
        </div>

        <BuildingNumberForm
          isOpen={open}
          handleClose={() => {
            handleClose();
          }}
          buildingNumber={buildingNumber}
        />

        <div className="flex mt-6">
          <div className="overflow-hidden border border-gray-300 shadow sm:rounded-lg">
            <table className="divide-y divide-gray-200">
              <thead className="bg-gray-100">
                <tr>
                  <th scope="col" className="px-6 py-1 text-xs font-medium leading-8 tracking-wider text-gray-500">
                    号棟
                  </th>
                  <th scope="col" className="relative px-6 py-3">
                    <span className="sr-only">Edit</span>
                  </th>
                </tr>
              </thead>
              <tbody>
                {building?.buildingNumbers?.map((buildingNumber) => (
                  <tr key={buildingNumber.id} className={active(buildingNumber) ? 'bg-blue-100' : ''}>
                    <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
                      <Link
                        to={`/buildings/${id}/building_numbers/${buildingNumber.id}`}
                        className="hover:underline focus:outline-none">
                        {buildingNumber.name}
                      </Link>
                    </td>
                    <td className="px-2 py-2 text-xs font-medium flex items-center">
                      <Button
                        type="button"
                        className="block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100"
                        onClick={() => handleClickBuildingNumber(buildingNumber)}>
                        編集
                      </Button>
                      <Button
                        type="button"
                        className="block text-gray-500 border border-gray-400 p-2 rounded-sm cursor-pointer hover:bg-gray-100"
                        onClick={() => handleRemoveBuildingNumber(buildingNumber)}>
                        削除
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <Routes>
            <Route path="rooms/:roomId/*" element={<Rooms />} />
            <Route index element={<Rooms />} />
          </Routes>
        </div>
      </div>
    </div>
  );
};

export {BuildingNumber};
