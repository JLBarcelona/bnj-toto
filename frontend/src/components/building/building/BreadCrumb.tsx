import {Building} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import React from 'react';
import {Link, useParams} from 'react-router-dom';

type Props = {
  building: Building;
};
const BreadCrumb = ({building}: Props) => {
  const {id} = useParams<{id: string}>();

  return (
    <nav className="flex" aria-label="Breadcrumb">
      <ol role="list" className="flex items-center space-x-4">
        <li>
          <div className="flex items-center">
            <Link to={'/buildings'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
              物件管理
            </Link>
          </div>
        </li>
        <li>
          <div className="flex items-center">
            <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
            <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">{building.name}</p>
          </div>
        </li>
      </ol>
    </nav>
  );
};

export {BreadCrumb};
