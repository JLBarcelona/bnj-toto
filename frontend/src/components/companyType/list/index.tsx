import {CompanyType, useCompanyTypesQuery, useUpdateCompanyTypeOrderMutation} from '@/api/index';
import update from 'immutability-helper';
import React, {useEffect, useState} from 'react';
import {DndProvider} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import {Link} from 'react-router-dom';
import {CompanyTypeRow} from './CompanyTypeRow';

const CompanyTypes = () => {
  const [tempCompanyTypes, setTempCompanyTypes] = useState([]);
  const {data: {companyTypes = []} = {}} = useCompanyTypesQuery({
    fetchPolicy: 'cache-and-network',
  });
  const [order] = useUpdateCompanyTypeOrderMutation();

  const moveItem = (id: string, dragIndex: number, hoverIndex: number) => {
    console.log(`update: id: ${id}`);
    console.log(`update: dragIndex: ${dragIndex}`);
    console.log(`update: hoverIndex: ${hoverIndex}`);
    // order({variables: {id, attributes: {position: dragIndex}}});
    setTempCompanyTypes((images: CompanyType[]) =>
      update(images, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, images[dragIndex] as CompanyType],
        ],
      }),
    );
  };

  const handleOrder = (id: string, index: number) => {
    console.log('🚀 ~ file: index.tsx:28 ~ handleOrder ~ id', id);
    console.log('🚀 ~ file: index.tsx:28 ~ handleOrder ~ index', index);
    order({variables: {id, attributes: {position: index}}});
  };

  useEffect(() => {
    setTempCompanyTypes(companyTypes);
  }, [companyTypes]);

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">業者種別</h1>

          <Link to="/companytypes/new">
            <div className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              新規作成
            </div>
          </Link>

          <div className="min-w-full mt-4 divide-y divide-gray-200">
            <div className="bg-gray-50">
              <div className="flex">
                <div className="px-6 py-3 text-xs font-medium text-left text-gray-500">名称</div>
              </div>
            </div>
            <DndProvider backend={HTML5Backend}>
              <div className="bg-white divide-y divide-gray-200">
                {tempCompanyTypes.map((companyType, i) => (
                  <CompanyTypeRow
                    id={companyType.id}
                    index={i}
                    companyType={companyType}
                    key={companyType.id}
                    moveItem={moveItem}
                    order={handleOrder}
                  />
                ))}
              </div>
            </DndProvider>
          </div>
        </div>
      </div>
    </div>
  );
};

export {CompanyTypes};
