import {CompanyType, useUpdateCompanyTypeModelMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate, useParams} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';

type Props = {
  companyType: CompanyType;
  onRemove: () => void;
};
const CompanyTypeForm = (props: Props) => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const [udpateCompanyTypeModel, {loading, data}] = useUpdateCompanyTypeModelMutation({
    onCompleted: (data) => {
      if (!data.updateCompanyTypeModel.error) {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/companytypes${location.search}`);
        }, 500);
      }
    },
  });

  const {
    register,
    handleSubmit,
    formState: {errors, isDirty, isValid},
  } = useForm<CompanyType>({mode: 'all', defaultValues: {name: props.companyType.name}});

  const onSubmit = (data: CompanyType) => {
    udpateCompanyTypeModel({variables: {id, attributes: data}});
  };

  return (
    <div className="mt-10 md:col-span-2 w-screenmax-w-xl">
      {data?.updateCompanyTypeModel.error && <p className="text-red-500">{data?.updateCompanyTypeModel.error}</p>}
      <form className="max-w-xl p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
        <div>
          <label htmlFor="name" className="block text-sm font-medium text-gray-700">
            名称
          </label>
          <input
            type="text"
            name="name"
            className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            {...register('name', {required: true})}
          />
          {errors.name?.type === 'required' && <p className="text-red-400">名称は必須項目です</p>}
        </div>

        <div className="flex space-x-4 py-3 text-right">
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-500 border-red-400 border  rounded-sm shadow-sm hover:bg-red-300"
            onClick={props.onRemove}
            loading={loading}>
            削除
          </Button>
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border-gray-400 border rounded-sm shadow-sm hover:bg-gray-300"
            onClick={() => navigate(`/companytypes`)}>
            戻る
          </Button>
          <Button
            type="submit"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            disabled={!isDirty || !isValid}
            loading={loading}>
            保存
          </Button>
          <Toaster position="top-right" />
        </div>
      </form>
    </div>
  );
};

export {CompanyTypeForm};
