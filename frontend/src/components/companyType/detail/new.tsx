import {CompanyType, useCreateCompanyTypeModelMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';
import {Layout} from './Layout';

const CompanyTypeNewForm = () => {
  const navigate = useNavigate();
  const [createCompanyTypeModel, {loading, data}] = useCreateCompanyTypeModelMutation({
    onCompleted: (data) => {
      if (!data.createCompanyTypeModel.error) {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/companytypes`);
        }, 500);
      }
    },
  });

  const {
    register,
    handleSubmit,
    formState: {errors, isDirty, isValid},
  } = useForm<CompanyType>({mode: 'all'});

  const onSubmit = (data) => {
    createCompanyTypeModel({variables: {attributes: data}});
  };

  return (
    <Layout>
      <div className="mt-10 md:col-span-2 w-screenmax-w-xl">
        {data?.createCompanyTypeModel.error && <p className="text-red-500">{data?.createCompanyTypeModel.error}</p>}
        <form className="max-w-xl p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              name="name"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('name', {required: true})}
            />
            {errors.name?.type === 'required' && <p className="text-red-400">名称は必須項目です</p>}
          </div>

          <div className="flex space-x-4 py-3 text-right">
            <Button
              type="button"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border-gray-400 border rounded-sm shadow-sm hover:bg-gray-300"
              onClick={() => navigate(`/companytypes`)}>
              戻る
            </Button>
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm disabled:opacity-30"
              disabled={!isDirty || !isValid}
              loading={loading}>
              保存
            </Button>
            <Toaster position="top-right" />
          </div>
        </form>
      </div>
    </Layout>
  );
};

export {CompanyTypeNewForm};
