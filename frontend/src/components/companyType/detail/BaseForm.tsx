import {useCompanyTypeQuery, useRemoveCompanyTypeModelMutation} from '@/api/index';
import React from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import {CompanyTypeForm} from './CompanyTypeForm';
import {Layout} from './Layout';

const CompanyTypeBaseForm = () => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const {data: {companyType = null} = {}} = useCompanyTypeQuery({
    variables: {id},
  });

  const [removeCompanyType] = useRemoveCompanyTypeModelMutation({
    onCompleted: (data) => {
      if (!data.removeCompanyTypeModel.error) {
        navigate(`/companytypes${location.search}`);
      }
    },
  });
  const handleRemove = () => {
    if (!confirm('削除しますか？')) return;
    removeCompanyType({variables: {id}});
  };

  if (!companyType) return null;

  return (
    <Layout companyType={companyType}>
      <CompanyTypeForm companyType={companyType} onRemove={handleRemove} />
    </Layout>
  );
};

export {CompanyTypeBaseForm};
