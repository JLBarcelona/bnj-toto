import {useJkkUsersQuery} from '@/api/index';
import React, {useEffect} from 'react';
import {Link, useSearchParams} from 'react-router-dom';
import {Tab} from './Tab';

const JkkUserList = () => {
  const [params, setParams] = useSearchParams();
  const active = params.get('active');

  const {data: {jkkUsers = []} = {}} = useJkkUsersQuery({
    variables: {search: {active: active === 'true'}},
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (active === null || active === undefined) {
      setParams({active: 'true'});
    }
  }, []);

  if (active === null || active === undefined) return null;

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">公社担当者</h1>

          <Tab />

          <Link to="/jkk_users/new">
            <div className="inline-flex justify-center px-4 py-2 mt-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              新規作成
            </div>
          </Link>

          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th className="px-6 py-3 text-xs font-medium text-left text-gray-500">ID</th>
                <th className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-60">名称</th>
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {jkkUsers.map((user) => (
                <tr className="bg-white" key={user.id}>
                  <td className="w-6">
                    <div className="px-2 cursor-pointer text-xs">{user.id}</div>
                  </td>
                  <td className="px-4 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
                    <Link to={`/jkk_users/${user.id}`}>
                      {user.lastName} {user.firstName}
                    </Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {JkkUserList};
