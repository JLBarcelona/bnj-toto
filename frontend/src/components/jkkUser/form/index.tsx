import {useJkkUserQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {JkkUserForm} from './JkkUserForm';

const Form = () => {
  const {id} = useParams<{id: string}>();
  const {data: {jkkUser = null} = {}} = useJkkUserQuery({
    variables: {id},
  });

  if (!jkkUser) return null;

  return <JkkUserForm jkkUser={jkkUser} />;
};

export {Form};
