import {JkkUser, useUpdateJkkUserMutation} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {Link, useNavigate} from 'react-router-dom';
import * as Yup from 'yup';

const validateSchema = Yup.object().shape({
  lastName: Yup.string().trim().required('必須項目です'),
  firstName: Yup.string().trim().required('必須項目です'),
});

type Props = {
  jkkUser: JkkUser;
};
const JkkUserForm = (props: Props) => {
  const navigate = useNavigate();
  const [update, {loading}] = useUpdateJkkUserMutation({
    onCompleted: () => navigate(`/jkk_users`),
  });

  const defaultValues = {
    lastName: props.jkkUser.lastName,
    firstName: props.jkkUser.firstName,
    active: props.jkkUser.active,
  };

  const {
    register,
    handleSubmit,
    setValue,
    watch,
    formState: {errors, isDirty, isValid, touchedFields},
  } = useForm<JkkUser>({
    mode: 'all',
    defaultValues,
  });

  const active = watch('active');

  const onSubmit = (data: JkkUser) => {
    update({variables: {id: props.jkkUser.id, attributes: data}});
  };

  return (
    <div className="w-screen max-w-xl">
      <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          <nav className="flex" aria-label="Breadcrumb">
            <ol role="list" className="flex items-center space-x-4">
              <li>
                <div className="flex items-center">
                  <Link to={'/jkk_users'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                    一覧
                  </Link>
                </div>
              </li>
              <li>
                <div className="flex items-center">
                  <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                  <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                    {props.jkkUser.lastName} {props.jkkUser.firstName}
                  </p>
                </div>
              </li>
            </ol>
          </nav>

          <div className="mt-10 md:col-span-2">
            <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
              <div>
                <label className="block text-sm font-medium text-gray-700">姓</label>
                <input
                  type="text"
                  name="lastName"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`}
                  {...register('lastName', {required: true})}
                />
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">名</label>
                <input
                  type="text"
                  name="firstName"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`}
                  {...register('firstName', {required: true})}
                />
              </div>

              <div className="mt-4 space-y-4">
                <div className="flex items-center">
                  <input
                    id="active"
                    type="radio"
                    className="w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500"
                    defaultChecked={defaultValues.active === true}
                    checked={active === true}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setValue('active', true, {shouldDirty: true});
                      }
                    }}
                  />
                  <label htmlFor="active" className="block ml-3 text-sm font-medium text-gray-700">
                    有効
                  </label>
                </div>
                <div className="flex items-center">
                  <input
                    id="non-active"
                    type="radio"
                    className="w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500"
                    defaultChecked={defaultValues.active === false}
                    checked={active === false}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setValue('active', false, {shouldDirty: true});
                      }
                    }}
                  />
                  <label htmlFor="non-active" className="block ml-3 text-sm font-medium text-gray-700">
                    無効
                  </label>
                </div>
              </div>

              <div className="py-3 text-right">
                <Button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  disabled={loading || !isValid || !isDirty}
                  loading={loading}>
                  保存
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export {JkkUserForm};
