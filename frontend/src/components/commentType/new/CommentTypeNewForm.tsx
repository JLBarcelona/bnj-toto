import {useCreateCommentTypeModelMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';
import {Breadcrumb} from '../Breadcrumb';

type FormProps = {
  name: string;
};
const CommentTypeNewForm = () => {
  const navigate = useNavigate();

  const [create, {loading}] = useCreateCommentTypeModelMutation({
    onCompleted: (data) => {
      if (!data.createCommentTypeModel.error) {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/commenttypes${location.search}`);
        }, 500);
      }
    },
  });

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<FormProps>({mode: 'all'});

  const onSubmit = (data) => {
    create({variables: {attributes: data}});
  };

  return (
    <div className="w-full">
      <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          <nav className="flex" aria-label="Breadcrumb">
            <Breadcrumb />
          </nav>

          <div className="mt-10 md:col-span-2">
            <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
              <div>
                <label className="block text-sm font-medium text-gray-700">名称</label>
                <input
                  type="text"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('name', {required: true})}
                />
                {errors.name?.type === 'required' && <p className="text-red-400">必須項目です</p>}
              </div>

              <div className="py-3 text-right">
                <Button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  disabled={loading || !isValid || !isDirty}
                  loading={loading}>
                  保存
                </Button>
                <Toaster position="top-right" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export {CommentTypeNewForm};
