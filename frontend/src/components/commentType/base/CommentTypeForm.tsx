import {CommentType, useRemoveCommentTypeModelMutation, useUpdateCommentTypeModelMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router';
import toast, {Toaster} from 'react-hot-toast';
import {Breadcrumb} from '../Breadcrumb';
import {Tab} from '../Tab';

type Props = {
  commentType: CommentType;
};

type FormProps = {
  name: string;
};
const CommentTypeForm = (props: Props) => {
  const navigate = useNavigate();

  const [updateCommentType, {loading}] = useUpdateCommentTypeModelMutation({
    onCompleted: (data) => {
      if (data.updateCommentTypeModel.error) {
        alert(data.updateCommentTypeModel.error);
      } else {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/commenttypes${location.search}`);
        }, 500);
      }
    },
  });

  const [removeCommentType] = useRemoveCommentTypeModelMutation({
    onCompleted: (data) => {
      if (data.removeCommentTypeModel.error) {
        alert(data.removeCommentTypeModel.error);
      } else {
        navigate(`/commenttypes${location.search}`);
      }
    },
  });
  const handleRemoveCommentType = () => {
    if (confirm('削除してよろしいですか？')) {
      removeCommentType({variables: {id: props.commentType.id}});
    }
  };

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<FormProps>({
    defaultValues: {
      name: props.commentType.name,
    },
  });

  const onSubmit = (data) => {
    updateCommentType({variables: {id: props.commentType.id, attributes: data}});
  };

  return (
    <div className="mt-4">
      <Breadcrumb commentType={props.commentType} />
      <Tab />
      <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6">
          <div className="col-span-6">
            <label className="block text-sm font-medium text-gray-700">名称</label>
            <input
              type="text"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('name', {required: true})}
            />
            {errors.name?.type === 'required' && <p className="text-red-400">必須項目です</p>}
          </div>
        </div>

        <div className="py-3 text-right flex items-center">
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm hover:bg-gray-200"
            onClick={() => navigate(`/commenttypes${location.search}`)}>
            戻る
          </Button>
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-red-400 rounded-sm shadow-sm hover:bg-red-200"
            onClick={handleRemoveCommentType}>
            削除
          </Button>
          <Button
            type="submit"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            disabled={loading || !isValid || !isDirty}
            loading={loading}>
            保存
          </Button>
          <Toaster position="top-right" />
        </div>
      </form>
    </div>
  );
};

export {CommentTypeForm};
