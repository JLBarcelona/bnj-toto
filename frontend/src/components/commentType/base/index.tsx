import {useCommentTypeQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {CommentTypeForm} from './CommentTypeForm';

const CommentType = () => {
  const {id} = useParams<{id: string}>();
  const {data: {commentType = null} = {}} = useCommentTypeQuery({variables: {id}});

  if (!commentType) return null;

  return <CommentTypeForm commentType={commentType} />;
};

export {CommentType};
