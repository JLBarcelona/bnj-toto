import React from 'react';
import {Comment, ItemCode, useRemoveCommentCodeMutation} from '@/api/index';
import {CSS} from '@dnd-kit/utilities';
import {SortableContext, sortableKeyboardCoordinates, rectSortingStrategy, useSortable} from '@dnd-kit/sortable';
import {Button} from '@/components/ui';
import {EllipseVerticalIcon} from '@/components/icons';

type Props = {
  comment: Comment;
};

const CommentRow = (props: Props) => {
  const {setNodeRef, attributes, listeners, transform, transition} = useSortable({id: props.comment.id});
  const style = {
    transform: CSS.Transform.toString(transform),
    transition,
  };

  return (
    <tr ref={setNodeRef} className="bg-white" {...attributes} {...listeners} style={style}>
      <td>
        <EllipseVerticalIcon className="h-6 text-gray-500 cursor-pointer" />
      </td>

      <td className="text-sm px-2 py-2">{props.comment.comment}</td>
    </tr>
  );
};

export {CommentRow};
