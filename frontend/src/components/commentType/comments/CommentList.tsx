import React, {useCallback, useEffect, useState} from 'react';
import {
  useSensors,
  useSensor,
  PointerSensor,
  KeyboardSensor,
  closestCenter,
  DndContext,
  DragStartEvent,
  DragEndEvent,
  UniqueIdentifier,
} from '@dnd-kit/core';
import {arrayMove, SortableContext, sortableKeyboardCoordinates, verticalListSortingStrategy} from '@dnd-kit/sortable';

import {Comment, CommentType, useUpdateCommentOrderMutation} from '@/api/index';
import {Breadcrumb} from '../Breadcrumb';
import {Tab} from '../Tab';
import {CommentRow} from './CommentRow';

type Props = {
  commentType: CommentType;
};
const CommentList = (props: Props) => {
  const [ids, setIds] = useState<string[]>(props.commentType.comments.map((comment) => comment.id));
  const [activeId, setActiveId] = useState<UniqueIdentifier | null>(null);
  const getIndex = (id: UniqueIdentifier) => ids.indexOf(String(id));
  const activeIndex = activeId ? getIndex(activeId) : -1;

  const [updateCommentOrder] = useUpdateCommentOrderMutation({
    onCompleted: () => {
      setActiveId(null);
    },
  });

  // ドラッグ&ドロップする時に許可する入力
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    }),
  );

  // ドラッグ開始時に発火する関数
  const onDragStart = ({active}) => {
    if (!active) {
      return;
    }

    setActiveId(String(active.id));
  };

  // ドラッグ終了時に発火する関数
  const onDragEnd = ({active, over}: DragEndEvent) => {
    if (over) {
      const overIndex = getIndex(over.id);

      if (activeIndex !== overIndex) {
        setIds((ids) => arrayMove(ids, activeIndex, overIndex));
        updateCommentOrder({variables: {id: String(active.id), position: overIndex}});
      }
    }
  };

  if (!props.commentType.comments) return null;

  return (
    <div className="mt-4">
      <Breadcrumb commentType={props.commentType} />
      <Tab />

      <DndContext sensors={sensors} collisionDetection={closestCenter} onDragStart={onDragStart} onDragEnd={onDragEnd}>
        <table className="min-w-full mt-4 divide-y divide-gray-200">
          <thead className="bg-gray-50">
            <tr>
              <th className="w-20" />
              <th className="px-2 py-3 text-xs font-medium text-left text-gray-500">名称</th>
              <th />
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-400">
            <SortableContext items={props.commentType.comments} strategy={verticalListSortingStrategy}>
              {ids.map((id) => (
                <CommentRow comment={props.commentType.comments.find((comment) => comment.id === id)} key={id} />
              ))}
            </SortableContext>
          </tbody>
        </table>
      </DndContext>
    </div>
  );
};

export {CommentList};
