import {useCommentTypeQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {CommentList} from './CommentList';

const CommentListContainer = () => {
  const {id} = useParams<{id: string}>();
  const {data: {commentType = null} = {}} = useCommentTypeQuery({variables: {id}, fetchPolicy: 'cache-and-network'});

  if (!commentType) return null;

  return <CommentList commentType={commentType} />;
};

export {CommentListContainer};
