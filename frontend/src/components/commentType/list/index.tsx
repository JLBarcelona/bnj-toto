import {useCommentTypesQuery} from '@/api/index';
import React from 'react';
import {Link} from 'react-router-dom';

const CommentTypes = () => {
  const {data: {commentTypes = []} = {}} = useCommentTypesQuery({
    fetchPolicy: 'cache-and-network',
  });

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">コメント種別</h1>

          <Link to="/commenttypes/new">
            <div className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              新規作成
            </div>
          </Link>

          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  名称
                </th>
                <th />
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {commentTypes.map((commentType) => (
                <tr className="bg-white" key={commentType?.id}>
                  <td className="px-2 py-4 text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300">
                    <Link to={`/commenttypes/${commentType.id}/base`}>{commentType.name}</Link>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {CommentTypes};
