import {useSigninMutation} from '@/api/index';
import CURRENT_USER from '@/api/queries/user/currentUser';
import {Button} from '@/components/ui';
import Layout from '@/layouts/Login';
import {useFormik} from 'formik';
import React from 'react';
import {useNavigate} from 'react-router-dom';

const Login = () => {
  const navigate = useNavigate();
  const [signin, {data, loading}] = useSigninMutation({
    update: (cache, {data}) => {
      if (!data?.signin.error) {
        cache.writeQuery({
          query: CURRENT_USER,
          data: {currentUser: data?.signin.user},
        });
      }
    },

    onCompleted: (data) => {
      if (!data?.signin.error) {
        navigate('/');
      }
    },
  });

  const formik = useFormik({
    initialValues: {loginId: '', password: ''},
    onSubmit: (values) => {
      signin({variables: {loginId: values.loginId, password: values.password}});
    },
  });

  return (
    <Layout>
      <div className="sm:mx-auto sm:w-full sm:max-w-md">
        <img
          className="w-auto h-12 mx-auto"
          src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
          alt="Workflow"
        />
        <h2 className="mt-6 text-3xl font-extrabold text-center text-gray-900">TOTO Login</h2>
      </div>

      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-lg">
        <div className="px-4 py-8 bg-white shadow sm:rounded-lg sm:px-10">
          {data?.signin.error && (
            <div className="p-4 rounded-md bg-red-50">
              <div className="flex">
                <div className="ml-3">
                  <h3 className="text-sm font-medium text-red-800">エラーをご確認ください</h3>
                  <div className="mt-2 text-sm text-red-700">
                    <ul role="list" className="pl-5 space-y-1 list-disc">
                      <li>{data?.signin.error}</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          )}
          <form className="space-y-6" onSubmit={formik.handleSubmit}>
            <div>
              <label htmlFor="loginId" className="block text-sm font-medium text-gray-700">
                ログインID
              </label>
              <div className="mt-1">
                <input
                  name="loginId"
                  type="text"
                  required
                  className="block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  value={formik.values.loginId}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>

            <div>
              <label htmlFor="password" className="block text-sm font-medium text-gray-700">
                Password
              </label>
              <div className="mt-1">
                <input
                  id="password"
                  name="password"
                  type="password"
                  autoComplete="current-password"
                  required
                  className="block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
              </div>
            </div>

            <div>
              <Button
                type="submit"
                className="flex justify-center w-full px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                loading={loading}>
                ログイン
              </Button>
            </div>
          </form>
        </div>
      </div>
    </Layout>
  );
};

export {Login};
