import {CompanyType, Item, Pagination as PaginationType, useCopyItemsMutation, Year} from '@/api/index';
import {Button, Loading} from '@/components/ui';
import Pagination from '@/components/ui/Pagination';
import React, {useEffect, useState} from 'react';
import {useForm} from 'react-hook-form';
import {Link, useSearchParams} from 'react-router-dom';
import {ItemNewForm} from '../new/ItemNewForm';

type Props = {
  items: Item[];
  years: Year[];
  pagination: PaginationType;
  loadingItems: boolean;
  companyTypes: CompanyType[];
  refetch: () => void;
  refetchYear: () => void;
};
const ItemList = (props: Props) => {
  const [params, setParams] = useSearchParams();
  const queryYear = params.get('year');
  const queryKeyword = params.get('keyword');
  const [isOpen, setIsOpen] = useState(false);

  const {register, handleSubmit, watch} = useForm<{year: string; keyword: string}>({
    defaultValues: {year: queryYear, keyword: queryKeyword},
  });

  const onSubmit = (data) => {
    setParams({year: data.year, page: '1', keyword: data.keyword});
  };
  const year = watch('year');

  let newYear = props.years.length > 0 ? props.years[0].year + 1 : null;
  const [copyItems, {loading}] = useCopyItemsMutation({
    onCompleted: () => {
      props.refetch();
      props.refetchYear();
    },
  });

  const handleCopy = () => {
    if (confirm(`${newYear}年度の項目をコピーで作成しますか？`)) {
      copyItems();
    }
  };

  const findCompanyTypes = (companyTypeIds: string[]) => {
    if (!companyTypeIds) return '';

    let values = [];
    for (const companyTypeId of companyTypeIds) {
      const companyType = props.companyTypes.find((companyType) => companyType.id === `${companyTypeId}`);
      if (companyType) {
        values.push(companyType.name);
        continue;
      }
    }

    return values.join(',');
  };

  useEffect(() => {
    setParams({year: year || '', page: '1', keyword: queryKeyword || ''});
  }, [year]);

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">項目</h1>

          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="my-4">
              <select className="px-4 py-2 border border-gray-300" {...register('year')}>
                <option value=""></option>
                {props.years.map((year) => (
                  <option key={`year-${year.id}`} value={String(year.year)}>
                    {year.year}
                  </option>
                ))}
              </select>

              <Button
                type="button"
                className="inline-flex justify-center px-4 py-2 ml-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                onClick={() => setIsOpen(true)}
                disabled={!year}>
                新規作成
              </Button>
              <ItemNewForm
                year={year}
                isOpen={isOpen}
                onClose={() => {
                  setIsOpen(false);
                  props.refetch();
                }}
              />

              <Button
                type="button"
                className="inline-flex justify-center px-4 py-2 ml-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                onClick={handleCopy}
                loading={loading}>
                新年度コピー
              </Button>
            </div>
            <div className="grid grid-cols-1 my-2 gap-y-6 gap-x-4 sm:grid-cols-6">
              <div className="sm:col-span-4">
                <label className="block text-sm font-medium text-gray-700">キーワード</label>
                <div className="mt-1">
                  <input
                    type="text"
                    className="block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    {...register('keyword')}
                  />
                </div>
              </div>
            </div>
          </form>

          <Pagination
            totalCount={props.pagination.totalCount || 0}
            currentPage={props.pagination.currentPage || 0}
            totalPages={props.pagination.totalPages || 0}
            pageSize={20}
          />
          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="w-6 px-6 py-3 text-xs font-medium text-left text-gray-500">
                  ID
                </th>
                <th scope="col" className="w-24 px-6 py-3 text-xs font-medium text-left text-gray-500">
                  コード
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-80">
                  名称
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  単価
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  発注対象
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  発注単価
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  業者区分
                </th>
              </tr>
            </thead>
            <tbody className={`divide-y divide-gray-200 ${!props.loadingItems && 'bg-white'}`}>
              {props.loadingItems && <Loading />}
              {!props.loadingItems &&
                props.items.map((item) => (
                  <tr className="bg-white" key={`item-${item.id}`}>
                    <td className="">
                      <div className="px-2 text-sm">{item.id}</div>
                    </td>
                    <td className="">
                      <div className="px-2 cursor-pointer">
                        <Link to={`/items/${item.id}${location.search}`}>{item.itemCode.code}</Link>
                      </div>
                    </td>
                    <td className="px-6 py-4 text-sm font-medium text-gray-900">
                      <Link to={`/items/${item.id}${location.search}`}>{item.name}</Link>
                    </td>
                    <td className="w-20">
                      <div className="px-2 text-sm text-right">{item.price}</div>
                    </td>
                    <td className="w-20">
                      <div className="px-2 text-sm text-right">{!item.order && '発注なし'}</div>
                    </td>
                    <td className="w-20">
                      <div className="px-2 text-sm text-right">{item.orderPrice}</div>
                    </td>
                    <td className="">
                      <div className="px-2 text-sm">{findCompanyTypes(item.companyTypeIds)}</div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {ItemList};
