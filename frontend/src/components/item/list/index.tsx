import {useCompanyTypesQuery, useItemsQuery, useYearsQuery} from '@/api/index';
import React from 'react';
import {useSearchParams} from 'react-router-dom';
import {ItemList} from './ItemList';

const Items = () => {
  const [params] = useSearchParams();
  const page = params.get('page') ? Number(params.get('page')) : 1;
  const year = params.get('year');
  const keyword = params.get('keyword');

  const {
    data: {items: {items = [], pagination = {}} = {}} = {},
    refetch,
    loading: loadingItems,
  } = useItemsQuery({
    variables: {page, search: {year, keyword}},
    skip: !year,
  });
  const {data: {years = []} = {}, refetch: refetchYear} = useYearsQuery();
  const {data: {companyTypes = []} = {}} = useCompanyTypesQuery();

  if (!years || years.length === 0) return null;

  return (
    <ItemList
      items={items}
      pagination={pagination}
      loadingItems={loadingItems}
      companyTypes={companyTypes}
      refetch={refetch}
      years={years}
      refetchYear={refetchYear}
    />
  );
};

export {Items};
