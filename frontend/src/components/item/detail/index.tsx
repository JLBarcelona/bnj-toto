import {useCompanyTypesQuery, useItemQuery, useUnitsQuery} from '@/api/index';
import React from 'react';
import ReactModal from 'react-modal';
import {useNavigate, useParams} from 'react-router-dom';
import {ItemForm} from './ItemForm';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '800px',
    height: '680px',
  },
};

const Item = () => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const {data: {item = null} = {}} = useItemQuery({
    variables: {id},
  });
  const {data: {companyTypes = []} = {}} = useCompanyTypesQuery();
  const {data: {units = []} = {}} = useUnitsQuery();

  const handleClose = () => {
    navigate(`/items${location.search}`);
  };

  if (!item || companyTypes.length === 0 || units.length === 0) return null;

  return (
    <ReactModal isOpen={true} onRequestClose={handleClose} style={customStyles}>
      <ItemForm item={item} companyTypes={companyTypes} units={units} />
    </ReactModal>
  );
};

export {Item};
