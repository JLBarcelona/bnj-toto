import {useCompanyTypesQuery, useCreateItemMutation, useUnitsQuery} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import ReactModal from 'react-modal';
import toast, {Toaster} from 'react-hot-toast';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '800px',
    height: '880px',
  },
};

type ItemFormProps = {
  year: string;
  code: string;
  name: string;
  shortName: string;
  price: number;
  orderPrice: number;
  companyId: string;
  companyTypeIds: string[];
  order: boolean;
  unitId: string;
};

type Props = {
  year: string;
  isOpen: boolean;
  onClose: () => void;
};
const ItemNewForm = (props: Props) => {
  const {data: {companyTypes = []} = {}} = useCompanyTypesQuery();

  const handleClose = () => {
    props.onClose();
  };

  const [createItem, {loading, data}] = useCreateItemMutation({
    onCompleted: (data) => {
      if (!data.createItem.error) {
        toast.success('保存しました');
        setTimeout(() => {
          handleClose();
        }, 500);
      }
    },
  });
  const {data: {units = []} = {}} = useUnitsQuery();

  const {
    register,
    handleSubmit,
    formState: {errors, isDirty, isValid},
  } = useForm<ItemFormProps>({mode: 'all'});

  const onSubmit = (data) => {
    const code = data.code;
    delete data.code;
    createItem({variables: {code: code, year: props.year, attributes: data}});
  };

  return (
    <ReactModal isOpen={props.isOpen} onRequestClose={handleClose} style={customStyles}>
      <div className="flex flex-col py-1 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
            {data?.createItem.error && <p className="text-red-400">{data?.createItem.error}</p>}
            <div>
              <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                コード
              </label>
              <input
                type="text"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                {...register('code', {required: true})}
              />
              {errors.code?.type === 'required' && <p className="text-red-400">必須項目です</p>}
            </div>

            <div>
              <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                名称
              </label>
              <input
                type="text"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                {...register('name', {required: true})}
              />
              {errors.name?.type === 'required' && <p className="text-red-400">必須項目です</p>}
            </div>

            <div>
              <label htmlFor="shortName" className="block text-sm font-medium text-gray-700">
                略称
              </label>
              <input
                type="text"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                {...register('shortName')}
              />
            </div>

            <div>
              <label htmlFor="price" className="block text-sm font-medium text-gray-700">
                単価
              </label>
              <input
                type="number"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                {...register('price', {valueAsNumber: true})}
              />
            </div>

            <div>
              <label htmlFor="orderPrice" className="block text-sm font-medium text-gray-700">
                発注単価
              </label>
              <input
                type="number"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                {...register('orderPrice', {valueAsNumber: true})}
              />
            </div>

            <div>
              <label htmlFor="price" className="block text-sm font-medium text-gray-700">
                業者種別
              </label>
              <div className="flex flex-col justify-center rounded-md shadow-sm">
                {companyTypes.map((companyType) => (
                  <div className="flex items-center h-5" key={`companyType-${companyType.id}`}>
                    <input
                      id={companyType.id}
                      key={companyType.id}
                      type="checkbox"
                      value={companyType.id}
                      className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                      {...register('companyTypeIds')}
                    />
                    <div className="ml-1 text-sm">
                      <label className="font-medium text-gray-700">{companyType.name}</label>
                    </div>
                  </div>
                ))}
              </div>
            </div>

            <div>
              <label htmlFor="unitId" className="block text-sm font-medium text-gray-700">
                単位
              </label>
              <div className="flex flex-col justify-center rounded-md shadow-sm">
                <select
                  className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                  {...register('unitId')}>
                  <option value=""></option>
                  {units.map((unit) => (
                    <option key={`unit-${unit.id}`} value={unit.id}>
                      {unit.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div>
              <label className="block text-sm font-medium text-gray-700">発注対象</label>
              <div className="flex flex-col justify-center rounded-md shadow-sm">
                <input
                  id="order"
                  type="checkbox"
                  className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                  {...register('order')}
                />
              </div>
            </div>

            <div className="py-3 text-right">
              <div className="inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <Button type="button" onClick={() => props.onClose()}>
                  閉じる
                </Button>
              </div>

              <Button
                type="submit"
                className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                disabled={!isDirty || !isValid}
                loading={loading}>
                保存
              </Button>
              <Toaster position="top-right" />
            </div>
          </form>
        </div>
      </div>
    </ReactModal>
  );
};

export {ItemNewForm};
