import {Project} from '@/api/index';
import React from 'react';
import {Link} from 'react-router-dom';

type Props = {
  project: Project;
};
const ProjectRow = ({project}: Props) => {
  return (
    <tr className="bg-white">
      <td className="text-sm px-2">{project?.id}</td>
      <td className="py-2">
        <div className="text-sm font-medium text-blue-500 whitespace-nowrap hover:text-blue-300 mt-2">
          <Link to={`/projects/${project?.id}/form/base${location.search}`}>{project?.name}</Link>
        </div>
        <div className="flex space-x-8">
          <div className="text-sm text-gray-500 w-32">
            <span className="mr-2">{project?.buildingNumber?.name} 号棟</span>
            <span>{project?.room?.name} 号室</span>
          </div>
          <div>
            <div>
              {project.itemUploadDateText && (
                <span className="rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1">1.査定表読込済</span>
              )}
              {project.reportDateText && (
                <span className="rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1">
                  2.施工前済・質疑回答待ち
                </span>
              )}
              {project.checklistDateText && (
                <span className="rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1">
                  3.施工前済・最終CL済
                </span>
              )}
              {project.lackImageDateText && (
                <span className="rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1">
                  4.施工後不足写真待ち
                </span>
              )}
              {project.completeImageDateText && (
                <span className="rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1">5.施工後済</span>
              )}
              {project.invoiceDateText && (
                <span className="rounded-sm bg-blue-100 px-2 py-1 text-[10px] font-semibold mx-1">6.請求書済</span>
              )}
            </div>

            <div>
              {project.jkkStatus1 && (
                <span className="rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1">1.CL済</span>
              )}
              {project.jkkStatus2 && (
                <span className="rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1">2.指示書済済</span>
              )}
              {project.jkkStatus3 && (
                <span className="rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1">
                  3.指示書再発行待ち
                </span>
              )}
              {project.jkkStatus4 && (
                <span className="rounded-sm bg-yellow-100 px-2 py-1 text-[10px] font-semibold mx-1">
                  4.指示書再発行済ち
                </span>
              )}
            </div>
          </div>
        </div>
      </td>
      <td className="px-2 py-4 text-sm font-medium whitespace-nowrap">
        <div className="text-gray-700 text-right">{project.totalPrice?.toLocaleString()}</div>
      </td>
      <td className="px-2 py-4 text-sm font-medium whitespace-nowrap">
        <div className="text-gray-700">
          <span>{project.constructionStartDate}</span>
          <span className="mx-2">-</span>
          <span>{project.constructionEndDate}</span>
        </div>
      </td>
    </tr>
  );
};

export {ProjectRow};
