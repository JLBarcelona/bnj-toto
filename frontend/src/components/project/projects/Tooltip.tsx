import React from 'react';

type Props = {
  tooltipText: string;
  children?: React.ReactNode;
};
const Tooltip: React.FC<Props> = ({tooltipText, children}) => {
  if (!tooltipText) return <div />;
  return (
    <div className="relative">
      <div className="absolute t-0 left-0 z-10 w-64 px-4 py-2 text-sm whitespace-pre-wrap bg-gray-100 border border-gray-500 rounded-md top-full">
        {tooltipText}
      </div>
      <div>{children}</div>
    </div>
  );
};

export {Tooltip};
