import {useProjectsQuery} from '@/api/index';
import Pagination from '@/components/ui/Pagination';
import {useFormik} from 'formik';
import React, {useEffect, useMemo} from 'react';
import {Link, useSearchParams} from 'react-router-dom';
import {ProjectRow} from './ProjectRow';

const Projects = () => {
  const [params, setParams] = useSearchParams();
  const page = params.get('page');
  const keyword = params.get('keyword');
  const orderType = params.get('orderType');

  const entries = useMemo(() => Array.from(new Set(params.entries())), [params]);
  const setParam = (key: string, value: string) => {
    let newParams = {};
    entries.forEach((entry) => (newParams[entry[0]] = entry[1]));
    setParams({...newParams, [key]: value});
  };

  const {data: {projects: {projects = [], pagination = {}} = {}} = {}} = useProjectsQuery({
    variables: {
      page: page ? Number(page) : 1,
      search: {keyword, orderType},
    },
    fetchPolicy: 'cache-and-network',
    skip: !orderType,
  });

  const formik = useFormik({
    initialValues: {keyword: keyword || '', active: false},
    onSubmit: (values) => {
      setParams({keyword: values.keyword, page: '1', orderType});
    },
  });

  useEffect(() => {
    if (!orderType) {
      setParams({orderType: 'desc', keyword: keyword || '', page: page || '1'});
    }
  }, [orderType]);

  return (
    <div className="flex flex-col h-full">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 h-full">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">案件一覧</h1>

          <form className="px-10 py-2 mb-4 bg-white" onSubmit={formik.handleSubmit}>
            <div className="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6">
              <div className="sm:col-span-4">
                <label className="block text-sm font-medium text-gray-700">キーワード</label>
                <div className="mt-1">
                  <input
                    name="keyword"
                    type="text"
                    className="block w-full px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.keyword}
                    onChange={formik.handleChange}
                    onBlur={() => formik.handleSubmit()}
                  />
                </div>
              </div>
            </div>
          </form>

          <Link to="/projects/new">
            <div className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              新規作成
            </div>
          </Link>

          <div className="flex mt-4">
            <div className="relative w-32">
              <select
                className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
                value={orderType}
                onChange={(e) => setParam('orderType', e.target.value)}>
                <option value="asc">昇順</option>
                <option value="desc">降順</option>
              </select>
            </div>
          </div>

          <Pagination
            totalCount={pagination.totalCount || 0}
            currentPage={pagination.currentPage || 0}
            totalPages={pagination.totalPages || 0}
            pageSize={20}
          />
          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-12">
                  ID
                </th>
                <th scope="col" className="px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  名称
                </th>
                <th scope="col" className="w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  工事金額（指示書/税込）
                </th>
                <th scope="col" className="px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  工期（開始日 / 終了日）
                </th>
                <th />
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-400">
              {projects.map((project) => (
                <ProjectRow project={project} key={project?.id} />
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {Projects};
