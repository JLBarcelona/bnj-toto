import React from 'react';
import {Link, useParams} from 'react-router-dom';

const Tab = () => {
  const {id} = useParams<{id: string}>();
  const currentPath = (path: string) => location.pathname.indexOf(path) > -1;

  return (
    <div className="hidden sm:block">
      <div className="border-b border-gray-200">
        <nav className="flex -mb-px space-x-8" aria-label="Tabs">
          <Link to={`/projects/${id}/form/base${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('form')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              基本情報
            </div>
          </Link>
          <Link to={`/projects/${id}/items${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('items')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              明細
            </div>
          </Link>
          <Link to={`/projects/${id}/company${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('company')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              発注
            </div>
          </Link>
          <Link to={`/projects/${id}/images${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('images')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              画像
            </div>
          </Link>
          <Link to={`/projects/${id}/report${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('report')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              レポート
            </div>
          </Link>
          <Link to={`/projects/${id}/remark${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('remark')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              備考
            </div>
          </Link>
        </nav>
      </div>
    </div>
  );
};

export {Tab};
