import {Item, ProjectImage, useAddImageCommentMutation} from '@/api/index';
import {PlusIcon} from '@/components/icons';
import {Loading} from '@/components/ui/loading';
import React from 'react';

interface Props {
  projectImage: ProjectImage;
  item: Item;
}
const Item = ({projectImage, item}: Props) => {
  const [addImageComment, {loading}] = useAddImageCommentMutation();

  const handleAddImageComment = () => {
    addImageComment({variables: {id: projectImage.id, itemId: item.id}});
  };

  return (
    <tr className={`bg-white`} key={item?.id}>
      <td className="w-6 m-auto">
        {!loading && <PlusIcon className="text-blue-700 cursor-pointer" onClick={() => handleAddImageComment()} />}
        {loading && <Loading className="w-3 h-3" />}
      </td>
      <td className="w-20 px-4 text-xs">{item.itemCode.code}</td>
      <td className="flex-1 px-4 text-xs">{item.itemCode.name}</td>
    </tr>
  );
};
export {Item};
