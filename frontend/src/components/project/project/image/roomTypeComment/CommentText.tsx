import React from 'react';
import {useFormik} from 'formik';
import {ImageComment, useUpdateImageCommentMutation} from '@/api/index';

interface Props {
  imageComment: ImageComment;
}
const CommentText = ({imageComment}: Props) => {
  const [updateImageComment] = useUpdateImageCommentMutation();

  const handleUpdateImageComment = (imageComment: ImageComment, attributes: ImageComment) => {
    updateImageComment({variables: {id: imageComment.id, attributes}});
  };

  const formik = useFormik({
    initialValues: {
      text: imageComment.text,
    },
    onSubmit: (values) => {
      handleUpdateImageComment(imageComment, values);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <input
        type="text"
        name="text"
        value={formik.values.text}
        onChange={formik.handleChange}
        onBlur={() => formik.handleSubmit()}
        className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"></input>
    </form>
  );
};
export {CommentText};
