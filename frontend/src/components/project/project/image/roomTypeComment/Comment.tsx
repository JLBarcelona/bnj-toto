import {
  ImageComment,
  Project,
  ProjectImage,
  useAllItemsQuery,
  useRemoveImageCommentMutation,
  useRoomTypeQuery,
} from '@/api/index';
import {MinusIcon} from '@/components/icons';
import React from 'react';
import {CommentText} from './CommentText';
import {Item} from './Item';

interface Props {
  project: Project;
  projectImage: ProjectImage;
}
const Comment = ({project, projectImage}: Props) => {
  const {data: {roomType = null} = {}} = useRoomTypeQuery({variables: {id: projectImage.roomTypeId}});
  const {data: {allItems = []} = {}} = useAllItemsQuery({variables: {search: {year: String(project.year.year)}}});
  const [removeImageComment] = useRemoveImageCommentMutation();
  const handleRemoveImageComment = (imageComment: ImageComment) => {
    removeImageComment({variables: {id: imageComment.id}});
  };

  // 明細にコードがあるコメントのみ対象
  const filtertems = () => {
    const targetProjectItems = project.projectItems.filter((projectItem) =>
      projectItem.roomTypeIds?.includes(projectImage.roomTypeId),
    );
    const targetItemCodes = targetProjectItems.map((projectItem) => projectItem.code);
    // const roomTypeIds = _.uniq(project.projectItems.map((item) => item.roomTypeIds).flatMap((v) => v));
    // const itemCodes = project.projectItems
    //   .filter((item) => item.roomTypeIds?.length > 0)
    //   .map((projectItem) => projectItem.code);
    // console.log('🚀 ~ file: Comment.tsx:47 ~  filtertems ~ itemCodes', itemCodes);

    // const items = roomType?.roomTypeComments?.filter(
    //   (roomTypeComment) =>
    //     projectImage.roomTypeId === roomTypeComment.roomTypeId &&
    //     roomTypeIds.includes(roomTypeComment.roomTypeId) &&
    //     itemCodes.includes(roomTypeComment.itemCode.code),
    // );
    const items = allItems.filter((item) => targetItemCodes.includes(item.itemCode.code));

    console.log('🚀 ~ file: Comment.tsx:56 ~  filtertems', items);
    return items;
  };

  return (
    <div>
      <div style={{height: '240px'}}>
        <table className="w-full divide-y divide-gray-200">
          <thead className="bg-gray-50">
            <tr className="flex text-center">
              <th className="w-4" />
              <th className="w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">コード</th>
              <th className="flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">名称</th>
            </tr>
          </thead>
          <tbody
            className="flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200"
            style={{height: '200px'}}>
            {filtertems()?.map((item, i) => (
              <Item projectImage={projectImage} item={item} key={`item-${i}-${item.id}`} />
            ))}
          </tbody>
        </table>
      </div>

      <table className="w-full divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr className="flex text-center">
            <th className="w-4" />
            <th className="w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">コード</th>
            <th className="flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">名称</th>
          </tr>
        </thead>
        <tbody className="flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200">
          {projectImage?.imageComments?.map((imageComment) => (
            <tr className="flex bg-white" key={imageComment?.id}>
              <td className="w-4 m-auto">
                <MinusIcon
                  className="text-blue-700 cursor-pointer"
                  onClick={() => handleRemoveImageComment(imageComment)}
                />
              </td>
              <td className="w-20 px-4 my-auto text-xs">{imageComment.code}</td>
              <td className="flex-1 px-4 text-xs">
                <CommentText imageComment={imageComment} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
export {Comment};
