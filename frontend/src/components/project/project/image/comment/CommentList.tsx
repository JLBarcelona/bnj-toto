import React, {useState} from 'react';
import {Project, ProjectImage, useCommentsQuery, useCommentTypesQuery} from '@/api/index';
import {Comment} from './Comment';

interface Props {
  project: Project;
  projectImage: ProjectImage;
}
const CommentList = (props: Props) => {
  const [commentTypeId, setCommentTypeId] = useState(null);
  const {data: {comments = []} = {}} = useCommentsQuery({variables: {search: {commentTypeId}}, skip: !commentTypeId});
  const {data: {commentTypes = []} = {}} = useCommentTypesQuery();

  const itemCodes = props.project.projectItems.map((item) => item.code);
  const filterComments = () =>
    comments.filter((comment) => comment.itemCodes.findIndex((itemCode) => itemCodes.includes(itemCode.code)) > -1);

  return (
    <div style={{height: '240px'}}>
      <div className="flex flex-wrap space-x-2 space-y-1">
        {commentTypes.map((commentType) => (
          <div
            key={commentType.id}
            className={`inline-flex items-center rounded-md bg-gray-100 px-2.5 py-0.5 text-sm font-medium cursor-pointer hover:bg-blue-100 ${
              commentTypeId === commentType.id && 'bg-blue-300'
            }`}
            onClick={() => setCommentTypeId(commentType.id)}>
            {commentType.name}
          </div>
        ))}
      </div>

      <table className="w-full divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr className="flex text-center">
            <th className="w-4" />
            <th className="flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">コメント</th>
          </tr>
        </thead>
        <tbody className="flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200">
          {filterComments()?.map((comment) => (
            <Comment projectImage={props.projectImage} comment={comment} key={`comment-${comment.id}`} />
          ))}
        </tbody>
      </table>
    </div>
  );
};
export {CommentList};
