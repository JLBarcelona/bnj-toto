import {Comment, ProjectImage, useAddCommentMutation, useRemoveProjectImageCommentMutation} from '@/api/index';
import {MinusIcon, PlusIcon} from '@/components/icons';
import {Loading} from '@/components/ui/loading';
import React from 'react';

interface Props {
  projectImage: ProjectImage;
  comment: Comment;
}
const Comment = ({projectImage, comment}: Props) => {
  const [addComment, {loading}] = useAddCommentMutation();
  const [removeProjectImageComment] = useRemoveProjectImageCommentMutation();

  const handleAddImageComment = (commentId: string) => {
    addComment({variables: {id: projectImage.id, commentId}});
  };

  const handleRemoveImageComment = (commentId: string) => {
    removeProjectImageComment({variables: {id: projectImage.id, commentId}});
  };

  const checkImageComment = (comment: Comment) =>
    projectImage.comments.findIndex((projectImageComment) => projectImageComment.id === comment.id) > -1;

  return (
    <tr className={`bg-white ${checkImageComment(comment) && 'bg-gray-200'}`} key={comment?.id}>
      <td className="w-6 m-auto">
        {!loading && !checkImageComment(comment) && (
          <PlusIcon className="text-blue-700 cursor-pointer" onClick={() => handleAddImageComment(comment.id)} />
        )}
        {!loading && checkImageComment(comment) && (
          <MinusIcon
            className="text-xs text-blue-700 cursor-pointer"
            onClick={() => handleRemoveImageComment(comment.id)}
          />
        )}
        {loading && <Loading className="w-3 h-2" />}
      </td>
      <td className="flex-1 px-4 text-xs">{comment.comment}</td>
    </tr>
  );
};
export {Comment};
