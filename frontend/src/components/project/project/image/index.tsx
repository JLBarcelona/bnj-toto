import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {Image} from './ImageBase';
import {OtherImageBase} from './other/OhterImageBase';

const ImageContainer = () => {
  return (
    <Routes>
      <Route path="other" element={<OtherImageBase />} />
      <Route path=":roomTypeId" element={<Image />} />
      <Route index element={<Image />} />
    </Routes>
  );
};

export {ImageContainer};
