import React, {useEffect, useState} from 'react';
import {Project, ProjectImage, useUpdateProjectImageOrderMutation} from '@/api/index';
import {Image} from './Image';
import _ from 'lodash';
import {DndProvider} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import update from 'immutability-helper';

type Props = {
  project: Project;
  roomTypeId: string;
  projectImages: Array<ProjectImage>;
};
const ImageListContainer = ({project, projectImages, roomTypeId}: Props) => {
  const [images, setImages] = useState([]);
  const findImage = (id: string) => {
    const projectImage = projectImages.find((pj) => pj.id === id);
    const index = projectImages.findIndex((pj) => pj.id === id);
    return {
      projectImage,
      index,
    };
  };
  const [order] = useUpdateProjectImageOrderMutation();
  const moveImage = (id: string, dragIndex: number, hoverIndex: number) => {
    console.log(`update: id: ${id}`);
    console.log(`update: dragIndex: ${dragIndex}`);
    console.log(`update: hoverIndex: ${hoverIndex}`);
    // order({variables: {id, attributes: {position: dragIndex}}});
    setImages((images: ProjectImage[]) =>
      update(images, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, images[dragIndex] as ProjectImage],
        ],
      }),
    );
  };

  const handleOrder = (id: string, index: number) => {
    order({variables: {id, attributes: {position: index}}});
  };

  useEffect(() => {
    setImages(projectImages);
  }, [projectImages]);

  return (
    <div>
      <DndProvider backend={HTML5Backend}>
        {images.map((projectImage, i) => (
          <Image
            index={i}
            id={projectImage.id}
            key={projectImage.id}
            project={project}
            projectImage={projectImage}
            roomTypeId={roomTypeId}
            imageType={projectImage.imageType}
            moveImage={moveImage}
            order={handleOrder}
          />
        ))}
      </DndProvider>
    </div>
  );
};

export {ImageListContainer};
