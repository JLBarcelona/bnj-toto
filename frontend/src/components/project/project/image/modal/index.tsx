import {Button} from '@/components/ui';
import React from 'react';
import ReactModal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '800px',
    height: '680px',
  },
  overlay: {
    zIndex: '20',
  },
};

type Props = {
  image: string;
  isOpen: boolean;
  onClose: () => void;
};
const ImageModal = (props: Props) => {
  return (
    <ReactModal isOpen={props.isOpen} onRequestClose={props.onClose} style={customStyles}>
      <div>
        <img className="max-h-[560px]" src={props.image} />

        <div className="flex justify-end items-center mt-10">
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm"
            onClick={() => props.onClose()}>
            閉じる
          </Button>
        </div>
      </div>
    </ReactModal>
  );
};

export {ImageModal};
