import React from 'react';
import {Link, useParams} from 'react-router-dom';
import {useCreateProjectImageReportMutation, useProjectQuery} from '@/api/index';
import {DocumentDownloadIcon} from '@/components/icons';
import {Layout} from '../Layout';
import {ImageList} from './ImageList';

const Image = () => {
  const {id, roomTypeId} = useParams<{id: string; roomTypeId: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}, fetchPolicy: 'cache-and-network'});
  const [createProjectImageReport] = useCreateProjectImageReportMutation({
    onCompleted: ({createProjectImageReport: {project}}) => (window.location.href = project.imageReportUrl),
  });
  const handleCreateProjectImageReport = () => {
    createProjectImageReport({variables: {id}});
  };

  if (!project) return null;
  console.log('🚀 ~ file: ImageBase.tsx ~ line 16 ~ Image ~ project', project);

  return (
    <Layout>
      {project.imageReportUrl && (
        <a className="block mt-6" onClick={handleCreateProjectImageReport}>
          <span className="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
            <DocumentDownloadIcon className="flex-shrink-0 w-5 h-5" />
            <span>Excelレポート</span>
          </span>
        </a>
      )}

      <nav className="flex space-x-4" aria-label="Tabs">
        <Link to={`/projects/${id}/images`}>
          <div className="px-3 py-2 text-sm font-medium text-gray-800 bg-gray-200 rounded-md hover:text-gray-800 ">
            塗装前
          </div>
        </Link>

        <Link to={`/projects/${id}/images/other`}>
          <div className="px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-800">塗装以降</div>
        </Link>
      </nav>

      <div className="mt-6">
        <nav className="flex space-x-1">
          {project.room?.roomTypes?.map((roomType) => (
            <Link
              key={roomType.id}
              to={`/projects/${id}/images/${roomType.id}`}
              className={`text-gray-500 hover:text-gray-700 w-24 border border-gray-300  bg-white py-2 text-sm font-medium text-center hover:bg-gray-200 rounded-sm ${
                roomType.id === roomTypeId ? 'bg-gray-200 border border-gray-900' : ''
              }`}>
              <span>{roomType.name}</span>
              <span aria-hidden="true" className={'bg-transparent absolute inset-x-0 bottom-0 h-0.5'} />
            </Link>
          ))}
        </nav>
      </div>
      {roomTypeId && <ImageList project={project} roomTypeId={roomTypeId} />}
    </Layout>
  );
};

export {Image};
