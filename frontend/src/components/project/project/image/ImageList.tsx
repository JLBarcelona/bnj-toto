import React, {useState} from 'react';
import {Project, useCreateProjectImageMutation} from '@/api/index';
import _ from 'lodash';
import {ImageListContainer} from './ImageListContainer';
import {Button} from '@/components/ui';

type Props = {
  project: Project;
  roomTypeId: string;
};
const ImageList = ({project, roomTypeId}: Props) => {
  const [count, setCount] = useState(1);
  const imageTypes = [
    {name: '壁1', value: 'wall1'},
    {name: '壁2', value: 'wall2'},
    {name: '壁3', value: 'wall3'},
    {name: '壁4', value: 'wall4'},
    {name: '床', value: 'floor'},
    {name: '天井', value: 'roof'},
  ];
  const [createProjectImage, {loading}] = useCreateProjectImageMutation();

  const handleCreate = (imageType: string) => {
    createProjectImage({
      variables: {count, attributes: {projectId: project.id, roomTypeId, imageType}},
    });
  };

  const filterImages = (imageType: string) => {
    const images = project.projectImages.filter(
      (projectImage) => projectImage.imageType === imageType && projectImage.roomTypeId === roomTypeId,
    );
    return _.sortBy(images, ['position']);
  };

  const imageCount = [1, 2, 3, 4, 5, 10, 20, 30];

  return (
    <div>
      {imageTypes.map((imageType) => (
        <div className="mt-10 w-[1100px]" key={imageType.value}>
          <h3 className="text-lg font-bold">{imageType.name}</h3>
          <ImageListContainer project={project} roomTypeId={roomTypeId} projectImages={filterImages(imageType.value)} />
          <div className="inline-flex mt-4">
            <select
              name="location"
              className="block py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              defaultValue={count}
              onChange={(e) => setCount(Number(e.target.value))}>
              {imageCount.map((count) => (
                <option key={count}>{count}</option>
              ))}
            </select>
            <Button
              type="button"
              className="block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              onClick={() => handleCreate(imageType.value)}
              loading={loading}>
              追加
            </Button>
          </div>
        </div>
      ))}
    </div>
  );
};

export {ImageList};
