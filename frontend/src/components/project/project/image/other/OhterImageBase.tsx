import React from 'react';
import {Link, useParams} from 'react-router-dom';
import {useProjectQuery} from '@/api/index';
import {DocumentDownloadIcon} from '@/components/icons';
import {Layout} from '../../Layout';
import {ImageList} from './ImageList';

const OtherImageBase = () => {
  const {id, roomTypeId} = useParams<{id: string; roomTypeId: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}, fetchPolicy: 'cache-and-network'});

  if (!project) return null;

  return (
    <Layout>
      {project.imageReportUrl && (
        <a href={project.imageReportUrl} className="block mt-6">
          <span className="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
            <DocumentDownloadIcon className="flex-shrink-0 w-5 h-5" />
            <span>Excelレポート</span>
          </span>
        </a>
      )}

      <nav className="flex space-x-4" aria-label="Tabs">
        <Link to={`/projects/${id}/images`}>
          <div className="px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-800">塗装前</div>
        </Link>

        <Link to={`/projects/${id}/images/other`}>
          <div className="px-3 py-2 text-sm font-medium text-gray-800 bg-gray-200 rounded-md hover:text-gray-800 ">
            塗装以降
          </div>
        </Link>
      </nav>

      <ImageList project={project} />
    </Layout>
  );
};

export {OtherImageBase};
