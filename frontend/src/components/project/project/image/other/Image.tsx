import {Project, ProjectImage, useRemoveProjectImageMutation, useUpdateProjectImageMutation} from '@/api/index';
import React, {useRef, useState} from 'react';
import {ImageModal} from '../modal';
import {ImageComment} from './ImageComment';

type Props = {
  project: Project;
  projectImage: ProjectImage;
};

const Image = ({project, projectImage}: Props) => {
  const [image, setImage] = useState(null);
  const FileRef = useRef(null);

  const [updateProjectImage] = useUpdateProjectImageMutation();
  const [removeProjectImage] = useRemoveProjectImageMutation();

  const uploadFile = (file: any) => {
    let values = null;
    values = {image: file};

    updateProjectImage({
      variables: {
        id: projectImage.id,
        attributes: values,
      },
    });

    return;
  };

  const handleFile = (e: any) => {
    const file = e.target.files[0];
    if (!file) {
      return;
    }

    uploadFile(file);
  };

  const handleRevemoFile = () => {
    removeProjectImage({variables: {id: projectImage.id}});
  };

  return (
    <div className="mt-4">
      <div className="text-gray-500 text-sm">#{projectImage.position}</div>
      <div className="flex h-72">
        <div className="flex p-4 border border-gray-300 w-96">
          <div className="w-64 h-64">
            {projectImage?.image && (
              <img
                className="h-full w-full object-contain cursor-pointer"
                src={projectImage?.image}
                onClick={() => setImage(projectImage.image)}
              />
            )}
          </div>
          <div className="mt-2 ml-2">
            <button
              type="button"
              className="block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              onClick={() => FileRef.current.click()}>
              登録
            </button>
            <button
              type="button"
              className="block px-2 py-2 text-xs font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              onClick={handleRevemoFile}>
              削除
            </button>

            <input
              id="file"
              name="file"
              type="file"
              accept=".jpg,.gif,.png,image/gif,image/jpeg,image/png"
              ref={FileRef}
              style={{display: 'none'}}
              onChange={(e) => {
                handleFile(e);
              }}
            />
          </div>
        </div>
        <div className="px-2 align-top border border-gray-300 w-44">
          <ImageComment project={project} projectImage={projectImage} onClose={() => {}} />
        </div>
      </div>

      <ImageModal isOpen={!!image} image={image} onClose={() => setImage(null)} />
    </div>
  );
};

export {Image};
