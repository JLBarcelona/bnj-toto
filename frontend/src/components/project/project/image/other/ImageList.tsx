import React, {useState} from 'react';
import {Project, useCreateProjectImageMutation} from '@/api/index';
import _ from 'lodash';
import {ImageListContainer} from './ImageListContainer';
import {Button} from '@/components/ui';

type Props = {
  project: Project;
};
const ImageList = ({project}: Props) => {
  const [count, setCount] = useState(1);
  const [createProjectImage, {loading}] = useCreateProjectImageMutation();

  const handleCreate = (imageType: string) => {
    createProjectImage({
      variables: {count, attributes: {projectId: project.id, imageType}},
    });
  };

  const filterImages = () => {
    const images = project.projectImages.filter((projectImage) => projectImage.imageType === 'other');
    return _.sortBy(images, ['position']);
  };

  return (
    <div>
      <div className="mt-10">
        <ImageListContainer project={project} projectImages={filterImages()} />
        <div className="inline-flex mt-4">
          <select
            name="location"
            className="block py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
            defaultValue={count}
            onChange={(e) => setCount(Number(e.target.value))}>
            <option>1</option>
            <option>5</option>
            <option>10</option>
            <option>20</option>
            <option>30</option>
          </select>
          <Button
            type="button"
            className="block px-2 py-2 ml-auto text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={() => {
              handleCreate('other');
            }}
            loading={loading}>
            追加
          </Button>
        </div>
      </div>
    </div>
  );
};

export {ImageList};
