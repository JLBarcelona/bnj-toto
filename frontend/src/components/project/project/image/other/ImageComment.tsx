import {Project, ProjectImage, useUpdateProjectImageMutation} from '@/api/index';
import {useFormik} from 'formik';
import React, {useState} from 'react';
import ReactModal from 'react-modal';
import {CommentList} from '../comment/CommentList';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    height: '600px',
  },
  overlay: {
    zIndex: '10',
  },
};
type Props = {
  project: Project;
  projectImage?: ProjectImage;
  onClose: () => void;
};
const ImageComment = ({project, projectImage, onClose}: Props) => {
  const [updateProjectImage] = useUpdateProjectImageMutation();
  const [isOpen, setOpen] = useState(false);
  const handleClose = () => {
    setOpen(false);
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      comment: projectImage?.comment,
    },
    onSubmit: (values) => {
      if (projectImage) {
        updateProjectImage({
          variables: {
            id: projectImage.id,
            attributes: values,
          },
        });
      } else {
        // createProjectImage({
        //   variables: {attributes: {projectId: project.id, imageType, ...values}},
        // });
      }

      onClose();
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <button
        type="button"
        onClick={() => setOpen(true)}
        className="block h-8 px-2 py-1 text-xs text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm ont-medium hover:bg-gray-50">
        編集
      </button>
      <ReactModal isOpen={isOpen} onRequestClose={handleClose} style={customStyles}>
        <CommentList project={project} projectImage={projectImage} />
      </ReactModal>
      <table className="w-full border border-gray-200 divide-y divide-gray-200">
        <tbody className="flex flex-col overflow-y-scroll bg-white divide-y divide-gray-400" style={{height: '180px'}}>
          {projectImage?.comments?.map((comment) => (
            <tr className="flex bg-white" key={comment?.id}>
              <td className="flex-1 px-1 py-1 text-xs">{comment.comment}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </form>
  );
};

export {ImageComment};
