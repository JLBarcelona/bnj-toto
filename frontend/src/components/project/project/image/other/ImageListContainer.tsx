import React, {useEffect, useState} from 'react';
import {Project, ProjectImage} from '@/api/index';
import {Image} from './Image';
import _ from 'lodash';

type Props = {
  project: Project;
  projectImages: Array<ProjectImage>;
};
const ImageListContainer = ({project, projectImages}: Props) => {
  const [images, setImages] = useState([]);

  useEffect(() => {
    setImages(projectImages);
  }, [projectImages]);

  return (
    <div className="flex flex-wrap w-full gap-2">
      {images.map((projectImage) => (
        <Image key={projectImage.id} project={project} projectImage={projectImage} />
      ))}
    </div>
  );
};

export {ImageListContainer};
