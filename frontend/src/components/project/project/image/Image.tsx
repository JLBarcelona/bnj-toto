import {Project, ProjectImage, useRemoveProjectImageMutation, useUpdateProjectImageMutation} from '@/api/index';
import {EllipseVerticalIcon, TrashIcon} from '@/components/icons';
import type {Identifier, XYCoord} from 'dnd-core';
import React, {useRef, useState} from 'react';
import {useDrag, useDrop} from 'react-dnd';
import {ImageComment} from './ImageComment';
import {ImageModal} from './modal';

type Props = {
  index: number;
  id: string;
  project: Project;
  projectImage: ProjectImage;
  roomTypeId: string;
  imageType: string;
  moveImage: (id: string, dragIndex: number, hoverIndex: number) => void;
  order: (id: string, index: number) => void;
};

interface DragItem {
  index: number;
  id: string;
  type: string;
}

const Image = ({project, id, projectImage, roomTypeId, imageType, index, moveImage, order}: Props) => {
  const [image, setImage] = useState(null);

  const FileBeforeRef = useRef(null);
  const FileAfterRef = useRef(null);
  const [isCommentForm, setCommentForm] = useState(false);

  const [updateProjectImage] = useUpdateProjectImageMutation();
  const [removeProjectImage] = useRemoveProjectImageMutation();

  const uploadFile = (file: any, fileType: 'before' | 'after') => {
    let values = null;
    if (fileType === 'before') {
      values = {imageBefore: file};
    } else {
      values = {imageAfter: file};
    }

    updateProjectImage({
      variables: {
        id: projectImage.id,
        attributes: values,
      },
    });

    return;
  };

  const handleRevemoFileBefore = () => {
    uploadFile(null, 'before');
  };

  const handleRemoveProjectImage = () => {
    removeProjectImage({variables: {id}});
  };

  const handleRevemoFileAfter = () => {
    uploadFile(null, 'after');
  };

  const handleFileBefore = (e: any) => {
    const file = e.target.files[0];
    if (!file) {
      return;
    }

    uploadFile(file, 'before');
  };

  const handleFileAfter = (e: any) => {
    const file = e.target.files[0];
    if (!file) {
      return;
    }

    uploadFile(file, 'after');
  };

  const ref = useRef<HTMLDivElement>(null);
  const [{handlerId}, drop] = useDrop<DragItem, void, {handlerId: Identifier | null}>({
    accept: 'Image',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item: DragItem, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      // Time to actually perform the action
      moveImage(item.id, dragIndex, hoverIndex);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex;
    },
  });

  const [{isDragging}, drag] = useDrag({
    type: 'Image',
    item: () => {
      return {id, index};
    },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
    end: (item) => {
      console.log('🚀 ~ file: Image.tsx ~ line 147 ~ Image ~ item', item);
      order(item.id, item.index);
    },
  });

  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));

  return (
    <div className="flex mt-4 h-72" ref={ref} style={{opacity}} data-handler-id={handlerId}>
      <div className="w-8 my-auto">
        <EllipseVerticalIcon className="cursor-pointer w-8" />
      </div>
      <div className="flex p-4 border border-gray-300 w-96">
        <div className="w-64 h-64">
          {projectImage?.imageBefore && (
            <img
              className="h-full w-full object-contain cursor-pointer"
              src={projectImage?.imageBefore}
              onClick={() => setImage(projectImage.imageBefore)}
            />
          )}
        </div>
        <div className="mt-2 ml-4">
          <button
            type="button"
            className="block px-4 py-2 ml-auto text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={() => FileBeforeRef.current.click()}>
            登録
          </button>
          <button
            type="button"
            className="block px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={handleRevemoFileBefore}>
            削除
          </button>

          <input
            id="fileBefore"
            name="fileBefore"
            type="file"
            accept=".jpg,.gif,.png,image/gif,image/jpeg,image/png"
            ref={FileBeforeRef}
            style={{display: 'none'}}
            onChange={(e) => {
              handleFileBefore(e);
            }}
          />
        </div>
      </div>
      <div className="flex p-4 border border-gray-300 w-96">
        <div className="w-64 h-64">
          {projectImage?.imageAfter && (
            <img
              className="h-full w-full object-contain cursor-pointer"
              src={projectImage?.imageAfter}
              onClick={() => setImage(projectImage.imageAfter)}
            />
          )}
        </div>
        <div className="mt-2 ml-4">
          <button
            type="button"
            className="block px-4 py-2 ml-auto text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={() => FileAfterRef.current.click()}>
            登録
          </button>
          <button
            type="button"
            className="block px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            onClick={handleRevemoFileAfter}>
            削除
          </button>

          <input
            id="fileAfter"
            name="fileAfter"
            type="file"
            accept=".jpg,.gif,.png,image/gif,image/jpeg,image/png"
            ref={FileAfterRef}
            style={{display: 'none'}}
            onChange={(e) => {
              e.preventDefault();
              handleFileAfter(e);
            }}
          />
        </div>
      </div>
      <div className="p-4 align-top border border-gray-300 w-72" onClick={() => setCommentForm(true)}>
        {!isCommentForm && <div className="whitespace-pre">{projectImage?.comment}</div>}

        <ImageComment project={project} projectImage={projectImage} onClose={() => setCommentForm(false)} />
      </div>
      <TrashIcon className="w-4 ml-2 text-red-500 cursor-pointer" onClick={() => handleRemoveProjectImage()} />

      <ImageModal isOpen={!!image} image={image} onClose={() => setImage(null)} />
    </div>
  );
};

export {Image};
