const closeTypeValues = [
  {value: 'close_type1', name: '閉鎖住宅'},
  {value: 'close_type2', name: '私負担清算'},
  {value: 'close_type3', name: '本工事'},
  {value: 'close_type4', name: '工事保留'},
  {value: 'close_type5', name: '工事中止'},
  {value: 'close_type6', name: '工事再開'},
  {value: 'close_type7', name: '被災者住宅'},
  {value: 'close_type8', name: '検査対象物件'},
  {value: 'close_type9', name: 'その他'},
];

export {closeTypeValues};
