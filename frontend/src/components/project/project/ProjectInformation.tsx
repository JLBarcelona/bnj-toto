import {Project, useCurrentUserQuery, useRemoveProjectMutation, useUpdateProjectMutation} from '@/api/index';
import {EllipseVerticalIcon, TrashIcon} from '@/components/icons';
import {Menu} from '@headlessui/react';
import {useFormik} from 'formik';
import React from 'react';
import {useNavigate} from 'react-router-dom';

const CheckBox = (props: {label: string; name: string; checked: boolean; onChange: (e: any) => void}) => {
  return (
    <div className="flex items-center space-x-1">
      <input
        id={props.name}
        name={props.name}
        type="checkbox"
        className="w-5 h-6 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
        checked={props.checked}
        onChange={(e) => {
          props.onChange(e);
        }}
      />
      <label className="block text-xs font-medium text-gray-700">{props.label}</label>
    </div>
  );
};

type StatsProps = {
  date: string;
};
const Stats = (props: StatsProps) => {
  return (
    <div className="mt-1 text-indigo-700 font-bold">
      <p>{props.date && props.date.split(' ')[0]}</p>
      <p>{props.date && props.date.split(' ')[1]}</p>
    </div>
  );
};

type Props = {
  project: Project;
};
const ProjectInformation = ({project}: Props) => {
  const navigate = useNavigate();
  const {data: {currentUser} = {}} = useCurrentUserQuery();
  const isReadOnly = !!(project.editUserId && project.editUserId !== currentUser.id);

  const [udpateProject] = useUpdateProjectMutation();
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      jkkStatus1: project.jkkStatus1,
      jkkStatus2: project.jkkStatus2,
      jkkStatus3: project.jkkStatus3,
      jkkStatus4: project.jkkStatus4,
    },
    onSubmit: (values) => {
      udpateProject({variables: {id: project.id, attributes: values}});
    },
  });

  const [remove] = useRemoveProjectMutation({onCompleted: () => navigate('/projects')});

  const handleRemoveProject = () => {
    if (confirm('削除してよろしいですか？')) {
      remove({variables: {id: project.id}});
    }
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="flex items-center justify-between">
        <div className="my-4 font-bold text-2xl">
          指示番号：{project.orderNumberType}-{project.orderNumber}
        </div>
        {isReadOnly && (
          <div className="ml-auto">
            <div className="text-right">
              <div className="inline-flex items-center border border-gray-500 px-4 py-2">
                <span className="text-xs text-gray-500">編集中：</span>
                <div className="text-sm text-gray-500">
                  {project.editUser?.lastName} {project.editUser?.firstName}
                </div>
              </div>
            </div>

            <p className="text-red-600">他ユーザーが編集中のため更新できません</p>
          </div>
        )}
      </div>
      <div className="flex items-center gap-8">
        <div className="flex items-center space-x-2 text-xs">
          <span className="text-gray-500">件名：</span>
          <span className="text-gray-900 text-base font-medium">{project.building?.name}</span>
          <span className="text-gray-900 text-base font-medium">
            {project.buildingNumber?.name}
            <span className="ml-2 text-gray-500">号棟</span>
          </span>
          <span className="text-gray-900 text-base font-medium">
            {project.room?.name}
            <span className="ml-2 text-gray-500">号室</span>
          </span>
        </div>

        <div className="flex items-center space-x-2">
          <span className="text-xs font-medium text-gray-500">住所：</span>
          <span className="font-medium text-gray-900 text-md">{project.building?.address}</span>
        </div>

        <Menu as="div" className="relative flex text-left ml-4">
          <Menu.Button>
            <EllipseVerticalIcon className="h-6 text-gray-500 cursor-pointer" />
          </Menu.Button>
          <Menu.Items className="absolute right-0 mt-2 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none py-2 px-4 whitespace-pre">
            {() => (
              <button
                className={`hover:bg-gray-300 text-gray-500 flex items-center rounded-md px-2 py-2 text-sm`}
                onClick={() => handleRemoveProject()}>
                <TrashIcon className="h-4 mr-2" />
                削除
              </button>
            )}
          </Menu.Items>
        </Menu>
      </div>

      <div className="flex my-2 space-x-7 text-gray-500 text-sm">
        <div>公社年度：{project.year.year}</div>
        <div>管轄窓口：{project.building?.mcType?.name}</div>
        <div>住宅区分：{project.building?.buildingType?.name}</div>

        <div className="flex items-center space-x-2">
          <label className="block text-sm font-medium">工事金額：</label>
          <span>
            {project.totalPriceInput?.toLocaleString()}
            <span className="ml-1 text-sm text-gray-700">円</span>
          </span>
        </div>
      </div>

      <div className="flex mt-2 items-center">
        <div className="text-xs font-medium text-gray-500 mr-4">当社状況</div>
        <div className="flex items-start space-x-1 h-[60px]">
          <div className="text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center">
            <div>①査定表読込済</div>
            <Stats date={project.itemUploadDateText} />
          </div>
          <div className="text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center">
            <div>②施工前済・質疑回答待ち</div>
            <Stats date={project.reportDateText} />
          </div>
          <div className="text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center">
            <div>③施工前済・最終CL済</div>
            <Stats date={project.checklistDateText} />
          </div>
          <div className="text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center">
            <div>④施工後不足写真待ち</div>
            <Stats date={project.lackImageDateText} />
          </div>
          <div className="text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center">
            <div>⑤施工後済</div>
            <Stats date={project.completeImageDateText} />
          </div>
          <div className="text-xs bg-white px-1 py-2 h-full rounded-sm w-36 text-center">
            <div>⑥請求書済</div>
            <Stats date={project.invoiceDateText} />
          </div>
        </div>
      </div>

      <div className="flex items-center mt-2">
        <div className="text-xs font-medium text-gray-500 mr-4">JKK状況</div>
        <div className="flex space-x-4">
          <CheckBox
            label="①CL済"
            name="jkkStatus1"
            checked={formik.values.jkkStatus1}
            onChange={(e) => {
              formik.setFieldValue('jkkStatus1', e.target.checked);
              formik.handleSubmit();
            }}
          />

          <CheckBox
            label="②指示書済"
            name="jkkStatus2"
            checked={formik.values.jkkStatus2}
            onChange={(e) => {
              formik.setFieldValue('jkkStatus2', e.target.checked);
              formik.handleSubmit();
            }}
          />

          <CheckBox
            label="③指示書再発行待ち"
            name="jkkStatus3"
            checked={formik.values.jkkStatus3}
            onChange={(e) => {
              formik.setFieldValue('jkkStatus3', e.target.checked);
              formik.handleSubmit();
            }}
          />

          <CheckBox
            label="④指示書再発行済み"
            name="jkkStatus4"
            checked={formik.values.jkkStatus4}
            onChange={(e) => {
              formik.setFieldValue('jkkStatus4', e.target.checked);
              formik.handleSubmit();
            }}
          />
        </div>
      </div>
    </form>
  );
};

export {ProjectInformation};
