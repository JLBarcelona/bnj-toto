import {ProjectReport, useCreateSpreadsheetMutation, useUpdateProjectReportMutation} from '@/api/index';
import {useFormik} from 'formik';
import moment from 'moment';
import React from 'react';
import {ReportRow} from './ReportRow';

type Props = {
  projectReport: ProjectReport;
};
const Report = ({projectReport}: Props) => {
  const [createSpreadsheet, {loading}] = useCreateSpreadsheetMutation();
  const [updateProjectReport] = useUpdateProjectReportMutation();

  const formik = useFormik({
    initialValues: {
      spreadsheetId1: projectReport.spreadsheetId1,
      spreadsheetId2: projectReport.spreadsheetId2,
      spreadsheetId3: projectReport.spreadsheetId3,
      spreadsheetId4: projectReport.spreadsheetId4,
      spreadsheetId5: projectReport.spreadsheetId5,
      spreadsheetId6: projectReport.spreadsheetId6,
      spreadsheetId7: projectReport.spreadsheetId7,
      spreadsheetCheckReport1: projectReport.spreadsheetCheckReport1,
      spreadsheetCheckReport2: projectReport.spreadsheetCheckReport2,
    },
    onSubmit: (_) => {},
  });

  const handleSubmit = (
    reportType: 'assesment' | 'orderMaterial' | 'cleaning' | 'checklist' | 'order' | 'report1' | 'report2',
  ) => {
    createSpreadsheet({variables: {id: projectReport.id, reportType, attributes: formik.values}});
  };

  return (
    <form onSubmit={formik.handleSubmit}>
      <ReportRow
        name="spreadsheetId1"
        label="査定表"
        value={formik.values.spreadsheetId1}
        datetime={
          projectReport.spreadsheetDatetime1 && moment(projectReport.spreadsheetDatetime1).format('YYYY/MM/DD HH:mm')
        }
        reportType="assesment"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
      />

      <ReportRow
        name="spreadsheetId2"
        label="材料発注"
        value={formik.values.spreadsheetId2}
        datetime={
          projectReport.spreadsheetDatetime2 && moment(projectReport.spreadsheetDatetime2).format('YYYY/MM/DD HH:mm')
        }
        reportType="orderMaterial"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
      />

      <ReportRow
        name="spreadsheetId3"
        label="クリーニング業者"
        value={formik.values.spreadsheetId3}
        datetime={
          projectReport.spreadsheetDatetime3 && moment(projectReport.spreadsheetDatetime3).format('YYYY/MM/DD HH:mm')
        }
        reportType="cleaning"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
      />

      <ReportRow
        name="spreadsheetId4"
        label="チェック表"
        value={formik.values.spreadsheetId4}
        datetime={
          projectReport.spreadsheetDatetime4 && moment(projectReport.spreadsheetDatetime4).format('YYYY/MM/DD HH:mm')
        }
        reportType="checklist"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
      />

      <ReportRow
        name="spreadsheetId5"
        label="発注書（塗装/木工/雑工）"
        value={formik.values.spreadsheetId5}
        datetime={
          projectReport.spreadsheetDatetime5 && moment(projectReport.spreadsheetDatetime5).format('YYYY/MM/DD HH:mm')
        }
        reportType="order"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
      />

      <ReportRow
        name="spreadsheetId6"
        label="写真報告書①"
        value={formik.values.spreadsheetId6}
        datetime={
          projectReport.spreadsheetDatetime6 && moment(projectReport.spreadsheetDatetime6).format('YYYY/MM/DD HH:mm')
        }
        reportType="report1"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
        checkName="spreadsheetCheckReport1"
        checkValue={formik.values.spreadsheetCheckReport1}
        handleFieldValue={(value: boolean) => {
          formik.setFieldValue('spreadsheetCheckReport1', value);
          updateProjectReport({variables: {id: projectReport.id, attributes: {spreadsheetCheckReport1: value}}});
        }}
      />

      <ReportRow
        name="spreadsheetId7"
        label="写真報告書②"
        value={formik.values.spreadsheetId7}
        datetime={
          projectReport.spreadsheetDatetime7 && moment(projectReport.spreadsheetDatetime7).format('YYYY/MM/DD HH:mm')
        }
        reportType="report2"
        handleChange={formik.handleChange}
        handleBlue={formik.handleBlur}
        handleSubmit={handleSubmit}
        checkName="spreadsheetCheckReport2"
        checkValue={formik.values.spreadsheetCheckReport2}
        handleFieldValue={(value: boolean) => {
          formik.setFieldValue('spreadsheetCheckReport2', value);
          updateProjectReport({variables: {id: projectReport.id, attributes: {spreadsheetCheckReport2: value}}});
        }}
      />
    </form>
  );
};

export {Report};
