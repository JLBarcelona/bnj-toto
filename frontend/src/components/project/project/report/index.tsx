import React from 'react';
import {useParams} from 'react-router-dom';
import {useFormik} from 'formik';
import {Layout} from '../Layout';
import {useProjectQuery} from '@/api/index';
import {Report} from './Report';

const ProjectReport = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}});

  const formik = useFormik({
    initialValues: {},
    onSubmit: (values) => {
      console.log('🚀 ~ file: index.tsx:20 ~ ProjectReport ~ values', values);
    },
  });

  if (!project) return null;

  return (
    <Layout>
      <div className="mt-2">
        <div className="text-lg font-bold">レポート出力</div>
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
              <div className="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
                <div className="min-w-full divide-y divide-gray-200">
                  <div className="flex flex-1 bg-gray-50">
                    <div className="w-64 px-2 py-3 ml-4 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                      レポート
                    </div>
                    <div className="px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase w-64">
                      スプレッドシートID
                    </div>
                    <div className="px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase w-24" />
                    <div className="px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                      最終出力日時
                    </div>
                  </div>
                  <Report projectReport={project.projectReport} />
                  {/* <ReportRow label="査定表" projectReport={project.projectReport} type="assesment" />
                  <ReportRow label="材料発注" projectReport={project.projectReport} type="orderMaterial" />
                  <ReportRow label="月次発注" projectReport={project.projectReport} type="orderMonthly" />
                  <ReportRow label="チェック表" projectReport={project.projectReport} type="checklist" />
                  <ReportRow label="工事写真報告書表1" projectReport={project.projectReport} type="report1" />
                  <ReportRow label="工事写真報告書表2" projectReport={project.projectReport} type="report2" /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export {ProjectReport};
