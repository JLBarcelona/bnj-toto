import React from 'react';

type Props = {
  label: string;
  name: string;
  checkName?: string;
  value: string;
  checkValue?: boolean;
  datetime: string;
  reportType: 'assesment' | 'orderMaterial' | 'cleaning' | 'checklist' | 'order' | 'report1' | 'report2';
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleBlue: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleSubmit: (
    reportType: 'assesment' | 'orderMaterial' | 'cleaning' | 'checklist' | 'order' | 'report1' | 'report2',
  ) => void;
  handleFieldValue?: (value: boolean) => void;
};
const ReportRow = (props: Props) => {
  return (
    <div className="bg-white divide-y divide-gray-200">
      <div className="flex">
        <div className="w-64 px-4 py-4 ml-4">
          <div className="flex items-center">
            <div className="text-sm font-medium text-gray-900">{props.label}</div>
          </div>
        </div>
        <div className="px-2 py-2 w-64">
          <div className="flex items-center">
            <div className="flex rounded-md shadow-sm w-full">
              <input
                type="text"
                name={props.name}
                value={props.value}
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                onChange={props.handleChange}
                onBlur={props.handleBlue}
              />
            </div>
          </div>
        </div>

        <div className="px-2 py-2 w-24">
          <button
            type="button"
            className=" px-2 py-2 text-xs font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 disabled:opacity-30"
            disabled={!props.value}
            onClick={() => props.handleSubmit(props.reportType)}>
            出力
          </button>
        </div>

        <div className="text-xs text-gray-500 my-auto w-32">
          <p>{props.datetime}</p>
        </div>

        {['report1', 'report2'].includes(props.reportType) && (
          <div className="text-xs text-gray-500 my-auto">
            <input
              name={props.checkName}
              type="checkbox"
              checked={props.checkValue}
              className="w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
              onChange={(e) => {
                props.handleFieldValue(e.target.checked);
              }}
              onBlur={props.handleBlue}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export {ReportRow};
