import {useProjectOrdersQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {Order} from './Order';

const EachOrderList = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectOrdersQuery({variables: {id}, fetchPolicy: 'cache-and-network'});

  if (!project) return null;

  return (
    <div className="flex flex-col mt-10">
      <div className="text-lg font-bold">都度発注</div>

      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <div className="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
            <div className="min-w-full divide-y divide-gray-200">
              <div className="flex flex-1 bg-gray-50">
                <div className="w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                  会社
                </div>
                <div className="w-48 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                  発注日
                </div>
                <div className="w-32 px-4 py-3 text-xs font-medium tracking-wider text-center text-gray-500 uppercase">
                  請求書確認
                </div>
              </div>
              <div className="bg-white divide-y divide-gray-200">
                {project.ordersEach.map((order) => (
                  <Order key={order.id} order={order} />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export {EachOrderList};
