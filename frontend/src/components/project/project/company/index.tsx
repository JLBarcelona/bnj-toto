import {useProjectQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {Layout} from '../Layout';
import {EachOrderList} from './EachOrderList';
import {MonthlyOrderList} from './MonthlyOrderList';

const Company = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}, fetchPolicy: 'cache-and-network'});

  if (!project) return null;

  return (
    <Layout>
      <EachOrderList />
      <MonthlyOrderList />
    </Layout>
  );
};

export {Company};
