import {Order, useUpdateProjectOrderMutation} from '@/api/index';
import ja from 'date-fns/locale/ja';
import {useFormik} from 'formik';
import moment from 'moment';
import React from 'react';
import DatePicker, {registerLocale} from 'react-datepicker';

registerLocale('ja', ja);

interface Props {
  order: Order;
}

const Order = ({order}: Props) => {
  const [updateProjectOrder] = useUpdateProjectOrderMutation();

  const formik = useFormik({
    initialValues: {
      companyId: order.companyId,
      isInvoice: order.isInvoice,
      orderDate: order.orderDate,
    },
    onSubmit: (values) => {
      updateProjectOrder({variables: {id: order.id, attributes: values}});
    },
  });

  return (
    <form className="flex" onSubmit={formik.handleSubmit}>
      <div className="w-64 px-4 ml-4">
        <div className="flex items-center">{order.company.name}</div>
      </div>

      <div className="w-48 px-4">
        <div className="flex items-center">
          <div className="flex rounded-md shadow-sm">
            <DatePicker
              locale="ja"
              dateFormat={'yyyy/MM/dd'}
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              selected={formik.values.orderDate && new Date(formik.values.orderDate)}
              onChange={async (date) => {
                await formik.setFieldValue('orderDate', moment(date).format('YYYY-MM-DD'));
                formik.handleSubmit();
              }}
            />
          </div>
        </div>
      </div>

      <div className="w-32 px-4 my-auto">
        <div className="flex justify-center rounded-md shadow-sm">
          <input
            type="checkbox"
            name="isInvoice"
            className="w-4 h-4 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500"
            checked={formik.values.isInvoice}
            onChange={async (e) => {
              await formik.setFieldValue('isInvoice', e.target.checked);
              formik.handleSubmit();
            }}
          />
        </div>
      </div>

      {order.orderType === 'monthly' && <div className="w-20 text-sm">{order.exportCsvDate}</div>}
    </form>
  );
};
export {Order};
