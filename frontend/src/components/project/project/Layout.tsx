import React from 'react';
import {useParams} from 'react-router-dom';
import {useProjectQuery} from '@/api/index';
import {Breadcrumb} from './Breadcrumb';
import {Tab} from './Tab';
import {ProjectInformation} from './ProjectInformation';

const Layout = ({children}) => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}});

  if (!project) return null;

  return (
    <div className="px-6">
      <div className="top-0 bg-gray-100 z-10 sticky pt-4">
        <Breadcrumb project={project} />
        <ProjectInformation project={project} />
        <Tab />
      </div>
      <div className="pb-10">{children}</div>
    </div>
  );
};

export {Layout};
