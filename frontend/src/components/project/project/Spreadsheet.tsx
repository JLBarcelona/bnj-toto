import React from 'react';
import {useFormik} from 'formik';
import {Project, useCreateSpreadsheetMutation} from '@/api/index';
import {Button} from '@/components/ui';

type Props = {
  project: Project;
};
const Spreadsheet = ({project}: Props) => {
  const [createSpreadsheet, {loading}] = useCreateSpreadsheetMutation();
  const formik = useFormik({
    initialValues: {
      spreadsheetId: project.spreadsheetId,
      worksheetBase: project.worksheetBase,
      worksheetItems: project.worksheetItems,
      worksheetImages: project.worksheetImages,
      worksheetChecklist: project.worksheetChecklist,
    },
    onSubmit: (values) => {
      createSpreadsheet({variables: {id: project.id, attributes: values}});
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="flex text-xs">
        <label className="leading-9 mr-2">スプレッドシートID</label>
        <input
          type="text"
          name="spreadsheetId"
          className="w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs"
          value={formik.values.spreadsheetId}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
      </div>
      <div className="flex space-x-4">
        <div className="text-xs">
          <label className="leading-9 mr-2">基本情報</label>
          <input
            type="text"
            name="worksheetBase"
            className="w-64 px-4 py-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs"
            value={formik.values.worksheetBase}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>

        <div className="text-xs">
          <label className="leading-9 mr-2">明細</label>
          <input
            type="text"
            name="worksheetItems"
            className="w-64 px-4 py-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs"
            value={formik.values.worksheetItems}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>

        <div className="text-xs">
          <label className="leading-9 mr-2">画像</label>
          <input
            type="text"
            name="worksheetImages"
            className="w-64 px-4 py-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs"
            value={formik.values.worksheetImages}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>

        <div className="text-xs">
          <label className="leading-9 mr-2">チェックリスト</label>
          <input
            type="text"
            name="worksheetChecklist"
            className="w-64 px-4 py-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs"
            value={formik.values.worksheetChecklist}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
      </div>

      <Button
        type="button"
        className="mt-1 mr-4 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        loading={loading}
        onClick={() => formik.handleSubmit()}>
        出力
      </Button>
    </form>
  );
};

export {Spreadsheet};
