import {Project, useClearProjectUserMutation, useEditProjectUserMutation, useProjectQuery} from '@/api/index';
import {Company} from '@/components/project/project/company';
import {ProjectForm} from '@/components/project/project/form';
import {ImageContainer} from '@/components/project/project/image/index';
import {ProjectItems} from '@/components/project/project/items';
import {PurchaseOrders} from '@/components/project/project/order/PurchaseOrders';
import {ProjectReport} from '@/components/project/project/report';
import React, {useEffect} from 'react';
import {Route, Routes, useParams} from 'react-router-dom';
import {Remark} from './remark';

const Project = () => {
  const {id} = useParams<{id: string}>();
  console.log('🚀 ~ file: index.tsx:15 ~ Project ~ id:', id);
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}});

  const [editUser] = useEditProjectUserMutation();
  const [clearUser] = useClearProjectUserMutation();

  useEffect(() => {
    if (project) {
      console.log('mounting');
      editUser({variables: {id: project.id}});
    }

    return () => {
      console.log('unmounting');
      if (project) {
        clearUser({variables: {id: project.id}});
      }
    };
  }, []);

  return (
    <Routes>
      <Route path="form/*" element={<ProjectForm />} />
      <Route path="orders" element={<PurchaseOrders />} />
      <Route path="images/*" element={<ImageContainer />} />
      <Route path="company" element={<Company />} />
      <Route path="items" element={<ProjectItems />} />
      <Route path="report" element={<ProjectReport />} />
      <Route path="remark" element={<Remark />} />
    </Routes>
  );
};

export {Project};
