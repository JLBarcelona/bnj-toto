import React from 'react';
import {useParams} from 'react-router-dom';
import {DocumentDownloadIcon} from '@/components/icons';
import {useProjectQuery} from '@/api/index';
import {Layout} from '../Layout';

const PurchaseOrders = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}, fetchPolicy: 'cache-and-network'});

  if (!project) return null;

  return (
    <Layout>
      <div className="mt-10">
        <div className="flex flex-col">
          <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
              <div className="overflow-hidden border-b border-gray-200 shadow sm:rounded-lg">
                <table className="min-w-full divide-y divide-gray-200">
                  <thead className="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                        業者
                      </th>
                      <th
                        scope="col"
                        className="px-6 py-3 text-xs font-medium tracking-wider text-left text-gray-500 uppercase">
                        発注書
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200">
                    {project.purchaseOrders.map((purchaseOrder) => (
                      <tr key={purchaseOrder.id}>
                        <td className="px-6 py-4">
                          <div className="flex items-center">
                            <div className="ml-4">
                              {/* <div className="text-sm font-medium text-gray-900">{purchaseOrder.contractor.name}</div> */}
                            </div>
                          </div>
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap">
                          <div className="text-sm text-gray-900">
                            {purchaseOrder.fileUrl && (
                              <a href={purchaseOrder.fileUrl}>
                                <span className="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                                  <DocumentDownloadIcon className="flex-shrink-0 w-5 h-5" />
                                  <span>発注書</span>
                                </span>
                              </a>
                            )}
                          </div>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export {PurchaseOrders};
