import React, {useState} from 'react';
import ReactModal from 'react-modal';
import {useFormik} from 'formik';
import {Item, Project, ProjectItem, useAllItemsQuery, useCreateProjectItemMutation} from '@/api/index';
import {ItemSearch} from './ItemSearch';
import {Button, Loading} from '@/components/ui';

type Props = {
  isOpen: boolean;
  project: Project;
  projectItem?: ProjectItem;
  handleClose: () => void;
  refetch: () => void;
};

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    height: '800px',
  },
  overlay: {
    zIndex: '10',
  },
};
const ItemForm = ({projectItem, project, isOpen, handleClose, refetch}: Props) => {
  const [keyword, setKeyword] = useState(null);
  const {data: {allItems = []} = {}, loading: loadingItems} = useAllItemsQuery({
    variables: {search: {year: `${project.year.year}`, keyword}},
    skip: !keyword,
  });
  const [create, {loading}] = useCreateProjectItemMutation({onCompleted: () => refetch()});

  const formik = useFormik({
    initialValues: {
      itemId: projectItem?.itemId || '',
      code: projectItem?.code || '',
      name: projectItem?.name || '',
      unitPrice: projectItem?.unitPrice,
      count: projectItem?.count,
    },
    onSubmit: (values) => {
      create({variables: {id: project.id, attributes: values}});
      formik.resetForm();
      handleClose();
    },
  });

  const handleSelectItem = (item: Item) => {
    formik.setFieldValue('itemId', item.id);
    formik.setFieldValue('code', `${item.itemCode.code}`);
    formik.setFieldValue('name', item.name);
    formik.setFieldValue('unitPrice', item.price);
  };

  return (
    <div className="w-screen max-w-xl">
      <ReactModal isOpen={isOpen} onRequestClose={handleClose} style={customStyles}>
        <div className="">
          <ItemSearch setKeyword={setKeyword} />
          <table className="w-full mt-4 border border-gray-300 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr className="flex text-center">
                <th className="w-4" />
                <th className="w-20 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">コード</th>
                <th className="flex-1 px-4 py-3 text-xs font-medium tracking-wider text-gray-500">名称</th>
                <th />
              </tr>
            </thead>
            <tbody
              className="flex flex-col overflow-y-scroll bg-white divide-y divide-gray-200"
              style={{height: '300px'}}>
              {loadingItems && (
                <div className="mx-auto">
                  <Loading />
                </div>
              )}
              {!loadingItems &&
                allItems.map((item) => (
                  <tr
                    className={`flex w-full text-left bg-white px-2 py-2 cursor-pointer hover:bg-gray-100 ${
                      formik.values.itemId === item.id && 'bg-indigo-100'
                    }`}
                    key={item?.id}
                    onClick={() => handleSelectItem(item)}>
                    <td className="w-4 m-auto">
                      <div className="flex items-center">
                        <input
                          id="itemId"
                          name="itemId"
                          type="radio"
                          value={item.id}
                          checked={formik.values.itemId === item.id}
                          className="w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500"
                          onChange={(e) => {
                            handleSelectItem(item);
                          }}
                        />
                      </div>
                    </td>
                    <td className="w-28 px-4">{item.itemCode.code}</td>
                    <td className="flex-1 px-4 my-auto ml-2 text-sm">{item.name}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>

        <form onSubmit={formik.handleSubmit}>
          <div className="mt-4">
            <label className="block text-sm font-medium text-gray-700">コード</label>
            <input
              type="text"
              name="code"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              value={formik.values.code}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              disabled
            />
          </div>

          <div className="mt-2">
            <label className="block text-sm font-medium text-gray-700">名称</label>
            <input
              type="text"
              name="name"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </div>

          <div className="mt-2">
            <label className="block text-sm font-medium text-gray-700">単価</label>
            <input
              type="number"
              name="unitPrice"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              value={formik.values.unitPrice}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </div>

          <div className="mt-2">
            <label className="block text-sm font-medium text-gray-700">数量</label>
            <input
              type="number"
              name="count"
              step="0.1"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              value={formik.values.count}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </div>

          <div className="py-3 text-right">
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              disabled={loading || !formik.isValid || !formik.dirty}
              loading={loading}>
              登録
            </Button>
          </div>
        </form>
      </ReactModal>
    </div>
  );
};

export {ItemForm};
