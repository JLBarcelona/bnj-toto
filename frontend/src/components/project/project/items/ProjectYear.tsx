import React from 'react';
import {useFormik} from 'formik';
import {Project, useYearsQuery, useUpdateProjectYearMutation} from '@/api/index';

type Props = {
  project: Project;
};
const ProjectYear = ({project}: Props) => {
  const {data: {years} = {}} = useYearsQuery();
  const [updateProjectYear] = useUpdateProjectYearMutation();

  const formik = useFormik({
    initialValues: {
      yearId: project.yearId,
    },
    onSubmit: (values) => {
      updateProjectYear({variables: {id: project.id, yearId: values.yearId}});
    },
  });

  const handleYearSelect = (yearId: string) => {
    formik.setFieldValue('yearId', yearId);
    updateProjectYear({variables: {id: project.id, yearId}});
  };

  if (!years) return null;

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="w-20 py-1 pl-1">
        <select
          value={formik.values.yearId}
          onChange={(e) => handleYearSelect(e.target.value)}
          className="block w-full py-2 pl-1 mt-1 text-xs border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500">
          {years.map((year) => (
            <option value={year.id}>{year.year}</option>
          ))}
        </select>
      </div>
    </form>
  );
};

export {ProjectYear};
