import React from 'react';
import {useFormik} from 'formik';

type Props = {
  setKeyword: (keyword?: string) => void;
};

const ItemSearch = ({setKeyword}: Props) => {
  const formik = useFormik({
    initialValues: {
      keyword: '',
    },
    onSubmit: (values) => {
      setKeyword(values.keyword);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="relative mt-1 rounded-md shadow-sm">
        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-4 h-4 text-gray-400"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            stroke-width="2">
            <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
          </svg>
        </div>
        <input
          type="text"
          name="keyword"
          className="block w-full px-4 py-1 pl-10 mt-1 border border-gray-400 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          value={formik.values.keyword}
          onChange={formik.handleChange}
          onBlur={() => formik.handleSubmit()}
        />
      </div>
    </form>
  );
};

export {ItemSearch};
