import {ProjectItem, useImportItemMutation, useProjectItemsQuery, useProjectQuery} from '@/api/index';
import {DocumentDownloadIcon} from '@/components/icons';
import moment from 'moment';
import React, {useEffect, useMemo, useState} from 'react';
import {useParams, useSearchParams} from 'react-router-dom';
import {Layout} from '../Layout';
import {CompanyTypeModal} from './CompanyTypeModal';
import {ItemForm} from './ItemForm';
import {ProjectItemForm} from './ProjectItemForm';
import {ProjectYear} from './ProjectYear';

const ProjectItems = () => {
  const [params, setParams] = useSearchParams();
  const orderItem = params.get('orderItem');
  const order = params.get('order');

  const [open, setOpen] = useState(false);
  const [openCompanyModal, setOpenCompanyModal] = useState(false);
  const [projectItem, setProjectItem] = useState(null);

  const handleCopy = async (projectItem: ProjectItem) => {
    await setProjectItem(projectItem);
    setOpen(true);
  };

  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}});
  const {data: {projectItems = []} = {}, refetch} = useProjectItemsQuery({
    variables: {id, orderItem, order},
    fetchPolicy: 'cache-and-network',
    skip: !orderItem || !order,
  });

  const [importItem] = useImportItemMutation({
    onCompleted: () => refetch(),
  });

  const handleFileChange = (e: any) => {
    e.preventDefault();
    const file = e.target.files[0];
    if (!file) {
      return;
    }

    const reader = new FileReader();
    reader.onload = (e: any) => {
      importItem({
        variables: {
          id: project.id,
          file,
        },
      });
      e.preventDefault();
    };
    reader.readAsDataURL(file);
  };

  const entries = useMemo(() => Array.from(new Set(params.entries())), [params]);
  const setParam = (key: string, value: string) => {
    let newParams = {};
    entries.forEach((entry) => (newParams[entry[0]] = entry[1]));
    setParams({...newParams, [key]: value});
  };

  useEffect(() => {
    if (orderItem === null || order === undefined) {
      setParams({orderItem: 'id', order: 'asc'});
    }
  }, []);

  if (!project) return null;

  return (
    <Layout>
      <div className="flex">
        <ProjectYear project={project} />

        <div className="mt-2 ml-4">
          <input
            id="file-upload"
            name="file-upload"
            type="file"
            accept=".xlsx"
            onChange={(e) => {
              e.preventDefault();
              handleFileChange(e);
              return;
            }}
          />

          {project.assessmentUrl && (
            <a href={project.assessmentUrl}>
              <span className="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                <DocumentDownloadIcon className="flex-shrink-0 w-5 h-5" />
                <span>査定表</span>
              </span>
            </a>
          )}
        </div>
      </div>

      <div className="flex mt-4">
        <button
          type="button"
          className="mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={() => setOpen(true)}>
          追加
        </button>

        <button
          type="button"
          className="mr-4 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={() => setOpenCompanyModal(true)}>
          業者指定
        </button>

        <div className="w-32">
          <select
            className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
            onChange={(e) => setParam('orderItem', e.target.value)}>
            <option value="id">登録順</option>
            <option value="code">コード順</option>
          </select>
        </div>
        <div className="w-32">
          <select
            className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
            onChange={(e) => setParam('order', e.target.value)}>
            <option value="asc">昇順</option>
            <option value="desc">降順</option>
          </select>
        </div>

        <div className="flex space-x-6 text-gray-700 text-sm ml-4 items-center">
          <div>金額小計：{project.totalItemPrice?.toLocaleString()}円</div>
          <div>金額：{project.totalItemPriceRound?.toLocaleString()}円</div>
          <div>金額（税込）：{project.totalPrice?.toLocaleString()}円</div>
        </div>
      </div>

      <div className="flex text-sm text-gray-500 mt-4 space-x-4">
        <div>最終更新日時：{project.updatedItemsAt && moment(project.updatedItemsAt).format('YYYY/MM/DD HH:mm')}</div>
        <div>
          最終更新ユーザー：{project.updatedItemsUser?.lastName}
          {project.updatedItemsUser?.firstName}
        </div>
      </div>

      <div className="mt-4 space-y-6 pb-20">
        <div className="overflow-hidden border-b border-gray-400 sm:rounded-lg">
          <div className="min-w-full border border-gray-300 divide-y divide-gray-300">
            <div className="flex bg-gray-50">
              <div className="w-10"></div>
              <div className="w-20"></div>
              <div className="w-64 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500">
                コード/項目
              </div>
              <div className="w-36 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500">
                単価/数量
              </div>
              <div className="w-16 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500">金額</div>
              <div className="w-32 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500">
                標準業者
              </div>
              <div className="flex-1 px-2 py-3 text-xs font-medium tracking-wider text-center text-gray-500">
                確定業者/部屋種別
              </div>
            </div>
            <div className="bg-white divide-y divide-gray-300">
              {projectItems.map((projectItem) => (
                <ProjectItemForm
                  project={project}
                  projectItem={projectItem}
                  key={projectItem.id}
                  onCopy={handleCopy}
                  refetch={refetch}
                />
              ))}
            </div>
          </div>
        </div>
      </div>

      {open && (
        <ItemForm
          project={project}
          projectItem={projectItem}
          isOpen={open}
          handleClose={() => setOpen(false)}
          refetch={refetch}
        />
      )}

      <CompanyTypeModal isOpen={openCompanyModal} project={project} handleClose={() => setOpenCompanyModal(false)} />
    </Layout>
  );
};

export {ProjectItems};
