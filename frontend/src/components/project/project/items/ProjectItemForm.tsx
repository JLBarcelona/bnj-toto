import {
  Project,
  ProjectItem,
  useRemoveProjectItemMutation,
  useUpdateProjectItemMutation,
  useYearsQuery,
} from '@/api/index';
import {CheckIcon} from '@/components/icons';
import {useFormik} from 'formik';
import moment from 'moment';
import React from 'react';

type Props = {
  project: Project;
  projectItem: ProjectItem;
  onCopy: (projectItem: ProjectItem) => void;
  refetch: () => void;
};
const ProjectItemForm = ({project, projectItem, onCopy, refetch}: Props) => {
  const {data: {years} = {}} = useYearsQuery();
  const [updateProjectItem] = useUpdateProjectItemMutation();
  const [removeProjectItem] = useRemoveProjectItemMutation({
    onCompleted: () => refetch(),
  });

  const options = project.room?.roomTypes?.map((roomType) => ({
    value: roomType.id,
    label: roomType.name,
  }));

  let companyOptions = [];
  companyOptions = projectItem.selectableCompanies.map((company) => ({
    value: company.id,
    label: company.name,
  }));
  if (projectItem.company && projectItem.selectableCompanies.includes(projectItem.company)) {
    companyOptions.push({value: projectItem.company.id, label: projectItem.company.name});
  }

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: projectItem.name,
      count: projectItem.count || 0,
      unitPrice: projectItem.unitPrice || 0,
      amount: projectItem.amount || 0,
      roomTypeIds: projectItem.roomTypeIds || [],
      companyId: projectItem.companyId,
    },
    onSubmit: (values) => {
      updateProjectItem({variables: {id: projectItem.id, attributes: values}});
    },
  });

  const handleRemoveProjectItem = () => {
    removeProjectItem({variables: {id: projectItem.id}});
  };

  const handleCompanySelect = (companyId: string) => {
    formik.setFieldValue('companyId', companyId);
    formik.submitForm();
  };

  const handleCopy = () => {
    onCopy(projectItem);
  };

  const handleEditable = () => {
    let isEditable = false;
    ['OM', 'SM'].forEach((keyword) => {
      if (projectItem.code.indexOf(keyword) > -1) {
        isEditable = true;
        return;
      }
    });

    return isEditable;
  };

  if (!years) return null;

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className="flex">
        <div className="w-10 text-gray-600 text-sm flex items-center px-4">#{projectItem.sequenceNumber}</div>
        <div className="px-1 m-auto w-20">
          <button
            type="button"
            className="block w-full px-2 py-1 text-xs font-medium text-white bg-indigo-600 border border-transparent rounded shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 whitespace-nowrap"
            onClick={handleCopy}>
            コピー
          </button>
          <button
            type="button"
            className="block w-full px-2 py-1 mx-auto mt-1 text-xs font-medium border rounded shadow-sm border-rose-600 text-rose-600 focus:outline-none focus:ring-2 focus:ring-offset-2 hover:bg-rose-50"
            onClick={handleRemoveProjectItem}>
            削除
          </button>
        </div>

        <div className="w-64 px-2 py-4">
          <div className="text-xs font-medium text-gray-900">{projectItem.code}</div>
          <div className="text-xs text-gray-500">
            <input
              type="text"
              name="name"
              className="block w-full px-2 py-1 mt-1 border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-xs"
              value={formik.values.name}
              disabled={!handleEditable()}
              onChange={formik.handleChange}
              onBlur={() => formik.handleSubmit()}
            />
          </div>
        </div>

        <div className="w-36 px-2 py-4 flex">
          <div>
            <input
              name="count"
              type="number"
              step="0.1"
              value={formik.values.count}
              className="w-16 px-1 py-1 mt-1 text-xs text-right border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500"
              onChange={formik.handleChange}
              onBlur={() => formik.handleSubmit()}
            />
          </div>

          <div>
            <input
              name="unitPrice"
              type="number"
              value={formik.values.unitPrice}
              className="w-16 px-1 py-1 mt-1 text-xs text-right border border-gray-300 rounded-sm focus:ring-indigo-500 focus:border-indigo-500"
              onChange={formik.handleChange}
              disabled={!handleEditable()}
              onBlur={() => formik.handleSubmit()}
            />
          </div>
        </div>
        <div className="w-16 px-2 py-4">
          <div className="text-xs leading-8 text-right text-gray-500">{projectItem.amount?.toLocaleString()}円</div>
        </div>

        <div className="w-32 px-4 flex flex-col justify-center">
          <div className="text-sm">
            {projectItem.company?.companyTypes?.map((companyType) => companyType.name).join('/')}
          </div>
          <div className="flex items-center">
            <div
              className={`text-xs  border border-gray-400 px-2 py-1 rounded-sm ${
                projectItem.item.order ? 'bg-gray-600 text-white' : 'text-gray-500'
              }`}>
              {projectItem.item?.order ? '発注対象' : '発注なし'}
            </div>
            {formik.values.companyId && <CheckIcon className="w-6 text-green-700" />}
          </div>
        </div>

        <div className="flex-1 px-2 py-4 text-xs leading-8 text-gray-500">
          <select
            value={formik.values.companyId}
            onChange={(e) => handleCompanySelect(e.target.value)}
            className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
            <option value=""></option>
            {companyOptions.map((companyOption) => (
              <option value={companyOption.value}>{companyOption.label}</option>
            ))}
          </select>
          <div className="flex items-center mt-2 flex-wrap gap-2">
            {project.room?.roomTypes?.map((roomType) => (
              <div className="flex items-center h-5" key={`${projectItem.id}-roomType-${roomType.id}`}>
                <input
                  id={`${projectItem.id}-roomType-${roomType.id}`}
                  type="checkbox"
                  className="w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                  checked={formik.values.roomTypeIds.includes(roomType.id)}
                  onChange={(e) => {
                    if (formik.values.roomTypeIds?.includes(roomType.id)) {
                      formik.setFieldValue(
                        'roomTypeIds',
                        formik.values.roomTypeIds.filter((v) => v !== roomType.id),
                      );
                    } else {
                      formik.setFieldValue('roomTypeIds', [...formik.values.roomTypeIds, roomType.id]);
                    }
                    formik.handleSubmit();
                  }}
                  onBlur={formik.handleBlur}
                />
                <div className="ml-1 text-sm">
                  <label className="font-medium text-gray-700">{roomType.name}</label>
                </div>
              </div>
            ))}
          </div>

          <div className="flex text-sm text-gray-500 mt-4 space-x-4">
            <div>
              最終更新日時：
              {projectItem.updatedItemsAt && moment(projectItem.updatedItemsAt).format('YYYY/MM/DD HH:mm')}
            </div>
            <div>
              最終更新ユーザー：{projectItem.updatedItemsUser?.lastName}
              {projectItem.updatedItemsUser?.firstName}
            </div>
          </div>
        </div>
      </div>
    </form>
  );
};

export {ProjectItemForm};
