import React, {useState} from 'react';
import ReactModal from 'react-modal';
import {Project, useCompanyTypesQuery, useUpdateProjectItemsCompanyMutation} from '@/api/index';
import {CompanyForm} from './CompanyForm';
import {Button} from '@/components/ui';

type Props = {
  isOpen: boolean;
  project: Project;
  handleClose: () => void;
};

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    height: '800px',
  },
  overlay: {
    zIndex: '10',
  },
};
const CompanyTypeModal = ({isOpen, project, handleClose}: Props) => {
  const [values, setValues] = useState<{companyTypeId: string; companyId: string}[]>([]);
  const {data: {companyTypes = []} = {}} = useCompanyTypesQuery();
  const [update, {loading}] = useUpdateProjectItemsCompanyMutation();

  const setCompanyIds = (companyTypeId: string, companyId?: string) => {
    const v = values.filter((v) => v.companyTypeId !== companyTypeId);
    if (companyId || companyId.length !== 0) {
      v.push({companyTypeId, companyId});
    }
    setValues(v);
  };

  const handleUpdateProjectItemsCompamy = () => {
    for (const value of values) {
      update({variables: {id: project.id, companyId: value.companyId, companyTypeId: value.companyTypeId}});
    }

    handleClose();
  };

  return (
    <div className="w-screen max-w-xl">
      <ReactModal isOpen={isOpen} onRequestClose={handleClose} style={customStyles}>
        <div className="">
          <table className="min-w-full divide-y divide-gray-300">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                  業者種別
                </th>
                <th scope="col" className="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                  業者
                </th>
              </tr>
            </thead>
            <tbody className="divide-y divide-gray-200 bg-white">
              {companyTypes.map((companyType) => (
                <CompanyForm companyType={companyType} key={companyType.id} setCompany={setCompanyIds} />
              ))}
            </tbody>
          </table>

          <div className="mt-10 text-right">
            <Button
              type="button"
              className="px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              onClick={handleUpdateProjectItemsCompamy}
              disabled={values.length === 0}
              loading={loading}>
              更新
            </Button>
          </div>
        </div>
      </ReactModal>
    </div>
  );
};

export {CompanyTypeModal};
