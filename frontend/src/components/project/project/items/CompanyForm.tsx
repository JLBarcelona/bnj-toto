import React, {useState} from 'react';
import {CompanyType} from '@/api/index';

type Props = {
  companyType: CompanyType;
  setCompany: (companyTypeId: string, companyId?: string) => void;
};
const CompanyForm = ({companyType, setCompany}: Props) => {
  const [companyId, setCompanyId] = useState(null);

  const handleUpdateProjectItemsCompamy = (companyId: string) => {
    setCompany(companyType.id, companyId);
  };

  return (
    <tr>
      <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{companyType.name}</td>
      <td className="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">
        <select
          value={companyId}
          onChange={(e) => {
            setCompanyId(e.target.value);
            handleUpdateProjectItemsCompamy(e.target.value);
          }}
          className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
          <option value=""></option>
          {companyType.companies.map((company) => (
            <option value={company.id}>{company.name}</option>
          ))}
        </select>
      </td>
    </tr>
  );
};

export {CompanyForm};
