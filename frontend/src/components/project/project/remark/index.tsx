import {useProjectQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {RemarkForm} from './RemarkForm';

const Remark = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}});

  if (!project) return null;
  return <RemarkForm project={project} />;
};

export {Remark};
