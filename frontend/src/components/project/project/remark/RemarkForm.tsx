import {Project, useUpdateProjectMutation} from '@/api/index';
import React from 'react';
import {useForm} from 'react-hook-form';
import {Layout} from '../Layout';

type Props = {
  project: Project;
};
type FormProps = {
  remark: string;
};
const RemarkForm = (props: Props) => {
  const [updateProject] = useUpdateProjectMutation();

  const {register, handleSubmit} = useForm<FormProps>({
    defaultValues: {
      remark: props.project.remark,
    },
    mode: 'onBlur',
  });

  const onSubmit = (data) => {
    updateProject({variables: {id: props.project.id, attributes: data}});
  };

  return (
    <Layout>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 mt-6 gap-y-2 gap-x-4 sm:grid-cols-6">
          <div className="col-span-6">
            <h3 className="text-lg font-medium leading-6 text-gray-900">備考</h3>
          </div>

          <div className="col-span-6">
            <textarea
              rows={5}
              className="block w-full p-2 text-sm border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500"
              {...register('remark', {onBlur: handleSubmit(onSubmit)})}
            />
          </div>
        </div>
      </form>
    </Layout>
  );
};

export {RemarkForm};
