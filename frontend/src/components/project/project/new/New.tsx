import {
  Building,
  BuildingNumber,
  useAllBuildingsQuery,
  useCreateProjectMutation,
  useProjectsQuery,
  useYearsQuery,
} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import {Button} from '@/components/ui';
import {useFormik} from 'formik';
import React, {useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Select from 'react-select';
import * as Yup from 'yup';
import {closeTypeValues} from '../closeTypes';

const validateSchema = Yup.object().shape({
  yearId: Yup.string().trim().required('必須項目です'),
  orderNumberType: Yup.string().trim().required('必須項目です'),
  orderNumber: Yup.string().trim().required('必須項目です'),
  buildingId: Yup.string().trim().required('必須項目です'),
  buildingNumberId: Yup.string().trim().required('必須項目です'),
  roomId: Yup.string().trim().required('必須項目です'),
});

const ProjectNew = () => {
  const navigate = useNavigate();
  const [orderNumber, setOrderNumber] = useState(null);
  const [orderNumberType, setOrderNumberType] = useState(null);
  const [createProject, {loading}] = useCreateProjectMutation();
  const [building, setBuilding] = useState<Building>(null);
  const [buildingNumber, setBuildingNumber] = useState<BuildingNumber>(null);

  const {data: {years = []} = {}} = useYearsQuery();

  const {data: {allBuildings = []} = {}} = useAllBuildingsQuery();
  const options = allBuildings.map((building) => ({
    value: building.id,
    label: building.name,
  }));

  const buildingNumberOptions = building?.buildingNumbers?.map((buildingNumber) => ({
    value: buildingNumber.id,
    label: buildingNumber.name,
  }));

  const roomOptions = buildingNumber?.rooms.map((room) => ({
    value: room.id,
    label: room.name,
  }));

  const {data: {projects: {projects = []} = {}} = {}} = useProjectsQuery({
    variables: {search: {orderNumber, orderNumberType}},
    skip: !orderNumber,
  });

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: validateSchema,
    initialValues: {
      yearId: '',
      orderNumberType: '',
      orderNumber: '',
      buildingId: null,
      buildingNumberId: null,
      roomId: null,
      closeTypes: [],
      closeReason: '',
    },
    onSubmit: (values) => {
      createProject({variables: {attributes: values}}).then(() => navigate(`/projects${location.search}`));
    },
  });

  const handleSelect = (selectOption: any) => {
    formik.setFieldValue('buildingId', selectOption.value);
    formik.setFieldValue('name', selectOption.label);
    const building = allBuildings.find((buiding) => buiding.id === selectOption.value);
    setBuilding(building);
  };

  const handleSelectBuildingNumber = (selectOption: any) => {
    formik.setFieldValue('buildingNumberId', selectOption.value);
    const selectedBuildingNumber = building.buildingNumbers.find(
      (buildingNumber) => buildingNumber.id === selectOption.value,
    );
    setBuildingNumber(selectedBuildingNumber);
  };

  const handleSelectRoom = (selectOption: any) => {
    formik.setFieldValue('roomId', selectOption.value);
  };

  return (
    <div className="w-full">
      <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          <nav className="flex" aria-label="Breadcrumb">
            <ol role="list" className="flex items-center space-x-4">
              <li>
                <div className="flex items-center">
                  <Link to={'/projects'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                    案件一覧
                  </Link>
                </div>
              </li>
              <li>
                <div className="flex items-center">
                  <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                  <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">新規作成</p>
                </div>
              </li>
            </ol>
          </nav>

          <div className="mt-10 md:col-span-2">
            <form className="p-6 space-y-6" onSubmit={formik.handleSubmit}>
              <div className="my-4">
                <label className="block text-sm font-medium text-gray-700">年度</label>
                <select
                  value={formik.values.yearId}
                  className="px-4 py-2 border border-gray-300"
                  onChange={(e) => {
                    formik.setFieldValue('yearId', e.target.value);
                  }}>
                  <option value=""></option>
                  {years.map((year) => (
                    <option key={`year-${year.id}`} value={year.id}>
                      {year.year}
                    </option>
                  ))}
                </select>
              </div>

              <div>
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  指示番号
                </label>
                <div className="flex">
                  <input
                    type="text"
                    name="orderNumberType"
                    className="block w-16 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.orderNumberType}
                    onChange={formik.handleChange}
                    onBlur={() => {
                      setOrderNumberType(formik.values.orderNumberType);
                    }}
                  />

                  <span className="inline-flex items-center mx-4">-</span>

                  <input
                    type="text"
                    name="orderNumber"
                    className="block w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.orderNumber}
                    onChange={formik.handleChange}
                    onBlur={() => {
                      setOrderNumber(formik.values.orderNumber);
                    }}
                  />
                </div>

                {projects.length > 0 && <span className="text-red-500 text-sm">すでに登録済みの指示番号です</span>}
              </div>

              <div>
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  建物
                </label>
                <Select options={options} isClearable closeMenuOnSelect onChange={handleSelect} />
              </div>

              <div>
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  号棟
                </label>
                <Select
                  options={buildingNumberOptions}
                  isClearable
                  closeMenuOnSelect
                  onChange={handleSelectBuildingNumber}
                />
              </div>

              <div>
                <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                  号室
                </label>
                <Select options={roomOptions} isClearable closeMenuOnSelect onChange={handleSelectRoom} />
              </div>

              <div className="col-span-2">
                <label className="block text-sm font-medium text-gray-700">閉鎖区分</label>

                <div className="relative flex items-start space-x-4">
                  {closeTypeValues.map((closeTypeValues) => (
                    <>
                      <div className="flex items-center h-5">
                        <input
                          id={closeTypeValues.value}
                          name={closeTypeValues.value}
                          type="checkbox"
                          className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                          value={closeTypeValues.value}
                          onChange={(e) => {
                            const value = `${e.target.value}`;
                            if (formik.values.closeTypes.includes(value)) {
                              formik.setFieldValue(
                                'closeTypes',
                                formik.values.closeTypes.filter((v) => v !== value),
                              );
                            } else {
                              formik.setFieldValue('closeTypes', [...formik.values.closeTypes, value]);
                            }
                          }}
                          onBlur={formik.handleBlur}
                        />
                        <div className="ml-1 text-sm">
                          <label className="font-medium text-gray-700">{closeTypeValues.name}</label>
                        </div>
                      </div>
                    </>
                  ))}
                </div>
              </div>

              {/* {formik.values.closeTypes === 'close_type9' && (
                <div className="col-span-2">
                  <label>閉鎖区分その他内容</label>
                  <input
                    type="text"
                    name="closeReason"
                    className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.closeReason}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
              )} */}

              <div className="py-3 text-right">
                <Button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  disabled={!formik.dirty || !formik.isValid || projects.length > 0}
                  loading={loading}>
                  保存
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export {ProjectNew};
