import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {BaseForm} from './base/BaseForm';
import {Document} from './document';
import {ManagementForm} from './management/ManagementForm';

const ProjectForm = () => {
  return (
    <Routes>
      <Route path="base" element={<BaseForm />} />
      <Route path="docs" element={<Document />} />
      <Route path="management" element={<ManagementForm />} />
    </Routes>
  );
};

export {ProjectForm};
