import {Project, useUpdateProjectMutation} from '@/api/index';
import ja from 'date-fns/locale/ja';
import moment from 'moment';
import React from 'react';
import DatePicker, {registerLocale} from 'react-datepicker';
import {useForm} from 'react-hook-form';
import {useParams} from 'react-router-dom';
import {Layout} from '../../Layout';
import {Tab as FormTab} from '../Tab';

registerLocale('ja', ja);

type Props = {
  project: Project;
};
type FormProps = {
  afterConstructionImageDate: string;
  beforeConstructionImageDate: string;
  checklistFirstDate: string;
  checklistLastDate: string;
  isFaq: boolean;
  imageStatus: string;
  submitDocumentDate: string;
};
const DocumentForm = (props: Props) => {
  const {id} = useParams<{id: string}>();
  const [updateProject, {loading}] = useUpdateProjectMutation();

  const {
    register,
    handleSubmit,
    getValues,
    setValue,
    formState: {isDirty, isValid, errors},
  } = useForm<FormProps>({
    mode: 'onBlur',
    defaultValues: {
      beforeConstructionImageDate: props.project.beforeConstructionImageDate,
      afterConstructionImageDate: props.project.afterConstructionImageDate,
      checklistFirstDate: props.project.checklistFirstDate,
      checklistLastDate: props.project.checklistLastDate,
      isFaq: props.project.isFaq,
      imageStatus: props.project.imageStatus,
      submitDocumentDate: props.project.submitDocumentDate,
    },
  });

  const onSubmit = (data) => {
    updateProject({variables: {id, attributes: data}});
  };

  return (
    <Layout>
      <FormTab />

      <div className="mt-4">
        <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
          {/* <Button
            type="submit"
            className="fixed top-60 right-10 z-10 flex justify-center items-center w-40 px-4 py-2 text-sm font-medium text-white bg-red-500 border border-transparent rounded-md shadow-sm hover:bg-red-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
            loading={loading}
            disabled={!formik.dirty || loading || !formik.isValid}>
            保存
            {formik.dirty && formik.isValid && <ExclamationIcon className="w-8" />}
          </Button> */}

          <div className="grid mt-6 gap-y-6 gap-x-2 grid-cols-5">
            <div className="col-span-1">
              <label className="block text-sm font-medium text-gray-700">施工前写真資料・写真確認</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('beforeConstructionImageDate')}
                  selected={
                    getValues('beforeConstructionImageDate') && new Date(getValues('beforeConstructionImageDate'))
                  }
                  onChange={async (date) => {
                    await setValue('beforeConstructionImageDate', moment(date).format('YYYY-MM-DD'));
                    handleSubmit(onSubmit)();
                  }}
                />
              </div>
            </div>

            <div className="col-span-1">
              <label className="block text-sm font-medium text-gray-700">初回チェックリスト</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('checklistFirstDate')}
                  selected={getValues('checklistFirstDate') && new Date(getValues('checklistFirstDate'))}
                  onChange={async (date) => {
                    await setValue('checklistFirstDate', moment(date).format('YYYY-MM-DD'));
                    handleSubmit(onSubmit)();
                  }}
                />
              </div>
            </div>

            <div className="col-span-1">
              <label className="block text-sm font-medium text-gray-700">最終チェックリスト</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('checklistLastDate')}
                  selected={getValues('checklistLastDate') && new Date(getValues('checklistLastDate'))}
                  onChange={async (date) => {
                    await setValue('checklistLastDate', moment(date).format('YYYY-MM-DD'));
                    handleSubmit(onSubmit)();
                  }}
                />
              </div>
            </div>

            <div className="col-span-1 h-5">
              <label htmlFor="isFaq" className="block text-sm font-medium text-gray-700">
                質疑回答
              </label>
              <input
                type="checkbox"
                className="w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                {...register('isFaq', {onBlur: handleSubmit(onSubmit)})}
              />
            </div>
          </div>

          <div className="grid mt-6 gap-y-6 gap-x-2 grid-cols-5">
            <div className="col-span-1">
              <label className="block text-sm font-medium text-gray-700">施工後写真資料</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('afterConstructionImageDate')}
                  selected={
                    getValues('afterConstructionImageDate') && new Date(getValues('afterConstructionImageDate'))
                  }
                  onChange={async (date) => {
                    await setValue('afterConstructionImageDate', moment(date).format('YYYY-MM-DD'));
                    handleSubmit(onSubmit)();
                  }}
                />
              </div>
            </div>

            <div className="h-5">
              <label htmlFor="isLackImage" className="block text-sm font-medium text-gray-700">
                写真ステータス
              </label>
              <select
                className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
                {...register('imageStatus', {onChange: handleSubmit(onSubmit)})}>
                <option value=""></option>
                <option value="lack">不足写真あり</option>
                <option value="complete">写真完了</option>
              </select>
            </div>
          </div>

          <div className="grid mt-6 gap-y-6 gap-x-2 grid-cols-5">
            <div className="col-span-1">
              <label className="block text-sm font-medium text-gray-700">請求書類提出日</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('submitDocumentDate')}
                  selected={getValues('submitDocumentDate') && new Date(getValues('submitDocumentDate'))}
                  onChange={async (date) => {
                    await setValue('submitDocumentDate', moment(date).format('YYYY-MM-DD'));
                    handleSubmit(onSubmit)();
                  }}
                />
              </div>
            </div>
          </div>
          {/* 
          <div className="sticky bottom-0 bg-gray-100 flex justify-center">
            <Button
              type="submit"
              className="flex justify-center items-center w-40 px-4 py-2 text-sm font-medium text-white bg-red-500 border border-transparent rounded-md shadow-sm hover:bg-red-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
              loading={loading}
              disabled={!formik.dirty || loading || !formik.isValid}>
              保存
              {formik.dirty && formik.isValid && <ExclamationIcon className="w-8" />}
            </Button>
          </div> */}
        </form>
      </div>
    </Layout>
  );
};

export {DocumentForm};
