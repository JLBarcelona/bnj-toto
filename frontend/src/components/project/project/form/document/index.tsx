import {useProjectQuery} from '@/api/index';
import ja from 'date-fns/locale/ja';
import React from 'react';
import {registerLocale} from 'react-datepicker';
import {useParams} from 'react-router-dom';
import {DocumentForm} from './DocumentForm';

registerLocale('ja', ja);

const Document = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}, fetchPolicy: 'cache-and-network'});

  if (!project) return null;

  return <DocumentForm project={project} />;
};

export {Document};
