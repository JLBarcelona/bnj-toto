import {
  useCurrentUserQuery,
  useJkkUsersQuery,
  useProjectQuery,
  useUpdateProjectMutation,
  useUsersQuery,
} from '@/api/index';
import ja from 'date-fns/locale/ja';
import {useFormik} from 'formik';
import _ from 'lodash';
import moment from 'moment';
import React, {useEffect, useState} from 'react';
import DatePicker, {registerLocale} from 'react-datepicker';
import {useParams} from 'react-router-dom';
import Select from 'react-select';
import {closeTypeValues} from '../../closeTypes';
import {Layout} from '../../Layout';
import {Tab as FormTab} from '../Tab';
import {User} from './User';

registerLocale('ja', ja);

const BaseForm = () => {
  const {id} = useParams<{id: string}>();
  const [totalPriceFormat, setTotalPrice] = useState(null);
  const [editTotalPrice, setEditTotalPrice] = useState(false);

  const {data: {project = {}} = {}} = useProjectQuery({variables: {id}});
  const {data: {currentUser} = {}} = useCurrentUserQuery();
  const isReadOnly = project.editUserId && project.editUserId !== currentUser.id;

  const [udpateProject] = useUpdateProjectMutation();
  const {data: {users: assesmentUsers = []} = {}} = useUsersQuery({
    variables: {search: {active: true, isAssesment: true}},
  });
  const {data: {users = []} = {}} = useUsersQuery({variables: {search: {active: true}}});
  const {data: {jkkUsers = []} = {}} = useJkkUsersQuery({variables: {search: {active: true}}});

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      orderNumberType: project?.orderNumberType || '',
      orderNumber: project?.orderNumber || '',
      name: project?.name || '',
      jkkUserId: project?.jkkUserId || '',
      assesmentUserId: project?.assesmentUserId || '',
      constructionUserId: project?.constructionUserId || '',
      address: project?.address || '',
      assessmentDate: project?.assessmentDate,
      availableDate: project?.availableDate || '',
      livingYear: project?.livingYear,
      livingMonth: project?.livingMonth,
      leaveDate: project?.leaveDate || '',
      totalPriceInput: project?.totalPriceInput,
      orderDate1: project?.orderDate1 || '',
      orderMemo1: project?.orderMemo1 || '',
      orderDate2: project?.orderDate2 || '',
      orderMemo2: project?.orderMemo2 || '',
      orderDate3: project?.orderDate3 || '',
      orderMemo3: project?.orderMemo3 || '',
      closeTypes: project?.closeTypes || [],
      closeReason: project?.closeReason,
      electricStartDate1: project?.electricStartDate1 || '',
      electricEndDate1: project?.electricEndDate1 || '',
      waterStartDate1: project?.waterStartDate1 || '',
      waterEndDate1: project?.waterEndDate1 || '',
      electricStartDate2: project?.electricStartDate2 || '',
      electricEndDate2: project?.electricEndDate2 || '',
      waterStartDate2: project?.waterStartDate2 || '',
      waterEndDate2: project?.waterEndDate2 || '',
      electricStartDate3: project?.electricStartDate3 || '',
      electricEndDate3: project?.electricEndDate3 || '',
      waterStartDate3: project?.waterStartDate3 || '',
      waterEndDate3: project?.waterEndDate3 || '',
      isVermiculite: project?.isVermiculite,
      isFinishedVermiculite: project?.isFinishedVermiculite,
      vermiculiteDate: project?.vermiculiteDate || '',
      beforeConstructionImageDate: project?.beforeConstructionImageDate || '',
      checklistFirstDate: project?.checklistFirstDate || '',
      checklistLastDate: project?.checklistLastDate || '',
      constructionStartDate: project?.constructionStartDate || '',
      constructionEndDate: project?.constructionEndDate || '',
      userIds: project?.userIds || [],
    },
    onSubmit: (values) => {
      udpateProject({variables: {id, attributes: values}});
    },
  });

  const findUser = (userId: string) => assesmentUsers.find((user) => user.id === userId);

  const jkkUserOptions = jkkUsers.map((user) => ({value: user.id, label: `${user.lastName} ${user.firstName}`}));

  useEffect(() => {
    if (formik.values.totalPriceInput) {
      setTotalPrice(formik.values.totalPriceInput?.toLocaleString());
    }
  }, [formik.values.totalPriceInput]);

  if (!project) return null;

  return (
    <Layout>
      <FormTab />

      <div className="mt-4">
        <form className="p-6 space-y-6" onSubmit={formik.handleSubmit}>
          {/* <Button
            type="submit"
            className="fixed top-60 right-10 z-10 flex justify-center items-center w-40 px-4 py-2 text-sm font-medium text-white bg-red-500 border border-transparent rounded-md shadow-sm hover:bg-red-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
            loading={loading}
            disabled={!formik.dirty || loading || !formik.isValid}>
            保存
            {formik.dirty && formik.isValid && <ExclamationIcon className="w-8" />}
          </Button> */}

          <div className="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-12">
            <div className="col-span-12">
              <label className="block text-sm font-medium text-gray-700">指示番号</label>
              <div className="flex">
                <input
                  type="text"
                  disabled={isReadOnly}
                  name="orderNumberType"
                  className="block w-16 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  value={formik.values.orderNumberType}
                  onChange={formik.handleChange}
                  onBlur={() => formik.handleSubmit()}
                />

                <span className="inline-flex items-center mx-4">-</span>

                <input
                  type="text"
                  name="orderNumber"
                  disabled={isReadOnly}
                  className="block w-64 px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  value={formik.values.orderNumber}
                  onChange={formik.handleChange}
                  onBlur={() => formik.handleSubmit()}
                />
              </div>
            </div>

            <div className="col-span-12">
              <label className="block text-sm font-medium text-gray-700">閉鎖区分</label>

              <div className="flex items-start space-x-4">
                {closeTypeValues.map((closeTypeValues) => (
                  <div className="flex items-center h-5" key={`closeTypeValues-${closeTypeValues.value}`}>
                    <input
                      id={closeTypeValues.value}
                      name={closeTypeValues.value}
                      type="checkbox"
                      className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                      value={closeTypeValues.value}
                      checked={formik.values.closeTypes.includes(closeTypeValues.value)}
                      onChange={(e) => {
                        const value = `${e.target.value}`;
                        if (formik.values.closeTypes.includes(value)) {
                          formik.setFieldValue(
                            'closeTypes',
                            formik.values.closeTypes.filter((v) => v !== value),
                          );
                        } else {
                          formik.setFieldValue('closeTypes', [...formik.values.closeTypes, value]);
                        }
                        formik.handleSubmit();
                      }}
                      onBlur={formik.handleBlur}
                    />
                    <div className="ml-1 text-sm">
                      <label className="font-medium text-gray-700">{closeTypeValues.name}</label>
                    </div>
                  </div>
                ))}
              </div>
            </div>

            {formik.values.closeTypes.includes('close_type9') && (
              <>
                <div className="col-span-8">
                  <label>閉鎖区分その他内容</label>
                  <input
                    type="text"
                    name="closeReason"
                    className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.closeReason}
                    onChange={formik.handleChange}
                    onBlur={() => formik.handleSubmit()}
                  />
                </div>
                <div className="col-span-4" />
              </>
            )}

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">査定日</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.assessmentDate && new Date(formik.values.assessmentDate)}
                  onChange={(date) => {
                    formik.setFieldValue('assessmentDate', moment(date).format('YYYY-MM-DD'));
                    formik.handleSubmit();
                  }}
                />
              </div>
            </div>
            <div className="col-span-8" />

            <div className="col-span-12 my-2 border-t border-gray-300" />

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">公社担当者</label>
              <Select
                options={jkkUserOptions}
                className="px-4 w-72"
                isClearable
                closeMenuOnSelect
                value={jkkUserOptions.find((option) => option.value === formik.values.jkkUserId)}
                onChange={(selectOption) => {
                  formik.setFieldValue('jkkUserId', selectOption.value) && formik.handleSubmit();
                }}
              />
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">当社担当者（査定）</label>
              <select
                value={formik.values.assesmentUserId}
                className="px-4 py-2 border border-gray-300"
                onChange={(e) => formik.setFieldValue('assesmentUserId', e.target.value) && formik.handleSubmit()}>
                <option value=""></option>
                {users.map((user) => (
                  <option key={`assesmentUserId-${user.id}`} value={user.id}>
                    {`${user.lastName} ${user.firstName}`}
                  </option>
                ))}
              </select>
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">当社担当者（工事）</label>
              <select
                value={formik.values.constructionUserId}
                className="px-4 py-2 border border-gray-300"
                onChange={(e) => formik.setFieldValue('constructionUserId', e.target.value) && formik.handleSubmit()}>
                <option value=""></option>
                {users.map((user) => (
                  <option key={`constructionUser-${user.id}`} value={user.id}>
                    {`${user.lastName} ${user.firstName}`}
                  </option>
                ))}
              </select>
            </div>

            <div className="col-span-12">
              <label className="block text-sm font-medium text-gray-700">
                査定同行者{' '}
                <span className="text-gray-600 bg-gray-200 ml-auto inline-block py-0.5 px-3 text-xs rounded-full">
                  {formik.values.userIds.length}
                </span>
              </label>
              <div className="flex">
                {formik.values.userIds.map((userId) => (
                  <User user={findUser(userId)} key={`user-${userId}`} />
                ))}
              </div>
              <div className="h-40 px-4 py-2 mt-1 overflow-y-scroll border border-gray-300 rounded-md flex flex-wrap gap-4">
                {assesmentUsers.map((user) => (
                  <div className="flex items-center h-5" key={`user-${user.id}`}>
                    <input
                      id={user.id}
                      name={`${user.lastName} ${user.firstName}`}
                      type="checkbox"
                      className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                      value={user.id}
                      checked={formik.values.userIds.includes(user.id)}
                      onChange={(e) => {
                        const value = `${e.target.value}`;
                        if (formik.values.userIds.includes(value)) {
                          formik.setFieldValue(
                            'userIds',
                            formik.values.userIds.filter((v) => v !== value),
                          );
                        } else {
                          formik.setFieldValue('userIds', [...formik.values.userIds, value]);
                        }
                        formik.handleSubmit();
                      }}
                      onBlur={formik.handleBlur}
                    />
                    <div className="ml-1 text-sm">
                      <label className="font-medium text-gray-700">{`${user.lastName} ${user.firstName}`}</label>
                    </div>
                  </div>
                ))}
              </div>
            </div>

            <div className="col-span-12 my-6 border-t border-gray-300" />

            <div className="col-span-2">
              <label className="block text-sm font-medium text-gray-700">退去日</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.leaveDate && new Date(formik.values.leaveDate)}
                  onChange={(date) =>
                    formik.setFieldValue('leaveDate', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-2">
              <label className="block text-sm font-medium text-gray-700">募集可能日</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.availableDate && new Date(formik.values.availableDate)}
                  onChange={(date) =>
                    formik.setFieldValue('availableDate', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-2">
              <label className="block text-sm font-medium text-gray-700">入居年数-年</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <input
                  type="number"
                  name="livingYear"
                  min={1}
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  value={formik.values.livingYear}
                  onChange={formik.handleChange}
                  onBlur={() => formik.handleSubmit()}
                />
              </div>
            </div>

            <div className="col-span-2">
              <label className="block text-sm font-medium text-gray-700">入居年数-ヶ月</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <select
                  value={formik.values.livingMonth}
                  className="px-4 py-1 mt-1 border border-gray-300"
                  onChange={(e) =>
                    formik.setFieldValue('livingMonth', Number(e.target.value)) && formik.handleSubmit()
                  }>
                  <option value=""></option>
                  {_.times(11).map((number) => (
                    <option key={`livingMonth-${number}`} value={Number(number + 1)}>
                      {number + 1}
                    </option>
                  ))}
                </select>
              </div>
            </div>

            <div className="col-span-8">
              <label className="block text-sm font-medium text-gray-700">住所</label>
              <input
                type="text"
                name="address"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formik.values.address}
                onChange={formik.handleChange}
                onBlur={() => formik.handleSubmit()}
              />
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">工事金額（税込み）</label>
              <div className="relative">
                <div className="absolute inset-y-0 left-0 items-center px-3 pointer-events-none bg-gray-200 text-sm inline-flex">
                  ¥
                </div>
                <input
                  type={editTotalPrice ? 'number' : 'text'}
                  name="totalPriceInput"
                  className="pl-10 block w-full py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  value={editTotalPrice ? formik.values.totalPriceInput : totalPriceFormat}
                  onChange={formik.handleChange}
                  onBlur={() => {
                    setEditTotalPrice(false);
                    formik.handleSubmit();
                  }}
                  onFocus={() => {
                    setEditTotalPrice(true);
                  }}
                />
              </div>
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">指示日</label>
              <DatePicker
                locale="ja"
                dateFormat="yyyy/MM/dd"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                selected={formik.values.constructionStartDate && new Date(formik.values.constructionStartDate)}
                onChange={(date) =>
                  formik.setFieldValue('constructionStartDate', moment(date).format('YYYY-MM-DD')) &&
                  formik.handleSubmit()
                }
              />
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">完了予定日</label>
              <DatePicker
                locale="ja"
                dateFormat="yyyy/MM/dd"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                selected={formik.values.constructionEndDate && new Date(formik.values.constructionEndDate)}
                onChange={(date) =>
                  formik.setFieldValue('constructionEndDate', moment(date).format('YYYY-MM-DD')) &&
                  formik.handleSubmit()
                }
              />
            </div>
            <div className="col-span-4" />

            <div className="col-span-12 border-t border-gray-300" />

            <div className="col-span-12 border-t border-gray-300" />
            <div className="col-span-12">
              <h3 className="text-lg font-medium leading-6 text-gray-900">指示書発行</h3>
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">指示書発行日1</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.orderDate1 && new Date(formik.values.orderDate1)}
                  onChange={(date) =>
                    formik.setFieldValue('orderDate1', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-8">
              <label>指示書発行メモ1</label>
              <input
                type="text"
                name="orderMemo1"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formik.values.orderMemo1}
                onChange={formik.handleChange}
                onBlur={() => formik.handleSubmit()}
              />
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">指示書発行日2</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.orderDate2 && new Date(formik.values.orderDate2)}
                  onChange={(date) =>
                    formik.setFieldValue('orderDate2', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-8">
              <label>指示書発行メモ2</label>
              <input
                type="text"
                name="orderMemo2"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formik.values.orderMemo2}
                onChange={formik.handleChange}
                onBlur={() => formik.handleSubmit()}
              />
            </div>

            <div className="col-span-4">
              <label className="block text-sm font-medium text-gray-700">指示書発行日3</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.orderDate3 && new Date(formik.values.orderDate3)}
                  onChange={(date) =>
                    formik.setFieldValue('orderDate3', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-8">
              <label>指示書発行メモ3</label>
              <input
                type="text"
                name="orderMemo2"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                value={formik.values.orderMemo3}
                onChange={formik.handleChange}
                onBlur={() => formik.handleSubmit()}
              />
            </div>

            <div className="col-span-12 border-t border-gray-300" />

            <div className="col-span-12">
              <h3 className="text-lg font-medium leading-6 text-gray-900">電気水道</h3>
            </div>

            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">電気開始日1</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.electricStartDate1 && new Date(formik.values.electricStartDate1)}
                  onChange={(date) =>
                    formik.setFieldValue('electricStartDate1', moment(date).format('YYYY-MM-DD')) &&
                    formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">電気停止日1</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.electricEndDate1 && new Date(formik.values.electricEndDate1)}
                  onChange={(date) =>
                    formik.setFieldValue('electricEndDate1', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">水道開始日1</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.waterStartDate1 && new Date(formik.values.waterStartDate1)}
                  onChange={(date) =>
                    formik.setFieldValue('waterStartDate1', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">水道停止日1</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.waterEndDate1 && new Date(formik.values.waterEndDate1)}
                  onChange={(date) =>
                    formik.setFieldValue('waterEndDate1', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">電気開始日2</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.electricStartDate2 && new Date(formik.values.electricStartDate2)}
                  onChange={(date) =>
                    formik.setFieldValue('electricStartDate2', moment(date).format('YYYY-MM-DD')) &&
                    formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">電気停止日2</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.electricEndDate2 && new Date(formik.values.electricEndDate2)}
                  onChange={(date) =>
                    formik.setFieldValue('electricEndDate2', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">水道開始日2</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.waterStartDate2 && new Date(formik.values.waterStartDate2)}
                  onChange={(date) =>
                    formik.setFieldValue('waterStartDate2', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">水道停止日2</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.waterEndDate2 && new Date(formik.values.waterEndDate2)}
                  onChange={(date) =>
                    formik.setFieldValue('waterEndDate2', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">電気開始日3</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.electricStartDate3 && new Date(formik.values.electricStartDate3)}
                  onChange={(date) =>
                    formik.setFieldValue('electricStartDate3', moment(date).format('YYYY-MM-DD')) &&
                    formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">電気停止日3</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.electricEndDate3 && new Date(formik.values.electricEndDate3)}
                  onChange={(date) =>
                    formik.setFieldValue('electricEndDate3', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">水道開始日3</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.waterStartDate3 && new Date(formik.values.waterStartDate3)}
                  onChange={(date) =>
                    formik.setFieldValue('waterStartDate3', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-3">
              <label className="block text-sm font-medium text-gray-700">水道停止日3</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.values.closeTypes.includes('close_type1') && 'bg-gray-300'
                  }`}
                  selected={formik.values.waterEndDate3 && new Date(formik.values.waterEndDate3)}
                  onChange={(date) =>
                    formik.setFieldValue('waterEndDate3', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>
            <div className="col-span-4" />
          </div>
          {/* 
          <div className="sticky bottom-0 bg-gray-100 flex justify-center">
            <Button
              type="submit"
              className="flex justify-center items-center w-40 px-4 py-2 text-sm font-medium text-white bg-red-500 border border-transparent rounded-md shadow-sm hover:bg-red-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
              loading={loading}
              disabled={!formik.dirty || loading || !formik.isValid}>
              保存
              {formik.dirty && formik.isValid && <ExclamationIcon className="w-8" />}
            </Button>
          </div> */}
        </form>
      </div>
    </Layout>
  );
};

export {BaseForm};
