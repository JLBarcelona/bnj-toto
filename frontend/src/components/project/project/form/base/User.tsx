import {User} from '@/api/index';
import React from 'react';

type Props = {
  user: User;
};
const User = ({user}: Props) => {
  return (
    <div>
      <span className="relative inline-flex items-center rounded-full border border-gray-400 px-3 py-0.5 text-sm">
        {user?.lastName} {user?.firstName}
      </span>
    </div>
  );
};

export {User};
