import {useProjectQuery, useUpdateProjectMutation} from '@/api/index';
import ja from 'date-fns/locale/ja';
import {useFormik} from 'formik';
import moment from 'moment';
import React from 'react';
import DatePicker, {registerLocale} from 'react-datepicker';
import {useParams} from 'react-router-dom';
import {Layout} from '../../Layout';
import {Tab as FormTab} from '../Tab';

registerLocale('ja', ja);

const ManagementForm = () => {
  const {id} = useParams<{id: string}>();
  const {data: {project = null} = {}} = useProjectQuery({variables: {id}});
  const [udpateProject, {loading}] = useUpdateProjectMutation();

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      isVermiculite: project?.isVermiculite,
      vermiculiteDate: project?.vermiculiteDate || '',
      isManagerAsbestosDocument1: project?.isManagerAsbestosDocument1,
      isManagerAsbestosDocument2: project?.isManagerAsbestosDocument2,
      isManagerAsbestosDocument3: project?.isManagerAsbestosDocument3,
      isOfficeworkAsbestosDocument1: project?.isOfficeworkAsbestosDocument1,
      isOfficeworkAsbestosDocument2: project?.isOfficeworkAsbestosDocument2,
      isOfficeworkAsbestosDocument3: project?.isOfficeworkAsbestosDocument3,
      processCreatedDate: project?.processCreatedDate || '',
    },
    onSubmit: (values) => {
      udpateProject({variables: {id, attributes: values}});
    },
  });

  const managerCheckList = [
    {
      label: '石綿事前調査報告書（公社提出用）',
      value: 'isManagerAsbestosDocument1',
      check: formik.values.isManagerAsbestosDocument1,
    },
    {
      label: '石綿事前調査報告書（電子申請用）',
      value: 'isManagerAsbestosDocument2',
      check: formik.values.isManagerAsbestosDocument2,
    },
    {
      label: '墨出し・ガス漏報告書',
      value: 'isManagerAsbestosDocument3',
      check: formik.values.isManagerAsbestosDocument3,
    },
  ];

  const officeworkerCheckList = [
    {
      label: '石綿事前調査報告書（公社提出用）',
      value: 'isOfficeworkAsbestosDocument1',
      check: formik.values.isOfficeworkAsbestosDocument1,
    },
    {
      label: '石綿事前調査報告書（電子申請用）',
      value: 'isOfficeworkAsbestosDocument2',
      check: formik.values.isOfficeworkAsbestosDocument2,
    },
    {
      label: '墨出し・ガス漏報告書',
      value: 'isOfficeworkAsbestosDocument3',
      check: formik.values.isOfficeworkAsbestosDocument3,
    },
  ];
  if (!project) return null;

  return (
    <Layout>
      <FormTab />

      <div className="mt-4">
        <form className="p-6 space-y-6" onSubmit={formik.handleSubmit}>
          {/* <Button
            type="submit"
            className="fixed top-60 right-10 z-10 flex justify-center items-center w-40 px-4 py-2 text-sm font-medium text-white bg-red-500 border border-transparent rounded-md shadow-sm hover:bg-red-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
            loading={loading}
            disabled={!formik.dirty || loading || !formik.isValid}>
            保存
            {formik.dirty && formik.isValid && <ExclamationIcon className="w-8" />}
          </Button> */}

          <div className="grid grid-cols-1 mt-6 gap-y-2 gap-x-4 sm:grid-cols-6">
            <div className="col-span-6">
              <h3 className="text-lg font-medium leading-6 text-gray-900">蛭石調査</h3>
            </div>

            <div className="col-span-1">
              <div className="flex space-x-6">
                <div className="flex items-center">
                  <input
                    id="isVermiculiteTrue"
                    name="isVermiculite"
                    type="radio"
                    checked={formik.values.isVermiculite}
                    className="w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500"
                    onChange={() => formik.setFieldValue('isVermiculite', true) && formik.handleSubmit()}
                  />
                  <label
                    htmlFor="isVermiculiteTrue"
                    className="block ml-3 text-sm font-medium text-gray-700 cursor-pointer">
                    有
                  </label>
                </div>
                <div className="flex items-center">
                  <input
                    id="isVermiculiteFalse"
                    name="isVermiculite"
                    type="radio"
                    checked={!formik.values.isVermiculite}
                    className="w-4 h-4 text-indigo-600 border-gray-300 focus:ring-indigo-500"
                    onChange={() => formik.setFieldValue('isVermiculite', false) && formik.handleSubmit()}
                  />
                  <label
                    htmlFor="isVermiculiteFalse"
                    className="block ml-3 text-sm font-medium text-gray-700 cursor-pointer">
                    無
                  </label>
                </div>
              </div>
            </div>

            <div className="col-span-2">
              <label className="block text-sm font-medium text-gray-700">蛭石調査完了日</label>
              <div className="rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.vermiculiteDate && new Date(formik.values.vermiculiteDate)}
                  onChange={(date) =>
                    formik.setFieldValue('vermiculiteDate', moment(date).format('YYYY-MM-DD')) && formik.handleSubmit()
                  }
                />
              </div>
            </div>

            <div className="col-span-6 mt-10">
              <h3 className="text-lg font-medium leading-6 text-gray-900">石綿調査報告管理</h3>
            </div>

            <label className="block text-sm font-medium text-gray-700">監督</label>
            <div className="flex col-span-6 space-x-8">
              {managerCheckList.map((managerCheck) => (
                <div className="flex items-center h-5">
                  <input
                    id={managerCheck.value}
                    name={managerCheck.value}
                    type="checkbox"
                    className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                    value={managerCheck.value}
                    checked={managerCheck.check}
                    onChange={(e) => {
                      formik.setFieldValue(managerCheck.value, e.target.checked) && formik.handleSubmit();
                    }}
                    onBlur={formik.handleBlur}
                  />
                  <div className="ml-1 text-sm">
                    <label className="font-medium text-gray-700">{managerCheck.label}</label>
                  </div>
                </div>
              ))}
            </div>

            <label className="block mt-6 text-sm font-medium text-gray-700">事務</label>
            <div className="flex col-span-6 space-x-8">
              {officeworkerCheckList.map((officeworkerCheck) => (
                <div className="flex items-center h-5">
                  <input
                    id={officeworkerCheck.value}
                    name={officeworkerCheck.value}
                    type="checkbox"
                    className="w-5 h-5 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                    value={officeworkerCheck.value}
                    checked={officeworkerCheck.check}
                    onChange={(e) => {
                      formik.setFieldValue(officeworkerCheck.value, e.target.checked) && formik.handleSubmit();
                    }}
                    onBlur={formik.handleBlur}
                  />
                  <div className="ml-1 text-sm">
                    <label className="font-medium text-gray-700">{officeworkerCheck.label}</label>
                  </div>
                </div>
              ))}
            </div>

            <div className="col-span-2 mt-10">
              <label className="block text-sm font-medium text-gray-700">工程表作成日</label>
              <div className="flex mt-1 rounded-md shadow-sm">
                <DatePicker
                  locale="ja"
                  dateFormat="yyyy/MM/dd"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  selected={formik.values.processCreatedDate && new Date(formik.values.processCreatedDate)}
                  onChange={(date) =>
                    formik.setFieldValue('processCreatedDate', moment(date).format('YYYY-MM-DD')) &&
                    formik.handleSubmit()
                  }
                />
              </div>
            </div>
          </div>

          {/* <div className="sticky bottom-0 bg-gray-100 flex justify-center">
            <Button
              type="submit"
              className="flex justify-center items-center w-40 px-4 py-2 text-sm font-medium text-white bg-red-500 border border-transparent rounded-md shadow-sm hover:bg-red-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500"
              loading={loading}
              disabled={!formik.dirty || loading || !formik.isValid}>
              保存
              {formik.dirty && formik.isValid && <ExclamationIcon className="w-8" />}
            </Button>
          </div> */}
        </form>
      </div>
    </Layout>
  );
};

export {ManagementForm};
