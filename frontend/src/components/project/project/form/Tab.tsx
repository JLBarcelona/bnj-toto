import React from 'react';
import {Link, useParams} from 'react-router-dom';

const Tab = () => {
  const {id} = useParams<{id: string}>();
  const currentPath = (path: string) => location.pathname.indexOf(path) > -1;

  return (
    <nav className="flex mt-6 space-x-4" aria-label="Tabs">
      <Link
        to={`/projects/${id}/form/base`}
        className={`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${
          currentPath('form/base') && 'bg-gray-200'
        }`}>
        現場詳細
      </Link>

      <Link
        to={`/projects/${id}/form/docs`}
        className={`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${
          currentPath('form/docs') && 'bg-gray-200'
        }`}>
        書類管理
      </Link>

      <Link
        to={`/projects/${id}/form/management${location.search}`}
        className={`px-3 py-2 text-sm font-medium text-gray-500 rounded-md hover:text-gray-700 ${
          currentPath('form/management') && 'bg-gray-200'
        }`}>
        現場管理
      </Link>
    </nav>
  );
};

export {Tab};
