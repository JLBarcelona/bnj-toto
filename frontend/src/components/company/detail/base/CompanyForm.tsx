import {Company, CompanyType, useRemoveCompanyMutation, useUpdateCompanyMutation} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {Link, useNavigate, useParams} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';

type FormProps = {
  name: string;
  companyTypeIds: string[];
  isOrderEach: boolean;
  isOrderMonthly: boolean;
};
type Props = {
  company: Company;
  companyTypes: CompanyType[];
};
const CompanyForm = (props: Props) => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const [updateCompany, {loading}] = useUpdateCompanyMutation({
    onCompleted: (data) => {
      if (!data.updateCompany.error) {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/companies${location.search}`);
        }, 500);
      } else {
        alert(data.updateCompany.error);
      }
    },
  });

  const [removeCompany] = useRemoveCompanyMutation({
    onCompleted: (data) => {
      if (data.removeCompany.error) {
        alert(data.removeCompany.error);
      } else {
        navigate(`/companies${location.search}`);
      }
    },
  });
  const handleRemove = () => {
    if (confirm('削除してよろしいですか？')) {
      removeCompany({variables: {id}});
      toast.success('削除しました');
    }
  };

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<FormProps>({
    defaultValues: {
      name: props.company.name,
      companyTypeIds: props.company.companyTypeIds,
      isOrderEach: props.company.isOrderEach,
      isOrderMonthly: props.company.isOrderMonthly,
    },
  });

  const onSubmit = (data) => {
    updateCompany({variables: {id, attributes: data}});
  };

  return (
    <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
      <div className="px-4 sm:px-6">
        <nav className="flex" aria-label="Breadcrumb">
          <ol role="list" className="flex items-center space-x-4">
            <li>
              <div className="flex items-center">
                <Link
                  to={`/companies${location.search}`}
                  className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                  業者マスター一覧
                </Link>
              </div>
            </li>
            <li>
              <div className="flex items-center">
                <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">{props.company.name}</p>
              </div>
            </li>
          </ol>
        </nav>

        <div className="mt-10 md:col-span-2">
          <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
            <div>
              <label htmlFor="name" className="block text-sm font-medium text-gray-700">
                名称
              </label>
              <input
                type="text"
                className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                {...register('name', {required: true})}
              />
              {errors.name?.type === 'required' && <p className="text-red-400">必須項目です</p>}
            </div>

            <div>
              <label className="block text-sm font-medium text-gray-700">業務種別</label>
              <div className="flex mb-2 space-x-7">
                {props.companyTypes.map((companyType) => (
                  <div className="flex items-center space-x-2" key={companyType.id}>
                    <input
                      id={companyType.id}
                      value={companyType.id}
                      type="checkbox"
                      className="w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                      {...register('companyTypeIds')}
                    />
                    <label className="block text-sm font-medium text-gray-700">{companyType.name}</label>
                  </div>
                ))}
              </div>
            </div>

            <div className="mt-4">
              <label className="block text-sm font-medium text-gray-700">契約種別</label>
              <div className="flex items-center space-x-4">
                <div className="flex items-center space-x-2">
                  <input
                    id="isOrderEach"
                    type="checkbox"
                    className="w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                    {...register('isOrderEach')}
                  />
                  <label className="block text-sm font-medium text-gray-700">都度契約</label>
                </div>

                <div className="flex items-center space-x-2">
                  <input
                    id="isOrderMonthly"
                    type="checkbox"
                    className="w-5 h-10 text-indigo-600 border-gray-300 rounded focus:ring-indigo-500 cursor-pointer"
                    {...register('isOrderMonthly')}
                  />
                  <label className="block text-sm font-medium text-gray-700">月次契約</label>
                </div>
              </div>
            </div>

            <div className="py-3 text-right">
              <div className="inline-flex items-center px-4 py-2 mr-4 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                <Link to={`/companies${location.search}`}>戻る</Link>
              </div>

              <Button
                type="button"
                className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-gray-300 rounded-sm shadow-sm hover:bg-red-200"
                onClick={handleRemove}>
                削除
              </Button>
              <Button
                type="submit"
                className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                disabled={!isDirty || !isValid}
                loading={loading}>
                保存
              </Button>
              <Toaster position="top-right" />
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export {CompanyForm};
