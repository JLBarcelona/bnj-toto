import {useCreateCompanyMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import ReactModal from 'react-modal';
import {useNavigate} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';

type Props = {
  isOpen: boolean;
  onClose: () => void;
};

const customStyles = {
  content: {
    top: '30%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '600px',
    height: '300px',
  },
};
const CompanyForm = ({isOpen, onClose}: Props) => {
  const navigate = useNavigate();
  const [createCompany, {loading, data}] = useCreateCompanyMutation({
    onCompleted: (data) => {
      if (!data.createCompany.error) {
        toast.success('保存しました');
        setTimeout(() => {
          handleClose();
          navigate(`/companies/${data.createCompany.company.id}`);
        }, 500);
      }
    },
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: {isDirty, isValid, errors},
  } = useForm<{name: string}>({
    defaultValues: {
      name: '',
    },
  });

  const onSubmit = (data) => {
    createCompany({variables: {attributes: data}});
  };

  const handleClose = () => {
    reset();
    onClose();
  };

  return (
    <div className="w-screen max-w-xl">
      <ReactModal isOpen={isOpen} onRequestClose={handleClose} style={customStyles}>
        <h1 className="text-xl">業者新規作成</h1>
        {data?.createCompany.error && <p className="text-red-500">{data.createCompany.error}</p>}

        <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('name', {required: true})}
            />
            {errors.name?.type === 'required' && <p className="text-red-400">名称は必須項目です</p>}
          </div>

          <div className="py-3 text-right flex items-center space-x-4">
            <Button
              type="button"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-black border border-gray-400 rounded-sm shadow-sm"
              onClick={() => handleClose()}>
              キャンセル
            </Button>
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              disabled={!isDirty || !isValid}
              loading={loading}>
              保存
            </Button>
            <Toaster position="top-right" />
          </div>
        </form>
      </ReactModal>
    </div>
  );
};

export {CompanyForm};
