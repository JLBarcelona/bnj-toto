import {useCompanyQuery, useCompanyTypesQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {CompanyForm} from '../detail/base/CompanyForm';

type FormProps = {
  name: string;
  zipcode: string;
  address: string;
  tel: string;
  fax: string;
  companyTypeIds: string[];
  isOrderEach: boolean;
  isOrderMonthly: boolean;
};
const Company = () => {
  const {id} = useParams<{id: string}>();
  const {data: {company = null} = {}} = useCompanyQuery({
    variables: {id},
  });
  const {data: {companyTypes = []} = {}} = useCompanyTypesQuery();

  if (!company || !companyTypes) return null;

  return <CompanyForm company={company} companyTypes={companyTypes} />;
};

export {Company};
