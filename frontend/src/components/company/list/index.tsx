import {useCompaniesQuery} from '@/api/index';
import {Button} from '@/components/ui';
import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {CompanyForm} from '../detail/new/CompanyForm';

const CompanyList = () => {
  const {data: {companies = []} = {}} = useCompaniesQuery({fetchPolicy: 'cache-and-network'});
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">業者マスター</h1>

          <div className="my-4">
            <Button
              type="button"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              onClick={() => setIsOpen(true)}>
              新規作成
            </Button>
            {isOpen && <CompanyForm isOpen={isOpen} onClose={() => setIsOpen(false)} />}
          </div>
          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="w-6 px-6 py-3 text-xs font-medium text-left text-gray-500">
                  ID
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-80">
                  名称
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-80">
                  業務種別
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-80">
                  都度契約
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-80">
                  月次契約
                </th>
              </tr>
            </thead>
            <tbody className={`divide-y divide-gray-200 bg-white`}>
              {companies.map((company) => (
                <tr className="bg-white" key={company.id}>
                  <td className="">
                    <div className="px-2">{company.id}</div>
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900">
                    <Link to={`/companies/${company.id}${location.search}`}>{company.name}</Link>
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900">
                    {company.companyTypes?.map((companyType) => companyType.name).join('/')}
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900">{company.isOrderEach && '都度契約'}</td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900">
                    {company.isOrderMonthly && '月次契約'}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {CompanyList};
