import {useCompaniesQuery, useOrdersQuery, useOutputOrderMutation} from '@/api/index';
import Pagination from '@/components/ui/Pagination';
import {useFormik} from 'formik';
import moment from 'moment';
import React from 'react';
import DatePicker from 'react-datepicker';
import {useSearchParams} from 'react-router-dom';
import {Button} from '../ui';
import {OrderRow} from './OrderRow';

const Order = () => {
  const [params, setParams] = useSearchParams();
  const page = params.get('page');
  const from = params.get('from');
  const to = params.get('to');
  const companyId = params.get('companyId');
  const spreadsheetId = params.get('spreadsheetId');

  const {data: {orders: {orders = [], pagination = {}} = {}} = {}} = useOrdersQuery({
    variables: {
      page: page ? Number(page) : 1,
      search: {from, to: to, companyId},
    },
    fetchPolicy: 'cache-and-network',
  });

  const {data: {companies = []} = {}} = useCompaniesQuery();

  const formik = useFormik({
    initialValues: {from, to, companyId, spreadsheetId},
    onSubmit: (values) => {
      setParams({
        from: values.from || '',
        to: values.to || '',
        companyId: values.companyId || '',
        page: '1',
        spreadsheetId: values.spreadsheetId || '',
      });
    },
  });

  const [output] = useOutputOrderMutation();
  const handleOutput = () => {
    output({
      variables: {
        spreadsheetId: formik.values.spreadsheetId,
        search: {from: formik.values.from, to: formik.values.to, companyId: formik.values.companyId},
      },
    });
  };

  return (
    <div className="flex flex-col h-full">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 h-full">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">発注一覧</h1>

          <form className="px-10 py-2 mb-4 bg-white" onSubmit={formik.handleSubmit}>
            <div className="mt-6">
              <label className="block text-sm font-medium text-gray-700">発注日</label>
              <div className="flex items-center space-x-4">
                <div className="flex mt-1 rounded-md shadow-sm">
                  <DatePicker
                    locale="ja"
                    dateFormat="yyyy/MM/dd"
                    selected={formik.values.from && new Date(formik.values.from)}
                    className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    onChange={(date) => formik.setFieldValue('from', moment(date).format('YYYY-MM-DD'))}
                  />
                </div>
                <span> - </span>
                <div className="flex mt-1 rounded-md shadow-sm">
                  <DatePicker
                    locale="ja"
                    dateFormat="yyyy/MM/dd"
                    selected={formik.values.to && new Date(formik.values.to)}
                    className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    onChange={(date) => formik.setFieldValue('to', moment(date).format('YYYY-MM-DD'))}
                  />
                </div>
              </div>

              <label className="block text-sm font-medium text-gray-700">業者</label>
              <div className="relative w-64">
                <select
                  className="inline-flex justify-center w-full px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500"
                  value={formik.values.companyId}
                  onChange={(e) => formik.setFieldValue('companyId', e.target.value)}>
                  <option value="">業者を選択してください</option>
                  {companies.map((company) => (
                    <option value={company.id}>{company.name}</option>
                  ))}
                </select>
              </div>

              <div className="sm:col-span-4">
                <label className="block text-sm font-medium text-gray-700">スプレッドシートID</label>

                <div className="mt-1">
                  <input
                    name="spreadsheetId"
                    type="text"
                    className="block w-44 px-4 py-2 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                    value={formik.values.spreadsheetId}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                  />
                </div>
              </div>
            </div>

            <div className="mt-6 flex space-x-2">
              <Button
                type="button"
                className="text-white w-40 px-4 py-2 text-sm font-medium bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-300"
                onClick={() => formik.handleSubmit()}>
                検索
              </Button>

              <Button
                type="button"
                disabled={!formik.values.spreadsheetId}
                className="text-white w-44 px-4 py-2 text-sm font-medium bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-300"
                onClick={() => handleOutput()}>
                スプレッドシート出力
              </Button>
            </div>
          </form>

          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-12">
                  ID
                </th>
                <th scope="col" className="px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500 w-28">
                  発注日
                </th>
                <th scope="col" className="w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  業者
                </th>
                <th scope="col" className="w-28 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  指示番号
                </th>
                <th scope="col" className="w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  案件名
                </th>
                <th scope="col" className="w-44 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  案件住所
                </th>
                <th scope="col" className="w-20 px-2 py-3 text-xs font-medium tracking-wider text-left text-gray-500">
                  金額
                </th>
                <th />
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-400">
              {orders.map((order) => (
                <OrderRow order={order} key={order?.id} />
              ))}
            </tbody>
          </table>

          <Pagination
            totalCount={pagination.totalCount || 0}
            currentPage={pagination.currentPage || 0}
            totalPages={pagination.totalPages || 0}
            pageSize={20}
          />
        </div>
      </div>
    </div>
  );
};

export {Order};
