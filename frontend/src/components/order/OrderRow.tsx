import {Order} from '@/api/index';
import React from 'react';

type Props = {
  order: Order;
};
const OrderRow = ({order}: Props) => {
  return (
    <tr className="bg-white">
      <td className="text-sm px-2">{order?.id}</td>
      <td className="py-2 text-sm">{order.orderDate}</td>
      <td className="px-2 py-4 text-sm">{order.company.name}</td>
      <td className="px-2 py-4 text-sm">
        {order.project.orderNumberType}-{order.project.orderNumber}
      </td>
      <td className="px-2 py-4 text-sm">{order.project.name}</td>
      <td className="px-2 py-4 text-sm">{order.project.address}</td>
      <td className="px-2 py-4 text-sm">{order.totalPrice?.toLocaleString()}</td>
      <td className="px-2 py-4 text-sm"></td>
    </tr>
  );
};

export {OrderRow};
