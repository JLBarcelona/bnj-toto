import {useCreateRoomTypeModelMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate, useParams} from 'react-router-dom';
import {Layout} from './Layout';

const RoomTypeNewForm = () => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const [createRoomTypeModel, {loading, data}] = useCreateRoomTypeModelMutation({
    onCompleted: (data) => {
      if (!data.createRoomTypeModel.error) {
        navigate(`/roomtypes`);
      }
    },
  });

  const {
    register,
    handleSubmit,
    formState: {errors, isDirty, isValid},
  } = useForm<{name: string}>({mode: 'all'});

  const onSubmit = (data) => {
    createRoomTypeModel({variables: {attributes: data}});
  };

  return (
    <Layout>
      <div className="mt-10 md:col-span-2 w-screen max-w-xl">
        {data?.createRoomTypeModel.error && <p className="text-red-500">{data?.createRoomTypeModel.error}</p>}
        <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('name', {required: true})}
            />
            {errors.name?.type === 'required' && <p className="text-red-400">名称は必須項目です</p>}
          </div>

          <div className="py-3 text-right flex items-center">
            <Button
              type="button"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm"
              onClick={() => navigate(`/roomtypes`)}>
              戻る
            </Button>
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              disabled={!isDirty || !isValid}
              loading={loading}>
              保存
            </Button>
          </div>
        </form>
      </div>
    </Layout>
  );
};

export {RoomTypeNewForm};
