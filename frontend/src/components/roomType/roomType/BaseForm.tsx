import {useRoomTypeQuery, useUpdateRoomTypeModelMutation} from '@/api/index';
import {Button} from '@/components/ui';
import {useFormik} from 'formik';
import React from 'react';
import {useNavigate, useParams} from 'react-router-dom';
import * as Yup from 'yup';
import {Layout} from './Layout';

const validateSchema = Yup.object().shape({
  name: Yup.string().trim().required('必須項目です'),
});

const RoomTypeBaseForm = () => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const [udpateRoomTypeModel, {loading}] = useUpdateRoomTypeModelMutation();
  const {data: {roomType = null} = {}} = useRoomTypeQuery({
    variables: {id},
  });

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: validateSchema,
    initialValues: {
      name: roomType?.name || '',
    },
    onSubmit: (values) => {
      udpateRoomTypeModel({variables: {id, attributes: values}}).then(() => navigate(`/roomtypes`));
    },
  });

  if (!roomType) return null;

  return (
    <Layout roomType={roomType}>
      <div className="mt-10 md:col-span-2 w-screen max-w-xl">
        <form className="p-6 space-y-6" onSubmit={formik.handleSubmit}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              name="name"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
          </div>

          <div className="py-3 text-right">
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              loading={loading}>
              保存
            </Button>
          </div>
        </form>
      </div>
    </Layout>
  );
};

export {RoomTypeBaseForm};
