import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {RoomTypeBaseForm} from './BaseForm';
import {RoomTypeNewForm} from './NewForm';

const RoomTypeContainer = () => {
  return (
    <Routes>
      <Route path="new" element={<RoomTypeNewForm />} />
      <Route path="base" element={<RoomTypeBaseForm />} />
    </Routes>
  );
};

export {RoomTypeContainer};
