import {RoomType} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import React from 'react';
import {Link, useParams} from 'react-router-dom';
import {ReactNode} from 'react-transition-group/node_modules/@types/react';

type Props = {
  children: ReactNode;
  roomType?: RoomType;
};
const Layout = (props: Props) => {
  const {id} = useParams<{id: string}>();
  const currentPath = (path: string) => location.pathname.indexOf(path) > -1;

  return (
    <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
      <div className="px-4 sm:px-6">
        <nav className="flex" aria-label="Breadcrumb">
          <ol role="list" className="flex items-center space-x-4">
            <li>
              <div className="flex items-center">
                <Link
                  to={'/roomtypes'}
                  className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700"
                  aria-current={undefined}>
                  部屋種別
                </Link>
              </div>
            </li>
            <li>
              <div className="flex items-center">
                <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">{props.roomType?.name}</p>
              </div>
            </li>
          </ol>
        </nav>

        <div className="hidden sm:block">
          <div className="border-b border-gray-200">
            <nav className="flex -mb-px space-x-8" aria-label="Tabs">
              <Link to={`/roomtypes/${id}/base${location.search}`}>
                <div
                  className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                    currentPath('base')
                      ? 'border-indigo-500 text-indigo-600'
                      : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
                  }`}>
                  基本情報
                </div>
              </Link>
            </nav>
          </div>
        </div>
        {props.children}
      </div>
    </div>
  );
};

export {Layout};
