import React, {useRef, useState} from 'react';
import {Link} from 'react-router-dom';
import {useDrag, useDrop} from 'react-dnd';
import type {XYCoord, Identifier} from 'dnd-core';
import {RoomType} from '@/api/index';

interface DragItem {
  index: number;
  id: string;
  type: string;
}

type Props = {
  id: string;
  index: number;
  roomType: RoomType;
  moveItem: (id: string, dragIndex: number, hoverIndex: number) => void;
  order: (id: string, index: number) => void;
};
const RoomTypeRow = ({id, index, roomType, moveItem, order}: Props) => {
  const ref = useRef<HTMLDivElement>(null);
  const [{handlerId}, drop] = useDrop<DragItem, void, {handlerId: Identifier | null}>({
    accept: 'RoomType',
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item: DragItem, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect();

      // Get vertical middle
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

      // Determine mouse position
      const clientOffset = monitor.getClientOffset();

      // Get pixels to the top
      const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top;

      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%

      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }

      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }

      // Time to actually perform the action
      moveItem(item.id, dragIndex, hoverIndex);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex;
    },
  });

  const [{isDragging}, drag] = useDrag({
    type: 'RoomType',
    item: () => {
      return {id, index};
    },
    collect: (monitor: any) => ({
      isDragging: monitor.isDragging(),
    }),
    end: (item) => {
      console.log('🚀 ~ file: RoomType.tsx:87 ~ RoomType ~ item', item);
      order(item.id, item.index);
    },
  });

  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));
  return (
    <div ref={ref} className="bg-white flex w-full" style={{opacity}}>
      <div className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
        <Link to={`/roomtypes/${roomType.id}/base`}>{roomType.name}</Link>
      </div>
    </div>
  );
};

export {RoomTypeRow};
