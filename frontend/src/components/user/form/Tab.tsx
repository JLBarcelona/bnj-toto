import React from 'react';
import {useParams, Link} from 'react-router-dom';

const Tab = () => {
  const {id} = useParams<{id: string}>();
  const isActive = (path) => location.pathname.endsWith(path);

  return (
    <div className="hidden sm:block">
      <div className="border-b border-gray-200">
        <nav className="flex -mb-px space-x-8" aria-label="Tabs">
          <div
            className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer ${
              isActive('base')
                ? 'border-indigo-500 text-indigo-600'
                : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
            }`}>
            <Link to={`/users/${id}/base`}>基本情報</Link>
          </div>
          <div
            className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer ${
              isActive('password')
                ? 'border-indigo-500 text-indigo-600'
                : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
            }`}>
            <Link to={`/users/${id}/password`}>パスワード</Link>
          </div>
        </nav>
      </div>
    </div>
  );
};

export {Tab};
