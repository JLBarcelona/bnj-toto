import {useCreateUserMutation} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import {Button} from '@/components/ui';
import {useFormik} from 'formik';
import React from 'react';
import {Link, useNavigate} from 'react-router-dom';
import * as Yup from 'yup';

const validateSchema = Yup.object().shape({
  loginId: Yup.string().required('必須項目です'),
  email: Yup.string().email('メールアドレスの形式が不正です'),
  lastName: Yup.string().trim().required('必須項目です'),
  firstName: Yup.string().trim().required('必須項目です'),
  password: Yup.string().trim().required('必須項目です').min(6, '6文字以上で設定してください'),
  passwordConfirmation: Yup.string()
    .trim()
    .required('必須項目です')
    .oneOf([Yup.ref('password'), null], 'パスワードが一致していません'),
});

const NewForm = () => {
  const navigate = useNavigate();
  const [create, {data, loading}] = useCreateUserMutation({
    onCompleted: (data) => {
      if (!data.createUser.error) {
        navigate(`/users${location.search}`);
      }
    },
  });

  const formik = useFormik({
    enableReinitialize: true,
    validationSchema: validateSchema,
    initialValues: {
      loginId: '',
      email: '',
      lastName: '',
      firstName: '',
      password: '',
      passwordConfirmation: '',
    },
    onSubmit: (values) => {
      create({variables: {attributes: values}});
    },
  });

  return (
    <div className="w-full">
      <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          <nav className="flex" aria-label="Breadcrumb">
            <ol role="list" className="flex items-center space-x-4">
              <li>
                <div className="flex items-center">
                  <Link to={'/users'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                    従業員
                  </Link>
                </div>
              </li>
              <li>
                <div className="flex items-center">
                  <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                  <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">新規作成</p>
                </div>
              </li>
            </ol>
          </nav>

          <div className="mt-10 md:col-span-2">
            {data?.createUser.error && (
              <div className="p-4 rounded-md bg-red-50">
                <div className="flex">
                  <div className="ml-3">
                    <div className="mt-2 text-sm text-red-700">
                      <ul role="list" className="pl-5 space-y-1 list-disc">
                        <li>{data?.createUser.error}</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            )}
            <form className="p-6 space-y-6" onSubmit={formik.handleSubmit}>
              <div>
                <label className="block text-sm font-medium text-gray-700">ログインID</label>
                <input
                  type="text"
                  name="loginId"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.errors.loginId && formik.touched.loginId
                      ? 'border-red-500 bg-red-200 focus:border-red-500'
                      : ''
                  }`}
                  value={formik.values.loginId}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.loginId && formik.touched.loginId && (
                  <span className="text-red-500">{formik.errors.loginId}</span>
                )}
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">姓</label>
                <input
                  type="text"
                  name="lastName"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.errors.lastName && formik.touched.lastName
                      ? 'border-red-500 bg-red-200 focus:border-red-500'
                      : ''
                  }`}
                  value={formik.values.lastName}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.lastName && formik.touched.lastName && (
                  <span className="text-red-500">{formik.errors.lastName}</span>
                )}
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">名</label>
                <input
                  type="text"
                  name="firstName"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.errors.firstName && formik.touched.firstName
                      ? 'border-red-500 bg-red-200 focus:border-red-500'
                      : ''
                  }`}
                  value={formik.values.firstName}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.firstName && formik.touched.firstName && (
                  <span className="text-red-500">{formik.errors.firstName}</span>
                )}
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">パスワード</label>
                <input
                  type="password"
                  name="password"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.errors.password && formik.touched.password
                      ? 'border-red-500 bg-red-200 focus:border-red-500'
                      : ''
                  }`}
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.password && formik.touched.password && (
                  <span className="text-red-500">{formik.errors.password}</span>
                )}
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">パスワード(確認)</label>
                <input
                  type="password"
                  name="passwordConfirmation"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.errors.passwordConfirmation && formik.touched.passwordConfirmation
                      ? 'border-red-500 bg-red-200 focus:border-red-500'
                      : ''
                  }`}
                  value={formik.values.passwordConfirmation}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.passwordConfirmation && formik.touched.passwordConfirmation && (
                  <span className="text-red-500">{formik.errors.passwordConfirmation}</span>
                )}
              </div>

              <div>
                <label className="block text-sm font-medium text-gray-700">メールアドレス</label>
                <input
                  type="email"
                  name="email"
                  className={`block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
                    formik.errors.email && formik.touched.email ? 'border-red-500 bg-red-200 focus:border-red-500' : ''
                  }`}
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                />
                {formik.errors.email && formik.touched.email && (
                  <span className="text-red-500">{formik.errors.email}</span>
                )}
              </div>

              <div className="py-3 text-right">
                <Button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  disabled={loading || !formik.isValid || !formik.dirty}
                  loading={loading}>
                  保存
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export {NewForm};
