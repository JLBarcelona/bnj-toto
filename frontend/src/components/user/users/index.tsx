import {useUsersQuery} from '@/api/index';
import React, {useEffect} from 'react';
import {Link, useSearchParams} from 'react-router-dom';
import {Tab} from './Tab';

const UserList = () => {
  const [params, setParams] = useSearchParams();
  const active = params.get('active');

  const {data: {users = []} = {}} = useUsersQuery({
    variables: {search: {active: active === 'true'}},
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (active === null || active === undefined) {
      setParams({active: 'true'});
    }
  }, []);

  if (active === null || active === undefined) return null;

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">従業員</h1>

          <Tab />

          <Link to="/users/new">
            <div className="inline-flex justify-center px-4 py-2 mt-10 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              新規作成
            </div>
          </Link>

          <table className="min-w-full mt-4 divide-y divide-gray-200">
            <thead className="bg-gray-50">
              <tr>
                <th scope="col" className="w-6 px-6 py-3 text-xs font-medium text-left text-gray-500">
                  ID
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-80">
                  ログインID
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500 w-60">
                  名称
                </th>
                <th scope="col" className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  査定同行者
                </th>
                <th />
              </tr>
            </thead>
            <tbody className="bg-white divide-y divide-gray-200">
              {users.map((user) => (
                <tr className="bg-white" key={user.id}>
                  <td className="ml-10">
                    <div className="px-2 cursor-pointer">{user.id}</div>
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
                    <Link to={`/users/${user.id}/base`}>{user.loginId}</Link>
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
                    <Link to={`/users/${user.id}/base`}>
                      {user.lastName} {user.firstName}
                    </Link>
                  </td>
                  <td className="px-6 py-4 text-sm font-medium text-gray-900 whitespace-nowrap">
                    {user.isAssesment && (
                      <div>
                        <span className="inline-flex items-center rounded-md bg-yellow-100 px-2.5 py-0.5 text-sm font-medium text-yellow-800">
                          査定同行者
                        </span>
                      </div>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export {UserList};
