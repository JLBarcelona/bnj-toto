import React from 'react';
import {useSearchParams} from 'react-router-dom';

const Tab = () => {
  const [params, setParams] = useSearchParams();
  const active = params.get('active');

  return (
    <div className="hidden sm:block">
      <div className="border-b border-gray-200">
        <nav className="flex -mb-px space-x-8" aria-label="Tabs">
          <div onClick={() => setParams({active: 'true'})}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer ${
                active === 'true'
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              有効
            </div>
          </div>
          <div onClick={() => setParams({active: 'false'})}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm cursor-pointer ${
                active !== 'true'
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              無効
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
};

export {Tab};
