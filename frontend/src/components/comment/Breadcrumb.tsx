import {Comment} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import React from 'react';
import {Link} from 'react-router-dom';

type Props = {
  comment: Comment;
};
const Breadcrumb = ({comment}: Props) => {
  return (
    <nav className="flex" aria-label="Breadcrumb">
      <ol role="list" className="flex items-center space-x-4">
        <li>
          <div className="flex items-center">
            <Link to={'/comments'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
              コメント一覧
            </Link>
          </div>
        </li>
        <li>
          <div className="flex items-center">
            <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
            <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">{comment?.id}</p>
          </div>
        </li>
      </ol>
    </nav>
  );
};

export {Breadcrumb};
