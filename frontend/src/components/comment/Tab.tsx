import React from 'react';
import {Link, useParams} from 'react-router-dom';

const Tab = () => {
  const {id} = useParams<{id: string}>();
  const currentPath = (path: string) => location.pathname.indexOf(path) > -1;

  return (
    <div className="hidden sm:block">
      <div className="border-b border-gray-200">
        <nav className="flex -mb-px space-x-8" aria-label="Tabs">
          <Link to={`/comments/${id}/base${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('base')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              基本情報
            </div>
          </Link>
          <Link to={`/comments/${id}/codes${location.search}`}>
            <div
              className={`whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm ${
                currentPath('codes')
                  ? 'border-indigo-500 text-indigo-600'
                  : 'border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300'
              }`}>
              コード
            </div>
          </Link>
        </nav>
      </div>
    </div>
  );
};

export {Tab};
