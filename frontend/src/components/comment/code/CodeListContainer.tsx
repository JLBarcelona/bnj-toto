import {useCommentQuery, useCommentTypesQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {CodeList} from './CodeList';

const CodeListContainer = () => {
  const {id} = useParams<{id: string}>();
  const {data: {comment = null} = {}} = useCommentQuery({variables: {id}});

  if (!comment) return null;

  return <CodeList comment={comment} />;
};

export {CodeListContainer};
