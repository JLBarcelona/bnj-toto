import React from 'react';
import {Comment, ItemCode, useRemoveCommentCodeMutation} from '@/api/index';
import {Button} from '@/components/ui';

type Props = {
  comment: Comment;
  itemCode: ItemCode;
};

const CodeRow = (props: Props) => {
  const [removeCommentCode] = useRemoveCommentCodeMutation();
  const handleRemoveCommentCode = () => {
    removeCommentCode({variables: {id: props.comment.id, itemCodeId: props.itemCode.id}});
  };

  return (
    <tr className="bg-white">
      <td>
        <Button
          type="button"
          className="mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          onClick={() => {
            handleRemoveCommentCode();
          }}>
          削除
        </Button>
      </td>
      <td className="text-sm px-2">{props.itemCode.code}</td>
      <td className="text-sm px-2">{props.itemCode.name}</td>
    </tr>
  );
};

export {CodeRow};
