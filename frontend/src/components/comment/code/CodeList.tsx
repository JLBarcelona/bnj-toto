import React, {useState} from 'react';
import {Comment} from '@/api/index';
import {Breadcrumb} from '../Breadcrumb';
import {Tab} from '../Tab';
import {CodeRow} from './CodeRow';
import ReactModal from 'react-modal';
import {ItemCodeModal} from './ItemCodeModal';

type Props = {
  comment: Comment;
};

const customStyles = {
  content: {
    top: '40%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: '900px',
    height: '600px',
  },
};
const CodeList = (props: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleClose = () => {
    setIsModalOpen(false);
  };

  return (
    <div className="mt-4">
      <Breadcrumb comment={props.comment} />
      <Tab />

      <button
        type="button"
        className="mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        onClick={() => setIsModalOpen(true)}>
        追加
      </button>

      <table className="min-w-full mt-4 divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr>
            <th className="w-20" />
            <th className="px-2 py-3 text-xs font-medium text-left text-gray-500 w-28">コード</th>
            <th className="px-2 py-3 text-xs font-medium text-left text-gray-500">名称</th>
            <th />
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-400">
          {props.comment.itemCodes.map((itemCode) => (
            <CodeRow comment={props.comment} itemCode={itemCode} key={itemCode.id} />
          ))}
        </tbody>
      </table>
      <ReactModal isOpen={isModalOpen} onRequestClose={handleClose} style={customStyles}>
        <ItemCodeModal comment={props.comment} onClose={handleClose} />
      </ReactModal>
    </div>
  );
};

export {CodeList};
