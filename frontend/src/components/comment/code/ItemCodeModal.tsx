import React, {useState} from 'react';
import {Comment, useAddCommentCodeMutation, useAllItemsQuery} from '@/api/index';
import {Button} from '@/components/ui';
import {useForm} from 'react-hook-form';

type Props = {
  comment: Comment;
  onClose: () => void;
};

const ItemCodeModal = (props: Props) => {
  const [keyword, setKeyword] = useState(null);

  const {register, handleSubmit} = useForm<{keyword: string}>({
    defaultValues: {
      keyword: '',
    },
  });
  const {data: {allItems = []} = {}} = useAllItemsQuery({
    variables: {search: {keyword}},
    skip: !keyword,
  });

  const onSubmit = (data) => {
    setKeyword(data.keyword);
  };

  const [addCommentCode] = useAddCommentCodeMutation({
    onCompleted: () => {
      props.onClose();
    },
  });
  const handleAddCommentCode = (itemCodeId: string) => {
    addCommentCode({variables: {id: props.comment.id, itemCodeId}});
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <input
        type="text"
        className="block w-full px-4 py-1 mt-1 border border-gray-400 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
        {...register('keyword')}
        onBlur={handleSubmit(onSubmit)}
      />
      <table className="min-w-full mt-4 divide-y divide-gray-200">
        <thead className="bg-gray-50">
          <tr>
            <th className="w-20" />
            <th className="px-2 py-3 text-xs font-medium text-left text-gray-500 w-28">コード</th>
            <th className="px-2 py-3 text-xs font-medium text-left text-gray-500">名称</th>
            <th />
          </tr>
        </thead>
        <tbody className="bg-white divide-y divide-gray-400">
          {allItems.map((item) => (
            <tr key={`item-${item.id}`}>
              <td>
                <Button
                  type="button"
                  className="mr-2 block px-2.5 py-1.5 border border-transparent text-xs font-medium rounded shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  onClick={() => {
                    handleAddCommentCode(item.itemCode.id);
                  }}>
                  追加
                </Button>
              </td>
              <td className="px-2 py-3 text-sm text-gray-500">{item.itemCode.code}</td>
              <td className="px-2 py-3 text-sm text-gray-500">{item.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </form>
  );
};

export {ItemCodeModal};
