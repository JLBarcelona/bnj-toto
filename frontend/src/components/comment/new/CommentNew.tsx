import React from 'react';
import {useForm} from 'react-hook-form';
import {Link, useNavigate} from 'react-router-dom';
import toast, {Toaster} from 'react-hot-toast';
import {useCommentTypesQuery, useCreateCommentMutation} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import {Button} from '@/components/ui';

type FormProps = {
  comment: string;
  commentTypeId: string;
};
const CommentNew = () => {
  const navigate = useNavigate();
  const [create, {loading}] = useCreateCommentMutation({
    onCompleted: (data) => {
      if (!data.createComment.error) {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/comments${location.search}`);
        }, 500);
      }
    },
  });
  const {data: {commentTypes = []} = {}} = useCommentTypesQuery();

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<FormProps>({mode: 'all'});

  const onSubmit = (data) => {
    create({variables: {attributes: data}});
  };

  return (
    <div className="w-full">
      <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
        <div className="px-4 sm:px-6">
          <nav className="flex" aria-label="Breadcrumb">
            <ol role="list" className="flex items-center space-x-4">
              <li>
                <div className="flex items-center">
                  <Link to={'/comments'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                    コメント一覧
                  </Link>
                </div>
              </li>
              <li>
                <div className="flex items-center">
                  <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                  <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">新規作成</p>
                </div>
              </li>
            </ol>
          </nav>

          <div className="mt-10 md:col-span-2">
            <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
              <div>
                <label className="block text-sm font-medium text-gray-700">コメント種別</label>
                <select
                  {...register('commentTypeId', {required: true})}
                  className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
                  <option value=""></option>
                  {commentTypes.map((commentType) => (
                    <option value={commentType.id} key={`commentType-${commentType.id}`}>
                      {commentType.name}
                    </option>
                  ))}
                </select>
                {errors.commentTypeId?.type === 'required' && <p className="text-red-400">必須項目です</p>}
              </div>
              <div>
                <label htmlFor="comment" className="block text-sm font-medium text-gray-700">
                  名称
                </label>
                <input
                  type="text"
                  className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
                  {...register('comment', {required: true})}
                />
                {errors.comment?.type === 'required' && <p className="text-red-400">必須項目です</p>}
              </div>

              <div className="py-3 text-right">
                <Button
                  type="submit"
                  className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  disabled={loading || !isValid || !isDirty}
                  loading={loading}>
                  保存
                </Button>
                <Toaster position="top-right" />
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export {CommentNew};
