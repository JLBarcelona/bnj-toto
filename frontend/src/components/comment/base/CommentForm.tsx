import React from 'react';
import {Comment, CommentType, useRemoveCommentMutation, useUpdateCommentMutation} from '@/api/index';
import {Button} from '@/components/ui';
import {useForm} from 'react-hook-form';
import {useNavigate} from 'react-router';
import toast, {Toaster} from 'react-hot-toast';
import {Breadcrumb} from '../Breadcrumb';
import {Tab} from '../Tab';

type Props = {
  comment: Comment;
  commentTypes: CommentType[];
};

type FormProps = {
  comment: string;
  commentTypeId: string;
};
const CommentForm = (props: Props) => {
  const navigate = useNavigate();
  const [updateComment, {loading}] = useUpdateCommentMutation({
    onCompleted: (data) => {
      if (data.updateComment.error) {
        alert(data.updateComment.error);
      } else {
        toast.success('保存しました');
        setTimeout(() => {
          navigate(`/comments${location.search}`);
        }, 500);
      }
    },
  });
  const [removeComment] = useRemoveCommentMutation({
    onCompleted: (data) => {
      if (data.removeComment.error) {
        alert(data.removeComment.error);
      } else {
        navigate(`/comments${location.search}`);
      }
    },
  });
  const handleRemoveComment = () => {
    if (confirm('削除してよろしいですか？')) {
      removeComment({variables: {id: props.comment.id}});
    }
  };

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<FormProps>({
    defaultValues: {
      comment: props.comment.comment,
      commentTypeId: props.comment.commentTypeId,
    },
  });

  const onSubmit = (data) => {
    updateComment({variables: {id: props.comment.id, attributes: data}});
  };

  return (
    <div className="mt-4">
      <Breadcrumb comment={props.comment} />

      <Tab />
      <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-1 mt-6 gap-y-6 gap-x-4 sm:grid-cols-6">
          <div>
            <label className="block text-sm font-medium text-gray-700">コメント種別</label>
            <select
              {...register('commentTypeId')}
              className="block w-full py-2 pl-3 pr-10 mt-1 text-base border border-gray-300 rounded-md focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm">
              <option value=""></option>
              {props.commentTypes.map((commentType) => (
                <option value={commentType.id} key={`commentType-${commentType.id}`}>
                  {commentType.name}
                </option>
              ))}
            </select>
          </div>

          <div className="col-span-6">
            <label className="block text-sm font-medium text-gray-700">名称</label>
            <input
              type="text"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('comment', {required: true})}
            />
            {errors.comment?.type === 'required' && <p className="text-red-400">必須項目です</p>}
          </div>
        </div>

        <div className="py-3 text-right flex items-center">
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-gray-400 border border-gray-400 rounded-sm shadow-sm hover:bg-gray-200"
            onClick={() => navigate(`/comments${location.search}`)}>
            戻る
          </Button>
          <Button
            type="button"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-red-400 border border-red-400 rounded-sm shadow-sm hover:bg-red-200"
            onClick={handleRemoveComment}>
            削除
          </Button>
          <Button
            type="submit"
            className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            disabled={loading || !isValid || !isDirty}
            loading={loading}>
            保存
          </Button>
          <Toaster position="top-right" />
        </div>
      </form>
    </div>
  );
};

export {CommentForm};
