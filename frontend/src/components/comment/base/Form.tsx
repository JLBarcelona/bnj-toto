import {useCommentQuery, useCommentTypesQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {CommentForm} from './CommentForm';

const Comment = () => {
  const {id} = useParams<{id: string}>();
  const {data: {comment = null} = {}} = useCommentQuery({variables: {id}});
  const {data: {commentTypes = []} = {}} = useCommentTypesQuery();

  if (!comment || !commentTypes) return null;

  return <CommentForm comment={comment} commentTypes={commentTypes} />;
};

export {Comment};
