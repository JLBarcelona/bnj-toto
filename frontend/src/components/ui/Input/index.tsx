import React from 'react';

type Props = {
  className?: string;
  disabled?: boolean;
  name: string;
  value: string;
  type: 'text' | 'email' | 'number' | 'password' | 'datetime-local' | 'time';
  required?: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur: (event: React.ChangeEvent<HTMLInputElement>) => void;
  error: boolean;
  errorMessage: string;
};

const Input: React.FC<Props> = ({
  name,
  value,
  type,
  required = false,
  error,
  errorMessage,
  className,
  onChange,
  onBlur,
}) => {
  return (
    <>
      <input
        name={name}
        type={type}
        required={required}
        className={`block w-full px-3 py-2 placeholder-gray-400 border border-gray-300 rounded-md shadow-sm appearance-none focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ${
          error ? 'border-red-500 bg-red-200 focus:border-red-500' : ''
        } ${className}`}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
      {error && <span className="text-sm text-red-600">{errorMessage}</span>}
    </>
  );
};
export {Input};
