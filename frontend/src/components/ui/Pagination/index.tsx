import React, {useMemo} from 'react';
import {useSearchParams} from 'react-router-dom';
import {DOTS, usePagination} from './usePagination';

type Props = {
  totalCount: number;
  currentPage: number;
  totalPages: number;
  pageSize: number;
  siblingCount?: number;
};
const Pagination = ({totalCount, siblingCount = 1, currentPage, pageSize, totalPages}: Props) => {
  const [params, setParams] = useSearchParams();
  const entries = useMemo(() => Array.from(new Set(params.entries())), [params]);
  const setParam = (key: string, value: string) => {
    let newParams = {};
    entries.forEach((entry) => (newParams[entry[0]] = entry[1]));
    setParams({...newParams, [key]: value});
  };

  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  if (currentPage === 0 || !paginationRange || paginationRange?.length < 2) {
    return null;
  }

  const onNext = () => {
    // setParams({page: String(currentPage + 1)});
    setParam('page', String(currentPage + 1));
  };

  const onPrevious = () => {
    // setParams({page: String(currentPage - 1)});
    setParam('page', String(currentPage - 1));
  };

  return (
    <>
      <div>
        <nav className="relative z-0 inline-flex -space-x-px rounded-md shadow-sm" aria-label="Pagination">
          {currentPage > 1 && (
            <a
              className="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-pointer rounded-l-md hover:bg-gray-50"
              onClick={onPrevious}>
              <span className="sr-only">Previous</span>
              <svg
                className="w-5 h-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </a>
          )}
          {currentPage === 1 && (
            <span className="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 bg-gray-100 border border-gray-300 rounded-l-md hover:bg-gray-50">
              <svg
                className="w-5 h-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </span>
          )}

          {paginationRange.map((pageNumber) => {
            if (pageNumber === DOTS) {
              return (
                <span
                  className="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300"
                  key={`pageNumber-${pageNumber}`}>
                  ...
                </span>
              );
            }

            return (
              <a
                key={pageNumber}
                className={`bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium cursor-pointer
								${pageNumber === currentPage ? 'border-indigo-500 text-indigo-600 z-10 bg-indigo-50' : ''}
								`}
                onClick={() => setParam('page', pageNumber)}>
                {pageNumber}
              </a>
            );
          })}

          {currentPage < totalPages && (
            <a
              className={`relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 cursor-pointer
						`}
              onClick={onNext}>
              <span className="sr-only">Next</span>
              <svg
                className="w-5 h-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </a>
          )}
          {currentPage === totalPages && (
            <span className="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-300 bg-gray-100 border border-gray-300 rounded-l-md hover:bg-gray-50">
              <span className="sr-only">Next</span>
              <svg
                className="w-5 h-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true">
                <path
                  fillRule="evenodd"
                  d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                  clipRule="evenodd"
                />
              </svg>
            </span>
          )}
        </nav>
      </div>
      {/* <ul className={classnames('pagination-container', {[className]: className})}>
      <li
        className={classnames('pagination-item', {
          disabled: currentPage === 1,
        })}
        onClick={onPrevious}>
        <div className="arrow left" />
      </li>
      {paginationRange.map((pageNumber) => {
        if (pageNumber === DOTS) {
          return <li className="pagination-item dots">&#8230;</li>;
        }

        return (
          <li
            className={classnames('pagination-item', {
              selected: pageNumber === currentPage,
            })}
            onClick={() => onPageChange(pageNumber)}>
            {pageNumber}
          </li>
        );
      })}
      <li
        className={classnames('pagination-item', {
          disabled: currentPage === lastPage,
        })}
        onClick={onNext}>
        <div className="arrow right" />
      </li>
    </ul> */}
    </>
  );
};

export default Pagination;
