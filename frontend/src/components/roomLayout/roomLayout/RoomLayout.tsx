import {useRoomLayoutQuery} from '@/api/index';
import React from 'react';
import {useParams} from 'react-router-dom';
import {RoomLayoutForm} from './RoomLayoutForm';

const RoomLayout = () => {
  const {id} = useParams<{id: string}>();
  const {data: {roomLayout = null} = {}} = useRoomLayoutQuery({
    variables: {id},
  });

  if (!roomLayout) return null;

  return <RoomLayoutForm roomLayout={roomLayout} />;
};

export {RoomLayout};
