import {RoomLayout} from '@/api/index';
import {ChevronRightIcon} from '@/components/icons';
import React from 'react';
import {Link} from 'react-router-dom';
import {ReactNode} from 'react-transition-group/node_modules/@types/react';

type Props = {
  children: ReactNode;
  roomLayout?: RoomLayout;
};
const Layout = (props: Props) => {
  return (
    <div className="flex flex-col h-full py-6 overflow-y-scroll bg-white">
      <div className="px-4 sm:px-6">
        <nav className="flex" aria-label="Breadcrumb">
          <ol role="list" className="flex items-center space-x-4">
            <li>
              <div className="flex items-center">
                <Link to={'/roomlayouts'} className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">
                  部屋レイアウト
                </Link>
              </div>
            </li>
            <li>
              <div className="flex items-center">
                <ChevronRightIcon className="flex-shrink-0 w-5 h-5 text-gray-400" aria-hidden="true" />
                <p className="ml-4 text-sm font-medium text-gray-500 hover:text-gray-700">{props.roomLayout?.name}</p>
              </div>
            </li>
          </ol>
        </nav>

        {props.children}
      </div>
    </div>
  );
};

export {Layout};
