import {RoomLayout, useUpdateRoomLayoutMutation} from '@/api/index';
import {Button} from '@/components/ui';
import React from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate, useParams} from 'react-router-dom';
import {Layout} from './Layout';

type Props = {
  roomLayout: RoomLayout;
};
const RoomLayoutForm = (props: Props) => {
  const {id} = useParams<{id: string}>();
  const navigate = useNavigate();
  const [udpateRoomLayout, {loading}] = useUpdateRoomLayoutMutation({
    onCompleted: (data) => {
      if (!data.updateRoomLayout.error) {
        navigate(`/roomlayouts${location.search}`);
      }
    },
  });

  const {
    register,
    handleSubmit,
    formState: {isDirty, isValid, errors},
  } = useForm<RoomLayout>({
    defaultValues: {
      name: props.roomLayout.name,
      price1: props.roomLayout.price1,
      price2: props.roomLayout.price2,
    },
  });

  const onSubmit = (data) => {
    udpateRoomLayout({variables: {id, attributes: data}});
  };

  return (
    <Layout roomLayout={props.roomLayout}>
      <div className="mt-10 md:col-span-2 w-screen max-w-xl">
        <form className="p-6 space-y-6" onSubmit={handleSubmit(onSubmit)}>
          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              名称
            </label>
            <input
              type="text"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('name', {required: true})}
            />
            {errors.name?.type === 'required' && <p className="text-red-400">必須項目です</p>}
          </div>

          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              工事金額：25万円（税抜）未満
            </label>
            <input
              type="number"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('price1', {valueAsNumber: true})}
            />
          </div>

          <div>
            <label htmlFor="name" className="block text-sm font-medium text-gray-700">
              工事金額：25万円（税抜）以上
            </label>
            <input
              type="number"
              className="block w-full px-4 py-1 mt-1 border border-gray-300 rounded-sm shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
              {...register('price2', {valueAsNumber: true})}
            />
          </div>

          <div className="py-3 text-right">
            <Button
              type="submit"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              disabled={!isDirty || !isValid || loading}
              loading={loading}>
              保存
            </Button>
          </div>
        </form>
      </div>
    </Layout>
  );
};

export {RoomLayoutForm};
