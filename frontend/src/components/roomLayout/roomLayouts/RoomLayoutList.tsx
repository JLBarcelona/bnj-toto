import {RoomLayout, useUpdateRoomLayoutOrderMutation} from '@/api/index';
import update from 'immutability-helper';
import React, {useEffect, useState} from 'react';
import {DndProvider} from 'react-dnd';
import {HTML5Backend} from 'react-dnd-html5-backend';
import {Link} from 'react-router-dom';
import {RoomLayoutRow} from './RoomLayoutRow';

type Props = {
  roomLayouts: RoomLayout[];
};
const RoomLayoutList = (props: Props) => {
  const [tempRoomLayouts, setTempRoomLayouts] = useState([]);
  const [order] = useUpdateRoomLayoutOrderMutation();

  const moveItem = (id: string, dragIndex: number, hoverIndex: number) => {
    console.log(`update: id: ${id}`);
    console.log(`update: dragIndex: ${dragIndex}`);
    console.log(`update: hoverIndex: ${hoverIndex}`);

    setTempRoomLayouts((images: RoomLayout[]) =>
      update(images, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, images[dragIndex] as RoomLayout],
        ],
      }),
    );
  };

  const handleOrder = (id: string, index: number) => {
    console.log('🚀 ~ file: index.tsx:28 ~ handleOrder ~ id', id);
    console.log('🚀 ~ file: index.tsx:28 ~ handleOrder ~ index', index);
    order({variables: {id, attributes: {position: index}}});
  };

  useEffect(() => {
    setTempRoomLayouts(props.roomLayouts);
  }, [props.roomLayouts]);

  return (
    <div className="flex flex-col">
      <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8">
          <h1 className="mb-4 text-2xl font-bold">部屋レイアウト</h1>

          <Link to="/roomlayouts/new">
            <div className="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-sm shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              新規作成
            </div>
          </Link>

          <div className="min-w-full mt-4 divide-y divide-gray-200">
            <div className="bg-gray-50">
              <div className="flex">
                <div className="w-44 px-6 py-3 text-xs font-medium text-left text-gray-500">名称</div>
                <div className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  工事金額：25万円（税抜）未満
                </div>
                <div className="px-6 py-3 text-xs font-medium text-left text-gray-500">
                  工事金額：25万円（税抜）以上
                </div>
              </div>
            </div>
            <DndProvider backend={HTML5Backend}>
              <div className="bg-white divide-y divide-gray-200">
                {tempRoomLayouts.map((roomLayout, i) => (
                  <RoomLayoutRow
                    id={roomLayout.id}
                    index={i}
                    roomLayout={roomLayout}
                    key={roomLayout.id}
                    moveItem={moveItem}
                    order={handleOrder}
                  />
                ))}
              </div>
            </DndProvider>
          </div>
        </div>
      </div>
    </div>
  );
};

export {RoomLayoutList};
