import {useRoomLayoutsQuery} from '@/api/index';
import React from 'react';
import {RoomLayoutList} from './RoomLayoutList';

const RoomLayouts = () => {
  const {data: {roomLayouts = null} = {}} = useRoomLayoutsQuery({
    fetchPolicy: 'cache-and-network',
  });

  if (!roomLayouts) return null;

  return <RoomLayoutList roomLayouts={roomLayouts} />;
};

export {RoomLayouts};
