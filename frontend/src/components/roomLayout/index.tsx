import React from 'react';
import {Route, Routes} from 'react-router-dom';
import {RoomLayoutNewForm} from './roomLayout/NewForm';
import {RoomLayout} from './roomLayout/RoomLayout';
import {RoomLayouts} from './roomLayouts';

const RoomLayoutContainer = () => {
  return (
    <Routes>
      <Route path="new" element={<RoomLayoutNewForm />} />
      <Route path=":id" element={<RoomLayout />} />
      <Route index element={<RoomLayouts />} />
    </Routes>
  );
};

export {RoomLayoutContainer};
