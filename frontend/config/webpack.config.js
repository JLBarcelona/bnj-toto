const path = require('path');
const env = process.env.NODE_ENV;
const webpack = require('webpack');
const {WebpackManifestPlugin} = require('webpack-manifest-plugin');
const WebpackBar = require('webpackbar');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

const config = {
  entry: {
    user: ['./frontend/src/user.tsx'],
  },
  output: {
    path: path.resolve('public/assets'),
    publicPath: '/',
    assetModuleFilename: 'images/[hash][ext][query]',
  },
  resolve: {
    alias: {
      api: path.join(__dirname, '../src/api'),
      components: path.join(__dirname, '../src/components'),
      graphqls: path.join(__dirname, '../src/graphqls'),
      helpers: path.join(__dirname, '../src/helpers'),
      queries: path.join(__dirname, '../src/api/queries'),
      mutations: path.join(__dirname, '../src/api/mutations'),
      fragments: path.join(__dirname, '../src/api/fragments'),
      hooks: path.join(__dirname, '../src/hooks'),
    },
    extensions: ['.ts', '.tsx', '.js', '.json'],
    plugins: [new TsconfigPathsPlugin()],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(env),
    }),
    new WebpackManifestPlugin({
      fileName: 'webpack-manifest.json',
    }),
    new webpack.IgnorePlugin({
      resourceRegExp: /^\.\/locale$/,
      contextRegExp: /moment$/,
    }),
    new WebpackBar(),
  ],
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'source-map-loader',
      },
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'esbuild-loader',
            options: {
              loader: 'tsx',
              target: 'es2016',
            },
          },
        ],
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', {loader: 'css-loader', options: {importLoaders: 1}}, 'postcss-loader'],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: '@svgr/webpack',
          },
        ],
      },
      {
        test: /\.(png|woff|woff2|eot|ttf)$/,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 100000,
          },
        },
      },
    ],
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'all',
        },
      },
    },
  },
};

module.exports = config;
