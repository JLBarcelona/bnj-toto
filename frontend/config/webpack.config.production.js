const {merge} = require('webpack-merge');
const baseConfig = require('./webpack.config.js');

const config = merge(baseConfig, {
  mode: 'production',
  devtool: 'source-map',
  output: {
    filename: '[name].bundle.[chunkhash:8].js',
  },
  performance: {hints: false},
});

module.exports = config;
