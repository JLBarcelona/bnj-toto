const {merge} = require('webpack-merge');
const baseConfig = require('./webpack.config.js');

const config = merge(baseConfig, {
  output: {
    filename: '[name].js',
    publicPath: 'http://localhost:3500/',
  },
  mode: 'development',
  devtool: 'eval-source-map',
  devServer: {
    compress: true,
    port: 3500,
    hot: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
});

module.exports = config;
