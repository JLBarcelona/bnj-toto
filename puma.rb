#!/usr/bin/env puma

directory '/var/www/toto/current'
rackup "/var/www/toto/current/config.ru"
environment 'staging'

tag ''

pidfile "/var/www/toto/shared/tmp/pids/puma.pid"
state_path "/var/www/toto/shared/tmp/pids/puma.state"
stdout_redirect '/var/www/toto/shared/log/puma_access.log', '/var/www/toto/shared/log/puma_error.log', true


#threads 0,16
threads 0,2



bind 'unix:///var/www/toto/shared/tmp/sockets/puma.sock'

workers 2




#restart_command 'bundle exec puma'


prune_bundler


on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = ""
end
