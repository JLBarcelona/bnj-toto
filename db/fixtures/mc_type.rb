require 'csv'

# MCマスター、建物区分
CSV.foreach('db/fixtures/mc_type.csv').with_index do |csv, i|
  next if i <= 4

  # MCマスター
  name = csv[1]
  mc_type = McType.find_by(name: name)
  unless mc_type
    mc_type = McType.create!(name: name)
  end

  # 建物区分マスター
  building_type_name = csv[2]
  building_type = BuildingType.find_by(name: building_type_name)
  unless building_type
    building_type = BuildingType.create!(name: building_type_name)
  end
end
