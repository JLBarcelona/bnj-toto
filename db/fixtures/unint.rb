require 'csv'

CSV.foreach('db/fixtures/unit.csv', headers: true) do |csv|
  Unit.find_or_create_by(name: csv[1])
end
