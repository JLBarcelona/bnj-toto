require 'csv'

# 業者マスター
CSV.foreach('db/fixtures/company_type.csv').with_index do |csv, i|
  next if i <= 4

  # 名称
  name = csv[1]
  company_type = CompanyType.find_by(name: name)
  unless company_type
    CompanyType.create!(name: name)
  end
end
