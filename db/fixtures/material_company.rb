require 'csv'

# 材料業者マスターを会社マスターとして作成
CSV.foreach('db/fixtures/material_company.csv').with_index do |csv, i|
  next if i <= 4

  # 名称
  name = csv[1]
  company = Company.find_by(name: name)
  unless company
    company = Company.create!(name: name, is_material: true, is_order_monthly: true)
  end
end
