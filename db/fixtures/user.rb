require 'csv'

password = 'password'

User.seed(:id, [
  { id: 1, role: :admin, login_id: 'toto0', last_name: 'toto1', first_name: 'A', password: password, password_confirmation: password, is_assesment: true },
  { id: 2, role: :admin, login_id: 'toto01', last_name: 'toto2', first_name: 'B', password: password, password_confirmation: password, is_assesment: false },
])

CSV.foreach('db/fixtures/user.csv').with_index do |csv, i|
  next if i <= 5

  last_name = csv[1]
  first_name = csv[2]
  login_id = csv[3]
  password = csv[4]
  unless login_id.present?
    login_id = "toto#{i - 5}"
  end

  user = User.find_by(first_name: first_name, last_name: last_name)
  unless user
    user = User.create!(
      login_id: login_id,
      password: password,
      password_confirmation: password,
      last_name: last_name,
      first_name: first_name,
      role: :admin,
      is_assesment: true,
      active: true
    )
  end
end
