require 'csv'

# 部屋タイプマスター
CSV.foreach('db/room_model_type.csv').with_index do |csv, i|
  next if i == 0

  name = csv[1]&.strip
  room_model_type = RoomModelType.find_by(name: name)
  unless room_model_type
    room_model_type = RoomModelType.create!(name: name)
  end
end
p "room_model_type:done"

# 部屋レイアウトマスター
CSV.foreach('db/room_layout.csv').with_index do |csv, i|
  next if i <= 4

  name = csv[1]&.strip
  room_layout = RoomLayout.find_by(name: name)
  if !room_layout && name.present?
    room_layout = RoomLayout.create!(name: name)
  end
end
p "room_layout:done"

# 業者マスター 
CSV.foreach('db/company.csv').with_index do |csv, i|
  next if i <= 4

  # 業者マスター
  name = csv[1]
  company = Company.find_by(name: name)
  unless company
    company = Company.create!(name: name, is_order_each: true)
  end

  # 業者区分
  (6..10).each do |i|
    company_type_name = csv[i]
    company_type = CompanyType.find_by(name: company_type_name)
    if company_type.present? && !company.company_types.where(id: company_type.id).exists?
      company.company_types << company_type
    end
  end
end
p "company:done"

# 会社マスターに材料業者を設定
Company.where(is_material: true).each do |company|
  company_type = CompanyType.find_by(name: '材料・電気点検')
  company.company_types = [company_type]
end
p "company is_material:done"

# 項目マスター
CSV.foreach('db/item.csv').with_index do |csv, i|
  next if i <= 4

  code = csv[1]
  item_code = ItemCode.find_by(code: code)
  unless item_code
    item_code = ItemCode.create!(code: code)
  end

  year_value = csv[2]
  year = Year.find_by(year: year_value)
  unless year
    year = Year.create!(year: year_value)
  end

  # unit
  unit_name = csv[3]
  unit = Unit.find_by(name: unit_name)

  # 項目名
  name = csv[4]
  short_name = csv[5]
  price = csv[6]&.gsub(/,/, '')&.to_i
  order_price = csv[7]&.gsub(/,/, '')&.to_i
  item = Item.find_by(item_code: item_code, year: year)
  if item
    item.update!(name: name, short_name: short_name, price: price, order_price: order_price, unit: unit)
  else
    item = Item.create!(item_code: item_code, year: year, name: name, short_name: short_name, unit: unit, price: price, order_price: order_price)
  end

  # 業者区分[O-S列]
  is_order = false
  company_type_ids = []
  (14..18).each do |i|
    value = csv[i]&.strip
    next unless value.present?

    if CompanyType.where(name: value).exists?
      company_type_ids << CompanyType.where(name: value).ids
    end

    # 業者が指定されていれば発注あり
    is_order = true
  end

  # item.update!(company_type_ids: company_types.ids) if company_types.present?
  if company_type_ids.flatten.present?
    item.update!(company_type_ids: company_type_ids.flatten)
  end

  item_code.update!(name: item.name)

  # 発注なしの場合
  unless is_order
    item.update!(order: false)
  end
end
p "item:done"

# コメント
CSV.foreach('db/comment.csv').with_index do |csv, i|
  next if i <= 0

  # ID
  csv_id = csv[0]
  # 分類
  csv_comment_type = csv[1].gsub(/\n/, '')
  # position
  csv_position = csv[2]
  # コメント
  csv_comment = csv[3]
  # コード
  csv_code = csv[4]
  # 備考
  csv_remark = csv[5]

  comment_type = CommentType.find_or_create_by(name: csv_comment_type)
  comment = Comment.find_by(id: csv_id)

  item_code = ItemCode.find_by(code: csv_code)

  unless comment
    comment = Comment.create!(
      comment_type: comment_type,
      comment: csv_comment,
      remark: csv_remark
    )
  end
  if item_code && !comment.item_codes.where(id: item_code.id).exists?
    comment.item_codes << item_code
  end
end
p "comment:done"

# 物件マスター
CSV.foreach('db/building.csv').with_index do |csv, i|
  next if i <= 4

  # 名称
  name = csv[1]
  address = csv[2]
  building = Building.find_by(name: name)
  unless building 
    building = Building.create!(
      name: name,
      address: address
    )
  end

  # 号棟
  building_no_name = csv[4]&.strip
  building_number = building.building_numbers.find_by(name: building_no_name)
  unless building_number
    building_number = building.building_numbers.create!(name: building_no_name)
  end

  # 部屋
  room_name = csv[5]&.strip
  room = building_number.rooms.find_by(name: room_name)
  unless room
    room = building_number.rooms.create!(name: room_name)
  end

  if room
    # 部屋タイプ
    room_model_type_name = csv[6]&.strip
    if room_model_type_name.present?
      room_model_type = RoomModelType.find_by(name: room_model_type_name)
      unless room_model_type
        room_model_type = RoomModelType.create!(name: room_model_type_name)
      end

      if room_model_type
        room.update!(room_model_type: room_model_type)
      end
    end

    # レイアウトタイプ
    room_layout_name = csv[7]&.strip
    if room_layout_name.present?
      room_layout = RoomLayout.find_by(name: room_layout_name)
      unless room_layout
        room_layout = RoomLayout.create!(name: room_layout_name)
      end

      if room_layout
        room.update!(room_layout: room_layout)
      end
    end
  end

  # MC
  mc = csv[8]
  mc_type = McType.find_by(name: mc)
  building.update!(mc_type: mc_type)

  # MC
  mc = csv[8]
  mc_type = McType.find_by(name: mc)
  building.update!(mc_type: mc_type) if mc_type.present?

  # 建物区分
  buiding_type_name = csv[9]
  building_type = BuildingType.find_by(name: buiding_type_name)
  building.update!(building_type: building_type) if building_type.present?

  # 部屋種別
  room_type_ids = []
  (11..20).each do |i|
    room_type_name = csv[i]
    next unless room_type_name.present?

    room_type = RoomType.find_by(name: room_type_name)
    unless room_type
      room_type = RoomType.create!(name: room_type_name)
    end
    room_type_ids << room_type.id
  end

  room.room_types = ::RoomType.where(id: room_type_ids)
end
p "building:done"
