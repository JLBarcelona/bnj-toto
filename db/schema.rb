# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_03_15_031113) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "building_numbers", force: :cascade do |t|
    t.bigint "building_id", null: false
    t.string "name", null: false
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["building_id"], name: "index_building_numbers_on_building_id"
  end

  create_table "building_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "buildings", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.integer "rank"
    t.bigint "mc_type_id"
    t.bigint "building_type_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["building_type_id"], name: "index_buildings_on_building_type_id"
    t.index ["mc_type_id"], name: "index_buildings_on_mc_type_id"
  end

  create_table "comment_item_codes", force: :cascade do |t|
    t.bigint "comment_id", null: false
    t.bigint "item_code_id"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["comment_id"], name: "index_comment_item_codes_on_comment_id"
    t.index ["item_code_id"], name: "index_comment_item_codes_on_item_code_id"
  end

  create_table "comment_types", force: :cascade do |t|
    t.string "name", null: false
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "comments", force: :cascade do |t|
    t.bigint "comment_type_id", null: false
    t.string "comment", null: false
    t.integer "position"
    t.text "remark"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["comment_type_id"], name: "index_comments_on_comment_type_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "is_order_each", default: false, null: false, comment: "個別発注フラグ"
    t.boolean "is_order_monthly", default: false, null: false, comment: "月次発注フラグ"
    t.string "zipcode"
    t.string "address"
    t.string "tel"
    t.string "fax"
    t.boolean "is_material", default: false, null: false, comment: "材料会社フラグ"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "company_company_types", force: :cascade do |t|
    t.bigint "company_type_id", null: false
    t.bigint "company_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_company_company_types_on_company_id"
    t.index ["company_type_id"], name: "index_company_company_types_on_company_type_id"
  end

  create_table "company_types", force: :cascade do |t|
    t.string "name", null: false
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "image_comments", force: :cascade do |t|
    t.bigint "project_image_id", null: false
    t.string "code", null: false
    t.text "text", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_image_id"], name: "index_image_comments_on_project_image_id"
  end

  create_table "item_codes", force: :cascade do |t|
    t.string "code"
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "items", force: :cascade do |t|
    t.bigint "item_code_id", null: false
    t.bigint "year_id", null: false
    t.bigint "unit_id"
    t.bigint "company_id"
    t.string "name"
    t.string "short_name"
    t.integer "price", comment: "単価"
    t.integer "order_price", comment: "発注単価"
    t.integer "company_type_ids", array: true
    t.boolean "order", default: true, comment: "発注フラグ"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_items_on_company_id"
    t.index ["item_code_id"], name: "index_items_on_item_code_id"
    t.index ["unit_id"], name: "index_items_on_unit_id"
    t.index ["year_id"], name: "index_items_on_year_id"
  end

  create_table "jkk_users", force: :cascade do |t|
    t.string "last_name", null: false
    t.string "first_name", null: false
    t.string "email"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "mc_types", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "company_id", null: false
    t.integer "order_type", default: 0, null: false, comment: "0:都度発注, 1:月次発注"
    t.integer "total_price", comment: "合計金額"
    t.date "order_date", comment: "発注日"
    t.boolean "is_invoice", default: false, null: false
    t.date "export_csv_date"
    t.string "csv"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_orders_on_company_id"
    t.index ["project_id"], name: "index_orders_on_project_id"
  end

  create_table "project_companies", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "company_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_project_companies_on_company_id"
    t.index ["project_id"], name: "index_project_companies_on_project_id"
  end

  create_table "project_image_comments", force: :cascade do |t|
    t.bigint "project_image_id"
    t.bigint "comment_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["comment_id"], name: "index_project_image_comments_on_comment_id"
    t.index ["project_image_id"], name: "index_project_image_comments_on_project_image_id"
  end

  create_table "project_images", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "room_type_id"
    t.integer "image_type", null: false
    t.string "image_before"
    t.string "image_after"
    t.string "image"
    t.text "comment"
    t.integer "position", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_project_images_on_project_id"
    t.index ["room_type_id"], name: "index_project_images_on_room_type_id"
  end

  create_table "project_items", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "item_id", null: false
    t.bigint "company_id"
    t.integer "sequence_number"
    t.string "name"
    t.string "code"
    t.decimal "count", comment: "数量"
    t.integer "unit_price", comment: "単価"
    t.integer "amount", comment: "金額"
    t.string "room_type_ids", array: true
    t.datetime "updated_items_at", comment: "明細最終更新日時"
    t.integer "updated_items_user_id", comment: "明細最終更新ユーザー"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["company_id"], name: "index_project_items_on_company_id"
    t.index ["item_id"], name: "index_project_items_on_item_id"
    t.index ["project_id"], name: "index_project_items_on_project_id"
    t.index ["updated_items_user_id"], name: "index_project_items_on_updated_items_user_id"
  end

  create_table "project_reports", force: :cascade do |t|
    t.bigint "project_id"
    t.string "worksheet_project", comment: "シート名-案件一覧"
    t.string "worksheet_room_layout", comment: "シート名-部屋タイプ"
    t.string "worksheet_base", comment: "シート名-基本情報"
    t.string "worksheet_items", comment: "シート名-明細"
    t.string "worksheet_images", comment: "シート名-画像"
    t.string "worksheet_checklist", comment: "シート名-チェックリスト"
    t.string "worksheet_users", comment: "シート名-従業員"
    t.string "worksheet_jkkusers", comment: "シート名-公社担当者"
    t.string "spreadsheet_id1", comment: "スプレッドシートID1"
    t.string "spreadsheet_id2", comment: "スプレッドシートID2"
    t.string "spreadsheet_id3", comment: "スプレッドシートID3"
    t.string "spreadsheet_id4", comment: "スプレッドシートID4"
    t.string "spreadsheet_id5", comment: "スプレッドシートID5"
    t.string "spreadsheet_id6", comment: "スプレッドシートID6"
    t.string "spreadsheet_id7", comment: "スプレッドシートID7"
    t.datetime "spreadsheet_datetime1", comment: "スプレッドシート出力日時-査定表"
    t.datetime "spreadsheet_datetime2", comment: "スプレッドシート出力日時-材料発注"
    t.datetime "spreadsheet_datetime3", comment: "スプレッドシート出力日時-月次発注"
    t.datetime "spreadsheet_datetime4", comment: "スプレッドシート出力日時-チェック表"
    t.datetime "spreadsheet_datetime5", comment: "スプレッドシート出力日時-工事写真報告書1"
    t.datetime "spreadsheet_datetime6", comment: "スプレッドシート出力日時-工事写真報告書2"
    t.datetime "spreadsheet_datetime7", comment: "スプレッドシート出力日時-工事写真報告書2"
    t.boolean "spreadsheet_check_report1", comment: "スプレッドシート-出力可否-工事写真報告書1"
    t.boolean "spreadsheet_check_report2", comment: "スプレッドシート-出力可否-工事写真報告書2"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_project_reports_on_project_id"
  end

  create_table "project_users", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_project_users_on_project_id"
    t.index ["user_id"], name: "index_project_users_on_user_id"
  end

  create_table "projects", force: :cascade do |t|
    t.bigint "year_id"
    t.string "name"
    t.integer "jkk_user_id", comment: "JKK担当者"
    t.integer "assesment_user_id", comment: "担当者-査定"
    t.integer "construction_user_id", comment: "担当者-工事"
    t.integer "total_item_price", comment: "合計金額(小計)"
    t.integer "total_item_price_round", comment: "合計金額(端数)"
    t.integer "tax", comment: "税金"
    t.integer "total_price", comment: "合計金額"
    t.integer "total_price_input", comment: "合計金額(入力)"
    t.datetime "item_upload_date", comment: "1.査定表読み込み日時"
    t.datetime "report_date", comment: "2.施工前写真出力日時"
    t.datetime "checklist_date", comment: "3.最終チェックリスト日時"
    t.datetime "lack_image_date", comment: "4.不足写真日時"
    t.datetime "complete_image_date", comment: "5.写真完了日時"
    t.datetime "invoice_date", comment: "6.請求書類提出日時"
    t.integer "image_status", comment: "写真ステータス"
    t.boolean "jkk_status1", default: false, comment: "①CL済"
    t.boolean "jkk_status2", default: false, comment: "②指示書済"
    t.boolean "jkk_status3", default: false, comment: "③指示書再発行待ち"
    t.boolean "jkk_status4", default: false, comment: "④指示書再発行済み"
    t.string "order_number_type"
    t.string "order_number"
    t.string "address"
    t.string "住所"
    t.date "construction_start_date", comment: "工事開始日"
    t.date "construction_end_date", comment: "工事終了日"
    t.string "item_csv"
    t.string "assessment"
    t.date "assessment_date", comment: "査定日"
    t.string "close_types", comment: "閉鎖区分", array: true
    t.string "close_reason", comment: "閉鎖区分内容"
    t.date "order_date1", comment: "指示書発行日1"
    t.string "order_memo1", comment: "指示書発行メモ1"
    t.date "order_date2", comment: "指示書発行日2"
    t.string "order_memo2", comment: "指示書発行メモ2"
    t.date "order_date3", comment: "指示書発行日3"
    t.string "order_memo3", comment: "指示書発行メモ3"
    t.date "electric_start_date1", comment: "電気開始日1"
    t.date "electric_end_date1", comment: "電気停止日1"
    t.date "water_start_date1", comment: "水道開始日1"
    t.date "water_end_date1", comment: "水道停止日1"
    t.date "electric_start_date2", comment: "電気開始日2"
    t.date "electric_end_date2", comment: "電気停止日2"
    t.date "water_start_date2", comment: "水道開始日2"
    t.date "water_end_date2", comment: "水道停止日2"
    t.date "electric_start_date3", comment: "電気開始日3"
    t.date "electric_end_date3", comment: "電気停止日3"
    t.date "water_start_date3", comment: "水道開始日3"
    t.date "water_end_date3", comment: "水道停止日3"
    t.boolean "is_vermiculite", comment: "蛭石"
    t.boolean "is_finished_vermiculite", comment: "蛭石完了"
    t.date "vermiculite_date", comment: "蛭石調査完了日"
    t.date "process_created_date", comment: "工程表作成日"
    t.date "available_date", comment: "募集可能日"
    t.integer "living_year", comment: "入居年数"
    t.integer "living_month", comment: "入居年数-月"
    t.date "leave_date", comment: "退去日"
    t.date "before_construction_image_date", comment: "施工前写真確認日"
    t.date "after_construction_image_date", comment: "施工後写真確認日"
    t.date "checklist_first_date", comment: "初回チェックリスト日"
    t.date "checklist_last_date", comment: "最終チェックリスト日"
    t.boolean "is_faq", comment: "質疑回答"
    t.boolean "is_lack_image", comment: "不足写真あり"
    t.boolean "is_complete_image", comment: "写真完了"
    t.date "submit_document_date", comment: "書類提出日"
    t.boolean "is_manager_asbestos_document1", comment: "監督-石綿事前調査報告書（公社提出用）"
    t.boolean "is_manager_asbestos_document2", comment: "監督-石綿事前調査報告書（電子申請用）"
    t.boolean "is_manager_asbestos_document3", comment: "監督-墨出し・ガス漏報告書"
    t.boolean "is_officework_asbestos_document1", comment: "事務-石綿事前調査報告書（公社提出用）"
    t.boolean "is_officework_asbestos_document2", comment: "事務-石綿事前調査報告書（電子申請用）"
    t.boolean "is_officework_asbestos_document3", comment: "事務-墨出し・ガス漏報告書"
    t.text "remark", comment: "備考"
    t.datetime "updated_items_at", comment: "明細最終更新日時"
    t.integer "updated_items_user_id", comment: "明細最終更新ユーザー"
    t.integer "edit_user_id", comment: "編集中ユーザー"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "building_id"
    t.bigint "building_number_id"
    t.bigint "room_id"
    t.string "image_report"
    t.index ["building_id"], name: "index_projects_on_building_id"
    t.index ["building_number_id"], name: "index_projects_on_building_number_id"
    t.index ["edit_user_id"], name: "index_projects_on_edit_user_id"
    t.index ["room_id"], name: "index_projects_on_room_id"
    t.index ["updated_items_user_id"], name: "index_projects_on_updated_items_user_id"
    t.index ["year_id"], name: "index_projects_on_year_id"
  end

  create_table "purchase_orders", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.string "file"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_purchase_orders_on_project_id"
  end

  create_table "room_layouts", force: :cascade do |t|
    t.string "name"
    t.integer "price1"
    t.integer "price2"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "room_model_types", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "room_type_rooms", force: :cascade do |t|
    t.bigint "room_type_id", null: false
    t.bigint "room_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["room_id"], name: "index_room_type_rooms_on_room_id"
    t.index ["room_type_id"], name: "index_room_type_rooms_on_room_type_id"
  end

  create_table "room_types", force: :cascade do |t|
    t.string "name"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.bigint "building_number_id", null: false
    t.string "name", null: false
    t.bigint "room_model_type_id"
    t.bigint "room_layout_id"
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["building_number_id"], name: "index_rooms_on_building_number_id"
    t.index ["room_layout_id"], name: "index_rooms_on_room_layout_id"
    t.index ["room_model_type_id"], name: "index_rooms_on_room_model_type_id"
  end

  create_table "units", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "login_id"
    t.string "password_digest"
    t.string "email"
    t.integer "role"
    t.string "last_name"
    t.string "first_name"
    t.string "tel"
    t.boolean "active", default: true
    t.boolean "is_assesment", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "years", force: :cascade do |t|
    t.integer "year"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "building_numbers", "buildings"
  add_foreign_key "buildings", "building_types"
  add_foreign_key "buildings", "mc_types"
  add_foreign_key "comment_item_codes", "comments"
  add_foreign_key "comment_item_codes", "item_codes"
  add_foreign_key "comments", "comment_types"
  add_foreign_key "company_company_types", "companies"
  add_foreign_key "company_company_types", "company_types"
  add_foreign_key "image_comments", "project_images"
  add_foreign_key "items", "companies"
  add_foreign_key "items", "item_codes"
  add_foreign_key "items", "units"
  add_foreign_key "items", "years"
  add_foreign_key "orders", "companies"
  add_foreign_key "orders", "projects"
  add_foreign_key "project_companies", "companies"
  add_foreign_key "project_companies", "projects"
  add_foreign_key "project_image_comments", "comments"
  add_foreign_key "project_image_comments", "project_images"
  add_foreign_key "project_images", "projects"
  add_foreign_key "project_images", "room_types"
  add_foreign_key "project_items", "companies"
  add_foreign_key "project_items", "items"
  add_foreign_key "project_items", "projects"
  add_foreign_key "project_reports", "projects"
  add_foreign_key "project_users", "projects"
  add_foreign_key "project_users", "users"
  add_foreign_key "projects", "building_numbers"
  add_foreign_key "projects", "buildings"
  add_foreign_key "projects", "rooms"
  add_foreign_key "projects", "users", column: "edit_user_id"
  add_foreign_key "projects", "users", column: "updated_items_user_id"
  add_foreign_key "projects", "years"
  add_foreign_key "purchase_orders", "projects"
  add_foreign_key "room_type_rooms", "room_types"
  add_foreign_key "room_type_rooms", "rooms"
  add_foreign_key "rooms", "building_numbers"
  add_foreign_key "rooms", "room_layouts"
  add_foreign_key "rooms", "room_model_types"
end
