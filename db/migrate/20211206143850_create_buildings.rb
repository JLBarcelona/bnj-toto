class CreateBuildings < ActiveRecord::Migration[6.1]
  def change
    create_table :mc_types do |t|
      t.string :name, null: :fasle

      t.timestamps
    end

    create_table :building_types do |t|
      t.string :name, null: :fasle

      t.timestamps
    end

    create_table :buildings do |t|
      t.string :name, null: :fasle
      t.string :address
      t.integer :rank
      t.references :mc_type, foreign_key: true
      t.references :building_type, foreign_key: true

      t.timestamps
    end
  end
end
