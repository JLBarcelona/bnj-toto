class CreateRoomTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :room_types do |t|
      t.string :name, null: :fasle
      t.integer :position

      t.timestamps
    end
  end
end
