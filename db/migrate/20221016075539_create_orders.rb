class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.references :project, null: false, foreign_key: true
      t.references :company, null: false, foreign_key: true
      t.integer :order_type, null: false, default: 0, comment: '0:都度発注, 1:月次発注'
      t.integer :total_price, comment: '合計金額'
      t.date :order_date, comment: '発注日'
      t.boolean :is_invoice, null: false, default: false
      t.date :export_csv_date
      t.string :csv

      t.timestamps
    end
  end
end
