class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :login_id, null: :fasle
      t.string :password_digest, null: :fasle
      t.string :email
      t.integer :role, null: :fasle
      t.string :last_name, null: :fasle
      t.string :first_name, null: :fasle
      t.string :tel
      t.boolean :active, null: :fasle, default: true
      t.boolean :is_assesment, null: :fasle, default: true

      t.timestamps
    end
  end
end
