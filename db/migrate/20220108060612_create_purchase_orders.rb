class CreatePurchaseOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :purchase_orders do |t|
      t.references :project, null: false, foreign_key: true
      t.string :file, null: :fasle

      t.timestamps
    end
  end
end
