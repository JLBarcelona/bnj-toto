class AddReferenceToBuilding < ActiveRecord::Migration[6.1]
  def change
    add_reference :projects, :building, foreign_key: true
    add_reference :projects, :building_number, foreign_key: true
    add_reference :projects, :room, foreign_key: true
  end
end
