class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comment_types do |t|
      t.string :name, null: false
      t.integer :position

      t.timestamps
    end

    create_table :comments do |t|
      t.references :comment_type, null: false, foreign_key: true
      t.string :comment, null: false
      t.integer :position
      t.text :remark

      t.timestamps
    end

    create_table :comment_item_codes do |t|
      t.references :comment, null: false, foreign_key: true
      t.references :item_code, foreign_key: true
      t.integer :position

      t.timestamps
    end
  end
end
