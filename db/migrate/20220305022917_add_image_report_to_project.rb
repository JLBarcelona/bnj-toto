class AddImageReportToProject < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :image_report, :string
  end
end
