class CreateProjectImageComments < ActiveRecord::Migration[6.1]
  def change
    create_table :project_image_comments do |t|
      t.references :project_image, foreign_key: true
      t.references :comment, foreign_key: true

      t.timestamps
    end
  end
end
