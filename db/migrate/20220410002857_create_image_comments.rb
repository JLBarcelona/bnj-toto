class CreateImageComments < ActiveRecord::Migration[6.1]
  def change
    create_table :image_comments do |t|
      t.references :project_image, null: false, foreign_key: true
      t.string :code, null: false
      t.text :text, null: false

      t.timestamps
    end
  end
end
