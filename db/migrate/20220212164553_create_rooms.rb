class CreateRooms < ActiveRecord::Migration[6.1]
  def change
    create_table :room_model_types do |t|
      t.string :name, null: :fasle
      t.integer :position

      t.timestamps
    end

    create_table :room_layouts do |t|
      t.string :name, null: :fasle
      t.integer :price1
      t.integer :price2
      t.integer :position

      t.timestamps
    end

    create_table :rooms do |t|
      t.references :building_number, null: false, foreign_key: true
      t.string :name, null: false
      t.references :room_model_type, foreign_key: true
      t.references :room_layout, foreign_key: true
      t.integer :position

      t.timestamps
    end

    create_table :room_type_rooms do |t|
      t.references :room_type, null: false, foreign_key: true
      t.references :room, null: false, foreign_key: true

      t.timestamps
    end
  end
end
