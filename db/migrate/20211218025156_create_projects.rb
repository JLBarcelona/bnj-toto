class CreateProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :years do |t|
      t.integer :year, null: :fasle

      t.timestamps
    end

    create_table :projects do |t|
      t.references :year, foreign_key: true
      t.string :name, null: :fasle
      t.integer :jkk_user_id, comment: 'JKK担当者'
      t.integer :assesment_user_id, comment: '担当者-査定'
      t.integer :construction_user_id, comment: '担当者-工事'
      t.integer :total_item_price, comment: '合計金額(小計)'
      t.integer :total_item_price_round, comment: '合計金額(端数)'
      t.integer :tax, comment: '税金'
      t.integer :total_price, comment: '合計金額'
      t.integer :total_price_input, comment: '合計金額(入力)'
      t.datetime :item_upload_date, comment: '1.査定表読み込み日時'
      t.datetime :report_date, comment: '2.施工前写真出力日時'
      t.datetime :checklist_date, comment: '3.最終チェックリスト日時'
      t.datetime :lack_image_date, comment: '4.不足写真日時'
      t.datetime :complete_image_date, comment: '5.写真完了日時'
      t.datetime :invoice_date, comment: '6.請求書類提出日時'
      t.integer :image_status, comment: '写真ステータス'
      t.boolean :jkk_status1, null: :fasle, default: false, comment: '①CL済'
      t.boolean :jkk_status2, null: :fasle, default: false, comment: '②指示書済'
      t.boolean :jkk_status3, null: :fasle, default: false, comment: '③指示書再発行待ち'
      t.boolean :jkk_status4, null: :fasle, default: false, comment: '④指示書再発行済み'
      t.string :order_number_type, null: :fasle
      t.string :order_number, null: :fasle
      t.string :address, '住所'
      t.date :construction_start_date, comment: '工事開始日'
      t.date :construction_end_date, comment: '工事終了日'
      t.string :item_csv
      t.string :assessment
      t.date :assessment_date, comment: '査定日'
      t.string :close_types, comment: '閉鎖区分', array: true
      t.string :close_reason, comment: '閉鎖区分内容'
      t.date :order_date1, comment: '指示書発行日1'
      t.string :order_memo1, comment: '指示書発行メモ1'
      t.date :order_date2, comment: '指示書発行日2'
      t.string :order_memo2, comment: '指示書発行メモ2'
      t.date :order_date3, comment: '指示書発行日3'
      t.string :order_memo3, comment: '指示書発行メモ3'
      t.date :electric_start_date1, comment: '電気開始日1'
      t.date :electric_end_date1, comment: '電気停止日1'
      t.date :water_start_date1, comment: '水道開始日1'
      t.date :water_end_date1, comment: '水道停止日1'
      t.date :electric_start_date2, comment: '電気開始日2'
      t.date :electric_end_date2, comment: '電気停止日2'
      t.date :water_start_date2, comment: '水道開始日2'
      t.date :water_end_date2, comment: '水道停止日2'
      t.date :electric_start_date3, comment: '電気開始日3'
      t.date :electric_end_date3, comment: '電気停止日3'
      t.date :water_start_date3, comment: '水道開始日3'
      t.date :water_end_date3, comment: '水道停止日3'
      t.boolean :is_vermiculite, comment: '蛭石'
      t.boolean :is_finished_vermiculite, comment: '蛭石完了'
      t.date :vermiculite_date, comment: '蛭石調査完了日'
      t.date :process_created_date, comment: '工程表作成日'
      t.date :available_date, comment: '募集可能日'
      t.integer :living_year, comment: '入居年数'
      t.integer :living_month, comment: '入居年数-月'
      t.date :leave_date, comment: '退去日'
      t.date :before_construction_image_date, comment: '施工前写真確認日'
      t.date :after_construction_image_date, comment: '施工後写真確認日'
      t.date :checklist_first_date, comment: '初回チェックリスト日'
      t.date :checklist_last_date, comment: '最終チェックリスト日'
      t.boolean :is_faq, comment: '質疑回答'
      t.boolean :is_lack_image, comment: '不足写真あり'
      t.boolean :is_complete_image, comment: '写真完了'
      t.date :submit_document_date, comment: '書類提出日'
      t.boolean :is_manager_asbestos_document1, comment: '監督-石綿事前調査報告書（公社提出用）'
      t.boolean :is_manager_asbestos_document2, comment: '監督-石綿事前調査報告書（電子申請用）'
      t.boolean :is_manager_asbestos_document3, comment: '監督-墨出し・ガス漏報告書'
      t.boolean :is_officework_asbestos_document1, comment: '事務-石綿事前調査報告書（公社提出用）'
      t.boolean :is_officework_asbestos_document2, comment: '事務-石綿事前調査報告書（電子申請用）'
      t.boolean :is_officework_asbestos_document3, comment: '事務-墨出し・ガス漏報告書'
      t.text :remark, comment: '備考'
      t.datetime :updated_items_at, comment: '明細最終更新日時'
      t.integer :updated_items_user_id, index: true, comment: '明細最終更新ユーザー'
      t.integer :edit_user_id, index: true, comment: '編集中ユーザー'

      t.timestamps
    end
    add_foreign_key :projects, :users, column: :updated_items_user_id
    add_foreign_key :projects, :users, column: :edit_user_id

    create_table :project_users do |t|
      t.references :project, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end

    create_table :project_reports do |t|
      t.references :project, foreign_key: true, null: true
      t.string :worksheet_project, comment: 'シート名-案件一覧'
      t.string :worksheet_room_layout, comment: 'シート名-部屋タイプ'
      t.string :worksheet_base, comment: 'シート名-基本情報'
      t.string :worksheet_items, comment: 'シート名-明細'
      t.string :worksheet_images, comment: 'シート名-画像'
      t.string :worksheet_checklist, comment: 'シート名-チェックリスト'
      t.string :worksheet_users, comment: 'シート名-従業員'
      t.string :worksheet_jkkusers, comment: 'シート名-公社担当者'
      t.string :spreadsheet_id1, comment: 'スプレッドシートID1'
      t.string :spreadsheet_id2, comment: 'スプレッドシートID2'
      t.string :spreadsheet_id3, comment: 'スプレッドシートID3'
      t.string :spreadsheet_id4, comment: 'スプレッドシートID4'
      t.string :spreadsheet_id5, comment: 'スプレッドシートID5'
      t.string :spreadsheet_id6, comment: 'スプレッドシートID6'
      t.string :spreadsheet_id7, comment: 'スプレッドシートID7'
      t.datetime :spreadsheet_datetime1, comment: 'スプレッドシート出力日時-査定表'
      t.datetime :spreadsheet_datetime2, comment: 'スプレッドシート出力日時-材料発注'
      t.datetime :spreadsheet_datetime3, comment: 'スプレッドシート出力日時-月次発注'
      t.datetime :spreadsheet_datetime4, comment: 'スプレッドシート出力日時-チェック表'
      t.datetime :spreadsheet_datetime5, comment: 'スプレッドシート出力日時-工事写真報告書1'
      t.datetime :spreadsheet_datetime6, comment: 'スプレッドシート出力日時-工事写真報告書2'
      t.datetime :spreadsheet_datetime7, comment: 'スプレッドシート出力日時-工事写真報告書2'
      t.boolean :spreadsheet_check_report1, comment: 'スプレッドシート-出力可否-工事写真報告書1'
      t.boolean :spreadsheet_check_report2, comment: 'スプレッドシート-出力可否-工事写真報告書2'

      t.timestamps
    end
  end
end
