class CreateProjectImages < ActiveRecord::Migration[6.1]
  def change
    create_table :project_images do |t|
      t.references :project, null: false, foreign_key: true
      t.references :room_type, foreign_key: true
      t.integer :image_type, null: false
      t.string :image_before
      t.string :image_after
      t.string :image
      t.text :comment
      t.integer :position, null: false

      t.timestamps
    end
  end
end
