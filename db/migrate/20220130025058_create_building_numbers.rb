class CreateBuildingNumbers < ActiveRecord::Migration[6.1]
  def change
    create_table :building_numbers do |t|
      t.references :building, null: false, foreign_key: true
      t.string :name, null: false
      t.integer :position

      t.timestamps
    end
  end
end
