class CreateJkkUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :jkk_users do |t|
      t.string :last_name, null: false
      t.string :first_name, null: false
      t.string :email
      t.boolean :active, null: :fasle, default: true

      t.timestamps
    end
  end
end
