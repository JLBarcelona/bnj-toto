class CreateItems < ActiveRecord::Migration[6.1]
  def change
    create_table :company_types do |t|
      t.string :name, null: false
      t.integer :position

      t.timestamps
    end

    create_table :companies do |t|
      t.string :name, null: false
      t.boolean :is_order_each, null: false, default: false, comment: '個別発注フラグ'
      t.boolean :is_order_monthly, null: false, default: false, comment: '月次発注フラグ'
      t.string :zipcode
      t.string :address
      t.string :tel
      t.string :fax
      t.boolean :is_material, null: false, default: false, comment: '材料会社フラグ'

      t.timestamps
    end

    create_table :company_company_types do |t|
      t.references :company_type, null: false, foreign_key: true
      t.references :company, null: false, foreign_key: true

      t.timestamps
    end

    create_table :units do |t|
      t.string :name, null: :fasle

      t.timestamps
    end

    create_table :item_codes do |t|
      t.string :code, null: :fasle
      t.string :name, null: :fasle

      t.timestamps
    end

    create_table :items do |t|
      t.references :item_code, null: false, foreign_key: true
      t.references :year, null: false, foreign_key: true
      t.references :unit, foreign_key: true
      t.references :company, foreign_key: true
      t.string :name, null: :fasle
      t.string :short_name
      t.integer :price, comment: '単価'
      t.integer :order_price, comment: '発注単価'
      t.integer :company_type_ids, array: true
      t.boolean :order, default: true, comment: '発注フラグ'

      t.timestamps
    end

    create_table :project_items do |t|
      t.references :project, null: false, foreign_key: true
      t.references :item, null: false, foreign_key: true
      t.references :company, foreign_key: true
      t.integer :sequence_number
      t.string :name
      t.string :code
      t.decimal :count, comment: '数量'
      t.integer :unit_price, comment: '単価'
      t.integer :amount, comment: '金額'
      t.string :room_type_ids, array: true
      t.datetime :updated_items_at, comment: '明細最終更新日時'
      t.integer :updated_items_user_id, index: true, comment: '明細最終更新ユーザー'

      t.timestamps
    end
  end
end
