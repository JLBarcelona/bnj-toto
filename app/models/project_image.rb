# == Schema Information
#
# Table name: project_images
#
#  id           :bigint           not null, primary key
#  comment      :text
#  image        :string
#  image_after  :string
#  image_before :string
#  image_type   :integer          not null
#  position     :integer          not null
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  project_id   :bigint           not null
#  room_type_id :bigint
#
# Indexes
#
#  index_project_images_on_project_id    (project_id)
#  index_project_images_on_room_type_id  (room_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (room_type_id => room_types.id)
#
class ProjectImage < ApplicationRecord
  belongs_to :project
  belongs_to :room_type, optional: true
  has_many :image_comments, dependent: :destroy
  has_many :project_image_comments, dependent: :destroy
  has_many :comments, through: :project_image_comments

  enum image_type: { 
    wall1: 0,
    wall2: 1,
    wall3: 2,
    wall4: 3,
    roof: 4,
    floor: 5,
    other: 9,
  }, _prefix: true

  acts_as_list scope: [:project_id, :room_type_id, :image_type]

  mount_uploader :image_before, ImageUploader
  mount_uploader :image_after, ImageUploader
  mount_uploader :image, ImageUploader

  # before_save :create_project_image_report

  class << self
    def search(params)
      project_images = self
      project_images = project_images.where(image_type: params[:image_type]) if params[:image_type].present?
      project_images
    end
  end

  # def create_project_image_report
  #   project.create_image_report
  # end
end
