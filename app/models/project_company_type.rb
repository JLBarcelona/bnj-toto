# == Schema Information
#
# Table name: project_company_types
#
#  id              :bigint           not null, primary key
#  order_date   :string
#  is_invoice      :boolean          default(FALSE), not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  company_id      :bigint           not null
#  company_type_id :bigint           not null
#  project_id      :bigint           not null
#
# Indexes
#
#  index_project_company_types_on_company_id       (company_id)
#  index_project_company_types_on_company_type_id  (company_type_id)
#  index_project_company_types_on_project_id       (project_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (company_type_id => company_types.id)
#  fk_rails_...  (project_id => projects.id)
#
class ProjectCompanyType < ApplicationRecord
  belongs_to :project
  belongs_to :company_type
  belongs_to :company
end
