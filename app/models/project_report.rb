# == Schema Information
#
# Table name: project_reports
#
#  id                                                                   :bigint           not null, primary key
#  spreadsheet_check_report1(スプレッドシート-出力可否-工事写真報告書1) :boolean
#  spreadsheet_check_report2(スプレッドシート-出力可否-工事写真報告書2) :boolean
#  spreadsheet_datetime1(スプレッドシート出力日時-査定表)               :datetime
#  spreadsheet_datetime2(スプレッドシート出力日時-材料発注)             :datetime
#  spreadsheet_datetime3(スプレッドシート出力日時-月次発注)             :datetime
#  spreadsheet_datetime4(スプレッドシート出力日時-チェック表)           :datetime
#  spreadsheet_datetime5(スプレッドシート出力日時-工事写真報告書1)      :datetime
#  spreadsheet_datetime6(スプレッドシート出力日時-工事写真報告書2)      :datetime
#  spreadsheet_id1(スプレッドシートID1)                                 :string
#  spreadsheet_id2(スプレッドシートID2)                                 :string
#  spreadsheet_id3(スプレッドシートID3)                                 :string
#  spreadsheet_id4(スプレッドシートID4)                                 :string
#  spreadsheet_id5(スプレッドシートID5)                                 :string
#  spreadsheet_id6(スプレッドシートID6)                                 :string
#  worksheet_base(シート名-基本情報)                                    :string
#  worksheet_checklist(シート名-チェックリスト)                         :string
#  worksheet_images(シート名-画像)                                      :string
#  worksheet_items(シート名-明細)                                       :string
#  worksheet_jkkusers(シート名-公社担当者)                              :string
#  worksheet_project(シート名-案件一覧)                                 :string
#  worksheet_room_layout(シート名-部屋タイプ)                           :string
#  worksheet_users(シート名-従業員)                                     :string
#  created_at                                                           :datetime         not null
#  updated_at                                                           :datetime         not null
#  project_id                                                           :bigint
#
# Indexes
#
#  index_project_reports_on_project_id  (project_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#
class ProjectReport < ApplicationRecord
  belongs_to :project

  before_create :set_worksheet

  def set_worksheet
    self.worksheet_project = '案件一覧'
    self.worksheet_room_layout = '部屋タイプ'
    self.worksheet_base = '基本情報'
    self.worksheet_items = '明細'
    self.worksheet_images = '画像'
    self.worksheet_checklist = 'チェックリスト'
    self.worksheet_users = '従業員'
    self.worksheet_jkkusers = '公社担当者'
  end

  def update_project_image_date
    if self.spreadsheet_check_report1
      project.update!(before_construction_image_date: Date.today)
    else
      project.update!(before_construction_image_date: nil)
    end

    if self.spreadsheet_check_report2
      project.update!(after_construction_image_date: Date.today)
    else
      project.update!(after_construction_image_date: nil)
    end
  end

  def create_spreadsheet(report_type)
    credential = 'gmail-345201-3b0fa8b41a99.json'

    service = Google::Apis::SheetsV4::SheetsService.new.tap do |s|
      s.authorization = Google::Auth::ServiceAccountCredentials.make_creds(
        json_key_io: File.open(credential),
        scope: %w(
          https://www.googleapis.com/auth/drive
          https://www.googleapis.com/auth/drive.file
          https://www.googleapis.com/auth/spreadsheets
        )
      )
    end

    service.authorization.fetch_access_token!
    spreadsheet_id = target_spreadsheet_id(report_type)

    begin
      create_spreadsheet_project(service, spreadsheet_id)
      create_spreadsheet_room_layout(service, spreadsheet_id)
      create_spreadsheet_base(service, spreadsheet_id)
      create_spreadsheet_items(service, spreadsheet_id)
      create_spreadsheet_images(service, spreadsheet_id)
      create_spreadsheet_checklist(service, spreadsheet_id)
      create_spreadsheet_users(service, spreadsheet_id)
      create_spreadsheet_jkkusers(service, spreadsheet_id)
      update_spreadsheet_datetime(report_type)
    rescue => e
      Rails.logger.error("ERROR!! create report")
      Rails.logger.error("project_report: #{self.id}, report_type: #{report_type}")
      Rails.logger.error("error: #{e.message}")
    end
  end

  def create_spreadsheet_room_layout(service, spreadsheet_id)
    return unless worksheet_room_layout.present?

    values = []
    values << ['部屋レイアウトタイプ', '部屋タイプ別クリーニング金額_A', '部屋タイプ別クリーニング金額_B']

    RoomLayout.all.order(:id).each do |room_layout|
      row = []
      row << room_layout.name
      row << room_layout.price1
      row << room_layout.price2

      values << row
    end

    p values

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_room_layout}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create report #create_spreadsheet_room_layout")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_project(service, spreadsheet_id)
    return unless self.worksheet_project.present?

    values = []
    values << ['指示番号', '名称', '住所', 'MC']

    Project.all.order(id: :desc).each do |project|
      row = []
      row << "#{project.order_number_type}-#{project.order_number}"
      row << project.name
      row << project.building.address
      row << project.building&.mc_type&.name

      values << row
    end
    
    p values

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_project}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create report #create_spreadsheet_project")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_base(service, spreadsheet_id)
    return unless worksheet_base.present?

    values = []
    values << ['MC区分', project.building.mc_type&.name]
    values << ['建物区分', project.building.building_type.name]
    values << ['建物', project.building.name]
    values << ['住所', project.building.address]
    values << ['号棟', project.building_number.name]
    values << ['号室', project.room.name]
    values << ['工事金額', project.total_price]
    values << ['指示番号', "#{project.order_number_type}-#{project.order_number}"]
    values << ['閉鎖区分', project.close_type_values]
    values << ['査定日', project.assessment_date]
    values << ['公社担当者', project.jkk_user&.full_name]
    values << ['当社担当者（査定）', project.assesment_user&.full_name]
    values << ['工期開始日', project.construction_start_date]
    values << ['工期終了日', project.construction_end_date]
    values << ['部屋タイプ', project.room.room_model_type&.name]
    values << ['レイアウトタイプ', project.room.room_layout&.name]
    values << ['退去日', project.leave_date]
    values << ['物件ランク', project.building.rank_i18n]

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_base}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create report #create_spreadsheet_base")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_items(service, spreadsheet_id)
    return unless worksheet_items.present?

    values = []

    values << ['コード', '項目名', '単価', '発注単価', '数量', '単位', '金額', '業者区分', '業者', '部屋種別', '部屋レイアウトタイプ', '退去日']

    project.project_items.each do |project_item|
      item_values = []
      item_values << project_item.item.item_code.code
      item_values << project_item.item.name
      item_values << project_item.unit_price
      item_values << project_item.item.order_price
      item_values << project_item.count
      item_values << project_item.item.unit.name
      item_values << project_item.amount
      item_values << project_item.company&.company_types&.map(&:name)&.join(',').presence || ''
      item_values << "#{project_item.company&.name}"
      item_values << project_item.room_types&.map(&:name).join(',')
      item_values << project.room.room_layout&.name
      item_values << project&.leave_date

      values << item_values
    end

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_items}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create_spreadsheet_items")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_images(service, spreadsheet_id)
    return unless worksheet_images.present?

    values = []
    values << ['左部屋種別',	'右部屋種別',	'左画像',	'右画像', '左コメント', '右コメント', '左カテゴリ', '右カテゴリ']

    # 塗装以前
    project.room.room_types.each do |room_type|
      next unless project.project_images.where(room_type: room_type).exists?

      project.project_images.where(room_type: room_type).each do |project_image|
        comment_text = project_image.image_comments.map{ |image_comment| image_comment.text }.join(',')

        values << [
          room_type.name,
          room_type.name,
          project_image.image_before_url,
          project_image.image_after_url,
          '',
          comment_text,
          '',
          ''
        ]
      end
    end

    # 塗装以降
    index_count = 0
    project.project_images.image_type_other.each do |project_image|
      index_count = index_count + 1
      comment_text = project_image.project_image_comments.map{ |project_image_comment| project_image_comment.comment.comment }.join(',')
      comment_type_names = project_image.project_image_comments.map{ |project_image_comment| project_image_comment.comment.comment_type.name }.join(',')

      url = project_image.image_url

      if [1,2,3].include?(index_count)
        values << [
          '塗装以降',
          '塗装以降',
          url,
          '',
          comment_text,
          '',
          comment_type_names,
          '',
        ]
      else
        row = nil
        if index_count == 4
          row = values[values.length - 3]
        elsif index_count == 5
          row = values[values.length - 2]
        elsif index_count == 6
          row = values[values.length - 1]
        end

        if row
          row[3] = url
          row[5] = comment_text
          row[7] = comment_type_names
        end
      end

      if index_count == 6
        index_count = 0
      end
    end

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_images}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create_spreadsheet_images")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_checklist(service, spreadsheet_id)
    return unless self.worksheet_checklist.present?

    values = []
    number = 1
    values << [number, '墨出し用穴開口部　及び　ガス漏れ警報器　報告（空家補修時）']
    number = number + 1
    values << [number, '請求書　（本社用　・　業者用　・　原課用）']

    if project.close_types&.include?('close_type1')
      number = number + 1
      values << [number, 'あき家補修工事に伴う石綿事前調査報告書']
    end

    item_codes = project.project_items.map(&:code)
    ['54000050', '54000100', '54000150', '54000200'].each do |code|
      if item_codes.include?(code)
        number = number + 1
        values << [number, '電気設備点検報告書（２枚）']
        values << [nil, '□　 エクセルに保存中（未入力項有り)']
        values << [nil, ' ・自動火災報知設備(自火報)']
        values << [nil, ' ・住宅用火災警報器 【 　　　　 年度】    　台所+居室数（ 　　）']
      end

      if item_codes.include?(code) && item_codes.include?('52000650')
        values << [number, '【52000650】']
        values << [nil, '・絶縁抵抗測定の改修内容欄に　回路（変更した回路番号を記入）　１００Vに変更と記載する']
      end

      if item_codes.include?(code)
        values << [nil, '・ﾚﾝｼﾞﾌｰﾄﾞﾌｧﾝ　・換気扇　・備付ｴｱｺﾝ　・熱交換換気扇等']
        values << [nil, '・浴室乾燥機　・ｲﾝﾀｰﾎﾝ（ｶﾒﾗ付・無）　・台所照明器具']
      end
    end

    if item_codes.include?('22020400')
      number = number + 1
      values << [number, '家電リサイクル券　・依頼中']
    end

    if item_codes.include?('47040050')
      number = number + 1
      values << [number, '給湯器作動点検報告書　・依頼中　・未スキャン']
    end

    ['47000100', '47000150', '47000300', '47000450'].each do |code|
      if item_codes.include?(code)
        number = number + 1
        values << [number, '浴槽･風呂釜点検整備完了報告書　・依頼中　・未スキャン']
      end
    end

    ['25014000', '25014050', '25014100', '25014150', '25014200', '25014250', '25015050'].each do |code|
      if item_codes.include?(code)
        number = number + 1
        values << [number, 'ノンタッチキー認証変更完了届　・依頼中　・未スキャン']
      end
    end

    ['22500100', '22500150', '22500200'].each do |code|
      if item_codes.include?(code)
        number = number + 1
        values << [number, '清掃完了報告書']
      end
    end

    # 工事区分が私負担工事以外出力（大井町MC・新宿MC）
    if project.close_types&.include?('close_type2')
      number = number + 1
      values << [number, 'あき家補修工事完了チェックシート　・依頼中　・未スキャン']
    end

    # 新宿MCの総務局、水道局、下水道局、交通局、教育長、消防庁の場合出力
    if project.building.mc_type&.name == '新宿MC'
      number = number + 1
      values << [number, '連絡票']
    end

    # 新宿MCの総務局、水道局、下水道局、交通局、教育長、消防庁の場合出力
    if project.building.mc_type&.name == '新宿MC'
      number = number + 1
      values << [number, '作業完了確認書']
    end

    # 工事区分が閉鎖住宅・私負担工事以外出力（大井町MC・新宿MC）
    if !project.close_types&.include?('close_type1') && !project.close_types&.include?('close_type2')
      number = number + 1
      values << [number, '間取図']
    end

    # 必須出力（大井町MC・新宿MC）
    number = number + 1
    values << [number, 'あき家補修工事記録写真']

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_checklist}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create_spreadsheet_checklist")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_users(service, spreadsheet_id)
    return unless self.worksheet_users.present?

    values = []
    User.where(active: true).each do |user|
      values << ["#{user.last_name} #{user.first_name}"]
    end

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_users}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create_spreadsheet_users")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def create_spreadsheet_jkkusers(service, spreadsheet_id)
    return unless self.worksheet_jkkusers.present?

    values = []
    JkkUser.where(active: true).each do |jkk_user|
      values << ["#{jkk_user.last_name} #{jkk_user.first_name}"]
    end

    service.update_spreadsheet_value(
      spreadsheet_id,
      "#{self.worksheet_jkkusers}!A1",
      Google::Apis::SheetsV4::ValueRange.new(values: values),
      value_input_option: "USER_ENTERED",
    )
  rescue => e
    Rails.logger.error("ERROR!! create_spreadsheet_jkkusers")
    Rails.logger.error("project_report: #{self.id}")
    Rails.logger.error("error: #{e.message}")
  end

  def target_spreadsheet_id(report_type)
    if report_type == 'assesment'
      self.spreadsheet_id1
    elsif report_type == 'orderMaterial'
      self.spreadsheet_id2
    elsif report_type == 'cleaning'
      self.spreadsheet_id3
    elsif report_type == 'checklist'
      self.spreadsheet_id4
    elsif report_type == 'order'
      self.spreadsheet_id5
    elsif report_type == 'report1'
      self.spreadsheet_id6
    elsif report_type == 'report2'
      self.spreadsheet_id7
    end
  end

  def update_spreadsheet_datetime(report_type)
    if report_type == 'assesment'
      update!(spreadsheet_datetime1: ::Time.now)
    elsif report_type == 'orderMaterial'
      update!(spreadsheet_datetime2: ::Time.now)
    elsif report_type == 'cleaning'
      update!(spreadsheet_datetime3: ::Time.now)
    elsif report_type == 'checklist'
      update!(spreadsheet_datetime4: ::Time.now)
    elsif report_type == 'order'
      update!(spreadsheet_datetime5: ::Time.now)
    elsif report_type == 'report1'
      update!(spreadsheet_datetime6: ::Time.now)
      project.update!(report_date: ::Time.now)
    elsif report_type == 'report2'
      update!(spreadsheet_datetime7: ::Time.now)
    end
  end
end
