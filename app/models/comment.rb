# == Schema Information
#
# Table name: comments
#
#  id              :bigint           not null, primary key
#  comment         :string           not null
#  position        :integer
#  remark          :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  comment_type_id :bigint           not null
#
# Indexes
#
#  index_comments_on_comment_type_id  (comment_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (comment_type_id => comment_types.id)
#
class Comment < ApplicationRecord
  acts_as_list scope: [:comment_type_id], top_of_list: 0

  belongs_to :comment_type
  has_many :comment_item_codes, dependent: :destroy
  has_many :item_codes, -> { order(:position) }, through: :comment_item_codes
  has_many :project_image_comments, dependent: :destroy
  has_many :project_images, through: :project_image_comments

  class << self
    def search(comments, params)
      comments = comments.where(comment_type_id: params[:comment_type_id]) if params[:comment_type_id].present?
      comments
    end
  end
end
