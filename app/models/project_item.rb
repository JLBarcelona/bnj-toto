# == Schema Information
#
# Table name: project_items
#
#  id                                          :bigint           not null, primary key
#  amount(金額)                                :integer
#  code                                        :string
#  count(数量)                                 :decimal(, )
#  name                                        :string
#  room_type_ids                               :string           is an Array
#  sequence_number                             :integer
#  unit_price(単価)                            :integer
#  updated_items_at(明細最終更新日時)          :datetime
#  created_at                                  :datetime         not null
#  updated_at                                  :datetime         not null
#  company_id                                  :bigint
#  item_id                                     :bigint           not null
#  project_id                                  :bigint           not null
#  updated_items_user_id(明細最終更新ユーザー) :integer
#
# Indexes
#
#  index_project_items_on_company_id             (company_id)
#  index_project_items_on_item_id                (item_id)
#  index_project_items_on_project_id             (project_id)
#  index_project_items_on_updated_items_user_id  (updated_items_user_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (item_id => items.id)
#  fk_rails_...  (project_id => projects.id)
#
class ProjectItem < ApplicationRecord
  belongs_to :project
  belongs_to :item
  belongs_to :company, optional: true
  belongs_to :updated_items_user, class_name: 'User', foreign_key: :updated_items_user_id, optional: true

  before_create :set_sequence_number

  def update_item(user)
    update_user(user)
    calculate
    project.update_companies
    project.calculate_price
    project.create_order
    project.update_items(user)
  end

  def update_user(user)
    update!(updated_items_at: Time.now, updated_items_user: user)
  end

  def set_sequence_number
    self.sequence_number = project.project_items.count + 1
  end

  def set_company
    if item.company
      self.company = item.company
      save!
    end
  end

  def change_year(year)
    next_item = item.item_code.items.find_by(year: year)
    return unless next_item

    update!(item: next_item, unit_price: next_item.price)
  end

  def selectable_companies
    Company.joins(:company_types).where(company_types: {id: item.company_types.ids}).distinct
  end

  def calculate
    self.amount = self.unit_price.present? && self.count.present? ? (self.unit_price * self.count || 0) : 0
    save!
  end

  def room_types
    RoomType.where(id: room_type_ids)
  end
end
