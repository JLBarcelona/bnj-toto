# == Schema Information
#
# Table name: comment_item_codes
#
#  id           :bigint           not null, primary key
#  position     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  comment_id   :bigint           not null
#  item_code_id :bigint
#
# Indexes
#
#  index_comment_item_codes_on_comment_id    (comment_id)
#  index_comment_item_codes_on_item_code_id  (item_code_id)
#
# Foreign Keys
#
#  fk_rails_...  (comment_id => comments.id)
#  fk_rails_...  (item_code_id => item_codes.id)
#
class CommentItemCode < ApplicationRecord
  belongs_to :comment
  belongs_to :item_code

  acts_as_list scope: [:comment_id], top_of_list: 0
end
