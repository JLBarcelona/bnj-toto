# == Schema Information
#
# Table name: room_types
#
#  id         :bigint           not null, primary key
#  name       :string
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class RoomType < ApplicationRecord
  acts_as_list top_of_list: 0

  has_many :room_type_rooms, dependent: :destroy
  has_many :rooms, through: :room_type_rooms
end
