# == Schema Information
#
# Table name: mc_types
#
#  id         :bigint           not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class McType < ApplicationRecord
  has_many :projects
end
