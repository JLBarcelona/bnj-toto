# == Schema Information
#
# Table name: room_model_types
#
#  id         :bigint           not null, primary key
#  name       :string
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class RoomModelType < ApplicationRecord
  has_many :projects

  acts_as_list
end
