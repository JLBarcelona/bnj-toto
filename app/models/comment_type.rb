# == Schema Information
#
# Table name: comment_types
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class CommentType < ApplicationRecord
  has_many :comments, -> { order(:position) }, dependent: :destroy

  acts_as_list top_of_list: 0
end
