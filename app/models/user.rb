# == Schema Information
#
# Table name: users
#
#  id              :bigint           not null, primary key
#  active          :boolean          default(TRUE)
#  email           :string
#  first_name      :string
#  is_assesment    :boolean          default(TRUE)
#  last_name       :string
#  password_digest :string
#  role            :integer
#  tel             :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  login_id        :string
#
class User < ApplicationRecord
  has_secure_password

  has_many :project_users, dependent: :destroy
  has_many :projects, through: :project_users

  enum role: { admin: 0, general: 1 }, _prefix: true

  class << self
    def search(params)
      users = self
      users = users.where(active: params[:active]) if params[:active] != nil
      users = users.where(is_assesment: params[:is_assesment]) if params[:is_assesment] != nil
      users
    end
  end

  def full_name
    "#{self.last_name}#{self.first_name}"
  end
end
