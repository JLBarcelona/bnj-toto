# == Schema Information
#
# Table name: orders
#
#  id                                 :bigint           not null, primary key
#  csv                                :string
#  export_csv_date                    :date
#  is_invoice                         :boolean          default(FALSE), not null
#  order_date(発注日)                 :date
#  order_type(0:都度発注, 1:月次発注) :integer          default(0), not null
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  company_id                         :bigint           not null
#  project_id                         :bigint           not null
#
# Indexes
#
#  index_orders_on_company_id  (company_id)
#  index_orders_on_project_id  (project_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (project_id => projects.id)
#

require 'csv'
require "tempfile"

class Order < ApplicationRecord
  belongs_to :project
  belongs_to :company

  mount_uploader :csv, FileUploader

  enum order_type: { each: 0, monthly: 1 }, _prefix: true

  class << self
    def search(orders, params)
      orders = orders.where("order_date >= ?", params[:from]) if params[:from].present?
      orders = orders.where("order_date <= ?", params[:to]) if params[:to].present?
      orders = orders.where(company_id: params[:company_id]) if params[:company_id].present?
      orders
    end

    def create_spreadsheet(orders, spreadsheet_id)
      credential = 'gmail-345201-3b0fa8b41a99.json'

      service = Google::Apis::SheetsV4::SheetsService.new.tap do |s|
        s.authorization = Google::Auth::ServiceAccountCredentials.make_creds(
          json_key_io: File.open(credential),
          scope: %w(
            https://www.googleapis.com/auth/drive
            https://www.googleapis.com/auth/drive.file
            https://www.googleapis.com/auth/spreadsheets
          )
        )
      end

      service.authorization.fetch_access_token!
      # spreadsheet_id = target_spreadsheet_id(report_type)

      begin
        create_spreadsheet_orders(orders, service, spreadsheet_id)
      rescue => e
        Rails.logger.error("ERROR!! create report")
        Rails.logger.error("error: #{e.message}")
      end
    end

    def create_spreadsheet_orders(orders, service, spreadsheet_id)
      # 発注シート
      create_spreadsheet_order(orders, service, spreadsheet_id)
      # 案件シート
      create_spreadsheet_projects(orders, service, spreadsheet_id)
      # 基本情報シート
      create_spreadsheet_bases(orders, service, spreadsheet_id)
      # 明細シート
      create_spreadsheet_items(orders, service, spreadsheet_id)
    end

    def create_spreadsheet_order(orders, service, spreadsheet_id)
      worksheet = "発注"
      values = []
      values << ['発注月', '業者', '指示番号', '案件名', '案件住所', '金額', 'MC']

      orders.each do |order|
        row = []
        row << order.order_date&.strftime("%Y/%m")
        row << order.company.name
        row << "#{order.project.order_number_type}-#{order.project.order_number}"
        row << order.project.name
        row << order.project.address
        row << order.total_price
        row << order.project.building.mc_type&.name

        values << row
      end
      
      p values

      service.update_spreadsheet_value(
        spreadsheet_id,
        "#{worksheet}!A1",
        Google::Apis::SheetsV4::ValueRange.new(values: values),
        value_input_option: "USER_ENTERED",
      )
    rescue => e
      Rails.logger.error("ERROR!! create report #create_spreadsheet_order")
      Rails.logger.error("error: #{e.message}")
    end

    def create_spreadsheet_projects(orders, service, spreadsheet_id)
      worksheet = "案件"

      values = []
      values << ['指示番号', '名称', '住所', 'MC']

      orders.map(&:project).uniq.each do |project|
        row = []
        row << "#{project.order_number_type}-#{project.order_number}"
        row << project.name
        row << project.building.address
        row << project.building&.mc_type&.name

        values << row
      end
      
      p values

      service.update_spreadsheet_value(
        spreadsheet_id,
        "#{worksheet}!A1",
        Google::Apis::SheetsV4::ValueRange.new(values: values),
        value_input_option: "USER_ENTERED",
      )
    rescue => e
      Rails.logger.error("ERROR!! create report #create_spreadsheet_projects")
      Rails.logger.error("error: #{e.message}")
    end

    # 基本情報シート
    def create_spreadsheet_bases(orders, service, spreadsheet_id)
      worksheet = "基本情報"

      values = []
      values << ['MC区分', '建物区分', '建物', '住所', '号棟', '工事金額', '指示番号', '閉鎖区分', '査定日', '公社担当者', '当社担当者（査定）', '工期開始日', '工期終了日', '部屋タイプ', 'レイアウトタイプ', '退去日', '物件ランク']
      orders.map(&:project).uniq.each do |project|
        row = []
        row << project.building.mc_type&.name
        row << project.building.building_type.name
        row << project.building.name
        row << project.building.address
        row << project.building_number.name
        row << project.room.name
        row << project.total_price
        row << "#{project.order_number_type}-#{project.order_number}"
        row << project.close_type_values
        row << project.assessment_date
        row << project.jkk_user&.full_name
        row << project.assesment_user&.full_name
        row << project.construction_start_date
        row << project.construction_end_date
        row << project.room.room_model_type&.name
        row << project.room.room_layout&.name
        row << project.leave_date
        row << project.building.rank_i18n

        values << row
      end

      p values

      service.update_spreadsheet_value(
        spreadsheet_id,
        "#{worksheet}!A1",
        Google::Apis::SheetsV4::ValueRange.new(values: values),
        value_input_option: "USER_ENTERED",
      )
    rescue => e
      Rails.logger.error("ERROR!! create report #create_spreadsheet_bases")
      Rails.logger.error("error: #{e.message}")
    end

    # 明細シート
    def create_spreadsheet_items(orders, service, spreadsheet_id)
      worksheet = "明細"

      values = []

      values << ['コード', '項目名', '単価', '発注単価', '数量', '単位', '金額', '業者区分', '業者', '部屋種別', '部屋レイアウトタイプ', '退去日']

      orders.map(&:project).uniq.each do |project|
        project.project_items.each do |project_item|
          item_values = []
          item_values << project_item.item.item_code.code
          item_values << project_item.item.name
          item_values << project_item.unit_price
          item_values << project_item.item.order_price
          item_values << project_item.count
          item_values << project_item.item.unit.name
          item_values << project_item.amount
          item_values << project_item.company&.company_types&.map(&:name)&.join(',').presence || ''
          item_values << "#{project_item.company&.name}"
          item_values << project_item.room_types&.map(&:name).join(',')
          item_values << project.room.room_layout&.name
          item_values << project&.leave_date

          values << item_values
        end
      end

      service.update_spreadsheet_value(
        spreadsheet_id,
        "#{worksheet}!A1",
        Google::Apis::SheetsV4::ValueRange.new(values: values),
        value_input_option: "USER_ENTERED",
      )
    rescue => e
      Rails.logger.error("ERROR!! create_spreadsheet_items")
      Rails.logger.error("error: #{e.message}")
    end
  end

  def calculate_price
    self.total_price = project.project_items.where(company: company).sum(:amount)
    save
  end
end
