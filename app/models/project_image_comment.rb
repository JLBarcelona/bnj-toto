# == Schema Information
#
# Table name: project_image_comments
#
#  id               :bigint           not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  comment_id       :bigint
#  project_image_id :bigint
#
# Indexes
#
#  index_project_image_comments_on_comment_id        (comment_id)
#  index_project_image_comments_on_project_image_id  (project_image_id)
#
# Foreign Keys
#
#  fk_rails_...  (comment_id => comments.id)
#  fk_rails_...  (project_image_id => project_images.id)
#
class ProjectImageComment < ApplicationRecord
  belongs_to :project_image
  belongs_to :comment
end
