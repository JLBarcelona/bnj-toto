# == Schema Information
#
# Table name: years
#
#  id         :bigint           not null, primary key
#  year       :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Year < ApplicationRecord
  has_many :items, dependent: :destroy
end
