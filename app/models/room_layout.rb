# == Schema Information
#
# Table name: room_layouts
#
#  id         :bigint           not null, primary key
#  name       :string
#  position   :integer
#  price1     :integer
#  price2     :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class RoomLayout < ApplicationRecord
  has_many :projects

  acts_as_list top_of_list: 0
end
