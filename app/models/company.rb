# == Schema Information
#
# Table name: companies
#
#  id                               :bigint           not null, primary key
#  address                          :string
#  fax                              :string
#  is_material(材料会社フラグ)      :boolean          default(FALSE), not null
#  is_order_each(個別発注フラグ)    :boolean          default(FALSE), not null
#  is_order_monthly(月次発注フラグ) :boolean          default(FALSE), not null
#  name                             :string           not null
#  tel                              :string
#  zipcode                          :string
#  created_at                       :datetime         not null
#  updated_at                       :datetime         not null
#
class Company < ApplicationRecord
  has_many :company_company_types, dependent: :destroy
  has_many :company_types, through: :company_company_types
  has_many :project_companies, dependent: :destroy
  has_many :projects, through: :project_companies
  has_many :project_items
end
