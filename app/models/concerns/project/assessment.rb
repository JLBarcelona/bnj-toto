module Concerns::Project::Assessment
  extend ActiveSupport::Concern

  included do
    def title(workbook, sheet)
      s = workbook.styles
      label = s.add_style alignment: { horizontal: :center }

      sheet.add_row
      sheet.add_row ["空家住宅補修工事　査定表"], style: [label]
      sheet.merge_cells('A2:I2')
    end

    def project_name(workbook, sheet)
      s = workbook.styles
      label = s.add_style alignment: { horizontal: :center }

      sheet.add_row ["住宅名", "", self.name], style: [label, nil]
      sheet.merge_cells('A3:B3')
      sheet.merge_cells('C3:I3')
    end

    def project_address(workbook, sheet)
      s = workbook.styles
      label = s.add_style alignment: { horizontal: :center }

      sheet.add_row ["所在地", "", self.address], style: [label, nil]
      sheet.merge_cells('A4:B4')
      sheet.merge_cells('C4:I4')
    end

    def item_header(workbook, sheet)
      s = workbook.styles
      label = s.add_style alignment: { horizontal: :center }

      sheet.add_row ["項目・形状寸法", "", "", "", "", "コード", "", "数量", "単位", "単価", "合計"], 
        style: [label, nil, nil, nil, nil, label, nil, label, label, label, label]
      sheet.merge_cells('A5:E5')
      sheet.merge_cells('F5:G5')
    end

    def item_rows(workbook, sheet)
      s = workbook.styles
      label = s.add_style alignment: { horizontal: :center }

      start_row_number = 6

      project_items.each_with_index do |project_item, index|
        row_index = start_row_number + index

        sheet.add_row [project_item.name, "", "", "", "", project_item.code, "", project_item.count, "", project_item.unit_price, project_item.amount],
          style: [nil]
        sheet.merge_cells("A#{row_index}:E#{row_index}")
        sheet.merge_cells("F#{row_index}:G#{row_index}")
      end
    end
  end
end
