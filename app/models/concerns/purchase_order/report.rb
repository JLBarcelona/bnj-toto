module Concerns::PurchaseOrder::Report
  extend ActiveSupport::Concern

  included do
    def constractor_name(sheet)
      # sheet.add_cell(3, 0, contractor.name)
      # sheet.add_cell(3, 1, '御中')
    end

    def title(sheet)
      sheet.add_cell(4, 0, '工事件名')
      sheet.add_cell(4, 1, project.name)
    end

    def item_rows(sheet)
      sheet.add_cell(9, 0, 'No')
      sheet.add_cell(9, 1, '名称')
      sheet.add_cell(9, 2, '数量')
      sheet.add_cell(9, 3, '単位')
      sheet.add_cell(9, 4, '単価')
      sheet.add_cell(9, 5, '金額')
      sheet.add_cell(9, 6, '備考')
      target_project_items.each_with_index do |project_item, index|
        row = 10 + index
        sheet.add_cell(row, 0, index + 1)
        sheet.add_cell(row, 1, project_item.item.name)
        sheet.add_cell(row, 2, project_item.count)
        # TODO: 単位
        sheet.add_cell(row, 3, "")
        sheet.add_cell(row, 4, project_item.item.amount_akiya)
        sheet.add_cell(row, 5, project_item.amount)
      end
    end
  end
end
