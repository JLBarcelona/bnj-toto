# == Schema Information
#
# Table name: rooms
#
#  id                 :bigint           not null, primary key
#  name               :string           not null
#  position           :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  building_number_id :bigint           not null
#  room_layout_id     :bigint
#  room_model_type_id :bigint
#
# Indexes
#
#  index_rooms_on_building_number_id  (building_number_id)
#  index_rooms_on_room_layout_id      (room_layout_id)
#  index_rooms_on_room_model_type_id  (room_model_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (building_number_id => building_numbers.id)
#  fk_rails_...  (room_layout_id => room_layouts.id)
#  fk_rails_...  (room_model_type_id => room_model_types.id)
#
class Room < ApplicationRecord
  belongs_to :building_number
  belongs_to :room_model_type, optional: true
  belongs_to :room_layout, optional: true

  has_many :room_type_rooms, dependent: :destroy
  has_many :room_types, through: :room_type_rooms
  has_many :projects

  acts_as_list scope: :building_number
end
