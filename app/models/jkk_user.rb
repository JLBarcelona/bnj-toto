# == Schema Information
#
# Table name: jkk_users
#
#  id         :bigint           not null, primary key
#  active     :boolean          default(TRUE)
#  email      :string
#  first_name :string           not null
#  last_name  :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class JkkUser < ApplicationRecord
  class << self
    def search(params)
      jkk_users = self
      jkk_users = jkk_users.where(active: params[:active]) if params[:active] != nil
      jkk_users
    end
  end

  def full_name
    "#{self.last_name}#{self.first_name}"
  end
end
