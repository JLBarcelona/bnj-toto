# == Schema Information
#
# Table name: building_numbers
#
#  id          :bigint           not null, primary key
#  name        :string           not null
#  position    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  building_id :bigint           not null
#
# Indexes
#
#  index_building_numbers_on_building_id  (building_id)
#
# Foreign Keys
#
#  fk_rails_...  (building_id => buildings.id)
#
class BuildingNumber < ApplicationRecord
  belongs_to :building
  has_many :rooms, -> { order(name: :asc) }, dependent: :destroy

  acts_as_list scope: :building
end
