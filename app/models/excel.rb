module Excel
  class Image
    attr_accessor :df, :sheet, :rid, :object, :begin_cell, :end_cell

    BASE_XML =<<-HTML.squish!
      <xdr:twoCellAnchor editAs="oneCell">
        <xdr:from>
          <xdr:col>%{begin_col}</xdr:col>
          <xdr:colOff>8000</xdr:colOff>
          <xdr:row>%{begin_row}</xdr:row>
          <xdr:rowOff>17000</xdr:rowOff>
        </xdr:from>
        <xdr:to>
          <xdr:col>%{end_col}</xdr:col>
          <xdr:colOff>0</xdr:colOff>
          <xdr:row>%{end_row}</xdr:row>
          <xdr:rowOff>0</xdr:rowOff>
        </xdr:to>
        <xdr:pic>
          <xdr:nvPicPr>
            <xdr:cNvPr id="4" name="図 3"/>
            <xdr:cNvPicPr>
              <a:picLocks noChangeAspect="1"/>
            </xdr:cNvPicPr>
          </xdr:nvPicPr>
          <xdr:blipFill>
            <a:blip xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:embed="%{rid}">
              <a:extLst>
                <a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}">
                  <a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/>
                </a:ext>
              </a:extLst>
            </a:blip>
            <a:stretch>
              <a:fillRect/>
            </a:stretch>
          </xdr:blipFill>
          <xdr:spPr>
            <a:xfrm>
              <a:off x="4533900" y="1600200"/>
              <a:ext cx="888687" cy="660400"/>
            </a:xfrm>
            <a:prstGeom prst="rect">
              <a:avLst/>
            </a:prstGeom>
          </xdr:spPr>
        </xdr:pic>
        <xdr:clientData/>
      </xdr:twoCellAnchor>
    HTML


    def initialize(sheet, file_path, begin_cell, end_cell)
      self.df         = get_drawing(sheet)
      self.sheet      = sheet
      self.begin_cell = begin_cell
      self.end_cell   = end_cell
      self.object = RubyXL::BinaryImageFile.new(
          Pathname("/xl/media/#{SecureRandom.hex}_#{file_path.basename}"),
          File.read(file_path)
        )
    end


    def self.add_to!(sheet, file_path, begin_cell, end_cell)
      image = new(sheet, file_path, begin_cell, end_cell)

      image.store_media
      image.append_rel
      image.add_to_xml
    end


    # xl/media/XXXX.xxx に画像ファイルを置く
    def store_media
      self.sheet.collect_related_objects
      self.sheet.attach_relationship(:dummy, object)
    end


    # xl/drawings/_rels/drawingX.xml.rels に自身を定義
    def append_rel
      self.df.relationship_container ||= RubyXL::OOXMLRelationshipsFile.new
      self.df.collect_related_objects
      self.df.relationship_container.send(:add_relationship, object)
      self.rid = df.relationship_container.relationships.last.id
    end


    # xl/drawings/drawingX.xml に自身を定義
    def add_to_xml
      xml = Nokogiri::XML.parse(df.data)
      xml.root.add_child(to_xml)
      self.df.data = xml.to_s
    end


    def begin_rowcol
      @begin_rowcol ||= ref_to_idx(begin_cell)
    end

    def end_rowcol
      @end_rowcol ||= ref_to_idx(end_cell)
    end

    def ref_to_idx(cell_addr)
      RubyXL::Reference.ref2ind(cell_addr)
    end

    # RubyXL::DrawingFile
    def get_drawing(sheet)
      rid = sheet.drawing
      if rid
        sheet.relationship_container.related_files[rid.r_id]
      end
    end


    def to_xml
      xml = BASE_XML.dup
      xml.sub!(/%{begin_row}/, begin_rowcol[0].to_s)
      xml.sub!(/%{begin_col}/, begin_rowcol[1].to_s)
      xml.sub!(/%{end_row}/, end_rowcol[0].to_s)
      xml.sub!(/%{end_col}/, end_rowcol[1].to_s)
      xml.sub!(/%{rid}/, rid)

      xml
    end
  end
end