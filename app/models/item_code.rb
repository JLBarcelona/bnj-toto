# == Schema Information
#
# Table name: item_codes
#
#  id         :bigint           not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class ItemCode < ApplicationRecord
  has_many :items
end
