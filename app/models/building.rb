# == Schema Information
#
# Table name: buildings
#
#  id               :bigint           not null, primary key
#  address          :string
#  name             :string
#  rank             :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  building_type_id :bigint
#  mc_type_id       :bigint
#
# Indexes
#
#  index_buildings_on_building_type_id  (building_type_id)
#  index_buildings_on_mc_type_id        (mc_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (building_type_id => building_types.id)
#  fk_rails_...  (mc_type_id => mc_types.id)
#
class Building < ApplicationRecord
  has_many :building_numbers, -> { order(name: :asc) }, dependent: :destroy
  belongs_to :mc_type, optional: true
  belongs_to :building_type, optional: true

  enum rank: { a: 0, b: 1, c: 2, d: 3 }, _prefix: true

  class << self
    def search(buidings, params)
      buidings = buidings.where("name like ?", "%#{params[:keyword]}%") if params[:keyword].present?
      buidings
    end
  end
end
