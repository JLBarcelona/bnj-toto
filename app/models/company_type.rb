# == Schema Information
#
# Table name: company_types
#
#  id         :bigint           not null, primary key
#  name       :string           not null
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class CompanyType < ApplicationRecord
  acts_as_list top_of_list: 0

  has_many :company_company_types, dependent: :destroy
  has_many :companies, through: :company_company_types
end
