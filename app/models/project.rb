# == Schema Information
#
# Table name: projects
#
#  id                                                                      :bigint           not null, primary key
#  address                                                                 :string
#  after_construction_image_date(施工後写真確認日)                         :date
#  assessment                                                              :string
#  assessment_date(査定日)                                                 :date
#  available_date(募集可能日)                                              :date
#  before_construction_image_date(施工前写真確認日)                        :date
#  checklist_date(3.最終チェックリスト日時)                                :datetime
#  checklist_first_date(初回チェックリスト日)                              :date
#  checklist_last_date(最終チェックリスト日)                               :date
#  close_reason(閉鎖区分内容)                                              :string
#  close_types(閉鎖区分)                                                   :string           is an Array
#  complete_image_date(5.写真完了日時)                                     :datetime
#  construction_end_date(工事終了日)                                       :date
#  construction_start_date(工事開始日)                                     :date
#  electric_end_date1(電気停止日1)                                         :date
#  electric_end_date2(電気停止日2)                                         :date
#  electric_end_date3(電気停止日3)                                         :date
#  electric_start_date1(電気開始日1)                                       :date
#  electric_start_date2(電気開始日2)                                       :date
#  electric_start_date3(電気開始日3)                                       :date
#  image_report                                                            :string
#  image_status(写真ステータス)                                            :integer
#  invoice_date(6.請求書類提出日時)                                        :datetime
#  is_complete_image(写真完了)                                             :boolean
#  is_faq(質疑回答)                                                        :boolean
#  is_finished_vermiculite(蛭石完了)                                       :boolean
#  is_lack_image(不足写真あり)                                             :boolean
#  is_manager_asbestos_document1(監督-石綿事前調査報告書（公社提出用）)    :boolean
#  is_manager_asbestos_document2(監督-石綿事前調査報告書（電子申請用）)    :boolean
#  is_manager_asbestos_document3(監督-墨出し・ガス漏報告書)                :boolean
#  is_officework_asbestos_document1(事務-石綿事前調査報告書（公社提出用）) :boolean
#  is_officework_asbestos_document2(事務-石綿事前調査報告書（電子申請用）) :boolean
#  is_officework_asbestos_document3(事務-墨出し・ガス漏報告書)             :boolean
#  is_vermiculite(蛭石)                                                    :boolean
#  item_csv                                                                :string
#  item_upload_date(1.査定表読み込み日時)                                  :datetime
#  jkk_status1(①CL済)                                                     :boolean          default(FALSE)
#  jkk_status2(②指示書済)                                                 :boolean          default(FALSE)
#  jkk_status3(③指示書再発行待ち)                                         :boolean          default(FALSE)
#  jkk_status4(④指示書再発行済み)                                         :boolean          default(FALSE)
#  lack_image_date(4.不足写真日時)                                         :datetime
#  leave_date(退去日)                                                      :date
#  living_month(入居年数-月)                                               :integer
#  living_year(入居年数)                                                   :integer
#  name                                                                    :string
#  order_date1(指示書発行日1)                                              :date
#  order_date2(指示書発行日2)                                              :date
#  order_date3(指示書発行日3)                                              :date
#  order_memo1(指示書発行メモ1)                                            :string
#  order_memo2(指示書発行メモ2)                                            :string
#  order_memo3(指示書発行メモ3)                                            :string
#  order_number                                                            :string
#  order_number_type                                                       :string
#  process_created_date(工程表作成日)                                      :date
#  remark(備考)                                                            :text
#  report_date(2.施工前写真出力日時)                                       :datetime
#  submit_document_date(書類提出日)                                        :date
#  tax(税金)                                                               :integer
#  total_item_price(合計金額(小計))                                        :integer
#  total_item_price_round(合計金額(端数))                                  :integer
#  total_price(合計金額)                                                   :integer
#  total_price_input(合計金額(入力))                                       :integer
#  updated_items_at(明細最終更新日時)                                      :datetime
#  vermiculite_date(蛭石調査完了日)                                        :date
#  water_end_date1(水道停止日1)                                            :date
#  water_end_date2(水道停止日2)                                            :date
#  water_end_date3(水道停止日3)                                            :date
#  water_start_date1(水道開始日1)                                          :date
#  water_start_date2(水道開始日2)                                          :date
#  water_start_date3(水道開始日3)                                          :date
#  住所                                                                    :string
#  created_at                                                              :datetime         not null
#  updated_at                                                              :datetime         not null
#  assesment_user_id(担当者-査定)                                          :integer
#  building_id                                                             :bigint
#  building_number_id                                                      :bigint
#  construction_user_id(担当者-工事)                                       :integer
#  edit_user_id(編集中ユーザー)                                            :integer
#  jkk_user_id(JKK担当者)                                                  :integer
#  room_id                                                                 :bigint
#  updated_items_user_id(明細最終更新ユーザー)                             :integer
#  year_id                                                                 :bigint
#
# Indexes
#
#  index_projects_on_building_id            (building_id)
#  index_projects_on_building_number_id     (building_number_id)
#  index_projects_on_edit_user_id           (edit_user_id)
#  index_projects_on_room_id                (room_id)
#  index_projects_on_updated_items_user_id  (updated_items_user_id)
#  index_projects_on_year_id                (year_id)
#
# Foreign Keys
#
#  fk_rails_...  (building_id => buildings.id)
#  fk_rails_...  (building_number_id => building_numbers.id)
#  fk_rails_...  (edit_user_id => users.id)
#  fk_rails_...  (room_id => rooms.id)
#  fk_rails_...  (updated_items_user_id => users.id)
#  fk_rails_...  (year_id => years.id)
#
require 'csv'
require "tempfile"
require "open-uri"
require "google/apis/sheets_v4"

class Project < ApplicationRecord
  include Concerns::Project::Assessment

  has_many :project_items, dependent: :destroy
  has_many :purchase_orders, -> { order(id: :asc) }, dependent: :destroy
  has_many :project_images, -> { order(:position) }, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :project_users, dependent: :destroy
  has_many :users, through: :project_users
  has_many :project_companies, dependent: :destroy
  has_many :companies, through: :project_companies
  has_one :project_report, dependent: :destroy

  belongs_to :year
  belongs_to :building, optional: true
  belongs_to :building_number, optional: true
  belongs_to :room, optional: true
  belongs_to :jkk_user, optional: true
  belongs_to :assesment_user, class_name: 'User', foreign_key: 'assesment_user_id', optional: true
  belongs_to :construction_user, class_name: 'User', foreign_key: 'construction_user_id', optional: true
  belongs_to :updated_items_user, class_name: 'User', foreign_key: :updated_items_user_id, optional: true
  belongs_to :edit_user, class_name: 'User', foreign_key: :edit_user_id, optional: true

  enum image_status: { lack: 0, complete: 1 }, _prefix: true

  mount_uploader :item_csv, FileUploader
  mount_uploader :assessment, FileUploader
  mount_uploader :image_report, FileUploader

  before_create :set_name
  before_create :set_address
  before_create :build_project_report
  before_update :check_checklist_first_date
  before_update :check_order_date1
  before_update :set_status

  class << self
    def search(projects, params)
      projects = projects.where("name like ?", "%#{params[:keyword]}%") if params[:keyword].present?
      projects = projects.where(order_number_type: params[:order_number_type]) if params[:order_number_type].present?
      projects = projects.where(order_number: params[:order_number]) if params[:order_number].present?
      projects
    end
  end

  def update_edit_user(user)
    unless edit_user
      update!(edit_user: user)
    end
  end

  def clear_edit_user(user)
    if edit_user.id == user.id
      update!(edit_user: nil)
    end
  end

  def create_item(attributes, user)
    project_item = add_project_item(attributes)
    project_item.update_user(user)
    update_items(user)
    create_order
    calculate_price
  end

  def add_project_item(attributes)
    project_item = project_items.create!(attributes)
    project_item.set_company
    project_item.calculate
    project_item
  end

  def update_items(user)
    update!(updated_items_at: Time.now, updated_items_user: user)
  end

  # 注文(発注書)CSV作成
  def create_order
    # 注文新規作成
    companies.each do |company|
      # 都度発注
      if company.is_order_each && !orders.where(company: company, order_type: :each).exists?
        orders.create!(company: company, order_type: :each)
      end

      # 月次発注
      if company.is_order_monthly && !orders.where(company: company, order_type: :monthly).exists?
        orders.create!(company: company, order_type: :monthly)
      end
    end

    # 指定されなくなった業者のorder削除
    order_company_ids = orders.map(&:company_id)
    (order_company_ids - companies.map(&:id)).each do |order_company_id|
      orders.find_by(company_id: order_company_id).destroy!
    end
  end

  # def companies
  #   project_items.map(&:company).uniq.compact
  # end

  def import_item(user)
    file_path = ENV['S3_BUCKET_NAME'].present? ? item_csv.url : item_csv.path

    xlsx = Roo::Spreadsheet.open(file_path)
    sheet = xlsx.sheet(0)
    codes = sheet.column(2)
    counts = sheet.column(3)

    return unless codes || codes.length < 5

    codes = codes[5..codes.length]
    counts = counts[5..counts.length]

    # 既存明細削除
    project_items.destroy_all

    codes.each_with_index do |code, i|
      count = counts[i]

      item_code = ::ItemCode.find_by(code: code)
      next unless item_code

      item = ::Item.find_by(year: self.year, item_code: item_code)
      next unless item

      project_item = project_items.find_by(item: item)
      project_item = add_project_item(
        code: item.item_code.code, name: item.name, item: item, count: count, unit_price: item.price
      )
      project_item.update_user(user)

      # 業者区分
      project_item.update!(company: item.company) if item.company

      project_item.calculate
    end

    calculate_price
    export_assessment
    create_purchase_orders
    create_order
    create_report

    update!(item_upload_date: ::Time.now)
  end

  # 明細の業者を更新する
  def update_companies
    self.companies.replace(project_items.map(&:company).uniq.compact)
  end

  # 明細の業者を更新する
  def update_items_company(company_type_id, company_id)
    company_type = ::CompanyType.find(company_type_id)

    project_items.each do |project_item|
      company_types = project_item.item.company_types

      # 明細に紐付いている業者種別が1つの場合のみ対象
      if company_types.count == 1 && company_types.first == company_type
        project_item.update!(company_id: company_id)
      end
    end

    update_companies
    create_order
    calculate_order_price
  end

  # 査定表出力
  def export_assessment
    package = Axlsx::Package.new
    workbook = package.workbook
    workbook.add_worksheet(name: '査定表') do |sheet|
      title(workbook, sheet)
      project_name(workbook, sheet)
      project_address(workbook, sheet)
      item_header(workbook, sheet)
      item_rows(workbook, sheet)

      sheet.column_widths 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10
    end

    Dir.mktmpdir do |dir|
      file_path = Rails.root.join(dir, "査定表-#{self.name}.xlsx")
      package.serialize(file_path)
      self.assessment = File.open(file_path)
      save!
    end
  end

  # TODO:発注書出力
  def create_purchase_orders
    # contractors = []
    # project_items.each do |project_item|
    #   contractors << project_item.item.contractor
    # end

    # contractors.uniq.each do |contractor|
    #   purchase_order = purchase_orders.find_by(contractor: contractor)
    #   unless purchase_order
    #     purchase_order = purchase_orders.create!(contractor: contractor)
    #   end
    # end
  end

  def create_report
    purchase_orders.each(&:create_report)
  end

  # 画像レポート
  def create_image_report
    package = Axlsx::Package.new
    workbook = package.workbook

    # S3用tempfile
    @tempfiles = []

    room.room_types.each do |room_type|
      workbook.add_worksheet(name: room_type.name) do |sheet|
        sheet.add_row ["", "施工前", "", "", "", "", "施工後"]
        sheet.add_row ["壁1"]

        target_project_images = project_images.where(room_type: room_type)
        @row_count = 4
        draw_image_report(sheet, target_project_images, "wall1")

        sheet.add_row [""]
        sheet.add_row [""]
        sheet.add_row ["壁2"]

        draw_image_report(sheet, target_project_images, "wall2")

        sheet.add_row [""]
        sheet.add_row [""]
        sheet.add_row ["壁3"]

        draw_image_report(sheet, target_project_images, "wall3")

        sheet.add_row [""]
        sheet.add_row [""]
        sheet.add_row ["壁4"]

        draw_image_report(sheet, target_project_images, "wall4")

        sheet.add_row [""]
        sheet.add_row [""]
        sheet.add_row ["天井"]

        draw_image_report(sheet, target_project_images, "roof")

        sheet.column_widths 40, 10, 10, 10, 10, 10, 10, 10, 10
      end

    end

    # 塗装以降
    project_image_others = project_images.where(image_type: "other")
    if project_image_others.count > 0
      workbook.add_worksheet(name: "塗装以降") do |sheet|
        draw_image_report(sheet, project_images, "other")

        sheet.column_widths 40, 10, 10, 10, 10, 10, 10, 10, 10
      end
    end


    Dir.mktmpdir do |dir|
      file_path = Rails.root.join(dir, "test.xlsx")
      # workbook.write(file_path)
      package.serialize(file_path)
      self.image_report = File.open(file_path)
      save!
    end

    if @tempfiles.present?
      @tempfiles.each(&:close!)
    end
  end

  def draw_image_report(sheet, target_project_images, image_type)
    sheet.add_row [""]
    sheet.add_row [""]

    row_height = 12
    project_image_walls = target_project_images.where(image_type: image_type)
    project_image_walls.each do |wall|
      # コメントセット
      if wall.image_comments.present?
        wall.image_comments.each do |image_comment|
          sheet.add_row [image_comment.text]
        end

        (row_height - wall.image_comments.count).times { sheet.add_row [""] }
      else
        row_height.times { sheet.add_row [""] }
      end

      if image_type != "other"
        # image
        if wall&.image_before_url
          path = wall.image_before.url
          tempfile = Tempfile.new(wall.image_before.identifier)
          @tempfiles << tempfile
          tempfile.binmode

          if ENV['S3_BUCKET_NAME'].present?
            tempfile.write open(path).read
          else
            tempfile.write File.open("public#{path}").read
          end

          tempfile.close

          sheet.add_image(image_src: tempfile.path, start_at: "B#{@row_count}", end_at: "F#{@row_count + 8}")
          # end
        end

        if wall&.image_after_url
          path = wall.image_after.url
          tempfile = Tempfile.new(wall.image_after.identifier)
          @tempfiles << tempfile
          tempfile.binmode

          if ENV['S3_BUCKET_NAME'].present?
            tempfile.write open(path).read
          else
            tempfile.write File.open("public#{path}").read
          end
          tempfile.close

          sheet.add_image(image_src: tempfile.path, start_at: "G#{@row_count}", end_at: "K#{@row_count + 8}")
        end
      else
        path = wall.image.url
        tempfile = Tempfile.new(wall.image.identifier)
        @tempfiles << tempfile
        tempfile.binmode

        if ENV['S3_BUCKET_NAME'].present?
          tempfile.write open(path).read
        else
          tempfile.write File.open("public#{path}").read
        end
        tempfile.close

        sheet.add_image(image_src: tempfile.path, start_at: "B#{@row_count}", end_at: "F#{@row_count + 8}")
      end

      @row_count += 12
    end
  end

  def set_name
    self.name = "#{building.name}"
  end

  def set_address
    self.address = building.address
  end

  def calculate_price
    self.total_item_price = project_items.sum(:amount)
    self.total_item_price_round = self.total_item_price.floor(-2)
    self.tax = self.total_item_price_round * 0.1
    self.total_price = self.total_item_price_round + self.tax
    calculate_order_price
    save!
  end

  def calculate_order_price
    orders.each { |order| order.calculate_price }
  end

  def change_year(year_id)
    year = Year.find(year_id)
    update!(year: year)

    project_items.each do |project_item|
      project_item.change_year(year)
    end
  end

  def close_type_values
    close_types.map { |close_type| close_type_value(close_type) }.join(',')
  end

  def close_type_value(close_type)
    case close_type
    when 'close_type1'
      '閉鎖住宅'
    when 'close_type2'
      '私負担清算'
    when 'close_type3'
      '本工事'
    when 'close_type4'
      '工事保留'
    when 'close_type5'
      '工事中止'
    when 'close_type6'
      '工事再開'
    when 'close_type7'
      '被災者住宅'
    when 'close_type8'
      '検査対象物件'
    when 'close_type9'
      'その他'
    end
  end

  def check_checklist_first_date
    if self.checklist_first_date
      self.jkk_status1 = true
    end
  end

  def check_order_date1
    if self.order_date1
      self.jkk_status2 = true
    end
  end

  # 都度発注業者
  def orders_each
    orders.order_type_each
  end

  # 月次発注業者
  def orders_monthly
    orders.order_type_monthly
  end

  def set_status
    # 最終チェックリストが入った
    if will_save_change_to_attribute?(:checklist_last_date) && checklist_last_date.present?
      self.checklist_date = Time.now
    end

    # 不足写真　が入ったら➡④施工後不足写真待ち
    if will_save_change_to_attribute?(:image_status) && image_status == 'lack'
      self.lack_image_date = Time.now
    end

    if will_save_change_to_attribute?(:image_status) && image_status == 'complete'
      self.complete_image_date = Time.now
    end

    # 請求書類提出日が入ったら➡⑥請求書済
    if will_save_change_to_attribute?(:submit_document_date) && submit_document_date != nil
      self.status6 = true
      self.invoice_date = Time.now
    end
  end

  def item_upload_date_text
    self.item_upload_date&.strftime('%Y/%m/%d %H:%M')
  end

  def report_date_text
    self.report_date&.strftime('%Y/%m/%d %H:%M')
  end

  def checklist_date_text
    self.checklist_date&.strftime('%Y/%m/%d %H:%M')
  end

  def lack_image_date_text
    self.lack_image_date&.strftime('%Y/%m/%d %H:%M')
  end

  def complete_image_date_text
    self.complete_image_date&.strftime('%Y/%m/%d %H:%M')
  end

  def invoice_date_text
    self.invoice_date&.strftime('%Y/%m/%d %H:%M')
  end
end
