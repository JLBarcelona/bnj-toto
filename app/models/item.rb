# == Schema Information
#
# Table name: items
#
#  id                    :bigint           not null, primary key
#  company_type_ids      :integer          is an Array
#  name                  :string
#  order(発注フラグ)     :boolean          default(TRUE)
#  order_price(発注単価) :integer
#  price(単価)           :integer
#  short_name            :string
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  company_id            :bigint
#  item_code_id          :bigint           not null
#  unit_id               :bigint
#  year_id               :bigint           not null
#
# Indexes
#
#  index_items_on_company_id    (company_id)
#  index_items_on_item_code_id  (item_code_id)
#  index_items_on_unit_id       (unit_id)
#  index_items_on_year_id       (year_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (item_code_id => item_codes.id)
#  fk_rails_...  (unit_id => units.id)
#  fk_rails_...  (year_id => years.id)
#
class Item < ApplicationRecord
  belongs_to :item_code
  belongs_to :year
  belongs_to :unit
  belongs_to :company, optional: true

  class << self
    def search(params)
      items = self

      if params[:year].present?
        items = items.joins(:year, :item_code).where({year: {year: params[:year]}}) 
      else
        latest_year = Year.order(id: :desc).first
        items = items.joins(:year, :item_code).where({year: {year: latest_year.year}}) 
      end

      if params[:keyword].present?
        items = items.where("items.name like ?", "%#{params[:keyword]}%").or(items.joins(:item_code).where("item_codes.code like ?", "%#{params[:keyword]}%"))
      end
      items
    end
  end

  def company_types
    CompanyType.where(id: company_type_ids)
  end
end
