# == Schema Information
#
# Table name: project_companies
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  company_id :bigint
#  project_id :bigint
#
# Indexes
#
#  index_project_companies_on_company_id  (company_id)
#  index_project_companies_on_project_id  (project_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (project_id => projects.id)
#
class ProjectCompany < ApplicationRecord
  belongs_to :project
  belongs_to :company
end
