# == Schema Information
#
# Table name: image_comments
#
#  id               :bigint           not null, primary key
#  code             :string           not null
#  text             :text             not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  project_image_id :bigint           not null
#
# Indexes
#
#  index_image_comments_on_project_image_id  (project_image_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_image_id => project_images.id)
#
class ImageComment < ApplicationRecord
  belongs_to :project_image
end
