# == Schema Information
#
# Table name: purchase_orders
#
#  id         :bigint           not null, primary key
#  file       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  project_id :bigint           not null
#
# Indexes
#
#  index_purchase_orders_on_project_id  (project_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#
class PurchaseOrder < ApplicationRecord
  include Concerns::PurchaseOrder::Report

  belongs_to :project

  mount_uploader :file, FileUploader

  def target_project_items
    # project.project_items.where(contractor: contractor)
  end

  def create_report
    # workbook = RubyXL::Workbook.new
    # sheet = workbook.worksheets[0]
    # constractor_name(sheet)
    # title(sheet)
    # item_rows(sheet)

    # Dir.mktmpdir do |dir|
    #   file_path = Rails.root.join(dir, "発注書-#{self.contractor.name}.xlsx")
    #   workbook.write(file_path)
    #   self.file = File.open(file_path)
    #   save!
    # end

    # Axlsx::Package.new do |p|
    #   img = File.open(Rails.root.join('public', 'stamp.png')).path

    #   p.workbook.add_worksheet(:name => "Pie Chart") do |sheet|
    #     sheet.add_row ["Simple Pie Chart"]
    #     binding.pry

    #     sheet.add_image(:image_src => img, :noSelect => true, :noMove => true, :hyperlink=>"http://axlsx.blogspot.com") do |image|
    #       image.width = 7
    #       image.height = 6
    #       image.hyperlink.tooltip = "Labeled Link"
    #       image.start_at 2, 2
    #     end

    #     # %w(first second third).each { |label| sheet.add_row [label, rand(24)+1] }
    #     # sheet.add_chart(Axlsx::Pie3DChart, :start_at => [0,5], :end_at => [10, 20], :title => "example 3: Pie Chart") do |chart|
    #     #   chart.add_series :data => sheet["B2:B4"], :labels => sheet["A2:A4"],  :colors => ['FF0000', '00FF00', '0000FF']
    #     # end
    #   end
    #   # p.serialize('simple.xlsx')
    #   Dir.mktmpdir do |dir|
    #     file_path = Rails.root.join(dir, "発注書-#{self.contractor.name}.xlsx")
    #     p.serialize(file_path)
    #     # workbook.write(file_path)
    #     self.file = File.open(file_path)
    #     save!
    #   end
    # end

  end
end
