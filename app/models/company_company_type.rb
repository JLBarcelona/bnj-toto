# == Schema Information
#
# Table name: company_company_types
#
#  id              :bigint           not null, primary key
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  company_id      :bigint           not null
#  company_type_id :bigint           not null
#
# Indexes
#
#  index_company_company_types_on_company_id       (company_id)
#  index_company_company_types_on_company_type_id  (company_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (company_id => companies.id)
#  fk_rails_...  (company_type_id => company_types.id)
#
class CompanyCompanyType < ApplicationRecord
  belongs_to :company_type
  belongs_to :company
end
