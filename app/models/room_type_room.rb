# == Schema Information
#
# Table name: room_type_rooms
#
#  id           :bigint           not null, primary key
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  room_id      :bigint           not null
#  room_type_id :bigint           not null
#
# Indexes
#
#  index_room_type_rooms_on_room_id       (room_id)
#  index_room_type_rooms_on_room_type_id  (room_type_id)
#
# Foreign Keys
#
#  fk_rails_...  (room_id => rooms.id)
#  fk_rails_...  (room_type_id => room_types.id)
#
class RoomTypeRoom < ApplicationRecord
  belongs_to :room_type
  belongs_to :room
end
