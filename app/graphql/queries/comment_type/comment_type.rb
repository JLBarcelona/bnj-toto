module Queries::CommentType
  class CommentType < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::CommentType, null: true

    def resolve(id:)
      ::CommentType.find(id)
    end
  end
end
