module Queries::CommentType
  class CommentTypes < Queries::BaseQuery
    type [ObjectTypes::CommentType], null: true

    def resolve
      ::CommentType.all.order(:name)
    end
  end
end
