module Queries::BuildingType
  class BuildingTypes < Queries::BaseQuery
    type [ObjectTypes::BuildingType], null: true

    def resolve
      ::BuildingType.order(id: :desc)
    end
  end
end
