module Queries::RoomLayout
  class RoomLayouts < Queries::BaseQuery
    type [ObjectTypes::RoomLayout], null: true

    def resolve
      ::RoomLayout.order(:position)
    end
  end
end
