module Queries::RoomLayout
  class RoomLayout < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::RoomLayout, null: true

    def resolve(id:)
      ::RoomLayout.find(id)
    end
  end
end
