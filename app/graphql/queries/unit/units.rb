module Queries::Unit
  class Units < Queries::BaseQuery
    type [ObjectTypes::Unit], null: true

    def resolve
      ::Unit.all.order(id: :desc)
    end
  end
end
