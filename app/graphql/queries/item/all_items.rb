module Queries::Item
  class AllItems < Queries::BaseQuery
    argument :search, InputTypes::ItemSearch, required: false

    type [ObjectTypes::Item], null: true

    def resolve(search: {})
      # ::Item.joins(:year, :item_code).eager_load(:company).search(search).order("item_codes.code")
      ::Item.joins(:year).eager_load(:item_code).search(search).order("item_codes.code")
    end
  end
end
