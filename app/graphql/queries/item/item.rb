module Queries::Item
  class Item < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::Item, null: true

    def resolve(id:)
      ::Item.find(id)
    end
  end
end
