module Queries::Item
  class Items < Queries::BaseQuery
    argument :page, Int, required: false, default_value: 1
    argument :per_page, Int, required: false, default_value: 20
    argument :search, InputTypes::ItemSearch, required: false

    type ObjectTypes::Items, null: true

    def resolve(page:, per_page:, search: {})
      items = ::Item.joins(:year, :item_code).eager_load(:company).search(search).order("item_codes.code")
      items.page(page).per(per_page)
    end
  end
end
