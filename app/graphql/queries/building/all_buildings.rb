module Queries::Building
  class AllBuildings < Queries::BaseQuery
    type [ObjectTypes::Building], null: true

    def resolve
      ::Building.all.order(id: :desc)
    end
  end
end
