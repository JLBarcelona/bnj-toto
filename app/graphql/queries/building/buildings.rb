module Queries::Building
  class Buildings < Queries::BaseQuery
    argument :page, Int, required: false, default_value: 1
    argument :per_page, Int, required: false, default_value: 20
    argument :search, InputTypes::BuildingSearch, required: false

    type ObjectTypes::Buildings, null: true

    def resolve(page:, per_page:, search: {})
      ::Building.search(::Building, search).order(id: :desc).page(page).per(per_page)
    end
  end
end
