module Queries::Building
  class Building < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::Building, null: true

    def resolve(id:)
      ::Building.find(id)
    end
  end
end
