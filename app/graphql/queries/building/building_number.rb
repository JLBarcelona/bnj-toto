module Queries::Building
  class BuildingNumber < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::BuildingNumber, null: true

    def resolve(id:)
      ::BuildingNumber.find(id)
    end
  end
end
