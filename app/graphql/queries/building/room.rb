module Queries::Building
  class Room < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::Room, null: true

    def resolve(id:)
      ::Room.find(id)
    end
  end
end
