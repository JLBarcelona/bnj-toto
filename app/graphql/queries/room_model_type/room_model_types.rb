module Queries::RoomModelType
  class RoomModelTypes < Queries::BaseQuery
    type [ObjectTypes::RoomModelType], null: true

    def resolve
      ::RoomModelType.order(id: :desc)
    end
  end
end
