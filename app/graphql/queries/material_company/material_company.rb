module Queries::MaterialCompany
  class MaterialCompany < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::MaterialCompany, null: true

    def resolve(id:)
      ::MaterialCompany.find(id)
    end
  end
end
