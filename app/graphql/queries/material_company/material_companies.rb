module Queries::MaterialCompany
  class MaterialCompanies < Queries::BaseQuery
    type [ObjectTypes::MaterialCompany], null: true

    def resolve
      ::MaterialCompany.all.order(:id)
    end
  end
end