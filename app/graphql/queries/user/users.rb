module Queries::User
  class Users < Queries::BaseQuery
    argument :search, InputTypes::UserSearch, required: false

    type [ObjectTypes::User], null: true

    def resolve(search:)
      ::User.search(search).order(id: :desc)
    end
  end
end