module Queries::Project
  class ProjectItems < Queries::BaseQuery
    argument :id, ID, required: true
    argument :order_item, String, required: false
    argument :order, String, required: false

    type [ObjectTypes::ProjectItem], null: true

    def resolve(id:, order_item:, order:)
      project = ::Project.find(id)
      project.project_items.order("#{order_item} #{order}")
    end
  end
end
