module Queries::Project
  class Project < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::Project, null: true

    def resolve(id:)
      ::Project.find(id)
    end
  end
end
