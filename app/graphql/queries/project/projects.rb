module Queries::Project
  class Projects < Queries::BaseQuery
    argument :page, Int, required: false, default_value: 1
    argument :per_page, Int, required: false, default_value: 20
    argument :search, InputTypes::ProjectSearch, required: false

    type ObjectTypes::Projects, null: true

    def resolve(page:, per_page:, search: {})
      projects = ::Project.search(::Project, search)

      if search[:order_item]
        # order_typeはasc/desc
        projects = projects.order("#{search[:order_item]} #{search[:order_type]}")
      else
        projects = projects.order(id: :desc).page(page).per(per_page)
      end

      projects.page(page).per(per_page)
    end
  end
end
