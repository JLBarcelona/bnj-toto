module Queries::CompanyType
  class CompanyTypes < Queries::BaseQuery
    type [ObjectTypes::CompanyType], null: true

    def resolve
      ::CompanyType.order(position: :desc)
    end
  end
end
