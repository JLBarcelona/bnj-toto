module Queries::CompanyType
  class CompanyType < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::CompanyType, null: true

    def resolve(id:)
      ::CompanyType.find(id)
    end
  end
end
