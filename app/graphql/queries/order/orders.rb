module Queries::Order
  class Orders < Queries::BaseQuery
    argument :page, Int, required: false, default_value: 1
    argument :per_page, Int, required: false, default_value: 20
    argument :search, InputTypes::OrderSearch, required: false

    type ObjectTypes::Orders, null: true

    def resolve(page:, per_page:, search: {})
      orders = ::Order.search(::Order, search).order(id: :desc)

      orders.page(page).per(per_page)
    end
  end
end
