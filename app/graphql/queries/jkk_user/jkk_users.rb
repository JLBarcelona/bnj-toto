module Queries::JkkUser
  class JkkUsers < Queries::BaseQuery
    argument :search, InputTypes::JkkUserSearch, required: false

    type [ObjectTypes::JkkUser], null: true

    def resolve(search:)
      ::JkkUser.search(search).order(id: :desc)
    end
  end
end