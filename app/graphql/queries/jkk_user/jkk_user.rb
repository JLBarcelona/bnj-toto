module Queries::JkkUser
  class JkkUser < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::JkkUser, null: true

    def resolve(id:)
      ::JkkUser.find(id)
    end
  end
end
