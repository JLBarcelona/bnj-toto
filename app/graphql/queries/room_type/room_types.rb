module Queries::RoomType
  class RoomTypes < Queries::BaseQuery
    type [ObjectTypes::RoomType], null: true

    def resolve
      ::RoomType.order(:position)
    end
  end
end
