module Queries::RoomType
  class RoomType < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::RoomType, null: true

    def resolve(id:)
      ::RoomType.find(id)
    end
  end
end
