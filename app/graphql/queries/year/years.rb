module Queries::Year
  class Years < Queries::BaseQuery
    type [ObjectTypes::Year], null: true

    def resolve
      ::Year.all.order(id: :desc)
    end
  end
end
