module Queries::Company
  class Company < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::Company, null: true

    def resolve(id:)
      ::Company.find(id)
    end
  end
end
