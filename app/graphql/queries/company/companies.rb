module Queries::Company
  class Companies < Queries::BaseQuery
    type [ObjectTypes::Company], null: true

    def resolve
      ::Company.eager_load(:company_types).order(id: :desc)
    end
  end
end
