module Queries::Comment
  class Comments < Queries::BaseQuery
    argument :search, InputTypes::CommentSearch, required: false

    type [ObjectTypes::Comment], null: true

    def resolve(search:)
      ::Comment.search(::Comment, search).joins(:comment_type).order("comment_types.name")
    end
  end
end
