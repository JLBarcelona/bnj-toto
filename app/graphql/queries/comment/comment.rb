module Queries::Comment
  class Comment < Queries::BaseQuery
    argument :id, ID, required: true

    type ObjectTypes::Comment, null: true

    def resolve(id:)
      ::Comment.find(id)
    end
  end
end
