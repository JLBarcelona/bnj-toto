module Queries::McType
  class McTypes < Queries::BaseQuery
    type [ObjectTypes::McType], null: true

    def resolve
      ::McType.order(id: :desc)
    end
  end
end
