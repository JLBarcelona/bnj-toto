class Mutations::Building::UpdateBuilding < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Building, required: true

  field :building, ObjectTypes::Building, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    building = ::Building.find(id)

    if building.name != attributes[:name] && ::Building.where(name: attributes[:name]).first
      return { error: "この名称はすでに存在しています。" }
    end

    building.update!(attributes.to_h)

    { building: building.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
