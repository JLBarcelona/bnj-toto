class Mutations::Building::CreateBuilding < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::Building, required: true

  field :building, ObjectTypes::Building, null: true
  field :error, String, null: true

  def resolve(attributes:)
    if Building.where(name: attributes[:name]).first
      return { error: "この名称はすでに存在しています。" }
    end

    building = Building.create!(attributes.to_h)

    { building: building }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
