class Mutations::Company::RemoveCompany < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :result, String, null: true
  field :error, String, null: true

  def resolve(id:)
    company = ::Company.find(id)

    if company.projects.present? || company.project_items.present?
      return { error: "この業者は削除できません。" }
    end

    company.destroy!

    { result: 'success' }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
