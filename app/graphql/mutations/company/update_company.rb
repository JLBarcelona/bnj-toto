class Mutations::Company::UpdateCompany < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Company, required: true

  field :company, ObjectTypes::Company, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    company = ::Company.find(id)

    if company.name != attributes[:name] && Company.where(name: attributes[:name]).exists?
      return { error: "この名称はすでに存在しています。" }
    end

    company.update!(attributes.to_h)

    { company: company.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
