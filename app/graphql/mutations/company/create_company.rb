class Mutations::Company::CreateCompany < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::Company, required: true

  field :company, ObjectTypes::Company, null: true
  field :error, String, null: true

  def resolve(attributes:)
    if Company.where(name: attributes[:name]).exists?
      return { error: "この名称はすでに存在しています。" }
    end

    company = Company.create!(attributes.to_h)

    { company: company }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
