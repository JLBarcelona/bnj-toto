class Mutations::Item::CopyItems < Mutations::BaseMutation
  field :error, String, null: true

  def resolve
    current_year = ::Year.last
    new_year = ::Year.create!(year: current_year.year + 1)
    ::Item.where(year: current_year).find_each do |item|
      new_item = ::Item.create!(
        year: new_year,
        name: item.name,
        price: item.price,
        short_name: item.short_name,
        item_code: item.item_code,
        unit: item.unit,
        company: item.company
      )

      new_item.item_code.update!(name: new_item.name)
    end
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
