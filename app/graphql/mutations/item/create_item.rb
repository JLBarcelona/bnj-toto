class Mutations::Item::CreateItem < Mutations::BaseMutation
  null false

  argument :code, String, required: true
  argument :year, String, required: true
  argument :attributes, InputTypes::Item, required: true

  field :item, ObjectTypes::Item, null: true
  field :error, String, null: true

  def resolve(code:, year:, attributes:)
    item_code = ::ItemCode.find_by(code: code)
    unless item_code
      item_code = ::ItemCode.create!(code: code)
    end

    y = ::Year.find_by(year: year)

    if ::Item.where(item_code_id: item_code.id, year: y).exists?
      return { error: "このコードはすでに存在しています。" }
    end

    item = ::Item.create!(attributes.to_h.merge(item_code_id: item_code.id, year: y))

    { item: item }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
