class Mutations::Item::UpdateItem < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Item, required: true

  field :item, ObjectTypes::Item, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    item = ::Item.find(id)
    item.update!(attributes.to_h)

    { item: item.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
