class Mutations::User::CreateUser < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::User, required: true

  field :user, ObjectTypes::User, null: true
  field :error, String, null: true

  def resolve(attributes:)
    if User.where(login_id: attributes[:login_id]).exists?
      return { error: '対象のログインIDは使用されています。' }
    end

    user = ::User.create!(attributes.to_h)

    { user: user }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
