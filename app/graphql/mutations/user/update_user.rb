class Mutations::User::UpdateUser < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::User, required: true

  field :user, ObjectTypes::User, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    if User.where(login_id: attributes[:login_id]).where.not(id: id).exists?
      return { error: '対象のログインIDは使用されています。' }
    end

    user = User.find(id)
    user.update!(attributes.to_h)

    { user: user.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
