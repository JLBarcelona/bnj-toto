class Mutations::JkkUser::CreateJkkUser < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::JkkUser, required: true

  field :jkk_user, ObjectTypes::JkkUser, null: true
  field :error, String, null: true

  def resolve(attributes:)
    jkk_user = ::JkkUser.create!(attributes.to_h)

    { jkk_user: jkk_user }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
