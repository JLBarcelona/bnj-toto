class Mutations::JkkUser::UpdateJkkUser < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::JkkUser, required: true

  field :jkk_user, ObjectTypes::JkkUser, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    jkk_user = ::JkkUser.find(id)
    jkk_user.update!(attributes.to_h)

    { jkk_user: jkk_user.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
