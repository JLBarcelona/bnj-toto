class Mutations::Project::UpdateProjectYear < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :year_id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, year_id:)
    project = Project.find(id)
    project.change_year(year_id)
    project.calculate_price

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
