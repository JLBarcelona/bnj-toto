class Mutations::Project::RemoveProjectItem < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:)
    project_item = ProjectItem.find(id)
    project = project_item.project
    project_item.destroy!

    project.calculate_price
    project.update_items(context[:current_user])
    # project.reload

    { project: project }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
