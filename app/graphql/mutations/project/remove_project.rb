class Mutations::Project::RemoveProject < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :result, String, null: true
  field :error, String, null: true

  def resolve(id:)
    project = ::Project.find(id)
    project.destroy!

    { result: 'success' }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
