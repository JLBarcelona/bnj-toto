class Mutations::Project::UpdateProjectOrderCsv < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :order, ObjectTypes::Order, null: true
  field :error, String, null: true

  def resolve(id:)
    order = ::Order.find(id)
    order.create_csv
    order.update!(export_csv_date: Date.today)

    { order: order.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.message
    return GraphQL::ExecutionError.new(e.message)
  end
end
