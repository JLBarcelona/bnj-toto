class Mutations::Project::ClearProjectUser < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:)
    project = ::Project.find(id)
    project.clear_edit_user(context[:current_user])

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
