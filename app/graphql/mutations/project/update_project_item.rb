class Mutations::Project::UpdateProjectItem < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::ProjectItem, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    project_item = ProjectItem.find(id)
    project_item.update!(attributes.to_h)
    project_item.update_item(context[:current_user])
    # project_item.project.update_companies
    # project_item.calculate
    # project_item.project.calculate_price
    # project_item.project.create_order
    # project_item.project.update_items(context[:current_user])

    { project: project_item.project.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
