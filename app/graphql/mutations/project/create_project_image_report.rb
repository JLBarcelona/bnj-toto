class Mutations::Project::CreateProjectImageReport < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:)
    project = ::Project.find(id)
    project.create_image_report

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.message
    return GraphQL::ExecutionError.new(e.message)
  end
end
