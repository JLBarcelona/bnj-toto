class Mutations::Project::UpdateProjectItemsCompany < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :company_type_id, ID, required: true
  argument :company_id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, company_type_id:, company_id:)
    project = ::Project.find(id)
    project.update_items_company(company_type_id, company_id)

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
