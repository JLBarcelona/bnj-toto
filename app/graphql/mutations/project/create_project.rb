class Mutations::Project::CreateProject < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::Project, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(attributes:)
    project = ::Project.create!(attributes.to_h)

    { project: project }
  rescue ActiveRecord::RecordInvalid => invalid
    Rails.logger.error invalid.record.errors
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
