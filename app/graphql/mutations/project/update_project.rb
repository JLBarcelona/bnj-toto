class Mutations::Project::UpdateProject < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Project, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    project = ::Project.find(id)
    project.update!(attributes.to_h)

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
