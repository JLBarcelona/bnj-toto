class Mutations::Project::UpdateProjectOrder < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Order, required: true

  field :order, ObjectTypes::Order, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    order = ::Order.find(id)
    order.update!(attributes.to_h)

    { order: order.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.message
    return GraphQL::ExecutionError.new(e.message)
  end
end
