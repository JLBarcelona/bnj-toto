class Mutations::Project::ImportItem < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :file, Types::FileType, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, file:)
    project = ::Project.find(id)
    project.update!(item_csv: file)
    project.import_item(context[:current_user])
    project.update_items(context[:current_user])

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
