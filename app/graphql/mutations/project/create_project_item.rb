class Mutations::Project::CreateProjectItem < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::ProjectItem, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    project = ::Project.find(id)

    # 業者設定
    item = ::Item.find(attributes.to_h[:item_id])
    project.create_item(attributes.to_h, context[:current_user])

    { project: project.reload }
  rescue ActiveRecord::RecordInvalid => invalid
    Rails.logger.error invalid.record.errors
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.message
    return GraphQL::ExecutionError.new(e.message)
  end
end
