class Mutations::Project::RemoveProjectCompanyType < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:)
    project_company_type = ::ProjectCompanyType.find(id)
    project = project_company_type.project
    project_company_type.destroy!

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.message
    return GraphQL::ExecutionError.new(e.message)
  end
end
