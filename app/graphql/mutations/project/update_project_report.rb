class Mutations::Project::UpdateProjectReport < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::ProjectReport, required: true

  field :project_report, ObjectTypes::ProjectReport, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    project_report = ::ProjectReport.find(id)
    project_report.update!(attributes.to_h)
    project_report.update_project_image_date

    { project_report: project_report.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
