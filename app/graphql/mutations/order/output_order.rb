class Mutations::Order::OutputOrder < Mutations::BaseMutation
  null false

  argument :spreadsheet_id, String, required: true
  argument :search, InputTypes::OrderSearch, required: true

  field :result, String, null: true

  def resolve(spreadsheet_id:, search:)
    orders = ::Order.search(::Order.all, search)
    ::Order.create_spreadsheet(orders, spreadsheet_id)

    { result: 'success' }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
