class Mutations::RoomLayout::CreateRoomLayout < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::RoomLayout, required: true

  field :room_layout, ObjectTypes::RoomLayout, null: true
  field :error, String, null: true

  def resolve(attributes:)
    if RoomLayout.where(name: attributes[:name]).exists?
      return { error: '対象の名称は使用されています。' }
    end

    room_layout = ::RoomLayout.create!(attributes.to_h)

    { room_layout: room_layout }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
