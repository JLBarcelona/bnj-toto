class Mutations::RoomLayout::UpdateRoomLayout < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::RoomLayout, required: true

  field :room_layout, ObjectTypes::RoomLayout, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    room_layout = RoomLayout.find(id)

    if room_layout.name != attributes[:name] && RoomLayout.where(name: attributes[:name]).exists?
      return { error: '対象の名称は使用されています。' }
    end

    room_layout.update!(attributes.to_h)

    { room_layout: room_layout.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
