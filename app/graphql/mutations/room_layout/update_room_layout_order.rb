class Mutations::RoomLayout::UpdateRoomLayoutOrder < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::RoomLayout, required: true

  field :room_layouts, [ObjectTypes::RoomLayout], null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    room_layout = ::RoomLayout.find(id)
    room_layout.insert_at(attributes[:position])

    { room_layouts: RoomLayout.order(:position) }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
