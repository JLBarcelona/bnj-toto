class Mutations::BuildingNumber::RemoveBuildingNumber < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :building, ObjectTypes::Building, null: true
  field :error, String, null: true

  def resolve(id:)
    building_number = BuildingNumber.find(id)
    if building_number.rooms.present?
      return { error: "この号棟は削除できません" }
    end

    building = building_number.building
    building_number.destroy!

    { building: building.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
