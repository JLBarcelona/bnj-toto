class Mutations::BuildingNumber::UpdateBuildingNumber < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::BuildingNumber, required: true

  field :building, ObjectTypes::Building, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    building_number = BuildingNumber.find(id)
    building_number.update!(attributes.to_h)

    { building: building_number.building.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
