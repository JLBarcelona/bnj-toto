class Mutations::BuildingNumber::CreateBuildingNumber < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::BuildingNumber, required: true

  field :building, ObjectTypes::Building, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    building = Building.find(id)
    building.building_numbers.create!(attributes.to_h)

    { building: building.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
