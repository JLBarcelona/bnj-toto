class Mutations::CompanyType::UpdateCompanyTypeModel < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::CompanyType, required: true

  field :company_type, ObjectTypes::CompanyType, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    company_type = ::CompanyType.find(id)

    if ::CompanyType.find_by(name: attributes[:name]).present?
      return { error: "この業者種別はすでに使用されています。" }
    end

    company_type.update!(attributes.to_h)

    { company_type: company_type.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
