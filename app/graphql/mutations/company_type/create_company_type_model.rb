class Mutations::CompanyType::CreateCompanyTypeModel < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::CompanyType, required: true

  field :company_type, ObjectTypes::CompanyType, null: true
  field :error, String, null: true

  def resolve(attributes:)
    company_type = ::CompanyType.find_by(name: attributes[:name])
    if company_type.present?
      return { error: "この業者種別はすでに使用されています。" }
    end

    company_type = ::CompanyType.create!(attributes.to_h)

    { company_type: company_type }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
