class Mutations::CompanyType::UpdateCompanyTypeOrder < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::CompanyType, required: true

  field :company_types, [ObjectTypes::CompanyType], null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    company_type = ::CompanyType.find(id)
    company_type.insert_at(attributes[:position])

    { company_types: CompanyType.order(:position) }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
