class Mutations::Comment::RemoveComment < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :comment, ObjectTypes::Comment, null: true
  field :error, String, null: true

  def resolve(id:)
    comment = ::Comment.find(id)

    if comment.project_images.present?
      return { error: "このコメントは削除できません" }
    end

    comment.destroy!

    { comment: comment }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
