class Mutations::Comment::AddCommentCode < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :item_code_id, ID, required: true

  field :comment, ObjectTypes::Comment, null: true
  field :error, String, null: true

  def resolve(id:, item_code_id:)
    comment = ::Comment.find(id)
    item_code = ::ItemCode.find(item_code_id)

    unless comment.item_codes.where(id: item_code.id).exists?
      comment.item_codes << item_code
    end

    { comment: comment.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
