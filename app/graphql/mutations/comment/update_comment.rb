class Mutations::Comment::UpdateComment < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Comment, required: true

  field :comment, ObjectTypes::Comment, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    comment = Comment.find(id)
    comment.update!(attributes.to_h)

    { comment: comment.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
