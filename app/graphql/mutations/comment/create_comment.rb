class Mutations::Comment::CreateComment < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::Comment, required: true

  field :comment, ObjectTypes::Comment, null: true
  field :error, String, null: true

  def resolve(attributes:)
    comment = ::Comment.create!(attributes.to_h)

    { comment: comment }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
