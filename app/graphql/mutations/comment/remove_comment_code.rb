class Mutations::Comment::RemoveCommentCode < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :item_code_id, ID, required: true

  field :comment, ObjectTypes::Comment, null: true
  field :error, String, null: true

  def resolve(id:, item_code_id:)
    comment = ::Comment.find(id)
    item_code = ::ItemCode.find(item_code_id)
    comment.comment_item_codes.find_by(item_code_id: item_code.id).destroy!

    { comment: comment.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
