class Mutations::RoomType::CreateRoomTypeModel < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::RoomType, required: true

  field :room_type, ObjectTypes::RoomType, null: true
  field :error, String, null: true

  def resolve(attributes:)
    same_room_type = ::RoomType.find_by(name: attributes[:name])
    if same_room_type.present?
      return { error: "この名称はすでに存在しています。" }
    end

    room_type = ::RoomType.create!(attributes.to_h)

    { room_type: room_type }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
