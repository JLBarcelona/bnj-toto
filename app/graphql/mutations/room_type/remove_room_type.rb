class Mutations::CompanyType::RemoveCompanyType < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :company_type, ObjectTypes::CompanyType, null: true
  field :error, String, null: true

  def resolve(id:)
    company_type = ::CompanyType.find(id)
    if company_type.companies.present?
      names = company_type.companies.map(&:name)
      return { error: "この業者種別は使用されているため、削除できません: #{names.join(',')}" }
    end

    company_type.destroy!

    { company_type: nil }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
