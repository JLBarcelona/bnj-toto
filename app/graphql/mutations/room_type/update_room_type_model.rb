class Mutations::RoomType::UpdateRoomTypeModel < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::RoomType, required: true

  field :room_type, ObjectTypes::RoomType, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    room_type = ::RoomType.find(id)
    room_type.update!(attributes.to_h)

    { room_type: room_type.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
