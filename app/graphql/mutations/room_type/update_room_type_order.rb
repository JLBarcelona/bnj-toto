class Mutations::RoomType::UpdateRoomTypeOrder < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::RoomType, required: true

  field :room_types, [ObjectTypes::RoomType], null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    room_type = ::RoomType.find(id)
    room_type.insert_at(attributes[:position])

    { room_types: RoomType.order(:position) }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
