class Mutations::CommentType::RemoveCommentTypeModel < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :comment_type, ObjectTypes::CommentType, null: true
  field :error, String, null: true

  def resolve(id:)
    comment_type = ::CommentType.find(id)

    if comment_type.comments.present?
      return { error: "このコメント種別は削除できません" }
    end

    comment_type.destroy!

    { comment_type: comment_type }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
