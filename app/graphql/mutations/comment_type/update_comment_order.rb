class Mutations::CommentType::UpdateCommentOrder < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :position, Int, required: true

  field :comment_type, ObjectTypes::CommentType, null: true
  field :error, String, null: true

  def resolve(id:, position:)
    comment = ::Comment.find(id)
    comment.insert_at(position)

    { comment_type: comment.reload.comment_type }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
