class Mutations::CommentType::CreateCommentTypeModel < Mutations::BaseMutation
  null false

  argument :attributes, InputTypes::CommentType, required: true

  field :comment_type, ObjectTypes::CommentType, null: true
  field :error, String, null: true

  def resolve(attributes:)
    comment_type = ::CommentType.create!(attributes.to_h)

    { comment_type: comment_type }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
