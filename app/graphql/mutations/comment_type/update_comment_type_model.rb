class Mutations::CommentType::UpdateCommentTypeModel < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::CommentType, required: true

  field :comment_type, ObjectTypes::CommentType, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    comment_type = CommentType.find(id)
    comment_type.update!(attributes.to_h)

    { comment_type: comment_type.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
