class Mutations::ProjectImage::RemoveProjectImage < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:)
    project_image = ::ProjectImage.find(id)
    project = project_image.project
    project_image.destroy!

    { project: project.reload }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
