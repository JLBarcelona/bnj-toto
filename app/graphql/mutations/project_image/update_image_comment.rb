class Mutations::ProjectImage::UpdateImageComment < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::ImageComment, required: true

  field :image_comment, ObjectTypes::ImageComment, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    image_comment = ::ImageComment.find(id)
    image_comment.update!(attributes.to_h)

    { image_comment: image_comment.reload }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
