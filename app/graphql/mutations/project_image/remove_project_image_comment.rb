class Mutations::ProjectImage::RemoveProjectImageComment < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :comment_id, ID, required: true

  field :project_image, ObjectTypes::ProjectImage, null: true
  field :error, String, null: true

  def resolve(id:, comment_id:)
    project_image = ::ProjectImage.find(id)
    comment = ::Comment.find(comment_id)
    project_image.project_image_comments.find_by(comment: comment).destroy!

    { project_image: project_image.reload }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
