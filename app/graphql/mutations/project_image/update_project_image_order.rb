class Mutations::ProjectImage::UpdateProjectImageOrder < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::ProjectImage, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    project_image = ::ProjectImage.find(id)
    project_image.insert_at(attributes[:position])

    { project: project_image.reload.project }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
