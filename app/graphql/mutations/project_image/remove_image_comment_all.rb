class Mutations::ProjectImage::RemoveImageCommentAll < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project_image, ObjectTypes::ProjectImage, null: true
  field :error, String, null: true

  def resolve(id:)
    project_image = ::ProjectImage.find(id)
    project_image.image_comments.destroy_all

    { project_image: project_image.reload }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
