class Mutations::ProjectImage::AddImageComment < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :item_id, ID, required: true

  field :project_image, ObjectTypes::ProjectImage, null: true
  field :error, String, null: true

  def resolve(id:, item_id:)
    project_image = ::ProjectImage.find(id)
    item = ::Item.find(item_id)
    project_image.image_comments.create!(
      code: item.item_code.code,
      text: item.item_code.name,
    )

    { project_image: project_image.reload }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
