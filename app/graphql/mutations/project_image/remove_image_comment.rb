class Mutations::ProjectImage::RemoveImageComment < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :project_image, ObjectTypes::ProjectImage, null: true
  field :error, String, null: true

  def resolve(id:)
    image_comment = ::ImageComment.find(id)
    project_image = image_comment.project_image
    image_comment.destroy!

    { project_image: project_image.reload }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
