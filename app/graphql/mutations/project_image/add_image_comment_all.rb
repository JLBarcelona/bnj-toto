class Mutations::ProjectImage::AddImageCommentAll < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :comment_id, ID, required: true

  field :project_image, ObjectTypes::ProjectImage, null: true
  field :error, String, null: true

  def resolve(id:, comment_id:)
    project_image = ::ProjectImage.find(id)
    comment = ::Comment.find(comment_id)

    comment.comment_items.each do |comment_item|
      next if project_image.image_comments.where(code: comment_item.item.code).exists?

      project_image.image_comments.create!(
        code: comment_item.item.code,
        text: comment_item.item.name,
      )
    end

    project = project_image.reload.project
    project.create_image_report

    { project_image: project_image.reload }
  rescue => e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
