class Mutations::ProjectImage::CreateProjectImage < Mutations::BaseMutation
  null false

  argument :count, Int, required: true
  argument :attributes, InputTypes::ProjectImage, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(count:, attributes:)
    project_image = nil
    count.times.each do |i|
      project_image = ::ProjectImage.create!(attributes.to_h)
    end

    { project: project_image.project }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
