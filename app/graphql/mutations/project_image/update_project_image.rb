class Mutations::ProjectImage::UpdateProjectImage < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::ProjectImage, required: true

  field :project, ObjectTypes::Project, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    project_image = ::ProjectImage.find(id)
    project_image.update!(attributes.to_h)
    project = project_image.reload.project

    { project: project }
  rescue => e
    Rails.logger.error e
    Rails.logger.error e.backtrace
    return GraphQL::ExecutionError.new(e.message)
  end
end
