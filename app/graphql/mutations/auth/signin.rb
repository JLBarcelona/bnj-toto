class Mutations::Auth::Signin < Mutations::BaseMutation
  null false

  argument :login_id, String, required: true
  argument :password, String, required: true

  field :user, ObjectTypes::User, null: true
  field :error, String, null: true

  def resolve(login_id:, password:)
    user = User.find_by(login_id: login_id)

    return { error: 'ログインIDまたはパスワードが正しくありません' } unless user

    if user.authenticate(password)
      context[:session][:user_id] = user.id

      { user: user }
    else
      { error: 'ログインIDまたはパスワードが正しくありません' }
    end
  rescue => e
    raise GraphQL::ExecutionError.new(e.message)
  end
end