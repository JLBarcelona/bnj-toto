class Mutations::Room::CopyRoom < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Room, required: true

  field :building_number, ObjectTypes::BuildingNumber, null: true
  field :error, String, null: true

  def resolve(id:, attributes:)
    room = ::Room.find(id)
    same_room = ::Room.find_by(building_number_id: room.building_number_id, name: attributes[:name])
    if same_room.present?
      return { error: "この名称はすでに存在しています。" }
    end

    new_room = room.dup
    new_room.assign_attributes(attributes.to_h)
    new_room.room_types = room.room_types
    new_room.save!

    { building_number: new_room.building_number.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
