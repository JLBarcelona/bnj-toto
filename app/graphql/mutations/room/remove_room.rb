class Mutations::Room::RemoveRoom < Mutations::BaseMutation
  null false

  argument :id, ID, required: true

  field :building_number, ObjectTypes::BuildingNumber, null: true
  field :error, String, null: true

  def resolve(id:)
    room = ::Room.find(id)
    if room.projects.present?
      return { error: "この部屋は案件で使用されています。" }
    end

    building_number = room.building_number
    room.destroy!

    { building_number: building_number.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
