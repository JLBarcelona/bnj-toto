class Mutations::Room::UpdateRoom < Mutations::BaseMutation
  null false

  argument :id, ID, required: true
  argument :attributes, InputTypes::Room, required: true
  argument :room_type_ids, [ID], required: true

  field :room, ObjectTypes::Room, null: true
  field :error, String, null: true

  def resolve(id:, attributes:, room_type_ids:)
    room = ::Room.find(id)
    room.update!(attributes.to_h)
    room.room_types = ::RoomType.where(id: room_type_ids)

    { room: room.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
