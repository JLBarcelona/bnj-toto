class Mutations::Room::CreateRoom < Mutations::BaseMutation
  null false

  argument :building_number_id, ID, required: true
  argument :attributes, InputTypes::Room, required: true

  field :building_number, ObjectTypes::BuildingNumber, null: true
  field :error, String, null: true

  def resolve(building_number_id:, attributes:)
    building_number = BuildingNumber.find(building_number_id)
    building_number.rooms.create!(attributes.to_h)

    { building_number: building_number.reload }
  rescue => e
    Rails.logger.error e
    return GraphQL::ExecutionError.new(e.message)
  end
end
