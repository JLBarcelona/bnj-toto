class ObjectTypes::JkkUser < Types::BaseObject
  graphql_name "JkkUser"

  field :id, ID, null: true
  field :email, String, null: true
  field :first_name, String, null: true
  field :last_name, String, null: true
  field :active, Boolean, null: true
end
