class ObjectTypes::Items < Types::BaseObject
  graphql_name "Items"

  field :items, [ObjectTypes::Item, null: true], null: true
  def items
    object
  end

  field :pagination, ObjectTypes::Pagination, null: true
  def pagination
    object
  end
end
