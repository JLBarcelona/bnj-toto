class ObjectTypes::Unit < Types::BaseObject
  graphql_name "Unit"

  field :id, ID, null: true
  field :name, String, null: true
end
