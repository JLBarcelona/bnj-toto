class ObjectTypes::ProjectItem < Types::BaseObject
  graphql_name "ProjectItem"

  field :id, ID, null: true
  field :project_id, ID, null: true
  field :item_id, ID, null: true
  field :company_id, ID, null: true
  field :code, String, null: true
  field :name, String, null: true
  field :count, Float, null: true
  field :unit_price, Int, null: true
  field :sequence_number, Int, null: true
  field :amount, Int, null: true
  field :item, ObjectTypes::Item, null: true
  field :company, ObjectTypes::Company, null: true
  field :room_type_ids, [ID], null: true
  field :room_types, [ObjectTypes::RoomType], null: true
  field :selectable_companies, [ObjectTypes::Company], null: true
  field :updated_items_at, Types::DateTimeType, null: true
  field :updated_items_user, ObjectTypes::User, null: true
end
