class ObjectTypes::Room < Types::BaseObject
  graphql_name "Room"

  field :id, ID, null: true
  field :building_number_id, ID, null: true
  field :name, String, null: true
  field :position, Int, null: true
  field :room_types, [ObjectTypes::RoomType], null: true
  field :room_model_type_id, ID, null: true
  field :room_layout_id, ID, null: true
end
