class ObjectTypes::CommentType < Types::BaseObject
  graphql_name "CommentType"

  field :id, ID, null: true
  field :name, String, null: true
  field :position, Int, null: true
  field :comments, [ObjectTypes::Comment], null: true
end
