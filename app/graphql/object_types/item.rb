class ObjectTypes::Item < Types::BaseObject
  graphql_name "Item"

  field :id, ID, null: true
  field :code, String, null: true
  field :name, String, null: true
  field :short_name, String, null: true
  field :price, Int, null: true
  field :order_price, Int, null: true
  field :company_id, ID, null: true
  field :company_type_ids, [ID], null: true
  field :company_types, [ObjectTypes::CompanyType], null: true
  field :item_code, ObjectTypes::ItemCode, null: true
  field :year, ObjectTypes::Year, null: true
  field :company, ObjectTypes::Company, null: true
  field :unit_id, ID, null: true
  field :unit, ObjectTypes::Unit, null: true
  field :order, Boolean, null: true
end
