class ObjectTypes::PurchaseOrder < Types::BaseObject
  graphql_name "PurchaseOrder"

  field :id, ID, null: true
  field :project_id, ID, null: true
  field :file_url, String, null: true

  def file_url
    Rails.env.development? ? 
      object.file_url.present? ? "http://localhost:3000#{object.file_url}" : nil :
      object.file_url
  end
end
