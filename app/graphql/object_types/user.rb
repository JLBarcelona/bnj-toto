class ObjectTypes::User < Types::BaseObject
  graphql_name "User"

  field :id, ID, null: true
  field :login_id, String, null: true
  field :email, String, null: true
  field :role, String, null: true
  field :first_name, String, null: true
  field :last_name, String, null: true
  field :tel, String, null: true
  field :is_assesment, Boolean, null: true
  field :active, Boolean, null: true
end
