class ObjectTypes::ProjectReport < Types::BaseObject
  graphql_name "ProjectReport"

  field :id, ID, null: true
  field :project_id, ID, null: true
  field :worksheet_base, String, null: true
  field :worksheet_items, String, null: true
  field :worksheet_images, String, null: true
  field :worksheet_checklist, String, null: true
  field :spreadsheet_id1, String, null: true
  field :spreadsheet_id2, String, null: true
  field :spreadsheet_id3, String, null: true
  field :spreadsheet_id4, String, null: true
  field :spreadsheet_id5, String, null: true
  field :spreadsheet_id6, String, null: true
  field :spreadsheet_id7, String, null: true
  field :spreadsheet_datetime1, String, null: true
  field :spreadsheet_datetime2, String, null: true
  field :spreadsheet_datetime3, String, null: true
  field :spreadsheet_datetime4, String, null: true
  field :spreadsheet_datetime5, String, null: true
  field :spreadsheet_datetime6, String, null: true
  field :spreadsheet_datetime7, String, null: true
  field :spreadsheet_check_report1, Boolean, null: true
  field :spreadsheet_check_report2, Boolean, null: true
end
