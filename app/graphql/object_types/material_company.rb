class ObjectTypes::MaterialCompany < Types::BaseObject
  graphql_name "MaterialCompany"

  field :id, ID, null: true
  field :name, String, null: true
  field :is_invoice, Boolean, null: true
end
