class ObjectTypes::CommentItem < Types::BaseObject
  graphql_name "CommentItem"

  field :id, ID, null: true
  field :comment_set_id, ID, null: true
  field :item_code_id, ID, null: true
  field :item_code, ObjectTypes::ItemCode, null: true
end
