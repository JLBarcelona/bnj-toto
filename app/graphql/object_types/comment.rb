class ObjectTypes::Comment < Types::BaseObject
  graphql_name "Comment"

  field :id, ID, null: false
  field :comment, String, null: true
  field :comment_type_id, ID, null: true
  field :item_code_id, ID, null: true
  field :position, Int, null: true
  field :remark, String, null: true
  field :comment_type, ObjectTypes::CommentType, null: true
  field :comment_item_codes, [ObjectTypes::CommentItemCode], null: true
  field :item_codes, [ObjectTypes::ItemCode], null: true
end
