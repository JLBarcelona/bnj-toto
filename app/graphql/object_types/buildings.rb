class ObjectTypes::Buildings < Types::BaseObject
  graphql_name "Buildings"

  field :buildings, [ObjectTypes::Building], null: true
  def buildings
    object
  end

  field :pagination, ObjectTypes::Pagination, null: true
  def pagination
    object
  end
end
