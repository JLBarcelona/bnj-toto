class ObjectTypes::CommentItemCode < Types::BaseObject
  graphql_name "CommentItemCode"

  field :id, ID, null: true
  field :comment_id, ID, null: true
  field :item_code_id, ID, null: true
  field :item_code, ObjectTypes::ItemCode, null: true
  field :position, Int, null: true
end
