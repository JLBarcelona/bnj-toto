class ObjectTypes::BuildingType < Types::BaseObject
  graphql_name "BuildingType"

  field :id, ID, null: true
  field :name, String, null: true
end
