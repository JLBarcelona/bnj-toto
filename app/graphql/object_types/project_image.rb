class ObjectTypes::ProjectImage < Types::BaseObject
  graphql_name "ProjectImage"

  field :id, ID, null: true
  field :project_id, ID, null: true
  field :room_type_id, ID, null: true
  field :image_type, String, null: true
  field :image_before, String, null: true
  field :image_after, String, null: true
  field :image, String, null: true
  field :comment, String, null: true
  field :position, Int, null: true
  field :image_comments, [ObjectTypes::ImageComment], null: true
  field :comments, [ObjectTypes::Comment], null: true
end
