class ObjectTypes::RoomType < Types::BaseObject
  graphql_name "RoomType"

  field :id, ID, null: true
  field :name, String, null: true
  field :position, Int, null: true
end
