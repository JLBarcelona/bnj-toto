class ObjectTypes::ItemCode < Types::BaseObject
  graphql_name "ItemCode"

  field :id, ID, null: true
  field :code, String, null: true
  field :name, String, null: true
  field :items, [ObjectTypes::Item], null: true
end
