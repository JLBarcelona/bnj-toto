class ObjectTypes::Company < Types::BaseObject
  graphql_name "Company"

  field :id, ID, null: true
  field :is_order_each, Boolean, null: true
  field :is_order_monthly, Boolean, null: true
  field :name, String, null: true
  field :zipcode, String, null: true
  field :address, String, null: true
  field :tel, String, null: true
  field :fax, String, null: true
  field :company_type_ids, [ID], null: true
  field :company_types, [ObjectTypes::CompanyType], null: true
end
