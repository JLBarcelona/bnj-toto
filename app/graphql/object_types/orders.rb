class ObjectTypes::Orders < Types::BaseObject
  graphql_name "Orders"

  field :orders, [ObjectTypes::Order, null: true], null: true
  def orders
    object
  end

  field :pagination, ObjectTypes::Pagination, null: true
  def pagination
    object
  end
end
