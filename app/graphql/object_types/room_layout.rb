class ObjectTypes::RoomLayout < Types::BaseObject
  graphql_name "RoomLayout"

  field :id, ID, null: true
  field :name, String, null: true
  field :price1, Int, null: true
  field :price2, Int, null: true
  field :position, Int, null: true
end
