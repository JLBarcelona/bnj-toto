class ObjectTypes::ImageComment < Types::BaseObject
  graphql_name "ImageComment"

  field :id, ID, null: true
  field :project_image_id, ID, null: true
  field :code, String, null: true
  field :text, String, null: true
end
