class ObjectTypes::BuildingNumber < Types::BaseObject
  graphql_name "BuildingNumber"

  field :id, ID, null: true
  field :building_id, ID, null: true
  field :name, String, null: true
  field :position, Int, null: true
  field :rooms, [ObjectTypes::Room], null: true
end
