class ObjectTypes::Projects < Types::BaseObject
  graphql_name "Projects"

  field :projects, [ObjectTypes::Project, null: true], null: true
  def projects
    object
  end

  field :pagination, ObjectTypes::Pagination, null: true
  def pagination
    object
  end
end
