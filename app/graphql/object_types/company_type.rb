class ObjectTypes::CompanyType < Types::BaseObject
  graphql_name "CompanyType"

  field :id, ID, null: true
  field :name, String, null: true
  field :position, Int, null: true
  field :companies, [ObjectTypes::Company], null: true
end
