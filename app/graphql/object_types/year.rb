class ObjectTypes::Year < Types::BaseObject
  graphql_name "Year"

  field :id, ID, null: true
  field :year, Int, null: true
end
