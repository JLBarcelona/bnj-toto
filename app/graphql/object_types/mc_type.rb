class ObjectTypes::McType < Types::BaseObject
  graphql_name "McType"

  field :id, ID, null: true
  field :name, String, null: true
end
