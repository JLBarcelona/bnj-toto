class ObjectTypes::RoomModelType < Types::BaseObject
  graphql_name "RoomModelType"

  field :id, ID, null: true
  field :name, String, null: true
  field :position, Int, null: true
end
