class ObjectTypes::Order < Types::BaseObject
  graphql_name "Order"

  field :id, ID, null: true
  field :project_id, ID, null: true
  field :company_id, ID, null: true
  field :is_invoice, Boolean, null: true
  field :order_type, String, null: true
  field :order_date, String, null: true
  field :total_price, Int, null: true
  field :export_csv_date, String, null: true
  # field :csv, String, null: true
  field :company, ObjectTypes::Company, null: true
  field :project, ObjectTypes::Project, null: true
end
