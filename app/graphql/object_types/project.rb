class ObjectTypes::Project < Types::BaseObject
  graphql_name "Project"

  field :id, ID, null: true
  field :year_id, ID, null: true
  field :year, ObjectTypes::Year, null: true
  field :building_id, ID, null: true
  field :building_number_id, ID, null: true
  field :room_id, ID, null: true
  field :user_ids, [ID], null: true
  field :jkk_user_id, ID, null: true
  field :assesment_user_id, ID, null: true
  field :construction_user_id, ID, null: true
  field :total_item_price, Int, null: true
  field :total_item_price_round, Int, null: true
  field :tax, Int, null: true
  field :total_price, Int, null: true
  field :total_price_input, Int, null: true
  field :building, ObjectTypes::Building, null: true
  field :building_number, ObjectTypes::BuildingNumber, null: true
  field :room, ObjectTypes::Room, null: true
  field :jkk_status1, Boolean, null: true
  field :jkk_status2, Boolean, null: true
  field :jkk_status3, Boolean, null: true
  field :jkk_status4, Boolean, null: true
  field :order_number_type, String, null: true
  field :order_number, String, null: true
  field :name, String, null: true
  field :assessment_date, String, null: true
  field :assessment_url, String, null: true
  field :order_date1, String, null: true
  field :order_memo1, String, null: true
  field :order_date2, String, null: true
  field :order_memo2, String, null: true
  field :order_date3, String, null: true
  field :order_memo3, String, null: true
  field :electric_start_date1, String, null: true
  field :electric_end_date1, String, null: true
  field :water_start_date1, String, null: true
  field :water_end_date1, String, null: true
  field :electric_start_date2, String, null: true
  field :electric_end_date2, String, null: true
  field :water_start_date2, String, null: true
  field :water_end_date2, String, null: true
  field :electric_start_date3, String, null: true
  field :electric_end_date3, String, null: true
  field :water_start_date3, String, null: true
  field :water_end_date3, String, null: true
  field :close_types, [String], null: true
  field :close_reason, String, null: true
  field :is_vermiculite, Boolean, null: true
  field :is_finished_vermiculite, Boolean, null: true
  field :vermiculite_date, String, null: true
  field :process_created_date, String, null: true
  field :send_assessment_date, String, null: true
  field :cleanup_images_date, String, null: true
  field :before_construction_image_date, String, null: true
  field :after_construction_image_date, String, null: true
  field :checklist_first_date, String, null: true
  field :checklist_last_date, String, null: true
  field :address, String, null: true
  field :construction_start_date, String, null: true
  field :construction_end_date, String, null: true
  field :is_faq, Boolean, null: true
  field :is_lack_image, Boolean, null: true
  field :is_complete_image, Boolean, null: true
  field :submit_document_date, String, null: true
  field :available_date, String, null: true
  field :living_year, Int, null: true
  field :living_month, Int, null: true
  field :leave_date, String, null: true
  field :is_manager_asbestos_document1, Boolean, null: true
  field :is_manager_asbestos_document2, Boolean, null: true
  field :is_manager_asbestos_document3, Boolean, null: true
  field :is_officework_asbestos_document1, Boolean, null: true
  field :is_officework_asbestos_document2, Boolean, null: true
  field :is_officework_asbestos_document3, Boolean, null: true
  field :remark, String, null: true
  field :project_items, [ObjectTypes::ProjectItem], null: true
  field :purchase_orders, [ObjectTypes::PurchaseOrder], null: true
  field :project_images, [ObjectTypes::ProjectImage], null: true
  field :orders, [ObjectTypes::Order], null: true
  field :orders_each, [ObjectTypes::Order], null: true
  field :orders_monthly, [ObjectTypes::Order], null: true
  field :project_report, ObjectTypes::ProjectReport, null: true
  field :image_report_url, String, null: true
  field :updated_items_at, Types::DateTimeType, null: true
  field :updated_items_user, ObjectTypes::User, null: true
  field :edit_user, ObjectTypes::User, null: true
  field :edit_user_id, ID, null: true
  field :item_upload_date_text, String, null: true
  field :report_date_text, String, null: true
  field :checklist_date_text, String, null: true
  field :checklist_date_text, String, null: true
  field :lack_image_date_text, String, null: true
  field :complete_image_date_text, String, null: true
  field :invoice_date_text, String, null: true
  field :image_status, String, null: true

  def assessment_url
    Rails.env.development? ? 
      object.assessment_url.present? ? "http://localhost:3000#{object.assessment_url}" : nil :
      object.assessment_url
  end

  def image_report_url
    Rails.env.development? ?
      object.image_report_url.present? ? "http://localhost:3000#{object.image_report_url}" : nil :
      object.image_report_url
  end
end
