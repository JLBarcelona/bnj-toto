class ObjectTypes::Building < Types::BaseObject
  graphql_name "Building"

  field :id, ID, null: true
  field :name, String, null: true
  field :address, String, null: true
  field :rank, String, null: true
  field :rank_text, String, null: true, hash_key: :rank_i18n
  field :building_numbers, [ObjectTypes::BuildingNumber], null: true
  field :mc_type_id, ID, null: true
  field :building_type_id, ID, null: true
  field :mc_type, ObjectTypes::McType, null: true
  field :building_type, ObjectTypes::BuildingType, null: true
end
