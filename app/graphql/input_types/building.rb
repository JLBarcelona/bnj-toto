class InputTypes::Building < Types::BaseInputObject
  graphql_name 'BuildingAttributes'

  argument :name, String, required: false
  argument :rank, String, required: false
  argument :building_type_id, ID, required: false
  argument :mc_type_id, ID, required: false
  argument :address, String, required: false
end
