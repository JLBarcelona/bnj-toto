class InputTypes::ProjectSearch < Types::BaseInputObject
  graphql_name 'ProjectSearch'

  argument :keyword, String, required: false
  argument :order_item, String, required: false
  argument :order_type, String, required: false
  argument :order_number_type, String, required: false
  argument :order_number, String, required: false
end
