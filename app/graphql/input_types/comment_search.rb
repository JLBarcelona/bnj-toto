class InputTypes::CommentSearch < Types::BaseInputObject
  graphql_name 'CommentSearch'

  argument :comment_type_id, ID, required: false
end
