class InputTypes::CommentType < Types::BaseInputObject
  graphql_name 'CommentTypeAttributes'

  argument :name, String, required: false
end
