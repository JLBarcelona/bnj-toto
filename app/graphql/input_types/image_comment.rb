class InputTypes::ImageComment < Types::BaseInputObject
  graphql_name 'ImageCommentAttributes'

  argument :code, String, required: false
  argument :text, String, required: false
end
