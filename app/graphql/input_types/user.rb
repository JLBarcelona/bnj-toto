class InputTypes::User < Types::BaseInputObject
  graphql_name 'UserAttributes'

  argument :login_id, String, required: false
  argument :email, String, required: false
  argument :password, String, required: false
  argument :password_confirmation, String, required: false
  argument :first_name, String, required: false
  argument :last_name, String, required: false
  argument :role, String, required: false
  argument :tel, String, required: false
  argument :is_assesment, Boolean, required: false
  argument :active, Boolean, required: false
end
