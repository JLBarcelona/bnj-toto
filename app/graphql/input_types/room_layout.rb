class InputTypes::RoomLayout < Types::BaseInputObject
  graphql_name 'RoomLayoutAttributes'

  argument :name, String, required: false
  argument :price1, Int, required: false
  argument :price2, Int, required: false
  argument :position, Int, required: false
end
