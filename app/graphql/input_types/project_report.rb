class InputTypes::ProjectReport < Types::BaseInputObject
  graphql_name 'ProjectReportAttributes'
  argument :worksheet_base, String, required: false
  argument :worksheet_items, String, required: false
  argument :worksheet_images, String, required: false
  argument :worksheet_checklist, String, required: false
  argument :spreadsheet_id1, String, required: false
  argument :spreadsheet_id2, String, required: false
  argument :spreadsheet_id3, String, required: false
  argument :spreadsheet_id4, String, required: false
  argument :spreadsheet_id5, String, required: false
  argument :spreadsheet_id6, String, required: false
  argument :spreadsheet_id7, String, required: false
  argument :spreadsheet_datetime1, Types::DateTimeType, required: false
  argument :spreadsheet_datetime2, Types::DateTimeType, required: false
  argument :spreadsheet_datetime3, Types::DateTimeType, required: false
  argument :spreadsheet_datetime4, Types::DateTimeType, required: false
  argument :spreadsheet_datetime5, Types::DateTimeType, required: false
  argument :spreadsheet_datetime6, Types::DateTimeType, required: false
  argument :spreadsheet_datetime7, Types::DateTimeType, required: false
  argument :spreadsheet_check_report1, Boolean, required: false
  argument :spreadsheet_check_report2, Boolean, required: false
end
