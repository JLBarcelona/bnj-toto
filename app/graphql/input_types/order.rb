class InputTypes::Order < Types::BaseInputObject
  graphql_name 'OrderAttributes'

  argument :project_id, ID, required: false
  argument :company_id, ID, required: false
  argument :is_invoice, Boolean, required: false
  argument :order_date, String, required: false
end
