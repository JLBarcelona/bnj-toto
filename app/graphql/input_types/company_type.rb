class InputTypes::CompanyType < Types::BaseInputObject
  graphql_name 'CompanyTypeAttributes'

  argument :name, String, required: false
  argument :position, Int, required: false
end
