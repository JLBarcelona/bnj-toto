class InputTypes::JkkUserSearch < Types::BaseInputObject
  graphql_name 'JkkUserSearch'

  argument :active, Boolean, required: false
end
