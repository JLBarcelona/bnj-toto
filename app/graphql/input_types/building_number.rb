class InputTypes::BuildingNumber < Types::BaseInputObject
  graphql_name 'BuildingNumberAttributes'

  argument :name, String, required: false
end
