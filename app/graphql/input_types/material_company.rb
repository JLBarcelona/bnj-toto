class InputTypes::MaterialCompany < Types::BaseInputObject
  graphql_name 'MaterialCompanyAttributes'

  argument :name, String, required: false
  argument :is_invoice, Boolean, required: false
end
