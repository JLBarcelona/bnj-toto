class InputTypes::BuildingSearch < Types::BaseInputObject
  graphql_name 'BuildingSearch'

  argument :keyword, String, required: false
end
