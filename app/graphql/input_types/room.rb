class InputTypes::Room < Types::BaseInputObject
  graphql_name 'RoomAttributes'

  argument :name, String, required: false
  argument :room_model_type_id, ID, required: false
  argument :room_layout_id, ID, required: false
end
