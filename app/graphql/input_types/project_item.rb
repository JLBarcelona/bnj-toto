class InputTypes::ProjectItem < Types::BaseInputObject
  graphql_name 'ProjectItemAttributes'

  argument :item_id, ID, required: false
  argument :year_id, ID, required: false
  argument :name, String, required: false
  argument :code, String, required: false
  argument :count, Float, required: false
  argument :unit_price, Int, required: false
  argument :amount, Int, required: false
  argument :room_type_ids, [ID], required: false
  argument :company_id, ID, required: false
end
