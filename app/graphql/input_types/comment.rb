class InputTypes::Comment < Types::BaseInputObject
  graphql_name 'CommentAttributes'

  argument :comment_type_id, ID, required: false
  argument :item_code_id, ID, required: false
  argument :comment, String, required: false
  argument :remark, String, required: false
end
