class InputTypes::Company < Types::BaseInputObject
  graphql_name 'CompanyAttributes'

  argument :name, String, required: false
  argument :zipcode, String, required: false
  argument :address, String, required: false
  argument :tel, String, required: false
  argument :fax, String, required: false
  argument :company_type_ids, [ID], required: false
  argument :is_order_each, Boolean, required: false
  argument :is_order_monthly, Boolean, required: false
end
