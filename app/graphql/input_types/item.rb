class InputTypes::Item < Types::BaseInputObject
  graphql_name 'ItemAttributes'

  argument :name, String, required: false
  argument :short_name, String, required: false
  argument :price, Int, required: false
  argument :order_price, Int, required: false
  argument :company_id, ID, required: false
  argument :company_type_ids, [ID], required: false
  argument :order, Boolean, required: false
  argument :unit_id, ID, required: false
end
