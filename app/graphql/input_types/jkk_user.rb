class InputTypes::JkkUser < Types::BaseInputObject
  graphql_name 'JkkUserAttributes'

  argument :email, String, required: false
  argument :first_name, String, required: false
  argument :last_name, String, required: false
  argument :active, Boolean, required: false
end
