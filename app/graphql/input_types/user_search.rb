class InputTypes::UserSearch < Types::BaseInputObject
  graphql_name 'UserSearch'

  argument :active, Boolean, required: false
  argument :is_assesment, Boolean, required: false
end
