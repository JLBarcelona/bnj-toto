class InputTypes::ItemSearch < Types::BaseInputObject
  graphql_name 'ItemSearch'

  argument :year, String, required: false
  argument :keyword, String, required: false
end
