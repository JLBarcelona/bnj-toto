class InputTypes::McType < Types::BaseInputObject
  graphql_name 'McTypeAttributes'

  argument :name, String, required: false
end
