class InputTypes::RoomType < Types::BaseInputObject
  graphql_name 'RoomTypeAttributes'

  argument :name, String, required: false
  argument :position, Int, required: false
end
