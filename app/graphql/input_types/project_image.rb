class InputTypes::ProjectImage < Types::BaseInputObject
  graphql_name 'ProjectImageAttributes'

  argument :project_id, ID, required: false
  argument :room_type_id, ID, required: false
  argument :image_type, String, required: false
  argument :image_before, Types::FileType, required: false
  argument :image_after, Types::FileType, required: false
  argument :image, Types::FileType, required: false
  argument :comment, String, required: false
  argument :position, Int, required: false
end
