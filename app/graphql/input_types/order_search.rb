class InputTypes::OrderSearch < Types::BaseInputObject
  graphql_name 'OrderSearch'

  argument :from, String, required: false
  argument :to, String, required: false
  argument :company_id, ID, required: false
end
