class InputTypes::Project < Types::BaseInputObject
  graphql_name 'ProjectAttributes'

  argument :year_id, ID, required: false
  argument :building_id, ID, required: false
  argument :building_number_id, ID, required: false
  argument :room_id, ID, required: false
  argument :user_ids, [ID], required: false
  argument :jkk_user_id, ID, required: false
  argument :assesment_user_id, ID, required: false
  argument :construction_user_id, ID, required: false
  argument :mc_type_id, ID, required: false
  argument :building_type_id, ID, required: false
  argument :name, String, required: false
  argument :jkk_status1, Boolean, required: false
  argument :jkk_status2, Boolean, required: false
  argument :jkk_status3, Boolean, required: false
  argument :jkk_status4, Boolean, required: false
  argument :order_number_type, String, required: false
  argument :order_number, String, required: false
  argument :order_date1, String, required: false
  argument :order_memo1, String, required: false
  argument :order_date2, String, required: false
  argument :order_memo2, String, required: false
  argument :order_date3, String, required: false
  argument :order_memo3, String, required: false
  argument :electric_start_date1, String, required: false
  argument :electric_end_date1, String, required: false
  argument :water_start_date1, String, required: false
  argument :water_end_date1, String, required: false
  argument :electric_start_date2, String, required: false
  argument :electric_end_date2, String, required: false
  argument :water_start_date2, String, required: false
  argument :water_end_date2, String, required: false
  argument :electric_start_date3, String, required: false
  argument :electric_end_date3, String, required: false
  argument :water_start_date3, String, required: false
  argument :water_end_date3, String, required: false
  argument :close_types, [String], required: false
  argument :close_reason, String, required: false
  argument :is_vermiculite, Boolean, required: false
  argument :is_finished_vermiculite, Boolean, required: false
  argument :vermiculite_date, String, required: false
  argument :process_created_date, String, required: false
  argument :assessment_date, String, required: false
  argument :send_assessment_date, String, required: false
  argument :cleanup_images_date, String, required: false
  argument :before_construction_image_date, String, required: false
  argument :after_construction_image_date, String, required: false
  argument :checklist_first_date, String, required: false
  argument :checklist_last_date, String, required: false
  argument :address, String, required: false
  argument :construction_start_date, String, required: false
  argument :construction_end_date, String, required: false
  argument :is_faq, Boolean, required: false
  argument :is_lack_image, Boolean, required: false
  argument :is_complete_image, Boolean, required: false
  argument :submit_document_date, String, required: false
  argument :available_date, String, required: false
  argument :living_year, Int, required: false
  argument :living_month, Int, required: false
  argument :leave_date, String, required: false
  argument :is_manager_asbestos_document1, Boolean, required: false
  argument :is_manager_asbestos_document2, Boolean, required: false
  argument :is_manager_asbestos_document3, Boolean, required: false
  argument :is_officework_asbestos_document1, Boolean, required: false
  argument :is_officework_asbestos_document2, Boolean, required: false
  argument :is_officework_asbestos_document3, Boolean, required: false
  argument :remark, String, required: false
  argument :total_price, Int, required: false
  argument :total_price_input, Int, required: false
  argument :image_status, String, required: false
end
