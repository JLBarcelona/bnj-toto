class InputTypes::BuildingType < Types::BaseInputObject
  graphql_name 'BuildingTypeAttributes'

  argument :name, String, required: false
end
