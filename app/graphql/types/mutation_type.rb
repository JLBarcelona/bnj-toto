module Types
  class MutationType < Types::BaseObject
    # auth
    field :signin, mutation: Mutations::Auth::Signin
    field :signout, mutation: Mutations::Auth::Signout

    # project
    field :create_project, mutation: Mutations::Project::CreateProject
    field :update_project, mutation: Mutations::Project::UpdateProject
    field :update_project_year, mutation: Mutations::Project::UpdateProjectYear
    field :create_project_item, mutation: Mutations::Project::CreateProjectItem
    field :update_project_item, mutation: Mutations::Project::UpdateProjectItem
    field :remove_project_item, mutation: Mutations::Project::RemoveProjectItem
    field :import_item, mutation: Mutations::Project::ImportItem
    field :update_project_order, mutation: Mutations::Project::UpdateProjectOrder
    field :update_project_order_csv, mutation: Mutations::Project::UpdateProjectOrderCsv
    field :create_project_image_report, mutation: Mutations::Project::CreateProjectImageReport
    field :create_spreadsheet, mutation: Mutations::Project::CreateSpreadsheet
    field :update_project_report, mutation: Mutations::Project::UpdateProjectReport
    field :update_project_items_company, mutation: Mutations::Project::UpdateProjectItemsCompany
    field :remove_project, mutation: Mutations::Project::RemoveProject
    field :edit_project_user, mutation: Mutations::Project::EditProjectUser
    field :clear_project_user, mutation: Mutations::Project::ClearProjectUser

    # project image
    field :create_project_image, mutation: Mutations::ProjectImage::CreateProjectImage
    field :update_project_image, mutation: Mutations::ProjectImage::UpdateProjectImage
    field :remove_project_image, mutation: Mutations::ProjectImage::RemoveProjectImage
    field :update_project_image_order, mutation: Mutations::ProjectImage::UpdateProjectImageOrder
    field :add_image_comment, mutation: Mutations::ProjectImage::AddImageComment
    field :add_image_comment_all, mutation: Mutations::ProjectImage::AddImageCommentAll
    field :remove_image_comment, mutation: Mutations::ProjectImage::RemoveImageComment
    field :remove_image_comment_all, mutation: Mutations::ProjectImage::RemoveImageCommentAll
    field :update_image_comment, mutation: Mutations::ProjectImage::UpdateImageComment
    field :add_comment, mutation: Mutations::ProjectImage::AddComment
    field :remove_project_image_comment, mutation: Mutations::ProjectImage::RemoveProjectImageComment

    # building
    field :create_building, mutation: Mutations::Building::CreateBuilding
    field :update_building, mutation: Mutations::Building::UpdateBuilding

    # building number
    field :create_building_number, mutation: Mutations::BuildingNumber::CreateBuildingNumber
    field :update_building_number, mutation: Mutations::BuildingNumber::UpdateBuildingNumber
    field :remove_building_number, mutation: Mutations::BuildingNumber::RemoveBuildingNumber

    # company
    field :create_company, mutation: Mutations::Company::CreateCompany
    field :update_company, mutation: Mutations::Company::UpdateCompany
    field :remove_company, mutation: Mutations::Company::RemoveCompany

    # item
    field :create_item, mutation: Mutations::Item::CreateItem
    field :update_item, mutation: Mutations::Item::UpdateItem
    field :copy_items, mutation: Mutations::Item::CopyItems

    # room
    field :create_room, mutation: Mutations::Room::CreateRoom
    field :update_room, mutation: Mutations::Room::UpdateRoom
    field :copy_room, mutation: Mutations::Room::CopyRoom
    field :remove_room, mutation: Mutations::Room::RemoveRoom

    # comment_type
    field :create_comment_type_model, mutation: Mutations::CommentType::CreateCommentTypeModel
    field :update_comment_type_model, mutation: Mutations::CommentType::UpdateCommentTypeModel
    field :remove_comment_type_model, mutation: Mutations::CommentType::RemoveCommentTypeModel
    field :update_comment_order, mutation: Mutations::CommentType::UpdateCommentOrder

    # comment
    field :create_comment, mutation: Mutations::Comment::CreateComment
    field :update_comment, mutation: Mutations::Comment::UpdateComment
    field :remove_comment, mutation: Mutations::Comment::RemoveComment
    field :add_comment_code, mutation: Mutations::Comment::AddCommentCode
    field :remove_comment_code, mutation: Mutations::Comment::RemoveCommentCode

    # company_type
    field :create_company_type_model, mutation: Mutations::CompanyType::CreateCompanyTypeModel
    field :update_company_type_model, mutation: Mutations::CompanyType::UpdateCompanyTypeModel
    field :update_company_type_order, mutation: Mutations::CompanyType::UpdateCompanyTypeOrder
    field :remove_company_type_model, mutation: Mutations::CompanyType::RemoveCompanyTypeModel

    # room_type
    field :create_room_type_model, mutation: Mutations::RoomType::CreateRoomTypeModel
    field :update_room_type_model, mutation: Mutations::RoomType::UpdateRoomTypeModel
    field :update_room_type_order, mutation: Mutations::RoomType::UpdateRoomTypeOrder

    # room_layout
    field :create_room_layout, mutation: Mutations::RoomLayout::CreateRoomLayout
    field :update_room_layout, mutation: Mutations::RoomLayout::UpdateRoomLayout
    field :update_room_layout_order, mutation: Mutations::RoomLayout::UpdateRoomLayoutOrder

    # user
    field :create_user, mutation: Mutations::User::CreateUser
    field :update_user, mutation: Mutations::User::UpdateUser

    # jkk_user
    field :create_jkk_user, mutation: Mutations::JkkUser::CreateJkkUser
    field :update_jkk_user, mutation: Mutations::JkkUser::UpdateJkkUser

    # order
    field :output_order, mutation: Mutations::Order::OutputOrder
  end
end
