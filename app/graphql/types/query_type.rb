module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    # include GraphQL::Types::Relay::HasNodeField
    # include GraphQL::Types::Relay::HasNodesField

    # user
    field :current_user, resolver: ::Queries::User::CurrentUser 
    field :users, resolver: ::Queries::User::Users
    field :user, resolver: ::Queries::User::User

    # jkk_user
    field :jkk_users, resolver: ::Queries::JkkUser::JkkUsers
    field :jkk_user, resolver: ::Queries::JkkUser::JkkUser

    # project
    field :projects, resolver: ::Queries::Project::Projects
    field :project, resolver: ::Queries::Project::Project
    field :project_items, resolver: ::Queries::Project::ProjectItems

    # order
    field :orders, resolver: ::Queries::Order::Orders

    # comment_type
    field :comment_types, resolver: ::Queries::CommentType::CommentTypes
    field :comment_type, resolver: ::Queries::CommentType::CommentType

    # comment
    field :comments, resolver: ::Queries::Comment::Comments
    field :comment, resolver: ::Queries::Comment::Comment

    # year
    field :years, resolver: ::Queries::Year::Years

    # item
    field :all_items, resolver: ::Queries::Item::AllItems
    field :items, resolver: ::Queries::Item::Items
    field :item, resolver: ::Queries::Item::Item

    # company_type
    field :company_types, resolver: ::Queries::CompanyType::CompanyTypes

    # company
    field :companies, resolver: ::Queries::Company::Companies
    field :company, resolver: ::Queries::Company::Company

    # building
    field :all_buildings, resolver: ::Queries::Building::AllBuildings
    field :buildings, resolver: ::Queries::Building::Buildings
    field :building, resolver: ::Queries::Building::Building
    field :building_number, resolver: ::Queries::Building::BuildingNumber
    field :room, resolver: ::Queries::Building::Room

    # company_type
    field :company_types, resolver: ::Queries::CompanyType::CompanyTypes
    field :company_type, resolver: ::Queries::CompanyType::CompanyType

    # room_type
    field :room_types, resolver: ::Queries::RoomType::RoomTypes
    field :room_type, resolver: ::Queries::RoomType::RoomType

    # building_type
    field :building_types, resolver: ::Queries::BuildingType::BuildingTypes
    # mc_type
    field :mc_types, resolver: ::Queries::McType::McTypes
    # room_model_type
    field :room_model_types, resolver: ::Queries::RoomModelType::RoomModelTypes

    # room_layout
    field :room_layouts, resolver: ::Queries::RoomLayout::RoomLayouts
    field :room_layout, resolver: ::Queries::RoomLayout::RoomLayout

    # unit
    field :units, resolver: ::Queries::Unit::Units
  end
end
