class Types::FileType < Types::BaseScalar
  graphql_name "File"
  description "ActionDispatch::Http::UploadedFile"

  def self.coerce_input(input_value, context)
    input_value
  end
end