# config valid only for current version of Capistrano
lock "3.16.0"

set :application, "toto"
set :repo_url, "git@github.com:daisuke-3/toto.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'puma.rb', '.env')

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads', 'node_modules')

set :branch, ask(:branch, 'main')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '3.1.2'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_roles, :all # default value

set :npm_flags, '--production --no-spin'
set :npm_roles, %w{web}

set :puma_service_unit_name, 'puma'
set :puma_systemctl_user, :system # accepts :user
#set :puma_phased_restart, true

namespace :deploy do

  # desc 'Runs rake db:migrate if migrations are set'
  # task :migrate => [:set_rails_env] do
  #   on primary fetch(:migration_role) do
  #     conditionally_migrate = fetch(:conditionally_migrate)
  #     info '[deploy:migrate] Checking changes in /db/migrate' if conditionally_migrate
  #     if conditionally_migrate && test("diff -q #{rails_root_release}/db/migrate #{rails_root_current}/db/migrate")
  #       info '[deploy:migrate] Skip `deploy:migrate` (nothing changed in db/migrate)'
  #     else
  #       info '[deploy:migrate] Run `rake db:migrate`'
  #       within rails_root_release do
  #         with rails_env: fetch(:rails_env) do
  #           execute :rake, "db:migrate"
  #         end
  #       end
  #     end
  #   end
  # end

  # def rails_root_release
  #   if fetch(:rails_root)
  #     release_path.join(fetch(:rails_root))
  #   else
  #     release_path
  #   end
  # end

  # def rails_root_current
  #   if fetch(:rails_root)
  #     current_path.join(fetch(:rails_root))
  #   else
  #     current_path
  #   end
  # end

  # after 'deploy:updated', 'deploy:migrate'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end

# namespace :load do
#   task :defaults do
#     set :conditionally_migrate, fetch(:conditionally_migrate, false)
#     set :migration_role, fetch(:migration_role, :db)
#   end
# end