require_relative "boot"

require "rails/all"
# require 'rubyXL/convenience_methods/cell'
# require 'rubyXL/convenience_methods/color'
# require 'rubyXL/convenience_methods/font'
# require 'rubyXL/convenience_methods/workbook'
# require 'rubyXL/convenience_methods/worksheet'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Toto
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
    config.i18n.default_locale = :ja
    # config.active_job.queue_adapter = :sidekiq
    config.time_zone = 'Tokyo'
    config.autoloader = :classic
    config.active_record.time_zone_aware_types = [:datetime, :time]

    config.assets.webpack_manifest =
      if File.exist?(Rails.root.join('public', 'assets', 'webpack-manifest.json'))
        JSON.parse(File.read(Rails.root.join('public', 'assets', 'webpack-manifest.json')))
      end
  end
end
